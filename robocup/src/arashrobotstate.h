/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ARASHROBOTSTATE_H
#define ROBOCUP_ARASHROBOTSTATE_H

#ifndef override
#define override
#endif

#include <services/data/robot/readstate.h>
#include <brain/robotstate.h>
#include <services/robot.h>

namespace robocup
{
class ArashRobotState
	: public brain::RobotState
{
private:
	services::Robot &robotService;
public:
	ArashRobotState(services::Robot &robotService);

	services::data::robot::ServoState neckLateral;
	services::data::robot::ServoState neckTransversal;

	virtual void fetch(services::data::robot::ReadStateResponse &response) override;

	bool update();
};
}


#endif //ROBOCUP_ARASHROBOTSTATE_H
