/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 20, 2015
 */

#ifndef ROBOCUP_VISIONDATAPROVIDER_H
#define ROBOCUP_VISIONDATAPROVIDER_H

#include <services/data/vision/fetch.h>
#include <services/vision.h>

namespace robocup
{
namespace providers
{
class VisionProvider
{
private:
	std::chrono::system_clock::time_point fetchResponseLastRequest;
	services::data::vision::FetchResponse fetchResponse;

	services::Vision &vision;
public:
	VisionProvider(services::Vision &vision);

	bool fetch(services::data::vision::FetchResponse &fetchResponse);
};
}
}


#endif //ROBOCUP_VISIONDATAPROVIDER_H
