/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 20, 2015
 */

#include "visiondataprovider.h"
 #include "../compilerdefinitions.h"

robocup::providers::VisionProvider::VisionProvider(services::Vision &vision)
	: vision(vision)
{ }

bool robocup::providers::VisionProvider::fetch(services::data::vision::FetchResponse &fetchResponse)
{
	std::chrono::system_clock::time_point current = std::chrono::system_clock::now();
	// if (std::chrono::duration_cast<std::chrono::milliseconds>(current - this->fetchResponseLastRequest).count() > 30)
	// {
		if (this->vision.fetch(fetchResponse))
		{
			this->fetchResponseLastRequest = current;
			// fetchResponse = this->fetchResponse;
			// VERBOSE(fetchResponse.goalArea.balls[0].center.x);
			VERBOSE("Update");
			return true;
		}
	// }
	// else
	// {
		// fetchResponse = this->fetchResponse;
	// }
	return false;
}
