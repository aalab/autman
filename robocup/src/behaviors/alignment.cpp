/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <iostream>
#include <services/data/vision/fetch.h>
#include <services/data/line.h>
#include <thread>

#include "alignment.h"

#include "../compilerdefinitions.h"
#include "gccontroller.h"

namespace data = services::data;
namespace rdata = services::data::robot;
namespace vdata = services::data::vision;

robocup::behaviors::Alignment *robocup::behaviors::Alignment::instance = nullptr;

robocup::behaviors::Alignment::Alignment(robocup::Arbitrator &arbitrator)
	: arbitrator(arbitrator)
{
	instance = this;
}

void robocup::behaviors::Alignment::process()
{

	if (this->dumpUiData)
	{
		VERBOSE("--");
	}
	vdata::FetchResponse fetchResponse;
	if (this->arbitrator.visionDataProvider.fetch(fetchResponse) && (!fetchResponse.goalArea.lines.empty()))
	{
		services::data::Line2D *bigger = &fetchResponse.goalArea.lines[0];
		for (services::data::Line2D &c : fetchResponse.goalArea.lines)
		{
			if (c.hypot() > bigger->hypot())
			{
				bigger = &c;
			}
		}
		services::data::Line2D line2;
		// this->arbitrator.currentCameraCalibration->i2w(*bigger, line2);
		if (bigger->a.x > bigger->b.x)
		{
			line2.a = bigger->b;
			line2.b = bigger->a;
		}
		else
		{
			line2.a = bigger->a;
			line2.b = bigger->b;
		}
		double angle = line2.inclination() + this->arbitrator.state.neckTransversal.angle;
		if (this->dumpUiData)
		{
			VERBOSE("Angle: " << angle << " (" << R2D(angle) << "º)");

			for (auto &ball : fetchResponse.goalArea.balls)
			{
				VERBOSE(ball.type);
			}
		}
		rdata::MoveAutGait move;
		move.tilt = 0.8;
		if (std::abs(angle) > D2R(10))
		{
			if (this->dumpUiData)
			{
				VERBOSE("Misaligned!");
			}
			if (angle > 0)    // turn right
			{
				if (this->dumpUiData)
				{
					VERBOSE("Turn right");
				}
				move.vt = -0.05;
			}
			else            // turn left
			{
				if (this->dumpUiData)
				{
					VERBOSE("Turn left");
				}
				move.vt = 0.05;
			}
		}
		else
		{
			VERBOSE("Aligned!");
		}
		for (auto &ball : fetchResponse.goalArea.balls)
		{
			VERBOSE("x: " << ball.center.x << ", y: " << ball.center.y);
		}

		this->arbitrator.robot.move(move);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

void robocup::behaviors::Alignment::disable()
{
	if (this->isEnabled())
	{
		Behavior::disable();

		rdata::Move move;
		move.frames.emplace_back(0.0, 0.0, 0.0);
		this->arbitrator.robot.move(move);

	}
}
