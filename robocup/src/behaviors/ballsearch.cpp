/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 21, 2015
 */

#include "ballsearch.h"

#include <services/data/vision/fetch.h>
#include <iostream>

#include "../compilerdefinitions.h"
#include "../robocuparbitrator.h"

#include <services/robot.h>


namespace data = services::data;
namespace rdata = services::data::robot;
namespace vdata = services::data::vision;

void
robocup::behaviors::BallSearch::process()
{
	if (this->dumpUiData)
	{
		VERBOSE("--");
	}
	vdata::FetchResponse fetchResponse;
	if (this->arbitrator.visionDataProvider.fetch(fetchResponse))
	{
		// Simple blocking motion based on camera middle offset
		// This center_offser need to be changed if a different camera is used.
		int camera_width = 320;
		rdata::MoveAutGait move;

		move.vx = 0;
		move.vy = 0;
		move.vt = 0;
		move.motion = 100;
		move.pan = 0;
		move.tilt = 0.6;

		if (fetchResponse.goalArea.balls.size() != 0)
		{
			auto &ball = fetchResponse.goalArea.balls[0];

			//assume the camera size is 320x240.
			center_offset = ball.center.x - (camera_width / 2.0);
			VERBOSE("Center offset: " << center_offset);
			int not_move_threshold = 40;
			unsigned int kick_threshold = 10000;
			double max_side_step = 0.4;
			// double side_step = 0.03;

			if (std::abs(center_offset) > not_move_threshold)
			{
				double factorX = center_offset / (camera_width / 2.0);
				if (center_offset > 0)
				{
					//in terms of robot it is on the left
					VERBOSE("Robot Move Right");
					// send motion commend with dy<0 maybe = -0.35 for fixed value if no time to tune
					//				double vy = -1*center_offset/camera_width*max_side_step; //use simple linear proportional behaviour
					move.vy = factorX * max_side_step;
				}
				else if (center_offset < 0)
				{
					VERBOSE("Robot Move left");
					//send motion commend with dy>0 maybe = 0.35
					//double vy = -1*center_offset/camera_width*max_side_step; //use simple linear proportional behaviour
					move.vy = factorX * max_side_step;
				}
			}
			else
			{
				//if size is small
				if (ball.size > kick_threshold)
				{
					//current kick_threshold set very high as other robot touch the goal keeper will need to be removed
					VERBOSE("Robot Kick"); //kick to clean out the ball
					move.motion = 1; //kick motion should be at 1
				}
				else
				{
					//std::cout << "no motion" << std::endl;
					VERBOSE("Robot No motion");
					move.motion = 100; //kick motion should be at 1
				}
				//send no motion commend
			}
			center_offset = 0;
		}
		else
		{
			VERBOSE("Robot No motion");
		}
		this->arbitrator.robot.move(move);
	}
}
