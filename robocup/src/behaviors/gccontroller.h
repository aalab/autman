/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_GCCONTROLLER_H
#define ROBOCUP_GCCONTROLLER_H

#include <brain/base/behavior.h>
#include <services/vision.h>
#include <services/robot.h>
#include <services/robocup/gamecontroller.h>

#ifndef override
#define override
#endif

namespace robocup
{
class Arbitrator;

namespace behaviors
{
class GCController
	: public brain::Behavior
{
public:
	static GCController *instance;
protected:
	Arbitrator &arbitrator;


	virtual void process() override;
public:
	services::robocup::GameController &gameController;

	GCController(robocup::Arbitrator &arbitrator, services::robocup::GameController &gameController);

	unsigned int getState();
};
}
}


#endif //ROBOCUP_GCCONTROLLER_H
