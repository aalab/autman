/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <iostream>
#include <services/data/vision/fetch.h>
#include <services/data/line.h>
#include <thread>

#include "gccontroller.h"

#include "../compilerdefinitions.h"
#include "alignment.h"

namespace data = services::data;
namespace rdata = services::data::robot;
namespace vdata = services::data::vision;

robocup::behaviors::GCController *robocup::behaviors::GCController::instance = nullptr;

robocup::behaviors::GCController::GCController(robocup::Arbitrator &arbitrator, services::robocup::GameController &gameController)
	: arbitrator(arbitrator), gameController(gameController)
{
	instance = this;
}

void robocup::behaviors::GCController::process()
{
	this->gameController.createResponse();
	this->gameController.update();
	if (this->gameController.getState() == STATE_SET)
	{
		Alignment::instance->disable();
	}
	if (this->gameController.getState() == STATE_PLAYING)
	{
		Alignment::instance->enable();
	}

	/*
	VERBOSE("Wait 5 sec");
	sleep(5);
	VERBOSE("Disabling");
	Alignment::instance->disable();
	VERBOSE("Wait 5 sec");
	sleep(5);
	VERBOSE("Enabling");
	Alignment::instance->enable();
	VERBOSE("Wait 5 sec");
	sleep(5);
	*/
}

unsigned int robocup::behaviors::GCController::getState()
{
	return this->gameController.getState();
}
