# AALab Robot Project

Visit the [AALab website](http://aalab.cs.umanitoba.ca).

This is the main repository of the **AALab** for our robot framework. On this repository each folder,
usually, contains a different project with its own `CMakeLists.txt` containing all the dependencies
configured properly according the standards describeds further in this documentation.

[./libs](./libs)
===========
Directory that keeps all the third libraries, which does not have its own repository. Inside of each
library you will find the instalation instruction.