/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 6, 2015.
 */

#include <iostream>
#include "configuration.h"

using namespace std;

Configuration &Configuration::operator<<(const Json::Value &json)
{
	this->udpServerPort = json["udpServer"]["port"].asUInt();

	// this->visionModuleHost = json["visionModule"]["host"].asString();
	this->visionModulePort = json["visionModule"]["port"].asUInt();

	cout << "UdpServerPort: " << this->udpServerPort << endl;
	cout << "visionModuleHost: " << this->visionModuleHost << endl;
	cout << "visionModulePort: " << this->visionModulePort << endl;

	return *this;
}
