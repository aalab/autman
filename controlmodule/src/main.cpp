#include <iostream>
#include <boost/asio/signal_set.hpp>
#include "udpserver.h"
#include "globals.h"

using namespace std;

int main(int argc, char* argv[])
{
	cout <<
		"Control module\n==============+ Version: " << __PROJECT_VERSION__
		<< "+ Compiled: " << __DATE__ << " " << __TIME__
		<< endl;


	cout << "Creating globals" << endl;
	control::Globals* g = control::Globals::getInstance();

	if (argc < 2)
	{
		cerr << "Usage: " << argv[0] << " <config file>" << endl;
		return 1;
	}
	else
	{
		ifstream configFile;
		configFile.open(argv[1]);
		if (configFile)
		{
			Json::Reader reader;
			/*
			if (reader.parse(configFile, configJson, false))
				g->configuration << configJson;
			else
			{
				cerr << "Error parsing the config file." << endl;
				return 2;
			}
			*/
		}
		else
		{
			cerr << "Error opening the config file: " << strerror(errno) << endl;
			return 3;
		}
	}

	cout << "Initializing ... " << endl;
	if (!g->init())
	{
		cerr << "Error on initializing globals." << endl;
		return 4;
	}

	cout << "Creating UdpServer" << endl;
	control::UdpServer server(12345);
	server.start();

	cout << "Sitting down ... ";
	g->getInstance()->getRobotController()->sitdown(true);

	delete g;
	cout << "exiting!" << endl;
	return 0;
}