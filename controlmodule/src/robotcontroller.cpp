/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#include "compilerdefinitions.h"
#include <MotionManager.h>
#include "globals.h"

#ifdef OP2
#include <LinuxDARwIn.h>
#else
#include <LinuxDARwIn.h>
#endif

#include <libgen.h>
#include "robotcontroller.h"
#include "modules/head.h"
#include "modules/gamepad.h"

using namespace control;
using namespace Robot;
using namespace std;

control::RobotController *control::RobotController::_instance = NULL;

RobotState &control::RobotState::operator<<(Json::Value &json)
{
	this->headPan = json["head"]["pan"].asDouble();
	this->headTilt = json["head"]["tilt"].asDouble();
	return *this;
}

RobotState &control::RobotState::operator>>(Json::Value &json)
{
	json["head"]["pan"] = this->headPan;
	json["head"]["tilt"] = this->headTilt;
	return *this;
}

control::RobotController::RobotController()
	: enabled(false), isWalking(false)
{
	this->_instance = this;

	this->rlSum = 0;
	this->rlLast = 0;
	this->rlCount = 0;

	VERBOSE("Initializing" << endl);

	/*
	double
		zo2 = Walking::GetInstance()->Z_OFFSET,
		hpo2 = Walking::GetInstance()->HIP_PITCH_OFFSET;
	*/

	double
		duration = 1000/*,
		zo = CONST_Z_OFFSET,
		hpo = CONST_HIP_PITCH_OFFSET*/;

	this->waitMilliseconds = duration*2;

	VERBOSE("RobotController::RobotController" << endl);
	VERBOSE2("\tAdding modules: " << endl);

#ifndef ENABLE_ONLY_HEAD
	VERBOSE2("\t\tWalking module");
	MotionManager::GetInstance()->AddModule(Walking::GetInstance());
	VERBOSE2(" [ok]" << endl << "\t\tHead module");
#endif

	MotionManager::GetInstance()->AddModule(Head::GetInstance());
	VERBOSE2(" [ok]" << endl);
	VERBOSE2("\t\tAll modules added" << endl);

	cout << "RobotController::RobotController: Actions file loaded" << endl;

#ifndef ENABLE_ONLY_HEAD
	Walking::GetInstance()->m_Joint.SetEnableBody(true,true);
#endif
	Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);

	cout << "RobotController::RobotController: Activating joints..." << endl;

	Head::GetInstance()->m_Joint.SetPGain(JointData::ID_HEAD_PAN, 8);
	Head::GetInstance()->m_Joint.SetPGain(JointData::ID_HEAD_TILT, 8);

	cout << "RobotController::RobotController: Head gain set..." << endl;

	this->SetEnable(true, false);

	cout << "RobotController::RobotController: Robot controller enabled..." << endl;

	/*
	timeval pt, ct;

	cout << "RobotController::RobotController: Smoothing to stable position..." << endl;
	gettimeofday(&pt, NULL);
	__suseconds_t diff;

	while (diff < duration)
	{
		gettimeofday(&ct, NULL);
		diff = ((ct.tv_sec*1000000 + ct.tv_usec) - (pt.tv_sec*1000000 + pt.tv_usec)) / 1000;

		tmp = zo2 + (zo - zo2)*(diff/duration);
#ifdef APPLY
		Walking::GetInstance()->Z_OFFSET = tmp;
#endif
		tmp = hpo2 + (hpo - hpo2)*(diff/duration);
#ifdef APPLY
		Walking::GetInstance()->HIP_PITCH_OFFSET = tmp;
#endif
#ifdef APPLY
		Walking::GetInstance()->Process();
#endif
	}
	 */
	this->waitMilliseconds = 0;

	/*
	Walking::GetInstance()->Z_OFFSET = zo;
	Walking::GetInstance()->HIP_PITCH_OFFSET = hpo;
	*/
	cout << "RobotController::RobotController: Starting process..." << endl;
}

void control::RobotController::standUp()
{
#ifdef APPLY
#ifndef ENABLE_ONLY_HEAD
	Action::GetInstance()->Start(STAND_UP);
	Action::GetInstance()->Stop();
#endif
#endif
}

void control::RobotController::release()
{
	//
}

void control::RobotController::startWalking()
{
#ifndef ENABLE_ONLY_HEAD
#ifdef APPLY
	if (!this->isWalking)
	{
		Walking::GetInstance()->m_Joint.SetEnableBody(true,true);
		Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);

		Walking::GetInstance()->Start();

		this->isWalking = true;
	}
#endif
#endif
}

void control::RobotController::stopWalking()
{
#ifndef ENABLE_ONLY_HEAD
#ifdef APPLY
	if (this->isWalking)
	{
		if (Walking::GetInstance()->IsRunning())
		{
			Walking::GetInstance()->Stop();
			this->waitAction();
		}
		this->isWalking = false;
	}
#endif
#endif
}

void control::RobotController::Initialize()
{
	// Action::GetInstance()->DEBUG_PRINT = true;
}

void control::RobotController::Process()
{
	this->isRunning = true;
	try
	{
		gettimeofday(&this->currentTime, NULL);
		// diff in miliseconds
		// TODO: Check the day stuff (probable BUG at midnight)
		__suseconds_t diff = ((this->currentTime.tv_sec*1000000 + this->currentTime.tv_usec) - (this->priorTime.tv_sec*1000000 + this->priorTime.tv_usec)) / 1000;
		__suseconds_t uiDiff = ((this->currentTime.tv_sec*1000000 + this->currentTime.tv_usec) - (this->uiPriorTime.tv_sec*1000000 + this->uiPriorTime.tv_usec)) / 1000;

		if (diff < this->waitMilliseconds)
		{
			cout << "RobotController::Process - Wait" << endl;
			this->isRunning = false;
			return;
		}
		else
			this->waitMilliseconds = 0;

		double tmp, tmp2;

		if (!this->actionsSet.isEmpty())
		{
			cout << "RobotController::Process - !isEmpty" << endl;
			while (!this->actionsSet.isEmpty())
			{
				cout << "RobotController::Process - while" << endl;
				this->actionsSet.next();
			}
		}
		else
		{
			if (uiDiff > 300)
				VERBOSE("actionSet.isEmtpy: true" << endl);

			if (this->command.params == NULL)
			{
				this->stopWalking();
			}
			else if (this->command.params == &this->command.followline)
			{
				if (uiDiff > 300)
					VERBOSE("FollowLine" << endl);

				if (this->command.followline.isLineFound)
				{
					if (uiDiff > 300)
						VERBOSE("followline.isLineFound" << endl);

					double
						bottomAngle = RADTODEG((atan2(this->command.followline.bottomLine.Y,
														 this->command.followline.bottomLine.X) - M_PI_2l) * -1),
						lineAngle = RADTODEG((atan2(this->command.followline.bottomLine.Y - this->command.followline.topLine.Y,
													   this->command.followline.bottomLine.X - this->command.followline.topLine.X) + M_PI_2l) * -1),

						aAmplitude = MINMAXRATIO(this->aMoveAmplitudeMin, this->aMoveAmplitudeMax, (std::min(std::max(lineAngle, -25.0), 25.0) / 25.0)),
						xAmplitude = MINMAXRATIO(this->xMoveAmplitudeMin, this->xMoveAmplitudeMax, (1.0-std::min(1.0, std::abs(lineAngle / 25.0)))),
						yAmplitude;

					VERBOSE("X factor: " << (1.0-std::min(1.0, std::abs(lineAngle / 25.0))) << endl);
					VERBOSE("X amplitude: " << (xAmplitude) << endl);

					if (std::abs(lineAngle) > 25)
					{
						if (std::abs(bottomAngle) < 20.0)
						{
							yAmplitude = this->yMoveAmplitudeMin;
						}
						else
						{
							yAmplitude = this->yMoveAmplitudeMax * (bottomAngle > 0 ? 1 : -1);
							// xAmplitude = 0;
							aAmplitude = 0;
						}
					}
					else
					{
						yAmplitude = 0.0;
					}
					yAmplitude = 0.0; // Overriding to test.

					if (uiDiff > 300)
					{
						VERBOSE(endl
							<< "\tbottomAngle: " << bottomAngle << endl
							<< "\tlineAngle: " << lineAngle << endl
						);
					}
#ifdef APPLY
					this->startWalking();
					Walking::GetInstance()->A_MOVE_AMPLITUDE = aAmplitude;
					Walking::GetInstance()->X_MOVE_AMPLITUDE = xAmplitude;
					Walking::GetInstance()->Y_MOVE_AMPLITUDE = yAmplitude;
					// Walking::GetInstance()->X_MOVE_AMPLITUDE = 10;
					// Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;

					if (uiDiff > 300)
					{
						VERBOSE(endl
							<< "\t+ A: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl
							<< "\t+ X: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl
							<< "\t+ Y: " << Walking::GetInstance()->Y_MOVE_AMPLITUDE << endl
						);
					}

					this->waitMilliseconds = 0;
#endif
				}
				else
				{
					if (uiDiff > 300)
						VERBOSE("!followline.isLineFound" << endl);
					// TODO: Scan
					this->stopWalking();
				}
			}
			else if (this->command.params == &this->command.followSprintMarker)
			{
				if (uiDiff > 300)
					cout << "followSprintMarker" << endl;

				if (this->command.followSprintMarker.found || (this->command.followSprintMarker.center.X > -1))
				{
					double distX = this->command.followSprintMarker.center.X - 320/2.0;
					double distY = 240/2.0 - this->command.followSprintMarker.center.Y;

					if (uiDiff > 300)
					{
						cout << "Mode: " << this->command.followSprintMarker.mode << endl;

						cout << "X: " << this->command.followSprintMarker.center.X << endl;
						cout << "Y: " << this->command.followSprintMarker.center.Y << endl;

						cout << "distX: " << distX << endl;
						cout << "distY: " << distY << endl;
						cout << "--------------" << endl;
					}
#ifdef APPLY
					if (this->command.followSprintMarker.found)
						control::modules::Head::track(this->command.followSprintMarker.center.X, this->command.followSprintMarker.center.Y,
							Camera::WIDTH, Camera::HEIGHT, 20.0);
#endif
					double a = Robot::Head::GetInstance()->GetHomePan() - Robot::Head::GetInstance()->GetPanAngle();

					VERBOSE("A = " << a << endl);

					if (std::abs(a) >= 20.0)
					{
#ifdef APPLY
						if (this->command.followSprintMarker.mode == "backward")
							Walking::GetInstance()->X_MOVE_AMPLITUDE = -5;
						else
							Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
						Walking::GetInstance()->A_MOVE_AMPLITUDE = 10 * (a < 0 ? 1 : -1);
						this->startWalking();
#endif
					}
					else
					{
#ifdef APPLY
						if (this->command.followSprintMarker.mode == "backward")
							Walking::GetInstance()->X_MOVE_AMPLITUDE = -7.0;
						else
							Walking::GetInstance()->X_MOVE_AMPLITUDE = 15.0;
						Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
						Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
						this->startWalking();
#endif
					}

/*
					if (std::abs(a) > 10)
					{
						Walking::GetInstance()->A_MOVE_AMPLITUDE = MINMAXRATIO(this->aMoveAmplitudeMin, 10, std::min(std::max(Head::GetInstance()->GetPanAngle()/-50.0, -1.0), 1.0));
						if (uiDiff)
						{
							cout << "A: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl;
							cout << "P: " << Head::GetInstance()->GetPanAngle() << endl;
						}
					}
					Walking::GetInstance()->X_MOVE_AMPLITUDE = 15;
					this->startWalking();
					*/
				}
				else
				{
#ifdef APPLY
					// TODO: Scan
					this->stopWalking();
#endif
				}
				this->waitMilliseconds = 0;
			}
			else if (this->command.params == &this->command.headTrackerMove)
			{
				if (uiDiff > 300)
					cout << "headTrackerMove" << endl;
#ifdef APPLY
				if (this->command.headTrackerMove.found)
				{
					cout << "headTrackerMove: FOUND!" << endl;
					// Head::GetInstance()->MoveTracking(this->command.headTrackerMove.point);
					control::modules::Head::track(this->command.headTrackerMove.point.X, this->command.headTrackerMove.point.Y,
						Camera::WIDTH, Camera::HEIGHT, 20.0);
					cout << "headTrackerMove: MoveTracking to " << this->command.headTrackerMove.point.X << ":" << this->command.headTrackerMove.point.Y << endl;
				}
#endif
			}
			else if ((this->command.params == &this->command.keyboard) || (this->command.params == &this->command.gamepad))
			{
				modules::GamepadController::getInstance()->enabled = true;
			}
		}

		this->rlSum += MotionStatus::RL_GYRO;

		if (this->rlLast == MotionStatus::RL_GYRO)
		{
			this->rlCount++;
			if (this->rlCount > 200)
			{
				this->rlCount = 0;
				this->rlSum = 0;
			}
		}
		else
		{
			this->rlLast  = MotionStatus::RL_GYRO;
			this->rlCount = 0;
		}

		if (uiDiff > 300)
		{
/*
			double rlAccel = ((MotionStatus::RL_ACCEL/128.0) - 4.0) * 9.81;
			double fbAccel = ((MotionStatus::FB_ACCEL/128.0) - 4.0) * 9.81;
			double zAccel = ((MotionStatus::Z_ACCEL/128.0) - 4.0) * 9.81;

			double
				rlInclination = atan2(rlAccel, zAccel) * 180/3.14,
				fbInclination = atan2(fbAccel, zAccel) * 180/3.14;
*/

			/*
			cout << "-------------------------------" << endl
				<< "  Z: " << MotionStatus::Z_ACCEL << " / " << MotionStatus::Z_GYRO << " / "
				<< " RL: " << MotionStatus::RL_ACCEL << " / " << MotionStatus::RL_GYRO << " / "
				<< "RLS: " << (this->rlSum/(double)this->rlCount) << " / " << this->rlSum
				<< endl << " z_offset " << Walking::GetInstance()->Z_OFFSET << " / hip offset " << Walking::GetInstance()->HIP_PITCH_OFFSET << endl;
			cout << "SPEED_BASE_SCHEDULE: " << Action::GetInstance()->SPEED_BASE_SCHEDULE << endl;
			*/
			this->uiPriorTime = this->currentTime; // TODO Copy Data!? If yes, it is okay.
		}
		this->priorTime = this->currentTime; // TODO Copy Data!? If yes, it is okay.
		this->state.headPan = Head::GetInstance()->GetPanAngle();
		this->state.headTilt = Head::GetInstance()->GetTiltAngle();
	}
	catch (const exception & e)
	{
		cout << e.what() << endl;
	}
	this->isRunning = false;
}

void control::RobotController::SetEnable(bool enable, bool exclusive)
{
	if (this->enabled != enable)
	{
		if (enable)
		{
			gettimeofday(&this->priorTime, NULL);
			gettimeofday(&this->uiPriorTime, NULL);
		}
	}
}

bool control::RobotController::IsRunning()
{
	return this->isRunning;
}

void control::RobotController::getUpFront()
{
#ifndef ENABLE_ONLY_HEAD
	this->waitMilliseconds = 15000;
	cout << "RobotController::getUpFront()" << endl;
#ifdef APPLY
	this->stopWalking();

	// Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	MotionManager::GetInstance()->SetEnable(true);

	if (!Action::GetInstance()->Start(STAND_UP_FRONT))
		cerr << "Error starting get front up action.";

	// usleep(this->waitMilliseconds*1000);
#endif
#endif
}

void control::RobotController::getUpBack()
{
#ifndef ENABLE_ONLY_HEAD
	this->waitMilliseconds = 15000;
	cout << "RobotController::getUpBack()" << endl;
#ifdef APPLY
	this->stopWalking();

	// Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	Action::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	MotionManager::GetInstance()->SetEnable(true);

	if (!Action::GetInstance()->Start(STAND_UP_BACK))
		cerr << "Error starting get front up action.";

	// usleep(this->waitMilliseconds*1000);
#endif
#endif
}

void control::RobotController::waitAction()
{
	usleep(8000);
}

void control::RobotController::turn(double angle)
{
#ifndef ENABLE_ONLY_HEAD
	cout << "RobotController::turn(): " << angle << endl;

#ifdef APPLY
	this->command.params = NULL;

	Walking::GetInstance()->X_MOVE_AMPLITUDE = 0.0;
	Walking::GetInstance()->A_MOVE_AMPLITUDE = 5.0;
	this->waitMilliseconds = std::abs(
		Walking::GetInstance()->PERIOD_TIME * angle / Walking::GetInstance()->A_MOVE_AMPLITUDE
		/ Walking::GetInstance()->PERIOD_TIME)
							 * Walking::GetInstance()->PERIOD_TIME;

	Walking::GetInstance()->Start();

	usleep(this->waitMilliseconds*1000);
#endif
#endif
}

void control::RobotController::sitdown(bool blocking)
{
#ifndef ENABLE_ONLY_HEAD
	this->waitMilliseconds = 5000;
	cout << "RobotController::sitdown()" << endl;
#ifdef APPLY
	this->stopWalking();

	Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	MotionManager::GetInstance()->SetEnable(true);

	if (!Action::GetInstance()->Start(SIT_DOWN))
		cerr << "Error starting get front up action.";
	else
	{
		while (Action::GetInstance()->IsRunning())
			this->waitAction();
	}
#endif
#endif
}

void control::RobotController::restartRotationRL()
{
	this->rlLast = 0;
	this->rlCount = 0;
	this->rlSum = 0;
}

RobotController &RobotController::getInstance()
{
	return *_instance;
}
