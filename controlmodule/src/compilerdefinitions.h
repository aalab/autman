/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 4/3/15.
 */

#ifndef _CONTROLMODULE_COMPILERDEFINITIONS_H_
#define _CONTROLMODULE_COMPILERDEFINITIONS_H_

#define MINMAXRATIO(min, max, ratio) min + ((max - min) * (ratio))

#ifdef USE_OP2LIB
	#ifdef MX28_1024
		#define MOTION_FILE_PATH    "data/op2/motion_1024.bin"
	#else
		#define MOTION_FILE_PATH    "data/op2/motion_4096.bin"
	#endif
	#define INI_FILE_PATH       "data/op2/config.ini"
#else
	#define MOTION_FILE_PATH    "data/op1/motion_4096.bin"
	#define INI_FILE_PATH       "data/op1/config.ini"
#endif

#define CONST_Z_OFFSET 36
#define CONST_HIP_PITCH_OFFSET 20

#define U2D_DEV_NAME0       "/dev/ttyUSB0"
#define U2D_DEV_NAME1       "/dev/ttyUSB1"

#define VERBOSE(x) cerr << "[VERBOSE] " << __FILE__ << " " << __FUNCTION__ << ":" << __LINE__ << "] " << x
#define VERBOSE2(x) cerr << x

#endif //_CONTROLMODULE_COMPILERDEFINITIONS_H_
