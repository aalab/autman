#ifndef __UDP_VISION_SERVER_HPP__
#define __UDP_VISION_SERVER_HPP__

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/asio/signal_set.hpp>

#include <jsoncpp/json/value.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <glob.h>
#include <boost/asio/deadline_timer.hpp>

#define UDPSERVER_BUFFER_SIZE 1024

namespace control
{
	class UdpServer
	{
	private:
		boost::asio::io_service ioService;
		boost::asio::signal_set signals;
		char data[UDPSERVER_BUFFER_SIZE];

		boost::asio::ip::udp::socket socket;
		boost::asio::ip::udp::endpoint remoteEP;
		boost::asio::ip::udp::endpoint senderEndpoint;

		// boost::asio::deadline_timer nocommandTimer;

		Json::Reader jsonReader;
	public:
		UdpServer(unsigned int port);

		void start();
		void stop();

		void handleReceiveFrom(const boost::system::error_code& error, size_t bytesReceived);
		void handleSendTo(const boost::system::error_code& error, size_t bytesSent);
		void handleNoCommandTimer();
	private:
		int sendResponse(std::string const &msg);

		void bindReceiveFrom();

		void send(std::string &message);

		void renewCommandTimer();
	};
}

#endif /* __UDP_VISION_SERVER_HPP__ */
