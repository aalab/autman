/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 4/9/15.
 */

#ifndef _CONTROLMODULE_HEAD_H_
#define _CONTROLMODULE_HEAD_H_

#include <jsoncpp/json/value.h>
#include "actionset.h"

namespace actions
{
	class HeadHome
		: public Action
	{
	public:
		HeadHome(ActionSet *actionSet);

		virtual void init();

		HeadHome& operator<<(Json::Value &json);
	};

	class HeadPan
		: public Action
	{
	public:
		HeadPan(ActionSet *actionSet);

		double angle;

		virtual void init();

		HeadPan& operator<<(Json::Value &json);
	};

	class HeadTilt
		: public Action
	{
	public:
		HeadTilt(ActionSet *actionSet);

		double angle;

		virtual void init();

		HeadTilt& operator<<(Json::Value &json);
	};
}


#endif //_CONTROLMODULE_HEAD_H_
