/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 3, 2015
 */

#ifndef _CONTROLMODULE_ACTIONSET_H_
#define _CONTROLMODULE_ACTIONSET_H_

#include <deque>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/asio.hpp>
#include <iostream>

namespace actions
{
	class ActionSet;

	class Action
	{
	private:
		ActionSet *actionSet;
	protected:
		virtual void doProcess();
	public:
		Action(ActionSet *actionSet);

		std::string name;

		virtual void run();
		virtual void process();

		virtual void release();
	};

	class ActionSet
	{
	private:
		boost::mutex mutexActionEmpty;
		boost::condition_variable actionsEmptyCond;

		boost::mutex mutexActions;
		std::deque<Action*> actions;
		Action* currentAction;
		bool running;

		Action* pop();
	public:
		ActionSet();

		void next();
		void push(Action* action);

		void clear();

		bool isEmpty();

		void sendEmptyCommand();
	};
}

#endif //_CONTROLMODULE_ACTIONSET_H_
