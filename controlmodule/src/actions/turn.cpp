/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 3, 2015
 */

#include <iostream>
#include <Walking.h>
#include <MotionStatus.h>
#include "turn.h"

#include "../compilerdefinitions.h"
#include "../globals.h"

using namespace std;
using namespace actions;
using namespace Robot;

Turn::Turn(ActionSet *actionSet, double angle)
	: angle(angle), Action(actionSet)
{ }

Turn &Turn::operator<<(Json::Value &json)
{
	this->angle = json["angle"].asDouble();
	return *this;
}

float easeIn (float t,float b , float c, float d)
{
	return c*t/d + b;
}

void Turn::doProcess()
{
	control::Globals::getInstance()->getRobotController()->restartRotationRL();
	cout << "Turn::action(): " << this->angle << endl;

	// Walking::GetInstance()->A_MOVE_AIM_ON = true;

	Walking::GetInstance()->X_MOVE_AMPLITUDE = 0.0;
	if (this->angle > 0)
		Walking::GetInstance()->A_MOVE_AMPLITUDE = 15.0;
	else
		Walking::GetInstance()->A_MOVE_AMPLITUDE = -15.0;

	double d = abs((
					   std::abs(Walking::GetInstance()->PERIOD_TIME * this->angle / Walking::GetInstance()->A_MOVE_AMPLITUDE)
				   ) / 1000.0 * 1.7 / Walking::GetInstance()->PERIOD_TIME) * Walking::GetInstance()->PERIOD_TIME;

	cout << "\tDuration: " << d << endl;

	Walking::GetInstance()->m_Joint.SetEnableBody(true,true);
	Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);

	Walking::GetInstance()->Start();
	usleep(d * 1000000);
	Walking::GetInstance()->Stop();}
