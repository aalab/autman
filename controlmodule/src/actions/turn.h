/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 3, 2015
 */

#ifndef _CONTROLMODULE_TURN_H_
#define _CONTROLMODULE_TURN_H_

#include <jsoncpp/json/value.h>
#include "actionset.h"

namespace actions
{
	class Turn
		: public Action
	{
	public:
		Turn(ActionSet *actionSet, double angle);

		double angle;
	protected:
		virtual void doProcess() override;

		Turn& operator<<(Json::Value &json);
	};
}

#endif //_CONTROLMODULE_TURN_H_
