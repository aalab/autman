/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 4, 2015.
 */

#ifndef _CONTROLMODULE_WALK_H_
#define _CONTROLMODULE_WALK_H_

#include <jsoncpp/json/value.h>
#include "actionset.h"

namespace actions
{
	class Walk:
		public Action
	{
	public:
		/*
		 * Distance in centimeters.
		 */
		Walk(actions::ActionSet *actionSet, double distance);

		double distance;


	protected:
		virtual void doProcess() override;

		Walk& operator<<(Json::Value &json);
	};
}


#endif //_CONTROLMODULE_WALK_H_
