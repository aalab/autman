/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 3, 2015
 */

#include "actionset.h"

using namespace actions;

void Action::process()
{
	this->doProcess();
}

void Action::doProcess()
{ }

void Action::run()
{
	this->process();
}

ActionSet::ActionSet()
	: running(false), actionsEmptyCond()
{ }

Action* ActionSet::pop()
{
	boost::lock_guard<boost::mutex> lock(this->mutexActions);
	Action *result = NULL;
	if (!this->actions.empty())
	{
		result = this->actions.front();
		this->actions.pop_front();
	}
	return result;
}

void ActionSet::push(Action *action)
{
	boost::lock_guard<boost::mutex> lock(this->mutexActions);
	this->actions.push_back(action);
	this->actionsEmptyCond.notify_one();
}

void ActionSet::clear()
{
	boost::lock_guard<boost::mutex> lock(this->mutexActions);
	for (std::deque<actions::Action *>::iterator action = this->actions.begin(); action != this->actions.end(); ++action)
		delete (*action);
	this->actions.clear();
}

Action::Action(ActionSet *actionSet)
	: actionSet(actionSet)
{
	actionSet->push(this);
}

bool ActionSet::isEmpty()
{
	return this->actions.empty();
}

void ActionSet::next()
{
	std::cout << "ActionSet::next" << std::endl;
	Action* action = this->pop();
	if (action != NULL)
	{
		std::cout << "ActionSet::next - action != NULL" << std::endl;
		this->currentAction = action;
		std::cout << "run action" << std::endl;
		action->run();
		std::cout << "delete action" << std::endl;
		delete this->currentAction;
		this->currentAction = NULL;
		if (this->isEmpty())
			this->sendEmptyCommand();
	}
}

void ActionSet::sendEmptyCommand()
{
	boost::asio::io_service io_service;

	boost::asio::ip::udp::resolver resolver(io_service);
	boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), "127.0.0.1", "2134");
	boost::asio::ip::udp::endpoint receiver_endpoint = *resolver.resolve(query);

	boost::asio::ip::udp::socket socket(io_service);
	socket.open(boost::asio::ip::udp::v4());

	std::stringstream str;
	str << "{ \"name\" : \"emptyqueue\" }";
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
}

void Action::release()
{ }
