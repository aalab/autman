/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 4/9/15.
 */

#include "../compilerdefinitions.h"

#include <Head.h>
#include "head.h"

using namespace std;
using namespace actions;

HeadHome::HeadHome(ActionSet *actionSet)
	: Action(actionSet)
{ }

void HeadHome::init()
{
	cout << "HeadHome::init(): " << endl;

	Robot::Head::GetInstance()->MoveToHome();
	double d = 0.5;									// seconds
	cout << "\tDuration: " << d << endl;
	usleep(d*1000000);
}

HeadHome &HeadHome::operator<<(Json::Value &json)
{
	return *this;
}

HeadPan::HeadPan(ActionSet *actionSet)
	: Action(actionSet)
{ }

void HeadPan::init()
{
	cout << "HeadHome::init(): " << endl;

	double d = 0.5;									// seconds
	Robot::Head::GetInstance()->MoveToHome();
	cout << "\tDuration: " << d << endl;
	usleep(d*1000000);
}

HeadPan &HeadPan::operator<<(Json::Value &json)
{
	this->angle = json["angle"].asDouble();
	return *this;
}
