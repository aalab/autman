/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 4, 2015.
 */

#include "../compilerdefinitions.h"

#include "Walking.h"

#include <cmath>
#include <iostream>
#include "walk.h"

using namespace std;
using namespace actions;
using namespace Robot;

#define MAX_STEP 15.0

Walk::Walk(actions::ActionSet *actionSet, double distance)
	: distance(distance), Action(actionSet)
{ }

void Walk::doProcess()
{
	VERBOSE("Walk::init(): " << this->distance << endl);

	double d = 10;

	Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	Walking::GetInstance()->X_MOVE_AMPLITUDE = d;
	/*
	 * In case of not walking forward properly
	 * Need to adjust the params.

	Walking::GetInstance()->Y_MOVE_AMPLITUDE = -7.0;
	Walking::GetInstance()->A_MOVE_AMPLITUDE = 7.0;
	*/
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = -4.0;
	Walking::GetInstance()->A_MOVE_AMPLITUDE = 4.0;

	double time = (((
		(this->distance/Walking::GetInstance()->X_MOVE_AMPLITUDE)
			* Walking::GetInstance()->PERIOD_TIME
	)/1000.0 * 2) / Walking::GetInstance()->PERIOD_TIME) * Walking::GetInstance()->PERIOD_TIME;
	VERBOSE(endl <<
				"\tDuration: " << d << endl <<
				"\tAt pace: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl
	);
	Walking::GetInstance()->Start();
	usleep(time * 1000000);
	Walking::GetInstance()->Stop();
}

Walk &Walk::operator<<(Json::Value &json)
{
	this->distance = json["distance"].asDouble();
	return *this;
}
