/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#ifndef _CONTROLMODULE_ROBOTCONTROLLER_H_
#define _CONTROLMODULE_ROBOTCONTROLLER_H_

#include <sys/time.h>
#include <jsoncpp/json/value.h>
#include "headcontroller.h"
#include "commands/command.h"
#include "actions/actionset.h"

namespace control
{
	class RobotState
	{
	public:
		double headPan;
		double headTilt;

		RobotState& operator<<(Json::Value &json);
		RobotState& operator>>(Json::Value &json);
	};

	enum Actions
	{
		STAND_UP_HIGH = 1,

		YES = 2,
		NO = 3,

		BOWDOWN = 4,

		GREETING = 6,

		KICK_RIGHT = 12,
		KICK_LEFT = 13,

		STAND_UP = 16,
		STAND_UP_FRONT = 18,
		STAND_UP_BACK = 11,
		SIT_DOWN = 15
	};

	class RobotController
		// : public MotionModule
	{
	public:
		double walkingSpeedRatio = 0;
		double walkingSpeedMax = 600;

		/*
		 * Max walking time cycle.
		 */
		double periodTimeMin = 600.0;
		double periodTimeMax = 600.0;

		double xMoveAmplitudeMin = 0.0;
		double xMoveAmplitudeMax = 15.0;
		double yMoveAmplitudeMin = 0.0;
		double yMoveAmplitudeMax = 25.0;
		double aMoveAmplitudeMin = 0;
		double aMoveAmplitudeMax = 20.0;

		// Camera angle velocity in seconds
		double headAngleSpeed = 20;
		double headAngleMaxStep = 10;

		double followLineAngle = 0.0;
		double followlineAngleSpeed = 30.0;
		double followLineAngleMax = 10.0;

		timeval priorTime;
		timeval currentTime;
		timeval uiPriorTime;

		HeadController head;
		commands::Command command;
		bool enabled;
		bool isRunning;

		__suseconds_t waitMilliseconds = 0;
	private:
		static RobotController* _instance;

	public:
		RobotController();
		~RobotController();

		static RobotController& getInstance();

		RobotState state;

		actions::ActionSet actionsSet;

		inline HeadController& getHead() { return this->head; };

		inline commands::Command& getCommand() { return this->command; }

		void startWalking();
		void stopWalking();

		void standUp();
		void release();

		void getUpFront();
		void getUpBack();

		void sitdown(bool blocking);

		void turn(double angle);

		void checkVersion();

		virtual void Initialize();
		virtual void Process();
		virtual void SetEnable(bool enable, bool exclusive);

		virtual bool IsRunning(); // override;

		void waitAction();

		int rlSum;
		int rlLast;
		int rlCount;

		void restartRotationRL();

		bool isWalking;
	};
}

#endif //_CONTROLMODULE_ROBOTCONTROLLER_H_
