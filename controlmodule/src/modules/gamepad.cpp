/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 28, 2015
 */

#include <Walking.h>
#include "gamepad.h"
#include "../robotcontroller.h"
#include "../compilerdefinitions.h"

using namespace control;

control::modules::GamepadController* control::modules::GamepadController::_instance = new modules::GamepadController();

modules::GamepadController::GamepadController()
	: enabled(false)
{ }

void control::modules::GamepadController::Process()
{
	if (enabled)
	{
		double
			tmp, tmp2, walkingSpeedRatio;
		RobotController &controller = RobotController::getInstance();

		gettimeofday(&this->currentTime, NULL);
		__suseconds_t diff = ((this->currentTime.tv_sec * 1000000 + this->currentTime.tv_usec) -
							  (this->priorTime.tv_sec * 1000000 + this->priorTime.tv_usec)) / 1000;
		__suseconds_t uiDiff = ((this->currentTime.tv_sec * 1000000 + this->currentTime.tv_usec) -
								(this->uiPriorTime.tv_sec * 1000000 + this->uiPriorTime.tv_usec)) / 1000;

		if (controller.getCommand().params == &controller.getCommand().keyboard)
		{
			double keySpeedRatio = 0.5;
			if (controller.getCommand().keyboard.button_shift)
				keySpeedRatio = 1;

			// Move forward or backwards
			if (controller.getCommand().keyboard.button_up)
			{
				controller.getCommand().gamepad.ljoystick.y = -1.0 * keySpeedRatio;
			}
			else if (controller.getCommand().keyboard.button_down)
			{
				controller.getCommand().gamepad.ljoystick.y = keySpeedRatio;
			}
			else
			{
				controller.getCommand().gamepad.ljoystick.y = 0;
			}

			// Strafing left or right
			if (controller.getCommand().keyboard.button_left)
			{
				controller.getCommand().gamepad.ljoystick.x = -1.0 * keySpeedRatio;
			}
			else if (controller.getCommand().keyboard.button_right)
			{
				controller.getCommand().gamepad.ljoystick.x = keySpeedRatio;
			}
			else
			{
				controller.getCommand().gamepad.ljoystick.x = 0;
			}

			// Turning head left or right
			if (controller.getCommand().keyboard.button_d)
			{
				controller.getCommand().gamepad.rjoystick.x = keySpeedRatio;
			}
			else if (controller.getCommand().keyboard.button_a)
			{
				controller.getCommand().gamepad.rjoystick.x = -1.0 * keySpeedRatio;
			}
			else
			{
				controller.getCommand().gamepad.rjoystick.x = 0;
			}

			// Turning head up or down
			if (controller.getCommand().keyboard.button_w)
			{
				controller.getCommand().gamepad.rjoystick.y = -1.0 * keySpeedRatio;
			}
			else if (controller.getCommand().keyboard.button_s)
			{
				controller.getCommand().gamepad.rjoystick.y = keySpeedRatio;
			}
			else
			{
				controller.getCommand().gamepad.rjoystick.y = 0;
			}

			// Move head to home
			if (controller.getCommand().keyboard.button_control)
			{
				controller.getCommand().gamepad.button_rjoystick = 1.0;
			}
			else
			{
				controller.getCommand().gamepad.button_rjoystick = 0.0;
			}
			controller.getCommand().params = &controller.getCommand().gamepad;

		}
		if (controller.getCommand().params == &controller.getCommand().gamepad)
		{
			walkingSpeedRatio = std::abs(controller.getCommand().gamepad.ljoystick.y);
			tmp = MINMAXRATIO(controller.periodTimeMin, controller.periodTimeMax, controller.walkingSpeedRatio);

			/*
			if (controller.getCommand().gamepad.button_y > 0)
			{
				this->isRunning = false;
				this->huh();
				return;
			}
			*/

#ifdef APPLY
			Walking::GetInstance()->PERIOD_TIME = tmp;
#endif
			if (walkingSpeedRatio == 0)
			{
				if ((controller.getCommand().gamepad.cross.x == 0) &&
					(controller.getCommand().gamepad.ljoystick.x == 0))
				{
#ifdef APPLY
					Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
					Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
					Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
					controller.stopWalking();
#endif
				}
				else if (controller.getCommand().gamepad.ljoystick.x != 0)
				{
#ifdef APPLY
					controller.startWalking();
#endif
					tmp = MINMAXRATIO(controller.aMoveAmplitudeMin, controller.aMoveAmplitudeMax,
									  controller.getCommand().gamepad.ljoystick.x);
#ifdef APPLY
					Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
					Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;

					Walking::GetInstance()->A_MOVE_AMPLITUDE = tmp;
#endif
				}
				else
				{
#ifdef APPLY
					controller.startWalking();
#endif
					tmp = MINMAXRATIO(controller.yMoveAmplitudeMin, controller.yMoveAmplitudeMax,
									  controller.getCommand().gamepad.cross.x);
#ifdef APPLY
					Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
					Walking::GetInstance()->Y_MOVE_AMPLITUDE = tmp;
#endif
				}
			}
			else
			{
				controller.startWalking();
				tmp = MINMAXRATIO(controller.xMoveAmplitudeMin, controller.xMoveAmplitudeMax,
								  -1 * controller.getCommand().gamepad.ljoystick.y);
				/*
				if (controller.getCommand().gamepad.ljoystick.y < 0)
				{
					// Walking::GetInstance()->HIP_PITCH_OFFSET = this->hipPitchOffsetMin;
					if (diff > 300)
					{
						cout << "BALANCE_HIP_PITCH_GAIN: " << Walking::GetInstance()->HIP_PITCH_OFFSET << endl;
						cout << "BALANCE_ENABLE: " << Walking::GetInstance()->BALANCE_ENABLE << endl;
					}
				}
				else
				{
					Walking::GetInstance()->HIP_PITCH_OFFSET = this->hipPitchOffsetMin;
					Walking::GetInstance()-> = this->hipPitchOffsetMin;
				}
				*/
#ifdef APPLY
				Walking::GetInstance()->X_MOVE_AMPLITUDE = tmp;
#endif

				tmp = MINMAXRATIO(controller.aMoveAmplitudeMin, controller.aMoveAmplitudeMax,
								  controller.getCommand().gamepad.ljoystick.x*-1);

#ifdef APPLY
				Walking::GetInstance()->A_MOVE_AMPLITUDE = tmp;
#endif
			}

			/*
			 * Head control
			 */
			if (controller.getCommand().gamepad.button_rjoystick > 0)
			{
				Head::GetInstance()->MoveToHome();
			}
			else
			{
				tmp2 = (controller.headAngleSpeed / 1000.0 * diff);

				tmp = std::max(-controller.headAngleSpeed, std::min(controller.headAngleMaxStep, tmp2 * controller.getCommand().gamepad.rjoystick.x));
				tmp2 = std::max(-controller.headAngleSpeed, std::min(controller.headAngleMaxStep, -tmp2 * controller.getCommand().gamepad.rjoystick.y));

				if (uiDiff > 300)
				{
					cout << " tilt: " << tmp2 << endl;
					cout << " Limits: [" << Head::GetInstance()->GetTopLimitAngle() << ", "
					<< Head::GetInstance()->GetLeftLimitAngle() << ", " << Head::GetInstance()->GetBottomLimitAngle()
					<< ", " << Head::GetInstance()->GetRightLimitAngle() << "]" << endl;
				}
#ifdef APPLY
				Head::GetInstance()->MoveByAngleOffset(tmp, tmp2);
#endif
			}
		}
		if (uiDiff > 300)
		{
			VERBOSE(endl
				<< "\t+ A: " << Walking::GetInstance()->A_MOVE_AMPLITUDE << endl
				<< "\t+ X: " << Walking::GetInstance()->X_MOVE_AMPLITUDE << endl
				<< "\t+ Y: " << Walking::GetInstance()->Y_MOVE_AMPLITUDE << endl
				<< endl
			);
			this->uiPriorTime = this->currentTime; // TODO Does it copies data!? If yes, it is okay.
		}
		this->priorTime = this->currentTime; // TODO Coes it copies data!? If yes, it is okay.
	}
}

modules::GamepadController *modules::GamepadController::getInstance()
{
	return _instance;
}

void modules::GamepadController::Initialize()
{ }
