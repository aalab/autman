/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 14, 2015
 */

#ifndef CONTROLMODULE_MODULES_HEAD_H
#define CONTROLMODULE_MODULES_HEAD_H

namespace control
{
	namespace modules
	{
		class Head
		{
		public:
			static void track(double x, double y, unsigned int width, unsigned int height, double distanceThreshold);
		};
	}
}


#endif //CONTROLMODULE_MODULES_HEAD_H
