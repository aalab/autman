/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 28, 2015
 */

#ifndef CONTROLMODULE_MODULES_GAMEPADCONTROLLER_H
#define CONTROLMODULE_MODULES_GAMEPADCONTROLLER_H

#include <sys/time.h>
#include <MotionModule.h>

namespace control
{
	namespace modules
	{
		class GamepadController:
			public Robot::MotionModule
		{
		private:
			static GamepadController* _instance;

			timeval priorTime;
			timeval currentTime;
			timeval uiPriorTime;
		public:
			GamepadController();

			virtual void Process();
			virtual void Initialize();

			static GamepadController* getInstance();

			bool enabled;
		};
	}
}


#endif //CONTROLMODULE_MODULES_GAMEPADCONTROLLER_H
