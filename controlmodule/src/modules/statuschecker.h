/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 28, 2015
 */

#ifndef CONTROLMODULE_STATUSCHECKER_H
#define CONTROLMODULE_STATUSCHECKER_H

#include <MotionModule.h>
#include <sys/time.h>

namespace control
{
	namespace modules
	{
		class StatusChecker
			: public Robot::MotionModule
		{
		private:
			static StatusChecker *_instance;

		public:
			StatusChecker();

			virtual void Initialize();
			virtual void Process();

			bool enabled;
			timeval currentTime;
			timeval fallTime;

			static StatusChecker* getInstance();
		};
	}
}


#endif //CONTROLMODULE_STATUSCHECKER_H
