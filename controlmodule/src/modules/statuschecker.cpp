/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 28, 2015
 */

#include "statuschecker.h"
#include "../robotcontroller.h"
#include "gamepad.h"

#include <MotionStatus.h>
#include <Action.h>

control::modules::StatusChecker* control::modules::StatusChecker::_instance = new modules::StatusChecker();

control::modules::StatusChecker *control::modules::StatusChecker::getInstance()
{
	return _instance;
}

control::modules::StatusChecker::StatusChecker()
	: enabled(false)
{ }

void control::modules::StatusChecker::Initialize()
{ }

void control::modules::StatusChecker::Process()
{
	if (this->enabled)
	{
		gettimeofday(&this->currentTime, NULL);
		__suseconds_t fallDiff = ((this->currentTime.tv_sec*1000000 + this->currentTime.tv_usec) - (this->fallTime.tv_sec*1000000 + this->fallTime.tv_usec)) / 1000;

		if (!Robot::Action::GetInstance()->IsRunning())
		{
			if (Robot::MotionStatus::FALLEN == FORWARD)
			{

				RobotController::getInstance().stopWalking();
				control::modules::GamepadController::getInstance()->enabled = false;
				if (fallDiff > 1000)
				{
					RobotController::getInstance().priorTime = this->currentTime;
					RobotController::getInstance().getUpFront();
				}
			}
			else if (MotionStatus::FALLEN == BACKWARD)
			{
				RobotController::getInstance().stopWalking();
				control::modules::GamepadController::getInstance()->enabled = false;
				if (fallDiff > 1000)
				{
					RobotController::getInstance().priorTime = this->currentTime;
					RobotController::getInstance().getUpBack();
				}
			}
			else
				this->fallTime = this->currentTime;
		}
		else
				this->fallTime = this->currentTime;
	}
}
