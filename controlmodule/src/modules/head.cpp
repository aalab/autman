/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 14, 2015
 */

#include <iostream>
#include <cmath>
#include <Head.h>
#include <Camera.h>
#include "head.h"

using namespace std;
using namespace control::modules;

void control::modules::Head::track(double x, double y, unsigned int width, unsigned int height, double distanceThreshold)
{
	double
		dX = (x - width/2.0),
		dY = (height/2.0 - y),
		pan = dX*Robot::Camera::VIEW_H_ANGLE/width,
		tilt = dY*Robot::Camera::VIEW_V_ANGLE/height;

/*
	cout << "modules::head::track" << endl
		<< "\tTO: (" << x << ":" << y << ")" << endl
		<< "\tAngle: (" << dX*Robot::Camera::VIEW_H_ANGLE/width << ":"
		<< dY*Robot::Camera::VIEW_V_ANGLE/height << ")" << endl;
*/

	pan /= 3 + std::abs(6 * (dX / width/2));
	tilt /= 3 + std::abs(6 * (dY / height/2));

	Robot::Head::GetInstance()->MoveByAngleOffset(pan, tilt);
}