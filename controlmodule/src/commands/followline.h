/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 6/12/15.
 */

#ifndef CONTROLMODULE_FOLLOWLINE_H
#define CONTROLMODULE_FOLLOWLINE_H


#include <Point.h>
#include "base.h"

namespace control
{
	namespace commands
	{
		class FollowLine
			: public CommandParams
		{
		public:
			Robot::Point2D topLine;
			Robot::Point2D middleLine;
			Robot::Point2D bottomLine;
			double radians;
			bool isLineFound;

			FollowLine & operator<<(const Json::Value& json);
		};
	}
}

#endif //CONTROLMODULE_FOLLOWLINE_H
