/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 18, 2015.
 */

#ifndef _CONTROLMODULE_COMMAND_H_
#define _CONTROLMODULE_COMMAND_H_

#include <string>
#include <jsoncpp/json/value.h>
#include <Point.h>
#include "../actions/actionset.h"


using namespace std;

namespace control
{
	namespace commands
	{
		class Axis
		{
		public:
			double x;
			double y;
		};

		class CommandParams
		{ };

		class FollowSprintMarker
			: public CommandParams
		{
		public:
			Robot::Point2D center;
			bool found;
			string mode;

			FollowSprintMarker & operator<<(const Json::Value& json);
		};

		Robot::Point2D& operator<<(Robot::Point2D &target, const Json::Value &json);

		Json::Value & operator<<(Json::Value &target, const Robot::Point2D point);
	}
}

#endif //_CONTROLMODULE_COMMAND_H_
