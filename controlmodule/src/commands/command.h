/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#ifndef CONTROLMODULE_COMMAND_H
#define CONTROLMODULE_COMMAND_H

#include <jsoncpp/json/value.h>
#include "base.h"
#include "queue.h"
#include "gamepad.h"
#include "keyboard.h"
#include "followline.h"
#include "headtrackermove.h"

namespace control
{
	namespace commands
	{
		class Command
		{
		public:
			Command(string name, CommandParams *params);
			Command();
			~Command();

			string name;
			CommandParams* params;

			Enqueue enqueue;
			ClearQueue clearQueue;
			FollowLine followline;
			GamePad gamepad;
			Keyboard keyboard;
			FollowSprintMarker followSprintMarker;
			HeadTrackerMove headTrackerMove;

			Command& operator<<(const Json::Value &json);
		};
	}
}


#endif //CONTROLMODULE_COMMAND_H
