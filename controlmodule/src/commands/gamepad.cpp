/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#include "gamepad.h"

using namespace control::commands;

GamePad &GamePad::operator<<(const Json::Value &json)
{
	this->button_a = json["a"].asDouble();
	this->button_b = json["b"].asDouble();
	this->button_x = json["x"].asDouble();
	this->button_y = json["y"].asDouble();
	this->button_lbumper = json["lbumper"].asDouble();
	this->button_rbumper = json["rbumper"].asDouble();
	this->button_back = json["back"].asDouble();
	this->button_start = json["start"].asDouble();
	this->button_xbox = json["xbox"].asDouble();
	this->button_ljoystick = json["ljoystick"].asDouble();
	this->button_rjoystick = json["rjoystick"].asDouble();

	this->ljoystick.x = json["ljoystick_x"].asDouble();
	this->ljoystick.y = json["ljoystick_y"].asDouble();

	this->rjoystick.x = json["rjoystick_x"].asDouble();
	this->rjoystick.y = json["rjoystick_y"].asDouble();

	this->cross.x = json["cross_x"].asDouble();
	this->cross.y = json["cross_y"].asDouble();

	this->ltrigger = json["ltrigger"].asDouble();
	this->rtrigger = json["rtrigger"].asDouble();

	return *this;
}

