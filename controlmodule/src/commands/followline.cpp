/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015.
 */

#include "followline.h"

using namespace control::commands;

FollowLine &control::commands::FollowLine::operator<<(const Json::Value &json)
{
	double lineTopX = json["line"]["top"]["x"].asDouble();
	double lineTopY = json["line"]["top"]["y"].asDouble();

	double lineMiddleX = json["line"]["middle"]["x"].asDouble();
	double lineMiddleY = json["line"]["middle"]["y"].asDouble();

	double lineBottomX = json["line"]["bottom"]["x"].asDouble();
	double lineBottomY = json["line"]["bottom"]["y"].asDouble();

	this->isLineFound = json["found"].asBool();

	this->topLine.X = lineTopX;
	this->topLine.Y = lineTopY;
	this->middleLine.X = lineMiddleX;
	this->middleLine.Y = lineMiddleY;
	this->bottomLine.X = lineBottomX;
	this->bottomLine.Y = lineBottomY;

	return *this;
}
