/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date ${DATE_MONTH}.
 */

#ifndef CONTROLMODULE_GAMEPAD_H
#define CONTROLMODULE_GAMEPAD_H

#include <jsoncpp/json/value.h>
#include "base.h"

namespace control
{
	namespace commands
	{
		class GamePad
			: public CommandParams
		{
		public:
			double button_a;
			double button_b;
			double button_x;
			double button_y;
			double button_lbumper;
			double button_rbumper;
			double button_back;
			double button_start;
			double button_xbox;
			double button_ljoystick;
			double button_rjoystick;

			Axis ljoystick;
			Axis rjoystick;
			Axis cross;
			double ltrigger;
			double rtrigger;

			GamePad &operator<<(const Json::Value &json);
		};
	}
}

#endif //CONTROLMODULE_GAMEPAD_H
