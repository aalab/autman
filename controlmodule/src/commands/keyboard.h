/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#ifndef CONTROLMODULE_KEYBOARD_H
#define CONTROLMODULE_KEYBOARD_H

#include "base.h"

namespace control
{
	namespace commands
	{
		class Keyboard
			: public CommandParams
		{
		public:
			bool button_a;
			bool button_d;
			bool button_s;
			bool button_w;
			bool button_up;
			bool button_left;
			bool button_right;
			bool button_down;
			bool button_alt;
			bool button_shift;
			bool button_control;

			Keyboard& operator<<(const Json::Value& json);
		};
	}
}

#endif //CONTROLMODULE_KEYBOARD_H
