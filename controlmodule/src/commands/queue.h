/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#ifndef CONTROLMODULE_QUEUE_H
#define CONTROLMODULE_QUEUE_H

#include "base.h"

namespace control
{
	namespace commands
	{
		class Enqueue
			: public CommandParams
		{
		public:
			Enqueue();
			std::vector<actions::Action*> actions;

			Enqueue & operator<<(const Json::Value & json);
		};

		class ClearQueue
			: public CommandParams
		{
		public:
			ClearQueue & operator<<(const Json::Value & json);
		};
	}
}

#endif //CONTROLMODULE_QUEUE_H
