/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015.
 */

#include "headtrackermove.h"

using namespace control::commands;

HeadTrackerMove &HeadTrackerMove::operator<<(const Json::Value &json)
{
	this->found = json["found"].asBool();
	if (this->found)
	{
		this->point.X = json["x"].asDouble();
		this->point.Y = json["y"].asDouble();
	}
	return *this;
}
