/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#include "command.h"

using namespace control::commands;

Command::Command(string name, CommandParams* params):
	name(name), params(params)
{ }

Command::Command()
	: params(NULL), enqueue()
{ }

Command::~Command()
{
	if (this->params != NULL)
		delete this->params;
}

Command& Command::operator<<(const Json::Value &json)
{
	this->name = json["name"].asString();

	if (this->name == "followline")
	{
		this->followline << json["params"];
		this->params = &this->followline;
	}
	else if (this->name == "gamepad")
	{
		this->gamepad << json["params"];
		this->params = &this->gamepad;
	}
	else if (this->name == "keyboard")
	{
		this->keyboard << json["params"];
		this->params = &this->keyboard;
	}
	else if (this->name == "enqueue")
	{
		this->enqueue << json["params"];
		this->params = &this->enqueue;
	}
	else if (this->name == "clearqueue")
	{
		this->clearQueue << json["params"];
		this->params = &this->clearQueue;
	}
	else if (this->name == "followsprintmarker")
	{
		this->followSprintMarker << json["params"];
		this->params = &this->followSprintMarker;
	}
	else if (this->name == "headtrackermove")
	{
		this->headTrackerMove << json["params"];
		this->params = &this->headTrackerMove;
	}
	else
		this->params = NULL;
	return *this;
}
