/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#include "queue.h"
#include "../actions/turn.h"
#include "../actions/walk.h"
#include "../globals.h"

using namespace control::commands;

Enqueue &Enqueue::operator<<(const Json::Value &json)
{
	cout << "Enqueue::operator<<" << endl;
	this->actions.clear();
	if (json.isArray())
	{
		Json::Value::const_iterator it;
		actions::Action* tmp;
		string name;
		for (it = json.begin(); it != json.end(); it++)
		{
			name = (*it)["name"].asString();
			cout << "\t+ " << name << endl;
			if (name == "turn")
				tmp = new actions::Turn(&Globals::getInstance()->getRobotController()->actionsSet, (*it)["angle"].asDouble());
			else if (name == "walk")
				tmp = new actions::Walk(&Globals::getInstance()->getRobotController()->actionsSet, (*it)["distance"].asDouble());
			else
				tmp = NULL;
			if (tmp != NULL)
				tmp->name = name;
		}
	}
	return *this;
}

Enqueue::Enqueue()
{ }

ClearQueue &ClearQueue::operator<<(Json::Value const &aConst)
{
	return *this;
}

