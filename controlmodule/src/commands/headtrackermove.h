/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015.
 */

#ifndef CONTROLMODULE_HEADTRACKING_H
#define CONTROLMODULE_HEADTRACKING_H

#include <Point.h>
#include <jsoncpp/json/value.h>
#include "base.h"

namespace control
{
	namespace commands
	{
		class HeadTrackerMove
			: public CommandParams
		{
		public:
			bool found;
			Robot::Point2D point;

			HeadTrackerMove & operator<<(const Json::Value& json);
		};
	}
}

#endif //CONTROLMODULE_HEADTRACKING_H
