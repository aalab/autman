/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#include "keyboard.h"

using namespace control::commands;

Keyboard &Keyboard::operator<<(const Json::Value &json)
{
	this->button_a = json["a"].asBool();
	this->button_d = json["d"].asBool();
	this->button_s = json["s"].asBool();
	this->button_w = json["w"].asBool();
	this->button_up = json["up"].asBool();
	this->button_left = json["left"].asBool();
	this->button_right = json["right"].asBool();
	this->button_down = json["down"].asBool();
	this->button_alt = json["alt"].asBool();
	this->button_shift = json["shift"].asBool();
	this->button_control = json["control"].asBool();

	return *this;
}
