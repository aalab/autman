/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 18, 2015.
 */

#include <iostream>
#include "base.h"
#include "../actions/turn.h"
#include "../actions/walk.h"
#include "../globals.h"

using namespace std;
using namespace control::commands;

Robot::Point2D& operator<<(Robot::Point2D &target, const Json::Value &json)
{
	target.X = json["x"].asDouble();
	target.Y = json["y"].asDouble();
	return target;
}

Json::Value & operator<<(Json::Value &target, const Robot::Point2D point)
{
	target["x"] = point.X;
	target["y"] = point.Y;
	return target;
}

FollowSprintMarker &control::commands::FollowSprintMarker::operator<<(const Json::Value &json)
{
	this->center.X = json["center"]["x"].asDouble();
	this->center.Y = json["center"]["y"].asDouble();

	this->mode = json["mode"].asString();

	this->found = json["found"].asBool();

	return *this;
}
