/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#include "headcontroller.h"
#include "globals.h"

using namespace control;
using namespace Robot;
using namespace std;

HeadController::HeadController()
{
	head = Head::GetInstance();
}

void HeadController::initialize()
{
	Globals::getInstance()->getMotionManager()->AddModule((MotionModule*)head);

	Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);

	// TODO: Check what this code does.
	Head::GetInstance()->m_Joint.SetPGain(JointData::ID_HEAD_PAN, 8);
	Head::GetInstance()->m_Joint.SetPGain(JointData::ID_HEAD_TILT, 8);
}

void HeadController::lookDown()
{
	cout << "HeadController::lookDown" << endl;
	cout << "HeadController::lookDown - Setting tilt to" << (this->head->GetTiltAngle() + STEP_ANGLE) << endl;
	this->head->MoveByAngle(this->head->GetPanAngle(), this->head->GetTiltAngle() + STEP_ANGLE);
	cout << "HeadController::lookDown - after" << endl;
}

void HeadController::lookUp()
{
	cout << "HeadController::lookUp" << endl;
	this->head->MoveByAngle(this->head->GetPanAngle(), this->head->GetTiltAngle() - STEP_ANGLE);
	cout << "HeadController::lookUp - after" << endl;
}
