/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#ifndef _CONTROLMODULE_GLOBALS_H_
#define _CONTROLMODULE_GLOBALS_H_

#define U2D_DEV_NAME        "/dev/ttyUSB0"

#include <MotionManager.h>
#include "robotcontroller.h"
#include "configuration.h"

namespace control
{
	class Globals
	{
	private:
		static Globals* instance;
	public:
		static Globals* getInstance();

	private:
		Robot::MotionManager* motionManager;

		RobotController* robotController;

		Globals();
	public:
		~Globals();

		Configuration configuration;

		inline Robot::MotionManager* getMotionManager() { return this->motionManager; }
		inline RobotController* getRobotController() { return this->robotController; }

		bool init();
	};
}

#endif //_CONTROLMODULE_GLOBALS_H_
