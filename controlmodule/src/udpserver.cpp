
#include "udpserver.h"
#include "compilerdefinitions.h"
#include "commands/base.h"
#include "globals.h"

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <iostream>
#include <string>
#include <glob.h>

#ifdef OP2
#include <Walking.h>
#else
#include <Walking.h>
#endif

using namespace boost::asio;
using namespace control;
using namespace std;
using namespace Robot;

UdpServer::UdpServer(unsigned int port)
	: ioService(), signals(ioService), socket(ioService, ip::udp::endpoint(ip::udp::v4(), port))//, nocommandTimer(ioService, boost::posix_time::seconds( 1 ) )
{
	this->signals.add(SIGINT);
	this->signals.add(SIGTERM);
#if defined(SIGQUIT)
	this->signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
}

void UdpServer::start()
{
	/*
	this->nocommandTimer.async_wait(boost::bind(
		&UdpServer::handleNoCommandTimer, this
	));
	*/

	cout << "Starting server" << endl;
	this->signals.async_wait(boost::bind(&UdpServer::stop, this));
	this->bindReceiveFrom();
	this->ioService.run();
}

int UdpServer::sendResponse(std::string const &msg)
{
	return this->socket.send_to(buffer(msg), this->remoteEP);
}

void UdpServer::bindReceiveFrom()
{
	this->socket.async_receive_from(
		boost::asio::buffer(this->data, UDPSERVER_BUFFER_SIZE),
		this->senderEndpoint,
		boost::bind(&UdpServer::handleReceiveFrom, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred
		)
	);
}

void UdpServer::handleReceiveFrom(const boost::system::error_code &error, size_t bytesReceived)
{
	if ((!error) && (bytesReceived > 0))
	{
		/*
		cout << "UdpServer::handleReceiveFrom" << endl;
		cout << "UdpServer::handleReceiveFrom (" << bytesReceived << ")" << this->data << endl;
		*/
		string message(this->data);
		/*
		cout << "UdpServer::handleReceiveFrom: SUCCESS " << message << endl;
		cout << flush;
		*/
		Json::Value json;
		if (this->jsonReader.parse(message, json))
		{
			// this->nocommandTimer.cancel();
			// this->renewCommandTimer();

			Globals::getInstance()->getRobotController()->getCommand() << json;

			// cout << json << endl;

			// cout << "Command: " << Globals::getInstance()->getRobotController()->getCommand().name << endl;

			if (Globals::getInstance()->getRobotController()->getCommand().params == NULL)
			{
				// cout << "UdpServer::handleReceiveFrom - Unknown command" << endl;
			}
			else if (Globals::getInstance()->getRobotController()->getCommand().name == "enqueue")
			{
				cout << "Enqueue command: " << endl;
				for (
					std::vector<actions::Action *>::iterator action = Globals::getInstance()->getRobotController()->getCommand().enqueue.actions.begin();
					action != Globals::getInstance()->getRobotController()->getCommand().enqueue.actions.end();
					++action)
				{
					cout << "\tEnqueuing: " << (*action)->name << endl;
					Globals::getInstance()->getRobotController()->actionsSet.push((*action));
				}
				Globals::getInstance()->getRobotController()->getCommand().params = NULL;
			}
			else if (Globals::getInstance()->getRobotController()->getCommand().name == "clearqueue")
			{
				Globals::getInstance()->getRobotController()->actionsSet.clear();
				Globals::getInstance()->getRobotController()->getCommand().params = NULL;
			}
			else if (Globals::getInstance()->getRobotController()->getCommand().name == "followline")
			{
				// cout << "Follow line" << endl;
				// cout << "Follow line: " << Globals::getInstance()->getRobotController()->getCommand().followline.bottomLine.X << " : " <<
				//	Globals::getInstance()->getRobotController()->getCommand().followline.bottomLine.Y << endl;
			}
			else if (Globals::getInstance()->getRobotController()->getCommand().name == "gamepad")
			{
				// cout << "UdpServer::handleReceiveFrom - " << Globals::getInstance()->getRobotController()->getCommand().gamepad.ljoystick.x << ":" << Globals::getInstance()->getRobotController()->getCommand().gamepad.ljoystick.y << endl;
			}
			else if (Globals::getInstance()->getRobotController()->getCommand().name == "keyboard")
			{
				// cout << "UdpServer::handleReceiveFrom - Keyboard section" << endl;
			}
			//
			Globals::getInstance()->getRobotController()->Process();
			//
			stringstream message;
			message << "{\"head\":{\"pan\":" << Globals::getInstance()->getRobotController()->state.headPan;
			message << ",\"tilt\":" << Globals::getInstance()->getRobotController()->state.headTilt;
			message << "}}";
			string response(message.str());
			this->send(response);
		}
		else
			cerr << "\tInvalid JSON." << endl;
	}
	// this->bindReceiveFrom();
}

void UdpServer::send(string& message)
{
	this->socket.async_send_to(
		boost::asio::buffer(message),
		this->senderEndpoint,
		boost::bind(
			&UdpServer::handleSendTo,
			this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred
		)
	);
}

void UdpServer::stop()
{
	cout << "Stopping the control server" << endl;
	this->ioService.stop();
}

void UdpServer::handleSendTo(const boost::system::error_code& error, size_t bytesSent)
{
	this->bindReceiveFrom();
}

void UdpServer::handleNoCommandTimer()
{
	/*
	cout << "UdpServer::handleNoCommandTimer" << endl;
	Globals::getInstance()->getRobotController()->getCommand().params = NULL;
	this->renewCommandTimer();
	this->nocommandTimer.async_wait(boost::bind(
		&UdpServer::handleNoCommandTimer, this
	));
	*/
}

void UdpServer::renewCommandTimer()
{
	// this->nocommandTimer.expires_from_now(boost::posix_time::seconds(1));
}
