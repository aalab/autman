/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 6, 2015.
 */

#ifndef _CONTROLMODULE_CONFIGURATION_H_
#define _CONTROLMODULE_CONFIGURATION_H_

#include <string.h>
#include <jsoncpp/json/value.h>

using namespace std;

class Configuration
{
public:
	string visionModuleHost;
	unsigned int visionModulePort;

	unsigned int udpServerPort;

	Configuration &operator<<(const Json::Value &json);

};

#endif //_CONTROLMODULE_CONFIGURATION_H_
