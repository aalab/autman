/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#ifndef _CONTROLMODULE_HEADCONTROLLER_H_
#define _CONTROLMODULE_HEADCONTROLLER_H_

#include <Head.h>
#include <cmath>

#define ANGLE(angle) angle*M_PI/180

#define RADTODEG(angle) angle*180/M_PI

// FIXME Known this values and if it should be radians or degrees
#define DOWN_ANGLE 		ANGLE(0)
#define UP_ANGLE 		ANGLE(0)

#define STEP_ANGLE 		ANGLE(5)

using namespace Robot;

namespace control
{
	class HeadController
	{
	private:
		Head *head;
	public:
		HeadController();

		/*
		 * Initialize the motors. Should be called internally by
		 */
		void initialize();

		void lookDown();

		void lookUp();
	};
}


#endif //_CONTROLMODULE_HEADCONTROLLER_H_
