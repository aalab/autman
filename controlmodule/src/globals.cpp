/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 19, 2015.
 */

#include "globals.h"
#include "compilerdefinitions.h"
#include "modules/gamepad.h"
#include "modules/statuschecker.h"
#include <CM730.h>
#include <LinuxCM730.h>
#include <assert.h>

#ifdef OP2
#include <LinuxDARwIn.h>
#include <libgen.h>
#else
#include <LinuxDARwIn.h>
#include <libgen.h>
#endif

using namespace control;
using namespace Robot;
using namespace std;

Globals* Globals::instance = NULL;

LinuxCM730 linux_cm730(U2D_DEV_NAME0);
CM730 cm730(&linux_cm730);

Globals* control::Globals::getInstance()
{
	if (instance == NULL)
		instance = new Globals();
	return instance;
}

control::Globals::Globals()
{ }

void change_current_dir()
{
	char exepath[1024] = {0};
	if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
	{
		if(chdir(dirname(exepath)))
			fprintf(stderr, "chdir error!! \n");
		else
			VERBOSE("Setting current dir to " << exepath << endl);
	}
}

bool control::Globals::init()
{
#ifdef OP2
	VERBOSE("Globals::init() : OP2 selected" << endl);
#else
	VERBOSE("Globals::init() : OP1 selected" << endl);
#endif

	change_current_dir();

	VERBOSE("Reading ini \"" << INI_FILE_PATH << "\"");
	minIni* ini = new minIni(INI_FILE_PATH);
	VERBOSE2(" OK." << endl);

	VERBOSE("Initializing MotionManager..." << endl);
	//loop until it connect
	long tryConnect = 1;
	//////////////////// Framework Initialize ////////////////////////////
	while(MotionManager::GetInstance()->Initialize(&cm730) == false)
	{
		cerr << endl << "[VERBOSE] Globals::init() : Setting port..." << endl;
		linux_cm730.SetPortName(U2D_DEV_NAME1);
		if(MotionManager::GetInstance()->Initialize(&cm730) == false)
		{
			cerr << "TELEOP: Fail to initialize Motion Manager!" << endl;
			return 0;
		}

		cerr << "TELEOP: Trying to connect to CM730 [" << tryConnect << "] ...";
		tryConnect++;
		sleep(1);
	}
	cerr << " OK." << endl;

	MotionManager::GetInstance()->AddModule(Action::GetInstance());
	MotionManager::GetInstance()->AddModule(modules::GamepadController::getInstance());

	MotionManager::GetInstance()->AddModule(modules::StatusChecker::getInstance());
	modules::StatusChecker::getInstance()->enabled = true;

	VERBOSE("Initializing motion timer...");
	LinuxMotionTimer *motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
	VERBOSE2(" Starting..." << endl);
	motion_timer->Start();
	VERBOSE2(" OK." << endl);

	int firm_ver = 0;
	if(cm730.ReadByte(JointData::ID_HEAD_PAN, MX28::P_VERSION, &firm_ver, 0)  != CM730::SUCCESS)
	{
		fprintf(stderr, "TELEOP: Can't read firmware version from Dynamixel ID %d!! \n\n", JointData::ID_HEAD_PAN);
		exit(0);
	}

	if(0 < firm_ver && firm_ver < 27)
	{
#ifdef MX28_1024
		Action::GetInstance()->LoadFile(MOTION_FILE_PATH);
#else
		fprintf(stderr, "TELEOP: MX-28's firmware is not support 4096 resolution!! \n");
		fprintf(stderr, "TELEOP: Upgrade MX-28's firmware to version 27(0x1B) or higher.\n\n");
		exit(0);
#endif
	}
	else if(27 <= firm_ver)
	{
#ifdef MX28_1024
		fprintf(stderr, "TELEOP: MX-28's firmware is not support 1024 resolution!! \n");
		fprintf(stderr, "TELEOP: Remove '#define MX28_1024' from 'MX28.h' file and rebuild.\n\n");
		exit(0);
#else
		VERBOSE("Loading '" << MOTION_FILE_PATH << "' as motion file...");
		if (!Action::GetInstance()->LoadFile((char*)MOTION_FILE_PATH))
		{
			cerr << "Can't load " << MOTION_FILE_PATH << endl;
			exit(0);
		}
		VERBOSE2("Ok" << endl);
#endif
	}
	else
	{
		cerr << "Firmware not supported" << endl;
		exit(0);
	}

	VERBOSE("Loading MotionModule configuration...");
	MotionManager::GetInstance()->LoadINISettings(ini);
	VERBOSE2(" OK." << endl);

	VERBOSE("Loading Head configuration...");
	Head::GetInstance()->LoadINISettings(ini);
	VERBOSE2(" OK." << endl);

#ifndef ENABLE_ONLY_HEAD
	VERBOSE("Loading Walking configuration...");
	Walking::GetInstance()->LoadINISettings(ini);
	VERBOSE2(" OK." << endl);

#ifdef APPLY
	VERBOSE("Enabling Action...");
	Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	MotionManager::GetInstance()->SetEnable(true);
	VERBOSE2(" OK." << endl);

	VERBOSE("Entering in a safe position...");
	Action::GetInstance()->Start(15);
	VERBOSE2(" Waiting end of action.");
	while(Action::GetInstance()->IsRunning())
		usleep(8*1000);
	VERBOSE2(" OK." << endl);

	VERBOSE("Standing...");
	Action::GetInstance()->Start(16);
	VERBOSE2(" Waiting end of action.");
	while(Action::GetInstance()->IsRunning())
		usleep(8*1000);
	VERBOSE2(" OK." << endl);
#endif
#else
	MotionStatus::m_CurrentJoints.SetEnableBodyWithoutHead(false);
	MotionManager::GetInstance()->SetEnable(true);
#endif

	Camera::WIDTH = 320;
	Camera::HEIGHT = 240;

	VERBOSE("Initializing RobotController...");
	this->robotController = new RobotController();
	VERBOSE2(" OK.");

	return true;
}

control::Globals::~Globals()
{
	VERBOSE("Releasing the motor...");
	Action::GetInstance()->m_Joint.SetEnableBody(false, false);
	MotionManager::GetInstance()->SetEnable(false);
	Head::GetInstance()->m_Joint.SetEnableHeadOnly(false, false);
	//All above resources have to be release to write byte to CM730
	// cm730.WriteByte(CM730::P_LED_EYE_L, 0xFF|0x02|0x04, NULL);
	for(int i=JointData::ID_R_SHOULDER_PITCH; i<JointData::NUMBER_OF_JOINTS; i++)
		cm730.WriteByte(i, MX28::P_TORQUE_ENABLE, 0, 0);
	usleep(50000);
	VERBOSE2(" OK." << endl);
	instance = NULL;
}
