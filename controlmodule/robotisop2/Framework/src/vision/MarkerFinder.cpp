/*
 * MarkerFinder.cpp
 *
 *  Created on: 2015. 4. 6.
 *      Author: Paul-Émile Crevier
 */

#include <stdlib.h>
#include <iostream>

#include "MarkerFinder.h"
// #include "ImgProcess.h"

using namespace Robot;

MarkerFinder::MarkerFinder() :
		ColorFinder(0, 61, 0, 100, 91, 100, 0.07, 30.0),
        // ColorFinder(0, 179, 0, 48, 0.07, 30.0),
		marker_section("")
{ }

MarkerFinder::MarkerFinder(int hue, int hue_tol, int min_sat, int max_sat, int min_val, int max_val, double min_per, double max_per) :
        ColorFinder(hue, hue_tol, min_sat, max_sat, min_val, max_val, min_per, max_per),
		marker_section("")
{ }

MarkerFinder::~MarkerFinder()
{
    // TODO Auto-generated destructor stub
}

void MarkerFinder::LoadINISettings(minIni* ini)
{
    LoadINISettings(ini, MARKER_SECTION);
}

void MarkerFinder::LoadINISettings(minIni* ini, const std::string &section)
{
    int value = -2;
    if((value = ini->geti(section, "hue", INVALID_VALUE)) != INVALID_VALUE)             m_hue = value;
    if((value = ini->geti(section, "hue_tolerance", INVALID_VALUE)) != INVALID_VALUE)   m_hue_tolerance = value;
    if((value = ini->geti(section, "min_saturation", INVALID_VALUE)) != INVALID_VALUE)  m_min_saturation = value;
	if((value = ini->geti(section, "max_saturation", INVALID_VALUE)) != INVALID_VALUE)  m_max_saturation = value;
    if((value = ini->geti(section, "min_value", INVALID_VALUE)) != INVALID_VALUE)       m_min_value = value;
	if((value = ini->geti(section, "max_value", INVALID_VALUE)) != INVALID_VALUE)       m_max_value = value;

    double dvalue = -2.0;
    if((dvalue = ini->getd(section, "min_percent", INVALID_VALUE)) != INVALID_VALUE)    m_min_percent = dvalue;
    if((dvalue = ini->getd(section, "max_percent", INVALID_VALUE)) != INVALID_VALUE)    m_max_percent = dvalue;

    marker_section = section;
}

void MarkerFinder::SaveINISettings(minIni* ini)
{
    SaveINISettings(ini, MARKER_SECTION);
}

void MarkerFinder::SaveINISettings(minIni* ini, const std::string &section)
{
	std::cout << "section = " << section << std::endl;
    ini->put(section,   "hue",              m_hue);
    ini->put(section,   "hue_tolerance",    m_hue_tolerance);
    ini->put(section,   "min_saturation",   m_min_saturation);
	ini->put(section,   "max_saturation",   m_max_saturation);
    ini->put(section,   "min_value",        m_min_value);
	ini->put(section,   "max_value",        m_max_value);
    ini->put(section,   "min_percent",      m_min_percent);
    ini->put(section,   "max_percent",      m_max_percent);

    marker_section = section;
}