/*
 *   ImgProcess.cpp
 *
 *   Author: ROBOTIS
 *
 */

#include <string.h>
#include <iostream>


#include "ImgProcess.h"
#include "Point.h"
#include "MarkerFinder.h"
#include "Camera.h"

using namespace Robot;

void ImgProcess::YUVtoRGB(FrameBuffer *buf)
{
    unsigned char *yuyv, *rgb;
    int z = 0;

    yuyv = buf->m_YUVFrame->m_ImageData;
    rgb = buf->m_RGBFrame->m_ImageData;

    for(int height = 0; height < buf->m_YUVFrame->m_Height; height++)
    {
        for(int width = 0; width < buf->m_YUVFrame->m_Width; width++)
        {
            int r, g, b;
            int y, u, v;

            if(!z)
                y = yuyv[0] << 8;
            else
                y = yuyv[2] << 8;
            u = yuyv[1] - 128;
            v = yuyv[3] - 128;

            r = (y + (359 * v)) >> 8;
            g = (y - (88 * u) - (183 * v)) >> 8;
            b = (y + (454 * u)) >> 8;

            *(rgb++) = (r > 255) ? 255 : ((r < 0) ? 0 : r);
            *(rgb++) = (g > 255) ? 255 : ((g < 0) ? 0 : g);
            *(rgb++) = (b > 255) ? 255 : ((b < 0) ? 0 : b);

            if (z++)
            {
                z = 0;
                yuyv += 4;
            }
        }
    }
}

void ImgProcess::RGBtoHSV(FrameBuffer *buf)
{
    int ir, ig, ib, imin, imax;
    int th, ts, tv, diffvmin;

    for(int i = 0; i < buf->m_RGBFrame->m_Width*buf->m_RGBFrame->m_Height; i++)
    {
        ir = buf->m_RGBFrame->m_ImageData[3*i+0];
        ig = buf->m_RGBFrame->m_ImageData[3*i+1];
        ib = buf->m_RGBFrame->m_ImageData[3*i+2];

        if( ir > ig )
        {
            imax = ir;
            imin = ig;
        }
        else
        {
            imax = ig;
            imin = ir;
        }

        if( imax > ib ) {
            if( imin > ib ) imin = ib;
        } else imax = ib;

        tv = imax;
        diffvmin = imax - imin;

        if( (tv!=0) && (diffvmin!=0) )
        {
            ts = (255* diffvmin) / imax;
            if( tv == ir ) th = (ig-ib)*60/diffvmin;
            else if( tv == ig ) th = 120 + (ib-ir)*60/diffvmin;
            else th = 240 + (ir-ig)*60/diffvmin;
            if( th < 0 ) th += 360;
            th &= 0x0000FFFF;
        }
        else
        {
            tv = 0;
            ts = 0;
            th = 0xFFFF;
        }

        ts = ts * 100 / 255;
        tv = tv * 100 / 255;

        //buf->m_HSVFrame->m_ImageData[i]= (unsigned int)th | ((unsigned int)ts<<16) | ((unsigned int)tv<<24);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+0] = (unsigned char)(th >> 8);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+1] = (unsigned char)(th & 0xFF);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+2] = (unsigned char)(ts & 0xFF);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+3] = (unsigned char)(tv & 0xFF);
    }
}

void ImgProcess::Erosion(Image* img)
{
    int x, y;

    unsigned char* temp_img = new unsigned char[img->m_Width*img->m_Height];
    memset(temp_img, 0, img->m_Width*img->m_Height);

    for( y=1; y<(img->m_Height-1); y++ )
    {
        for( x=1; x<(img->m_Width-1); x++ )
        {
            temp_img[y*img->m_Width+x]= img->m_ImageData[(y-1)*img->m_Width+(x-1)]
                                      & img->m_ImageData[(y-1)*img->m_Width+(x  )]
                                      & img->m_ImageData[(y-1)*img->m_Width+(x+1)]
                                      & img->m_ImageData[(y  )*img->m_Width+(x-1)]
                                      & img->m_ImageData[(y  )*img->m_Width+(x  )]
                                      & img->m_ImageData[(y  )*img->m_Width+(x+1)]
                                      & img->m_ImageData[(y+1)*img->m_Width+(x-1)]
                                      & img->m_ImageData[(y+1)*img->m_Width+(x  )]
                                      & img->m_ImageData[(y+1)*img->m_Width+(x+1)];
        }
    }

    memcpy(img->m_ImageData, temp_img, img->m_Width*img->m_Height);

    delete[] temp_img;
}

void ImgProcess::Erosion(Image* src, Image* dest)
{
    int x, y;

    for( y=1; y<(src->m_Height-1); y++ )
    {
        for( x=1; x<(src->m_Width-1); x++ )
        {
            dest->m_ImageData[y*src->m_Width+x]= src->m_ImageData[(y-1)*src->m_Width+(x-1)]
                                               & src->m_ImageData[(y-1)*src->m_Width+(x  )]
                                               & src->m_ImageData[(y-1)*src->m_Width+(x+1)]
                                               & src->m_ImageData[(y  )*src->m_Width+(x-1)]
                                               & src->m_ImageData[(y  )*src->m_Width+(x  )]
                                               & src->m_ImageData[(y  )*src->m_Width+(x+1)]
                                               & src->m_ImageData[(y+1)*src->m_Width+(x-1)]
                                               & src->m_ImageData[(y+1)*src->m_Width+(x  )]
                                               & src->m_ImageData[(y+1)*src->m_Width+(x+1)];
        }
    }
}

void ImgProcess::Dilation(Image* img)
{
    int x, y;

    unsigned char* temp_img = new unsigned char[img->m_Width*img->m_Height];
    memset(temp_img, 0, img->m_Width*img->m_Height);

    for( y=1; y<(img->m_Height-1); y++ )
    {
        for( x=1; x<(img->m_Width-1); x++ )
        {
            temp_img[y*img->m_Width+x]= img->m_ImageData[(y-1)*img->m_Width+(x-1)]
                                      | img->m_ImageData[(y-1)*img->m_Width+(x  )]
                                      | img->m_ImageData[(y-1)*img->m_Width+(x+1)]
                                      | img->m_ImageData[(y  )*img->m_Width+(x-1)]
                                      | img->m_ImageData[(y  )*img->m_Width+(x  )]
                                      | img->m_ImageData[(y  )*img->m_Width+(x+1)]
                                      | img->m_ImageData[(y+1)*img->m_Width+(x-1)]
                                      | img->m_ImageData[(y+1)*img->m_Width+(x  )]
                                      | img->m_ImageData[(y+1)*img->m_Width+(x+1)];
        }
    }

    memcpy(img->m_ImageData, temp_img, img->m_Width*img->m_Height);

    delete[] temp_img;
}

void ImgProcess::Dilation(Image* src, Image* dest)
{
    int x, y;

    for( y=1; y<(src->m_Height-1); y++ )
    {
        for( x=1; x<(src->m_Width-1); x++ )
        {
            dest->m_ImageData[y*src->m_Width+x]= src->m_ImageData[(y-1)*src->m_Width+(x-1)]
                                               | src->m_ImageData[(y-1)*src->m_Width+(x  )]
                                               | src->m_ImageData[(y-1)*src->m_Width+(x+1)]
                                               | src->m_ImageData[(y  )*src->m_Width+(x-1)]
                                               | src->m_ImageData[(y  )*src->m_Width+(x  )]
                                               | src->m_ImageData[(y  )*src->m_Width+(x+1)]
                                               | src->m_ImageData[(y+1)*src->m_Width+(x-1)]
                                               | src->m_ImageData[(y+1)*src->m_Width+(x  )]
                                               | src->m_ImageData[(y+1)*src->m_Width+(x+1)];

        }
    }
}

//assume passing in and out 3 byte image
void ImgProcess::AvgIntensity(Image* src, Image* dest)
{
    int x, y, ir, ig, ib, avg;

    for( y=0; y<(src->m_Height); y++ )
    {
        for( x=0; x<(src->m_WidthStep); x = x + 3 )
        {
			ir = src->m_ImageData[y*src->m_WidthStep+x];
			ig = src->m_ImageData[y*src->m_WidthStep+x+1];
			ib = src->m_ImageData[y*src->m_WidthStep+x+2];
			
			avg = ( ir + ig + ib ) / 3;
			
			dest->m_ImageData[y*src->m_WidthStep+x] = avg;
			dest->m_ImageData[y*src->m_WidthStep+x+1] = avg;
			dest->m_ImageData[y*src->m_WidthStep+x+2] = avg;
        }
    }
	
	std::cout << "\n//////////////////////////////////////////////////////" << std::endl;
	std::cout << "Average Intensity Values in a 4x4 Matrix" << std::endl;
	for( y=0; y<(src->m_Height-1); y++ )
	{
		if( y < 4 )
		{
			std::cout << "[ ";
			for( x=0; x<(src->m_WidthStep-1); x = x + 3 )
			{
				if( x < 12 )
					std::cout << (unsigned int)dest->m_ImageData[y*src->m_WidthStep+x] << " ";
			}
			std::cout << "]" << std::endl;
		}
	}
	std::cout << "//////////////////////////////////////////////////////" << std::endl;
}

void ImgProcess::IntegralImage(Image* src, unsigned int* dest)
{
	//calculate the integral image
	int x = 0;
	int y = 0;
	int ir = 0;
	int ig = 0;
	int ib = 0;
	
	//first row
	int sumr=0;
	int sumg=0;
	int sumb=0;
	
	int upperr = 0;
	int upperg = 0;
	int upperb = 0;
	
	int leftr = 0;
	int leftg = 0;
	int leftb = 0;
	
	int cornerr = 0;
	int cornerg = 0;
	int cornerb = 0;
	
	int newr = 0;
	int newg = 0;
	int newb = 0;
	
	// unsigned int integralBuffer[src->m_ImageSize];
	
	for( x=0; x<(src->m_WidthStep); x = x + 3 )
	{
		ir = src->m_ImageData[0*src->m_WidthStep+x];
		ig = src->m_ImageData[0*src->m_WidthStep+x+1];
		ib = src->m_ImageData[0*src->m_WidthStep+x+2];
		
		sumr += ir;
		sumg += ig;
		sumb += ib;
		dest[0*src->m_WidthStep+x] = sumr;
		dest[0*src->m_WidthStep+x+1] = sumg;
		dest[0*src->m_WidthStep+x+2] = sumb;
	}
	
	//the rest
	for( y=1; y<(src->m_Height); y++ )
	{
		// first column
		ir = src->m_ImageData[y*src->m_WidthStep];
		ig = src->m_ImageData[y*src->m_WidthStep+1];
		ib = src->m_ImageData[y*src->m_WidthStep+2];
		
		upperr = dest[(y-1)*src->m_WidthStep];
		upperg = dest[(y-1)*src->m_WidthStep+1];
		upperb = dest[(y-1)*src->m_WidthStep+2];
		
		newr = upperr + ir;
		newg = upperg + ig;
		newb = upperb + ib;
		
		dest[y*src->m_WidthStep] = newr;
		dest[y*src->m_WidthStep+1] = newg;
		dest[y*src->m_WidthStep+2] = newb;
		
		for( x=3; x<(src->m_WidthStep); x = x + 3 )
		{
			ir = src->m_ImageData[y*src->m_WidthStep+x];
			ig = src->m_ImageData[y*src->m_WidthStep+x+1];
			ib = src->m_ImageData[y*src->m_WidthStep+x+2];
			
			upperr = dest[(y-1)*src->m_WidthStep+x];
			upperg = dest[(y-1)*src->m_WidthStep+x+1];
			upperb = dest[(y-1)*src->m_WidthStep+x+2];
			
			leftr = dest[y*src->m_WidthStep+x-3];
			leftg = dest[y*src->m_WidthStep+x-2];
			leftb = dest[y*src->m_WidthStep+x-1];
			
			cornerr = dest[(y-1)*src->m_WidthStep+x-3];
			cornerg = dest[(y-1)*src->m_WidthStep+x-2];
			cornerb = dest[(y-1)*src->m_WidthStep+x-1];
			
			newr = ir + upperr + leftr - cornerr;
			newg = ig + upperg + leftg - cornerg;
			newb = ib + upperb + leftb - cornerb;
			
			dest[y*src->m_WidthStep+x] = newr;
			dest[y*src->m_WidthStep+x+1] = newg;
			dest[y*src->m_WidthStep+x+2] = newb;
		}
	}
	
	std::cout << "\n//////////////////////////////////////////////////////" << std::endl;
	std::cout << "Integral Image Values in a 4x4 Matrix" << std::endl;
	for( y=0; y<(src->m_Height-1); y++ )
	{
		if( y < 4 )
		{
			std::cout << "[ ";
			for( x=0; x<(src->m_WidthStep-1); x = x + 3 )
			{
				// only print 30 pixels horizontaly
				if( x < 12 )
					std::cout << dest[y*src->m_WidthStep+x] << " ";
			}
			std::cout << "]" << std::endl;
		}
	}
	std::cout << "//////////////////////////////////////////////////////" << std::endl;
	
	// // print out the integral image r values in matrix form
	// std::cout << "\n\n//////////////////////////////////////////////////////" << std::endl;
	// std::cout << "Original r Values in Matrix Form for 30 Pixels in the Horizontal" << std::endl;
	// for( y=0; y<(src->m_Height-1); y++ )
	// {
	// 	std::cout << "[ ";
	// 	for( x=0; x<(src->m_WidthStep-1); x = x + 3 )
	// 	{
	// 		// only print 30 pixels horizontaly
	// 		if( x > src->m_WidthStep - 90)//x < 90 )
	// 			std::cout << (unsigned int)src->m_ImageData[y*src->m_WidthStep+x] << " ";
	// 	}
	// 	std::cout << "]" << std::endl;
	// }
	// std::cout << "//////////////////////////////////////////////////////" << std::endl;
	
	// print out the integral image r values in matrix form
	// std::cout << "\n\n//////////////////////////////////////////////////////" << std::endl;
	// std::cout << "Integral Image r Values in Matrix Form for 30 Pixels in the Horizontal" << std::endl;
	// for( y=0; y<(src->m_Height-1); y++ )
	// {
	// 	std::cout << "[ ";
	// 	for( x=0; x<(src->m_WidthStep-1); x = x + 3 )
	// 	{
	// 		// only print 30 pixels horizontaly
	// 		if( x > src->m_WidthStep - 90)//x < 90 )
	// 			std::cout << dest[y*src->m_WidthStep+x] << " ";
	// 	}
	// 	std::cout << "]" << std::endl;
	// }
	// std::cout << "//////////////////////////////////////////////////////" << std::endl;
	
	// std::cout << "Width Step = " << src->m_WidthStep << std::endl;
}

void ImgProcess::ScaleIntegralImage(unsigned int* buffer, Image* dest, int widthStep, int width, int height)
{
	//calculate the integral image
	int x = 0;
	int y = 0;
	unsigned int ir = 0;
	unsigned int ig = 0;
	unsigned int ib = 0;
	
	unsigned int newr = 0;
	unsigned int newg = 0;
	unsigned int newb = 0;
	
	unsigned int scale = 0;

	
	// unsigned int* tempBuffer = new unsigned int[height*widthStep];
	
	// // First 4 pixels for the first 3 rows of the unscaled buffer
	// // row 1 r values
	// unsigned int unscaledrow1p1r = buffer[0*widthStep+0];
	// unsigned int unscaledrow1p2r = buffer[0*widthStep+3];
	// unsigned int unscaledrow1p3r = buffer[0*widthStep+6];
	// unsigned int unscaledrow1p4r = buffer[0*widthStep+9];
	
	// // row 2 r values
	// unsigned int unscaledrow2p1r = buffer[1*widthStep+0];
	// unsigned int unscaledrow2p2r = buffer[1*widthStep+3];
	// unsigned int unscaledrow2p3r = buffer[1*widthStep+6];
	// unsigned int unscaledrow2p4r = buffer[1*widthStep+9];
	
	// // row 3 r values
	// unsigned int unscaledrow3p1r = buffer[2*widthStep+0];
	// unsigned int unscaledrow3p2r = buffer[2*widthStep+3];
	// unsigned int unscaledrow3p3r = buffer[2*widthStep+6];
	// unsigned int unscaledrow3p4r = buffer[2*widthStep+9];
	
	// std::cout << "\n\nCalcIntegralImage First 4 Pixels From Destination" << std::endl;
	// std::cout << unscaledrow1p1r << "  " << unscaledrow1p2r << "  " << unscaledrow1p3r << "  " << unscaledrow1p4r << std::endl;
	// std::cout << unscaledrow2p1r << "  " << unscaledrow2p2r << "  " << unscaledrow2p3r << "  " << unscaledrow2p4r << std::endl;
	// std::cout << unscaledrow3p1r << "  " << unscaledrow3p2r << "  " << unscaledrow3p3r << "  " << unscaledrow3p4r << std::endl;
	
	
	// int count = 0;
	
	int r_width=0;
	int r_height=0;
	int max_index=0;

	// std::cout<<"TRACKER: last pixel "<< widthStep*height-1<<std::endl;
	scale = (buffer[widthStep*height-1]+1);
	for( y=0; y<dest->m_Height; y++ )
	{	
		r_width=0;
		for( x=0; x<dest->m_WidthStep; x = x + 3 )
		{
			ir = buffer[y*widthStep+x];
			ig = buffer[y*widthStep+x+1];
			ib = buffer[y*widthStep+x+2];
			
			
			
			newr = ( ir *256/ scale );
			newg = ( ig *256/ scale );
			newb = ( ib *256/ scale );
			
			
				dest->m_ImageData[y*widthStep+x] = newr;
				dest->m_ImageData[y*widthStep+x+1] = newg;
				dest->m_ImageData[y*widthStep+x+2] = newb;
				
			max_index +=3;
			r_width++;
		}
		r_height++;
	}	

	

	// std::cout<<"TRACKER: scale "<< scale<<"max accessing "<<max_index<<"width acess: "<<r_width<<"height access: "<<r_height<<"widthStep: "<<widthStep<<std::endl;


	// // First 4 pixels for the first 3 rows of the scaled buffer
	// // row 1 r values
	// unsigned int scaledrow1p1r = (unsigned int)dest->m_ImageData[0*widthStep+0];
	// unsigned int scaledrow1p2r = (unsigned int)dest->m_ImageData[0*widthStep+3];
	// unsigned int scaledrow1p3r = (unsigned int)dest->m_ImageData[0*widthStep+6];
	// unsigned int scaledrow1p4r = (unsigned int)dest->m_ImageData[0*widthStep+9];
	
	// // row 2 r values
	// unsigned int scaledrow2p1r = (unsigned int)dest->m_ImageData[1*widthStep+0];
	// unsigned int scaledrow2p2r = (unsigned int)dest->m_ImageData[1*widthStep+3];
	// unsigned int scaledrow2p3r = (unsigned int)dest->m_ImageData[1*widthStep+6];
	// unsigned int scaledrow2p4r = (unsigned int)dest->m_ImageData[1*widthStep+9];
	
	// // row 3 r values
	// unsigned int scaledrow3p1r = (unsigned int)dest->m_ImageData[2*widthStep+0];
	// unsigned int scaledrow3p2r = (unsigned int)dest->m_ImageData[2*widthStep+3];
	// unsigned int scaledrow3p3r = (unsigned int)dest->m_ImageData[2*widthStep+6];
	// unsigned int scaledrow3p4r = (unsigned int)dest->m_ImageData[2*widthStep+9];
	
	// std::cout << "\n\nCalcIntegralImage First 4 Pixels From Destination" << std::endl;
	// std::cout << scaledrow1p1r << "  " << scaledrow1p2r << "  " << scaledrow1p3r << "  " << scaledrow1p4r << std::endl;
	// std::cout << scaledrow2p1r << "  " << scaledrow2p2r << "  " << scaledrow2p3r << "  " << scaledrow2p4r << std::endl;
	// std::cout << scaledrow3p1r << "  " << scaledrow3p2r << "  " << scaledrow3p3r << "  " << scaledrow3p4r << std::endl;

	// delete(tempBuffer);
}

unsigned int ImgProcess::IntegralImageArea(unsigned int* integralBuffer,Image* src,Point2D tl, Point2D tr, Point2D ll, Point2D lr ){
	// unsigned int width = src->m_WidthStep;
	unsigned int width = 320*3;

	// unsigned int x=(unsigned int)tl.X;
	// unsigned int y=(unsigned int)tl.Y;
	// unsigned int tl_i;
	// std::cout<<"Area: print tl x "<<x<<" y "<<y<<std::endl;
	// // if(((y-1)*widthStep+(x-1))<0){
	// // 	tl_i=0;
	// // }
	// // else{
	// 	tl_i=integralBuffer[(y)*widthStep+(x)] ;
	// // }
	// // std::cout<<"RUN IntegralImageArea 1"<<std::endl;
	
	// x=(unsigned int)tr.X;
	// y=(unsigned int)tr.Y;
	// unsigned int tr_i;
	// std::cout<<"Area: print tr x "<<x<<"y "<<y<<std::endl;
	// // if((y-1)*widthStep+x<0){
	// 	// tr_i=0;
	// // }
	// // else{
	// 	tr_i=integralBuffer[(y)*widthStep+x] ;
	// // }
	
	// x=(unsigned int)ll.X;
	// y=(unsigned int)ll.Y;
	// unsigned int ll_i ;
	// std::cout<<"Area: print ll x "<<x<<"y "<<y<<std::endl;
	// // if(y*widthStep+(x-1)<0){
	// 	// ll_i=0;
	// // }
	// // else{
	// 	ll_i=integralBuffer[y*widthStep+(x)] ;
	// // }
	// x=(unsigned int)lr.X;
	// y=(unsigned int)lr.Y;
	// unsigned int lr_i ;
	// std::cout<<"Area: print lr x "<<x<<"y "<<y<<std::endl;
	// // if(y*widthStep+x<0){
	// 	lr_i=0;
	// // }
	// // else{
	// 	lr_i=integralBuffer[y*widthStep+x] ;
	// // }
	// int area = lr_i-ll_i-tr_i+tl_i;
	// unsigned int area_unsigned =0;
	// if(area<0){
	// 	std::cout<<"ERROR: Area<0: "<<area<<"  tl_i: "<<tl_i <<" tr_i: "<<tr_i <<" ll_i: "<<ll_i <<" lr_i: "<<lr_i<<std::endl;
		
	// }
	// else{
	// 	unsigned int area_unsigned = lr_i-ll_i-tr_i+tl_i;
	
	// 	std::cout<<"TRACKER: Area: "<< area<<" tl_i: "<<tl_i <<" tr_i: "<<tr_i <<" ll_i: "<<ll_i <<" lr_i: "<<lr_i<<std::endl; 

	// }

	
	// return area_unsigned;

  // unsigned int tlx = (unsigned int)tl.X*3;
  // unsigned int tly = (unsigned int)tl.Y;
  // unsigned int brx = (unsigned int)lr.X*3;
  // unsigned int bry = (unsigned int)lr.Y;


  //return overlap area
  unsigned int tlx = (unsigned int)tl.X*3;
  unsigned int tly = (unsigned int)tl.Y;
  unsigned int brx = (unsigned int)lr.X*3;
  unsigned int bry = (unsigned int)lr.Y;

  // unsigned int overlap_offset_x = (tlx-brx)/4;
  // if(tlx>overlap_offset_x>=0){
  // 	tlx = tlx - overlap_offset;
  // }
  // if(brx< overlap_offset_x<=0){
  // 	brx = brx + overlap_offset;
  // }


  // std::cout<<"Check area " <<(integralBuffer[ width * bry + brx ] - integralBuffer[ width * bry + tlx ] - integralBuffer[ width * tly + brx ] + integralBuffer[ width * tly + tlx ])<<std::endl;
  // std::cout << "Area of tl: " << integralBuffer[ width * tly + tlx ] << std::endl;
  // std::cout << "Area of tr: " << integralBuffer[ width * tly + brx ] << std::endl;
  // std::cout << "Area of bl: " << integralBuffer[ width * bry + tlx ] << std::endl;
  // std::cout << "Area of br: " << integralBuffer[ width * bry + brx ] << std::endl;
  
  
	// int x = 0;
	// int y = 0;
	// // print out the first 4 values in the integral image for the first 4 rows
	// std::cout << "\n//////////////////////////////////////////////////////" << std::endl;
	// std::cout << "Integral Image r Values in Matrix Form for 4 Pixels in the Horizontal" << std::endl;
	// for( y=0; y<(src->m_Height-1); y++ )
	// {
		// if( y == 0 || y == 63 )
		// {
			// std::cout << "[ ";
			// for( x=0; x<(src->m_WidthStep-1); x = x + 3 )
			// {
				// // only print a 4x4 matrix
				// if( x == 0 || x == 141 )
					// std::cout << integralBuffer[y*src->m_WidthStep+x] << " ";
			// }
			// std::cout << "]" << std::endl;
		// }
	// }
	// std::cout << "//////////////////////////////////////////////////////" << std::endl;
  
  // return (unsigned int)(integralBuffer[ width * bry + brx ] - integralBuffer[ width * bry + tlx ] - integralBuffer[ width * tly + brx ] + integralBuffer[ width * tly + tlx ]);
  //return overlap area
  return (unsigned int)(integralBuffer[ width * bry + brx ] - integralBuffer[ width * bry + tlx ] - integralBuffer[ width * tly + brx ] + integralBuffer[ width * tly + tlx ]);
  
}

//forward = 1, left =2, right =3
unsigned int ImgProcess::IdentifyMarker(Image* src,MarkerFinder* marker_finder, ColorFinder*  line_finder,Point2D marker_pos, Point2D  line_pos){
	unsigned int result =0;

	Image* temp_rgb_line = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);
	unsigned int* integralBuffer = new unsigned int[src->m_ImageSize];
	//filter image
	memcpy(temp_rgb_line->m_ImageData, src->m_ImageData, src->m_ImageSize);
	//greyscale image
	// ImgProcess::AvgIntensity(temp_rgb_line,temp_rgb_line);




	

	//greyscale image
	ImgProcess::AvgIntensity(temp_rgb_line,temp_rgb_line);

		// //filter marker 
	for(int i = 0; i < temp_rgb_line->m_NumberOfPixels; i++)
	{
		if(marker_finder->m_result->m_ImageData[i] == 1)
		{
			// temp_rgb_line->m_ImageData[i*src->m_PixelSize + 0] = 0;
			// temp_rgb_line->m_ImageData[i*src->m_PixelSize + 1] = 255;
			// temp_rgb_line->m_ImageData[i*src->m_PixelSize + 2] = 0;

			//find marker 
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 0] = 255;
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 1] = 255;
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 2] = 255;
		}
		else{
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 0] = 0;
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 1] = 0;
			temp_rgb_line->m_ImageData[i*src->m_PixelSize + 2] = 0;
			// src->m_ImageData[i*src->m_PixelSize + 0] = 0;
			// src->m_ImageData[i*src->m_PixelSize + 1] = 0;
			// src->m_ImageData[i*src->m_PixelSize + 2] = 0;
		}
	}

	// Point2D line_pos = line_finder
	//second filter reject lower half of the marker
	for (int m_row = 0;m_row<temp_rgb_line->m_Height ;m_row++){
		for (int m_col = 0;m_col<temp_rgb_line->m_WidthStep;m_col++ ){
			if( m_row< line_pos.Y){

			}
			else{
				temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 0]= 0;
				temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 1]= 0;
				temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 2]= 0;
			}
			
		}
		
		// std::cout<<std::endl;
	}
	//filter the first and last 2 col
	// for (int m_row = 0;m_row<temp_rgb_line->m_Height ;m_row++){
	// 	for (int m_col = 0;m_col<2*3;m_col++ ){
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 0]= 0;
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 1]= 0;
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 2]= 0;
			
	// 	}
		
	// 	// std::cout<<std::endl;
	// }
	// for (int m_row = 0;m_row<temp_rgb_line->m_Height ;m_row++){
	// 	for (int m_col = temp_rgb_line->m_WidthStep-6;m_col<temp_rgb_line->m_WidthStep;m_col++ ){
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 0]= 0;
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 1]= 0;
	// 			temp_rgb_line->m_ImageData[m_row*temp_rgb_line->m_WidthStep+ m_col+ 2]= 0;
			
	// 	}
		
	// 	// std::cout<<std::endl;
	// }
	



	// //integral image
	// // ImgProcess::IntegralImage(src,integralBuffer);
	ImgProcess::IntegralImage(temp_rgb_line,integralBuffer);

	// //check integral image
	// // ImgProcess::ScaleIntegralImage(integralBuffer, temp_rgb_line, temp_rgb_line->m_WidthStep, temp_rgb_line->m_Width, temp_rgb_line->m_Height);
	
	// //calulate and locate Marker
	// //q_row_col
	unsigned int q_width = src->m_Width;
	unsigned int q_height = src->m_Height;
	Point2D q0000 = Point2D(0*q_width/13,0*q_height/13);
	Point2D q0001 = Point2D(1*q_width/13,0*q_height/13);
	Point2D q0002 = Point2D(2*q_width/13,0*q_height/13);
	Point2D q0003 = Point2D(3*q_width/13,0*q_height/13);
	Point2D q0004 = Point2D(4*q_width/13,0*q_height/13);
	Point2D q0005 = Point2D(5*q_width/13,0*q_height/13);
	Point2D q0006 = Point2D(6*q_width/13,0*q_height/13);
	Point2D q0007 = Point2D(7*q_width/13,0*q_height/13);
	Point2D q0008 = Point2D(8*q_width/13,0*q_height/13);
	Point2D q0009 = Point2D(9*q_width/13,0*q_height/13);
	Point2D q0010 = Point2D(10*q_width/13,0*q_height/13);
	Point2D q0011 = Point2D(11*q_width/13,0*q_height/13);
	Point2D q0012 = Point2D(12*q_width/13,0*q_height/13);
	Point2D q0013 = Point2D(13*q_width/13-1,0*q_height/13);


	// std::cout << "IDEN: Point q00 - (x,y) = (" << (unsigned int)q00.X << ", " << (unsigned int)q00.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q01 - (x,y) = (" << (unsigned int)q01.X << ", " << (unsigned int)q01.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q02 - (x,y) = (" << (unsigned int)q02.X << ", " << (unsigned int)q02.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q03 - (x,y) = (" << (unsigned int)q03.X << ", " << (unsigned int)q03.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q04 - (x,y) = (" << (unsigned int)q04.X << ", " << (unsigned int)q04.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q05 - (x,y) = (" << (unsigned int)q05.X << ", " << (unsigned int)q05.Y << ")" << std::endl;
	
	// Point2D q10 = Point2D(0*q_width/5,1*q_height/5-1);
	// Point2D q11 = Point2D(1*q_width/5,1*q_height/5-1);
	// Point2D q12 = Point2D(2*q_width/5,1*q_height/5-1);
	// Point2D q13 = Point2D(3*q_width/5,1*q_height/5-1);
	// Point2D q14 = Point2D(4*q_width/5,1*q_height/5-1);
	// Point2D q15 = Point2D(5*q_width/5-1,1*q_height/5-1);
	Point2D q0100 = Point2D(0*q_width/13,1*q_height/13-1);
	Point2D q0101 = Point2D(1*q_width/13,1*q_height/13-1);
	Point2D q0102 = Point2D(2*q_width/13,1*q_height/13-1);
	Point2D q0103 = Point2D(3*q_width/13,1*q_height/13-1);
	Point2D q0104 = Point2D(4*q_width/13,1*q_height/13-1);
	Point2D q0105 = Point2D(5*q_width/13,1*q_height/13-1);
	Point2D q0106 = Point2D(6*q_width/13,1*q_height/13-1);
	Point2D q0107 = Point2D(7*q_width/13,1*q_height/13-1);
	Point2D q0108 = Point2D(8*q_width/13,1*q_height/13-1);
	Point2D q0109 = Point2D(9*q_width/13,1*q_height/13-1);
	Point2D q0110 = Point2D(10*q_width/13,1*q_height/13-1);
	Point2D q0111 = Point2D(11*q_width/13,1*q_height/13-1);
	Point2D q0112 = Point2D(12*q_width/13,1*q_height/13-1);
	Point2D q0113 = Point2D(13*q_width/13-1,1*q_height/13-1);

	// std::cout << "IDEN: Point q10 - (x,y) = (" << (unsigned int)q10.X << ", " << (unsigned int)q10.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q11 - (x,y) = (" << (unsigned int)q11.X << ", " << (unsigned int)q11.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q12 - (x,y) = (" << (unsigned int)q12.X << ", " << (unsigned int)q12.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q13 - (x,y) = (" << (unsigned int)q13.X << ", " << (unsigned int)q13.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q14 - (x,y) = (" << (unsigned int)q14.X << ", " << (unsigned int)q14.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q15 - (x,y) = (" << (unsigned int)q15.X << ", " << (unsigned int)q15.Y << ")" << std::endl;
	
	// Point2D q20 = Point2D(0*q_width/5,2*q_height/5-1);
	// Point2D q21 = Point2D(1*q_width/5,2*q_height/5-1);
	// Point2D q22 = Point2D(2*q_width/5,2*q_height/5-1);
	// Point2D q23 = Point2D(3*q_width/5,2*q_height/5-1);
	// Point2D q24 = Point2D(4*q_width/5,2*q_height/5-1);
	// Point2D q25 = Point2D(5*q_width/5-1,2*q_height/5-1);

	Point2D q0200 = Point2D(0*q_width/13,2*q_height/13-1);
	Point2D q0201 = Point2D(1*q_width/13,2*q_height/13-1);
	Point2D q0202 = Point2D(2*q_width/13,2*q_height/13-1);
	Point2D q0203 = Point2D(3*q_width/13,2*q_height/13-1);
	Point2D q0204 = Point2D(4*q_width/13,2*q_height/13-1);
	Point2D q0205 = Point2D(5*q_width/13,2*q_height/13-1);
	Point2D q0206 = Point2D(6*q_width/13,2*q_height/13-1);
	Point2D q0207 = Point2D(7*q_width/13,2*q_height/13-1);
	Point2D q0208 = Point2D(8*q_width/13,2*q_height/13-1);
	Point2D q0209 = Point2D(9*q_width/13,2*q_height/13-1);
	Point2D q0210 = Point2D(10*q_width/13,2*q_height/13-1);
	Point2D q0211 = Point2D(11*q_width/13,2*q_height/13-1);
	Point2D q0212 = Point2D(12*q_width/13,2*q_height/13-1);
	Point2D q0213 = Point2D(13*q_width/13-1,2*q_height/13-1);

	// std::cout << "IDEN: Point q20 - (x,y) = (" << (unsigned int)q20.X << ", " << (unsigned int)q20.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q21 - (x,y) = (" << (unsigned int)q21.X << ", " << (unsigned int)q21.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q22 - (x,y) = (" << (unsigned int)q22.X << ", " << (unsigned int)q22.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q23 - (x,y) = (" << (unsigned int)q23.X << ", " << (unsigned int)q23.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q24 - (x,y) = (" << (unsigned int)q24.X << ", " << (unsigned int)q24.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q25 - (x,y) = (" << (unsigned int)q25.X << ", " << (unsigned int)q25.Y << ")" << std::endl;
	


	// Point2D q30 = Point2D(0*q_width/5,3*q_height/5-1);
	// Point2D q31 = Point2D(1*q_width/5,3*q_height/5-1);
	// Point2D q32 = Point2D(2*q_width/5,3*q_height/5-1);
	// Point2D q33 = Point2D(3*q_width/5,3*q_height/5-1);
	// Point2D q34 = Point2D(4*q_width/5,3*q_height/5-1);
	// Point2D q35 = Point2D(5*q_width/5-1,3*q_height/5-1);
	Point2D q0300 = Point2D(0*q_width/13,3*q_height/13-1);
	Point2D q0301 = Point2D(1*q_width/13,3*q_height/13-1);
	Point2D q0302 = Point2D(2*q_width/13,3*q_height/13-1);
	Point2D q0303 = Point2D(3*q_width/13,3*q_height/13-1);
	Point2D q0304 = Point2D(4*q_width/13,3*q_height/13-1);
	Point2D q0305 = Point2D(5*q_width/13,3*q_height/13-1);
	Point2D q0306 = Point2D(6*q_width/13,3*q_height/13-1);
	Point2D q0307 = Point2D(7*q_width/13,3*q_height/13-1);
	Point2D q0308 = Point2D(8*q_width/13,3*q_height/13-1);
	Point2D q0309 = Point2D(9*q_width/13,3*q_height/13-1);
	Point2D q0310 = Point2D(10*q_width/13,3*q_height/13-1);
	Point2D q0311 = Point2D(11*q_width/13,3*q_height/13-1);
	Point2D q0312 = Point2D(12*q_width/13,3*q_height/13-1);
	Point2D q0313 = Point2D(13*q_width/13-1,3*q_height/13-1);


	// std::cout << "IDEN: Point q30 - (x,y) = (" << (unsigned int)q30.X << ", " << (unsigned int)q30.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q31 - (x,y) = (" << (unsigned int)q31.X << ", " << (unsigned int)q31.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q32 - (x,y) = (" << (unsigned int)q32.X << ", " << (unsigned int)q32.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q33 - (x,y) = (" << (unsigned int)q33.X << ", " << (unsigned int)q33.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q34 - (x,y) = (" << (unsigned int)q34.X << ", " << (unsigned int)q34.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q35 - (x,y) = (" << (unsigned int)q35.X << ", " << (unsigned int)q35.Y << ")" << std::endl;
	

	// Point2D q40 = Point2D(0*q_width/5,4*q_height/5-1);
	// Point2D q41 = Point2D(1*q_width/5,4*q_height/5-1);
	// Point2D q42 = Point2D(2*q_width/5,4*q_height/5-1);
	// Point2D q43 = Point2D(3*q_width/5,4*q_height/5-1);
	// Point2D q44 = Point2D(4*q_width/5,4*q_height/5-1);
	// Point2D q45 = Point2D(5*q_width/5-1,4*q_height/5-1);

	Point2D q0400 = Point2D(0*q_width/13,4*q_height/13-1);
	Point2D q0401 = Point2D(1*q_width/13,4*q_height/13-1);
	Point2D q0402 = Point2D(2*q_width/13,4*q_height/13-1);
	Point2D q0403 = Point2D(3*q_width/13,4*q_height/13-1);
	Point2D q0404 = Point2D(4*q_width/13,4*q_height/13-1);
	Point2D q0405 = Point2D(5*q_width/13,4*q_height/13-1);
	Point2D q0406 = Point2D(6*q_width/13,4*q_height/13-1);
	Point2D q0407 = Point2D(7*q_width/13,4*q_height/13-1);
	Point2D q0408 = Point2D(8*q_width/13,4*q_height/13-1);
	Point2D q0409 = Point2D(9*q_width/13,4*q_height/13-1);
	Point2D q0410 = Point2D(10*q_width/13,4*q_height/13-1);
	Point2D q0411 = Point2D(11*q_width/13,4*q_height/13-1);
	Point2D q0412 = Point2D(12*q_width/13,4*q_height/13-1);
	Point2D q0413 = Point2D(13*q_width/13-1,4*q_height/13-1);
	// std::cout << "IDEN: Point q40 - (x,y) = (" << (unsigned int)q40.X << ", " << (unsigned int)q40.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q41 - (x,y) = (" << (unsigned int)q41.X << ", " << (unsigned int)q41.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q42 - (x,y) = (" << (unsigned int)q42.X << ", " << (unsigned int)q42.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q43 - (x,y) = (" << (unsigned int)q43.X << ", " << (unsigned int)q43.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q44 - (x,y) = (" << (unsigned int)q44.X << ", " << (unsigned int)q44.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q45 - (x,y) = (" << (unsigned int)q45.X << ", " << (unsigned int)q45.Y << ")" << std::endl;
	
	// Point2D q50 = Point2D(0*q_width/5,5*q_height/5-1);
	// Point2D q51 = Point2D(1*q_width/5,5*q_height/5-1);
	// Point2D q52 = Point2D(2*q_width/5,5*q_height/5-1);
	// Point2D q53 = Point2D(3*q_width/5,5*q_height/5-1);
	// Point2D q54 = Point2D(4*q_width/5,5*q_height/5-1);
	// Point2D q55 = Point2D(5*q_width/5-1,5*q_height/5-1);
	Point2D q0500 = Point2D(0*q_width/13,5*q_height/13-1);
	Point2D q0501 = Point2D(1*q_width/13,5*q_height/13-1);
	Point2D q0502 = Point2D(2*q_width/13,5*q_height/13-1);
	Point2D q0503 = Point2D(3*q_width/13,5*q_height/13-1);
	Point2D q0504 = Point2D(4*q_width/13,5*q_height/13-1);
	Point2D q0505 = Point2D(5*q_width/13,5*q_height/13-1);
	Point2D q0506 = Point2D(6*q_width/13,5*q_height/13-1);
	Point2D q0507 = Point2D(7*q_width/13,5*q_height/13-1);
	Point2D q0508 = Point2D(8*q_width/13,5*q_height/13-1);
	Point2D q0509 = Point2D(9*q_width/13,5*q_height/13-1);
	Point2D q0510 = Point2D(10*q_width/13,5*q_height/13-1);
	Point2D q0511 = Point2D(11*q_width/13,5*q_height/13-1);
	Point2D q0512 = Point2D(12*q_width/13,5*q_height/13-1);
	Point2D q0513 = Point2D(13*q_width/13-1,5*q_height/13-1);
	// std::cout << "IDEN: Point q50 - (x,y) = (" << (unsigned int)q50.X << ", " << (unsigned int)q50.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q51 - (x,y) = (" << (unsigned int)q51.X << ", " << (unsigned int)q51.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q52 - (x,y) = (" << (unsigned int)q52.X << ", " << (unsigned int)q52.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q53 - (x,y) = (" << (unsigned int)q53.X << ", " << (unsigned int)q53.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q54 - (x,y) = (" << (unsigned int)q54.X << ", " << (unsigned int)q54.Y << ")" << std::endl;
	// std::cout << "IDEN: Point q55 - (x,y) = (" << (unsigned int)q55.X << ", " << (unsigned int)q55.Y << ")" << std::endl;
	Point2D q0600 = Point2D(0*q_width/13,6*q_height/13-1);
	Point2D q0601 = Point2D(1*q_width/13,6*q_height/13-1);
	Point2D q0602 = Point2D(2*q_width/13,6*q_height/13-1);
	Point2D q0603 = Point2D(3*q_width/13,6*q_height/13-1);
	Point2D q0604 = Point2D(4*q_width/13,6*q_height/13-1);
	Point2D q0605 = Point2D(5*q_width/13,6*q_height/13-1);
	Point2D q0606 = Point2D(6*q_width/13,6*q_height/13-1);
	Point2D q0607 = Point2D(7*q_width/13,6*q_height/13-1);
	Point2D q0608 = Point2D(8*q_width/13,6*q_height/13-1);
	Point2D q0609 = Point2D(9*q_width/13,6*q_height/13-1);
	Point2D q0610 = Point2D(10*q_width/13,6*q_height/13-1);
	Point2D q0611 = Point2D(11*q_width/13,6*q_height/13-1);
	Point2D q0612 = Point2D(12*q_width/13,6*q_height/13-1);
	Point2D q0613 = Point2D(13*q_width/13-1,6*q_height/13-1);

	Point2D q0700 = Point2D(0*q_width/13,7*q_height/13-1);
	Point2D q0701 = Point2D(1*q_width/13,7*q_height/13-1);
	Point2D q0702 = Point2D(2*q_width/13,7*q_height/13-1);
	Point2D q0703 = Point2D(3*q_width/13,7*q_height/13-1);
	Point2D q0704 = Point2D(4*q_width/13,7*q_height/13-1);
	Point2D q0705 = Point2D(5*q_width/13,7*q_height/13-1);
	Point2D q0706 = Point2D(6*q_width/13,7*q_height/13-1);
	Point2D q0707 = Point2D(7*q_width/13,7*q_height/13-1);
	Point2D q0708 = Point2D(8*q_width/13,7*q_height/13-1);
	Point2D q0709 = Point2D(9*q_width/13,7*q_height/13-1);
	Point2D q0710 = Point2D(10*q_width/13,7*q_height/13-1);
	Point2D q0711 = Point2D(11*q_width/13,7*q_height/13-1);
	Point2D q0712 = Point2D(12*q_width/13,7*q_height/13-1);
	Point2D q0713 = Point2D(13*q_width/13-1,7*q_height/13-1);

	Point2D q0800 =    Point2D(0*q_width/13,8*q_height/13-1);
	Point2D q0801 =    Point2D(1*q_width/13,8*q_height/13-1);
	Point2D q0802 =    Point2D(2*q_width/13,8*q_height/13-1);
	Point2D q0803 =    Point2D(3*q_width/13,8*q_height/13-1);
	Point2D q0804 =    Point2D(4*q_width/13,8*q_height/13-1);
	Point2D q0805 =    Point2D(5*q_width/13,8*q_height/13-1);
	Point2D q0806 =    Point2D(6*q_width/13,8*q_height/13-1);
	Point2D q0807 =    Point2D(7*q_width/13,8*q_height/13-1);
	Point2D q0808 =    Point2D(8*q_width/13,8*q_height/13-1);
	Point2D q0809 =    Point2D(9*q_width/13,8*q_height/13-1);
	Point2D q0810 =  Point2D(10*q_width/13,8*q_height/13-1);
	Point2D q0811 =  Point2D(11*q_width/13,8*q_height/13-1);
	Point2D q0812 =  Point2D(12*q_width/13,8*q_height/13-1);
	Point2D q0813 =Point2D(13*q_width/13-1,8*q_height/13-1);

	Point2D q0900 =    Point2D(0*q_width/13,9*q_height/13-1);
	Point2D q0901 =    Point2D(1*q_width/13,9*q_height/13-1);
	Point2D q0902 =    Point2D(2*q_width/13,9*q_height/13-1);
	Point2D q0903 =    Point2D(3*q_width/13,9*q_height/13-1);
	Point2D q0904 =    Point2D(4*q_width/13,9*q_height/13-1);
	Point2D q0905 =    Point2D(5*q_width/13,9*q_height/13-1);
	Point2D q0906 =    Point2D(6*q_width/13,9*q_height/13-1);
	Point2D q0907 =    Point2D(7*q_width/13,9*q_height/13-1);
	Point2D q0908 =    Point2D(8*q_width/13,9*q_height/13-1);
	Point2D q0909 =    Point2D(9*q_width/13,9*q_height/13-1);
	Point2D q0910 =  Point2D(10*q_width/13,9*q_height/13-1);
	Point2D q0911 =  Point2D(11*q_width/13,9*q_height/13-1);
	Point2D q0912 =  Point2D(12*q_width/13,9*q_height/13-1);
	Point2D q0913 =Point2D(13*q_width/13-1,9*q_height/13-1);

	Point2D q1000 =    Point2D(0*q_width/13,10*q_height/13-1);
	Point2D q1001 =    Point2D(1*q_width/13,10*q_height/13-1);
	Point2D q1002 =    Point2D(2*q_width/13,10*q_height/13-1);
	Point2D q1003 =    Point2D(3*q_width/13,10*q_height/13-1);
	Point2D q1004 =    Point2D(4*q_width/13,10*q_height/13-1);
	Point2D q1005 =    Point2D(5*q_width/13,10*q_height/13-1);
	Point2D q1006 =    Point2D(6*q_width/13,10*q_height/13-1);
	Point2D q1007 =    Point2D(7*q_width/13,10*q_height/13-1);
	Point2D q1008 =    Point2D(8*q_width/13,10*q_height/13-1);
	Point2D q1009 =    Point2D(9*q_width/13,10*q_height/13-1);
	Point2D q1010 =  Point2D(10*q_width/13,10*q_height/13-1);
	Point2D q1011 =  Point2D(11*q_width/13,10*q_height/13-1);
	Point2D q1012 =  Point2D(12*q_width/13,10*q_height/13-1);
	Point2D q1013 =Point2D(13*q_width/13-1,10*q_height/13-1);

	Point2D q1100 =    Point2D(0*q_width/13,11*q_height/13-1);
	Point2D q1101 =    Point2D(1*q_width/13,11*q_height/13-1);
	Point2D q1102 =    Point2D(2*q_width/13,11*q_height/13-1);
	Point2D q1103 =    Point2D(3*q_width/13,11*q_height/13-1);
	Point2D q1104 =    Point2D(4*q_width/13,11*q_height/13-1);
	Point2D q1105 =    Point2D(5*q_width/13,11*q_height/13-1);
	Point2D q1106 =    Point2D(6*q_width/13,11*q_height/13-1);
	Point2D q1107 =    Point2D(7*q_width/13,11*q_height/13-1);
	Point2D q1108 =    Point2D(8*q_width/13,11*q_height/13-1);
	Point2D q1109 =    Point2D(9*q_width/13,11*q_height/13-1);
	Point2D q1110 =  Point2D(10*q_width/13,11*q_height/13-1);
	Point2D q1111 =  Point2D(11*q_width/13,11*q_height/13-1);
	Point2D q1112 =  Point2D(12*q_width/13,11*q_height/13-1);
	Point2D q1113 =Point2D(13*q_width/13-1,11*q_height/13-1);

	Point2D q1200 =    Point2D(0*q_width/13,12*q_height/13-1);
	Point2D q1201 =    Point2D(1*q_width/13,12*q_height/13-1);
	Point2D q1202 =    Point2D(2*q_width/13,12*q_height/13-1);
	Point2D q1203 =    Point2D(3*q_width/13,12*q_height/13-1);
	Point2D q1204 =    Point2D(4*q_width/13,12*q_height/13-1);
	Point2D q1205 =    Point2D(5*q_width/13,12*q_height/13-1);
	Point2D q1206 =    Point2D(6*q_width/13,12*q_height/13-1);
	Point2D q1207 =    Point2D(7*q_width/13,12*q_height/13-1);
	Point2D q1208 =    Point2D(8*q_width/13,12*q_height/13-1);
	Point2D q1209 =    Point2D(9*q_width/13,12*q_height/13-1);
	Point2D q1210 =  Point2D(10*q_width/13,12*q_height/13-1);
	Point2D q1211 =  Point2D(11*q_width/13,12*q_height/13-1);
	Point2D q1212 =  Point2D(12*q_width/13,12*q_height/13-1);
	Point2D q1213 =Point2D(13*q_width/13-1,12*q_height/13-1);

	Point2D q1300 =    Point2D(0*q_width/13,13*q_height/13-1);
	Point2D q1301 =    Point2D(1*q_width/13,13*q_height/13-1);
	Point2D q1302 =    Point2D(2*q_width/13,13*q_height/13-1);
	Point2D q1303 =    Point2D(3*q_width/13,13*q_height/13-1);
	Point2D q1304 =    Point2D(4*q_width/13,13*q_height/13-1);
	Point2D q1305 =    Point2D(5*q_width/13,13*q_height/13-1);
	Point2D q1306 =    Point2D(6*q_width/13,13*q_height/13-1);
	Point2D q1307 =    Point2D(7*q_width/13,13*q_height/13-1);
	Point2D q1308 =    Point2D(8*q_width/13,13*q_height/13-1);
	Point2D q1309 =    Point2D(9*q_width/13,13*q_height/13-1);
	Point2D q1310 =  Point2D(10*q_width/13,13*q_height/13-1);
	Point2D q1311 =  Point2D(11*q_width/13,13*q_height/13-1);
	Point2D q1312 =  Point2D(12*q_width/13,13*q_height/13-1);
	Point2D q1313 =Point2D(13*q_width/13-1,13*q_height/13-1);

	unsigned int Q0000 = IntegralImageArea(integralBuffer,temp_rgb_line,q0000, q0001, q0100,q0101 );
	unsigned int Q0001 = IntegralImageArea(integralBuffer,temp_rgb_line,q0001, q0002, q0101,q0102 );
	unsigned int Q0002 = IntegralImageArea(integralBuffer,temp_rgb_line,q0002, q0003, q0102,q0103 );
	unsigned int Q0003 = IntegralImageArea(integralBuffer,temp_rgb_line,q0003, q0004, q0103,q0104 );
	unsigned int Q0004 = IntegralImageArea(integralBuffer,temp_rgb_line,q0004, q0005, q0104,q0105 );
	unsigned int Q0005 = IntegralImageArea(integralBuffer,temp_rgb_line,q0005, q0006, q0105,q0106 );
	unsigned int Q0006 = IntegralImageArea(integralBuffer,temp_rgb_line,q0006, q0007, q0106,q0107 );
	unsigned int Q0007 = IntegralImageArea(integralBuffer,temp_rgb_line,q0007, q0008, q0107,q0108 );
	unsigned int Q0008 = IntegralImageArea(integralBuffer,temp_rgb_line,q0008, q0009, q0108,q0109 );
	unsigned int Q0009 = IntegralImageArea(integralBuffer,temp_rgb_line,q0009, q0010, q0109,q0110 );
	unsigned int Q0010 = IntegralImageArea(integralBuffer,temp_rgb_line,q0010, q0011, q0110,q0111 );
	unsigned int Q0011 = IntegralImageArea(integralBuffer,temp_rgb_line,q0011, q0012, q0111,q0112 );
	unsigned int Q0012 = IntegralImageArea(integralBuffer,temp_rgb_line,q0012, q0013, q0112,q0113 );

	std::cout<<"IDEN: MARKER Q0000: "<<Q0000<<" Q0001: "<<Q0001<<" Q0002: "<<Q0002<<" Q0003: "<<Q0003<<" Q0004: "<<Q0004<<" Q0005: "<<Q0005<<" Q0006: "<<Q0006<<" Q0007: "<<Q0007<<" Q0008: "<<Q0008<<" Q0009: "<<Q0009<<" Q0010: "<<Q0010<<" Q0011: "<<Q0011<<" Q0012: "<<Q0012<<std::endl;

	// unsigned int Q10 = IntegralImageArea(integralBuffer,temp_rgb_line,q10, q11, q20,q21 );
	// unsigned int Q11 = IntegralImageArea(integralBuffer,temp_rgb_line,q11, q12, q21,q22 );
	// unsigned int Q12 = IntegralImageArea(integralBuffer,temp_rgb_line,q12, q13, q22,q23 );
	// unsigned int Q13 = IntegralImageArea(integralBuffer,temp_rgb_line,q13, q14, q23,q24 );
	// unsigned int Q14 = IntegralImageArea(integralBuffer,temp_rgb_line,q14, q15, q24,q25 );


	unsigned int Q0100 = IntegralImageArea(integralBuffer,temp_rgb_line,q0100, q0101, q0200,q0201 );
	unsigned int Q0101 = IntegralImageArea(integralBuffer,temp_rgb_line,q0101, q0102, q0201,q0202 );
	unsigned int Q0102 = IntegralImageArea(integralBuffer,temp_rgb_line,q0102, q0103, q0202,q0203 );
	unsigned int Q0103 = IntegralImageArea(integralBuffer,temp_rgb_line,q0103, q0104, q0203,q0204 );
	unsigned int Q0104 = IntegralImageArea(integralBuffer,temp_rgb_line,q0104, q0105, q0204,q0205 );
	unsigned int Q0105 = IntegralImageArea(integralBuffer,temp_rgb_line,q0105, q0106, q0205,q0206 );
	unsigned int Q0106 = IntegralImageArea(integralBuffer,temp_rgb_line,q0106, q0107, q0206,q0207 );
	unsigned int Q0107 = IntegralImageArea(integralBuffer,temp_rgb_line,q0107, q0108, q0207,q0208 );
	unsigned int Q0108 = IntegralImageArea(integralBuffer,temp_rgb_line,q0108, q0109, q0208,q0209 );
	unsigned int Q0109 = IntegralImageArea(integralBuffer,temp_rgb_line,q0109, q0110, q0209,q0210 );
	unsigned int Q0110 = IntegralImageArea(integralBuffer,temp_rgb_line,q0110, q0111, q0210,q0211 );
	unsigned int Q0111 = IntegralImageArea(integralBuffer,temp_rgb_line,q0111, q0112, q0211,q0212 );
	unsigned int Q0112 = IntegralImageArea(integralBuffer,temp_rgb_line,q0112, q0113, q0212,q0213 );

	std::cout
	<<"IDEN: MARKER Q0100: "<<Q0100
	<<" Q0101: "<<Q0101
	<<" Q0102: "<<Q0102
	<<" Q0103: "<<Q0103
	<<" Q0104: "<<Q0104
	<<" Q0105: "<<Q0105
	<<" Q0106: "<<Q0106
	<<" Q0107: "<<Q0107
	<<" Q0108: "<<Q0108
	<<" Q0109: "<<Q0109
	<<" Q0110: "<<Q0110
	<<" Q0111: "<<Q0111
	<<" Q0112: "<<Q0112<<std::endl;

	unsigned int Q0200 = IntegralImageArea(integralBuffer,temp_rgb_line,q0200, q0201, q0300,q0301 );
	unsigned int Q0201 = IntegralImageArea(integralBuffer,temp_rgb_line,q0201, q0202, q0301,q0302 );
	unsigned int Q0202 = IntegralImageArea(integralBuffer,temp_rgb_line,q0202, q0203, q0302,q0303 );
	unsigned int Q0203 = IntegralImageArea(integralBuffer,temp_rgb_line,q0203, q0204, q0303,q0304 );
	unsigned int Q0204 = IntegralImageArea(integralBuffer,temp_rgb_line,q0204, q0205, q0304,q0305 );
	unsigned int Q0205 = IntegralImageArea(integralBuffer,temp_rgb_line,q0205, q0206, q0305,q0306 );
	unsigned int Q0206 = IntegralImageArea(integralBuffer,temp_rgb_line,q0206, q0207, q0306,q0307 );
	unsigned int Q0207 = IntegralImageArea(integralBuffer,temp_rgb_line,q0207, q0208, q0307,q0308 );
	unsigned int Q0208 = IntegralImageArea(integralBuffer,temp_rgb_line,q0208, q0209, q0308,q0309 );
	unsigned int Q0209 = IntegralImageArea(integralBuffer,temp_rgb_line,q0209, q0210, q0309,q0310 );
	unsigned int Q0210 = IntegralImageArea(integralBuffer,temp_rgb_line,q0210, q0211, q0310,q0311 );
	unsigned int Q0211 = IntegralImageArea(integralBuffer,temp_rgb_line,q0211, q0212, q0311,q0312 );
	unsigned int Q0212 = IntegralImageArea(integralBuffer,temp_rgb_line,q0212, q0213, q0312,q0313 );

	std::cout
	<<"IDEN: MARKER Q0200: "
				<<Q0200
	<<" Q0201: "<<Q0201
	<<" Q0202: "<<Q0202
	<<" Q0203: "<<Q0203
	<<" Q0204: "<<Q0204
	<<" Q0205: "<<Q0205
	<<" Q0206: "<<Q0206
	<<" Q0207: "<<Q0207
	<<" Q0208: "<<Q0208
	<<" Q0209: "<<Q0209
	<<" Q0210: "<<Q0210
	<<" Q0211: "<<Q0211
	<<" Q0212: "<<Q0212<<std::endl;

	unsigned int Q0300 = IntegralImageArea(integralBuffer,temp_rgb_line,q0300, q0301, q0400,q0401 );
	unsigned int Q0301 = IntegralImageArea(integralBuffer,temp_rgb_line,q0301, q0302, q0401,q0402 );
	unsigned int Q0302 = IntegralImageArea(integralBuffer,temp_rgb_line,q0302, q0303, q0402,q0403 );
	unsigned int Q0303 = IntegralImageArea(integralBuffer,temp_rgb_line,q0303, q0304, q0403,q0404 );
	unsigned int Q0304 = IntegralImageArea(integralBuffer,temp_rgb_line,q0304, q0305, q0404,q0405 );
	unsigned int Q0305 = IntegralImageArea(integralBuffer,temp_rgb_line,q0305, q0306, q0405,q0406 );
	unsigned int Q0306 = IntegralImageArea(integralBuffer,temp_rgb_line,q0306, q0307, q0406,q0407 );
	unsigned int Q0307 = IntegralImageArea(integralBuffer,temp_rgb_line,q0307, q0308, q0407,q0408 );
	unsigned int Q0308 = IntegralImageArea(integralBuffer,temp_rgb_line,q0308, q0309, q0408,q0409 );
	unsigned int Q0309 = IntegralImageArea(integralBuffer,temp_rgb_line,q0309, q0310, q0409,q0410 );
	unsigned int Q0310 = IntegralImageArea(integralBuffer,temp_rgb_line,q0310, q0311, q0410,q0411 );
	unsigned int Q0311 = IntegralImageArea(integralBuffer,temp_rgb_line,q0311, q0312, q0411,q0412 );
	unsigned int Q0312 = IntegralImageArea(integralBuffer,temp_rgb_line,q0312, q0313, q0412,q0413 );

	std::cout
	<<"IDEN: MARKER Q0300: "
				<<Q0300
	<<" Q0201: "<<Q0301
	<<" Q0202: "<<Q0302
	<<" Q0203: "<<Q0303
	<<" Q0204: "<<Q0304
	<<" Q0205: "<<Q0305
	<<" Q0206: "<<Q0306
	<<" Q0207: "<<Q0307
	<<" Q0208: "<<Q0308
	<<" Q0209: "<<Q0309
	<<" Q0210: "<<Q0310
	<<" Q0211: "<<Q0311
	<<" Q0212: "<<Q0312<<std::endl;

	unsigned int Q0400 = IntegralImageArea(integralBuffer,temp_rgb_line,q0300, q0301, q0400,q0401 );
	unsigned int Q0401 = IntegralImageArea(integralBuffer,temp_rgb_line,q0401, q0302, q0401,q0402 );
	unsigned int Q0402 = IntegralImageArea(integralBuffer,temp_rgb_line,q0402, q0303, q0402,q0403 );
	unsigned int Q0403 = IntegralImageArea(integralBuffer,temp_rgb_line,q0403, q0304, q0403,q0404 );
	unsigned int Q0404 = IntegralImageArea(integralBuffer,temp_rgb_line,q0404, q0305, q0404,q0405 );
	unsigned int Q0405 = IntegralImageArea(integralBuffer,temp_rgb_line,q0405, q0306, q0405,q0406 );
	unsigned int Q0406 = IntegralImageArea(integralBuffer,temp_rgb_line,q0406, q0307, q0406,q0407 );
	unsigned int Q0407 = IntegralImageArea(integralBuffer,temp_rgb_line,q0407, q0308, q0407,q0408 );
	unsigned int Q0408 = IntegralImageArea(integralBuffer,temp_rgb_line,q0408, q0309, q0408,q0409 );
	unsigned int Q0409 = IntegralImageArea(integralBuffer,temp_rgb_line,q0409, q0310, q0409,q0410 );
	unsigned int Q0410 = IntegralImageArea(integralBuffer,temp_rgb_line,q0410, q0311, q0410,q0411 );
	unsigned int Q0411 = IntegralImageArea(integralBuffer,temp_rgb_line,q0411, q0312, q0411,q0412 );
	unsigned int Q0412 = IntegralImageArea(integralBuffer,temp_rgb_line,q0412, q0313, q0412,q0413 );

	std::cout
	<<"IDEN: MARKER Q0400: "
				<<Q0400
	<<" Q0401: "<<Q0401
	<<" Q0402: "<<Q0402
	<<" Q0403: "<<Q0403
	<<" Q0404: "<<Q0404
	<<" Q0405: "<<Q0405
	<<" Q0406: "<<Q0406
	<<" Q0407: "<<Q0407
	<<" Q0408: "<<Q0408
	<<" Q0409: "<<Q0409
	<<" Q0410: "<<Q0410
	<<" Q0411: "<<Q0411
	<<" Q0412: "<<Q0412<<std::endl;

	unsigned int Q0500 = IntegralImageArea(integralBuffer,temp_rgb_line,q0500, q0501, q0600,q0601 );
	unsigned int Q0501 = IntegralImageArea(integralBuffer,temp_rgb_line,q0501, q0502, q0601,q0602 );
	unsigned int Q0502 = IntegralImageArea(integralBuffer,temp_rgb_line,q0502, q0503, q0602,q0603 );
	unsigned int Q0503 = IntegralImageArea(integralBuffer,temp_rgb_line,q0503, q0504, q0603,q0604 );
	unsigned int Q0504 = IntegralImageArea(integralBuffer,temp_rgb_line,q0504, q0505, q0604,q0605 );
	unsigned int Q0505 = IntegralImageArea(integralBuffer,temp_rgb_line,q0505, q0506, q0605,q0606 );
	unsigned int Q0506 = IntegralImageArea(integralBuffer,temp_rgb_line,q0506, q0507, q0606,q0607 );
	unsigned int Q0507 = IntegralImageArea(integralBuffer,temp_rgb_line,q0507, q0508, q0607,q0608 );
	unsigned int Q0508 = IntegralImageArea(integralBuffer,temp_rgb_line,q0508, q0509, q0608,q0609 );
	unsigned int Q0509 = IntegralImageArea(integralBuffer,temp_rgb_line,q0509, q0510, q0609,q0610 );
	unsigned int Q0510 = IntegralImageArea(integralBuffer,temp_rgb_line,q0510, q0511, q0610,q0611 );
	unsigned int Q0511 = IntegralImageArea(integralBuffer,temp_rgb_line,q0511, q0512, q0611,q0612 );
	unsigned int Q0512 = IntegralImageArea(integralBuffer,temp_rgb_line,q0512, q0513, q0612,q0613 );

	std::cout
	<<"IDEN: MARKER Q0500: "
				<<Q0500
	<<" Q0501: "<<Q0501
	<<" Q0502: "<<Q0502
	<<" Q0503: "<<Q0503
	<<" Q0504: "<<Q0504
	<<" Q0505: "<<Q0505
	<<" Q0506: "<<Q0506
	<<" Q0507: "<<Q0507
	<<" Q0508: "<<Q0508
	<<" Q0509: "<<Q0509
	<<" Q0510: "<<Q0510
	<<" Q0511: "<<Q0511
	<<" Q0512: "<<Q0512<<std::endl;

	unsigned int Q0600 = IntegralImageArea(integralBuffer,temp_rgb_line,q0600, q0601, q0700,q0701 );
	unsigned int Q0601 = IntegralImageArea(integralBuffer,temp_rgb_line,q0601, q0602, q0701,q0702 );
	unsigned int Q0602 = IntegralImageArea(integralBuffer,temp_rgb_line,q0602, q0603, q0702,q0703 );
	unsigned int Q0603 = IntegralImageArea(integralBuffer,temp_rgb_line,q0603, q0604, q0703,q0704 );
	unsigned int Q0604 = IntegralImageArea(integralBuffer,temp_rgb_line,q0604, q0605, q0704,q0705 );
	unsigned int Q0605 = IntegralImageArea(integralBuffer,temp_rgb_line,q0605, q0606, q0705,q0706 );
	unsigned int Q0606 = IntegralImageArea(integralBuffer,temp_rgb_line,q0606, q0607, q0706,q0707 );
	unsigned int Q0607 = IntegralImageArea(integralBuffer,temp_rgb_line,q0607, q0608, q0707,q0708 );
	unsigned int Q0608 = IntegralImageArea(integralBuffer,temp_rgb_line,q0608, q0609, q0708,q0709 );
	unsigned int Q0609 = IntegralImageArea(integralBuffer,temp_rgb_line,q0609, q0610, q0709,q0710 );
	unsigned int Q0610 = IntegralImageArea(integralBuffer,temp_rgb_line,q0610, q0611, q0710,q0711 );
	unsigned int Q0611 = IntegralImageArea(integralBuffer,temp_rgb_line,q0611, q0612, q0711,q0712 );
	unsigned int Q0612 = IntegralImageArea(integralBuffer,temp_rgb_line,q0612, q0613, q0712,q0713 );

	std::cout
	<<"IDEN: MARKER Q0600: "
				<<Q0600
	<<" Q0601: "<<Q0601
	<<" Q0602: "<<Q0602
	<<" Q0603: "<<Q0603
	<<" Q0604: "<<Q0604
	<<" Q0605: "<<Q0605
	<<" Q0606: "<<Q0606
	<<" Q0607: "<<Q0607
	<<" Q0608: "<<Q0608
	<<" Q0609: "<<Q0609
	<<" Q0610: "<<Q0610
	<<" Q0611: "<<Q0611
	<<" Q0612: "<<Q0612<<std::endl;

	unsigned int Q0700 = IntegralImageArea(integralBuffer,temp_rgb_line,q0700, q0701, q0800,q0801 );
	unsigned int Q0701 = IntegralImageArea(integralBuffer,temp_rgb_line,q0701, q0702, q0801,q0802 );
	unsigned int Q0702 = IntegralImageArea(integralBuffer,temp_rgb_line,q0702, q0703, q0802,q0803 );
	unsigned int Q0703 = IntegralImageArea(integralBuffer,temp_rgb_line,q0703, q0704, q0803,q0804 );
	unsigned int Q0704 = IntegralImageArea(integralBuffer,temp_rgb_line,q0704, q0705, q0804,q0805 );
	unsigned int Q0705 = IntegralImageArea(integralBuffer,temp_rgb_line,q0705, q0706, q0805,q0806 );
	unsigned int Q0706 = IntegralImageArea(integralBuffer,temp_rgb_line,q0706, q0707, q0806,q0807 );
	unsigned int Q0707 = IntegralImageArea(integralBuffer,temp_rgb_line,q0707, q0708, q0807,q0808 );
	unsigned int Q0708 = IntegralImageArea(integralBuffer,temp_rgb_line,q0708, q0709, q0808,q0809 );
	unsigned int Q0709 = IntegralImageArea(integralBuffer,temp_rgb_line,q0709, q0710, q0809,q0810 );
	unsigned int Q0710 = IntegralImageArea(integralBuffer,temp_rgb_line,q0710, q0711, q0810,q0811 );
	unsigned int Q0711 = IntegralImageArea(integralBuffer,temp_rgb_line,q0711, q0712, q0811,q0812 );
	unsigned int Q0712 = IntegralImageArea(integralBuffer,temp_rgb_line,q0712, q0713, q0812,q0813 );

	std::cout
	<<"IDEN: MARKER Q0700: "
				<<Q0700
	<<" Q0701: "<<Q0701
	<<" Q0702: "<<Q0702
	<<" Q0703: "<<Q0703
	<<" Q0704: "<<Q0704
	<<" Q0705: "<<Q0705
	<<" Q0706: "<<Q0706
	<<" Q0707: "<<Q0707
	<<" Q0708: "<<Q0708
	<<" Q0709: "<<Q0709
	<<" Q0710: "<<Q0710
	<<" Q0711: "<<Q0711
	<<" Q0712: "<<Q0712<<std::endl;

	unsigned int Q0800 = IntegralImageArea(integralBuffer,temp_rgb_line,q0800, q0801, q0900,q0901 );
	unsigned int Q0801 = IntegralImageArea(integralBuffer,temp_rgb_line,q0801, q0802, q0901,q0902 );
	unsigned int Q0802 = IntegralImageArea(integralBuffer,temp_rgb_line,q0802, q0803, q0902,q0903 );
	unsigned int Q0803 = IntegralImageArea(integralBuffer,temp_rgb_line,q0803, q0804, q0903,q0904 );
	unsigned int Q0804 = IntegralImageArea(integralBuffer,temp_rgb_line,q0804, q0805, q0904,q0905 );
	unsigned int Q0805 = IntegralImageArea(integralBuffer,temp_rgb_line,q0805, q0806, q0905,q0906 );
	unsigned int Q0806 = IntegralImageArea(integralBuffer,temp_rgb_line,q0806, q0807, q0906,q0907 );
	unsigned int Q0807 = IntegralImageArea(integralBuffer,temp_rgb_line,q0807, q0808, q0907,q0908 );
	unsigned int Q0808 = IntegralImageArea(integralBuffer,temp_rgb_line,q0808, q0809, q0908,q0909 );
	unsigned int Q0809 = IntegralImageArea(integralBuffer,temp_rgb_line,q0809, q0810, q0909,q0910 );
	unsigned int Q0810 = IntegralImageArea(integralBuffer,temp_rgb_line,q0810, q0811, q0910,q0911 );
	unsigned int Q0811 = IntegralImageArea(integralBuffer,temp_rgb_line,q0811, q0812, q0911,q0912 );
	unsigned int Q0812 = IntegralImageArea(integralBuffer,temp_rgb_line,q0812, q0813, q0912,q0913 );

	std::cout
	<<"IDEN: MARKER Q0800: "
				<<Q0800
	<<" Q0801: "<<Q0801
	<<" Q0802: "<<Q0802
	<<" Q0803: "<<Q0803
	<<" Q0804: "<<Q0804
	<<" Q0805: "<<Q0805
	<<" Q0806: "<<Q0806
	<<" Q0807: "<<Q0807
	<<" Q0808: "<<Q0808
	<<" Q0809: "<<Q0809
	<<" Q0810: "<<Q0810
	<<" Q0811: "<<Q0811
	<<" Q0812: "<<Q0812<<std::endl;

	unsigned int Q0900 = IntegralImageArea(integralBuffer,temp_rgb_line,q0900, q0901, q1000,q1001 );
	unsigned int Q0901 = IntegralImageArea(integralBuffer,temp_rgb_line,q0901, q0902, q1001,q1002 );
	unsigned int Q0902 = IntegralImageArea(integralBuffer,temp_rgb_line,q0902, q0903, q1002,q1003 );
	unsigned int Q0903 = IntegralImageArea(integralBuffer,temp_rgb_line,q0903, q0904, q1003,q1004 );
	unsigned int Q0904 = IntegralImageArea(integralBuffer,temp_rgb_line,q0904, q0905, q1004,q1005 );
	unsigned int Q0905 = IntegralImageArea(integralBuffer,temp_rgb_line,q0905, q0906, q1005,q1006 );
	unsigned int Q0906 = IntegralImageArea(integralBuffer,temp_rgb_line,q0906, q0907, q1006,q1007 );
	unsigned int Q0907 = IntegralImageArea(integralBuffer,temp_rgb_line,q0907, q0908, q1007,q1008 );
	unsigned int Q0908 = IntegralImageArea(integralBuffer,temp_rgb_line,q0908, q0909, q1008,q1009 );
	unsigned int Q0909 = IntegralImageArea(integralBuffer,temp_rgb_line,q0909, q0910, q1009,q1010 );
	unsigned int Q0910 = IntegralImageArea(integralBuffer,temp_rgb_line,q0910, q0911, q1010,q1011 );
	unsigned int Q0911 = IntegralImageArea(integralBuffer,temp_rgb_line,q0911, q0912, q1011,q1012 );
	unsigned int Q0912 = IntegralImageArea(integralBuffer,temp_rgb_line,q0912, q0913, q1012,q1013 );

	std::cout
	<<"IDEN: MARKER Q0900: "
				<<Q0900
	<<" Q0901: "<<Q0901
	<<" Q0902: "<<Q0902
	<<" Q0903: "<<Q0903
	<<" Q0904: "<<Q0904
	<<" Q0905: "<<Q0905
	<<" Q0906: "<<Q0906
	<<" Q0907: "<<Q0907
	<<" Q0908: "<<Q0908
	<<" Q0909: "<<Q0909
	<<" Q0910: "<<Q0910
	<<" Q0911: "<<Q0911
	<<" Q0912: "<<Q0912<<std::endl;

	unsigned int Q1000 = IntegralImageArea(integralBuffer,temp_rgb_line,q1000, q1001, q1100,q1101 );
	unsigned int Q1001 = IntegralImageArea(integralBuffer,temp_rgb_line,q1001, q1002, q1101,q1102 );
	unsigned int Q1002 = IntegralImageArea(integralBuffer,temp_rgb_line,q1002, q1003, q1102,q1103 );
	unsigned int Q1003 = IntegralImageArea(integralBuffer,temp_rgb_line,q1003, q1004, q1103,q1104 );
	unsigned int Q1004 = IntegralImageArea(integralBuffer,temp_rgb_line,q1004, q1005, q1104,q1105 );
	unsigned int Q1005 = IntegralImageArea(integralBuffer,temp_rgb_line,q1005, q1006, q1105,q1106 );
	unsigned int Q1006 = IntegralImageArea(integralBuffer,temp_rgb_line,q1006, q1007, q1106,q1107 );
	unsigned int Q1007 = IntegralImageArea(integralBuffer,temp_rgb_line,q1007, q1008, q1107,q1108 );
	unsigned int Q1008 = IntegralImageArea(integralBuffer,temp_rgb_line,q1008, q1009, q1108,q1109 );
	unsigned int Q1009 = IntegralImageArea(integralBuffer,temp_rgb_line,q1009, q1010, q1109,q1110 );
	unsigned int Q1010 = IntegralImageArea(integralBuffer,temp_rgb_line,q1010, q1011, q1110,q1111 );
	unsigned int Q1011 = IntegralImageArea(integralBuffer,temp_rgb_line,q1011, q1012, q1111,q1112 );
	unsigned int Q1012 = IntegralImageArea(integralBuffer,temp_rgb_line,q1012, q1013, q1112,q1113 );

	std::cout
	<<"IDEN: MARKER Q1000: "
				<<Q1000
	<<" Q1001: "<<Q1001
	<<" Q1002: "<<Q1002
	<<" Q1003: "<<Q1003
	<<" Q1004: "<<Q1004
	<<" Q1005: "<<Q1005
	<<" Q1006: "<<Q1006
	<<" Q1007: "<<Q1007
	<<" Q1008: "<<Q1008
	<<" Q1009: "<<Q1009
	<<" Q1010: "<<Q1010
	<<" Q1011: "<<Q1011
	<<" Q1012: "<<Q1012<<std::endl;

	unsigned int Q1100 = IntegralImageArea(integralBuffer,temp_rgb_line,q1100, q1101, q1200,q1201 );
	unsigned int Q1101 = IntegralImageArea(integralBuffer,temp_rgb_line,q1101, q1102, q1201,q1202 );
	unsigned int Q1102 = IntegralImageArea(integralBuffer,temp_rgb_line,q1102, q1103, q1202,q1203 );
	unsigned int Q1103 = IntegralImageArea(integralBuffer,temp_rgb_line,q1103, q1104, q1203,q1204 );
	unsigned int Q1104 = IntegralImageArea(integralBuffer,temp_rgb_line,q1104, q1105, q1204,q1205 );
	unsigned int Q1105 = IntegralImageArea(integralBuffer,temp_rgb_line,q1105, q1106, q1205,q1206 );
	unsigned int Q1106 = IntegralImageArea(integralBuffer,temp_rgb_line,q1106, q1107, q1206,q1207 );
	unsigned int Q1107 = IntegralImageArea(integralBuffer,temp_rgb_line,q1107, q1108, q1207,q1208 );
	unsigned int Q1108 = IntegralImageArea(integralBuffer,temp_rgb_line,q1108, q1109, q1208,q1209 );
	unsigned int Q1109 = IntegralImageArea(integralBuffer,temp_rgb_line,q1109, q1110, q1209,q1210 );
	unsigned int Q1110 = IntegralImageArea(integralBuffer,temp_rgb_line,q1110, q1111, q1210,q1211 );
	unsigned int Q1111 = IntegralImageArea(integralBuffer,temp_rgb_line,q1111, q1112, q1211,q1212 );
	unsigned int Q1112 = IntegralImageArea(integralBuffer,temp_rgb_line,q1112, q1113, q1212,q1213 );

	std::cout
	<<"IDEN: MARKER Q1100: "
				<<Q1100
	<<" Q1101: "<<Q1101
	<<" Q1102: "<<Q1102
	<<" Q1103: "<<Q1103
	<<" Q1104: "<<Q1104
	<<" Q1105: "<<Q1105
	<<" Q1106: "<<Q1106
	<<" Q1107: "<<Q1107
	<<" Q1108: "<<Q1108
	<<" Q1109: "<<Q1109
	<<" Q1110: "<<Q1110
	<<" Q1111: "<<Q1111
	<<" Q1112: "<<Q1112<<std::endl;

	unsigned int Q1200 = IntegralImageArea(integralBuffer,temp_rgb_line,q1200, q1201, q1300,q1301 );
	unsigned int Q1201 = IntegralImageArea(integralBuffer,temp_rgb_line,q1201, q1202, q1301,q1302 );
	unsigned int Q1202 = IntegralImageArea(integralBuffer,temp_rgb_line,q1202, q1203, q1302,q1303 );
	unsigned int Q1203 = IntegralImageArea(integralBuffer,temp_rgb_line,q1203, q1204, q1303,q1304 );
	unsigned int Q1204 = IntegralImageArea(integralBuffer,temp_rgb_line,q1204, q1205, q1304,q1305 );
	unsigned int Q1205 = IntegralImageArea(integralBuffer,temp_rgb_line,q1205, q1206, q1305,q1306 );
	unsigned int Q1206 = IntegralImageArea(integralBuffer,temp_rgb_line,q1206, q1207, q1306,q1307 );
	unsigned int Q1207 = IntegralImageArea(integralBuffer,temp_rgb_line,q1207, q1208, q1307,q1308 );
	unsigned int Q1208 = IntegralImageArea(integralBuffer,temp_rgb_line,q1208, q1209, q1308,q1309 );
	unsigned int Q1209 = IntegralImageArea(integralBuffer,temp_rgb_line,q1209, q1210, q1309,q1310 );
	unsigned int Q1210 = IntegralImageArea(integralBuffer,temp_rgb_line,q1210, q1211, q1310,q1311 );
	unsigned int Q1211 = IntegralImageArea(integralBuffer,temp_rgb_line,q1211, q1212, q1311,q1312 );
	unsigned int Q1212 = IntegralImageArea(integralBuffer,temp_rgb_line,q1212, q1213, q1312,q1313 );

	std::cout
	<<"IDEN: MARKER Q1200: "
				<<Q1200
	<<" Q1201: "<<Q1201
	<<" Q1202: "<<Q1202
	<<" Q1203: "<<Q1203
	<<" Q1204: "<<Q1204
	<<" Q1205: "<<Q1205
	<<" Q1206: "<<Q1206
	<<" Q1207: "<<Q1207
	<<" Q1208: "<<Q1208
	<<" Q1209: "<<Q1209
	<<" Q1210: "<<Q1210
	<<" Q1211: "<<Q1211
	<<" Q1212: "<<Q1212<<std::endl;

	//stupidly put marker calculated area in an array
	unsigned int markerArea[13*13]; 
	unsigned row = 0;
	// markerArea[row*13+0] = Q0000;
	// markerArea[row*13+1] = Q0001;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0002;
	markerArea[row*13+3] = Q0003;
	markerArea[row*13+4] = Q0004;
	markerArea[row*13+5] = Q0005;
	markerArea[row*13+6] = Q0006;
	markerArea[row*13+7] = Q0007;
	markerArea[row*13+8] = Q0008;
	markerArea[row*13+9] = Q0009;
	markerArea[row*13+10] = Q0010;
	markerArea[row*13+11] = Q0011;
	markerArea[row*13+12] = Q0012;
	row++;
	// markerArea[row*13+0] = Q0100;
	// markerArea[row*13+1] = Q0101;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0102;
	markerArea[row*13+3] = Q0103;
	markerArea[row*13+4] = Q0104;
	markerArea[row*13+5] = Q0105;
	markerArea[row*13+6] = Q0106;
	markerArea[row*13+7] = Q0107;
	markerArea[row*13+8] = Q0108;
	markerArea[row*13+9] = Q0109;
	markerArea[row*13+10] = Q0110;
	markerArea[row*13+11] = Q0111;
	markerArea[row*13+12] = Q0112;
	row++;
	// markerArea[row*13+0] = Q0200;
	// markerArea[row*13+1] = Q0201;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0202;
	markerArea[row*13+3] = Q0203;
	markerArea[row*13+4] = Q0204;
	markerArea[row*13+5] = Q0205;
	markerArea[row*13+6] = Q0206;
	markerArea[row*13+7] = Q0207;
	markerArea[row*13+8] = Q0208;
	markerArea[row*13+9] = Q0209;
	markerArea[row*13+10] = Q0210;
	markerArea[row*13+11] = Q0211;
	markerArea[row*13+12] = Q0212;
	row++;
	// markerArea[row*13+0] = Q0300;
	// markerArea[row*13+1] = Q0301;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0302;
	markerArea[row*13+3] = Q0303;
	markerArea[row*13+4] = Q0304;
	markerArea[row*13+5] = Q0305;
	markerArea[row*13+6] = Q0306;
	markerArea[row*13+7] = Q0307;
	markerArea[row*13+8] = Q0308;
	markerArea[row*13+9] = Q0309;
	markerArea[row*13+10] = Q0310;
	markerArea[row*13+11] = Q0311;
	markerArea[row*13+12] = Q0312;
	row++;
	// markerArea[row*13+0] = Q0400;
	// markerArea[row*13+1] = Q0401;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0402;
	markerArea[row*13+3] = Q0403;
	markerArea[row*13+4] = Q0404;
	markerArea[row*13+5] = Q0405;
	markerArea[row*13+6] = Q0406;
	markerArea[row*13+7] = Q0407;
	markerArea[row*13+8] = Q0408;
	markerArea[row*13+9] = Q0409;
	markerArea[row*13+10] = Q0410;
	markerArea[row*13+11] = Q0411;
	markerArea[row*13+12] = Q0412;
	row++;

	// markerArea[row*13+0] = Q0500;
	// markerArea[row*13+1] = Q0501;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0502;
	markerArea[row*13+3] = Q0503;
	markerArea[row*13+4] = Q0504;
	markerArea[row*13+5] = Q0505;
	markerArea[row*13+6] = Q0506;
	markerArea[row*13+7] = Q0507;
	markerArea[row*13+8] = Q0508;
	markerArea[row*13+9] = Q0509;
	markerArea[row*13+10] = Q0510;
	markerArea[row*13+11] = Q0511;
	markerArea[row*13+12] = Q0512;
	row++;
	// markerArea[row*13+0] = Q0600;
	// markerArea[row*13+1] = Q0601;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0602;
	markerArea[row*13+3] = Q0603;
	markerArea[row*13+4] = Q0604;
	markerArea[row*13+5] = Q0605;
	markerArea[row*13+6] = Q0606;
	markerArea[row*13+7] = Q0607;
	markerArea[row*13+8] = Q0608;
	markerArea[row*13+9] = Q0609;
	markerArea[row*13+10] = Q0610;
	markerArea[row*13+11] = Q0611;
	markerArea[row*13+12] = Q0612;
	row++;
	// markerArea[row*13+0] = Q0700;
	// markerArea[row*13+1] = Q0701;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0702;
	markerArea[row*13+3] = Q0703;
	markerArea[row*13+4] = Q0704;
	markerArea[row*13+5] = Q0705;
	markerArea[row*13+6] = Q0706;
	markerArea[row*13+7] = Q0707;
	markerArea[row*13+8] = Q0708;
	markerArea[row*13+9] = Q0709;
	markerArea[row*13+10] = Q0710;
	markerArea[row*13+11] = Q0711;
	markerArea[row*13+12] = Q0712;
	row++;
	// markerArea[row*13+0] = Q0800;
	// markerArea[row*13+1] = Q0801;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0802;
	markerArea[row*13+3] = Q0803;
	markerArea[row*13+4] = Q0804;
	markerArea[row*13+5] = Q0805;
	markerArea[row*13+6] = Q0806;
	markerArea[row*13+7] = Q0807;
	markerArea[row*13+8] = Q0808;
	markerArea[row*13+9] = Q0809;
	markerArea[row*13+10] = Q0810;
	markerArea[row*13+11] = Q0811;
	markerArea[row*13+12] = Q0812;
	row++;
	// markerArea[row*13+0] = Q0900;
	// markerArea[row*13+1] = Q0901;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q0902;
	markerArea[row*13+3] = Q0903;
	markerArea[row*13+4] = Q0904;
	markerArea[row*13+5] = Q0905;
	markerArea[row*13+6] = Q0906;
	markerArea[row*13+7] = Q0907;
	markerArea[row*13+8] = Q0908;
	markerArea[row*13+9] = Q0909;
	markerArea[row*13+10] = Q0910;
	markerArea[row*13+11] = Q0911;
	markerArea[row*13+12] = Q0912;
	row++;
	// markerArea[row*13+0] = Q1000;
	// markerArea[row*13+1] = Q1001;
		markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q1002;
	markerArea[row*13+3] = Q1003;
	markerArea[row*13+4] = Q1004;
	markerArea[row*13+5] = Q1005;
	markerArea[row*13+6] = Q1006;
	markerArea[row*13+7] = Q1007;
	markerArea[row*13+8] = Q1008;
	markerArea[row*13+9] = Q1009;
	markerArea[row*13+10] = Q1010;
	markerArea[row*13+11] = Q1011;
	markerArea[row*13+12] = Q1012;
	row++;
	// markerArea[row*13+0] = Q1100;
	// markerArea[row*13+1] = Q1101;
	markerArea[row*13+0] =0;
	markerArea[row*13+1] =0;
	markerArea[row*13+2] = Q1102;
	markerArea[row*13+3] = Q1103;
	markerArea[row*13+4] = Q1104;
	markerArea[row*13+5] = Q1105;
	markerArea[row*13+6] = Q1106;
	markerArea[row*13+7] = Q1107;
	markerArea[row*13+8] = Q1108;
	markerArea[row*13+9] = Q1109;
	markerArea[row*13+10] = Q1110;
	markerArea[row*13+11] = Q1111;
	markerArea[row*13+12] = Q1112;
	row++;
	// markerArea[row*13+0] = Q1200;
	// markerArea[row*13+1] = Q1201;
	markerArea[row*13+0] = 0;
	markerArea[row*13+1] = 0;
	markerArea[row*13+2] = Q1202;
	markerArea[row*13+3] = Q1203;
	markerArea[row*13+4] = Q1204;
	markerArea[row*13+5] = Q1205;
	markerArea[row*13+6] = Q1206;
	markerArea[row*13+7] = Q1207;
	markerArea[row*13+8] = Q1208;
	markerArea[row*13+9] = Q1209;
	markerArea[row*13+10] = Q1210;
	markerArea[row*13+11] = Q1211;
	markerArea[row*13+12] = Q1212;


	unsigned int threshold = 2000; 

	//find max_value from block
	unsigned int max_area =0;
	unsigned int area_count=0;
	for (int m_row = 0;m_row<12;m_row++){
		for (int m_col = 0;m_col<12;m_col++ ){
			if(markerArea[m_row*13+m_col]>max_area ){
			// std::cout<<markerArea[m_row*13+m_col]<<std::endl;
				max_area = markerArea[m_row*13+m_col];
				
			}
		}
	}
	// if(max_area > threshold){
	// 	threshold = max_area/4;	
	// }
	std::cout<<"Threshold = "<<threshold<<std::endl;

	//find top left based on threshold
	Point2D point= Point2D(); 
	for (int m_row = 0;m_row<12;m_row++){
		for (int m_col = 0;m_col<12;m_col++ ){
			if(markerArea[m_row*13+m_col]>threshold ){
				area_count++;
			// std::cout<<markerArea[m_row*13+m_col]<<std::endl;

				if(point.X ==0 && point.Y ==0){
					point.X = m_col;
					point.Y = m_row;
					// std::cout<<"TRACKER: MARKER top left corner is in "<< point.X<<" , "<<point.Y<<std::endl;
				}
				
			}
		}
	}

	//scan 4 blocks
	int block_size_l = 4;
	unsigned int block[16];
	int b_row=0;
	int b_col=0;
	int b_count=0;
	for (int m_row = point.Y;m_row<12 && m_row< (point.Y+block_size_l);m_row++){
		for (int m_col = point.X;m_col<12 && m_col< (point.X+block_size_l);m_col++ ){
			if(markerArea[m_row*13+m_col]>threshold ){
			// std::cout<<markerArea[m_row*13+m_col]<<std::endl;

					// std::cout<<"TRACKER: MARKER block point is in "<< m_col<<" , "<<m_row<<std::endl;
				
				b_count++;
			}
			block[b_row*block_size_l+b_col] = markerArea[m_row*13+m_col];
			// std::cout<<"TRACKER: Marker: "<<m_col<<" , "<<m_row<<" block "<<b_col<<" , "<<b_row<< " value from marker "<< markerArea[m_row*13+m_col]<< " value from block "<< block[b_row*block_size_l+b_col] <<std::endl;
			
			b_col++;
		}
		b_row++;
		b_col=0;
	}
		std::cout<<"TRACKER: Marker block count"<< b_count<<std::endl;

	//print block
	for (int m_row = 0;m_row<block_size_l;m_row++){
		for (int m_col = 0;m_col<block_size_l;m_col++ ){
			std::cout<<(unsigned int)block[m_row*block_size_l+m_col]<<" ";
		}
		std::cout<<std::endl;
	}

	//Identification forward case

	// Point2D b_point= Point2D(); 
	// for (int m_row = 0;m_row<block_size_l;m_row++){
	// 	for (int m_col = 0;m_col<block_size_l;m_col++ ){
	// 		if(block[m_row*block_size_l+m_col]>threshold ){
	// 		// std::cout<<markerArea[m_row*13+m_col]<<std::endl;
	// 			if(b_point.X ==0 && b_point.Y ==0){
	// 				b_point.X = m_col;
	// 				b_point.Y = m_row;
	// 				// std::cout<<"TRACKER: BLOCK top left corner is in "<< point.X<<" , "<<point.Y<<std::endl;
	// 			}
	// 		}
	// 	}
	// 	std::cout<<std::endl;
	// }

	//find max value in the block

	unsigned int max_b_area =0;
	unsigned int b_threshold = 0;
	for (int m_row = 0;m_row<4;m_row++){
		for (int m_col = 0;m_col<4;m_col++ ){
			if(block[m_row*4+m_col]>max_b_area ){
			// std::cout<<markerArea[m_row*13+m_col]<<std::endl;
				max_b_area = block[m_row*13+m_col];
				
			}
		}
	}

	//set block threshold base on area max in the range
	b_threshold = 1000;
	if(max_b_area > b_threshold){
		b_threshold = max_b_area/4;	
	}
	std::cout<<"Block Threshold = "<<b_threshold<<std::endl;
	


	//detect RIGHT and 
	int diff = (int)block[0*block_size_l+2]-(int)block[0*block_size_l+0];
	int d_threshold = 40000;
	//first row 3 block case
	if(area_count>2 && block[0*block_size_l+0]>0 && block[0*block_size_l+1]>0 && block[0*block_size_l+2] >0 && block[2*block_size_l+0]>0 ){
		std::cout<<"TRACKER: right marker case 0"<<std::endl;
		result = 3;
	}
	else if(area_count>2 && block[0*block_size_l+0]>0 && block[0*block_size_l+1]>0 && block[0*block_size_l+2] >0 && block[2*block_size_l+2]>0 ){
		std::cout<<"TRACKER: left marker case 0"<<std::endl;
		result =  2;
	}
	else if(area_count>2 && block[0*block_size_l+0]>0 && block[0*block_size_l+1]>15000 && block[1*block_size_l+0] >0 && block[2*block_size_l+0]>0 && block[1*block_size_l+1]==0){
		std::cout<<"TRACKER: right marker case 0.5"<<std::endl;
		result = 3;
	}
	else if(area_count>2 && block[0*block_size_l+0]>0 && block[0*block_size_l+1]>0 && block[1*block_size_l+1] >0 && block[2*block_size_l+1]>0 && block[1*block_size_l+0]==0 ){
		std::cout<<"TRACKER: left marker case 0.5"<<std::endl;
		result =  2;
	}
	else if (area_count>2 && block[0*block_size_l+0]>0 && block[0*block_size_l+1]>0 && block[0*block_size_l+1]>0 && block[0*block_size_l+2]>0 && block[1*block_size_l+0] >0 && block[1*block_size_l+1]>15000 && block[1*block_size_l+2]==0){
		std::cout<<"TRACKER: right marker case 0.6"<<std::endl;
		result = 3;
	}
	else if(area_count >2 && block[0*block_size_l+0]>1000 && block[0*block_size_l+1]>1000 && block[0*block_size_l+2]>1000 && block[0*block_size_l+3]<=1000){
		if(diff>0){
			// if(diff<d_threshold+10000 ){
			// 	std::cout<<"TRACKER: right marker case 1:  diff "<< diff<<std::endl;
			// 	result =3;	
			// }else{
			// 	std::cout<<"TRACKER: left marker case 1: diff "<< diff<< std::endl;
			
			// 	result =2;
			// }
			if(diff<d_threshold ){
				std::cout<<"TRACKER: left marker case 1:  diff "<< diff<<std::endl;
				result =2;	
			}else{
				std::cout<<"TRACKER: right marker case 1: diff "<< diff<< std::endl;
			
				result =3;
			}
			// std::cout<<"TRACKER: right marker case 1: diff "<< diff<< std::endl;
			
			result =3;
		}else{
			if(diff<d_threshold ){
				std::cout<<"TRACKER: left marker case 1.5:  diff "<< diff<<std::endl;
				result =2;	
			}else{
				std::cout<<"TRACKER: right marker case 1.5: diff "<< diff<< std::endl;
			
				result =3;
			}
			
		}
	}
	//second row 3 block case
	else if(area_count >2 && block[1*block_size_l+0]>1000 && block[1*block_size_l+1]>1000 && block[1*block_size_l+2]>1000 && block[1*block_size_l+3]<=1000){
		diff = (int)block[1*block_size_l+0]-(int)block[1*block_size_l+2];
		if(diff>0){
			std::cout<<"TRACKER: left marker case 2: diff"<< block[1*block_size_l+2]-block[1*block_size_l+0]<< std::endl;
			result =2;
		}else{
			std::cout<<"TRACKER: right marker case 2:  diff"<< block[1*block_size_l+2]-block[1*block_size_l+0]<<std::endl;
			result =3;
		}
	}
	//first row 4 block case
	else if(area_count >2 && block[0*block_size_l+0]>1000 && block[0*block_size_l+1]>1000 && block[0*block_size_l+2]>1000 && block[0*block_size_l+3]>1000){
		diff = (int)block[0*block_size_l+1]-(int)block[0*block_size_l+2];
		if(diff>0){
			std::cout<<"TRACKER: left marker case 3: diff "<< diff<< std::endl;
			
			result =2;
		}else{
			std::cout<<"TRACKER: right marker case 3:  diff "<< diff<<std::endl;
			result =3;
		}
	}
	//second row 4 block case
	else if(area_count >2 && block[1*block_size_l+0]>1000 && block[1*block_size_l+1]>1000 && block[1*block_size_l+2]>1000 && block[1*block_size_l+3]>1000){
		diff = (int)block[1*block_size_l+1]-(int)block[1*block_size_l+2];
		if(diff>0){
			std::cout<<"TRACKER: left marker case 4: diff "<< diff<< std::endl;
			
			result =2;
		}else{
			std::cout<<"TRACKER: right marker case 4:  diff "<< diff<<std::endl;
			result =3;
		}
	}

	

	//FORWARD 1st col on and 2nd col off 
	else if(area_count >=2 && block[0*block_size_l+1]<=b_threshold && block[1*block_size_l+1]<=threshold && block[0*block_size_l+0]>= threshold && block[1*block_size_l+0]>= threshold ){
		std::cout<<"TRACKER: forward marker case 1"<<std::endl;
		result = 1;
	}
	//two col and 3rd col zero
	else if(area_count >=2 && block[0*block_size_l+0]>=b_threshold && block[0*block_size_l+1]>=b_threshold && block[1*block_size_l+1]>=threshold && block[1*block_size_l+0]>= threshold && block[0*block_size_l+2]<=threshold && block[1*block_size_l+2]<= threshold ){
		std::cout<<"TRACKER: forward marker case 2"<<std::endl;
		result = 1;
	}
	else if( area_count >=2 && block[0*block_size_l+0]>=b_threshold && block[1*block_size_l+0]>=b_threshold){
		std::cout<<"TRACKER: forward marker case 3"<<std::endl;
		result = 1;
	}
	else if(area_count >=2 && block[0*block_size_l+0]>=b_threshold && block[2*block_size_l+0]>=threshold && block[3*block_size_l+0]>= threshold && block[0*block_size_l+1]>= threshold && block[2*block_size_l+1]>= threshold && block[3*block_size_l+1]>= threshold){
		std::cout<<"TRACKER: forward marker case 4"<<std::endl;
		result = 1;
	}
	else if(area_count >=2 && block[0*block_size_l+0]>=b_threshold && block[2*block_size_l+0]>=threshold && block[0*block_size_l+1]>= threshold && block[2*block_size_l+1]>= threshold){
		std::cout<<"TRACKER: forward marker case 4"<<std::endl;
		result = 1;
	}
	else{
		result =0;
	}
	// else if()




	// std::cout<<"TRACKER: MARKER Q10: "<<Q10<<" Q11: "<<Q11<<" Q12: "<<Q12<<" Q13: "<<Q13<<" Q14: "<<Q14<<std::endl;
	// std::cout<<"IDEN: MARKER Q10: "<<Q10<<" Q11: "<<Q11<<" Q12: "<<Q12<<" Q13: "<<Q13<<" Q14: "<<Q14<<" Q15: "<<Q15<<" Q16: "<<Q16<<" Q17: "<<Q17<<" Q18: "<<Q18<<" Q19: "<<Q19<<" Q110: "<<Q110<<" Q111: "<<Q111<<" Q112: "<<Q112<<std::endl;

	// unsigned int Q20 = IntegralImageArea(integralBuffer,temp_rgb_line,q20, q21, q30,q31 );
	// unsigned int Q21 = IntegralImageArea(integralBuffer,temp_rgb_line,q21, q22, q31,q32 );
	// unsigned int Q22 = IntegralImageArea(integralBuffer,temp_rgb_line,q22, q23, q32,q33 );
	// unsigned int Q23 = IntegralImageArea(integralBuffer,temp_rgb_line,q23, q24, q33,q34 );
	// unsigned int Q24 = IntegralImageArea(integralBuffer,temp_rgb_line,q24, q25, q34,q35 );

	// std::cout<<"TRACKER: MARKER Q20: "<<Q20<<" Q21: "<<Q21<<" Q22: "<<Q22<<" Q23: "<<Q23<<" Q24: "<<Q24<<std::endl;

	// unsigned int Q30 = IntegralImageArea(integralBuffer,temp_rgb_line,q30, q31, q40,q41 );
	// unsigned int Q31 = IntegralImageArea(integralBuffer,temp_rgb_line,q31, q32, q41,q42 );
	// unsigned int Q32 = IntegralImageArea(integralBuffer,temp_rgb_line,q32, q33, q42,q43 );
	// unsigned int Q33 = IntegralImageArea(integralBuffer,temp_rgb_line,q33, q34, q43,q44 );
	// unsigned int Q34 = IntegralImageArea(integralBuffer,temp_rgb_line,q34, q35, q44,q45 );

	// std::cout<<"TRACKER: MARKER Q30: "<<Q30<<" Q31: "<<Q31<<" Q32: "<<Q32<<" Q33: "<<Q33<<" Q34: "<<Q34<<std::endl;

	// unsigned int Q40 = IntegralImageArea(integralBuffer,temp_rgb_line,q40, q41, q50,q51 );
	// unsigned int Q41 = IntegralImageArea(integralBuffer,temp_rgb_line,q41, q42, q51,q52 );
	// unsigned int Q42 = IntegralImageArea(integralBuffer,temp_rgb_line,q42, q43, q52,q53 );
	// unsigned int Q43 = IntegralImageArea(integralBuffer,temp_rgb_line,q43, q44, q53,q54 );
	// unsigned int Q44 = IntegralImageArea(integralBuffer,temp_rgb_line,q44, q45, q54,q55 );

	// std::cout<<"TRACKER: MARKER Q40: "<<Q40<<" Q41: "<<Q41<<" Q42: "<<Q42<<" Q43: "<<Q43<<" Q44: "<<Q44<<std::endl;


// //test 
// 	//test one pixel area
// 	unsigned int Q00 = IntegralImageArea(integralBuffer,temp_rgb_line,Point2D(0,0), Point2D(1,0), Point2D(0,1),Point2D(1,1) );
// 	//test more blocks
// 	unsigned int Q01 = IntegralImageArea(integralBuffer,temp_rgb_line,Point2D(0,0), Point2D(2,0), Point2D(0,2),Point2D(2,2) );
// 	//test subsection
// 	unsigned int Q02 = IntegralImageArea(integralBuffer,temp_rgb_line,Point2D(2,2), Point2D(3,2), Point2D(2,3),Point2D(3,3) );

// 	std::cout<<"IDEN: MARKER Q00: "<<Q00<<"IDEN: MARKER Q01: "<<Q01<<"IDEN: MARKER Q02: "<<Q02<<std::endl;





	//copy temp to rgb_line
	memcpy(src->m_ImageData, temp_rgb_line->m_ImageData, src->m_ImageSize);
	delete(temp_rgb_line);
	delete(integralBuffer);
	return result;
}

void ImgProcess::HFlipYUV(Image* img)
{
    int sizeline = img->m_Width * 2; /* 2 bytes per pixel*/
    unsigned char *pframe;
    pframe=img->m_ImageData;
    unsigned char line[sizeline-1];/*line buffer*/
    for (int h = 0; h < img->m_Height; h++)
    {   /*line iterator*/
        for(int w = sizeline-1; w > 0; w = w - 4)
        {   /* pixel iterator */
            line[w-1]=*pframe++;
            line[w-2]=*pframe++;
            line[w-3]=*pframe++;
            line[w]=*pframe++;
        }
        memcpy(img->m_ImageData+(h*sizeline), line, sizeline); /*copy reversed line to frame buffer*/
    }
}

void ImgProcess::VFlipYUV(Image* img)
{
    int sizeline = img->m_Width * 2; /* 2 bytes per pixel */
    unsigned char line1[sizeline-1];/*line1 buffer*/
    unsigned char line2[sizeline-1];/*line2 buffer*/
    for(int h = 0; h < img->m_Height/2; h++)
    {   /*line iterator*/
        memcpy(line1,img->m_ImageData+h*sizeline,sizeline);
        memcpy(line2,img->m_ImageData+(img->m_Height-1-h)*sizeline,sizeline);

        memcpy(img->m_ImageData+h*sizeline, line2, sizeline);
        memcpy(img->m_ImageData+(img->m_Height-1-h)*sizeline, line1, sizeline);
    }
}

// ***   WEBOTS PART  *** //

void ImgProcess::BGRAtoHSV(FrameBuffer *buf)
{
    int ir, ig, ib, imin, imax;
    int th, ts, tv, diffvmin;

    for(int i = 0; i < buf->m_BGRAFrame->m_Width*buf->m_BGRAFrame->m_Height; i++)
    {
        ib = buf->m_BGRAFrame->m_ImageData[4*i+0];
        ig = buf->m_BGRAFrame->m_ImageData[4*i+1];
        ir = buf->m_BGRAFrame->m_ImageData[4*i+2];

        if( ir > ig )
        {
            imax = ir;
            imin = ig;
        }
        else
        {
            imax = ig;
            imin = ir;
        }

        if( imax > ib ) {
            if( imin > ib ) imin = ib;
        } else imax = ib;

        tv = imax;
        diffvmin = imax - imin;

        if( (tv!=0) && (diffvmin!=0) )
        {
            ts = (255* diffvmin) / imax;
            if( tv == ir ) th = (ig-ib)*60/diffvmin;
            else if( tv == ig ) th = 120 + (ib-ir)*60/diffvmin;
            else th = 240 + (ir-ig)*60/diffvmin;
            if( th < 0 ) th += 360;
            th &= 0x0000FFFF;
        }
        else
        {
            tv = 0;
            ts = 0;
            th = 0xFFFF;
        }

        ts = ts * 100 / 255;
        tv = tv * 100 / 255;

        //buf->m_HSVFrame->m_ImageData[i]= (unsigned int)th | ((unsigned int)ts<<16) | ((unsigned int)tv<<24);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+0] = (unsigned char)(th >> 8);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+1] = (unsigned char)(th & 0xFF);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+2] = (unsigned char)(ts & 0xFF);
        buf->m_HSVFrame->m_ImageData[i*buf->m_HSVFrame->m_PixelSize+3] = (unsigned char)(tv & 0xFF);
    }
}

