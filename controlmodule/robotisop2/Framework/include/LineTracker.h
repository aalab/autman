/*
 *   LineTracker.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _LINE_TRACKER_H_
#define _LINE_TRACKER_H_

#include <string.h>

#include "Point.h"
#include "minIni.h"

namespace Robot
{
	class LineTracker
	{
	private:
		int NoBallCount;
		static const int NoBallMaxCount = 15;

	public:
        Point2D     ball_position;

		LineTracker();
		~LineTracker();

		void Process(Point2D pos);
	};
}

#endif
