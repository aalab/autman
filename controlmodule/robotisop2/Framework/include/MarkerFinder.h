/*
 * MarkerFinder.h
 *
 *  Created on: 2015. 4. 6.
 *      Author: Paul-Émile Crevier
 */

#ifndef MARKERFINDER_H_
#define MARKERFINDER_H_

// #include <string>

// #include "Point.h"
// #include "Image.h"
// #include "minIni.h"
#include "ColorFinder.h"

#define MARKER_SECTION   "Find Marker"
// #define INVALID_VALUE   -1024.0

namespace Robot
{
    class MarkerFinder : public ColorFinder
    {
		// private:
			// Point2D m_center_point;

			// void Filtering(Image* img);

		public:
			// int m_hue;             /* 0 ~ 360 */
			// int m_hue_tolerance;   /* 0 ~ 180 */
			// int m_min_saturation;  /* 0 ~ 100 */
			// int m_min_value;       /* 0 ~ 100 */
			// double m_min_percent;  /* 0.0 ~ 100.0 */
			// double m_max_percent;  /* 0.0 ~ 100.0 */
			// double test;
			
			std::string marker_section;

			// Image*  m_result;

			MarkerFinder();
			MarkerFinder(int hue, int hue_tol, int min_sat, int max_sat, int min_val, int max_val, double min_per, double max_per);
			virtual ~MarkerFinder();

			void LoadINISettings(minIni* ini);
			void LoadINISettings(minIni* ini, const std::string &section);
			void SaveINISettings(minIni* ini);
			void SaveINISettings(minIni* ini, const std::string &section);

			// Point2D& GetPosition(Image* hsv_img);
    };
}

#endif /* MARKERFINDER_H_ */
