//cycle_walking main
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier
// Description: This is a walking module that will perform the new walking 
// from the robot for the darwinop2

// #include <LinuxDARwIn.h>
// #include <iostream>
// #include <darwin/framework/Walking.h>
// #include <darwin/framework/Action.h>
// #include <darwin/framework/MotionStatus.h>

//I change something v02

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>
#include <math.h>
#include <espeak/speak_lib.h>

// #include "Walking.h"
#include "mjpg_streamer.h"
#include "LinuxDARwIn.h"
#include "MotionStatus.h"
#include "BufferToggle.cpp"
// #include "LinuxActionScript.h"

// #include "StatusCheck.h"
// #include "VisionMode.h"
// using namespace Robot;
using namespace std;



#ifdef MX28_1024
#define MOTION_FILE_PATH    "../../../Data/motion_1024.bin"
#else
#define MOTION_FILE_PATH    "../../../Data/motion_4096.bin"
#endif

#define INI_FILE_PATH       "../../../Data/config.ini"
#define SCRIPT_FILE_PATH    "script.asc"

#define U2D_DEV_NAME0       "/dev/ttyUSB0"
#define U2D_DEV_NAME1       "/dev/ttyUSB1"
LinuxCM730 linux_cm730(U2D_DEV_NAME0);
CM730 cm730(&linux_cm730);


void change_current_dir()
{
    char exepath[1024] = {0};
    if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
    {
        if(chdir(dirname(exepath)))
            fprintf(stderr, "chdir error!! \n");
    }
}

void sighandler(int sig)
{
    exit(0);
}
void walkInitilize(){
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
    Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
    Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
}

void walkForward(const long duration, const long step_size){
    Walking::GetInstance()->X_MOVE_AMPLITUDE = step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}
void walkBackward(const long duration, const long step_size){
    Walking::GetInstance()->X_MOVE_AMPLITUDE = -step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}
void walkLeft(const long duration, const long step_size){
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}
void walkRight(const long duration, const long step_size){
    Walking::GetInstance()->Y_MOVE_AMPLITUDE = -step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}
void walkRLeft(const long duration, const long step_size){
    Walking::GetInstance()->A_MOVE_AMPLITUDE = step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}
void walkRRight(const long duration, const long step_size){
    Walking::GetInstance()->A_MOVE_AMPLITUDE = -step_size;
    Walking::GetInstance()->Start();
    sleep(duration);
}

void HeadInitialize(){
  Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
    // Head::GetInstance()->Initialize();
}
void headHome(){

    HeadInitialize();
    Head::GetInstance()->MoveToHome();
}
void headUp(const double tilt_degree){
    double tilt = tilt_degree/M_PI;
    HeadInitialize();
    Head::GetInstance()->MoveByAngleOffset(0,tilt);
    Head::GetInstance()->Process();
}
void headDown(const double tilt_degree){
    double tilt = tilt_degree/M_PI;
    HeadInitialize();
    Head::GetInstance()->MoveByAngleOffset(0,-tilt);
    Head::GetInstance()->Process();
}
void headLeft(const double pan_degree){
    double pan = pan_degree/M_PI;
    HeadInitialize();
    Head::GetInstance()->MoveByAngleOffset(pan,0);
    Head::GetInstance()->Process();
}
void headRight(const double pan_degree){
    double pan = pan_degree/M_PI;
    HeadInitialize();
    Head::GetInstance()->MoveByAngleOffset(-pan,0);
    Head::GetInstance()->Process();
}


int main()
{
    printf( "\n===== Cycle walking for DARwIn OP2=====\n\n");
    int m_old_btn      = 0;

    
    double m_FBStep;
    double m_RLTurn;
    BufferToggle bt;

    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    signal(SIGQUIT, &sighandler);
    signal(SIGINT, &sighandler);

    change_current_dir();

    minIni* ini = new minIni(INI_FILE_PATH);
    
    cerr << "Initializing..." << endl;

    //////////////////// Framework Initialize ////////////////////////////
    if(MotionManager::GetInstance()->Initialize(&cm730) == false)
    {
        linux_cm730.SetPortName(U2D_DEV_NAME1);
        if(MotionManager::GetInstance()->Initialize(&cm730) == false)
        {
            printf("Fail to initialize Motion Manager!\n");
            return 0;
        }
    }


    Walking::GetInstance()->LoadINISettings(ini);

    cerr << "done" << endl << endl;
    
    
    
    cerr << "Setting up Walking module..." << endl;
    MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());
    MotionManager::GetInstance()->AddModule((MotionModule*)Head::GetInstance());
    MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    LinuxMotionTimer *motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
    motion_timer->Start();
    /////////////////////////////////////////////////////////////////////


    MotionManager::GetInstance()->LoadINISettings(ini);


    int firm_ver = 0;
    if(cm730.ReadByte(JointData::ID_HEAD_PAN, MX28::P_VERSION, &firm_ver, 0)  != CM730::SUCCESS)
    {
        fprintf(stderr, "Can't read firmware version from Dynamixel ID %d!! \n\n", JointData::ID_HEAD_PAN);
        exit(0);
    }

    if(0 < firm_ver && firm_ver < 27)
    {
#ifdef MX28_1024
        Action::GetInstance()->LoadFile(MOTION_FILE_PATH);
#else
        fprintf(stderr, "MX-28's firmware is not support 4096 resolution!! \n");
        fprintf(stderr, "Upgrade MX-28's firmware to version 27(0x1B) or higher.\n\n");
        exit(0);
#endif
    }
    else if(27 <= firm_ver)
    {
#ifdef MX28_1024
        fprintf(stderr, "MX-28's firmware is not support 1024 resolution!! \n");
        fprintf(stderr, "Remove '#define MX28_1024' from 'MX28.h' file and rebuild.\n\n");
        exit(0);
#else
        Action::GetInstance()->LoadFile((char*)MOTION_FILE_PATH);
#endif
    }
    else
        exit(0);

    

    Action::GetInstance()->m_Joint.SetEnableBody(true, true);
    MotionManager::GetInstance()->SetEnable(true);


    cm730.WriteByte(CM730::P_LED_PANNEL, 0x01|0x02|0x04, NULL);
/////////////////////////////////////////////////////////////////////
    LinuxActionScript::PlayMP3("../../../Data/mp3/Demonstration ready mode.mp3");

    Action::GetInstance()->Start(15);

    while(Action::GetInstance()->IsRunning()){
        usleep(8*1000);
        // Action::GetInstance()->Stop();
    } 

    // Action::GetInstance()->m_Joint.SetEnableBody(false, false);
    // MotionManager::GetInstance()->SetEnable(false);


    printf("Press the ENTER key to begin!\n");
    bt.on();
    getchar();


    cerr << "Standing up..." << endl;
    Action::GetInstance()->Start(1);

    while(Action::GetInstance()->IsRunning()){
        usleep(8*1000);
        // Action::GetInstance()->Stop();
    } 

    // int n = 0;
    // int param[JointData::NUMBER_OF_JOINTS * 5];
    // int wGoalPosition, wStartPosition, wDistance;

    // for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
    // {
    //     wStartPosition = MotionStatus::m_CurrentJoints.GetValue(id);
    //     wGoalPosition = Walking::GetInstance()->m_Joint.GetValue(id);
    //     if( wStartPosition > wGoalPosition )
    //         wDistance = wStartPosition - wGoalPosition;
    //     else
    //         wDistance = wGoalPosition - wStartPosition;

    //     wDistance >>= 2;
    //     if( wDistance < 8 )
    //         wDistance = 8;

    //     param[n++] = id;
    //     param[n++] = CM730::GetLowByte(wGoalPosition);
    //     param[n++] = CM730::GetHighByte(wGoalPosition);
    //     param[n++] = CM730::GetLowByte(wDistance);
    //     param[n++] = CM730::GetHighByte(wDistance);
    // }
    // cm730.SyncWrite(MX28::P_GOAL_POSITION_L, 5, JointData::NUMBER_OF_JOINTS - 1, param);    

    LinuxActionScript::PlayMP3("../../../Data/mp3/balalala.mp3");

    cerr << "done" << endl << endl;

//////////////////////////////////////////////////////////////




    usleep(8*1000);


    //For walking
// MotionManager::GetInstance()->SetEnable(true);
    if(Action::GetInstance()->IsRunning() == 0)
    {
      
    // MotionManager::GetInstance()->SetEnable(true);



        cerr << "Enter detected" << endl << endl;

        char temp;
        bt.off();
        while(1)
        {


            m_FBStep = 20;
            m_RLTurn = 0;


        // bt.off();
            temp=std::getchar() ; 
            cout<<""<<endl;

        // cout<< ""<<endl;
            //walking 


            if(temp == 'w'){
          // cout<<"w pressed"<<endl;
                  walkInitilize();
                walkForward(1,15);

            }
            else if(temp == 's'){
          // cout<<"w pressed"<<endl;
                walkInitilize();
                walkBackward(1,15);

            }
            else if(temp == 'a'){
          // cout<<"w pressed"<<endl;
                walkInitilize();
                walkLeft(1,15);

            }
            else if(temp == 'd'){
          // cout<<"w pressed"<<endl;
                walkInitilize();
                walkRight(1,15);

            }
            else if(temp == 'q'){
          // cout<<"w pressed"<<endl;
                walkInitilize();
                walkRLeft(1,15);

            }
            else if(temp == 'e'){
          // cout<<"w pressed"<<endl;
                walkInitilize();
                walkRRight(1,15);

            }
            else if(temp == 32){
              cout<<"space bar pressed"<<endl;
              headHome();

          }

          else if(temp == 'o'){
          // cout<<"up pressed"<<endl;
            headDown(5);

        }
        else if(temp == 'l'){
          // cout<<"down pressed"<<endl;
            headUp(5);
        }
        else if(temp == 'k'){
          // cout<<"up pressed"<<endl;
            headLeft(5);

        }
        else if(temp == ';'){
          // cout<<"down pressed"<<endl;
            headRight(5);
        }
        // bt.off();


        if(Walking::GetInstance()->IsRunning() ==1 ){
            // cout<<"STOP called"<<endl;
            Walking::GetInstance()->Stop();   
            sleep(1);

        }

        // }
        
        // LinuxActionScript::ScriptStart(SCRIPT_FILE_PATH);
    }        


}
return 0;

}




