//cycle_walking main
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier
// Description: This is a walking module that will perform the new walking 
// from the robot for the darwinop2 and able to control by the keyboard
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>

// #include "MotionStatus.h"
#include "LinuxDARwIn.h"
#include "TeleOperation.h"

#include "BufferToggle.cpp"

using namespace std;
int main()
{
    long test_m =1;
    bool enter_pressed = false;
	// TeleOperation* teleOp = new TeleOperation();
	
	TeleOperation::GetInstance()->Initialize();

    BufferToggle bt;
    printf("Press the ENTER key to begin!\n");
    bt.on();
    getchar();

    TeleOperation::GetInstance()->StandUp();

    usleep(8*1000);


    cerr << "Enter detected" << endl;

    char temp;
    bt.off();
//     while(1)
//     {



//         // bt.off();
//         temp=std::getchar() ; 
//         cout<<""<<endl;


//         if(temp == 'w'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->WalkForward(1,15);

//         }
//         else if(temp == 's'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->WalkBackward(1,15);

//         }
//         else if(temp == 'a'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->WalkLeft(1,15);

//         }
//         else if(temp == 'd'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->WalkRight(1,15);

//         }
//         else if(temp == 'q'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->RotateLeft(1,15);

//         }
//         else if(temp == 'e'){
//           // cout<<"w pressed"<<endl;
//             teleOp->WalkInitialize();
//             teleOp->RotateRight(1,15);

//         }
//         else if(temp == 32){
//           cout<<"space bar pressed"<<endl;
//           teleOp->ResetHeadPosition();

//         }

//           else if(temp == 'o'){
//               // cout<<"up pressed"<<endl;
//             teleOp->TiltHeadDown(5);

//         }
//         else if(temp == 'l'){
//               // cout<<"down pressed"<<endl;
//             teleOp->TiltHeadUp(5);
//         }
//         else if(temp == 'k'){
//               // cout<<"up pressed"<<endl;
//             teleOp->PanHeadLeft(5);

//         }
//         else if(temp == ';'){
//               // cout<<"down pressed"<<endl;
//             teleOp->PanHeadRight(5);
//         }
//         else if(temp == '\n'){
//             if(enter_pressed == false){
//             //   cout<<"enter pressed"<<endl;
//               teleOp->SitDown();  
//             }else{
//               teleOp->StandUp();
//             }

//             // ActionInitialize();
//             // teleOp->ActionTestmotion(test_m);
//             enter_pressed = !enter_pressed;
//             // test_m++;
              
//         }
//         else if(temp == '+'){
//             // if(enter_pressed == false){
//             //   cout<<"enter pressed"<<endl;
//             //   teleOp->SitDown();  
//             // }else{
//             //   teleOp->StandUp();
//             // }

//             // ActionInitialize();
//             test_m++;
//             teleOp->ActionTestmotion(test_m);
//             // enter_pressed = !enter_pressed;
            
              
//         }
//         else if(temp == '-'){
//             // if(enter_pressed == false){
//             //   cout<<"enter pressed"<<endl;
//             //   teleOp->SitDown();  
//             // }else{
//             //   teleOp->StandUp();
//             // }

//             // ActionInitialize();
//             test_m--;
//             teleOp->ActionTestmotion(test_m);
//             // enter_pressed = !enter_pressed;
            
//         }
//         else if(temp == '0'){
//             // if(enter_pressed == false){
//             //   cout<<"enter pressed"<<endl;
//             //   teleOp->SitDown();  
//             // }else{
//             //   teleOp->StandUp();
//             // }

//             // ActionInitialize();
//             // test_m--;
//             teleOp->ActionTestmotion(test_m);
//             // enter_pressed = !enter_pressed;
            
//         }




// }


    return 0;
}
//1  bend and unbend knee      
//2  head nod
//3  shake head
//4 bow
//5 look at the right
//6 hand shake right left
//9 going into walking position
//10 kinda jumping 
//11 getup from behind
//12 Right kick
//13 Left kick
//15 Sit down
//16 Standup and ready to walk
//17 Handstand and try to get up
//18 getup from front with stand position
//19 unknown with the other side with stand back
//23 Yes!
//24 Wow
//25 Wowonly on one side
//27 Oops
//29 like 2 but uncomplete
//30 headnod slowly with angle
//31 humping 
//38 Queen Hi
//39 Same as 18 but faster 
//41 Introducing
//42 More 41
//43 Slower 42
//44 More from the other side like 43
//45 like 44
//46 Similar with 45
//47 Other side like 46
//54 Clap Please!!!
//55 same as 54 but faster 
//56 crazy fast 55 (DON't RUN)
//57 CARZY CRAZY like 56 (DON't RUN!!!)
//70 Side Kick
//71 the other side kick
//90 layddown
//91 layback




