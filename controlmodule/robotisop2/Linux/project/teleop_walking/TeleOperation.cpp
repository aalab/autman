// TeleOperation.cpp
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier
// Description: This is a walking module that will perform the new walking 
// from the robot for the darwinop2

// #include <LinuxDARwIn.h>
// #include <iostream>
// #include <darwin/framework/Walking.h>
// #include <darwin/framework/Action.h>
// #include <darwin/framework/MotionStatus.h>

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>
#include <math.h>
// #include <espeak/speak_lib.h>

// #include "Walking.h"
#include "mjpg_streamer.h"
#include "LinuxDARwIn.h"
#include "MotionStatus.h"
// #include "BufferToggle.cpp"
#include "TeleOperation.h"
#include "Voice.h"
// #include "LinuxActionScript.h"

// #include "StatusCheck.h"
// #include "VisionMode.h"
// using namespace Robot;

#ifdef MX28_1024
#define MOTION_FILE_PATH    "../../../Data/motion_1024.bin"
#else
#define MOTION_FILE_PATH    "../../../Data/motion_4096.bin"
#endif

#define INI_FILE_PATH       "../../../Data/config.ini"
#define SCRIPT_FILE_PATH    "script.asc"

#define U2D_DEV_NAME0       "/dev/ttyUSB0"
#define U2D_DEV_NAME1       "/dev/ttyUSB1"
LinuxCM730 linux_cm730(U2D_DEV_NAME0);
CM730 cm730(&linux_cm730);

int gID = CM730::ID_CM; //to release motor

using namespace std;

TeleOperation* TeleOperation::m_UniqueInstance = new TeleOperation();
int TeleOperation::value;
int TeleOperation::GFB;
int TeleOperation::GRL;
int TeleOperation::AFB;
int TeleOperation::ARL;
int TeleOperation::Btn;

void change_current_dir()
{
	char exepath[1024] = {0};
	if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
	{
		if(chdir(dirname(exepath)))
			fprintf(stderr, "chdir error!! \n");
	}
}

void sighandler(int sig)
{
	exit(0);
}

TeleOperation::TeleOperation(){}

int TeleOperation::Initialize()
{
	printf( "\nTELEOP: ===== Tele Operation for DARwIn OP2 =====\n\n");
	// int m_old_btn      = 0;
	
	// double m_FBStep;
	// double m_RLTurn;
	// BufferToggle bt;

	signal(SIGABRT, &sighandler);
	signal(SIGTERM, &sighandler);
	signal(SIGQUIT, &sighandler);
	signal(SIGINT, &sighandler);

	change_current_dir();

	minIni* ini = new minIni(INI_FILE_PATH);
	
	cerr << "TELEOP: Initializing..." << endl;

	//loop until it connect
	long tryConnect =1;
	//////////////////// Framework Initialize ////////////////////////////
	while(MotionManager::GetInstance()->Initialize(&cm730) == false)
	{
		linux_cm730.SetPortName(U2D_DEV_NAME1);
		if(MotionManager::GetInstance()->Initialize(&cm730) == false)
		{
			printf("TELEOP: Fail to initialize Motion Manager!\n");
			return 0;
		}

		cerr << "TELEOP: Trying to connect to CM730" <<tryConnect << endl << endl;
		sleep(1);
	}


	Walking::GetInstance()->LoadINISettings(ini);

	cerr << "TELEOP: done" << endl << endl;
	
	
	
	cerr << "TELEOP: Setting up Walking module..." << endl;
	MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());
	MotionManager::GetInstance()->AddModule((MotionModule*)Head::GetInstance());
	MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
	cerr <<"TELEOP: Setting up motion_timer"<<endl;
	LinuxMotionTimer *motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
	

	motion_timer->Start();
		cerr <<"TELEOP: Motion_timer Start"<<endl;
	/////////////////////////////////////////////////////////////////////


	MotionManager::GetInstance()->LoadINISettings(ini);


	int firm_ver = 0;
	if(cm730.ReadByte(JointData::ID_HEAD_PAN, MX28::P_VERSION, &firm_ver, 0)  != CM730::SUCCESS)
	{
		fprintf(stderr, "TELEOP: Can't read firmware version from Dynamixel ID %d!! \n\n", JointData::ID_HEAD_PAN);
		exit(0);
	}

	if(0 < firm_ver && firm_ver < 27)
	{
#ifdef MX28_1024
		Action::GetInstance()->LoadFile(MOTION_FILE_PATH);
#else
		fprintf(stderr, "TELEOP: MX-28's firmware is not support 4096 resolution!! \n");
		fprintf(stderr, "TELEOP: Upgrade MX-28's firmware to version 27(0x1B) or higher.\n\n");
		exit(0);
#endif
	}
	else if(27 <= firm_ver)
	{
#ifdef MX28_1024
		fprintf(stderr, "TELEOP: MX-28's firmware is not support 1024 resolution!! \n");
		fprintf(stderr, "TELEOP: Remove '#define MX28_1024' from 'MX28.h' file and rebuild.\n\n");
		exit(0);
#else
		Action::GetInstance()->LoadFile((char*)MOTION_FILE_PATH);
#endif
	}
	else
		exit(0);

	

	Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	MotionManager::GetInstance()->SetEnable(true);

    // cm730.WriteWord(JointData::ID_R_SHOULDER_PITCH, MX28::P_TORQUE_ENABLE, 0, 0);
    // cm730.WriteWord(JointData::ID_R_SHOULDER_ROLL,  MX28::P_TORQUE_ENABLE, 0, 0);
    // cm730.WriteWord(JointData::ID_R_ELBOW,          MX28::P_TORQUE_ENABLE, 0, 0);

    // cm730.WriteByte(JointData::ID_L_SHOULDER_PITCH, MX28::P_P_GAIN, 8, 0);
    // cm730.WriteByte(JointData::ID_L_SHOULDER_ROLL,  MX28::P_P_GAIN, 8, 0);
    // cm730.WriteByte(JointData::ID_L_ELBOW,          MX28::P_P_GAIN, 8, 0);


	// cm730.WriteByte(CM730::P_LED_PANNEL, 0x01|0x02|0x04, NULL);
/////////////////////////////////////////////////////////////////////
	

	Action::GetInstance()->Start(15);

	cout<<"Mp3 ran"<<endl;
	while(Action::GetInstance()->IsRunning()){
		usleep(8*1000);
		// Action::GetInstance()->Stop();
	} 
	cout<<"Initialization Complete"<<endl;

	// Action::GetInstance()->m_Joint.SetEnableBody(false, false);
	// MotionManager::GetInstance()->SetEnable(false);
	Voice::Initialize("en-us+m7");
	Voice::Voice::Speak("Ready to Love All Humans");
	// LinuxActionScript::PlayMP3("../../../Data/mp3/Demonstration ready mode.mp3");
	return 0;
}

void TeleOperation::ActionInitialize(){
	Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	MotionManager::GetInstance()->SetEnable(true);
}
void TeleOperation::ActionTestmotion(const long test_m){
	ActionInitialize();
    Action::GetInstance()->Start(test_m);

    while(Action::GetInstance()->IsRunning())
    {
        usleep(8*1000);
    }
    cerr << "TELEOP: Testmotion: " << test_m<< endl;
}

void TeleOperation::StandUp()
{
	CheckStatus();
	// sleep(1);
	if(AFB<400){
		ActionInitialize();
		cerr << "TELEOP: Standing up...from the front" << endl;
		Action::GetInstance()->Start(18);

		while(Action::GetInstance()->IsRunning())
		{
			usleep(8*1000);
		}

		LinuxActionScript::PlayMP3("../../../Data/mp3/Wow.mp3");

	}
	else if(AFB>600){
		ActionInitialize();
		cerr << "TELEOP: Standing up...from the back" << endl;
		Action::GetInstance()->Start(11);

		while(Action::GetInstance()->IsRunning())
		{
			usleep(8*1000);
		}
		LinuxActionScript::PlayMP3("../../../Data/mp3/Wow.mp3");

	}
	else{
		// cm730.WriteByte(CM730::P_LED_PANNEL, 0x01|0x02|0x04, NULL);
		ActionInitialize();
		cerr << "TELEOP: Standing up..." << endl;
		Action::GetInstance()->Start(16);

		while(Action::GetInstance()->IsRunning())
		{
			usleep(8*1000);
		}
		LinuxActionScript::PlayMP3("../../../Data/mp3/balalala.mp3");
		cerr << "TELEOP: standup done" << endl << endl;
	}




	
}

void TeleOperation::SitDown()
{
	ActionInitialize();
	Action::GetInstance()->Start(15);

	while(Action::GetInstance()->IsRunning())
	{
		usleep(8*1000);
	}
	LinuxActionScript::PlayMP3("../../../Data/mp3/low_Battery.mp3");
	cerr << "TELEOP: sitdown and ready" << endl << endl;

}

void TeleOperation::WalkInitialize()
{
	Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
	Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
}

void TeleOperation::StopWalking()
{
	if(Walking::GetInstance()->IsRunning() ==1 )
	{
		// cout<<"STOP called"<<endl;
		Walking::GetInstance()->Stop();   
		sleep(1);
      }
}

void TeleOperation::WalkForward(const long duration, const long step_size)
{
	Walking::GetInstance()->X_MOVE_AMPLITUDE = step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::WalkBackward(const long duration, const long step_size)
{
	Walking::GetInstance()->X_MOVE_AMPLITUDE = -step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::WalkLeft(const long duration, const long step_size)
{
	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::WalkRight(const long duration, const long step_size)
{
	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
	Walking::GetInstance()->Y_MOVE_AMPLITUDE = -step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::RotateLeft(const long duration, const long step_size)
{
	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
	Walking::GetInstance()->A_MOVE_AMPLITUDE = step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::RotateRight(const long duration, const long step_size)
{
	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
	Walking::GetInstance()->A_MOVE_AMPLITUDE = -step_size;
	Walking::GetInstance()->Start();
	sleep(duration);
	StopWalking();
}

void TeleOperation::HeadInitialize()
{
  Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
	// Head::GetInstance()->Initialize();
}

void TeleOperation::ResetHeadPosition()
{
	HeadInitialize();
	Head::GetInstance()->MoveToHome();
}

void TeleOperation::TiltHeadUp(const double tilt_degree)
{
	double tilt = tilt_degree/M_PI;
	HeadInitialize();
	Head::GetInstance()->MoveByAngleOffset(0,tilt);
	Head::GetInstance()->Process();
}

void TeleOperation::TiltHeadDown(const double tilt_degree)
{
	double tilt = tilt_degree/M_PI;
	HeadInitialize();
	Head::GetInstance()->MoveByAngleOffset(0,-tilt);
	Head::GetInstance()->Process();
}

void TeleOperation::PanHeadLeft(const double pan_degree)
{
	double pan = pan_degree/M_PI;
	HeadInitialize();
	Head::GetInstance()->MoveByAngleOffset(pan,0);
	Head::GetInstance()->Process();
}

void TeleOperation::PanHeadRight(const double pan_degree)
{
	double pan = pan_degree/M_PI;
	HeadInitialize();
	Head::GetInstance()->MoveByAngleOffset(-pan,0);
	Head::GetInstance()->Process();
}	
void TeleOperation::CheckStatus(){
	printf("\r");

		printf("TELEOP: GFB:");
		if(cm730.ReadWord(CM730::ID_CM, CM730::P_GYRO_Y_L, &GFB, 0) == CM730::SUCCESS)
			printf("%3d", GFB);
		else
			printf("---");

		printf(" GRL:");
		if(cm730.ReadWord(CM730::ID_CM, CM730::P_GYRO_X_L, &GRL, 0) == CM730::SUCCESS)
			printf("%3d", GRL);
		else
			printf("---");

		printf(" AFB:");
		if(cm730.ReadWord(CM730::ID_CM, CM730::P_ACCEL_Y_L, &AFB, 0) == CM730::SUCCESS)
			printf("%3d", AFB);
		else
			printf("----");

		printf(" ARL:");
		if(cm730.ReadWord(CM730::ID_CM, CM730::P_ACCEL_X_L, &ARL, 0) == CM730::SUCCESS)
			printf("%3d", ARL);
		else
			printf("----");

		printf(" BTN:");
		if(cm730.ReadWord(CM730::ID_CM, CM730::P_BUTTON, &Btn, 0) == CM730::SUCCESS)
			printf("%1d", Btn);
		else
			printf("----");

		// printf(" ID[%d]:", JointData::ID_R_SHOULDER_PITCH);
		// if(cm730.ReadWord(JointData::ID_R_SHOULDER_PITCH, MX28::P_PRESENT_POSITION_L, &value, 0) == CM730::SUCCESS)
		// {
		// 	printf("%4d", value);
		// 	cm730.WriteWord(JointData::ID_L_SHOULDER_PITCH, MX28::P_GOAL_POSITION_L, MX28::GetMirrorValue(value), 0);
		// }
		// else
		// 	printf("----");

		// printf(" ID[%d]:", JointData::ID_R_SHOULDER_ROLL);
		// if(cm730.ReadWord(JointData::ID_R_SHOULDER_ROLL, MX28::P_PRESENT_POSITION_L, &value, 0) == CM730::SUCCESS)
		// {
		// 	printf("%4d", value);
		// 	cm730.WriteWord(JointData::ID_L_SHOULDER_ROLL, MX28::P_GOAL_POSITION_L, MX28::GetMirrorValue(value), 0);
		// }
		// else
		// 	printf("----");

		// printf(" ID[%d]:", JointData::ID_R_ELBOW);
		// if(cm730.ReadWord(JointData::ID_R_ELBOW, MX28::P_PRESENT_POSITION_L, &value, 0) == CM730::SUCCESS)
		// {
		// 	printf("%4d", value);
		// 	cm730.WriteWord(JointData::ID_L_ELBOW, MX28::P_GOAL_POSITION_L, MX28::GetMirrorValue(value), 0);
		// }
		// else
		// 	printf("----");

		// if(cm730.ReadWord(CM730::ID_CM, CM730::P_LED_HEAD_L, &value, 0) == CM730::SUCCESS)
		// {
		// 	if(value == 0x7FFF)
		// 		value = 0;
		// 	else
		// 		value++;

		// 	cm730.WriteWord(CM730::P_LED_HEAD_L, value, 0);
		// }

		// if(cm730.ReadWord(CM730::ID_CM, CM730::P_LED_EYE_L, &value, 0) == CM730::SUCCESS)
		// {
		// 	if(value == 0)
		// 		value = 0x7FFF;
		// 	else
		// 		value--;

		// 	cm730.WriteWord(CM730::P_LED_EYE_L, value, 0);
		// }

		usleep(50000);

}
void TeleOperation::ReleaseMotor(){
	Action::GetInstance()->m_Joint.SetEnableBody(false, false);
	MotionManager::GetInstance()->SetEnable(false);
	  Head::GetInstance()->m_Joint.SetEnableHeadOnly(false, false);
	//All above resources have to be release to write byte to CM730
	// cm730.WriteByte(CM730::P_LED_EYE_L, 0xFF|0x02|0x04, NULL);
	for(int i=JointData::ID_R_SHOULDER_PITCH; i<JointData::NUMBER_OF_JOINTS; i++){
							cm730.WriteByte(i, MX28::P_TORQUE_ENABLE, 0, 0);
	}
	
	// if(cm730.ReadWord(CM730::ID_CM, CM730::P_LED_EYE_L, &value, 0) == CM730::SUCCESS)
	// {
	// 		if(value == 0)
	// 			// value = 0x7FFF;
	// 			value =0;
	// 			// value = 0x07|0xFF|0x02|0x04;
	// 		else
	// 			value--;

	// 		cm730.WriteWord(CM730::P_LED_EYE_L, value, 0);
	// }
	// Voice::Initialize();
	// Voice::Speak("Test");
	usleep(50000);
		
}