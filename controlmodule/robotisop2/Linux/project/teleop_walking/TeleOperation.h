// TeleOperation.h
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier

#ifndef __TELEOPERATION_H__
#define __TELEOPERATION_H__

class TeleOperation
{
	public:

		TeleOperation();
		
		static TeleOperation* GetInstance(){return m_UniqueInstance;}
	    static int Initialize();
		static void StandUp();
		static void SitDown();
		static void ActionInitialize();
		static void ActionTestmotion(const long test_m);
		static void WalkInitialize();
		static void WalkForward(const long duration, const long step_size);
		static void WalkBackward(const long duration, const long step_size);
		static void WalkLeft(const long duration, const long step_size);
		static void WalkRight(const long duration, const long step_size);
		static void RotateLeft(const long duration, const long step_size);
		static void RotateRight(const long duration, const long step_size);
		static void HeadInitialize();
		static void ResetHeadPosition();
		static void TiltHeadUp(const double tilt_degree);
		static void TiltHeadDown(const double tilt_degree);
		static void PanHeadLeft(const double pan_degree);
		static void PanHeadRight(const double pan_degree);
		static void CheckStatus();
		static void ReleaseMotor();
	private:
		static TeleOperation* m_UniqueInstance;
		static void StopWalking();
		static int value; //to see status of orientation
		static int GFB;
		static int GRL;
		static int AFB;
		static int ARL;
		static int Btn;
};

#endif /* __TELEOPERATION_H__ */



