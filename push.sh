#!/bin/bash
scriptpath="`dirname \"$0\"`"
scriptpath="`( cd \"$scriptpath\" && pwd )`"

base="~/aalab"

writeHelp()
{
	echo "$0 <to> [options]"
	echo ""
	echo "Options:"
	echo "<to>            : RSync connection URI of the computer where it should be pushed"
	echo "--base          : Changes the folder where the files will be copied"
	echo "--no-autman     : Flag to not rsync the autman project"
	echo "--no-vm         : Flag to not rsync the vison module"
	echo "--no-darwin     : Flag to not rsync the darwin robot server"
	echo "--no-teleop     : Flag to not rsync the teleop module"
	echo "--no-libs       : Flag to not rsync the ~/libs folder"
}

while test $# -gt 0; do
	case "$1" in
		--no-vm) vm=1 ;;
		--no-autman) autman=1 ;;
		--no-darwin) darwin=1 ;;
		--no-teleop) teleop=1 ;;
		--no-libs) libs=1 ;;
		--base)
			shift
			base=$1
		;;
		-h)
			writeHelp
			exit 0
		;;
		--help)
			writeHelp
			exit 0
		;;
		*)
			if [ -z $to ] ; then
				to=$1
			else
				echo "Unexpected option ${flag}"
				exit 1
			fi
		;;
	esac
	shift
done

if [ -z $to ] ; then
	echo "Param <to> could not be found"
	exit 2
fi

if [ -z $base ] ; then
	echo "Param <base> could not be found"
	exit 3
fi

rsync="rsync -aAxv"
$rsync ${scriptpath}/config $to:$base

if [ -z $vm ] ; then
	ssh $to mkdir -p $base/autman/bin/debug
	$rsync ${scriptpath}/autman/bin/debug $to:$base/autman/bin \
		--exclude="CMakeFiles" \
		--exclude="cmake_install.cmake" \
		--exclude="CMakeCache.txt" \
		--exclude="imumodule" \
		--exclude="Include" \
		--exclude="jsoncpp" \
		--exclude="*.a" \
		--exclude="__pycache__" \
		--exclude="robot_test"

	$rsync ${scriptpath}/autman/Configs $to:$base/autman
fi

if [ -z $vm ] ; then
	ssh $to mkdir -p $base/visionmodule/bin
	$rsync ${scriptpath}/visionmodule/bin/debug $to:$base/visionmodule/bin \
		--exclude="CMakeFiles" \
		--exclude="cmake_install.cmake" \
		--exclude="CMakeCache.txt"
fi

if [ -z $darwin ] ; then
	ssh $to mkdir -p $base/darwin/bin
	$rsync ${scriptpath}/darwin/bin/debug $to:$base/darwin/bin \
		--exclude="Makefile" \
		--exclude="CMakeFiles" \
		--exclude="cmake_install.cmake" \
		--exclude="CMakeCache.txt"
fi

if [ -z $teleop ] ; then
	ssh $to mkdir -p $base/modules/teleop/bin
	$rsync ${scriptpath}/modules/teleop/bin/debug $to:$base/modules/teleop/bin \
		--exclude="brain" \
		--exclude="include" \
		--exclude="jsoncpp" \
		--exclude="services" \
		--exclude="tsai2d" \
		--exclude="Makefile" \
		--exclude="CMakeFiles" \
		--exclude="cmake_install.cmake" \
		--exclude="CMakeCache.txt"
fi


if [ -z $libs ] ; then
	ssh $to mkdir -p "~/libs"
	$rsync ~/libs "$to:~/"
fi
