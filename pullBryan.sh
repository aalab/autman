#!/bin/bash
echo You are copying from $1.

scriptpath="`dirname \"$0\"`"
scriptpath="`( cd \"$scriptpath\" && pwd )`"
sourcedir=/Users/Kamp/Projects/aalab
cd $scriptpath

rsync -avh Kamp@$1:$sourcedir/$offsetdir \
    --exclude "visionmodule/bin/debug/www" \
    --exclude "visionmodule/bin/debug/CMakeFiles" \
    --exclude "visionmodule/bin/debug/cmake_install.cmake" \
    --exclude "visionmodule/bin/debug/CMakeCache.txt" \
    --exclude "visionmodule/bin/debug/Makefile" \
    --exclude "visionmodule/bin/debug/jsoncpp" \
    --exclude "visionmodule/bin/debug/tsai2d" \
    --exclude "visionmodule/bin/debug/libtsai2d.a" \
    --exclude "visionmodule/bin/debug/find_*" \
    --exclude "visionmodule/bin/debug/bulk_*" \
    --exclude "visionmodule/bin/debug/vision_module" \
    --exclude "visionmodule/bin/debug/ppms" \
    --exclude "visionmodule/bin/debug/services" \
    --exclude "visionmodule/bin/debug/www.sh" \
    --exclude "visionmodule/bin/debug/*_find_grid" \
    --exclude "visionmodule/bin/debug/src" \
    --exclude "controlmodule/bin/debug/CMakeFiles" \
    --exclude "controlmodule/bin/debug/cmake_install.cmake" \
    --exclude "controlmodule/bin/debug/CMakeCache.txt" \
    --exclude "controlmodule/bin/debug/Makefile" \
    --exclude "autman/bin/debug/CMakeFiles" \
    --exclude "autman/bin/debug/cmake_install.cmake" \
    --exclude "autman/bin/debug/CMakeCache.txt" \
    --exclude "autman/bin/debug/Makefile" \
    --exclude "autman/bin/debug/Include/config.h" \
    --exclude "autman/bin/debug/imumodule/CMakeFiles" \
    --exclude "autman/bin/debug/imumodule/cmake_install.cmake" \
    --exclude "autman/bin/debug/imumodule/Makefile" \
    --exclude "autman/bin/debug/imumodule/imu_module" \
    --exclude "autman/bin/debug/imumodule/include/config.h" \
    --exclude "autman/bin/debug/jsoncpp" \
    --exclude "autman/bin/debug/robocup_test" \
    --exclude "autman/bin/debug/robot_server" \
    \
    --exclude "darwin/bin/debug/CMakeFiles" \
    --exclude "darwin/bin/debug/cmake_install.cmake" \
    --exclude "darwin/bin/debug/CMakeCache.txt" \
    --exclude "darwin/bin/debug/Makefile" \
    --exclude "darwin/bin/debug/services" \
    --exclude "darwin/bin/debug/libdarwin" \
    --exclude "darwin/bin/debug/include" \
    --exclude "darwin/bin/debug/darwin_server" \
    --exclude "darwin/bin/debug/.gitignore" \
    \
    --exclude "modules/teleop/bin/debug/.gitignore" \
    --exclude "modules/teleop/bin/debug/Makefile" \
    --exclude "modules/teleop/bin/debug/CMakeFiles" \
    --exclude "modules/teleop/bin/debug/cmake_install" \
    --exclude "modules/teleop/bin/debug/CMakeCache.txt" \
    --exclude "modules/teleop/bin/debug/cmake_install.cmake" \
    --exclude "modules/teleop/bin/debug/teleop" \
    --exclude "modules/teleop/bin/debug/brain" \
    --exclude "modules/teleop/bin/debug/services" \
    --exclude "modules/teleop/bin/debug/include" \
    \
    --exclude "robocup/bin/debug/include/config.h" \
    --exclude "robocup/bin/debug/cmake_install.cmake" \
    --exclude "robocup/bin/debug/CMakeFiles" \
    --exclude "robocup/bin/debug/Makefile" \
    --exclude "robocup/bin/debug/CMakeCache.txt" \
    --exclude "robocup/bin/debug/robocup" \
    --exclude "hurocup/bin/debug/include" \
    --exclude "hurocup/bin/debug/cmake_install.cmake" \
    --exclude "hurocup/bin/debug/services" \
    --exclude "hurocup/bin/debug/brain" \
    --exclude "hurocup/bin/debug/tsai2d" \
    --exclude "hurocup/bin/debug/jsoncpp" \
    --exclude "hurocup/bin/debug/CMakeFiles" \
    --exclude "hurocup/bin/debug/Makefile" \
    --exclude "hurocup/bin/debug/CMakeCache.txt" \
    --exclude "hurocup/bin/debug/hurocup" \
    \
    --exclude "TODO.txt" \
    ./
rsync -avh Kamp@$1:$sourcedir/visionmodule/www ./visionmodule/bin/debug
exit $?
