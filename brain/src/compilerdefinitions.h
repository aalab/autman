/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 30, 2015
 */

#ifndef SERVICES_COMPULERDEFINITIONS_H
#define SERVICES_COMPULERDEFINITIONS_H

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define VERBOSE_HEADER "[VERBOSE] " << __FILENAME__ << ":" << __LINE__ << ") "
#define VERBOSE(x) std::cout << VERBOSE_HEADER << x << std::endl
#define VERBOSENEL(x) std::cout << VERBOSE_HEADER << x
#define VERBOSEDATA(d, s) Interface::dumpData(d, s)
#define VERBOSEB(x) std::cout << x << std::endl
#define VERBOSEBNEL(x) std::cout << x

/*
#define VERBOSE_HEADER
#define VERBOSE(x)
#define VERBOSENEL(x)
#define VERBOSEDATA(d, s)
#define VERBOSEB(x)
#define VERBOSEBNEL(x)
*/

#define ERROR_HEADER "[ERROR] " << __FILENAME__ << ":" << __LINE__ << ") "
#define ERROR(x) std::cout << VERBOSE_HEADER << x << std::endl
#define ERRORNEL(x) std::cout << VERBOSE_HEADER << x
#define ERRORB(x) std::cout << x << std::endl
#define ERRORBNEL(x) std::cout << x

#endif //SERVICES_COMPULERDEFINITIONS_H
