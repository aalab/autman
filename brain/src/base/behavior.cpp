/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <thread>

#include <iostream>

#include "../../include/brain/base/behavior.h"
#include "../../src/compilerdefinitions.h"

void brain::Behavior::runTrampoline(brain::Behavior *behavior)
{
	behavior->run();
}

brain::Behavior::Behavior()
	: enabled(true), dumpUiData(false)
{ }

brain::Behavior::~Behavior()
{ }

void brain::Behavior::start()
{
	this->start(false);
}

void brain::Behavior::start(bool block)
{
	if (block)
		this->run();
	else
	{
		std::thread thread(runTrampoline, this);
		thread.detach();
	}
}

void handleSignal(int sig, siginfo_t *si, void *context)
{
	VERBOSE("HANDLE SIGNAL!");
}


void brain::Behavior::run()
{
	this->running = true;
	this->lastTime = std::chrono::system_clock::now();

	this->lastUiTime = this->lastTime;
	while (!this->terminated)
	{
		this->waitEnabled();
		this->currentTime = std::chrono::system_clock::now();
		this->pastIdleMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(this->currentTime - this->lastTime).count();
		this->dumpUiData = (std::chrono::duration_cast<std::chrono::milliseconds>(this->currentTime - this->lastUiTime).count() > 1000);
		if (this->dumpUiData)
			this->lastUiTime = this->currentTime;
		try
		{
			this->process();
		}
		catch (std::exception &e)
		{
			ERROR(e.what());
		}
		this->lastTime = this->currentTime;
	}
	this->running = false;
}

void brain::Behavior::enable()
{
	if (!this->enabled)
	{
		this->enabled = true;
		this->enabledCondVar.notify_all();
	}
}

void brain::Behavior::disable()
{
	if (this->enabled)
		this->enabled = false;
}

void brain::Behavior::waitEnabled()
{
	if (!this->enabled)
	{
		VERBOSE("Waiting for enable.");
		std::unique_lock<std::mutex> lock(this->enabledMutex);
		this->enabledCondVar.wait(lock);
	}
}

bool brain::Behavior::isEnabled()
{
	return this->enabled;
}

bool brain::Behavior::isRunning()
{
	return this->running;
}

void brain::Behavior::terminate()
{
	this->terminated = true;
}

bool brain::Behavior::isUnique()
{
	return false;
}
