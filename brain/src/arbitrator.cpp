/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <iostream>

#include "../include/brain/arbitrator.h"

void brain::Arbitrator::run()
{
	for (Behavior* b : this->behaviors)
	{
#ifdef DEBUG
		std::cout << "brain::Arbitrator::run(): Starting behavior..." << std::endl;
#endif
		b->start();
	}
	Behavior::run();
}

void brain::Arbitrator::addBehavior(Behavior *behavior)
{
	if (behavior->isUnique())
	{
		for (Behavior *b : this->behaviors)
		{
			if (b == behavior)
				return;
		}
	}
	this->behaviors.push_back(behavior);
}

bool brain::Arbitrator::empty()
{
	return this->behaviors.empty();
}
