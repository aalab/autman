CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

if(POLICY CMP0040)
	cmake_policy(SET CMP0040 OLD)
endif()

if(POLICY CMP0048)
	cmake_policy(SET CMP0048 OLD)
endif()

IF(NOT CMAKE_BUILD_TYPE)
	SET(CMAKE_BUILD_TYPE Debug CACHE STRING
		"Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
	FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)

project(brain)

set (VERSION_MAJOR 0)
set (VERSION_MINOR 1)

set(PROJECT_VERSION ${VERSION_MAJOR}.${VERSION_MINOR})

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=c++0x -DDEBUG -Wall -fpermissive")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++0x -Wall")

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG -Wall")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Wall")

configure_file (
	"${PROJECT_SOURCE_DIR}/src/config.h.in"
	"${PROJECT_BINARY_DIR}/include/config.h"
)
include_directories("${PROJECT_BINARY_DIR}/include")

# External libraries
# ------------------

# Boost

set(Boost_NO_SYSTEM_PATHS ON)
set(BOOST_ROOT "$ENV{HOME}/libs/libboost_1_59_0")
set(BOOST_INCLUDEDIR "${BOOST_ROOT}/include")
set(BOOST_LIBRARYDIR "${BOOST_ROOT}/lib")
find_package(Boost 1.59.0 REQUIRED system unit_test_framework regex thread)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

## Tsai2D

set(tsai2d_ROOT "$ENV{HOME}/libs/tsai2d")
set(tsai2d_INCLUDE_DIR "${tsai2d_ROOT}/include")
set(tsai2d_LIBRARY_DIR "${tsai2d_ROOT}/lib")

message("Tsai2D INCLUDE DIRECTORY: ${tsai2d_INCLUDE_DIR}")
message("Tsai2D LIBRARY DIRECTORY: ${tsai2d_LIBRARY_DIR}")
# include(/home/jsantos/libs/tsai2d/lib/tsai2d-shared.cmake)
#add_library(tsai2d STATIC ${TSAI2D_LIBRARY_DIR}/libtsai2d.a)
#set_target_properties(tsai2d PROPERTIES LINKER_LANGUAGE C)
include_directories(${tsai2d_INCLUDE_DIR})
link_directories(${tsai2d_LIBRARY_DIR})
find_library(tsai2d NAMES tsai2d)

## jsoncpp

set(jsoncpp_ROOT "$ENV{HOME}/libs/jsoncpp")
message(JSON dir: ${jsoncpp_ROOT})
include_directories(${jsoncpp_ROOT}/include)
link_directories(${jsoncpp_ROOT}/lib)
find_library(jsoncpp NAMES jsoncpp)

# --------

find_library(services ../services)

set(${PROJECT_NAME}_INCLUDE_DIRS
	${PROJECT_SOURCE_DIR}/include
	${SERVICES_INCLUDE_DIRS}
)

include_directories(../services/include)

# ------------------

add_library(brain
	src/base/behavior.cpp
	src/arbitrator.cpp
	src/robotstate.cpp
)

target_link_libraries (brain LINK_PUBLIC services)

# target_include_directories (brain PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
