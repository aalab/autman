/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_ARBITRATOR_H
#define ROBOCUP_ARBITRATOR_H

#include <vector>
#include "base/behavior.h"

#ifndef override
#define override
#endif

namespace brain
{
class Arbitrator
	: public Behavior
{
protected:
	std::vector<Behavior*> behaviors;

	virtual void run() override;
public:
	virtual void addBehavior(Behavior *behavior);

	bool empty();
};
}


#endif //ROBOCUP_ARBITRATOR_H
