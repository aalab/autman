/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_BEHAVIOR_H
#define ROBOCUP_BEHAVIOR_H

#include <mutex>
#include <condition_variable>

#include <signal.h>

namespace brain
{
class Behavior
{
private:
	volatile bool enabled;
	volatile bool running;
	volatile bool terminated;
	struct sigaction oldSigAction;

	static void runTrampoline(Behavior* behavior);
protected:
	std::chrono::system_clock::time_point currentTime;
	std::chrono::system_clock::time_point lastTime;
	std::chrono::system_clock::time_point lastUiTime;
	long pastIdleMilliseconds;
	bool dumpUiData;

	virtual void process() = 0;

	virtual void run();

	void waitEnabled();
public:
	Behavior();
	virtual ~Behavior();

	virtual void start();
	virtual void start(bool block);

	virtual void enable();
	virtual void disable();

	bool isEnabled();
	bool isRunning();

	virtual void terminate();

	std::mutex enabledMutex;
	std::condition_variable enabledCondVar;

	virtual bool isUnique();
};
}


#endif //ROBOCUP_BEHAVIOR_H
