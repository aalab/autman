/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ROBOTSTATE_H
#define ROBOCUP_ROBOTSTATE_H

#include <services/data/robot/readstate.h>

namespace brain
{
class RobotState
{
public:
	RobotState();

	virtual void fetch(services::data::robot::ReadStateResponse &response);
};
}


#endif //ROBOCUP_ROBOTSTATE_H
