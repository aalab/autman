#!/bin/bash
echo You are copying from $1.

scriptpath="`dirname \"$0\"`"
scriptpath="`( cd \"$scriptpath\" && pwd )`"
cd $scriptpath
rsync -aAxv jsantos@$1:/home/walle2/aalabUofM/madmax --exclude "madmax/visionmodule/bin/debug/www" --exclude "madmax/visionmodule/bin/debug/CMakeFiles" --exclude "madmax/visionmodule/bin/debug/cmake_install.cmake" --exclude "madmax/visionmodule/bin/debug/CMakeCache.txt" --exclude "madmax/visionmodule/bin/debug/Makefile" --exclude "madmax/controlmodule/bin/debug/CMakeFiles" --exclude "madmax/controlmodule/bin/debug/cmake_install.cmake" --exclude "madmax/controlmodule/bin/debug/CMakeCache.txt" --exclude "madmax/controlmodule/bin/debug/Makefile" --exclude "madmax/autman/bin/debug/CMakeFiles" --exclude "madmax/autman/bin/debug/cmake_install.cmake" --exclude "madmax/autman/bin/debug/CMakeCache.txt" --exclude "madmax/autman/bin/debug/Makefile" --exclude "madmax/autman/bin/debug/Include/config.h" --exclude "madmax/autman/bin/debug/imumodule/CMakeFiles" --exclude "madmax/autman/bin/debug/imumodule/cmake_install.cmake" --exclude "madmax/autman/bin/debug/imumodule/Makefile" --exclude "madmax/autman/bin/debug/imumodule/imu_module" --exclude "madmax/autman/bin/debug/imumodule/include/config.h" --exclude "madmax/robocup/bin/debug/include/config.h" --exclude "madmax/robocup/bin/debug/cmake_install.cmake" --exclude "madmax/robocup/bin/debug/CMakeFiles" --exclude "madmax/robocup/bin/debug/Makefile" --exclude "madmax/robocup/bin/debug/CMakeCache.txt" --exclude "madmax/robocup/bin/debug/robocup" ./
rsync -aAxv jsantos@$1:/home/jsantos/projects/aalabUofM/madmax/visionmodule/www ./madmax/visionmodule/bin/debug
exit $?
