#author Kyle Morris, feb/25/2016
#Move neck motor to desired degree, with some customizations
#PARAM 1: "lateral" for lateral, "transversal" for transversal
#PARAM 2: angle in radians
#PARAM 3: speed

if [ "$1" == "lateral" ]; then
echo "{\"name\":\"Arash\",\"command\":\"Actuator\",\"params\":{\"command\":\"Position\",\"name\":\"NeckLateral\",\"angle\":$2,\"speed\":$3,\"gainP\": 32,\"gainI\": 0,\"gainD\": 0}}" | nc -u localhost 1313
elif [ "$1" == "transversal" ]; then
echo "{\"name\":\"Arash\",\"command\":\"Actuator\",\"params\":{\"command\":\"Position\",\"name\":\"NeckTransversal\",\"angle\":$2,\"speed\":$3,\"gainP\": 32,\"gainI\": 0,\"gainD\": 0}}" | nc -u localhost 1313
fi
