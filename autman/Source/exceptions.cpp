/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 05, 2015
 */

#include "exceptions.hpp"


exceptions::CommandException::CommandException(std::string errCode, std::string errMsg)
	: errCode(errCode), errMsg(errMsg)
{ }

Json::Value &operator<<(Json::Value &result, exceptions::CommandException &e)
{
	result["success"] = false;
	result["errCode"] = e.code();
	result["errMsg"] = e.msg();
	return result;
}

exceptions::robot::NameException::NameException(std::string name)
	: CommandException("E2-1", "Wrong robot name.")
{
}

std::string &exceptions::CommandException::code()
{
	return this->errCode;
}

std::string &exceptions::CommandException::msg()
{
	return this->errMsg;
}

exceptions::CommandException::~CommandException() noexcept
{ }
