
#include "config.h"

#include "position.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
/* #include <boost/asio/signal_set.hpp> */
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>

#define DEFAULT_PORT "1313"
#define DEFAULT_SERVER "localhost"

using boost::asio::ip::udp;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );

  socket.send_to( boost::asio::buffer( command ), * iterator );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  std::cout << "motion_tuner::SendCommand Responses " << reply << std::endl;
#endif					   		      

  return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
  return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
  std::cout<<"from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif 
  return SendCommand( socket, iterator, trajectory.ToCommandString() );
}

static std::vector<Trajectory> motions;

void
LoadTrajectories( boost::property_tree::ptree & pt )
{
  using boost::property_tree::ptree;
  
  BOOST_FOREACH( ptree::value_type & v , pt.get_child("trajectories"))
    {
      //std::string tmp = pt.get<std::string>("motion");
      std::string motionName = (std::string)v.first.data();
#ifndef NDEBUG
      std::cout << "Found Trajectory for: " << motionName << std::endl;
#endif
      BOOST_FOREACH( ptree::value_type & joint , v.second)
       { 
	 //std::string nameOfTheJoint = pt.get<std::string>("name");
	 //boost::to_lower(nameOfTheJoint);
	 //std::string nameOfTheJoint = (std::string)val.first.data();
	 // std::cout<<"The name of the joint is:"<< test <<std::endl;
	 std::string jointName = (std::string)joint.first.data();
#ifndef NDEBUG
	 std::cout << "Found  joint: " << jointName << std::endl;
#endif
	 
	  Trajectory tmpT( motionName, jointName, joint.second );
          motions.push_back(tmpT);
       } 
    }
}

void 
LoadTrajectories(std::string const filename )
{
  using boost::property_tree::ptree;
  ptree pt;

  read_info( filename, pt );
  LoadTrajectories( pt );
}


unsigned long int tdiffUSec(struct timespec const start, struct timespec const end);

void addTimeNSec( struct timespec * time, long int disp );


float polynomialHold(float const amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage);



int 
main(int argc, char *argv[] )
{
  namespace po = boost::program_options;
  std::string port;
  std::string server;
  std::vector<std::string> trajectoryFiles;

  po::options_description commandLineOnlyOptions("Command Line Options");
  commandLineOnlyOptions.add_options()
    ( "version,v", "print version string" )
    ( "help,h", "print help message" )
    ( "server,s", po::value<std::string>( & server )->default_value(DEFAULT_SERVER), "server address" )
    ( "port,p", po::value<std::string> ( & port )->default_value(DEFAULT_PORT), "port number" )
    ( "trajectories,t", po::value<std::vector<std::string> >( & trajectoryFiles ), "file with trajectory information");
  
  std::cout << "Motion Tuner" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
  std::cout << "Debugging build" << std::endl;
#endif
  
  po::options_description commandLineOptions;
  commandLineOptions.add(commandLineOnlyOptions);

  po::positional_options_description p;
  p.add("trajectories",-1);

  try 
    {
      po::variables_map vm;
      po::store( po::command_line_parser( argc, argv).options(commandLineOptions).positional(p).run(), vm );
      po::notify( vm );

      if ( vm.count("help") )
	{
	  std::cout << commandLineOptions << "\n";
	  return 1;
	}

      if (vm.count("version") )
	{
	  std::cout << "Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
	  return 1;
	}
    }
  catch( std::exception & e )
    {
      std::cerr << "boost::po exception " << e.what() << std::endl;
      return 1;
    }

#if 0 
  if ( argc > 1 )
    {
      for( int i = 1; i < argc; i++ )
	{
	  std::string fname = std::string(CONFIG_DIR "/" ) + std::string(argv[i]);
	  LoadTrajectories( fname );	 
	}
    }
#endif  
  std::cout << "Server " << server << ", port " << port << std::endl;
  std::cout << "Trajectory Files" << std::endl;

  BOOST_FOREACH( std::string s, trajectoryFiles )
    {
      if ( ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".info" ) && 
	   ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".json" ) &&
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".xml" ) && 
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".ini" ) )
	{
	  s = s + ".info";
	}
      
      LoadTrajectories( std::string(CONFIG_DIR) + "/" + s );	 
    }

  std::cout << std::endl;

  boost::asio::io_service io_service;
  udp::socket socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  
  udp::resolver resolver( io_service );
  udp::resolver::query query( udp::v4(), server, port );
  udp::resolver::iterator iterator = resolver.resolve( query );

  Position saved;

  std::string com("ReadState");
#ifndef NDEBUG
  std::cout << "Sending command " << com << std::endl;
#endif      

  std::string reply;
  reply = SendCommand( socket, iterator, com );
#ifndef NDEBUG
  std::cout << "Reply for Read State " << reply << std::endl;
#endif  

  std::istringstream ireply( reply );

  if ( saved.parseFromStream( ireply ) < 0 )
    {
      std::cerr << "ERROR: Unable to parse position from response " << reply << std::endl;
      //      std::exit(34);
    }
  
  BOOST_FOREACH( Trajectory  traj, motions ) 
     {
       std::string reply = SendTrajectory( socket, iterator, traj );
       
#ifndef NDEBUG
       std::cout << "Response from sending trajectory " << reply << std::endl;
#endif
     }
  
 #ifndef NDEBUG
  std::cout << "Saved position" << std::endl;
  saved.Print();
#endif
  



/*
         dRight
       ******
       *    *
       *    *
       *    *
dStart *    * dMiddle     dEnd   in percentage;
********    ********     *********
                   *     *
                   *     *
                   *     *
                   *******
                     dLeft
*/

  for( ;; )
    {
      std::string com = "Arash Trajectory=Walk CycleTimeUSec = 400000 Cycles=200\n";
      reply = SendCommand( socket, iterator,  com );
#ifndef NDEBUG
      std::cout << "Trajectory returns with ret=" << reply << std::endl;
#endif      
      sleep(1);
    }
	struct timespec time1;
	struct timespec startCycle;
	unsigned int s;
	unsigned long int cycleTimeUSec = 500000L;
	unsigned long int diffTimeUSec;
	//	unsigned long int ratio;
		
	clock_gettime(CLOCK_REALTIME, &startCycle);

	for( s = 0; s < 10000; s++ )
	{
	  clock_gettime(CLOCK_REALTIME, &time1);
	  diffTimeUSec = tdiffUSec( time1, startCycle );
	  while ( diffTimeUSec > cycleTimeUSec )
	  {
	    addTimeNSec( & startCycle, cycleTimeUSec * 1000L );// cout<<time1 &startCyc
	    diffTimeUSec = diffTimeUSec - cycleTimeUSec;
	  }
	 
#if 0
#ifndef NDEBUG
	  std::cout << "Interpolation Torso " <<  percentage  << "," << trajectoryTorso.Interpolate(percentage) << std::endl;
	  std::cout << "Interpolation LeftLeg " <<  percentage  << "," << trajectoryLeft.Interpolate(percentage) << std::endl;
	  std::cout << "Interpolation RightLeg " <<  percentage  << "," << trajectoryRight.Interpolate(percentage) << std::endl;
#endif
#endif
	  usleep(10000);
	}
	com = "PowerOff";
	reply = SendCommand( socket, iterator,  com );
	return 0;
}


unsigned long int 
tdiffUSec(struct timespec const end, struct timespec const start)
{
	unsigned long int diff; 
	diff = ( end.tv_sec-start.tv_sec ) * 1000000 + ( end.tv_nsec-start.tv_nsec ) / 1000;   
	return diff;
}

void
addTimeNSec( struct timespec * time, long int disp )
{
    long int nsec;
    int correct = 0;
    
    nsec = time->tv_nsec + disp;
    while ( nsec < 0 ) 
    {
	nsec = nsec + 1000000000L;
	correct--;
    }
    while ( nsec > 1000000000L )
    {
	nsec = nsec - 1000000000L;
	correct++;
    }
    time->tv_nsec = nsec;
    time->tv_sec = time->tv_sec + correct;
}

float polynomialHold( float const  amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage)
		  
{
  float sum = 0.0;
  unsigned int index;
  float ampl = -0.001234;

  if ( currentCyclePercentage < 0.0 )
    {
      currentCyclePercentage = 0.0;
    }
  if ( currentCyclePercentage > 1.0 )
    {
      currentCyclePercentage = 1.0;
    }

  index = -1;
  while( sum <= currentCyclePercentage )
    {
      index = index + 1;
      sum = sum + durations[index];
      if ( index == N-1 )
	{
	  break;
	}
    }
  if ( ( index >= 0 ) && ( index < N ) )
    {
      ampl = amplitudes[index];
    }
  return ampl;
}



  

