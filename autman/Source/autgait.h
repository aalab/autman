/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 18, 2015
 */

#ifndef MOTIONLIBRARY_AUTGAIT_H
#define MOTIONLIBRARY_AUTGAIT_H

#include <unix_serial_port.hpp>

struct __attribute__((__packed__)) AUTGaitHeader
{
	uint8_t header;
	uint8_t command;
};

struct __attribute__((__packed__)) AUTGaitMoveBody
{
	uint8_t vx;
	uint8_t vy;
	uint8_t vt;
};

struct __attribute__((__packed__)) AUTGaitMove
{
	AUTGaitHeader header;
	AUTGaitMoveBody move;
};


struct __attribute__((__packed__)) AUTGaitHeadBody
{
	uint8_t panLOW;
	uint8_t panHIGH;
	uint8_t tiltLOW;
	uint8_t tiltHIGH;
};

struct __attribute__((__packed__)) AUTGaitHead
{
	AUTGaitHeader header;
	AUTGaitHeadBody head;
};

struct __attribute__((__packed__)) AUTGaitMotion
{
	AUTGaitHeader header;
	uint8_t move;
};

struct __attribute__((__packed__)) AUTGaitData
{
	AUTGaitHeader header;
	AUTGaitMoveBody move;
	uint8_t motion;
	AUTGaitHeadBody head;
};

struct __attribute__((__packed__)) AUTGaitEulerData
{
	uint8_t header[2];
	uint8_t xLO;
	uint8_t xHI;
	uint8_t yLO;
	uint8_t yHI;
	uint8_t zLO;
	uint8_t zHI;
};

struct __attribute__((__packed__)) AUTGaitWEPBody
{
	uint8_t index;
	double value;
};

struct __attribute__((__packed__)) AUTGaitWEP
{
	AUTGaitHeader header;
	AUTGaitWEPBody wep;
};

struct __attribute__((__packed__)) AUTGaitCommonResponse
{
	AUTGaitHeader header;
	uint8_t errorCode;
};

struct __attribute__((__packed__)) AUTGaitWEPReadBody
{
	uint8_t index;
	uint8_t count;
};

struct __attribute__((__packed__)) AUTGaitWEPRead
{
	AUTGaitHeader header;
	AUTGaitWEPReadBody wepRead;
};

struct __attribute__((__packed__)) AUTGaitWEPReadResponse
{
	AUTGaitHeader header;
	double value;
};

class AUTGait
{
private:
	UnixSerialPort* port;

	int16_t DTOU_Pi(double dbl);

protected:
	bool readDataNoLock(AUTGaitCommonResponse &data);

public:
	AUTGait(UnixSerialPort *port);

	void initData(AUTGaitData &data, double vx, double vy, double vt, uint8_t motion, double pan, double tilt);
	void initData(AUTGaitData &data, double vx, double vy, double vt, double pan, double tilt);

	void initData(AUTGaitHead &data, double pan, double tilt);
	void initData(AUTGaitHeadBody &data, double pan, double tilt);

	void initData(AUTGaitMove &data, double vx, double vy, double vt);
	void initData(AUTGaitMoveBody &data, double vx, double vy, double vt);

	void initData(AUTGaitWEP &data, uint8_t index, double value);
	void initData(AUTGaitWEPBody &data, uint8_t index, double value);

	void initData(AUTGaitWEPReadBody &data, uint8_t index, uint8_t count);
	void initData(AUTGaitWEPRead &data, uint8_t index);

	bool sendData(AUTGaitData *data);
	bool sendData(AUTGaitHead *data);
	bool sendData(AUTGaitMove *data);
	bool sendData(AUTGaitWEP *data);

	bool readData(AUTGaitEulerData &data);
	bool readData(AUTGaitWEPRead &request, AUTGaitWEPReadResponse &response);
};


#endif //MOTIONLIBRARY_AUTGAIT_H
