/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 18, 2015
 */

#include "autgait.h"
#include "compilerdefinitions.h"

AUTGait::AUTGait(UnixSerialPort *port)
	: port(port)
{ }

int16_t AUTGait::DTOU_Pi(double dbl)
{
	if (
		((dbl < 0) && (dbl >= -M_PIl))
		|| ((dbl >= 0) && (dbl <= M_PIl))
	)
	{
		return (dbl*1000);
	}
	return 0;
}

void AUTGait::initData(AUTGaitData &data, double vx, double vy, double vt, uint8_t motion, double pan, double tilt)
{
	data.header.header = 254;
	data.header.command = 1;

	initData(data.move, vx, vy, vt);

	data.motion = motion;

	initData(data.head, pan, tilt);
}

void AUTGait::initData(AUTGaitData &data, double vx, double vy, double vt, double pan, double tilt)
{
	initData(data, vx, vy, vt, 100, pan, tilt);
}

void AUTGait::initData(AUTGaitMove &data, double vx, double vy, double vt)
{
	data.header.header = 254;
	data.header.command = 2;
	initData(data.move, vx, vy, vt);
}

void AUTGait::initData(AUTGaitMoveBody &data, double vx, double vy, double vt)
{
	data.vx = (100 + (vx * 100.0));
	data.vy = (100 + (vy * 100.0));
	data.vt = (100 + (vt * 100.0));
}

void AUTGait::initData(AUTGaitHead &data, double pan, double tilt)
{
	data.header.header = 254;
	data.header.command = 3;

	initData(data.head, pan, tilt);
}

void AUTGait::initData(AUTGaitHeadBody &data, double pan, double tilt)
{
	data.panLOW = (DTOU_Pi(pan));
	data.panHIGH = (uint8_t) (DTOU_Pi(pan) >> 8);

	data.tiltLOW = (uint8_t) (DTOU_Pi(tilt));
	data.tiltHIGH = (uint8_t) (DTOU_Pi(tilt) >> 8);

	int16_t x = (data.tiltHIGH << 8) + (data.tiltLOW);
	float tmp = x/1000.0;

	VERBOSE("Value : " << tmp);
}

bool AUTGait::sendData(AUTGaitData *data)
{
	ChannelLockGuard locker(*this->port);
	VERBOSE("Sending data: ");
	VERBOSEB("Header: " << static_cast<unsigned int>(data->header.header));
	VERBOSEB("Command: " << static_cast<unsigned int>(data->header.command));
	VERBOSEB("VT: " << static_cast<unsigned int>(data->move.vt));
	VERBOSEB("VX: " << static_cast<unsigned int>(data->move.vx));
	VERBOSEB("VY: " << static_cast<unsigned int>(data->move.vy));
	VERBOSEB("Motion: " << static_cast<unsigned int>(data->motion));
	VERBOSEB("panHI: " << static_cast<unsigned int>(data->head.panHIGH));
	VERBOSEB("panLO: " << static_cast<unsigned int>(data->head.panLOW));
	VERBOSEB("tiltHI: " << static_cast<unsigned int>(data->head.tiltHIGH));
	VERBOSEB("tiltLO: " << static_cast<unsigned int>(data->head.tiltLOW));
	int ret = this->port->writeData((uint8_t*)data, sizeof(AUTGaitData));
	VERBOSE(ret << " bytes written");
	return (ret == sizeof(AUTGaitData));
}

bool AUTGait::sendData(AUTGaitHead *data)
{
	ChannelLockGuard locker(*this->port);
	VERBOSE("Sending data: ");
	VERBOSEB("Header: " << static_cast<unsigned int>(data->header.header));
	VERBOSEB("Command: " << static_cast<unsigned int>(data->header.command));
	VERBOSEB("panHI: " << static_cast<unsigned int>(data->head.panHIGH));
	VERBOSEB("panLO: " << static_cast<unsigned int>(data->head.panLOW));
	VERBOSEB("tiltHI: " << static_cast<unsigned int>(data->head.tiltHIGH));
	VERBOSEB("tiltLO: " << static_cast<unsigned int>(data->head.tiltLOW));
	int ret = this->port->writeData((uint8_t*)data, sizeof(AUTGaitHead));
	VERBOSE(ret << " bytes written");
	return (ret == sizeof(AUTGaitHead));
}

bool AUTGait::sendData(AUTGaitMove *data)
{
	ChannelLockGuard locker(*this->port);
	VERBOSE("Sending data: ");
	VERBOSEB("Header: " << static_cast<unsigned int>(data->header.header));
	VERBOSEB("Command: " << static_cast<unsigned int>(data->header.command));
	VERBOSEB("VT: " << static_cast<unsigned int>(data->move.vt));
	VERBOSEB("VX: " << static_cast<unsigned int>(data->move.vx));
	VERBOSEB("VY: " << static_cast<unsigned int>(data->move.vy));
	int ret = this->port->writeData((uint8_t*)data, sizeof(AUTGaitMove));
	VERBOSE(ret << " bytes written");
	return (ret == sizeof(AUTGaitMove));
}

bool AUTGait::readData(AUTGaitEulerData &data)
{
	ChannelLockGuard locker(*this->port);
	return
		(this->port->readData((uint8_t*)&data, sizeof(AUTGaitEulerData)) == sizeof(AUTGaitEulerData))
		&& (data.header[0] == 254) && (data.header[1] == 254);
}

void AUTGait::initData(AUTGaitWEP &data, uint8_t index, double value)
{
	data.header.header = 254;
	data.header.command = 4;
	initData(data.wep, index, value);
}

void AUTGait::initData(AUTGaitWEPBody &data, uint8_t index, double value)
{
	data.index = index;
	data.value = value;
	/*
	int v = (value * 1000);
	data.value[0] = (v & 0xff000000) >> 24;
	data.value[1] = (v & 0xff0000) >> 16;
	data.value[2] = (v & 0xff00) >> 8;
	data.value[3] = (v & 0xff);
	*/
}

bool AUTGait::sendData(AUTGaitWEP *data)
{
	ChannelLockGuard locker(*this->port);
	VERBOSE("Sending data: ");
	VERBOSEB("Header: " << static_cast<unsigned int>(data->header.header));
	VERBOSEB("Command: " << static_cast<unsigned int>(data->header.command));
	VERBOSEB("Index: " << static_cast<unsigned int>(data->wep.index));
	VERBOSEB("Value: " << data->wep.value);
	int ret = this->port->writeData((uint8_t*)data, sizeof(AUTGaitWEP));
	VERBOSE(ret << " bytes written");
	if ((ret == sizeof(AUTGaitWEP)))
	{
		usleep(100);
		AUTGaitCommonResponse response;
		if (this->readDataNoLock(response))
		{
			if (response.errorCode > 0)
			{
				ERROR("Response code: " << response.errorCode);
			}
			return (response.errorCode == 0);
		}
		else
		{
			ERROR("Cannot read response.");
		}
	}
	return false;
}

void AUTGait::initData(AUTGaitWEPRead &data, uint8_t index)
{
	data.header.header = 254;
	data.header.command = 5;
	initData(data.wepRead, index, 1);
}

void AUTGait::initData(AUTGaitWEPReadBody &data, uint8_t index, uint8_t count)
{
	data.index = index;
	data.count = 1;
}

bool AUTGait::readData(AUTGaitWEPRead &request, AUTGaitWEPReadResponse &response)
{
	ChannelLockGuard locker(*this->port);
	VERBOSE("Sending data: ");
	VERBOSEB("Header: " << static_cast<unsigned int>(request.header.header));
	VERBOSEB("Command: " << static_cast<unsigned int>(request.header.command));
	VERBOSEB("Index: " << static_cast<unsigned int>(request.wepRead.index));
	VERBOSEB("Count: " << static_cast<unsigned int>(request.wepRead.count));
	int ret = this->port->writeData((uint8_t*)&request, sizeof(AUTGaitWEPRead));
	VERBOSE(ret << " bytes written");
	if (ret == sizeof(AUTGaitWEPRead))
	{
		return
			(this->port->readData((uint8_t*)&response, sizeof(AUTGaitWEPReadResponse)) == sizeof(AUTGaitWEPReadResponse))
			&& (response.header.header == 254) && (response.header.command == 5);
	}
	return false;
}

bool AUTGait::readDataNoLock(AUTGaitCommonResponse &data)
{
	return
		(this->port->readData((uint8_t*)&data, sizeof(AUTGaitCommonResponse)) == sizeof(AUTGaitCommonResponse))
		&& (data.header.header == 254);

}
