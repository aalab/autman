 /* RX-28 Actuator Definition 
 *
 */

#include "robotis_servo_rx_28.hpp"

RobotisServoRX28::RobotisServoRX28( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
: RobotisServoRXSeries( name, interface, pt ),
  defaultSpeed(0)
{
  type = ROBOTIS_SERVO_RX_28;  

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo RX28 Actuator " << std::endl;
#endif

  maxSpeed = 11.938052; //I'm not sure, must check..
}


