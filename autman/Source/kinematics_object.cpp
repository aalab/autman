#include <iostream>
#include <cmath>
#include "robot.hpp"
#include "kinematics_object.hpp"

KinematicsObject::KinematicsObject(Robot const * robot): servoResolutions(), lowerServoBounds(), upperServoBounds(), robot(robot) {}
        
KinematicsObject::~KinematicsObject() {}

//Checks that all angles are within valid ranges
//angles - vector where the angles to check are stored
//buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
//returns 0 if valid, -1 if not
int KinematicsObject::validAngles(std::vector<float>& angles, float bufferAngle) const {
    int length = angles.size();
    int valid = 0;
    
    for(int i=0;i<length;i++) {
        if(angles[i] < lowerServoBounds[i]+bufferAngle || angles[i] > upperServoBounds[i]-bufferAngle) {
            valid = -1;
            std::cerr << "ERROR: A servo with index " << i << " is outside of its safety bounds (angle: " << angles[i] << ")" << std::endl;
        } else {
            if(servoResolutions[i] == 1024) {
                if(angles[i] < -5.0*M_PI/6.0 || angles[i] > 5.0*M_PI/6.0) {
                    valid = -1;
                    std::cerr << "ERROR: A servo with index " << i << " is in the dead zone (angle: " << angles[i] << ")" << std::endl;
                }
            } else if (servoResolutions[i] != 4096) {
                valid = -1;
                std::cerr << "ERROR: Bad servo resolution (" << servoResolutions[i] << ") given for a servo with index " << i << std::endl;
            }
        }
    }
    
    return valid;
}
