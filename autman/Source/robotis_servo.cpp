/*
 * Robotis Servos Actuator Definition 
 *
 */

#include "robotis_servo.hpp"
#include "interface.hpp"
#include "dynamixel_interface.hpp"
#include "robotis_servo_rx_28.hpp"
#include "robotis_servo_rx_64.hpp"
#include "robotis_servo_mx_106.hpp"
#include "robotis_servo_mx_64.hpp"
#include "robotis_servo_mx_28.hpp"
#include "robot.hpp"

#ifndef NDEBUG

#include <boost/io/ios_state.hpp>
#include <iomanip>

#endif

#include <iostream>

#include <cmath>
#include <sstream>
#include <dynamixel2_interface.hpp>

#include "channel.hpp"
#include "interface.hpp"
#include "utility.hpp"
#include "actuator.hpp"
#include "robotis_servo_rx_series.hpp"
#include "robotis_servo_mx_series.hpp"
#include "compilerdefinitions.h"

float RobotisServo::maxSpeed = 1023;

RobotisServo::RobotisServo(std::string name, DynamixelInterface *interface, boost::property_tree::ptree &pt)
	: Servo(name, interface, pt),
	  defaultHome(Utility::DegreeToRadians(180.0)),
	  defaultMin(Utility::DegreeToRadians(0.0)),
	  defaultMax(Utility::DegreeToRadians(360.0)),
	  statusReturnLevel(RESPONSE_INVALID),
	  defaultGainD(0),
	  defaultGainI(0),
	  defaultGainP(32)
{
	int id = pt.get<int>("id");

	this->id = id;

	home = pt.get<float>("home", defaultHome);
	min = pt.get<float>("min", defaultMin);
	max = pt.get<float>("max", defaultMax);

#ifndef NDEBUG
	std::cout << "Creating Robotis Servo " << name << " with id " << this->id << std::endl;
#endif
	hasFeedback = true;
}

int
RobotisServo::SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed)
{
	ERROR("ERROR");
	return -1;
}

int
RobotisServo::SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
	unsigned int gainI, unsigned int gainP)
{
	ERROR("ERROR");
	return -1;
}

int
RobotisServo::ProcessCommand(Json::Value &json)
{
	int ret = -1;
	if (interface != nullptr)
	{
		if (json["command"] == "Position")
		{
			float angle, speed;
			int
				gainD = -1,
				gainI = -1,
				gainP = -1;
			ret = ParsePositionAndSpeedMessage(json, &angle, &speed, &gainD, &gainI, &gainP);
			if (ret == 0)
			{
				VERBOSE("parsed");
				VERBOSE("Speed: " << speed);
				if ((gainP > 0) || (gainI > 0) || (gainD > 0))
				{
					if (gainD < 0)
						gainD = this->gainD;
					if (gainI < 0)
						gainI = this->gainI;
					if (gainP < 0)
						gainP = this->gainP;
					ret = this->SendPositionAndSpeedMessage(angle, speed, static_cast<unsigned int>(gainD),
						static_cast<unsigned int>(gainI), static_cast<unsigned int>(gainP));
				}
				else
					ret = SendPositionAndSpeedMessage(angle, speed);
			}
		}
		else
			ret = Servo::ProcessCommand(json);
	}
	return ret;
}

int
RobotisServo::SendPositionAndSpeedMessage(float angle, float speed)
{
	unsigned int ticks = this->AngleRadiansToPositionTicks(home + angle);
	unsigned int ispeed = this->SpeedToISpeed(speed);
	int ret = 0;

	DynamixelInterface *dyniface;

	if (((dyniface = dynamic_cast<DynamixelInterface *>(interface)) != 0) && (dyniface->IsSyncCommandReady()))
	{
		int err;
		if ((err = dyniface->AddSyncCommand(id, this->gainD, this->gainI, this->gainP, ticks, ispeed)) != 0)
		{
			ret = err;
			std::cerr << "RobotisServo::SendPositionAndSpeed AddSyncCommand failed with code " << err << std::endl;
		}
	}
	else
	{
		VERBOSE("222 Speed: " << ispeed);
		if (SendPositionTicksAndISpeed(ticks, ispeed))
		{
			std::cerr << "RobotisServo::SendPositionAndSpeed SendPositionTicksAndISpeed failed" << std::endl;
			ret = -5;
		}
	}
	return ret;
}

unsigned int
RobotisServo::AngleRadiansToPositionTicks(float angle)
{
	std::cout << "ERROR..RobotisServo::AngleRadiansToPositionTicks..ERROR" << std::endl;
	return -1;
}

float
RobotisServo::AnglePositionTicksToRadians(unsigned int ticks)
{
	std::cout << "ERROR..RobotisServo::AnglePositionTicksToRadians..ERROR" << std::endl;
	return -1;
}


unsigned int
RobotisServo::SpeedToISpeed(float speed)
{
	/*
	unsigned int ispeed = static_cast<unsigned int>(speed / maxSpeed * 1024);
	return static_cast<unsigned int>( Utility::Clamp(0, static_cast<int>(ispeed), 1023));
	*/
	return speed;
}

float
RobotisServo:: ISpeedToSpeed(unsigned int ispeed)
{
	bool neg = false;
	if (ispeed & 0x0400)
	{
		neg = true;
		ispeed = ispeed & 0x377;
	}
	float speed = static_cast<float>(ispeed) / 1024.0 * maxSpeed;
	if (neg)
	{
		speed = -speed;
	}
	return speed;
}

float
RobotisServo::ILoadToLoad(unsigned int iload)
{
	bool neg = false;
	if (iload & 0x0400)
	{
		neg = true;
		iload = iload & 0x377;
	}
	float load = static_cast<float>(iload) / 1024.0 * 100.0;
	if (neg)
	{
		load = -load;
	}
	return load;
}

int
RobotisServo::ReadPositionTicksAndISpeedAndILoad(servos::dynamixel::mx::ReadStatePackage* resultPackage, protocols::dynamixel1::ErrorByte *error)
{
	std::cout << "ERROR..RobotisServo::ReadPositionTicksAndISpeedAndILoad..ERROR" << std::endl;
	return -1;
}

int
RobotisServo::ReadFullData(servos::dynamixel::mx::ReadFullStatePackage* resultPackage, protocols::dynamixel1::ErrorByte *error)
{
	std::cout << "ERROR..RobotisServo::ReadPositionTicksAndISpeedAndILoad..ERROR" << std::endl;
	return -1;
}

int
RobotisServo::ReadState(Json::Value &jsonResult)
{
	servos::dynamixel::mx::ReadStatePackage readStateResult;
	int ret;
	int result = 0;
	protocols::dynamixel1::ErrorByte errorFlags;
	RobotisServo::init_error(&errorFlags);

	ret = ReadPositionTicksAndISpeedAndILoad(&readStateResult, &errorFlags);
	if (ret)
	{
		ERROR("failed with code " << ret);
		result = -1;
	}

	float a = AnglePositionTicksToRadians(LOHI_INT(readStateResult.positionSpeed.position_LO, readStateResult.positionSpeed.position_HI));
	float angle = Utility::NormalizeAngle(a - home);

	jsonResult["angle"] = angle;
	jsonResult["speed"] = ISpeedToSpeed(LOHI_UINT(readStateResult.positionSpeed.speed_LO, readStateResult.positionSpeed.speed_HI));
	jsonResult["load"] = ILoadToLoad(LOHI_INT(readStateResult.load_LO, readStateResult.load_HI));
	jsonResult["temperature"] = static_cast<int>(readStateResult.temperature);
	jsonResult["voltage"] = (readStateResult.voltage/10.0);

	if (!errorFlags.noError)
	{
		if (errorFlags.instruction)
		{
			jsonResult["error"]["instruction"] = true;
		}
		if (errorFlags.overload)
		{
			jsonResult["error"]["overload"] = true;
		}
		if (errorFlags.checksum)
		{
			jsonResult["error"]["checksum"] = true;
		}
		if (errorFlags.range)
		{
			jsonResult["error"]["range"] = true;
		}
		if (errorFlags.overheating)
		{
			jsonResult["error"]["overheating"] = true;
		}
		if (errorFlags.anglelimit)
		{
			jsonResult["error"]["anglelimit"] = true;
		}
		if (errorFlags.inputvoltage)
		{
			jsonResult["error"]["inputvoltage"] = true;
		}
	}
	return result;
}

int RobotisServo::ReadFullState(Json::Value &jsonResult)
{
	servos::dynamixel::mx::ReadFullStatePackage readStateResult;
	int ret;
	int result = 0;
	protocols::dynamixel1::ErrorByte errorFlags;
	RobotisServo::init_error(&errorFlags);

	ret = ReadFullData(&readStateResult, &errorFlags);
	if (ret)
	{
		ERROR("failed with code " << ret);
		result = -1;
	}

	float a = AnglePositionTicksToRadians(LOHI_UINT(readStateResult.positionSpeed.position_LO, readStateResult.positionSpeed.position_HI));
	float angle = Utility::NormalizeAngle(a - home);

	jsonResult["angle"] = angle;
	jsonResult["gains"]["p"] = readStateResult.gains.p;
	jsonResult["gains"]["i"] = readStateResult.gains.i;
	jsonResult["gains"]["d"] = readStateResult.gains.d;
	jsonResult["goalPosition"] = AnglePositionTicksToRadians(LOHI_UINT(readStateResult.goalPosition_LO, readStateResult.goalPosition_HI));
	jsonResult["movingSpeed"] = ISpeedToSpeed(LOHI_UINT(readStateResult.movingSpeed_LO, readStateResult.movingSpeed_HI));
	jsonResult["torqueLimit"] = ILoadToLoad(LOHI_UINT(readStateResult.torqueLimit_LO, readStateResult.torqueLimit_HI));
	jsonResult["speed"] = ISpeedToSpeed(LOHI_UINT(readStateResult.positionSpeed.speed_LO, readStateResult.positionSpeed.speed_HI));
	jsonResult["load"] = ILoadToLoad(LOHI_UINT(readStateResult.presentLoad_LO, readStateResult.presentLoad_HI));
	jsonResult["temperature"] = static_cast<int>(readStateResult.presentTemperature);
	jsonResult["voltage"] = (readStateResult.presentTemperature/10.0);
	jsonResult["moving"] = (readStateResult.moving == 1);

	if (!errorFlags.noError)
	{
		if (errorFlags.instruction)
		{
			jsonResult["error"]["instruction"] = true;
		}
		if (errorFlags.overload)
		{
			jsonResult["error"]["overload"] = true;
		}
		if (errorFlags.checksum)
		{
			jsonResult["error"]["checksum"] = true;
		}
		if (errorFlags.range)
		{
			jsonResult["error"]["range"] = true;
		}
		if (errorFlags.overheating)
		{
			jsonResult["error"]["overheating"] = true;
		}
		if (errorFlags.anglelimit)
		{
			jsonResult["error"]["anglelimit"] = true;
		}
		if (errorFlags.inputvoltage)
		{
			jsonResult["error"]["inputvoltage"] = true;
		}
	}
	return result;
}

Channel *
RobotisServo::GetChannel(void) const
{
	Channel *chan = nullptr;

	if (interface != nullptr)
	{
		chan = interface->GetChannel();
	}
	return chan;
}

int
RobotisServo::WriteRegisters(uint8_t startRegister, uint8_t *registers, uint8_t numRegisters)
{
	int result = -1;
	Interface *iface = GetInterface();
	DynamixelInterface *diface = dynamic_cast<DynamixelInterface *>(iface);

	if (diface != nullptr)
	{
		result = diface->WriteRegisters(id, startRegister, registers, numRegisters, statusReturnLevel);
	}
	return result;
}

int
RobotisServo::ReadRegisters(uint8_t startRegister, uint8_t *reply, uint8_t numRegisters) const
{
	int result = -1;
	Interface *iface = GetInterface();

#ifndef NDEBUG
	if (iface == nullptr)
	{
		std::cout << "iface is nullptr.." << std::endl;
	}
	else
	{
		std::cout << "iface is NOT empty.." << std::endl;
	}
#endif
	DynamixelInterface *diface = dynamic_cast<DynamixelInterface *>(iface);

	if (diface != nullptr)
	{
		result = diface->ReadRegisters(id, startRegister, reply, numRegisters);
	}
	return result;
}

enum RobotisServo::RobotisStatusReturnLevel
RobotisServo::ReadStatusReturnLevel() const
{
	uint8_t reply[1];
	int result = -1;
	enum RobotisServo::RobotisStatusReturnLevel sret = RESPONSE_INVALID;

	result = ReadRegisters(RobotisServo::STATUS_RETURN_LEVEL, reply, sizeof(reply));
	if (result == 0)
	{
		if (reply[0] == 0)
		{
			sret = RESPONSE_NONE;
		}
		else if (reply[0] == 1)
		{
			sret = RESPONSE_READ;
		}
		else if (reply[0] == 2)
		{
			sret = RESPONSE_ALL;
		}
		else
		{
			std::cerr << "RobotisServo - ReadStatusReturnLevel failed don't know what sret " << reply[0] << " is" <<
			std::endl;
			sret = RESPONSE_INVALID;
		}
	}
	else
	{
		std::cerr << "RobotisServo - ReadStatusReturnLevel failed with error " << result << std::endl;
		sret = RESPONSE_INVALID;
	}
	return sret;
}

enum Actuator::MotorState
RobotisServo::GetMotorState(void) const
{
	uint8_t reply[1];
	int result = -1;
	enum Actuator::MotorState sact = Actuator::MOTOR_INVALID;

	result = ReadRegisters(RobotisServo::TORQUE_ENABLE, reply, sizeof(reply));
	if (result == 0)
	{
		if (reply[0] == 0)
		{
			sact = MOTOR_OFF;
		}
		else if (reply[0] == 1)
		{
			sact = MOTOR_ON;
		}
		else
		{
			std::cerr << "RobotisServo - GetMotorState failed don't know what sact " << reply[0] << " is" << std::endl;
			sact = MOTOR_INVALID;
		}
	}
	else
	{
		std::cerr << "RobotisServo - GetMotorState failed with error " << result << std::endl;
		sact = MOTOR_INVALID;
	}
	return sact;
}

int
RobotisServo::SetMotorState(enum Actuator::MotorState state)
{
	uint8_t data[1];
	int result = -1;

	if (state == Actuator::MOTOR_ON)
	{
		data[0] = 1;
	}
	else if (state == Actuator::MOTOR_OFF)
	{
		data[0] = 0;
	}
	else
	{
		ERROR("RobotisServo::SetMotorState failed, don't know how to deal with motor state " << state);
		return -1;
	}

	result = WriteRegisters(RobotisServo::TORQUE_ENABLE, data, sizeof(data));
	return result;
}

Actuator *
RobotisServo::factory(std::string name, std::string type, DynamixelInterface *iface, boost::property_tree::ptree &pt)
{
	Actuator *act = nullptr;

	if (type == "robotis_servo_rx_64")
	{
		act = new RobotisServoRX64(name, iface, pt);
	}
	else if (type == "robotis_servo_rx_28")
	{
		act = new RobotisServoRX28(name, iface, pt);
	}
	else if (type == "robotis_servo_mx_106")
	{
		act = new RobotisServoMX106(name, iface, pt);
	}
	else if (type == "robotis_servo_mx_64")
	{
		act = new RobotisServoMX64(name, iface, pt);
	}
	else if (type == "robotis_servo_mx_28")
	{
		act = new RobotisServoMX28(name, iface, pt);
	}

	return act;
}

int
RobotisServo::Open(void)
{
	int ret;

#ifndef NDEBUG
	std::cout << "RobotisServo::Open()" << std::endl;
#endif


	ret = Actuator::Open();
	if (ret == 0)
	{
		statusReturnLevel = ReadStatusReturnLevel();
		if (statusReturnLevel == RESPONSE_INVALID)
		{
			ret = -1;
		}
	}
	return ret;
}

void RobotisServo::byteToStructError(uint8_t errorByte, protocols::dynamixel1::ErrorByte *error)
{
	error->noError = (errorByte == RobotisServo::RobotisErrorBits::ERROR_NOERROR);

	error->instruction = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_INSTRUCTION) ==
						  RobotisServo::RobotisErrorBits::ERROR_INSTRUCTION);
	error->overload = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_OVERLOAD) ==
					   RobotisServo::RobotisErrorBits::ERROR_OVERLOAD);
	error->checksum = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_CHECKSUM) ==
					   RobotisServo::RobotisErrorBits::ERROR_CHECKSUM);
	error->range = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_RANGE) ==
					RobotisServo::RobotisErrorBits::ERROR_RANGE);
	error->overheating = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_OVERHEATING) ==
						  RobotisServo::RobotisErrorBits::ERROR_OVERHEATING);
	error->anglelimit = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_ANGLELIMIT) ==
						 RobotisServo::RobotisErrorBits::ERROR_ANGLELIMIT);
	error->inputvoltage = ((errorByte & RobotisServo::RobotisErrorBits::ERROR_INPUTVOLTAGE) ==
						   RobotisServo::RobotisErrorBits::ERROR_INPUTVOLTAGE);
}

void RobotisServo::init_error(protocols::dynamixel1::ErrorByte *err)
{
	err->noError = true;
	err->instruction = false;
	err->overload = false;
	err->checksum = false;
	err->range = false;
	err->overheating = false;
	err->anglelimit = false;
	err->inputvoltage = false;
}

int RobotisServo::ReadGains(servos::dynamixel::mx::Gains *gains, protocols::dynamixel1::ErrorByte *error)
{
	ERROR("Not implemented.");
	return -1;
}

int RobotisServo::ReadGains(Json::Value &jsonResult)
{
	servos::dynamixel::mx::Gains gains;
	protocols::dynamixel1::ErrorByte error;
	int ret = this->ReadGains(&gains, &error);
	if (ret == 0)
	{
		jsonResult["d"] = gains.d;
		jsonResult["i"] = gains.i;
		jsonResult["p"] = gains.p;
	}
	return ret;
}

int RobotisServo::SendPositionAndSpeedMessage(float angle, float speed, unsigned int gainD, unsigned int gainI,
	unsigned int gainP)
{
	unsigned int ticks = this->AngleRadiansToPositionTicks(home + angle);
	// unsigned int ispeed = this->SpeedToISpeed(speed);
	unsigned int ispeed = speed;

	int ret = 0;

	DynamixelInterface *dyniface;

	if (((dyniface = dynamic_cast<DynamixelInterface *>(interface)) != 0) && (dyniface->IsSyncCommandReady()))
	{
		int err;
		if ((err = dyniface->AddSyncCommand(id, gainD, gainI, gainP, ticks, ispeed)) != 0)
		{
			ret = err;
			ERROR("RobotisServo::SendPositionAndSpeed AddSyncCommand failed with code " << err);
		}
	}
	else
	{

		if (SendPositionTicksAndISpeed(ticks, ispeed, gainD, gainI, gainP))
		{
			ERROR("RobotisServo::SendPositionAndSpeed SendPositionTicksAndISpeed failed");
			ret = -5;
		}
	}
	return ret;
}
