#include <iostream>
#include <cmath>
#include "robot.hpp"
#include "kinematics_object.hpp"
#include "kinematics_head.hpp"

KinematicsHead::KinematicsHead(Robot const * robot): KinematicsObject(robot) {
    //neck transversal
    servoResolutions.push_back(1024);
    lowerServoBounds.push_back(-M_PI);
    upperServoBounds.push_back(M_PI);
    
    //neck lateral
    servoResolutions.push_back(1024);
    lowerServoBounds.push_back(-M_PI); //TODO: remeasure these values
    upperServoBounds.push_back(M_PI);
}
        
KinematicsHead::~KinematicsHead() {}

//Calculates the angles in radians required of the servos for the head to reach a given position
//returnAngles - vector where the calculated angles will be stored
//params - vector that must contain exactly 3 values: the sign of the desired x value, followed by the desired y value, followed by the desired z value
//buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
//returns 0 on success, -1 on error
int KinematicsHead::inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle) const {
    int successful = 0;
    
    if(params.size() == 3) {
        float desiredSignX = params[0];
        float goalY = params[1];
        float goalZ = params[2];
        std::vector<float> measurements;
        robot->getHeadMeasurements(measurements);
        float torsoNeckDistanceZ = measurements[1];
        float neckTransversalToNeckLateralZ = measurements[2];
        float neckLateralToCameraLateralZ = measurements[3];
        
        float deltaY = goalY;
        float deltaZ = goalZ - torsoNeckDistanceZ - neckTransversalToNeckLateralZ;
        float effectiveLength = std::sqrt(deltaY*deltaY + deltaZ*deltaZ);
        
        if(effectiveLength > neckLateralToCameraLateralZ) {
            std::cerr << "ERROR: Head position is beyond reach" << std::endl;
            successful = -1;
        } else {
            float neckLateral = std::acos(deltaZ/neckLateralToCameraLateralZ);
            float xyProj = std::abs(neckLateralToCameraLateralZ*std::sin(neckLateral));
            float neckTransversal = std::asin(goalY/xyProj);
            float neckLateralOut, neckTransversalOut;
            
            if(desiredSignX < 0) {
                neckLateralOut = neckLateral;
                neckTransversalOut = -neckTransversal;
            } else {
                neckLateralOut = -neckLateral;
                neckTransversalOut = neckTransversal;
            }
            
            returnAngles.clear();
            returnAngles.push_back(neckTransversalOut);
            returnAngles.push_back(neckLateralOut);
            
            successful = validAngles(returnAngles, buffer_angle);
        }
    } else {
        std::cerr << "ERROR: Incorrect number of parameters given to head inverse kinematics" << std::endl;
        successful = -1;
    }
    
    return successful;
}
