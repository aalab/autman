/*
 * Specification of the Robot Interfaces via various input methods.
 * Jacky Baltes <jacky@cs.umanitoba.ca>
 * Mon Oct  7 19:42:13 CDT 2013
 */

#include "robot.hpp"

#include "interface.hpp"
#include "actuator.hpp"
#include "channel.hpp"
#include "motion.hpp"
#include "motion_sequence.hpp"
#include "roboard.h"
#include "utility.hpp"
#include "udp_robot_server.hpp"
#include "kinematics_humanoid.hpp"
#include "compilerdefinitions.h"
#include "autgait.h"
#include "interfacefactory.hpp"

#include <syslog.h>
#include <ios>
#include <string>
#include <vector>
#include <algorithm>
#include <thread>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <exceptions.hpp>
#include <unix_serial_port.hpp>

Robot::Robot(std::string fname)
	: port(0),
	  opened(false),
	  nextMotionSeq(nullptr), currentMotionSequence(nullptr)
{
#ifndef NDEBUG
	std::cout << "Creating robot" << " " << std::endl;
#endif

	//measurements default to 85 cm robot
	torsoNeckDistanceX = 0.0420;
	torsoNeckDistanceZ = 0.2170;
	neckTransversalToNeckLateralZ = 0.0330;
	neckLateralToCameraLateralZ = 0.0700;
	torsoShoulderDistanceY = 0.1315;
	torsoShoulderDistanceZ = 0.1950;
	shoulderLateralToShoulderFrontalY = 0.0360;
	shoulderFrontalToElbowLateralY = 0.2000;
	elbowLateralToHandFrontalY = 0.1670;
	elbowLateralToHandFrontalZ = 0.0000; //0.0200
	torsoHipDistanceY = 0.0930;
	torsoHipDistanceZ = -0.0950;
	hipTransversalToHipFrontalZ = -0.0350;
	hipLateralToKneeLateralZ = -0.1720;
	kneeLateralToAnkleLateralZ = -0.1780;
	ankleLateralToFootFrontalZ = -0.0400;

	//gait parameters default to 85 cm
	kin = new KinematicsHumanoid(this);
	kin->gaitParams.stepX = 0.0;
	kin->gaitParams.stepY = 0.0;
	kin->gaitParams.stepAngle = 0.0;
	kin->gaitParams.maxHeightProp = 0.1;
	kin->gaitParams.transitionProp = 0.1;
	kin->gaitParams.stepHeight = 0.03;
	kin->gaitParams.hipPitch = 9.0 * M_PI / 180.0;
	kin->gaitParams.anklePitch = 0.0 * M_PI / 180.0;
	kin->gaitParams.xOffset = 0.01;
	kin->gaitParams.yOffset = 0.0;
	kin->gaitParams.zOffset = -0.5;
	kin->gaitParams.transferAnkleRoll = 0.0 * M_PI / 180.0;
	kin->gaitParams.transferHipRoll = 5.0 * M_PI / 180.0;
	kin->gaitParams.transferY = 0.02;

	LoadConfiguration(fname);
}

void
Robot::LoadConfiguration(std::string const filename)
{
	using boost::property_tree::ptree;
	ptree pt;

	read_info(filename, pt);
	LoadConfiguration(pt);
}

void
Robot::LoadConfiguration(boost::property_tree::ptree &pt)
{
	using boost::property_tree::ptree;
	this->name = pt.get<std::string>("name", "unknown");
	//robot_name = pt.get<std::string>("name","unknown");
#ifndef NDEBUG
	//std::cout << "Robot name " << robot_name << std::endl;
	std::cout << "Robot name " << this->name << std::endl;
#endif

	system = pt.get<std::string>("system");
	boost::to_lower(system);

	port = pt.get<int>("port", 0);

	defaultMotionSpeed = pt.get<float>("default_motion_speed", 100.0);
	defaultMotionEntrySpeed = pt.get<float>("default_motion_entry_speed", 1.0);

	for (auto &v : pt.get_child("channels"))
	{
		VERBOSE("Found channel " << (std::string) v.first.data());
		std::string name = (std::string) v.first.data();

		Channel *chan = nullptr;
		//chan = new Channel::factory( name, v.second );
		chan = chan->factory(name, v.second);
		if (chan != nullptr)
		{
			this->AddChannel(chan);
		}
	}

	for (auto &v : pt.get_child("interfaces"))
	{
		VERBOSE("Found interface " << (std::string) v.first.data());
		std::string name = (std::string) v.first.data();

		Interface *iface = nullptr;
		iface = InterfaceFactory::create(name, v.second, this);
		if (iface != nullptr)
		{
			AddInterface(iface);
		}
	}

	for (auto &v : pt.get_child("sensors"))
	{
		VERBOSE("Found sensor " << (std::string) v.first.data());
		std::string name = (std::string) v.first.data();
	}

	for (auto &v : pt.get_child("actuators"))
	{
		std::string name = (std::string) v.first.data();

		VERBOSE("Actuator " << name);
		VERBOSE("id " << v.second.get<unsigned int>("id"));
		VERBOSE("home " << v.second.get<float>("home"));

		Actuator *act = Actuator::factory(name, v.second, this);
		if (act != nullptr)
		{
			std::string interface = v.second.get<std::string>("interface");
			Interface *iface = this->FindInterfaceByName(interface);
			if (iface != nullptr)
			{
				act->interface = iface;
				iface->AddActuator(act);
			}
			else
			{
				ERROR("Unable to find interface " << interface << " of actuator " << act->GetName());
				std::exit(1);
			}
		}
	}

	for (auto &v : pt.get_child("walking_config"))
	{
#ifndef NDEBUG
		std::cout << "Found walking configuration " << (std::string) v.first.data() << " " <<
		(std::string) v.second.data() << std::endl;
#endif
		std::string name = (std::string) v.first.data();
		if (name == "step_x")
		{
			kin->gaitParams.stepX = stof(v.second.data());
		}
		else if (name == "step_y")
		{
			kin->gaitParams.stepY = stof(v.second.data());
		}
		else if (name == "step_angle")
		{
			kin->gaitParams.stepAngle = stof(v.second.data());
		}
		else if (name == "max_height_prop")
		{
			kin->gaitParams.maxHeightProp = stof(v.second.data());
		}
		else if (name == "transition_prop")
		{
			kin->gaitParams.transitionProp = stof(v.second.data());
		}
		else if (name == "step_height")
		{
			kin->gaitParams.stepHeight = stof(v.second.data());
		}
		else if (name == "hip_pitch")
		{
			kin->gaitParams.hipPitch = stof(v.second.data()) * M_PI / 180.0;
		}
		else if (name == "ankle_pitch")
		{
			kin->gaitParams.anklePitch = stof(v.second.data()) * M_PI / 180.0;
		}
		else if (name == "x_offset")
		{
			kin->gaitParams.xOffset = stof(v.second.data());
		}
		else if (name == "y_offset")
		{
			kin->gaitParams.yOffset = stof(v.second.data());
		}
		else if (name == "z_offset")
		{
			kin->gaitParams.zOffset = stof(v.second.data());
		}
		else if (name == "transfer_ankle_roll")
		{
			kin->gaitParams.transferAnkleRoll = stof(v.second.data()) * M_PI / 180.0;
		}
		else if (name == "transfer_hip_roll")
		{
			kin->gaitParams.transferHipRoll = stof(v.second.data()) * M_PI / 180.0;
		}
		else if (name == "transfer_y")
		{
			kin->gaitParams.transferY = stof(v.second.data());
		}
		else
		{
			ERROR("Unrecognized walking configuration name: " << name);
			std::exit(1);
		}
	}

	auto autgaitPt = pt.get_child_optional("autgait");
	if (autgaitPt)
	{
		this->serialPort.reset(new UnixSerialPort("subcontroller", *autgaitPt));
		this->serialPort->scanOpen("/dev/ttyACM", 0, 3);

		if (this->serialPort->opened)
		{
			this->autGait.reset(new AUTGait(&(*this->serialPort)));
		}
		else
		{
			ERROR("Cannot find subcontroller.");
			std::exit(1);
		}
	}

	for (auto &v : pt.get_child("measurements"))
	{
		VERBOSE("Found measurement " << (std::string) v.first.data() << " " << (std::string) v.second.data());

		std::string name = (std::string) v.first.data();
		if (name == "torso_neck_distance_x")
		{
			torsoNeckDistanceX = stof(v.second.data());
		}
		else if (name == "torso_neck_distance_z")
		{
			torsoNeckDistanceZ = stof(v.second.data());
		}
		else if (name == "neck_transversal_to_neck_lateral_z")
		{
			neckTransversalToNeckLateralZ = stof(v.second.data());
		}
		else if (name == "neck_lateral_to_camera_lateral_z")
		{
			neckLateralToCameraLateralZ = stof(v.second.data());
		}
		else if (name == "torso_shoulder_distance_y")
		{
			torsoShoulderDistanceY = stof(v.second.data());
		}
		else if (name == "torso_shoulder_distance_z")
		{
			torsoShoulderDistanceZ = stof(v.second.data());
		}
		else if (name == "shoulder_lateral_to_shoulder_frontal_y")
		{
			shoulderLateralToShoulderFrontalY = stof(v.second.data());
		}
		else if (name == "shoulder_frontal_to_elbow_lateral_y")
		{
			shoulderFrontalToElbowLateralY = stof(v.second.data());
		}
		else if (name == "elbow_lateral_to_hand_frontal_y")
		{
			elbowLateralToHandFrontalY = stof(v.second.data());
		}
		else if (name == "elbow_lateral_to_hand_frontal_z")
		{
			elbowLateralToHandFrontalZ = stof(v.second.data());
		}
		else if (name == "torso_hip_distance_y")
		{
			torsoHipDistanceY = stof(v.second.data());
		}
		else if (name == "torso_hip_distance_z")
		{
			torsoHipDistanceZ = stof(v.second.data());
		}
		else if (name == "hip_transversal_to_hip_frontal_z")
		{
			hipTransversalToHipFrontalZ = stof(v.second.data());
		}
		else if (name == "hip_lateral_to_knee_lateral_z")
		{
			hipLateralToKneeLateralZ = stof(v.second.data());
		}
		else if (name == "knee_lateral_to_ankle_lateral_z")
		{
			kneeLateralToAnkleLateralZ = stof(v.second.data());
		}
		else if (name == "ankle_lateral_to_foot_frontal_z")
		{
			ankleLateralToFootFrontalZ = stof(v.second.data());
		}
		else
		{
			std::cerr << "Unrecognized measurement name: " << name << std::endl;
			std::exit(1);
		}
	}

/*
	VERBOSE("Finding defaults");
	for (auto &v : pt.get_child("defaults"))
	{
		VERBOSE("Found defaults " << (std::string) v.first.data() << " " << (std::string) v.second.data());

		std::string name = (std::string) v.first.data();
		if (name == "gainD")
		{
			this->defaultGainD = stoi(v.second.data());
		}
		else if (name == "gainI")
		{
			this->defaultGainI = stoi(v.second.data());
		}
		else if (name == "gainP")
		{
			this->defaultGainP = stoi(v.second.data());
		}
		else
		{
			std::cerr << "Unrecognized defaults name: " << name << std::endl;
			std::exit(1);
		}
	}
 */
}

int
Robot::Open(void)
{
	if (!opened)
	{
		if (0)
		{
		}
#if defined(ENABLE_RB110)
      else if ( system == "rb-110")
	{
	  roboio_SetRBVer(RB_110);		// if your RoBoard is RB-110
	  /*
	    for(Interface * iface : interfaces)
	    {
	    iface->Open();
	    }
	  */

	  for( auto iface : interfaces )
	    {
	      iface->Open();
	    }

	}
#endif
		else if (system == "unix")
		{
			/*
			For the new gait
			for (auto iface : interfaces)
			{
				iface->Open();
			}
			 */
		}
		else
		{
			std::cerr << "Unknown robot system " << system << std::endl;
			std::exit(2);
		}
		opened = true;
	}
	return 0;
}


int
Robot::Close(void)
{
	if (opened)
	{
		PowerOff();
		for (auto iface : interfaces)
		{
			// iface->Close();
		}
		opened = false;
	}

	return 0;
}

Interface *
Robot::FindInterfaceByName(std::string name) const
{
	Interface *fnd = 0;
	std::vector<Interface *>::const_iterator it;

	it = std::find_if(interfaces.begin(),
					  interfaces.end(),
					  boost::bind(&Interface::GetName, _1) == name);
	if (it != interfaces.end())
	{
		fnd = *it;
	}
	return fnd;
}

Channel *
Robot::FindChannelByName(std::string name) const
{
	Channel *fnd = 0;
	std::vector<Channel *>::const_iterator it;

	it = std::find_if(channels.begin(),
					  channels.end(),
					  boost::bind(&Channel::GetName, _1) == name);
	if (it != channels.end())
	{
		fnd = *it;
	}
	return fnd;
}

int
Robot::AddInterface(Interface *new_iface)
{
	Interface *iface = FindInterfaceByName(new_iface->GetName());
	if (iface == nullptr)
	{
		interfaces.push_back(new_iface);
	}
	else
	{
		ERROR("Duplicate defined interface " << new_iface->GetName());
		std::exit(1);
	}
	return 0;
}

int
Robot::AddChannel(Channel *new_channel)
{
	Channel *chan = FindChannelByName(new_channel->GetName());
	if (chan == nullptr)
	{
		channels.push_back(new_channel);
	}
	else
	{
		ERROR("Duplicate defined channel " << new_channel->GetName());
		std::exit(1);
	}
	return 0;
}

int Robot::ProcessCommand(Json::Value &command, Json::Value &jsonResult)
{
	std::string name = command["name"].asString();
	int result = -1;
	if (name == this->name)
	{
		return this->InternalProcessCommand(command, jsonResult);
	}
	else
	{
		ERROR("Are you talking to me punk! My name is " << this->name << " not " << name);
		throw new exceptions::robot::NameException(name);
	}
	return result;
}

int Robot::InternalProcessCommand(Json::Value &command, Json::Value &jsonResult)
{
	int result = -1;

	std::string commandName = command["command"].asString();
	VERBOSE("'" << commandName << "' received.");

	if (commandName == "MultiCommand")
	{
		Json::Value &commands = command["commands"];
		jsonResult["success"] = true;
		jsonResult["results"] = Json::arrayValue;
		for (Json::ValueIterator commandIt = commands.begin(); commandIt != commands.end(); ++commandIt)
		{
			Json::Value jsonCommandResult;
			this->InternalProcessCommand((*commandIt), jsonCommandResult);
			jsonResult["results"].append(jsonCommandResult);
		 }
		result = 0;
	}
	else if (commandName == "ReadySync")
	{
		this->ReadySyncCommand();
		result = 0;
	}
	else if (commandName == "SendSync")
	{
		this->SendSyncCommand();
		result = 0;
	}
	else if (commandName == "ReadGains")
	{
		result = this->ReadGains(command["params"], jsonResult);
		jsonResult["success"] = (result == 0);
	}
	else if (commandName == "ReadState")
	{
		int ret;
		if ((ret = this->ReadState(command["params"], jsonResult["data"])) < 0)
		{
			ERROR("ReadState failed with error " << ret);
			jsonResult["success"] = false;
			result = -1;
		}
		else
		{
			jsonResult["success"] = true;
			result = 0;
		}
	}
	else if (commandName == "KinematicOptions")
	{
		result = 0;
		Json::Value &params = command["params"];
		std::string memberName;
		for (Json::ValueIterator member = params.begin(); member != params.end(); ++member)
		{
			memberName = member.name();
			if (memberName == "stepX")
				this->kin->gaitParams.stepX = member->asFloat();
			else if (memberName == "stepY")
				this->kin->gaitParams.stepY = member->asFloat();
			else if (memberName == "stepAngle")
				this->kin->gaitParams.stepAngle = member->asFloat();
			else if (memberName == "maxHeightProp")
				this->kin->gaitParams.maxHeightProp = member->asFloat();
			else if (memberName == "transitionProp")
				this->kin->gaitParams.transitionProp = member->asFloat();
			else if (memberName == "stepHeight")
				this->kin->gaitParams.stepHeight = member->asFloat();
			else if (memberName == "hipPitch")
				this->kin->gaitParams.hipPitch = member->asFloat();
			else if (memberName == "anklePitch")
				this->kin->gaitParams.anklePitch = member->asFloat();
			else if (memberName == "xOffset")
				this->kin->gaitParams.xOffset = member->asFloat();
			else if (memberName == "yOffset")
				this->kin->gaitParams.yOffset = member->asFloat();
			else if (memberName == "zOffset")
				this->kin->gaitParams.zOffset = member->asFloat();
			else if (memberName == "transferAnkleRoll")
				this->kin->gaitParams.transferAnkleRoll = member->asFloat();
			else if (memberName == "transferHipRoll")
				this->kin->gaitParams.transferHipRoll = member->asFloat();
			else if (memberName == "transferY")
				this->kin->gaitParams.transferY = member->asFloat();
			else
			{
				result = -1;
				jsonResult["success"] = false;
				jsonResult["message"] = "Option '" + memberName + "' is invalid.";
				break;
			}
		}

		if (result > -1)
		{
			jsonResult["success"] = true;
			jsonResult["data"]["stepX"] =  this->kin->gaitParams.stepX;
			jsonResult["data"]["stepY"] =  this->kin->gaitParams.stepY;
			jsonResult["data"]["stepAngle"] =  this->kin->gaitParams.stepAngle;
			jsonResult["data"]["maxHeightProp"] =  this->kin->gaitParams.maxHeightProp;
			jsonResult["data"]["transitionProp"] =  this->kin->gaitParams.transitionProp;
			jsonResult["data"]["stepHeight"] =  this->kin->gaitParams.stepHeight;
			jsonResult["data"]["hipPitch"] =  this->kin->gaitParams.hipPitch;
			jsonResult["data"]["anklePitch"] =  this->kin->gaitParams.anklePitch;
			jsonResult["data"]["xOffset"] =  this->kin->gaitParams.xOffset;
			jsonResult["data"]["yOffset"] =  this->kin->gaitParams.yOffset;
			jsonResult["data"]["zOffset"] =  this->kin->gaitParams.zOffset;
			jsonResult["data"]["transferAnkleRoll"] =  this->kin->gaitParams.transferAnkleRoll;
			jsonResult["data"]["transferHipRoll"] =  this->kin->gaitParams.transferHipRoll;
			jsonResult["data"]["transferY"] =  this->kin->gaitParams.transferY;
		}
	}
	else if (commandName == "ReadFullState")
	{
		int ret;
		if ((ret = this->ReadFullState(command["params"], jsonResult["data"])) < 0)
		{
			ERROR("ReadState failed with error " << ret);
			jsonResult["success"] = false;
			result = -1;
		}
		else
		{
			jsonResult["success"] = true;
			result = 0;
		}
	}
	else if (commandName == "PowerOff")
	{
		this->PowerOff();
		result = 0;
	}
	else if (commandName == "PowerOn")
	{
		this->PowerOn();
		result = 0;
	}
	else if (commandName == "Move")
	{
		std::vector<float> dx;
		std::vector<float> dy;
		std::vector<float> dtheta;

		float motionSpeed = defaultMotionSpeed;
		float motionEntrySpeed = defaultMotionEntrySpeed;

		std::vector<unsigned long long int> cycleTimeUSec;
		unsigned long int entryTimeUSec = 0;
		std::vector<unsigned int> nCycles;

		bool stop = false;

		Json::Value &params = command["params"];
		for (Json::ValueIterator member = params.begin() ; member != params.end() ; ++member)
		{
			std::string key = member.name();
			VERBOSE("'" << key << "' member found");
			if (key == "stop")
			{
				stop = true;
			}
			else if (key == "cycleTimeUSec")
			{
				if ((*member).isArray())
				{
					for (Json::Value &tmp : *member)
						cycleTimeUSec.push_back(tmp.asLargestUInt());
				}
				else
					cycleTimeUSec.push_back((*member).asLargestUInt());
			}
			else if (key == "entryTimeUSec")
			{
				entryTimeUSec = (*member).asLargestUInt();
			}
			else if (key == "cycles")
			{
				if ((*member).isArray())
				{
					for (Json::Value &tmp : (*member))
						nCycles.push_back(tmp.asUInt());
				}
				else
					nCycles.push_back((*member).asUInt());
			}
			else if (key == "motionSpeed")
			{
				motionSpeed = (*member).asFloat();
			}
			else if (key == "motionEntrySpeed")
			{
				motionEntrySpeed = (*member).asFloat();
			}
			else if (key == "dx")
			{
				if ((*member).isArray())
				{
					for (Json::Value &tmp : (*member))
						dx.push_back(tmp.asDouble());
				}
				else
					dx.push_back((*member).asDouble());
			}
			else if (key == "dy")
			{
				if ((*member).isArray())
				{
					for (Json::Value &tmp : (*member))
						dy.push_back(tmp.asDouble());
				}
				else
					dy.push_back((*member).asDouble());
			}
			else if (key == "dtheta")
			{
				if ((*member).isArray())
				{
					for (Json::Value &tmp : (*member))
						dtheta.push_back(tmp.asDouble());
				}
				else
					dtheta.push_back((*member).asDouble());
			}
			else
			{
				ERROR(name << " Robot::ProcessCommand unknown parameter " << key);
				// TODO: Throw an exception
			}
		}

		bool validValues = (dx.size() == dy.size()) && (dx.size() == dtheta.size())
						   && (dx.size() == cycleTimeUSec.size()) && (dx.size() == nCycles.size());
		if (validValues)
		{
			if (stop)
			{
				this->terminateMotion = Motion::FinishCycle;
				VERBOSE("Setting Motion to FinishCycle.");
			}
			if ((dx.size() > 0))
			{
				VERBOSE("Received motion sequence with " << dx.size() << " frames");
				VERBOSEB("\tentryTimeUSec: " << entryTimeUSec << ", motionSpeed: " << motionSpeed << ", motionEntrySpeed: " << motionEntrySpeed);
				for (unsigned int i = 0; i < dx.size(); i++)
				{
					VERBOSEB(
							"\t" << i << ") x: " << dx[i] << ", y: " << dy[i] << ", dtheta: " << dtheta[i]
							<< ", cycleTimeUSec: " << cycleTimeUSec[i] << ", cycles: " << nCycles[i]
					);
				}

				/*
				AUTGaitData autGaitData;
				this->autGait->initData(autGaitData, dx[0], dy[0], dtheta[0], 0, 0);
				if (this->autGait->sendData(&autGaitData))
				{
					result = 0;
				}
				else
				{
					result = -1;
				}
				*/
				StartMotionSequence(
						dx, dy, dtheta, cycleTimeUSec, nCycles, entryTimeUSec, motionSpeed, motionEntrySpeed
				);
			}
		}
		else
		{
			ERROR("Parameters invalid");
			ERRORB("\tDx.size = " << dx.size());
			ERRORB("\tDy.size = " << dy.size());
			ERRORB("\tDtheta.size = " << dtheta.size());
			ERRORB("\tCycles.size = " << nCycles.size());
			// TODO: Throw an exception
			result = -1;
		}
	}
	else if (commandName == "SpecialMotion")
	{
		unsigned int motion = command["params"]["motion"].asUInt();
		if (motion < 3)
		{
			AUTGaitData autGaitData;
			this->autGait->initData(autGaitData, 0, 0, 0, command["params"]["motion"].asUInt(), 0, 0);
			if (this->autGait->sendData(&autGaitData))
			{
				result = 0;
			}
			else
			{
				result = -1;
				jsonResult["success"] = false;
				jsonResult["message"] = "Cannot send command to the subcontroller.";
			}
		}
		else
		{
			ERROR("Motion out of bounds.");
			jsonResult["success"] = false;
			jsonResult["message"] = "Motion out of bounds.";
		}
	}
	else if (commandName == "OmniWalk")
	{
		if (this->autGait)
		{
			double
					vx = command["params"]["vx"].asDouble(),
					vy = command["params"]["vy"].asDouble(),
					vt = command["params"]["vt"].asDouble();

			AUTGaitMove autGaitMove;
			this->autGait->initData(autGaitMove, vx, vy, vt);
			if (this->autGait->sendData(&autGaitMove))
			{
				result = 0;
			}
			else
			{
				result = -1;
				jsonResult["success"] = false;
				jsonResult["message"] = "Cannot send command to the subcontroller.";
			}
		}
		else
		{
			result = -1;
			jsonResult["success"] = false;
			jsonResult["message"] = "AutGait is disabled.";
		}
	}
	else if (commandName == "FullMove")
	{
		if (this->autGait)
		{
			double
					vx = command["params"]["vx"].asDouble(),
					vy = command["params"]["vy"].asDouble(),
					vt = command["params"]["vt"].asDouble(),
					pan = command["params"]["pan"].asDouble(),
					tilt = command["params"]["tilt"].asDouble();

			AUTGaitData autGaitData;
			this->autGait->initData(autGaitData, vx, vy, vt, pan, tilt);
			if (this->autGait->sendData(&autGaitData))
			{
				result = 0;
			}
			else
			{
				result = -1;
				jsonResult["success"] = false;
				jsonResult["message"] = "Cannot send command to the subcontroller.";
			}
		}
		else
		{
			result = -1;
			jsonResult["success"] = false;
			jsonResult["message"] = "AutGait is disabled.";
		}
	}
	else if (commandName == "Head")
	{
		if (this->autGait)
		{
			double
					pan = command["params"]["pan"].asDouble(),
					tilt = command["params"]["tilt"].asDouble();

			AUTGaitHead autGaitHead;
			this->autGait->initData(autGaitHead, pan, tilt);
			if (this->autGait->sendData(&autGaitHead))
			{
				result = 0;
			}
			else
			{
				result = -1;
				jsonResult["success"] = false;
				jsonResult["message"] = "Cannot send command to the subcontroller.";
			}
		}
		else
		{
			result = -1;
			jsonResult["success"] = false;
			jsonResult["message"] = "AutGait is disabled.";
		}
	}
	else if (commandName == "IMUData")
	{
		jsonResult["success"] = true;
		Json::Value &euler = jsonResult["euler"];
		euler["x"] = (((this->eulerData.xHI << 8) + (this->eulerData.xLO))/1000.0);
		euler["y"] = (((this->eulerData.yHI << 8) + (this->eulerData.yLO))/1000.0);
		euler["z"] = (((this->eulerData.zHI << 8) + (this->eulerData.zLO))/1000.0);
	}
	else if (commandName == "Trajectory")
	{
		std::string mName;
		unsigned int nCycles = 1;
		std::string keyword;

		unsigned long long int cycleTimeUSec = 0;
		unsigned long long int entryTimeUSec = 0;

		float motionSpeed = defaultMotionSpeed;
		float motionEntrySpeed = defaultMotionEntrySpeed;

		Json::Value &params = command["params"];

		for (Json::ValueIterator member = params.begin() ; member != params.end() ; ++member)
		{
			keyword = member.name();
			if (keyword == "name")
			{
				mName = (*member).asString();
			}
			else if (keyword == "cycleTimeUSec")
			{
				cycleTimeUSec = (*member).asLargestUInt();
			}
			else if (keyword == "cycles")
			{
				nCycles = (*member).asUInt();
			}
			else if (keyword == "entryTimeUSec")
			{
				entryTimeUSec = (*member).asLargestUInt();
			}
			else if (keyword == "motionSpeed")
			{
				motionSpeed = (*member).asFloat();
			}
			else if (keyword == "motionEntrySpeed")
			{
				motionEntrySpeed = (*member).asFloat();
			}
			else
			{
				ERROR("unknown parameter \"" << keyword << "\"");
			}
		}

		VERBOSE("Starting motion " << mName << " for " << nCycles << " cycles");

		if ((result = this->StartMotion(mName, cycleTimeUSec, nCycles, entryTimeUSec, motionSpeed, motionEntrySpeed)) > -1)
		{
			jsonResult["success"] = true;
		}
		else
		{
			jsonResult["success"] = false;
			jsonResult["message"] = "Invalid motion.";
		}
	}
	else if (commandName == "Actuator")
	{
		Json::Value &params = command["params"];
		Actuator *act = this->FindActuatorByName(params["name"].asString());
		if (act != nullptr)
		{
			int ret = act->ProcessCommand(params);
			if (ret < 0)
			{
				result = ret;
			}
			else
			{
				result = 0;
			}
		}
		else
		{
			ERROR("Unable to find actuator '" << params["name"].asString() << "'");
			// TODO: Throw an exception.
			result = -1;
		}
	}
	else
	{
		jsonResult["success"] = false;
		jsonResult["message"] = "Unknown command '" + commandName + "'";
		ERROR("Unknown command '" << commandName << "'");
	}
	return result;
}


int
Robot::ReadySyncCommand(void)
{
	for (auto iface : interfaces)
	{
		iface->ReadySyncCommand();
	}

	return 0;
}

int
Robot::SendSyncCommand(void)
{
	for (auto iface : interfaces)
	{
		iface->SendSyncCommand();
	}
	return 0;
}

Actuator *
Robot::FindActuatorByName(std::string name) const
{
	Actuator *act = 0;

#ifndef NDEBUG
	// std::cout << "Looking for >" << name << "<" << std::endl;
#endif

	for (auto iface : interfaces)
	{
		act = iface->FindActuatorByName(name);
		if (act != 0)
		{
			break;
		}
	}
	return act;
}

int
Robot::NumberOfActuators(void) const
{
	int numActuators = 0;

	for (auto iface : interfaces)
	{
		numActuators = numActuators + iface->NumberOfActuators();
	}
	return numActuators;
}

int
Robot::ReadState(Json::Value &params, Json::Value &jsonResult) const
{
	int result = 0;
	for (auto iface : interfaces)
	{
		int ret = iface->ReadState(params, jsonResult);
		if (ret < 0)
		{
			result = -1;
		}
	}
	return result;
}

int
Robot::ReadFullState(Json::Value &params, Json::Value &jsonResult) const
{
	int result = 0;
	for (auto iface : interfaces)
	{
		int ret = iface->ReadFullState(params, jsonResult);
		if (ret < 0)
		{
			result = -1;
		}
	}
	return result;
}

int
Robot::SetMotorState(enum Actuator::MotorState onoff)
{
	int err = 0;
	terminateMotion = Motion::ImmediateTerminate;
	usleep(1000);

	for (auto iface : interfaces)
	{
		int ret = iface->SetMotorState(onoff);
		if ((err == 0) && (ret < 0))
		{
			err = ret;
		}
	}
	return err;
}

void
Robot::StartServer(bool foreground)
{
	if (this->port > 0)
	{
		if (foreground)
		{
			// this->startReadingIMU();

			server = new UDPRobotServer(io_service, port, this);
			server->StartServer(); // This will not return at the moment
		}
		else
		{
			// Do the double fork here
		}
	}
}


unsigned long long int
tdiffUSec(struct timespec const end, struct timespec const start)
{
	unsigned long long int diff;
	diff = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	return diff;
}


void
addTimeNSec(struct timespec *time, long long int disp)
{
	long long int nsec;
	int correct = 0;

	nsec = time->tv_nsec + disp;
	while (nsec < 0)
	{
		nsec = nsec + 1000000000L;
		correct--;
	}
	while (nsec > 1000000000L)
	{
		nsec = nsec - 1000000000L;
		correct++;
	}
	time->tv_nsec = nsec;
	time->tv_sec = time->tv_sec + correct;
}

int
Robot::StartMotion(std::string mName, unsigned long long int cycleTimeUSec, unsigned int nCycles,
				   unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed)
{
	int ret = -1;
	//  Motion currentMotion = Motion( this, mName );
	std::unique_ptr<Motion> currentMotion(new Motion(this, mName));

	if (currentMotion->IsValid())
	{
		RunMotion(currentMotion, cycleTimeUSec, nCycles, entryTimeUSec, motionSpeed, motionEntrySpeed);
		ret = 0;
	}
	return ret;
}

int
Robot::StartMotion(float dx, float dy, float dtheta, unsigned long long int cycleTimeUSec, unsigned int nCycles,
				   unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed)
{
	int ret = 0;

	HumanoidTrajectoryParams params(kin->gaitParams);
	params.stepX = dx;
	params.stepY = dy;
	params.stepAngle = dtheta;

	std::unique_ptr<Motion> currentMotion = kin->calcWalkingGait(params);
	if (currentMotion)
	{
		RunMotion(currentMotion, cycleTimeUSec, nCycles, entryTimeUSec, motionSpeed, motionEntrySpeed);
	}
	else
	{
		ret = -1;
	}
	return ret;
}

int
Robot::StartMotionSequence(std::vector<float> &dx, std::vector<float> &dy, std::vector<float> &dtheta,
	std::vector<unsigned long long int> &cycleTimeUSec, std::vector<unsigned int> &nCycles,
	unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed)
{
	int ret = 0;

	std::vector<MotionSequenceParams> seqParams;

	for (unsigned int i = 0; i < dx.size(); i++)
	{
		VERBOSE("dx[" << i << "]: " << dx[i] << ", dy[" << i << "]: " << dy[i] << ", dtheta[" << i << "]: " << dtheta[i]
				<< ", cycleTimeUSec[" << i << "]: " << cycleTimeUSec[i] << ", nCycles[" << i << "]: " << nCycles[i]);
		seqParams.push_back(MotionSequenceParams(dx[i], dy[i], dtheta[i], cycleTimeUSec[i], nCycles[i]));
	}

	//std::unique_ptr<MotionSequence> seq( new MotionSequence(kin, seqParams) );
	MotionSequence *seq = new MotionSequence(kin, seqParams);

	if (seq->frames.size() > 0)
	{
		if (this->currentMotionSequence == nullptr)
		{
			this->terminateMotion = Motion::Continue;
			std::thread motionRunner(RunMotionSequenceTrampoline, this, seq, entryTimeUSec, motionSpeed, motionEntrySpeed);
			motionRunner.detach();
		}
		else
		{
			VERBOSE("SCHEDULE FOR NEXT CYCLE");
			std::lock_guard<std::mutex> lockGuard(this->nextMotionSeqMutex);
			if (this->nextMotionSeq != nullptr)
				delete this->nextMotionSeq;
			this->nextMotionSeq = seq;
		}
	}
	else
	{
		ret = -1;
		ERROR("Gait calculation failed");
		delete seq;
	}
	return ret;
}

void
Robot::RunMotion(std::unique_ptr<Motion> &motion, unsigned long long int cycleTimeUSec, unsigned int nCycles,
				 unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed)
{
	struct timespec time1;
	struct timespec startCycle;
	unsigned long long int diffTimeUSec;
	unsigned int ratio;
	float percentage;

	clock_gettime(CLOCK_REALTIME, &startCycle);

	if (entryTimeUSec > 0)
	{
		do
		{
			ReadySyncCommand();
			motion->PulseMotion(0.0, motionEntrySpeed);
			SendSyncCommand();

			struct timespec request = {tv_sec: 0, tv_nsec: (20L * 1000000L)};
			int s;
			while ((s = clock_nanosleep(CLOCK_REALTIME, 0, &request, &request)) != 0);

			clock_gettime(CLOCK_REALTIME, &time1);
			diffTimeUSec = tdiffUSec(time1, startCycle);
		}
		while (diffTimeUSec < entryTimeUSec);
	}

	unsigned int cycles = 0;
	clock_gettime(CLOCK_REALTIME, &startCycle);
	while (cycles < nCycles)
	{
#ifndef NDEBUG
		// std:: cout << " HERE IS RUNMOTION" << "Cycles:"<< cycles <<" CycleTimeUSec is:"<< cycleTimeUSec<<std::endl;
#endif
		clock_gettime(CLOCK_REALTIME, &time1);
		diffTimeUSec = tdiffUSec(time1, startCycle);
#ifndef NDEBUG
		//  std::cout << "!!!!!diffTimeUSec is:" << diffTimeUSec<< std::endl;
#endif
		while (diffTimeUSec > cycleTimeUSec)
		{
			addTimeNSec(&startCycle, cycleTimeUSec * 1000L);// cout<<time1 &startCyc
			diffTimeUSec = diffTimeUSec - cycleTimeUSec;

			if ( this->terminateMotion == Motion::FinishCycle)
			{
				cycles = nCycles;
			}
			else
			{
				cycles++;
			}
			VERBOSE("END OF CYCLE!");
		}

		ratio = (diffTimeUSec * 1000L) / cycleTimeUSec;
		percentage = ((float) ratio) / 1000.0;
		if (cycles >= nCycles)
		{
			percentage = 1.0;
		}
		// percentage = 0.90;
#ifndef NDEBUG
		//std::cout<< "*******PERCENTAGE FROM robot.cpp*******" << percentage<<" And ratio is: "<< ratio << " diffTimeUSec:"<< diffTimeUSec << std::endl ;
#endif

		ReadySyncCommand();
		motion->PulseMotion(percentage, motionSpeed);
		SendSyncCommand();

		struct timespec request = {tv_sec: 0, tv_nsec: (20L * 1000000L)};
		int s;
		while ((s = clock_nanosleep(CLOCK_REALTIME, 0, &request, &request)) != 0);
	}
}

void
Robot::RunMotionSequenceTrampoline(Robot *robot, MotionSequence *motionSeq, unsigned long int entryTimeUSec,
	float motionSpeed, float motionEntrySpeed)
{
	while (motionSeq != nullptr)
	{
		robot->terminateMotion = Motion::Continue;
		robot->RunMotionSequence(motionSeq, entryTimeUSec, motionSpeed, motionEntrySpeed);

		std::lock_guard<std::mutex> lockGuard(robot->nextMotionSeqMutex);
		motionSeq = robot->nextMotionSeq;
		robot->nextMotionSeq = nullptr;
	}
}

void
Robot::RunMotionSequence(MotionSequence *motionSeq, unsigned long int entryTimeUSec, float motionSpeed,
	float motionEntrySpeed)
{
	this->currentMotionSequence = motionSeq;
	struct timespec time1;
	struct timespec startCycle;
	unsigned long long int diffTimeUSec;
	unsigned int ratio;
	float percentage;
	unsigned int currFrameNum = 0;
	MotionSequenceFrame *currFrame;
	std::unique_ptr<Motion> currMotion;
	unsigned long long int currCycleTimeUSec;
	unsigned int currNumCycles;

	clock_gettime(CLOCK_REALTIME, &startCycle);

	currFrame = motionSeq->frames[currFrameNum];
	currMotion = std::move(currFrame->motion);
	currCycleTimeUSec = currFrame->cycleTimeUSec;
	currNumCycles = currFrame->numCycles;

	if (entryTimeUSec > 0)
	{
		do
		{
			ReadySyncCommand();
			currMotion->PulseMotion(0.0, motionEntrySpeed);
			SendSyncCommand();

			struct timespec request = {tv_sec: 0, tv_nsec: (20L * 1000000L)};
			int s;
			while (
				((s = clock_nanosleep(CLOCK_REALTIME, 0, &request, &request)) != 0)
				&& (terminateMotion == Motion::Continue)
			);

			clock_gettime(CLOCK_REALTIME, &time1);
			diffTimeUSec = tdiffUSec(time1, startCycle);
		}
		while ((diffTimeUSec < entryTimeUSec) &&
			   ((terminateMotion == Motion::Continue) || (terminateMotion == Motion::FinishCycle)));
	}

	unsigned int cycles = 0;
	clock_gettime(CLOCK_REALTIME, &startCycle);
	while (
		(currFrameNum < motionSeq->frames.size())
		&& (
			(terminateMotion == Motion::Continue) || (terminateMotion == Motion::FinishCycle)
		)
	)
	{
		clock_gettime(CLOCK_REALTIME, &time1);
		diffTimeUSec = tdiffUSec(time1, startCycle);
		while (diffTimeUSec > currCycleTimeUSec)
		{
			addTimeNSec(&startCycle, currCycleTimeUSec * 1000L);
			diffTimeUSec = diffTimeUSec - currCycleTimeUSec;

			cycles++;
			// std::cout << "cycles increased" << std::endl;
			if (terminateMotion == Motion::FinishCycle)
			{
				cycles = currNumCycles;
				terminateMotion = Motion::ImmediateTerminate;
				break;
			}

			if (cycles >= currNumCycles)
			{
				currFrameNum++;
				if (currFrameNum < motionSeq->frames.size())
				{
					currFrame = motionSeq->frames[currFrameNum];
					currMotion = std::move(currFrame->motion);
					currCycleTimeUSec = currFrame->cycleTimeUSec;
					currNumCycles = currFrame->numCycles;
					cycles = 0;
				}
			}
		}

		ratio = (diffTimeUSec * 1000L) / currCycleTimeUSec;
		percentage = ((float) ratio) / 1000.0;

		if (cycles >= currNumCycles)
		{
			percentage = 1.0;
		}

		ReadySyncCommand();
		currMotion->PulseMotion(percentage, motionSpeed);
		SendSyncCommand();

		struct timespec request = {tv_sec: 0, tv_nsec: (20L * 1000000L)};
		int s;
		while ( (s = clock_nanosleep(CLOCK_REALTIME, 0, &request, &request)) != 0);
	}
	delete motionSeq;
	this->currentMotionSequence = nullptr;
}

void
Robot::getHeadMeasurements(std::vector<float> &returnVals) const
{
	returnVals.clear();
	returnVals.push_back(torsoNeckDistanceX);
	returnVals.push_back(torsoNeckDistanceZ);
	returnVals.push_back(neckTransversalToNeckLateralZ);
	returnVals.push_back(neckLateralToCameraLateralZ);
}

int
Robot::setHeadMeasurements(std::vector<float> &newVals)
{
	int successful = 0;

	if (newVals.size() != 4)
	{
		std::cerr << "ERROR: Incorrect number of parameters given to head measurement setter" << std::endl;
		successful = -1;
	}
	else
	{
		torsoNeckDistanceX = newVals[0];
		torsoNeckDistanceZ = newVals[1];
		neckTransversalToNeckLateralZ = newVals[2];
		neckLateralToCameraLateralZ = newVals[3];
	}

	return successful;
}

void
Robot::getArmMeasurements(std::vector<float> &returnVals) const
{
	returnVals.clear();
	returnVals.push_back(torsoShoulderDistanceY);
	returnVals.push_back(torsoShoulderDistanceZ);
	returnVals.push_back(shoulderLateralToShoulderFrontalY);
	returnVals.push_back(shoulderFrontalToElbowLateralY);
	returnVals.push_back(elbowLateralToHandFrontalY);
	returnVals.push_back(elbowLateralToHandFrontalZ);
}

//sets private measurement data
int Robot::setArmMeasurements(std::vector<float> &newVals)
{
	int successful = 0;

	if (newVals.size() != 6)
	{
		std::cerr << "ERROR: Incorrect number of parameters given to arm measurement setter" << std::endl;
		successful = -1;
	}
	else
	{
		torsoShoulderDistanceY = newVals[0];
		torsoShoulderDistanceZ = newVals[1];
		shoulderLateralToShoulderFrontalY = newVals[2];
		shoulderFrontalToElbowLateralY = newVals[3];
		elbowLateralToHandFrontalY = newVals[4];
		elbowLateralToHandFrontalZ = newVals[5];
	}

	return successful;
}

void Robot::getLegMeasurements(std::vector<float> &returnVals) const
{
	returnVals.clear();
	returnVals.push_back(torsoHipDistanceY);
	returnVals.push_back(torsoHipDistanceZ);
	returnVals.push_back(hipTransversalToHipFrontalZ);
	returnVals.push_back(hipLateralToKneeLateralZ);
	returnVals.push_back(kneeLateralToAnkleLateralZ);
	returnVals.push_back(ankleLateralToFootFrontalZ);
}

int Robot::setLegMeasurements(std::vector<float> &newVals)
{
	int successful = 0;

	if (newVals.size() != 6)
	{
		std::cerr << "ERROR: Incorrect number of parameters given to leg measurement setter" << std::endl;
		successful = -1;
	}
	else
	{
		torsoHipDistanceY = newVals[0];
		torsoHipDistanceZ = newVals[1];
		hipTransversalToHipFrontalZ = newVals[2];
		hipLateralToKneeLateralZ = newVals[3];
		kneeLateralToAnkleLateralZ = newVals[4];
		ankleLateralToFootFrontalZ = newVals[5];
	}

	return successful;
}

bool Robot::isNumber(std::string &potentialNumber)
{
	return (strspn(potentialNumber.c_str(), "+-.0123456789") == potentialNumber.size());
}

std::string Robot::readNumsToVector(std::vector<float> &receiveVector, std::istringstream &ins)
{
	bool done = false;
	std::string potentialNum;
	std::string returnString = "";

	receiveVector.clear();
	while (!done && !ins.eof())
	{
		ins >> potentialNum;
		if (isNumber(potentialNum))
		{
			try
			{
				receiveVector.push_back(boost::lexical_cast<float>(potentialNum));
			}
			catch (...)
			{
				std::cerr << "ERROR: Incorrect numerical type given; was expecting float" << std::endl;
			}
		}
		else
		{
			returnString = potentialNum;
			done = true;
		}
	}

	return returnString;
}

std::string Robot::readNumsToVector(std::vector<unsigned int> &receiveVector, std::istringstream &ins)
{
	bool done = false;
	std::string potentialNum;
	std::string returnString = "";

	receiveVector.clear();
	while (!done && !ins.eof())
	{
		ins >> potentialNum;
		if (isNumber(potentialNum))
		{
			try
			{
				receiveVector.push_back(boost::lexical_cast<unsigned int>(potentialNum));
			}
			catch (...)
			{
				std::cerr << "ERROR: Incorrect numerical type given; was expecting unsigned int" << std::endl;
			}
		}
		else
		{
			returnString = potentialNum;
			done = true;
		}
	}
	return returnString;
}

std::string Robot::readNumsToVector(std::vector<unsigned long long int> &receiveVector, std::istringstream &ins)
{
	bool done = false;
	std::string potentialNum;
	std::string returnString = "";

	receiveVector.clear();
	while (!done && !ins.eof())
	{
		ins >> potentialNum;
		if (isNumber(potentialNum))
		{
			try
			{
				receiveVector.push_back(boost::lexical_cast<unsigned long long int>(potentialNum));
			}
			catch (...)
			{
				std::cerr << "ERROR: Incorrect numerical type given; was expecting unsigned long long int" << std::endl;
			}
		}
		else
		{
			returnString = potentialNum;
			done = true;
		}
	}
	return returnString;
}

int Robot::ReadGains(Json::Value &params, Json::Value &result)
{
	Json::Value &jsonActuators = params["actuators"];
	if (jsonActuators.isArray())
	{
		Actuator *actuator;
		std::string actuatorName;
		bool allFound = true;
		for (Json::Value &jsonActuator: jsonActuators)
		{
			actuatorName = jsonActuator.asString();
			actuator = this->FindActuatorByName(actuatorName);
			allFound = allFound && (actuator == nullptr);
			if (actuator == nullptr)						// Not found
			{
				result["actuators"][actuatorName] = false;
			}
			else
			{
				if (actuator->ReadGains(result["actuators"][actuatorName]) != 0)
				{
					result["actuators"][actuatorName] = false;
				}
			}
		}
	}
	else
	{
		for (Interface *iface : this->interfaces)
		{
			for (Actuator *actuator : iface->GetActuators())
			{
				std::string &actuatorName = actuator->name;
				if (actuator->ReadGains(result["actuators"][actuatorName]) != 0)
				{
					result["actuators"][actuatorName] = false;
				}
			}
		}
	}
	return 0;
}

void Robot::RunReadIMUTrampoline(Robot *robot)
{
	robot->readIMU();
}

void Robot::startReadingIMU()
{
	std::thread thread(RunReadIMUTrampoline, this);
	thread.detach();
}

void Robot::readIMU()
{
	std::chrono::system_clock::time_point noErrorSince = std::chrono::system_clock::now();
	while (true)
	{
		VERBOSE("START ]------");
		if (this->autGait->readData(this->eulerData))
		{
			noErrorSince = std::chrono::system_clock::now();
			VERBOSE("EULER Data:");
			VERBOSEB("\t+ X: " << ((this->eulerData.xHI << 8) + (this->eulerData.xLO))/1000.0);
			VERBOSEB("\t+ Y: " << ((this->eulerData.yHI << 8) + (this->eulerData.yLO))/1000.0);
			VERBOSEB("\t+ Z: " << ((this->eulerData.zHI << 8) + (this->eulerData.zLO))/1000.0);
			usleep(60000);
		}
		else
		{
			if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - noErrorSince).count() > 15)
			{
				this->serialPort->scanOpen(0, 3);
				sleep(1);
			}
			else
			{
				usleep(10000);
			}
		}
		VERBOSE("END   ]------");
	}
}
