/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 22, 2015
 **/

#ifndef MOTIONLIBRARY_INTERFACEFACTORY_HPP
#define MOTIONLIBRARY_INTERFACEFACTORY_HPP

#include <interface.hpp>

#include <string>

class InterfaceFactory
{
public:
	static Interface* create(std::string name, boost::property_tree::ptree &pt, Robot const *robot);
};


#endif //MOTIONLIBRARY_INTERFACEFACTORY_HPP
