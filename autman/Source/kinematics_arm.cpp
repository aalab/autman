#include <iostream>
#include <cmath>
#include "robot.hpp"
#include "kinematics_object.hpp"
#include "kinematics_arm.hpp"

KinematicsArm::KinematicsArm(Robot const * robot, bool isLeft): KinematicsObject(robot) 
{
	this->isLeft = isLeft;
    
    //shoulder lateral
    servoResolutions.push_back(1024);
    lowerServoBounds.push_back(-M_PI);
    upperServoBounds.push_back(M_PI);
    
    if(isLeft) {
        //shoulder frontal
        servoResolutions.push_back(1024);
        lowerServoBounds.push_back(-1.88);
        upperServoBounds.push_back(1.60);
        
        //elbow lateral
        servoResolutions.push_back(1024);
        lowerServoBounds.push_back(-1.93);
        upperServoBounds.push_back(1.88);
    } else {
        //shoulder frontal
        servoResolutions.push_back(1024);
        lowerServoBounds.push_back(-1.61);
        upperServoBounds.push_back(1.90);
        
        //elbow lateral
        servoResolutions.push_back(1024);
        lowerServoBounds.push_back(-1.95);
        upperServoBounds.push_back(1.88);
    }
}
        
KinematicsArm::~KinematicsArm() 
{
}

//Calculates the angles in radians required of the servos for the head to reach a given position
//returnAngles - vector where the calculated angles will be stored
//params - vector that must contain exactly 3 values: the sign of the desired x value, followed by the desired y value, followed by the desired z value
//buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
//returns 0 on success, -1 on error
int KinematicsArm::inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle) const 
{
    int successful = 0;
    
    if(params.size() == 3) {
        float goalX = params[0];
        float goalY = params[1];
        float goalZ = params[2];
        std::vector<float> measurements;
        robot->getLegMeasurements(measurements);
        float torsoShoulderDistanceY = measurements[0];
        float torsoShoulderDistanceZ = measurements[1];
        float shoulderLateralToShoulderFrontalY = measurements[2];
        float shoulderFrontalToElbowLateralY = measurements[3];
        float elbowLateralToHandFrontalY = measurements[4];
        int signFactor;
        float deltaX, deltaY, deltaZ, effectiveLength;
        
        if(isLeft) {
            signFactor = +1;
        } else {
            signFactor = -1;
        }
        
        deltaX = goalX;
        deltaY = goalY - signFactor*(torsoShoulderDistanceY + shoulderLateralToShoulderFrontalY);
        deltaZ = goalZ - torsoShoulderDistanceZ;
        effectiveLength = std::sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);
        
        if(effectiveLength > shoulderFrontalToElbowLateralY + elbowLateralToHandFrontalY) {
            if(isLeft) {
                std::cerr << "ERROR: Left arm position is beyond reach" << std::endl;
            } else {
                std::cerr << "ERROR: Right arm position is beyond reach" << std::endl;
            }
            successful = -1;
        } else {
            float shoulderLateralOut, shoulderFrontalOut, elbowLateralOut;
            
            float elbowLateral = std::acos((effectiveLength*effectiveLength - shoulderFrontalToElbowLateralY*shoulderFrontalToElbowLateralY - elbowLateralToHandFrontalY*elbowLateralToHandFrontalY) / (-2.0*shoulderFrontalToElbowLateralY*elbowLateralToHandFrontalY));
            
            float shoulderFrontal = std::acos((goalY - signFactor*(torsoShoulderDistanceY + shoulderLateralToShoulderFrontalY)) / (shoulderFrontalToElbowLateralY + elbowLateralToHandFrontalY*std::sin(elbowLateral - M_PI/2.0)));
            
            float shoulderToElbowProj = shoulderFrontalToElbowLateralY*std::sin(shoulderFrontal);
            float elbowToHandProj = std::sqrt(std::pow(elbowLateralToHandFrontalY*std::cos(elbowLateral-M_PI/2.0), 2) + std::pow(elbowLateralToHandFrontalY*std::sin(elbowLateral-M_PI/2.0)*std::sin(shoulderFrontal), 2));
            float effectiveXZProj = std::sqrt(deltaX*deltaX + deltaZ*deltaZ);
            float effectiveShoulderLateral = std::asin(deltaX / effectiveXZProj);
            float elbowLateralXZProj = std::acos((effectiveXZProj*effectiveXZProj - shoulderToElbowProj*shoulderToElbowProj - elbowToHandProj*elbowToHandProj) / (-2.0*shoulderToElbowProj*elbowToHandProj));
            float shoulderLateral = effectiveShoulderLateral - std::asin(elbowToHandProj*std::sin(elbowLateralXZProj)/effectiveXZProj);
            
            if(isLeft) {
                elbowLateralOut = -(M_PI-elbowLateral);
                shoulderFrontalOut = shoulderFrontal;
                shoulderLateralOut = -shoulderLateral;
            } else {
                elbowLateralOut = M_PI-elbowLateral;
                shoulderFrontalOut = -(M_PI - shoulderFrontal);
                shoulderLateralOut = shoulderLateral;
            }
            
            returnAngles.clear();
            returnAngles.push_back(shoulderLateralOut);
            returnAngles.push_back(shoulderFrontalOut);
            returnAngles.push_back(elbowLateralOut);
            
            successful = validAngles(returnAngles, buffer_angle);
        }
    } else {
        if(isLeft) {
            std::cerr << "ERROR: Incorrect number of parameters given to left arm inverse kinematics" << std::endl;
        } else {
            std::cerr << "ERROR: Incorrect number of parameters given to right arm inverse kinematics" << std::endl;
        }
        successful = -1;
    }
    
    return successful;
}
