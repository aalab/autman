/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 08, 2015
 */

#include "autindexes.h"

AutIndex::AutIndex()
{ }

AutIndex::AutIndex(std::string name, int index): name(name), index(index)
{ }

std::vector<AutIndex> AutIndexes::indexes;

void AutIndexes::initIndexes()
{
	AutIndexes::indexes.emplace_back("Motion_Resolution ", 1);
	AutIndexes::indexes.emplace_back("Gait_Frequency", 2);
	AutIndexes::indexes.emplace_back("Double_Support_Sleep", 3);
	AutIndexes::indexes.emplace_back("Single_Support_Sleep", 4);
	AutIndexes::indexes.emplace_back("Fly_Roll_Gain ", 5);
	AutIndexes::indexes.emplace_back("Fly_Pitch_Gain", 6);
	AutIndexes::indexes.emplace_back("Fly_Yaw_Gain", 7);
	AutIndexes::indexes.emplace_back("Fly_X_Swing_Gain", 8);
	AutIndexes::indexes.emplace_back("Fly_Y_Swing_Gain", 9);
	AutIndexes::indexes.emplace_back("Fly_Z_Swing_Gain", 10);
	AutIndexes::indexes.emplace_back("Support_Roll_Gain ", 11);
	AutIndexes::indexes.emplace_back("Support_Pitch_Gain", 12);
	AutIndexes::indexes.emplace_back("Support_Yaw_Gain", 13);
	AutIndexes::indexes.emplace_back("Support_X_Swing_Gain", 14);
	AutIndexes::indexes.emplace_back("Support_Y_Swing_Gain", 15);
	AutIndexes::indexes.emplace_back("Support_Z_Swing_Gain", 16);
	AutIndexes::indexes.emplace_back("Body_X_Swing_Gain ", 17);
	AutIndexes::indexes.emplace_back("Body_Y_Swing_Gain ", 18);
	AutIndexes::indexes.emplace_back("Body_Z_Swing_Gain ", 19);
	AutIndexes::indexes.emplace_back("Stablizer_Arm_Pitch_Gain", 20);
	AutIndexes::indexes.emplace_back("Stablizer_Arm_Roll_Gain ", 21);
	AutIndexes::indexes.emplace_back("Stablizer_Arm_Elbow_Gain", 22);
	AutIndexes::indexes.emplace_back("Stablizer_Hip_Roll_Gain ", 23);
	AutIndexes::indexes.emplace_back("Stablizer_Hip_Pitch_Gain", 24);
	AutIndexes::indexes.emplace_back("Stablizer_Knee_Gain ", 25);
	AutIndexes::indexes.emplace_back("Stablizer_Foot_Pitch_Gain ", 26);
	AutIndexes::indexes.emplace_back("Stablizer_Foot_Roll_Gain", 27);
	AutIndexes::indexes.emplace_back("Stablizer_COM_X_Shift_Gain", 28);
	AutIndexes::indexes.emplace_back("Stablizer_COM_Y_Shift_Gain", 29);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Arm_Pitch_Gain ", 30);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Arm_Roll_Gain", 31);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Arm_Elbow_Gain ", 32);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Hip_Roll_Gain", 33);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Hip_Pitch_Gain ", 34);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Knee_Gain", 35);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Foot_Pitch_Gain", 36);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_Foot_Roll_Gain ", 37);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_COM_X_Shift_Gain ", 38);
	AutIndexes::indexes.emplace_back("Gyro_Stablizer_COM_Y_Shift_Gain ", 39);
	AutIndexes::indexes.emplace_back("Stablizer_Hopping_Gait_X_Gain ", 40);
	AutIndexes::indexes.emplace_back("Stablizer_Hopping_Gait_Y_Gain ", 41);
	AutIndexes::indexes.emplace_back("COM_X_offset", 42);
	AutIndexes::indexes.emplace_back("COM_Y_offset", 43);
	AutIndexes::indexes.emplace_back("COM_Z_offset", 44);
	AutIndexes::indexes.emplace_back("COM_Roll_offset ", 45);
	AutIndexes::indexes.emplace_back("COM_Pitch_offset", 46);
	AutIndexes::indexes.emplace_back("COM_Yaw_offset", 47);
	AutIndexes::indexes.emplace_back("Left_Leg_Hip_Yaw_Offset ", 48);
	AutIndexes::indexes.emplace_back("Left_Leg_Hip_Roll_Offset", 49);
	AutIndexes::indexes.emplace_back("Left_Leg_Hip_Pitch_Offset ", 50);
	AutIndexes::indexes.emplace_back("Left_Leg_Knee_Offset", 51);
	AutIndexes::indexes.emplace_back("Left_Leg_Foot_Pitch_Offset", 52);
	AutIndexes::indexes.emplace_back("Left_Leg_Foot_Roll_Offset ", 53);
	AutIndexes::indexes.emplace_back("Right_Leg_Hip_Yaw_Offset", 54);
	AutIndexes::indexes.emplace_back("Right_Leg_Hip_Roll_Offset ", 55);
	AutIndexes::indexes.emplace_back("Right_Leg_Hip_Pitch_Offset", 56);
	AutIndexes::indexes.emplace_back("Right_Leg_Knee_Offset ", 57);
	AutIndexes::indexes.emplace_back("Right_Leg_Foot_Pitch_Offset ", 58);
	AutIndexes::indexes.emplace_back("Right_Leg_Foot_Roll_Offset", 59);
	AutIndexes::indexes.emplace_back("Left_Leg_X_Offset ", 60);
	AutIndexes::indexes.emplace_back("Left_Leg_Y_Offset ", 61);
	AutIndexes::indexes.emplace_back("Left_Leg_Z_Offset ", 62);
	AutIndexes::indexes.emplace_back("Left_Leg_Roll_Offset", 63);
	AutIndexes::indexes.emplace_back("Left_Leg_Pitch_Offset ", 64);
	AutIndexes::indexes.emplace_back("Left_Leg_Yaw_Offset ", 65);
	AutIndexes::indexes.emplace_back("Right_Leg_X_Offset", 66);
	AutIndexes::indexes.emplace_back("Right_Leg_Y_Offset", 67);
	AutIndexes::indexes.emplace_back("Right_Leg_Z_Offset", 68);
	AutIndexes::indexes.emplace_back("Right_Leg_Roll_Offset ", 69);
	AutIndexes::indexes.emplace_back("Right_Leg_Pitch_Offset", 70);
	AutIndexes::indexes.emplace_back("Right_Leg_Yaw_Offset", 71);
	AutIndexes::indexes.emplace_back("R_Arm_Pitch_offset", 72);
	AutIndexes::indexes.emplace_back("R_Arm_Roll_offset ", 73);
	AutIndexes::indexes.emplace_back("R_Arm_Elbow_offset", 74);
	AutIndexes::indexes.emplace_back("L_Arm_Pitch_offset", 75);
	AutIndexes::indexes.emplace_back("L_Arm_Roll_offset ", 76);
	AutIndexes::indexes.emplace_back("L_Arm_Elbow_offset", 77);
	AutIndexes::indexes.emplace_back("Fall_Roll_Thershold ", 78);
	AutIndexes::indexes.emplace_back("Fall_Pitch_Thershold", 79);
	AutIndexes::indexes.emplace_back("IMU_X_Angle_Offset", 80);
	AutIndexes::indexes.emplace_back("IMU_Y_Angle_Offset", 81);
	AutIndexes::indexes.emplace_back("Gyro_X_LowPass_Gain ", 82);
	AutIndexes::indexes.emplace_back("Gyro_Y_LowPass_Gain ", 83);
	AutIndexes::indexes.emplace_back("Kalman_Roll_RM_Rate ", 84);
	AutIndexes::indexes.emplace_back("Kalman_Pitch_RM_Rate", 85);
	AutIndexes::indexes.emplace_back("Kalman_Yaw_RM_Rate", 86);
	AutIndexes::indexes.emplace_back("Vx_Smoothing_Ratio", 87);
	AutIndexes::indexes.emplace_back("Vy_Smoothing_Ratio", 88);
	AutIndexes::indexes.emplace_back("Vt_Smoothing_Ratio", 89);
	AutIndexes::indexes.emplace_back("Leg_Length", 90);
	AutIndexes::indexes.emplace_back("Head_Pan_Speed", 91);
	AutIndexes::indexes.emplace_back("Head_Tilt_Speed ", 92);
	AutIndexes::indexes.emplace_back("Min_Voltage_Limit ", 93);
	AutIndexes::indexes.emplace_back("Vx_Offset ", 94);
	AutIndexes::indexes.emplace_back("Vy_Offset ", 95);
	AutIndexes::indexes.emplace_back("Vt_Offset ", 96);
	AutIndexes::indexes.emplace_back("Left_Leg_Hip_Pitch_Offset_Original", 97);
	AutIndexes::indexes.emplace_back("Right_Leg_Hip_Pitch_Offset_Original ", 98);
	AutIndexes::indexes.emplace_back("Left_Leg_Hip_Pitch_Offset_Backwards ", 99);
	AutIndexes::indexes.emplace_back("Right_Leg_Hip_Pitch_Offset_Backwards", 100);
}
