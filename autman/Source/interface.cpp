/*
 * Interfaces Class
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#include "interface.hpp"

#include "actuator.hpp"
#include "dynamixel_interface.hpp"
#include "rc_servo_gpio_interface.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include "compilerdefinitions.h"

#include <string>
#include <algorithm>
#include <boost/io/ios_state.hpp>
#include <iomanip>
#include <dynamixel2_interface.hpp>

Interface::Interface(std::string name, boost::property_tree::ptree pt, Robot const *robot)
	: opened(false), type(INVALID), name(name), robot(robot)
{ }

int
Interface::Open(void)
{
	if (!opened)
	{
		opened = true;
	}
	return 0;
}

int
Interface::Close(void)
{
	int result = 0;
	if (opened)
	{
		result = PowerOff();
		opened = false;
	}
	return result;
}

int
Interface::AddActuator(Actuator *new_act)
{
	actuators.push_back(new_act);
	return 0;
}

Actuator *
Interface::FindActuatorByName(std::string name)
{
	Actuator *fnd = 0;
	std::vector<Actuator *>::iterator it;

	it = std::find_if(actuators.begin(),
					  actuators.end(),
					  boost::bind(&Actuator::GetName, _1) == name);
	if (it != actuators.end())
	{
		fnd = *it;
	}
	return fnd;
}

/*
int
Interface::CommandTxAndRx( uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[], unsigned int const replyLength )
{
  // Return an error
  return -1;
}
*/

int
Interface::ReadState(Json::Value &jsonCommand, Json::Value &jsonResult)
{
	int result = 0;
	if (!jsonCommand.isNull())
	{
		Json::Value &jsonActuators = jsonCommand["actuators"];
		if (jsonActuators.isArray() && (!jsonActuators.empty()))
		{
			std::string n;
			for (Json::Value &jsonActuator : jsonActuators)
			{
				n = jsonActuator.asString();
				for (auto act : actuators)
				{
					if (n == act->GetName())
					{
						int ret = act->ReadState(jsonResult[act->GetName()]);
						if (ret)
						{
							result = -1;
						}
					}
				}
			}
			return result;
		}
	}

	for (auto act : actuators)
	{
		int ret = act->ReadState(jsonResult[act->GetName()]);
		if (ret)
		{
			result = -1;
		}
	}
	return result;
}

int
Interface::ReadFullState(Json::Value &jsonCommand, Json::Value &jsonResult)
{
	int result = 0;
	if (!jsonCommand.isNull())
	{
		Json::Value &jsonActuators = jsonCommand["actuators"];
		if (jsonActuators.isArray() && (!jsonActuators.empty()))
		{
			std::string n;
			for (Json::Value &jsonActuator : jsonActuators)
			{
				n = jsonActuator.asString();
				for (auto act : actuators)
				{
					if (n == act->GetName())
					{
						int ret = act->ReadFullState(jsonResult[act->GetName()]);
						if (ret)
						{
							result = -1;
						}
					}
				}
			}
			return result;
		}
	}

	for (auto act : actuators)
	{
		int ret = act->ReadFullState(jsonResult[act->GetName()]);
		if (ret)
		{
			result = -1;
		}
	}
	return result;
}

int
Interface::NumberOfActuators(void) const
{
	return actuators.size();
}

int
Interface::SetMotorState(enum Actuator::MotorState onoff)
{
	int err = 0;
	for (auto act : actuators)
	{
		usleep(20000);
		int ret = act->SetMotorState(onoff);
		if (ret)
		{
			err = -1;
		}
	}
	return err;
}

int
Interface::PulseMotion(std::string mName, float percentage, float motionSpeed)
{
	int err = 0;
	for (auto act : actuators)
	{
		Trajectory const *t = act->FindTrajectoryByName(mName);
		if (t != nullptr)
		{
			std::cout << "Before t interpolate: t->GetSize()" << t->GetSize() << std::endl;
			float angle = t->Interpolate(percentage);
			std::cout << "After t interpolate: t->GetSize()" << t->GetSize() << std::endl;
			//std::ostringstream cmd;
			// cmd << "Actuator " << act->GetName() << " Position "  << " Angle "<< angle  << " Speed " << speed;
			// act->ProcessCommand( cmd.str() );
			act->SendPositionAndSpeedMessage(angle, motionSpeed);
#ifndef NDEBUG
			std::cout << "PulseMotion::Actuator Name: " << act->GetName() << " Angle:" << angle << " Speed " <<
			motionSpeed << " Percentage is:" << percentage << " t->" << t->GetSize();
#endif
		}
	}
	return err;
}
