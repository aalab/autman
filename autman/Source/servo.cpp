/*
 * Servo Motor Class
 */

#include "servo.hpp"
#include "trajectory.hpp"
#include "compilerdefinitions.h"

#include <string>
#include <climits>
#include <cmath>

Servo::Servo(std::string name, Interface *interface, boost::property_tree::ptree &pt)
	: Actuator(name, interface, pt),
	  min(-M_PI),
	  max(+M_PI),
	  hasFeedback(false)
{
}

int
Servo::ReadState(std::string &pos)
{
	std::cout << "ERROR..Servo::ReadState..ERROR" << std::endl;
	return -1;
}

int
Servo::SetMotorState(enum MotorState state)
{
	std::cout << "ERROR..Servo::SetMotorState..ERROR" << std::endl;
	return -1;
}

int
Servo::ProcessCommand(Json::Value &json)
{
	int ret;
	if (json["command"].asString() == "Trajectory")
	{
		Trajectory trajTmp(name, "NO NAME YET");
		ret = trajTmp.Parse(name, json);
		if (ret == 0)
		{
			VERBOSE("servo accepted trajectory " << trajTmp << ".");
			VERBOSE("NAME---: " << name);
			AddTrajectory(trajTmp);
		}
	}
	else
		ret = Actuator::ProcessCommand(json);
	return ret;
}

int
Servo::ParsePositionAndSpeedMessage(Json::Value &json, float *pangle, float *pspeed, int *pgainD, int *pgainI,
	int *pgainP)
{
	int ret = 0;

	std::string name;
	for (Json::ValueIterator member = json.begin() ; member != json.end() ; member++)
	{
		name = member.name();
		if (name == "angle")
		{
			if (pangle != nullptr)
				*pangle = (*member).asFloat();
		}
		else if (name == "speed")
		{
			if (pspeed != nullptr)
				*pspeed = (*member).asFloat();
		}
		else if (name == "gainD")
		{
			if (pgainD != nullptr)
				*pgainD = (*member).asUInt();
		}
		else if (name == "gainI")
		{
			if (pgainI != nullptr)
				*pgainI = (*member).asUInt();
		}
		else if (name == "gainP")
		{
			if (pgainP != nullptr)
				*pgainP = (*member).asUInt();
		}
		else if (name == "name" || name == "command")
		{ }
		else
		{
			ERROR("unable to parse joint command. Unknown '" << name << "' parameter.");
			ret = -3;
		}
	}
	return ret;
}

int
Servo::SendPositionAndSpeedMessage(float angle, float speed)
{
	ERROR("ERROR");
	return -1;
}

enum Actuator::MotorState
Servo::GetMotorState(void) const
{
	ERROR("ERROR");
	return MOTOR_INVALID;
}

int Servo::SendPositionAndSpeedMessage(float angle, float speed, int gainD, int gainI, int gainP)
{
	ERROR("ERROR");
	return -1;
}
