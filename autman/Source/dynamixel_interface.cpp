/*
 * Interface Class for Dynamixel Servos on Serial Line
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#include "dynamixel_interface.hpp"
#include "channel.hpp"
#include "interface.hpp"
#include "roboard.h"
#include "robot.hpp"
#include "robotis_servo.hpp"
#include "robotis_servo_mx_series.hpp"
#include "compilerdefinitions.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <cstdio>

#ifndef NDEBUG

#include <boost/io/ios_state.hpp>
#include <iomanip>

#endif

struct
{
	int num;
	std::string typeName;
} servoTypes[] =
	{
		{num: 29, typeName: "robotis_servo_mx_28"},
//    { num: 7424,      typeName: "robotis_servo_mx_28"  },
		{num: 310, typeName: "robotis_servo_mx_64"},
//    { num: 13824,     typeName: "robotis_servo_mx_64"  },
		{num: 320, typeName: "robotis_servo_mx_106"},
		{num: 28, typeName: "robotis_servo_rx_28"},
		{num: 64, typeName: "robotis_servo_rx_64"},
		{num: 12, typeName: "robotis_servo_ax_12"},
		{num: 18, typeName: "robotis_servo_ax_18"}
	};


DynamixelInterface::DynamixelInterface(std::string name, boost::property_tree::ptree pt, Robot const *robot)
	: Interface(name, pt, robot),
	  channel(nullptr),
	  syncCommandReady(false),
	  syncOut(0)
{
	VERBOSE("Creating Dynamixel Interface " << name);

	std::string channelName = pt.get<std::string>("channel");
	if (channel == nullptr)
	{
		Channel *tchan = nullptr;
		if ((tchan = robot->FindChannelByName(channelName)) != nullptr)
		{
			VERBOSE("Setting channel to " << tchan->GetName());
			channel = tchan;
			//tchan->SetInterface( this );
		}
		else
		{
			ERROR("Cannot find channel " << channelName);
			std::exit(46);
		}
		channel = tchan;
		tchan->SetInterface(this->GetInterface());
	}
	else
	{
		ERROR("Channel already defined " << channelName);
		std::exit(46);
	}
	type = DYNAMIXEL;
}

int
DynamixelInterface::Open(void)
{
	int result = -1;
	if ((result = OpenChannel()) == 0)
	{
		BOOST_FOREACH(Actuator *act, actuators)
		{
			int ret = act->Open();
			if (ret)
			{
				result = -1;
			}
		}

	}
	return result;
}

int
DynamixelInterface::Close(void)
{
	int res = -1;

	res = Interface::Close();
	if (opened)
	{
	}
	opened = false;

	return res;
}

int
DynamixelInterface::CommandTxAndRx(uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[],
	unsigned int const replyLength)
{
	return channel->CommandTxAndRx(buffer, bufferLength, reply, replyLength);
}

int
DynamixelInterface::AddSyncCommand(unsigned int id, unsigned int gainD, unsigned int gainI, unsigned int gainP, unsigned int ticks, unsigned int ispeed)
{
	int ret = -1;

	/*
	VERBOSE("AddSyncCommand");
	VERBOSEB("\t- called syncOut=" << syncOut);
	VERBOSEB("\t- number of entries=" << sizeof(syncCommands) / sizeof(syncCommands[0]));
	*/

	if ((syncCommandReady) && (syncOut < sizeof(syncCommands) / sizeof(syncCommands[0])))
	{
		syncCommands[syncOut].id = id;
		syncCommands[syncOut].data.gains.d = static_cast<uint8_t>(gainD);
		syncCommands[syncOut].data.gains.i = static_cast<uint8_t>(gainI);
		syncCommands[syncOut].data.gains.p = static_cast<uint8_t>(gainP);
		syncCommands[syncOut].data.padding1 = 0;

		syncCommands[syncOut].data.positionSpeed.position_LO = static_cast<uint8_t>(LOBYTE(ticks));
		syncCommands[syncOut].data.positionSpeed.position_HI = static_cast<uint8_t>(HIBYTE(ticks));
		syncCommands[syncOut].data.positionSpeed.speed_LO = static_cast<uint8_t>(LOBYTE(ispeed));
		syncCommands[syncOut].data.positionSpeed.speed_HI = static_cast<uint8_t>(HIBYTE(ispeed));

		syncOut++;
		ret = 0;
	}
	return ret;
}

int
DynamixelInterface::ReadySyncCommand(void)
{
	syncCommandReady = true;
	return 0;
}

int
DynamixelInterface::SendSyncCommand(void)
{
	// Code here to send the sync write message
	int ret = -1;
	VERBOSE(this->syncOut);
	if (this->syncOut > 0)
	{
		unsigned int bufferLength = this->calculateBufferLength(3 /* package */ + (syncOut * sizeof(protocols::dynamixel1::SyncWriteGainsPosition)));
		// unsigned int bufferLength = 7 + (syncOut * sizeof(protocols::dynamixel1::SyncWriteGainsPosition)) + 1;
		uint8_t buffer[bufferLength];
		uint8_t out;
		out = this->prepareBufferSyncCommand(buffer, bufferLength, RobotisServoMXSeries::D_GAIN, sizeof(protocols::dynamixel1::SyncWriteGainsPosition) - 1 /* ID byte on the protocols::dynamixel1::SyncWriteGainsPosition struct */);
		this->calculateBufferChecksum(buffer, bufferLength);

		VERBOSENEL("SendSyncCommand bufferLength " << bufferLength << " out " << out << "[");
		VERBOSEDATA(buffer, bufferLength);
		VERBOSEB("]");

		bool err = channel->CommandTxAndRx(buffer, bufferLength, 0, 0);
		if (err)
		{
			ret = 0;
		}
		else
		{
			ret = -1;
		}
		syncCommandReady = 0;
		syncOut = 0;
	}
	else
	{
		ERROR("returns: " << syncOut);
		ERROR(
			"Exiting the program.. better check the config file for a better parameters.(It's just a suggestion ;) ) "
			<< syncOut
		);
		exit(EXIT_FAILURE);
	}
	return ret;
}

int
DynamixelInterface::OpenChannel(void)
{
	VERBOSE("Trying to open channel...");
	int result = -1;

	if (channel != nullptr)
	{
		// result = channel->Open();
		VERBOSE("Channel open!? " << channel->opened);
		if (channel->opened)
			result = 0;
		else
		{
			if (channel->scanOpen(0, 3))
			{
				result = 0;
			}
		}
	}
	return result;
}

int DynamixelInterface::ReadRegisters(uint8_t id, uint8_t startRegister, uint8_t *reply, uint8_t numRegisters)
{
	int result = -1;

	Channel *chan = GetChannel();

	if (chan != nullptr)
	{
		ChannelLockGuard clg(*chan);

		unsigned int bufferSize = this->calculateReadRegisterBufferLength(numRegisters); // packet size
		unsigned int treplySize = this->calculateBufferLength(1 + numRegisters); // error byte
		uint8_t buffer[bufferSize];
		uint8_t treply[treplySize];

		this->prepareBufferReadRegisters(buffer, bufferSize, id, startRegister, numRegisters);
		this->calculateBufferChecksum(buffer, bufferSize);

		VERBOSENEL("sending buffer (" << bufferSize << ") [");
		VERBOSEDATA(buffer, bufferSize);
		VERBOSEB("]");

		int ret;
		if ((ret = chan->writeData(&buffer[0], bufferSize)) > 0)
		{
			// VERBOSENEL("Reading answer (" << treplySize << ") ");
			if ((ret = this->readPackage(&treply[0], treplySize)) > 0)
			{
				// Interface::dumpData(buffer, bufferSize);
				// VERBOSEB("OK!");

				result = this->readRegistersReply(treply, treplySize, reply, id, numRegisters);
			}
			else
			{
				VERBOSEB("FAIL! (Code: " << ret << ")");
				// Read fail
			}
		}
		else
		{
			VERBOSE("FAIL!");
			// Write command fail.
		}

/*
		if ((ret = chan->CommandTxAndRx(buffer, bufferSize, &treply[0], treplySize)) >= 0)
		{
#ifndef NDEBUG
			boost::io::ios_flags_saver ifs(std::cout);
			VERBOSENEL("DynamixelInterface::ReadRegisters received reply " << "(" << ret << ") ");
#endif
			unsigned int retend = static_cast<unsigned int>(ret);
			int headerFound = -1;
			for (unsigned int i = 0; i < retend; i++)
			{
#ifndef NDEBUG
				VERBOSEBNEL(std::hex << std::setfill('_') << std::setw(3) << static_cast<unsigned int>( treply[i] ));
#endif
				// Finds the begining of the header
				if (
					(headerFound < 0) &&
					(*((uint16_t*)&treply[i]) == RobotisServo::FULL_HEADER)
				)
				{
					headerFound = i;
					ret -= headerFound;
				}
			}
#ifndef NDEBUG
			std::cout << std::endl;
			ifs.restore();
#endif
			// If the header is right shifted.
			if (headerFound < 0)
			{
				VERBOSE("DynamixelInterface::ReadRegisters - Header wasn't found.");
				return -2;
			}
			else if (headerFound > 0)
			{
				VERBOSE("DynamixelInterface::ReadRegisters - Header shifted found at position " << headerFound);
				for (unsigned int i = headerFound; i < retend; i++)
					treply[i-headerFound] = treply[i];

				VERBOSE("DynamixelInterface::ReadRegisters - Reading more " << headerFound << " | starting at on reply "
					<< (treplySize-headerFound) << " bytes.");
				int tmpRet;
				if ((tmpRet = chan->CommandTxAndRx(buffer, bufferSize, &treply[treplySize-headerFound], headerFound)) >= 0)
				{
					VERBOSENEL("DynamixelInterface::ReadRegisters - OK (" << ret << "+" << tmpRet);
					ret += tmpRet;
#ifndef NDEBUG
					VERBOSEB(" = " << ret << ").");
					boost::io::ios_flags_saver ifs2(std::cout);
					std::cout << "DynamixelInterface::ReadRegisters - Dumping new reply (" << ret << "): ";
					unsigned int retend = static_cast<unsigned int>(ret);
					for (unsigned int i = 0; i < retend; i++)
					{
						std::cout << std::hex << std::setfill('_') << std::setw(3);
						std::cout << static_cast<unsigned int>( treply[i] );
					}
					std::cout << std::endl;
					ifs2.restore();
#endif
				}
				else
				{
					VERBOSE("DynamixelInterface::ReadRegisters - Cannot read a splitted package.");
					return -3;
				}
			}

			uint8_t cs;
			if (
				(treply[0] == RobotisServo::HEADER) &&
				(treply[1] == RobotisServo::HEADER) &&
				(treply[2] == id) &&
				(treply[3] == treplySize - 4) &&
				(treply[4] == RobotisServo::NO_ERROR) &&
				((cs = CalculateCheckSum(&treply[2], treplySize - 2 - 1)) == treply[treplySize - 1])
				)
			{
				for (int i = 0; i < numRegisters; ++i)
				{
					reply[i] = treply[5 + i];
				}
				result = 0;
			}
			else
			{
				boost::io::ios_flags_saver ifs2(std::cout);
				VERBOSE("DynamixelInterface::ReadRegisters - Error reading the package (size " << ret << "): " << std::hex
					<< std::setfill('0') << "0x" << static_cast<unsigned int>(treply[4]));
				if ((treply[0] != RobotisServo::HEADER) || (treply[1] != RobotisServo::HEADER))
				{
					VERBOSEB("\t+ Headers received doesn't match with the documentation. Expected: 0xff 0xff Received: 0x"
						<< std::hex << std::setfill('0') << static_cast<unsigned int>(treply[0]) << " 0x" << std::hex
						<< std::setfill('0') << static_cast<unsigned int>(treply[1]) << ".");
				}
				ifs2.restore();
				if (cs != treply[treplySize - 1])
				{

					VERBOSEB("\t+ Error on checksum of the received package. Expected: 0x" << std::hex << static_cast<unsigned int>(cs) << " Received: 0x" << std::hex << static_cast<unsigned int>(treply[treplySize - 1]));
					ifs2.restore();
				}
				else
				{
					if ((ret > 2) && (treply[2] != id))
					{
						VERBOSEB("\t+ Package from a different servo. Expexted id: " << static_cast<unsigned int>(id) << " Received: " << static_cast<unsigned int>(treply[2]));
					}
					if ((ret > 3) && (treply[3] != treplySize - 4))
					{
						VERBOSEB("\t+ Package size mismatch. Expected: " << (treplySize - 4) << ". Received: " << static_cast<unsigned int>(treply[4]));
					}
					if ((ret > 4) && (treply[4] != RobotisServo::RobotisErrorBits::ERROR_NOERROR))
					{
						RobotisServoError err;
						RobotisServoMXSeries::byteToStructError(treply[4], &err);
						VERBOSEB("\t+ Errors:");
						if (err.instruction)
							VERBOSEB("\t\t- INSTRUCTION");
						if (err.overload)
							VERBOSEB("\t\t- OVERLOAD");
						if (err.checksum)
							VERBOSEB("\t\t- CHECKSUM");
						if (err.range)
							VERBOSEB("\t\t- RANGE");
						if (err.overheating)
							VERBOSEB("\t\t- OVERHEATING");
						if (err.anglelimit)
							VERBOSEB("\t\t- ANGLELIMIT");
						if (err.inputVoltage)
							VERBOSEB("\t\t- INPUTVOLTAGE");
					}
				}
			}
		}
*/
	}
	return result;
}

int
DynamixelInterface::WriteRegisters(
	uint8_t id, uint8_t startRegister, uint8_t *registers, uint8_t numRegisters, enum RobotisServo::RobotisStatusReturnLevel statusReturnLevel
)
{
	int ret = -1;

	Channel *chan = this->GetChannel();
#ifndef NDEBUG
	if (chan == nullptr)
	{
		VERBOSE("channel is nullptr");
	}
#endif
	if (chan != nullptr)
	{
		VERBOSE("channel is NOT nullptr");

		unsigned int
			bufferSize = this->calculateBufferLength(1 /* instruction */ + 1 /* startRegister */ + numRegisters);
		uint8_t buffer[bufferSize];

		unsigned int offset = this->prepareBufferWriteRegisters(buffer, bufferSize, id, startRegister);
		for (int i = 0; i < numRegisters; i++)
		{
			buffer[offset + i] = registers[i];
		}
		this->calculateBufferChecksum(buffer, bufferSize);
		VERBOSENEL("Sending buffer [");
		VERBOSEDATA(buffer, bufferSize);
		VERBOSEB("]");

		if (statusReturnLevel != RobotisServo::RobotisStatusReturnLevel::RESPONSE_NONE)
		{
			unsigned int treplySize = 6;
			uint8_t treply[treplySize];

			uint8_t *dreply;
			uint8_t dreplyLen;

			VERBOSE("statusReturnLevel " << statusReturnLevel);
			if (statusReturnLevel == RobotisServo::RobotisStatusReturnLevel::RESPONSE_ALL)
			{
				dreply = treply;
				dreplyLen = sizeof(treply);
			}
			else
			{
				dreply = nullptr;
				dreplyLen = 0;
			}

			if ((ret = chan->CommandTxAndRx(buffer, bufferSize, dreply, dreplyLen)) >= 0)
			{
				VERBOSENEL("Received reply " << "(" << ret << ") [");
				VERBOSEDATA(treply, ret);
				VERBOSEB("]");

				if (dreplyLen > 0)
				{
					protocols::dynamixel1::ErrorByte error;
					RobotisServoMXSeries::byteToStructError(dreply[RobotisServoMXSeries::ERROR_INDEX], &error);
					if (error.noError)
					{
						VERBOSE("noError");
					}
					else
					{
						VERBOSE("Error:");
						if (error.instruction)
							VERBOSEB("\t+ instruction");
						if (error.overload)
							VERBOSEB("\t+ overload");
						if (error.checksum)
							VERBOSEB("\t+ checksum");
						if (error.range)
							VERBOSEB("\t+ range");
						if (error.overheating)
							VERBOSEB("\t+ overheating");
						if (error.anglelimit)
							VERBOSEB("\t+ anglelimit");
						if (error.inputvoltage)
							VERBOSEB("\t+ inputVoltage");
					}
				}
			}
		}
	}
	return ret;
}

Actuator *
DynamixelInterface::factoryActuatorAuto(std::string name, boost::property_tree::ptree &pt)
{
	Actuator *act = nullptr;
	int id = pt.get<int>("id");
	int result = -1;

	if ((result = OpenChannel()) == 0)
	{
		// Send query string to channel to check for type and id
		uint8_t reply[2];
		if ((result = this->ReadRegisters(id, RobotisServo::RobotisProtocol::MODEL_NUMBER_L, reply, sizeof(reply))) == 0)
		{
			std::string typeName = "";
			int num = reply[0] | (reply[1] << 8);

			VERBOSE("sizeOf servoTypes = " << sizeof(servoTypes) / sizeof(servoTypes[0]));

			for (size_t i = 0; i < sizeof(servoTypes) / sizeof(servoTypes[0]); ++i)
			{
				if (servoTypes[i].num == num)
				{
					typeName = servoTypes[i].typeName;
				}
			}

			if (typeName == "")
			{
				ERROR("do not know how to create robotis servo of type " << num);
				std::exit(19);
			}
			act = RobotisServo::factory(name, typeName, this, pt);
		}
		else
		{
			ERROR("unable to read model number");
			std::exit(57);
		}
	}
	else
	{
		ERROR("ERROR unable to open channel " << channel->GetName());
		std::exit(58);
	}
	return act;
}

uint8_t
DynamixelInterface::CalculateCheckSum(uint8_t buffer[], unsigned int length)
{
	uint8_t result = 0;

	for (unsigned int i = 0; i < length; i++)
	{
		result = result + buffer[i];
	}
	return ~result;
}

int
DynamixelInterface::readPackage(uint8_t buffer[], unsigned int const bufferLength) const
{
	if (bufferLength < sizeof(protocols::dynamixel1::Header)+1) // Size of the header + checksum
		return -2;

	Channel *chan = this->GetChannel();

	if (chan == nullptr)
		return -1;

	unsigned int
		bytesRead,
		i, shift = 0,
		readMore = 0;

	protocols::dynamixel1::Header* header;
	unsigned int retryCount = 0;

	do
	{
		VERBOSENEL("Reading from buffer[" << shift << "] to buffer[" << sizeof(protocols::dynamixel1::Header) << "]");
		bytesRead = chan->readData(&buffer[shift], sizeof(protocols::dynamixel1::Header));
		if (bytesRead > 0)
		{
			VERBOSEBNEL(" (" << bytesRead << ") [");
			VERBOSEDATA(&buffer[shift], sizeof(protocols::dynamixel1::Header)-shift);
			VERBOSEB("] OK!");
			for (i = 0; i < bytesRead-1; i++) // bytesRead - 1, because two bytes from the header
			{
				header = (protocols::dynamixel1::Header*)&buffer[i];
				// VERBOSE("Header got: " << header->header << ", expected: " << RobotisServoMXSeries::FULL_HEADER);
				if (header->header == RobotisServoMXSeries::FULL_HEADER)
				{
					// VERBOSENEL("Data: [");
					// VERBOSEDATA(&buffer[i], bytesRead - i);
					// VERBOSEB("]");

					readMore = i;

					// Brings the HEADER to the begining of the package.
					for (unsigned int z = i; z < bytesRead; z++)
						buffer[z - i] = buffer[z];

					header = (protocols::dynamixel1::Header*)&buffer[0];

					// VERBOSEB("\t+ Header: " << static_cast<unsigned int>(header->header));
					// VERBOSEB("\t+     ID: " << static_cast<unsigned int>(header->id));
					// VERBOSEB("\t+    LEN: " << static_cast<unsigned int>(header->length));
					// VERBOSEB("\t+ ToRead: " << readMore);

					break;
				}
			}
			// In case of the last byte of the header be a start of the next package.
			if (buffer[bytesRead-1] == RobotisServoMXSeries::HEADER)
			{
				shift = 1;
				buffer[0] = RobotisServoMXSeries::HEADER;
			}
			else
				shift = 0;
			// VERBOSE("Last byte is: " << static_cast<unsigned int>(buffer[bytesRead-1]) << ". Shift is: " << shift);
		}
		else
		{
			VERBOSEB(" (" << bytesRead << ") FAIL! Cannot read!!");
			if ( retryCount++ < 3 )
			{
				VERBOSE("Restarting the PORT!");
				chan->Reset();
				VERBOSE("Done");
				bytesRead = 1;
			}
			else
			{
				VERBOSE("Failing!");
				return -1;
			}
		}
	} while ((bytesRead > 0) && (header->header != RobotisServoMXSeries::RobotisProtocol::FULL_HEADER));

	// If doesn't have the whole header
	if (readMore > 0)
	{
		// VERBOSENEL("Reading the rest of the header (more " << readMore << " bytes)");
		bytesRead = chan->readData(&buffer[sizeof(protocols::dynamixel1::Header) - readMore], readMore);
		if (bytesRead != readMore)
		{
			// VERBOSEB("FAIL: Got: " << bytesRead << ", expected: " << readMore);
			return -1;	// No package found.
		}
		// VERBOSEBNEL("[");
		// VERBOSEDATA(&buffer[0], sizeof(protocols::dynamixel1::Header));
		// VERBOSEB("] OK");
	}

	// VERBOSEB("\t+ Header: " << static_cast<unsigned int>(header->header));
	// VERBOSEB("\t+     ID: " << static_cast<unsigned int>(header->id));
	// VERBOSEB("\t+    LEN: " << static_cast<unsigned int>(header->length));

	if ((sizeof(protocols::dynamixel1::Header) + header->length) > bufferLength)
	{
		// VERBOSENEL("Buffer is too short for the package. Got: " << bufferLength << ", Expected: " << (sizeof(protocols::dynamixel1::Header) + header->length));
		return -2; // Package too big for the buffer.
	}

	// VERBOSENEL("Reading body (" << static_cast<unsigned int>(header->length) << " bytes)");
	bytesRead = chan->readData(&buffer[sizeof(protocols::dynamixel1::Header)], header->length);
	if (bytesRead == header->length)
	{
		// VERBOSEBNEL(" OK! [");
		// VERBOSEDATA(&buffer[0], sizeof(protocols::dynamixel1::Header) + bytesRead);
		// VERBOSEB("");
		return sizeof(protocols::dynamixel1::Header) + bytesRead;
	}
	else
	{
		// VERBOSEBNEL(" [");
		// VERBOSEDATA(&buffer[0], sizeof(protocols::dynamixel1::Header) + bytesRead);
		// VERBOSEB("] FAIL! Got: " << bytesRead << ", Expected: " << static_cast<unsigned int>(header->length));
		return -1;
	}
}

unsigned int
DynamixelInterface::prepareHeader(uint8_t *buffer, unsigned int bufferSize, uint8_t id)
{
	buffer[0] = RobotisServo::HEADER;
	buffer[1] = RobotisServo::HEADER;
	buffer[2] = id;
	buffer[3] = bufferSize - 4;
	return 4;
}

unsigned int
DynamixelInterface::calculateBufferLength(unsigned int packetSize)
{
	return 2 /* header */ + 1 /* ID */ + 1 /* length */ + packetSize + 1 /* CRC */;
}

unsigned int
DynamixelInterface::calculateReadRegisterBufferLength(unsigned int numRegisters)
{
	return this->calculateBufferLength(1 /* instruction */ + 1 /* start address */ + 1 /* num registers */);
}

unsigned int
DynamixelInterface::prepareBufferWriteRegisters(
	uint8_t *buffer, unsigned int bufferSize, uint8_t id, uint8_t startRegister
)
{
	unsigned int out = this->prepareHeader(buffer, bufferSize, id);
	buffer[out++] = RobotisServo::CMD_WRITE_DATA;
	buffer[out++] = startRegister;
	return out;
}

void
DynamixelInterface::calculateBufferChecksum(uint8_t *buffer, unsigned int bufferLength)
{
	buffer[bufferLength - 1] = this->CalculateCheckSum(&buffer[2], bufferLength - 2 - 1);
}

unsigned int
DynamixelInterface::prepareBufferReadRegisters(
		uint8_t *buffer, unsigned int bufferSize, uint8_t id, unsigned int startRegister, unsigned int numRegisters
)
{
	unsigned int out = this->prepareHeader(buffer, bufferSize, id);
	buffer[out++] = RobotisServo::CMD_READ_DATA;
	buffer[out++] = startRegister;
	buffer[out++] = numRegisters;
	return out;
}

unsigned int
DynamixelInterface::prepareBufferSyncCommand(
	uint8_t *buffer, unsigned int bufferSize, unsigned int startAddress, unsigned int dataLength
)
{
	unsigned int out = this->prepareHeader(buffer, bufferSize, RobotisServo::BROADCAST_ID);
	buffer[out++] = RobotisServo::CMD_SYNC_WRITE;
	buffer[out++] = startAddress;
	buffer[out++] = dataLength;
	memcpy(&buffer[out], syncCommands, syncOut * sizeof(protocols::dynamixel1::SyncWriteGainsPosition));
	out += (this->syncOut * sizeof(protocols::dynamixel1::SyncWriteGainsPosition));
	return out;
}

int
DynamixelInterface::readRegistersReply(uint8_t *treply, unsigned int treplySize, uint8_t *reply, uint8_t id, uint8_t numRegisters)
{
	uint8_t cs;
	int
		result = -1;
	if (
			(treply[0] == RobotisServo::HEADER) &&
			(treply[1] == RobotisServo::HEADER) &&
			(treply[2] == id) &&
			(treply[3] == treplySize - 4) &&
			(treply[4] == RobotisServo::NO_ERROR) &&
			((cs = this->CalculateCheckSum(&treply[2], treplySize - 2 - 1)) == treply[treplySize - 1])
			)
	{
		for (int i = 0; i < numRegisters; ++i)
		{
			reply[i] = treply[5 + i];
		}
		result = 0;
	}
	else
	{
		VERBOSE("Error reading the package (size " << treplySize << "): ");
		VERBOSEDATA(&treply[4], 1);
		VERBOSEB("");
		if ((treply[0] != RobotisServo::HEADER) || (treply[1] != RobotisServo::HEADER))
		{
			VERBOSEBNEL("\t+ Headers received doesn't match with the documentation. Expected: 0xff 0xff Received: [");
			VERBOSEDATA(&treply[0], 2);
		}
		if (cs != treply[treplySize - 1])
		{
			VERBOSEBNEL("\t+ Error on checksum of the received package. Expected: [");
			VERBOSEDATA(&cs, 1);
			VERBOSEBNEL("] Received: [");
			VERBOSEDATA(&treply[treplySize - 1], 1);
			VERBOSEB("]");
		}
		else
		{
			if ((treplySize > 2) && (treply[2] != id))
			{
				VERBOSEB("\t+ Package from a different servo. Expexted id: " << static_cast<unsigned int>(id) << " Received: " << static_cast<unsigned int>(treply[2]));
			}
			if ((treplySize > 3) && (treply[3] != treplySize - 4))
			{
				VERBOSEB("\t+ Package size mismatch. Expected: " << (treplySize - 4) << ". Received: " << static_cast<unsigned int>(treply[4]));
			}
			if ((treplySize > 4) && (treply[4] != RobotisServo::RobotisErrorBits::ERROR_NOERROR))
			{
				protocols::dynamixel1::ErrorByte err;
				RobotisServoMXSeries::byteToStructError(treply[4], &err);
				VERBOSEB("\t+ Errors:");
				if (err.instruction)
					VERBOSEB("\t\t- INSTRUCTION");
				if (err.overload)
					VERBOSEB("\t\t- OVERLOAD");
				if (err.checksum)
					VERBOSEB("\t\t- CHECKSUM");
				if (err.range)
					VERBOSEB("\t\t- RANGE");
				if (err.overheating)
					VERBOSEB("\t\t- OVERHEATING");
				if (err.anglelimit)
					VERBOSEB("\t\t- ANGLELIMIT");
				if (err.inputvoltage)
					VERBOSEB("\t\t- INPUTVOLTAGE");
			}
		}
	}
	return result;
}
