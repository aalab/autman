
#include "config.h"
#include "utility.hpp"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
/* #include <boost/asio/signal_set.hpp> */
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <ctime>
#include <iostream>
#include <syslog.h>
#include <unistd.h>

#include <cmath>
#include <iostream>
#include <sstream>


#define DEFAULT_PORT "1313"
#define DEFAULT_SERVER "localhost"

using boost::asio::ip::udp;

int 
main(int argc, char *argv[] )
{
  std::cout << "Robot Server" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
  std::cout << "Debugging build" << std::endl;
#endif
  
  std::string port;
  std::string server;
  if ( argc == 3 )
    {
      server = argv[1];
      port = atoi(argv[2]);
    }
  else if ( argc == 2 ) 
    {
      port = argv[1];
    }
  else
    {
      server = DEFAULT_SERVER;
      port = DEFAULT_PORT;
    }

  boost::asio::io_service io_service;
  udp::socket socket( io_service, udp::endpoint( udp::v4(), 0 ) );

  udp::resolver resolver( io_service );
  udp::resolver::query query( udp::v4(), server, port );
  udp::resolver::iterator iterator = resolver.resolve( query );

  float speed = 0.0;


  for( float angle = Utility::DegreeToRadians(20.0); angle > Utility::DegreeToRadians(0.0); angle -= Utility::DegreeToRadians(2.0) )
    {
      std::ostringstream os;
      os << "NeckLateralJoint: angle=0.0 speed=1&";
      os << "NeckTransversalJoint: angle= " << angle << " speed= " << speed;
      std::string com = os.str();
#ifndef NDEBUG
      std::cout << "Sending command command " << com << std::endl;
#endif      

      socket.send_to( boost::asio::buffer( com ), *iterator );
      
      char replyBuffer[4096];
      size_t replyBufferMaxLength = sizeof( replyBuffer );

      udp::endpoint sender_endpoint;
      size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

      std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
      std::cout << "Response " << reply << std::endl;
#endif					   		      
      os.clear();
    }

  std::string com = "ReadState";
#ifndef NDEBUG
  std::cout << "Sending command command " << com << std::endl;
#endif      
  
  socket.send_to( boost::asio::buffer( com ), *iterator );
  
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );
  
  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  std::cout << "Response " << reply << std::endl;
#endif					   		      

  return 0;
}
