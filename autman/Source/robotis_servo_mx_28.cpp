/*
 * MX-28 Actuator Definition 
 *
 */

#include "robotis_servo_mx_28.hpp"

RobotisServoMX28::RobotisServoMX28( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
: RobotisServoMXSeries( name, interface, pt ),
  defaultSpeed(0)
{
  type = ROBOTIS_SERVO_MX_28;  

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo MX 28 Actuator " << std::endl;
#endif

  //maxSpeed = 11.938052; // this needs to be change later.it's not correct!!!!
}


