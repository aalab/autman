/* RX-64 Actuator Definition 
 *
 */

#include "robotis_servo_rx_64.hpp"

RobotisServoRX64::RobotisServoRX64( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
: RobotisServoRXSeries( name, interface, pt ),
  defaultSpeed(0)
{
  type = ROBOTIS_SERVO_RX_64;  

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo RX64 Actuator " << std::endl;
#endif

  maxSpeed = 11.938052; // 114 RPM converted into 11.938052 radians per second
}


