/*
 * MX-106 Actuator Definition 
 *
 */

#include "robotis_servo_mx_106.hpp"

RobotisServoMX106::RobotisServoMX106( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
: RobotisServoMXSeries( name, interface, pt ),
  defaultSpeed(0)
{
  type = ROBOTIS_SERVO_MX_106;  

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo MX106 Actuator " << std::endl;
#endif

  //maxSpeed = 11.938052; // this needs to be change later.it's not correct!!!!
}


