#include "udp_robot_server.hpp"

#include "robot.hpp"
#include "compilerdefinitions.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
/* #include <boost/asio/signal_set.hpp> */
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <ctime>
#include <iostream>
#include <syslog.h>
#include <unistd.h>
#include <exceptions.hpp>

using boost::asio::ip::udp;

UDPRobotServer::UDPRobotServer(boost::asio::io_service &io_service, unsigned int port, Robot *robot)
	: socket(io_service, udp::endpoint(udp::v4(), port)), robot(robot)
{ }

void
UDPRobotServer::StartServer()
{
	char data[4096];
	size_t maxLength = sizeof(data);

	for (; ;)
	{
		size_t len = socket.receive_from(boost::asio::buffer(data, maxLength), remoteEP);
		std::string command(data, len);
		VERBOSE("Received command (" << len << "): " << command);

		Json::Value json;
		std::string resp;
		try
		{
			if (json::unserialize(command, json))
			{
				try
				{
					Json::Value result;
					int ret = robot->ProcessCommand(json, result);
					if (ret < 0)
					{
						ERROR("robot->ProcessCommand(" << command << ") failed with error code " << ret);
						resp = "{\"name\":" + robot->GetName() + ",\"success\":false}";
					}
					else
					{
						result["success"] = true;
						result["name"] = robot->GetName();
						// resp = robot->GetName() + "&Ok&" + robot->GetState() + "\n";
						json::serialize(result, resp);
					}
				}
				catch (exceptions::CommandException *e)
				{
					resp.append("{\"name\":\"" + robot->GetName() + "\",\"success\":false, \"errcode\":\"");
					resp.append(e->code());
					resp.append("\", \"errmsg\":\"");
					resp.append(e->msg());
					resp.append("\"}");

				}
				catch (std::exception *e)
				{
					resp.append("{\"name\":\"" + this->robot->GetName() +
								"\", \"success\":false, \"errcode\":\"E0\", \"errmsg\":\"");
					resp.append(e->what());
					resp.append("\"}");
				}
			}
			else
			{
				resp = "{\"name\":\"" + robot->GetName() +
					   "\", \"success\":false, \"errcode\":\"E1\", \"errmsg\":\"Error parsing the input.\"}";
			}
			this->SendResponse(resp);
			std::cout << std::endl << std::endl << std::endl << std::endl << std::endl;
		}
		catch (std::exception &e)
		{
			Json::Value resp;
			resp["success"] = false;
			resp["message"] = e.what();
			this->SendResponse(resp);
		}
	}
}

int
UDPRobotServer::SendResponse(std::string &msg)
{
	const boost::asio::const_buffers_1 &bf = boost::asio::buffer(msg, msg.size());
	int
		sent = 0,
		remaining = msg.size(),
		ret;

	VERBOSE("[OUTPUT] >> " << msg);

	while (remaining > 0)
	{
		ret = this->socket.send_to(bf, this->remoteEP);
		remaining -= ret;
		sent += ret;
	}
	return sent;
}

int UDPRobotServer::SendResponse(Json::Value &json)
{
	std::string response;
	json::serialize(json, response);
	return this->SendResponse(response);
}
