
/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date Aug 8, 2015
 */

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <unix_serial_port.hpp>
#include <thread>
#include <boost/algorithm/string/case_conv.hpp>

#include "config.h"

#include "autgait.h"
#include "autindexes.h"
#include "compilerdefinitions.h"

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;
	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("config_file,c", po::value<std::string>(&configFile)->default_value("robocup_test.info"), "robot config file");

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}
		if (vm.count("version"))
		{
			std::cout << "Robot Server Version: ???. " << "???" << std::endl;
			return 0;
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
#ifndef NDEBUG
	std::cout << "Loading config file " << configFile << " config path " << configPath << std::endl;
#endif

	using boost::property_tree::ptree;
	ptree pt;
	read_info(configPath, pt);

	UnixSerialPort serialPort("/dev/ttyACM0", pt);

	if (serialPort.scanOpen("/dev/ttyACM", 0, 3) >= 0)
	{
		VERBOSE("Port open!");
		AutIndexes::initIndexes();
		AUTGait autGait(&serialPort);

		std::string cmd;
		while (cmd != "quit")
		{
			std::cout << "command$ ";
			std::cin >> cmd;
			VERBOSE("Command: " << cmd);
			if (cmd == "help")
			{
				std::cout << "List of commands:" << std::endl;
				std::cout << " + help";
				std::cout << " + indexes";
				std::cout << " + search";
				std::cout << " + set";
				std::cout << " + read";
				std::cout << " + head";
				std::cout << " + headr";
				std::cout << " + walk";
				std::cout << " + stop";
				std::cout << " + quit";
			}
			else if (cmd == "indexes")
			{
				for (auto &index : AutIndexes::indexes)
				{
					std::cout << index.index << ". " << index.name << std::endl;
				}
			}
			else if (cmd == "search")
			{
				std::cout << "Term for search: ";
				std::cin >> cmd;
				boost::algorithm::to_lower(cmd);
				unsigned int count;
				std::string tmpName;
				for (auto &index : AutIndexes::indexes)
				{
					tmpName = index.name;
					boost::algorithm::to_lower(tmpName);
					if (tmpName.find(cmd)!=std::string::npos)
					{
						std::cout << index.index << ". " << index.name << std::endl;
						count++;
					}
				}
				if (count == 0)
				{
					std::cout << "No entries found." << std::endl;
				}
				else
				{
					std::cout << "--------------------------------------------" << std::endl
						<< count << " entries found." << std::endl;
				}
			}
			else if (cmd == "headr")
			{
				AUTGaitHead move;
				double pan, tilt;
				std::cin >> pan;
				if (std::cin.fail())
				{
					ERROR("Cannot read pan.");
				}
				else
				{
					std::cin >> tilt;
					if (std::cin.fail())
					{
						ERROR("Cannot read tilt.");
					}
					else
					{
						autGait.initData(move, pan, tilt);
						if (!autGait.sendData(&move))
						{
							ERROR("Cannot send stop command to the subcontroller.");
						}
					}
				}
			}
			else if (cmd == "head")
			{
				AUTGaitHead move;
				double pan, tilt;
				std::cin >> pan;
				if (std::cin.fail())
				{
					ERROR("Cannot read pan.");
				}
				else
				{
					std::cin >> tilt;
					if (std::cin.fail())
					{
						ERROR("Cannot read tilt.");
					}
					else
					{
						autGait.initData(move, D2R(pan), D2R(tilt));
						if (!autGait.sendData(&move))
						{
							ERROR("Cannot send stop command to the subcontroller.");
						}
					}
				}
			}
			else if (cmd == "stop")
			{
				AUTGaitMove move;
				autGait.initData(move, 0, 0, 0);
				if (!autGait.sendData(&move))
				{
					ERROR("Cannot send stop command to the subcontroller.");
				}
			}
			else if (cmd == "walk")
			{
				AUTGaitMove move;
				double vx, vy, vt;
				std::cout << "VX: ";
				std::cin >> vx;
				if (std::cin.fail())
				{
					ERROR("Cannot read VX.");
				}
				else
				{
					std::cout << "VT: ";
					std::cin >> vy;
					if (std::cin.fail())
					{
						ERROR("Cannot read VY.");
					}
					else
					{
						std::cout << "VT: ";
						std::cin >> vt;
						if (std::cin.fail())
						{
							ERROR("Cannot read VT.");
						}
						else
						{
							autGait.initData(move, vx, vy, vt);
							if (!autGait.sendData(&move))
							{
								ERROR("Cannot send command to the subcontroller.");
							}
						}
					}
				}
			}
			else if (cmd == "set")
			{
				int index;
				std::cout << "index$ ";
				std::cin >> index;
				if (std::cin.fail())
				{
					std::cerr << "Invalid integer value";
				}
				else
				{
					AUTGaitWEPRead read;
					AUTGaitWEPReadResponse readResponse;
					autGait.initData(read, index);
					if (autGait.readData(read, readResponse))
					{
						std::cout << "Current value: " << readResponse.value << std::endl;
					}

					double value;
					std::cout << "value$ ";
					std::cin >> value;
					if (std::cin.fail())
					{
						std::cerr << "Invalid double value";
					}
					else
					{
						AUTGaitWEP wep;
						autGait.initData(wep, index, value);
						if (autGait.sendData(&wep))
						{
							if (autGait.readData(read, readResponse))
							{
								std::cout << "Read value: " << readResponse.value << std::endl;
							}
							else
							{
								ERROR("Cannot read value again, for checking. Try reading it manually.");
							}
						}
						else
						{
							ERROR("Cannot send the command to controller.");
						}
					}
				}
			}
			else if (cmd == "read")
			{
				int index;
				std::cout << "index$ ";
				std::cin >> index;
				if (std::cin.fail())
				{
					std::cerr << "Invalid integer value";
				}
				else
				{
					AUTGaitWEPRead read;
					AUTGaitWEPReadResponse readResponse;
					autGait.initData(read, index);
					if (autGait.readData(read, readResponse))
					{
						std::cout << "Value: " << readResponse.value << std::endl;
					}
					else
					{
						ERROR("Cannot read index: " << index);
					}
				}
			}
		}
	}
	else
	{
		std::cout << "Error opening the port." << std::endl;
	}
	return 0;
}