
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <unix_serial_port.hpp>
#include <thread>

#include "config.h"

#include "autgait.h"
#include "compilerdefinitions.h"

/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 18, 2015
 */


int main(int argc, char* argv[])
{
	namespace po = boost::program_options;
	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("config_file,c", po::value<std::string>(&configFile)->default_value("robocup_test.info"), "robot config file");

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "Robot Server Version: ???. " << "???" << std::endl;
			return 0;
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
#ifndef NDEBUG
	std::cout << "Loading config file " << configFile << " config path " << configPath << std::endl;
#endif

	using boost::property_tree::ptree;
	ptree pt;
	read_info(configPath, pt);

	UnixSerialPort serialPort("/dev/ttyAMA0", pt);

	if (serialPort.Open() >= 0)
	{
		VERBOSE("Opened!");

		VERBOSE("Moves!?");
		serialPort.writeChar(0xff);
		serialPort.writeChar(0xff);
		serialPort.writeChar(0x2d);
		serialPort.writeChar(0x0b);
		serialPort.writeChar(0x03);
		serialPort.writeChar(0x1a);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x20);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0xff);
		serialPort.writeChar(0x0b);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x80);
		VERBOSE("------------");

/*
		serialPort.writeChar(0xff);
		serialPort.writeChar(0xff);
		serialPort.writeChar(0x0d);
		serialPort.writeChar(0x0b);
		serialPort.writeChar(0x03);
		serialPort.writeChar(0x1a);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x20);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x73);
		serialPort.writeChar(0x09);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x2e);

		serialPort.writeChar(0xff);
		serialPort.writeChar(0xff);
		serialPort.writeChar(0x14);
		serialPort.writeChar(0x04);
		serialPort.writeChar(0x02);
		serialPort.writeChar(0x00);
		serialPort.writeChar(0x02);
		serialPort.writeChar(0xe3);
		while (true)
		{
			serialPort.writeChar(0xff);
			serialPort.writeChar(0xff);
			serialPort.writeChar(0x2d);
			serialPort.writeChar(0x04);
			serialPort.writeChar(0x02);
			serialPort.writeChar(0x00);
			serialPort.writeChar(0x02);
			serialPort.writeChar(0xca);
			usleep(1000000);
		}
*/

		uint8_t c;
		while (true)
		{
			serialPort.writeChar(0xff);
			serialPort.writeChar(0xff);
			serialPort.writeChar(0x2d);
			serialPort.writeChar(0x04);
			serialPort.writeChar(0x02);
			serialPort.writeChar(0x00);
			serialPort.writeChar(0x02);
			serialPort.writeChar(0xca);
			usleep(1000000);

			if (serialPort.readData(&c, 1, 100))
				std::cout << " " << static_cast<unsigned int>(c);
		}

		/*
		AUTGait autGait(&serialPort);

		AUTGaitData walkingData;
		autGait.initData(walkingData, -0.1, 0.0, 0, 0.7, 0.0);

		if (autGait.sendData(&walkingData))
		{
			std::cout << "OK!" << std::endl;
		}
		else
		{
			std::cout << "Failed!" << std::endl;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
		 */


		/*
		AUTGaitHead headCommand;
		autGait.initData(headCommand, 0.7, 0.7);
		if (autGait.sendData(&headCommand))
		{
			std::cout << "OK!" << std::endl;
		}
		else
		{
			std::cout << "Failed!" << std::endl;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));

		AUTGaitMove moveCommand;
		autGait.initData(moveCommand, -0.1, 0, 0);
		if (autGait.sendData(&moveCommand))
		{
			std::cout << "OK!" << std::endl;
		}
		else
		{
			std::cout << "Failed!" << std::endl;
		}
		*/
		std::this_thread::sleep_for(std::chrono::milliseconds(3000));

		/*
		std::chrono::system_clock::time_point last = std::chrono::system_clock::now();
		while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - last).count() < 10000)
		{
			if (autGait.sendData(&walkingData))
			{
				std::cout << "OK!" << std::endl;
			}
			else
			{
				std::cout << "Failed!" << std::endl;
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(30));
			for (unsigned int x = 0; x < sizeof(walkingData); x++)
			{
				printf("%c", ((char*)&walkingData)[x]);
			}
			std::cout << std::endl << std::endl;
		}
		 */

		/*
		autGait.initData(walkingData, 0.0, 0.0, 0, 0, 0);
		if (autGait.sendData(&walkingData))
		{
			std::cout << "OK!" << std::endl;
		}
		else
		{
			std::cout << "Failed!" << std::endl;
		}
		 */

		/*
		for (;;)
		{
			if (autGait.sendData(&walkingData))
			{
				std::cout << "OK!" << std::endl;
			}
			else
			{
				std::cout << "Failed!" << std::endl;
			}
			usleep(100000);
		}
		 */
	}
	else
	{
		std::cerr << "Error opening the port." << std::endl;
	}
	return 0;
}