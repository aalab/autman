/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 04, 2015
 */

#include <robotis_servo.hpp>
#include <robotis_servo_mx_series.hpp>
#include "dynamixel_protocol.hpp"

void ::protocols::dynamixel1::init(::protocols::dynamixel1::Header *header)
{
	header->header = RobotisServo::FULL_HEADER;
}

void ::protocols::dynamixel1::init(protocols::dynamixel1::ReadRegisters *command, uint8_t startAddress, uint8_t readDataLength)
{
	init(&command->header);
	command->instruction = RobotisServoMXSeries::CMD_READ_DATA;
	command->header.length = 3 + 1;
	command->registerAddress = startAddress;
	command->length = readDataLength;
}

uint8_t ::protocols::dynamixel1::checksum(uint8_t *command, unsigned int length)
{
	uint8_t result = 0;
	for (unsigned int i = 0; i < length; i++)
		result = result + command[i];
	return ~result;
}

void ::protocols::dynamixel1::checksum(protocols::dynamixel1::ReadRegisters *command)
{
	command->checksum = checksum(&((uint8_t*)command)[2], sizeof(protocols::dynamixel1::ReadRegisters) - 2 - 1);
}

void ::protocols::dynamixel2::init(protocols::dynamixel2::Header *header, uint8_t id, uint16_t length)
{
	header->header1 = 0xFF;
	header->header2 = 0xFF;
	header->header3 = 0xFD;
	header->header4 = 0x00;
	header->id = id;
	header->length_lo = (length & 0xFF);
	header->length_hi = ((length >> 8) & 0xFF);
}

