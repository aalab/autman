
#include "config.h"

#include "position.hpp"
#include "robot.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>
#include <string>

#define DEFAULT_VISION_PORT "2134"
#define DEFAULT_VISION_SERVER "127.0.0.1" 
#define DEFAULT_ROBOT_PORT  "1313"
#define DEFAULT_ROBOT_SERVER "127.0.0.1" 
#define DEFAULT_GYRO_PORT  "1414"
#define DEFAULT_GYRO_SERVER "127.0.0.1" 


static bool ball_found = false;
static int ball_x      = -1;
static int ball_y      = -1;
static int topX        = -1;
static int topY        = -1;
static int bottomX     = -1;
static int bottomY     = -1;

using boost::asio::ip::udp;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );

  std::cout << "\t\t\t\t\t" << command << std::endl << std::endl << std::endl;

  socket.send_to( boost::asio::buffer(command), * iterator );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  std::cout << "Soccer::SendCommand Responses " << reply << std::endl;
#endif					   		      

  return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
  return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
  std::cout<<"!!from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif 
  return SendCommand( socket, iterator, trajectory.ToCommandString() );
}

static std::vector<Trajectory> motions;

void
LoadTrajectories( boost::property_tree::ptree & pt )
{
  using boost::property_tree::ptree;
  
  BOOST_FOREACH( ptree::value_type & v , pt.get_child("trajectories"))
    {
      //std::string tmp = pt.get<std::string>("motion");
      std::string motionName = (std::string)v.first.data();
#ifndef NDEBUG
      std::cout << "Found Trajectory for: " << motionName << std::endl;
#endif
      BOOST_FOREACH( ptree::value_type & joint , v.second)
	{ 
	  std::string jointName = (std::string)joint.first.data();
#ifndef NDEBUG
	  std::cout << "Found  joint: " << jointName << std::endl;
#endif
	  Trajectory tmpT( motionName, jointName, joint.second );
	  motions.push_back(tmpT);
	} 
    }
}

void 
LoadTrajectories(std::string const filename )
{
  using boost::property_tree::ptree;
  ptree pt;

  read_info( filename, pt );
  LoadTrajectories( pt );
}


unsigned long int tdiffUSec(struct timespec const start, struct timespec const end);

void addTimeNSec( struct timespec * time, long int disp );


float polynomialHold(float const amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage);

int
ParseFromStreamVision(std::string const & message)
{
  int result = -1;
  std::string trim;
  std::string keyword;
  unsigned int size;
  unsigned int tx;
  unsigned int ty;
  unsigned int bx;
  unsigned int by;
  std::string target;
  std::string state;
  char sep;
  unsigned int tmpx;
  unsigned int tmpy;

  ball_found = false;

  std::ostringstream os;
  os << "Vision " << message;
  std::istringstream iss( os.str() );

  iss >> keyword;
  iss >> state;
  iss >> sep;
  if ( ( iss ) && ( keyword == "Vision" ) && ( state == "Ok" ) && ( sep == '&' ) )
    {
      //result = 0;
      while( std::getline( iss, trim, '&' ) )
	{
	  trim = Utility::ReplaceChars(trim ,":=[](),;-", ' ' );
	  Utility::Trim( trim );
	  std::istringstream ist(trim);
	  //unsigned max = 0;
	  
#ifndef NDEBUG
	  std::cout<< "-----trim----"<< trim << std::endl;
#endif  
	  
	  ist >> target;
	  
	  ist >> size;
	  ist >> tmpx;   //static variable
	  ist >> tmpy;   //static variable
	  ist >> tx;
	  ist >> ty;
	  ist >> bx;
	  ist >> by;
	  if( ist )
	    {
#ifndef NDEBUG
	      std::cout<<"ParseFromStreamVision found object " << target << "," << size << "," <<tmpx << "," << tmpy << ","  << tx << "," << ty << ","  << bx << "," << by << std::endl;
#endif
	      if ( size > 15 )
		{
		  result = 0; 
		}
	      if ( ( target == "ball" ) || (target == "goalpost") || (target == "track_line") )
		{
		  // max  = size;
		  ball_x  = tmpx;
		  ball_y  = tmpy;
		  topX    = tx;
		  topY    = ty;
		  bottomX = bx;
		  bottomY = by;
		  //ball_found = true;
		}
	    }
	  else
	    {
	      result = -1;
	      goto err_exit;
	    }
	}
    }
  
 err_exit:  
#ifndef NDEBUG
  std::cout<< "ParseFromStreamVision returns " << result << std::endl;
#endif
  return result;

}
	    
float 
ImageXCoordinateToRadians( float ball_x ) 
{
  float const cameraRange = Utility::DegreeToRadians( 45.0 );
  float const cameraWidth = 160.0;

  float dx = ( ball_x - cameraWidth/2.0f ) / (-cameraWidth/2.0f);
  float dRad = dx * cameraRange;

  return dRad;
}

int 
main(int argc, char *argv[] )
{
  namespace po = boost::program_options;
  std::string vision_port;
  std::string vision_server;
  std::string robot_port;
  std::string robot_server;
  std::string gyro_port;
  std::string gyro_server;
  std::vector<std::string> trajectoryFiles;

  po::options_description commandLineOnlyOptions("Command Line Options");
  commandLineOnlyOptions.add_options()
    ( "version,v", "print version string" )
    ( "help,h"   , "print help message" )
    ( "robot_server" , po::value<std::string>( & robot_server )->default_value(DEFAULT_ROBOT_SERVER), "robot server address" )
    ( "robot_port" , po::value<std::string> ( & robot_port )->default_value(DEFAULT_ROBOT_PORT), "robot port number" )
    ("vision_server" , po::value<std::string> ( & vision_server )->default_value(DEFAULT_VISION_SERVER),"vision server" )
    ("vision_port" , po::value<std::string> ( & vision_port )->default_value(DEFAULT_VISION_PORT),"vision port number" )
    ( "gyro_server" , po::value<std::string>( & gyro_server )->default_value(DEFAULT_GYRO_SERVER), "gyro server address" )
    ( "gyro_port" , po::value<std::string> ( & gyro_port )->default_value(DEFAULT_GYRO_PORT), "gyro port number" )
    ( "trajectories,t", po::value<std::vector<std::string> >( & trajectoryFiles ), "file with trajectory information");
  
  std::cout << "Soccer" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
  std::cout << "Soccer Debugging build" << std::endl;
#endif
  
  po::options_description commandLineOptions;
  commandLineOptions.add(commandLineOnlyOptions);

  po::positional_options_description p;
  p.add("trajectories",-1);

  try 
    {
      po::variables_map vm;
      po::store( po::command_line_parser( argc, argv).options(commandLineOptions).positional(p).run(), vm );
      po::notify( vm );

      if ( vm.count("help") )
	{
	  std::cout << commandLineOptions << "\n";
	  return 1;
	}

      if (vm.count("version") )
	{
	  std::cout << "Scoccer Rostam Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
	  return 1;
	}
    }
  catch( std::exception & e )
    {
      std::cerr << "boost::po exception " << e.what() << std::endl;
      return 1;
    }

  std::cout << "robot server " << robot_server << ", robot port " << robot_port << "vision server " << vision_server << ", vision port " << vision_port << std::endl;
  std::cout << "Trajectory Files" << std::endl;

  for ( std::string s : trajectoryFiles )
    {
      if ( ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".info" ) && 
	   ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".json" ) &&
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".xml" ) && 
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".ini" ) )
	{
	  s = s + ".info";
	}
      
      LoadTrajectories( std::string(CONFIG_DIR) + "/" + s );	 
    }

  std::cout << std::endl;

  boost::asio::io_service io_service;
  udp::socket robot_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  udp::socket vision_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  udp::socket gyro_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  
  udp::resolver robot_resolver( io_service );
  udp::resolver::query robot_query( udp::v4(), robot_server, robot_port );
  udp::resolver::iterator robot_iterator = robot_resolver.resolve( robot_query );

  udp::resolver vision_resolver( io_service );
  udp::resolver::query vision_query( udp::v4(), vision_server, vision_port );
  udp::resolver::iterator vision_iterator = vision_resolver.resolve( vision_query );
  
  /*
  udp::resolver gyro_resolver( io_service );
  udp::resolver::query gyro_query( udp::v4(), gyro_server, gyro_port );
  udp::resolver::iterator gyro_iterator = gyro_resolver.resolve( gyro_query );
  */
  Position saved;

  std::string reply;
  std::string replyVision;
  //std::string replyGyro;
  std::string com("Arash ReadState");
  reply = SendCommand( robot_socket, robot_iterator, com );

#ifndef NDEBUG
  std::cout << "Reply for Read State " << reply << std::endl;
#endif  

  std::istringstream ireply( reply );

  if ( saved.parseFromStream( ireply ) < 0 )
    {
      std::cerr << "ERROR: Unable to parse position from response " << reply << std::endl;
      //      std::exit(34);
    }
  
  for( Trajectory & traj : motions ) 
    {
      std::string reply = SendTrajectory( robot_socket, robot_iterator, traj );
      
#ifndef NDEBUG
      std::cout << "Response from sending trajectory " << reply << std::endl;
#endif
    }
  
#ifndef NDEBUG
  std::cout << "Saved position" << std::endl;
  saved.Print();
#endif
  /*
#ifndef NDEBUG
    std::cout<< "Sending the Trajectory Stand.."<< std::endl;
#endif
    std::string commandStand = "Trajectory = Stand CycleTimeUSec = 7000000 EntryTimeUSec = 5000000 Cycles = 1\n";
    std::string commandStandReply = SendCommand(robot_socket, robot_iterator, commandStand);
#ifndef NDEBUG
    std::cout<< "Reply for trajectory Stand:" << commandStandReply << std::endl;
#*endif
  */
  
  int check       = -1;
  float angleTilt = 0.0;
  float anglePan  = 0.0;
  static float angleTiltStart = 0.0;
  static float anglePanStart  = 0.0;
  static bool cont;
  static int varCycleTime=0;
  static int varCycles=0;
  std::ostringstream osNeckLateral;
  osNeckLateral <<  "Arash Actuator "<< "NeckLateral" << " Position " << "Angle" << " = " << 0.28 << " " << "Speed" << " = " << 100.0 ;
  const std::string replyNeckLateral = osNeckLateral.str();
  std::string replyTT = SendCommand( robot_socket, robot_iterator, replyNeckLateral );
  for( ;; )
    {
      /*
      //check gyro state
      replyGyro = SendCommand( gyro_socket, gyro_iterator, "ReadState" );
      std::cout << replyGyro <<std::endl;
      if(replyGyro.find("UPRIGHT") != std::string::npos) {
        //all is well, continue processing
      } else if(replyGyro.find("FALLEN_FORWARD") != std::string::npos || replyGyro.find("FALLEN_BACKWARD") != std::string::npos) {
        bool receivedFromGyro = false;
        std::string recoveryTrajectory;
        if(replyGyro.find("FALLEN_FORWARD") != std::string::npos) {
          recoveryTrajectory = "FaceDownRecovery";
        } else {
          recoveryTrajectory = "FaceUpRecovery";
        }
        
        //disables gyro
        while(!receivedFromGyro) {
          replyGyro = SendCommand( gyro_socket, gyro_iterator, "DisablePowerOff" );
          if(replyGyro.find("OK") != std::string::npos) {
            receivedFromGyro = true;
          }
        }
        receivedFromGyro = false;
        
        sleep(3);
        std::string recoveryCommand = "Trajectory=" + recoveryTrajectory + " CycleTimeUSec=15000000 EntryTimeUSec=8500000 Cycles = 1\n";
        SendCommand(robot_socket, robot_iterator, recoveryCommand);
          
        //enables gyro
        while(!receivedFromGyro) {
          replyGyro = SendCommand( gyro_socket, gyro_iterator, "EnablePowerOff" );
          if(replyGyro.find("OK") != std::string::npos) {
            receivedFromGyro = true;
          }
        }
        
      } else {
        std::cerr << "ERROR: Unknown responce received from gyro server" << std::endl;
      }
      */
      //continue state machine
      float currentTargetAngle = 0.0;
      //std::string com = "Move OmniWalk CycleTimeUSec = 700000  Dx =0.01 Dy=0.0 Dtheta= 0.0 Cycles= 2 \n";
      // reply = SendCommand(robot_socket, robot_iterator, com);
      //  usleep(15000000);

      std::ostringstream os;
            
      	      
      std::string replyVision = SendCommand( vision_socket, vision_iterator, "Results" );
#ifndef NDEBUG
      std::cout << "Vision reply " << replyVision << std::endl;
#endif            
      check = ParseFromStreamVision( replyVision );
     
      if( check >= 0)
	{
	  float speed = 100.0;
	  // if ( ball_found ) {
	  currentTargetAngle = anglePan + ImageXCoordinateToRadians( ball_x );
#ifndef NDEBUG
	  std::cout << "currentTargetAngle x "<< currentTargetAngle << " ball_x "<< ball_x << " anglePan " << Utility::RadiansToDegree( anglePan ) << " currentTargetAngle(Radians) " << Utility::DegreeToRadians( currentTargetAngle ) << std::endl;
#endif
	  if ( (ball_x > 0 ) && ( ball_x <= 60 ) )
	    {
	      std::ostringstream osNeck;
	      anglePan = anglePan + Utility::DegreeToRadians(6.0);
	      
	      //osNeck << "Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << anglePan << " " << "Speed" << "=" << 100.0 ;
	      
	      //const std::string replyConstT = osNeck.str();
	      //std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
	      varCycleTime =800000;
	      varCycles=4;
	      std::stringstream comstream;
	      reply = SendCommand( robot_socket, robot_iterator,  comstream.str() );
	      std::string com = "Arash Move = OmniWalk Dx=0.04 Dy=0.01 Dtheta=0.1 CycleTimeUSec = 650000 Cycles=2\n";
	      reply = SendCommand( robot_socket, robot_iterator,  com );
		  
	      usleep(650000 * 4 );
#ifndef NDEBUG	      
		  
 	      std::cout << "Arash_sprint.cpp Turn Right" << std::endl; 
		  		  
#endif		  
 	    }
	  else if ( ( ball_x > 60 )  && ( (ball_x <= 100 ) ) ) 
 	    {
	      std::ostringstream osNeck;
	      
 	      //osNeck << "Arash Actuator "<< " NeckTransversal " << " Position " << "Angle" << " = " << anglePan << " " << "Speed" << " = " << 100.0 ;
 	      //const std::string replyConstT = osNeck.str();
 	      //std::string replyT = SendCommand( robot_socket, robot_iterator,  replyConstT );
 	      std::string com = "Arash Move = OmniWalk Dx=0.04 Dy=0.0 Dtheta=0.0 CycleTimeUSec = 650000 Cycles=4\n";
 	      reply = SendCommand(robot_socket, robot_iterator, com);
 	      usleep(3600000);
#ifndef NDEBUG	      
	      
 	      std::cout << "Arash_sprint.cpp going forward" << std::endl;
#endif
 	    }
 	  else if ( (ball_x > 100 ) && ( (ball_x <= 160 ) ) ) 
 	    {	  
 	      std::ostringstream osNeck;
 	      anglePan = anglePan - Utility::DegreeToRadians(6.0);
 	      //osNeck << "Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << anglePan << " " << "Speed" << "=" << 100.0 ;
 	      //const std::string replyConstT = osNeck.str();
 	      //std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
		
 	      std::string com = "Arash Move = OmniWalk Dx=0.04 Dy=0.0 Dtheta=0 CycleTimeUSec = 650000 Cycles=2\n";
	      reply = SendCommand( robot_socket, robot_iterator,  com );
	      usleep (650000*4);
#ifndef NDEBUG	      
	      
	      std::cout << "Going Left" << std::endl; 
		  
#endif	      
		  
	    } //else going left
	      //} if (ball_found)
	}
      //check = -1;
      

      /*

      else
	{
	  
	  std::ostringstream osNeck;
	  bool lost = true;
	  
	  if( lost)
	    {
	      
	      osNeck << " Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << -0.85 << " " << "Speed" << "=" << 100.0 ;
	      std::string replyConstT = osNeck.str();
	      std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
	      
	      replyT = SendCommand( robot_socket, robot_iterator, "Results" );
	      int checkRight = ParseFromStreamVision( replyVision );
	      if (checkRight >=0 )
		{
		  std::string com = "Arash Move = OmniWalk Dx=0.04 Dy=0.01 Dtheta=0.1 CycleTimeUSec = 650000 Cycles=4\n";
		  reply = SendCommand( robot_socket, robot_iterator,  com );
		  
		  usleep(650000 * 4 );
		  osNeck << " Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << 0.0 << " " << "Speed" << "=" << 1000.0 ;
		  std::string replyConstT = osNeck.str();
		  std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
		  
#ifndef NDEBUG	      
		  
		  std::cout << "Arash_sprint.cpp Turn Right" << std::endl; 
		  lost = false;
#endif	
		}
	    }
	  
	  if( lost )
	    {
	      osNeck << " Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << 0.85 << " " << "Speed" << "=" << 100.0 ;
	      std::string replyConstT = osNeck.str();
	      std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
	      
	      replyT = SendCommand( robot_socket, robot_iterator, "Results" );
	      int checkLeft = ParseFromStreamVision( replyVision );
	      if( checkLeft >= 0)
		{
		  std::string com = "Arash Move = OmniWalk Dx=0.04 Dy=-0.01 Dtheta=-0.1 CycleTimeUSec = 650000 Cycles=4\n";
		  reply = SendCommand( robot_socket, robot_iterator,  com );
		  
		  usleep(650000 * 4 );
		  osNeck << " Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << 0.0 << " " << "Speed" << "=" << 1000.0 ;
		  std::string replyConstT = osNeck.str();
		  std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
		  
#ifndef NDEBUG	      
		  
		  std::cout << "Arash_sprint.cpp Turn Left" << std::endl; 
		  lost = false;
#endif	
		}
	      
	    }
	  
	  
	    float speed = 100.0;
	  Position pos;
	  bool found = false;
	  for(size_t i = 0; i < 1 ; i++)
	    {
	      if ( !found )
		{
		  if ( !cont )
		    {
		      angleTiltStart = Utility::DegreeToRadians(40.0);
		    }
		 
		  float angleTiltRange = Utility::DegreeToRadians(19.0);
		  float angleTiltEnd   = Utility::DegreeToRadians(0.0);
		  
		  //( angleTilt >= angleTiltEnd  )
		  for( angleTilt = angleTiltStart ; (!found ) && ( angleTilt <= angleTiltEnd  ); angleTilt += angleTiltRange)
		    {
		      float diff;
		      float start;
		      float end;
		      float mult;
		      
		      if( cont )
			{
			  if ( anglePanStart < 0.0  ) 
			    {
			      start = anglePanStart;
			      end   = Utility::DegreeToRadians( 40.0);
			      diff  = Utility::DegreeToRadians(15.0);
			      mult  = 1.0;
			    }
			  else 
			    {
			      start = anglePanStart;
			      end   = Utility::DegreeToRadians( -40.0);
			      diff  = Utility::DegreeToRadians(-15.0);
			      mult  = -1.0;
			    }
			}
		      else
			{
			  if ( anglePan < 0.0  ) 
			    {
			      start = Utility::DegreeToRadians( -40.0);
			      end   = Utility::DegreeToRadians( 40.0);
			      diff  = Utility::DegreeToRadians(15.0);
			      mult  = 1.0;
			    }
			  else 
			    {
			      start = Utility::DegreeToRadians( 40.0);
			      end   = Utility::DegreeToRadians( -40.0);
			      diff  = Utility::DegreeToRadians(-15.0);
			      mult  = -1.0;
			    }
			}
		      
		     		     		     
		      for( anglePan = start; (!found) && (anglePan * mult < end * mult); anglePan += diff )
			{
			  
			  std::ostringstream osNeck;
			  osNeck << "Arash Actuator "<< "NeckLateral" << " Position " << "Angle" << "=" << angleTilt << " " << "Speed" << "=" << 100.0 ;
			  osNeck << "& Arash Actuator "<< "NeckTransversal" << " Position " << "Angle" << "=" << anglePan << " " << "Speed" << "=" << 100.0 ;
			  const std::string replyConstT = osNeck.str();
			  std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
			  replyVision = SendCommand( vision_socket, vision_iterator, "Results" );
#ifndef NDEBUG
			  std::cout << "Vision " << replyVision << std::endl;
#endif	  
			  for(size_t vLoop = 0; (!found) && (vLoop< 30) ; ++vLoop )
			    {
			      check = ParseFromStreamVision( replyVision );
			      if( check >= 0 )
				{ 
				  found = true;
				  cont  = true;
				  angleTiltStart = angleTilt;
				  anglePanStart  = anglePan;
				  break;
				}
			      usleep(10000);
			    }
			}
		    }
		}
	    }
	  if(!found )
	    {
	      cont = false;
	      speed = 1.0;
	      std::ostringstream osNeck;
	      osNeck << "Arash Actuator "<< "NeckLateral" << " Position " << "Angle" << "=" << -0.77 << " " << "Speed" << "=" << 100.0 ;
	      
	      const std::string replyConstT = osNeck.str();
	      //std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
	      std::string comRight = "Arash Move = OmniWalk Dx=0.02 Dy=-0.01 Dtheta=-0.1 CycleTimeUSec = 650000 Cycles=2\n";
#ifndef NDEBUG	      
	      std::cout << std::endl << std::endl << "Target not found going randomly right" << std::endl << std::endl;	      	      
#endif
	      std::string replyRight = SendCommand( robot_socket, robot_iterator, comRight );
	      usleep(650000*1);
	    }
	  
	}
      */ 
    }
    
  //usleep(100000);
  //count++;
}
  
