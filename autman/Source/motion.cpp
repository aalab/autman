#include "actuator.hpp"
#include "interface.hpp"
#include "motion.hpp"
#include "robot.hpp"
#include "compilerdefinitions.h"

#include <boost/foreach.hpp>

#include <string>
#include <vector>


Motion::Motion(Robot const *robot, std::string tName)
	: name(tName),
	  valid(false)
{
	bool v = false;
	if (tName != "")
	{
		BOOST_FOREACH(Interface *iface, robot->GetInterfaces())
					{
						BOOST_FOREACH(Actuator *act, iface->GetActuators())
									{
										Trajectory const *tmp;
										if ((tmp = act->FindTrajectoryByName(tName)) != nullptr)
										{
											actuators.push_back(act);
											trajectories.push_back(*tmp);
											v = true;
										}
									}
					}
		valid = v;
	}
	else
	{
		valid = true;
	}
}

int
Motion::PulseMotion(float percentage, float motionSpeed) const
{
	int ret = -1;

	for (unsigned int i = 0; i < actuators.size(); ++i)
	{
		Actuator *act = actuators[i];
		Trajectory const &t = trajectories[i];

		float angle = t.Interpolate(percentage);
		/*
		//std::cout<< "Motion::PulseMotion after t interpolate: t->GetSize()" << t.GetSize() << std::endl;
		std::ostringstream cmd;
		cmd << "Actuator " << act->GetName() << " Position " << " Angle " << angle << " Speed " << motionSpeed;
		act->ProcessCommand(cmd.str());
		 */
		act->SendPositionAndSpeedMessage(angle, motionSpeed);
		VERBOSE("Actuator Name: " << act->GetName() << " Angle:" << angle << " Speed " <<
			motionSpeed << " Percentage is:" << percentage << " t.GetSize()" << t.GetSize());
	}
	ret = 0;
	return ret;
}
