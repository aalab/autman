/*
 * MX-series Actuator Definition 
 *
 */

#include "channel.hpp"
#include "robotis_servo_mx_series.hpp"
#include "interface.hpp"
#include "dynamixel_interface.hpp"

#ifndef NDEBUG

#include <boost/io/ios_state.hpp>
#include <iomanip>

#endif

#include <iostream>

#include <cmath>
#include <sstream>

#include "interface.hpp"
#include "utility.hpp"
#include "compilerdefinitions.h"

RobotisServoMXSeries::RobotisServoMXSeries(std::string name, DynamixelInterface *interface,
	boost::property_tree::ptree &pt)
	: RobotisServo(name, interface, pt)
{
	int id = pt.get<int>("id");

	this->id = id;

	home = pt.get<float>("home", defaultHome);
	min = pt.get<float>("min", defaultMin);
	max = pt.get<float>("max", defaultMax);

	this->gainP = pt.get<unsigned int>("gain_p", this->defaultGainP);
	this->gainI = pt.get<unsigned int>("gain_i", this->defaultGainI);
	this->gainD = pt.get<unsigned int>("gain_d", this->defaultGainD);

	VERBOSE("Creating Robotis Servo MX Series " << name << " with id " << this->id);
	hasFeedback = true;
}

int
RobotisServoMXSeries::SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed)
{
	VERBOSE("ISpeed: " << ispeed);
	return this->SendPositionTicksAndISpeed(ticks, ispeed, this->gainD, this->gainI, this->gainP);
}


int RobotisServoMXSeries::SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
	unsigned int gainI, unsigned int gainP)
{
	int result = -1;

	VERBOSE("Speed: " << static_cast<unsigned int>(ispeed));

	if ((ticks < RobotisServoMXSeries::MAX_TICKS) && (ispeed <= RobotisServoMXSeries::MAX_ISPEED))
	{
		servos::dynamixel::mx::PositionSpeedGains data;

		data.gains.d = static_cast<uint8_t>(gainD);
		data.gains.i = static_cast<uint8_t>(gainI);
		data.gains.p = static_cast<uint8_t>(gainP);

		data.padding1 = 0;

		data.positionSpeed.position_LO = LOBYTE(ticks);
		data.positionSpeed.position_HI = HIBYTE(ticks);
		data.positionSpeed.speed_LO = static_cast<uint8_t>(LOBYTE(ispeed));
		data.positionSpeed.speed_HI = static_cast<uint8_t>(HIBYTE(ispeed));

		VERBOSE("Speed: " << static_cast<unsigned int>(ispeed));
		VERBOSE("Speed_LO: " << static_cast<unsigned int>(data.positionSpeed.speed_LO));
		VERBOSE("Speed_HI: " << static_cast<unsigned int>(data.positionSpeed.speed_HI));

		return this->SendPositionTicksISpeedGains(&data);
	}
	else
	{
		ERROR("RobotisServoMxSeries::SendPositionTicksAndISpeed failed with ticks " << ticks << " and ispeed " <<
		ispeed);
	}

	return result;
}

unsigned int
RobotisServoMXSeries::AngleRadiansToPositionTicks(float angle)
{
	// Angle is offset from home and converted to range between 0 and 360 deg
	float a = Utility::NormalizeAngle(angle);
	if (a < 0)
	{
		a = 2.0 * M_PI + a;
	}
	// The total range for the robotis servo mx series is 360 deg.
	//4096 ticks/360 deg


	int ticks = static_cast<int>(  a / (2.0 * M_PI) * 4096);

#ifndef NDEBUG
	std::cout << "AngleRadiansToPositionTicks MX Series(" << angle << ")=" << " a= " << a << " ticks=" << ticks <<
	std::endl;
#endif

	return static_cast<unsigned int>( Utility::Clamp(0, static_cast<int>(ticks), 4095));
}

float
RobotisServoMXSeries::AnglePositionTicksToRadians(unsigned int ticks)
{
	float f;
	if (ticks < 4096)
	{
		f = Utility::DegreeToRadians(0.0) + (static_cast<float>(ticks) / 4096.0) * Utility::DegreeToRadians(360.0);
	}
	else
	{
		f = nanf("ticksToRadians");
	}
#ifndef NDEBUG
	std::cout << "RobotisServoMXSeries::AnglePositionTicksToRadians(" << ticks << ")=" << f << std::endl;
#endif
	return f;
}

int
RobotisServoMXSeries::ReadPositionTicksAndISpeedAndILoad(servos::dynamixel::mx::ReadStatePackage* resultPackage,
	protocols::dynamixel1::ErrorByte *error)
{
	int result = 1;

	Channel *chan = GetChannel();

	if (chan != nullptr)
	{
		ChannelLockGuard clg(*chan);

		unsigned int bufferSize = 8;
		unsigned int replySize = 14;
		uint8_t buffer[bufferSize];
		uint8_t reply[replySize];

		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::HEADER1_INDEX] = RobotisServoMXSeries::HEADER;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::HEADER2_INDEX] = RobotisServoMXSeries::HEADER;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::ID_INDEX] = this->id;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::LENGTH_INDEX] = sizeof(buffer) - 4;

		buffer[4] = RobotisServoMXSeries::CMD_READ_DATA;
		buffer[5] = RobotisServoMXSeries::PRESENT_POSITION_L;
		buffer[6] = 8;
		buffer[7] = DynamixelInterface::CalculateCheckSum(&buffer[2], sizeof(buffer) - 2 - 1);

		VERBOSENEL("Sending buffer (" << bufferSize << ") [");
		VERBOSEDATA(buffer, bufferSize);
		VERBOSENEL("]");

		int ret;
		if ((ret = chan->writeData(buffer, bufferSize)) > 0)
		{
			VERBOSEB(" OK!");

			VERBOSE("Receiving reply");
			if ((ret = this->GetInterface()->readPackage(reply, replySize)) > 0)
			{
				VERBOSENEL(" OK! (" << ret << ") [");
				VERBOSEDATA(&reply[0], static_cast<unsigned int>(ret));
				VERBOSEB("");

				if (error != NULL)
					RobotisServo::byteToStructError(reply[RobotisProtocolIndexes::ERROR_INDEX], error);

				memcpy((void*)resultPackage, &reply[RobotisProtocolIndexes::READ_ANSWER_CONTENT_INDEX], sizeof(servos::dynamixel::mx::ReadStatePackage));
				/*
				VERBOSE("Content: ");
				VERBOSEB("\tTicks: " << static_cast<int>(content->position));
				VERBOSEB("\tSpeed: " << static_cast<int>(content->speed));
				VERBOSEB("\tLoad: " << static_cast<int>(content->load));
				VERBOSEB("\tVoltage: " << static_cast<int>(content->voltage));
				VERBOSEB("\tTemperature: " << static_cast<int>(content->temperature));
				*/

				/*
				if (ticks != 0)
				{
					*ticks = (reply[6] << 8) | (reply[5]);
#ifndef NDEBUG
					VERBOSE("ticks " << *ticks);
#endif
				}

				if (speed != 0)
					*speed = (reply[8] << 8) | (reply[7]);

				if (iload != 0)
					*iload = (reply[10] << 8) | (reply[9]);
				*/

				result = 0;
			}
			else
			{
				VERBOSEB(" FAIL!");
				// TODO: Return fail.
				result = -2;
			}
		}
		else
		{
			VERBOSEB("FAIL!");
			// TODO: Return fail.
			result = -1;
		}

/*
		if ((ret = chan->CommandTxAndRx(buffer, bufferSize, reply, replySize)) >= 0)
		{
#ifndef NDEBUG
			boost::io::ios_flags_saver ifs(std::cout);
			VERBOSENEL("Received reply (" << ret << ") ");
			for (unsigned int i = 0; i < static_cast<unsigned int>(ret); i++)
			{
				VERBOSEBNEL(std::hex << std::setfill('_') << std::setw(3) << static_cast<unsigned int>(reply[i]));
			}
			VERBOSEBNEL(std::endl);
			ifs.restore();
#endif

			if (error != NULL)
				RobotisServo::byteToStructError(reply[RobotisProtocolIndexes::ERROR_INDEX], error);

			if (ticks != 0)
			{
				*ticks = (reply[6] << 8) | (reply[5]);
#ifndef NDEBUG
				VERBOSE("ticks " << *ticks);
#endif
			}

			if (speed != 0)
			{
				*speed = (reply[8] << 8) | (reply[7]);
			}

			if (iload != 0)
			{
				*iload = (reply[10] << 8) | (reply[9]);
			}
			result = 0;
		}
		else
		{
			ERROR("failed with error " << ret);
			result = -1;
		}
*/
	}
	else
	{
		ERROR("failed channel not defined");
	}
	return result;
}

int
RobotisServoMXSeries::ReadFullData(servos::dynamixel::mx::ReadFullStatePackage *resultPackage,
	protocols::dynamixel1::ErrorByte *error)
{
	int result = 1;

	Channel *chan = this->GetChannel();

	if (chan != nullptr)
	{
		ChannelLockGuard clg(*chan);

		unsigned int bufferSize = 8;
		unsigned int replySize = sizeof(servos::dynamixel::mx::ReadFullStatePackage) + 6;
		uint8_t buffer[bufferSize];
		uint8_t reply[replySize];

		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::HEADER1_INDEX] = RobotisServoMXSeries::HEADER;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::HEADER2_INDEX] = RobotisServoMXSeries::HEADER;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::ID_INDEX] = this->id;
		buffer[RobotisServoMXSeries::RobotisProtocolIndexes::LENGTH_INDEX] = sizeof(buffer) - 4;

		buffer[4] = RobotisServoMXSeries::CMD_READ_DATA;
		buffer[5] = RobotisServoMXSeries::TORQUE_ENABLE;
		buffer[6] = sizeof(servos::dynamixel::mx::ReadFullStatePackage);
		buffer[7] = DynamixelInterface::CalculateCheckSum(&buffer[2], sizeof(buffer) - 2 - 1);

		VERBOSENEL("Sending buffer (" << bufferSize << ") [");
		VERBOSEDATA(buffer, bufferSize);
		VERBOSENEL("]");

		int ret;
		if ((ret = chan->writeData(buffer, bufferSize)) > 0)
		{
			VERBOSEB(" OK!");

			VERBOSE("Receiving reply");
			if ((ret = this->GetInterface()->readPackage(reply, replySize)) > 0)
			{
				VERBOSENEL(" OK! (" << ret << ") [");
				VERBOSEDATA(&reply[0], static_cast<unsigned int>(ret));
				VERBOSEB("");

				if (error != NULL)
					RobotisServo::byteToStructError(reply[RobotisProtocolIndexes::ERROR_INDEX], error);

				memcpy((void*)resultPackage, &reply[RobotisProtocolIndexes::READ_ANSWER_CONTENT_INDEX], sizeof(servos::dynamixel::mx::ReadFullStatePackage));

				/*
				VERBOSE("Content: ");
				VERBOSEB("\t torque: " << resultPackage->torque);
				VERBOSEB("\t led: " << resultPackage->led);
				VERBOSEB("\t gains:");
				VERBOSEB("\t\tP: " << static_cast<unsigned int>(resultPackage->gains.p));
				VERBOSEB("\t\tI: " << static_cast<unsigned int>(resultPackage->gains.i));
				VERBOSEB("\t\tD: " << static_cast<unsigned int>(resultPackage->gains.d));
				VERBOSEB("\t goalPosition: " << LOHI_INT(resultPackage->goalPosition_LO, resultPackage->goalPosition_HI));
				VERBOSEB("\t movingSpeed: " << LOHI_INT(resultPackage->movingSpeed_LO, resultPackage->movingSpeed_HI));
				VERBOSEB("\t torqueLimit: " << LOHI_INT(resultPackage->torqueLimit_LO, resultPackage->torqueLimit_HI));
				VERBOSEB("\t positionSpeed:");
				VERBOSEB("\t\tposition: " << LOHI_INT(resultPackage->positionSpeed.position_LO, resultPackage->positionSpeed.position_HI));
				VERBOSEB("\t\tspeed: " << LOHI_INT(resultPackage->positionSpeed.speed_LO, resultPackage->positionSpeed.speed_HI));
				VERBOSEB("\t presentLoad: " << LOHI_INT(resultPackage->presentLoad_LO, resultPackage->presentLoad_HI));
				VERBOSEB("\t presentVoltage: " << static_cast<unsigned int>(resultPackage->presentVoltage));
				VERBOSEB("\t presentTemperature: " << static_cast<unsigned int>(resultPackage->presentTemperature));
				VERBOSEB("\t moving: " << static_cast<unsigned int>(resultPackage->moving));
				*/

				result = 0;
			}
			else
			{
				VERBOSEB(" FAIL!");
				// TODO: Return fail.
				result = -2;
			}
		}
		else
		{
			VERBOSEB("FAIL!");
			// TODO: Return fail.
			result = -1;
		}
	}
	else
	{
		ERROR("failed channel not defined");
	}
	return result;
}

int RobotisServoMXSeries::SendPositionTicksISpeedGains(servos::dynamixel::mx::PositionSpeedGains *params)
{
	int result = -1;

	if (
		(LOHI_UINT(params->positionSpeed.position_LO, params->positionSpeed.position_HI) < RobotisServoMXSeries::MAX_TICKS)
		&& (LOHI_UINT(params->positionSpeed.speed_LO, params->positionSpeed.speed_HI) < RobotisServoMXSeries::MAX_ISPEED))
	{
		VERBOSENEL("Body package [");
		VERBOSEDATA(((uint8_t*)params), sizeof(servos::dynamixel::mx::PositionSpeedGains));
		VERBOSEB("]");

		VERBOSEB("\tGainD:" << static_cast<unsigned int>(params->gains.d));
		VERBOSEB("\tGainI:" << static_cast<unsigned int>(params->gains.i));
		VERBOSEB("\tGainP:" << static_cast<unsigned int>(params->gains.p));
		VERBOSEB("\tSpeed:" << LOHI_UINT(params->positionSpeed.speed_LO, params->positionSpeed.speed_HI));
		VERBOSEB("\tPosition:" << LOHI_UINT(params->positionSpeed.position_LO, params->positionSpeed.position_HI));

		result = this->WriteRegisters(D_GAIN, (uint8_t*)params, sizeof(servos::dynamixel::mx::PositionSpeedGains));
		if (result >= 0)
		{
			VERBOSEB("OK!");
			result = 0;
		}
		else
		{
			VERBOSEB("FAIL!");
			ERROR("failed with " << result);
		}
	}
	else
	{
		ERROR("failed with ticks " << LOHI_UINT(params->positionSpeed.position_LO, params->positionSpeed.position_HI) << " and ispeed " << LOHI_UINT(params->positionSpeed.speed_LO, params->positionSpeed.speed_HI));
	}
	return result;
}

int RobotisServoMXSeries::SendGains(servos::dynamixel::mx::Gains *params)
{
	int result = -1;

	VERBOSENEL("Sending [");
	VERBOSEDATA(((uint8_t*)params), sizeof(servos::dynamixel::mx::Gains));
	VERBOSEB("]");

	VERBOSEB("\td:" << static_cast<unsigned int>(params->d));
	VERBOSEB("\ti:" << static_cast<unsigned int>(params->i));
	VERBOSEB("\tp:" << static_cast<unsigned int>(params->p));

	result = WriteRegisters(D_GAIN, (uint8_t*)params, sizeof(servos::dynamixel::mx::Gains));
	if (result >= 0)
	{
		VERBOSEB("OK!");
		result = 0;
	}
	else
	{
		VERBOSEB("FAIL!");
		ERROR("failed with " << result);
	}
	return result;
}

int RobotisServoMXSeries::ReadGains(servos::dynamixel::mx::Gains *params, protocols::dynamixel1::ErrorByte *error)
{
	int result = 1;

	Channel *chan = GetChannel();

	if (chan != nullptr)
	{
		ChannelLockGuard clg(*chan);

		protocols::dynamixel1::ReadRegisters command;
		protocols::dynamixel1::init(&command, RobotisServoMXSeries::D_GAIN, 3);
		command.header.id = this->id;
		protocols::dynamixel1::checksum(&command);

		VERBOSENEL("Sending buffer (" << sizeof(protocols::dynamixel1::ReadRegisters) << ") [");
		VERBOSEDATA((uint8_t*)&command, sizeof(protocols::dynamixel1::ReadRegisters));
		VERBOSENEL("]");

		int ret;
		if ((ret = chan->writeData((uint8_t*)&command, sizeof(protocols::dynamixel1::ReadRegisters))) > 0)
		{
			VERBOSEB(" OK!");

			unsigned int replySize = sizeof(protocols::dynamixel1::StatusPackageHeader) + 3;
			uint8_t reply[replySize];

			VERBOSE("Receiving reply");
			if ((ret = this->GetInterface()->readPackage(reply, replySize)) > 0)
			{
				VERBOSENEL(" OK! (" << ret << ") [");
				VERBOSEDATA(&reply[0], static_cast<unsigned int>(ret));
				VERBOSEB("]");

				protocols::dynamixel1::StatusPackageHeader *response = (protocols::dynamixel1::StatusPackageHeader*)reply;

				if (error != NULL)
					RobotisServo::byteToStructError(response->error, error);

				memcpy((void*)params, &response->data, 3);

				result = 0;
			}
			else
			{
				VERBOSEB(" FAIL!");
				result = -2;
			}
		}
		else
		{
			VERBOSEB("FAIL!");
			result = -1;
		}
	}
	else
	{
		ERROR("failed channel not defined");
	}
	return result;
}
