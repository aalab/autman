#include "rb110_serial_interface.hpp"

#include "interface.hpp"
#include "roboard.h"
#include "robotis_servo.hpp"
#include "robotis_servo_mx_series.hpp"

#include <boost/property_tree/ptree.hpp>
#include <cstdio>
#ifndef NDEBUG
#include <boost/io/ios_state.hpp>
#include <iomanip>
#endif


Rb110SerialInterface::Rb110SerialInterface( std::string name, boost::property_tree::ptree pt )
: Interface( name, pt ),
  comPort(UNDEFINED_COMPORT),
  syncCommandReady( false ),
  syncOut(0)
{
#ifndef NDEBUG
  std::cout << "Creating Rb110 Serial Interface " << name << std::endl;
#endif

  SetInterface( pt.get<std::string>("interface","/dev/ttyS0") );
  SetBaudrate( pt.get<std::string>("baudrate","115200") );  

  type = DYNAMIXEL;
}

int
Rb110SerialInterface::Open( void )
{
  if ( ! opened )
    {
      if ( interface == "/dev/ttyS2" ) 
	{
	  comPort = 1;
	}
      else if ( interface == "/dev/ttyS3" )
	{
	  comPort = 2;
	}
    }
  
  if ( comPort != UNDEFINED_COMPORT )
    {
      if( !com_Init(comPort, COM_HDUPLEX) )
	{
	  std::cerr << "ERROR: " << roboio_GetErrMsg() << std::endl;
	  return -1;
	}
	  
      // Set data format as 8 bits, 1 stop bit, no parity
      com_SetFormat(comPort, COM_BYTESIZE8,COM_STOPBIT1,COM_NOPARITY);
	  
      int baud = UNDEFINED_BAUDRATE;
      if ( baudrate == "500000" ) 
	{
	  baud = COMBAUD_499200BPS;
	}
      else if ( baudrate == "115200" )
	{
	  baud = COMBAUD_115200BPS;
	}
      else
	{
	  std::cerr << "Unknown baudrate " << baudrate << std::endl;
	  return -2;
	}
      if ( baud != UNDEFINED_BAUDRATE )
	{
	  com_SetBaud(comPort, baud );
	}
    
      opened = true;
    }
  else
    {
      // Try and open it as a standard Linux serial port
      
    }
  return 0;
}

int
Rb110SerialInterface::Close( void )
{
  int res = 0;

  res = Interface::Close( );
  if ( opened )
    {
    }
  opened = false;

  return res;
}

int
Rb110SerialInterface::CommandTxAndRx( uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[], unsigned int const replyLength )
{
  int ret = -1;
  if ( comPort != UNDEFINED_COMPORT )
    {
      for(unsigned int i = 0; i < replyLength; i++ )
	{
	  reply[i] = 0;
	}
      bool err = com_ServoTRX( comPort, const_cast<uint8_t *>( buffer ), bufferLength, reply, replyLength );
      if ( err )
	{
	  ret = replyLength;
	}
      else
	{
	  ret = -2;
	}
    }
  else
    {
      ret = -1;
    }
  return ret;
}

int
Rb110SerialInterface::AddSyncCommand( unsigned int id, unsigned int ticks, unsigned int ispeed )
{
  int ret = -1;

#ifndef NDEBUG
  std::cout << "AddSyncCommand called syncOut=" << syncOut << std::endl;
#endif

  if ( ( syncCommandReady ) && ( syncOut < sizeof(syncCommands)/sizeof(syncCommands[0] ) ) )
    {
      syncCommands[syncOut].id = id;
      syncCommands[syncOut].ticks_L = ticks & 0x0ff;
      syncCommands[syncOut].ticks_H = ( ticks >> 8 ) & 0x0ff;
      syncCommands[syncOut].ispeed_L = ispeed & 0xff;
      syncCommands[syncOut].ispeed_H = ( ispeed >> 8 ) & 0xff;

      syncOut++;
      ret = 0;
    }
  return ret;
}

int
Rb110SerialInterface::ReadySyncCommand( void )
{
  syncCommandReady = true;
  return 0;
}

int
Rb110SerialInterface::SendSyncCommand( void )
{
  // Code here to send the sync write message
  unsigned int bufferLength = 4 + syncOut * sizeof(SyncCommand)+4;
  uint8_t buffer[bufferLength];
  uint8_t out;
  int ret = -1;

#ifndef NDEBUG
  std::cout << "Rb110SerialInterface::SendSyncCommand(void)" << std::endl;
#endif
  out = 0;
  buffer[out++] = RobotisServo::HEADER;
  buffer[out++] = RobotisServo::HEADER;
  buffer[out++] = RobotisServo::BROADCAST_ID;
  buffer[out++] = sizeof(buffer) - 4;
  buffer[out++] = RobotisServo::CMD_SYNC_WRITE;
  buffer[out++] = RobotisServo::GOAL_POSITION_L;
  buffer[out++] = 4;


  for( unsigned int i = 0; i < syncOut; i++ )
    {
      buffer[out++] = syncCommands[i].id;
      buffer[out++] = syncCommands[i].ticks_L;
      buffer[out++] = syncCommands[i].ticks_H;
      buffer[out++] = syncCommands[i].ispeed_L;
      buffer[out++] = syncCommands[i].ispeed_H;      
    }

  uint8_t crc = RobotisServo::CalculateCheckSum(&buffer[2], bufferLength - 2 - 1 );
  buffer[out++] = crc;

#ifndef NDEBUG
  std::cout << "SendSyncCommand bufferLength " << bufferLength << " out " << out << std::endl;
#endif
#ifndef NDEBUG
  boost::io::ios_flags_saver ifs( std::cout );
  std::cout << "Sending buffer ";
  for( unsigned int i = 0; i < bufferLength; i++ )
    {
      std::cout << std::hex << std::setfill('_') << std::setw(3); 
      std::cout << static_cast<unsigned int>( buffer[i] );
    }
  std::cout << std::endl;
  ifs.restore();
#endif


  bool err = com_ServoTRX( comPort, const_cast<uint8_t *>( buffer ), bufferLength, 0, 0 );
  if ( err )
    {
      ret = 0;
    }
  else
    {
      ret = -1;
    }

  syncCommandReady = 0;
  syncOut = 0;
  return ret;
}

