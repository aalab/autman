/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 08, 2015
 */

#ifndef MOTIONLIBRARY_AUTINDEXES_H
#define MOTIONLIBRARY_AUTINDEXES_H

#include <string>
#include <vector>

class AutIndex
{
public:
	AutIndex();
	AutIndex(std::string name, int index);
	std::string name;
	int index;
};

class AutIndexes
{
public:
	static std::vector<AutIndex> indexes;
	static void initIndexes();

	static int byName(std::string name);
	static std::string byIndex(std::string name);
};


#endif //MOTIONLIBRARY_AUTINDEXES_H
