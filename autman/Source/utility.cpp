/*
 * Various utility functions
 */

#include "utility.hpp"

#include <cmath>

#include <string>


std::string 
Utility::ReplaceChars( std::string str, std::string const replace, char newChar )
{
  size_t found = str.find_first_of( replace );
  while( found != std::string::npos )
    {
      str[found] = newChar;
      found = str.find_first_of( replace, found + 1 );
    }

  return str;
}

float
Utility::NormalizeAngle( float angle )
{
  while (angle > M_PI )
    {
      angle = angle - 2.0 * M_PI;
    }

  while( angle <= -M_PI )
    {
      angle = angle + 2.0 * M_PI;
    }
  return angle;
}


