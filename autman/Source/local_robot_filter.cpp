#include "local_robot_filter.hpp"

#include "robot.hpp"

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#if defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

using boost::asio::local::stream_protocol;

void
LocalRobotFilter::Start( void )
{
  socket.async_read_some( boost::asio::buffer(data), boost::bind( & LocalRobotFilter::ProcessRead, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred) );
}

void
LocalRobotFilter::ProcessRead( boost::system::error_code const & ec, std::size_t size )
{
#ifndef NDEBUG
  std::cout << "LocalRobotFilter::ProcessRead called" << std::endl;
#endif 

  if ( !ec )
    {

    }
  else
    {
      throw boost::system::system_error ( ec );
    }
}


void
LocalRobotFilter::ProcessWrite( boost::system::error_code const & ec )
{
#ifndef NDEBUG
  std::cout << "LocalRobotFilter::ProcessWrite called" << std::endl;
#endif  
  if ( ! ec )
    {

    }
  else
    {
      throw boost::system::system_error( ec );
    }
}

void
LocalRobotFilter::Run( boost::asio::io_service * ioService )
{
  try
    {
      ioService->run();
    }
  catch ( std::exception & e )
    {
      std::cerr << "Exception in thread: " << e.what() << std::endl;
      std::exit(1);
    }
}

#endif
