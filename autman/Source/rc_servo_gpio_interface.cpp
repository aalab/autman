/*
 * Interfaces Class for Dynamixel Servos on an RS485 Serial Line
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#include "rc_servo_gpio_interface.hpp"

#include "interface.hpp"
#include "roboard.h"

#include <boost/property_tree/ptree.hpp>

#include <cstdio>

RCServoGPIOInterface::RCServoGPIOInterface( std::string name, boost::property_tree::ptree & pt , Robot const * robot )
  : Interface( name, pt, robot )
{
#ifndef NDEBUG
  std::cout << "Creating RC Servo GPIO Interface " << name << std::endl;
#endif

  type = RC_SERVO_GPIO;
}

int
RCServoGPIOInterface::Open( void )
{
  int ret = 0;
  if ( ! opened )
    {
      long used = 0;
      
      for( std::vector<Actuator *>::iterator it = actuators.begin();
	   it != actuators.end();
	   ++it )
	{
	  int id = (*it)->GetID();
	  used = used | (1 << id );

	  if ( (*it)->GetType() == Actuator::GENERIC_RC_SERVO_NO_FEEDBACK )
	    {
	      //rcservo_SetServo(id, RCSERVO_SERVO_DEFAULT_NOFB);    
	    }
	}

#ifndef NDEBUG
      std::cout << "Init servos with " << used << std::endl;
#endif      
      if (rcservo_Init( used ) == false)  
	{
	  std::cerr << "ERROR: fail to init RC Servo lib " << roboio_GetErrMsg();
	  return -1;
	}

      rcservo_EnterPWMMode();
      opened = true;
    }
  return ret;
}

int
RCServoGPIOInterface::Close( void )
{
  int ret = 0;
  if ( opened )
    {
      for( std::vector<Actuator *>::iterator it = actuators.begin();
	   it != actuators.end();
	   ++it )
	{
	  int err;
	  if ( ( err = (*it)->PowerOff() ) < 0 ) 
	    {
	      std::cerr << "RCServoGPIOInterface::Close( void ) failed powerdown on actuator " << (*it)->GetID() << std::endl;
	      ret = -1;	      
	    }
	}
      
      rcservo_Close();
      opened = false;
    }
  return ret;
}

