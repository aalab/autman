#include "position.hpp"

#include "utility.hpp"
#include <json/value.h>
#include <json/reader.h>
#include <string>
#include <istream>
#include <sstream>

#ifndef NDEBUG
#   include <iostream>
#endif

std::string const Position::jointNames[] =
  {
    "LeftShoulderLateral",
    "LeftShoulderFrontal",
    "LeftElbowLateral",
    "RightShoulderLateral",
    "RightShoulderFrontal",
    "RightElbowLateral",
    "NeckTransversal",
    "NeckLateral",
    "LeftHipTransversal",
    "LeftHipFrontal",
    "LeftHipLateral",
    "LeftKneeLateral",
    "LeftAnkleLateral",
    "LeftAnkleFrontal",
    "RightHipTransversal",
    "RightHipFrontal",
    "RightHipLateral",
    "RightKneeLateral",
    "RightAnkleLateral",
    "RightAnkleFrontal",
    
  };

int
Position::parseFromStream(std::istream & is )
{
    Json::Value value;
    Json::Reader reader;
  int result = -1;

  for( unsigned int i = 0; i < NUM_JOINTS; i++ )
    {
      joints[i] = -1.0;
    }
  valid = false;

  std::string status;

  std::getline(is, status, '&' );
    bool parseGood = reader.parse(status, value);
  if ( parseGood == true )
    {
      std::string com;
        result = 1;
        for (unsigned int i = 0; i < NUM_JOINTS; i++){
            joints[i] = value["data"][Position::jointNames[i]]["angle"].asFloat();
        }
    }
  else
    {
      std::cerr << "ERROR: Position::parseFromString failed with status " << status << std::endl;
    }
  return result;
}

int
Position::parseJointFromString( std::string const & com )
{
  int result = 0;

  std::istringstream ins( com );
  std::string initial;
  bool angleOk = false;

  ins >> initial;

#ifndef NDEBUG
  std::cout << "Position::parseJointFromString( " << com << ") with initial " << initial << std::endl;
#endif

  int index = FindJointIndexByName( initial );
  if ( ( index >= 0 ) && (index < NUM_JOINTS) )
    {
      float angle = -1234.0;
      std::string attribute;

      while ( ! ins.eof() )
	{
	  ins >> attribute;
#ifndef NDEBUG
	  std::cout << "Position::parseJointFromString" << " " << "Attribute " << attribute << std::endl;
#endif
	  if ( attribute == "angle" )
	    {
	      ins >> angle;
#ifndef NDEBUG
	      std::cout << "Position::parseJointFromString" << " " << "angle " << angle << std::endl;
#endif
	      if ( ins.good() )
		{
		  angleOk = true;
		}
	    }
	  else
	    {
#ifndef NDEBUG
	      std::cout << "Position::parseJointFromString" << " " << "Skipping attribute " << attribute << std::endl;
#endif
	      std::string ignore;
	      ins >> ignore;
	    }
	}

      if ( angleOk )
	{ 
	  joints[index] = angle;
	  result = 0;
	}
      else
	{
	  std::cerr << "Error: Unable to parse joint from stream" << com << std::endl;
	  result = -1;
	}     
    } 
  else
    {
      std::cerr << "Error: Unable to find joint with name " << initial << std::endl;
    }
  return result;
}

int
Position::FindJointIndexByName( std::string jointName )
{
  int result = -1;
  for( unsigned int i = 0; i < NUM_JOINTS; ++i )
    {
      if ( Position::jointNames[i] == jointName )
	{
	  result = i;
	  break;
	}
    }
  return result;
}

std::string
Position::ToCommandString( void ) const
{
  std::ostringstream os;

  for ( unsigned int i = 0; i < NUM_JOINTS; i++ )
    {
      if ( i > 0 )
	{
	  os << "&";
	}
      os << "Arash Actuator " << jointNames[i] << " Position " << "Angle" << "=" << joints[i] << " " << "Speed" << "=" << 10 ;
    }
  return os.str();
}

void
Position::Print( void ) const
{
  for(unsigned int i = 0; i < NUM_JOINTS; ++i )
    {
      std::cout << "Arash Joint " << jointNames[i] << " position (deg) " << Utility::RadiansToDegree( joints[i] ) << std::endl;
    }
}

