
#include "config.h"

#include "position.hpp"
#include "robot.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>
#include <string>

#define DEFAULT_VISION_PORT "2134"
#define DEFAULT_VISION_SERVER "127.0.0.1" 
#define DEFAULT_ROBOT_PORT  "1313"
#define DEFAULT_ROBOT_SERVER "127.0.0.1" 

static bool ball_found = false;
static int ball_x      = -1;
static int ball_y      = -1;
static int topX        = -1;
static int topY        = -1;
static int bottomX     = -1;
static int bottomY     = -1;

using boost::asio::ip::udp;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );

  socket.send_to( boost::asio::buffer( command ), * iterator );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  std::cout << "Soccer::SendCommand Responses " << reply << std::endl;
#endif					   		      

  return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
  return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
  std::cout<<"from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif 
  return SendCommand( socket, iterator, trajectory.ToCommandString() );
}

static std::vector<Trajectory> motions;

void
LoadTrajectories( boost::property_tree::ptree & pt )
{
  using boost::property_tree::ptree;
  
  BOOST_FOREACH( ptree::value_type & v , pt.get_child("trajectories"))
    {
      std::string motionName = (std::string)v.first.data();
#ifndef NDEBUG
      std::cout << "Found Trajectory for: " << motionName << std::endl;
#endif
      BOOST_FOREACH( ptree::value_type & joint , v.second)
	{ 
	  std::string jointName = (std::string)joint.first.data();
#ifndef NDEBUG
	  std::cout << "Found  joint: " << jointName << std::endl;
#endif
	  Trajectory tmpT( motionName, jointName, joint.second );
	  motions.push_back(tmpT);
	} 
    }
}

void 
LoadTrajectories(std::string const filename )
{
  using boost::property_tree::ptree;
  ptree pt;

  read_info( filename, pt );
  LoadTrajectories( pt );
}


unsigned long int tdiffUSec(struct timespec const start, struct timespec const end);

void addTimeNSec( struct timespec * time, long int disp );


float polynomialHold(float const amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage);

int
ParseFromStreamVision(std::string const & message)
{
  int result = -1;
  std::string trim;
  std::string keyword;
  unsigned int size;
  unsigned int tx;
  unsigned int ty;
  unsigned int bx;
  unsigned int by;
  std::string target;
  std::string state;
  char sep;
  unsigned int tmpx;
  unsigned int tmpy;

  ball_found = false;

  std::ostringstream os;
  os << "Vision " << message;
  std::istringstream iss( os.str() );

  iss >> keyword;
  iss >> state;
  iss >> sep;
  if ( ( iss ) && ( keyword == "Vision" ) && ( state == "Ok" ) && ( sep == '&' ) )
    {
      while( std::getline( iss, trim, '&' ) )
	{
	  trim = Utility::ReplaceChars(trim ,":=[](),;-", ' ' );
	  Utility::Trim( trim );
	  std::istringstream ist(trim);
	  
#ifndef NDEBUG
	  std::cout<< "-----trim----"<< trim << std::endl;
#endif  
	  
	  ist >> target;
	  
	  ist >> size;
	  ist >> tmpx;   //static variable
	  ist >> tmpy;   //static variable
	  ist >> tx;
	  ist >> ty;
	  ist >> bx;
	  ist >> by;
	  if( ist )
	    {
#ifndef NDEBUG
	      std::cout<<"ParseFromStreamVision found object " << "Target:" << target << ", size:" << size << ", center X" <<tmpx << ", center Y" << tmpy << ", top X"  << tx << ", top Y" << ty << ", bottom X"  << bx << ", bottom Y" << by << std::endl;
#endif
	      if ( size > 100 )
		{
		  result = 0; 
		}
	      if ( ( target == "ball" ) || (target == "goalpost") ) //&& ( size > max ) )
		{
		  // max  = size;
		  ball_x  = tmpx;
		  ball_y  = tmpy;
		  topX    = tx;
		  topY    = ty;
		  bottomX = bx;
		  bottomY = by;
		  //ball_found = true;
		}
	    }
	  else
	    {
	      result = -1;
	      goto err_exit;
	    }
	}
    }
  
 err_exit:  
#ifndef NDEBUG
  std::cout<< "ParseFromStreamVision returns " << result << std::endl;
#endif
  return result;

}
	    
float 
ImageXCoordinateToRadians( float ball_x ) 
{
  float const cameraRange = Utility::DegreeToRadians( 45.0 );
  float const cameraWidth = 160.0;

  float dx = ( ball_x - cameraWidth/2.0f ) / (-cameraWidth/2.0f);
  float dRad = dx * cameraRange;

  return dRad;
}

int 
main(int argc, char *argv[] )
{
  namespace po = boost::program_options;
  std::string vision_port;
  std::string vision_server;
  std::string robot_port;
  std::string robot_server;
  std::string gyro_port;
  std::string gyro_server;
  std::vector<std::string> trajectoryFiles;

  po::options_description commandLineOnlyOptions("Command Line Options");
  commandLineOnlyOptions.add_options()
    ( "version,v", "print version string" )
    ( "help,h"   , "print help message" )
    ( "robot_server" , po::value<std::string>( & robot_server )->default_value(DEFAULT_ROBOT_SERVER), "robot server address" )
    ( "robot_port" , po::value<std::string> ( & robot_port )->default_value(DEFAULT_ROBOT_PORT), "robot port number" )
    ("vision_server" , po::value<std::string> ( & vision_server )->default_value(DEFAULT_VISION_SERVER),"vision server" )
    ("vision_port" , po::value<std::string> ( & vision_port )->default_value(DEFAULT_VISION_PORT),"vision port number" )
    ( "trajectories,t", po::value<std::vector<std::string> >( & trajectoryFiles ), "file with trajectory information");
  
  std::cout << "Ball tracking" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
  std::cout << "Soccer Debugging build" << std::endl;
#endif
  
  po::options_description commandLineOptions;
  commandLineOptions.add(commandLineOnlyOptions);

  po::positional_options_description p;
  p.add("trajectories",-1);

  try 
    {
      po::variables_map vm;
      po::store( po::command_line_parser( argc, argv).options(commandLineOptions).positional(p).run(), vm );
      po::notify( vm );

      if ( vm.count("help") )
	{
	  std::cout << commandLineOptions << "\n";
	  return 1;
	}

      if (vm.count("version") )
	{
	  std::cout << "Scoccer Rostam Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
	  return 1;
	}
    }
  catch( std::exception & e )
    {
      std::cerr << "boost::po exception " << e.what() << std::endl;
      return 1;
    }

  std::cout << "robot server " << robot_server << ", robot port " << robot_port << "vision server " << vision_server << ", vision port " << vision_port << std::endl;
  std::cout << "Trajectory Files" << std::endl;

  for ( std::string s : trajectoryFiles )
    {
      if ( ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".info" ) && 
	   ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".json" ) &&
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".xml" ) && 
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".ini" ) )
	{
	  s = s + ".info";
	}
      
      LoadTrajectories( std::string(CONFIG_DIR) + "/" + s );	 
    }

  std::cout << std::endl;

  boost::asio::io_service io_service;
  udp::socket robot_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  udp::socket vision_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
    
  udp::resolver robot_resolver( io_service );
  udp::resolver::query robot_query( udp::v4(), robot_server, robot_port );
  udp::resolver::iterator robot_iterator = robot_resolver.resolve( robot_query );

  udp::resolver vision_resolver( io_service );
  udp::resolver::query vision_query( udp::v4(), vision_server, vision_port );
  udp::resolver::iterator vision_iterator = vision_resolver.resolve( vision_query );
  
  Position saved;

  std::string reply;
  std::string replyVision;
  std::string com("ReadState");
  reply = SendCommand( robot_socket, robot_iterator, com );

#ifndef NDEBUG
  std::cout << "Reply for Read State " << reply << std::endl;
#endif  

  std::istringstream ireply( reply );

  if ( saved.parseFromStream( ireply ) < 0 )
    {
      std::cerr << "ERROR: Unable to parse position from response " << reply << std::endl;
      //      std::exit(34);
    }
  
  for( Trajectory & traj : motions ) 
    {
      std::string reply = SendTrajectory( robot_socket, robot_iterator, traj );
      
#ifndef NDEBUG
      std::cout << "Response from sending trajectory " << reply << std::endl;
#endif
    }
  
#ifndef NDEBUG
  std::cout << "Saved position" << std::endl;
  saved.Print();
#endif
 
  int check       = -1;
  float angleTilt = 0.0;
  float anglePan  = 0.0;
  static float angleTiltStart = 0.0;
  static float anglePanStart  = 0.0;
  static bool cont;
  static int varCycleTime=0;
  static int varCycles=0;
  static double angleNt = 0.0;
  static double angleNl = 0.0;
  std::ostringstream  os;
  std::ostringstream sc;
  //std::string reply;
  std::string replyFromSending;
  bool secondRoundScan = true;
  for( ;; )
    {
      float currentTargetAngle = 0.0;
      
      std::ostringstream os;
      
      const std::string replyConstT = os.str();
      std::string replyT = SendCommand( robot_socket, robot_iterator, replyConstT );
	      
	      
      std::string replyVision = SendCommand( vision_socket, vision_iterator, "Results" );
#ifndef NDEBUG
      std::cout << "Vision reply Scann... " << replyVision << std::endl;
#endif 
      //usleep(40000);
      //check = ParseFromStreamVision( replyVision );
      /*
      for (int i = 0 ; i < 20 ; i++)
	{
	  std::cerr <<" HELLOOOOOOOOOOOOOO:"<< i<<std::endl;
	}

      for (int j = 20 ; j > 0 ; j--)
	{
	  std::cerr <<" BYE:"<<j <<std::endl;
	}

      */
      
      
       sc.str("");
       sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< -1.40 <<" Speed "<< 1000;
       reply = sc.str();
       replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
       
      for ( double i = -1.20; i < 1.20 ; i += 0.01)
	{
	  
	  sc.str("");
	  sc << " Arash " << " Actuator " << " NeckTransversal "<< " Position " << " Angle "<< i <<" Speed "<< 1000;
	  reply = sc.str();
	  replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	  usleep(10000);
	  std::cerr <<" HELLOOOOOOOOOOOOOO:"<< i<<std::endl;
	 
	}
	
    	
      for ( double j = -1.40; j < -0.5 ; j += 0.01)
	{
	  
	  sc.str("");
	  sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< j <<" Speed "<< 1000;
	  reply = sc.str();
	  replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	  usleep(10000);
	  std::cerr <<" j is:"<< j<<std::endl;
	}
      
       for ( double i = 1.20; i > -1.20 ; i -= 0.01)
	{
	  
	  sc.str("");
	  sc << " Arash " << " Actuator " << " NeckTransversal "<< " Position " << " Angle "<< i <<" Speed "<< 1000;
	  reply = sc.str();
	  replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	  usleep(10000);
	  std::cerr <<" HELLOOOOOOOOOOOOOO:"<< i<<std::endl;
	 
	}

       for ( double j = -0.5 ; j > -1.40 ; j -= 0.01)
	{
	  
	  sc.str("");
	  sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< j <<" Speed "<< 1000;
	  reply = sc.str();
	  replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	  usleep(10000);
	  std::cerr <<" j Helloooooooooooooooooooooooooooooooooooooooooo:"<< j<<std::endl;
	}
      

       if ( secondRoundScan )
	 {
	   sc.str("");
	   sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< -1.10 <<" Speed "<< 1000;
	   reply = sc.str();
	   replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	   
	   for ( double i = -1.20; i < 1.20 ; i += 0.01)
	     {
	       
	       sc.str("");
	       sc << " Arash " << " Actuator " << " NeckTransversal "<< " Position " << " Angle "<< i <<" Speed "<< 1000;
	       reply = sc.str();
	       replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	       usleep(10000);
	       std::cerr <<" HELLOOOOOOOOOOOOOO:"<< i<<std::endl;
	       
	     }
	   
	   
	   for ( double j = -1.20; j < 0.0 ; j += 0.01)
	     {
	       
	       sc.str("");
	       sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< j <<" Speed "<< 1000;
	       reply = sc.str();
	       replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	       usleep(10000);
	       std::cerr <<" j is:"<< j<<std::endl;
	     }
	   
	   for ( double i = 1.20; i > -1.20 ; i -= 0.01)
	     {
	       
	       sc.str("");
	       sc << " Arash " << " Actuator " << " NeckTransversal "<< " Position " << " Angle "<< i <<" Speed "<< 1000;
	       reply = sc.str();
	       replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	       usleep(10000);
	       std::cerr <<" HELLOOOOOOOOOOOOOO:"<< i<<std::endl;
	 
	     }

	   for ( double j = 0.0 ; j > -1.20 ; j -= 0.01)
	     {
	       
	       sc.str("");
	       sc << " Arash " << " Actuator " << " NeckLateral "<< " Position " << " Angle "<< j <<" Speed "<< 1000;
	       reply = sc.str();
	       replyFromSending = SendCommand( robot_socket, robot_iterator, reply);
	       usleep(10000);
	       std::cerr <<" j Helloooooooooooooooooooooooooooooooooooooooooo:"<< j<<std::endl;
	     }
	   
	   
	 } 
       
    }
}
