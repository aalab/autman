#include <string>
#include <sstream>
#include <istream>
#include <iostream>
#include "utility.hpp"
#include "trajectory.hpp"
#include "compilerdefinitions.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <json/value.h>


Trajectory::Trajectory(std::string _name, std::string _joint)
	: name(_name),
	  joint(_joint)
{
}

Trajectory::Trajectory(std::string _name, std::string _joint, boost::property_tree::ptree &pt)
	: name(_name),
	  joint(_joint)
{
	using boost::property_tree::ptree;

	BOOST_FOREACH(ptree::value_type & v, pt)
				{
					float angleInRadians = boost::lexical_cast<float>(v.first.data());
					float timeInCycles = boost::lexical_cast<float>(v.second.data());
					std::cout << "angle (Rad) " << angleInRadians << " time " << timeInCycles << std::endl;
					Add(angleInRadians, timeInCycles);
				}
}


void
Trajectory::Clear()
{
	angles.clear();
	times.clear();
}

void
Trajectory::Add(float angle, float time)
{
	angles.push_back(angle);
	times.push_back(time);
}

unsigned int
Trajectory::GetSize(void) const
{
#ifndef NDEBUG
	if (angles.size() != times.size())
	{
		std::cerr << "ERROR Trajectory " << name << " angles.size() " << angles.size() << " != " << " times.size() " <<
		times.size() << std::endl;
	}
	else
	{
		std::cout << "Trajectory::GetSize , Trajectory " << name << " angles.size() " << angles.size() << " == " <<
		" times.size() " << times.size() << std::endl;
	}
#endif
	return angles.size();
}

float
Trajectory::Interpolate(float percentage) const
{
	float t = percentage;
	float value = 0.0;
	unsigned int min = 0;
	unsigned int max = GetSize() - 1;
	unsigned int range = max - min;
	unsigned int mid = 0;
	//t = 0.14;

	if (t < 0) // here is the quick hack to test the problem in the first cycle
	{
		t = 0;
	}
	else if (t > 1.0)
	{
		t = 1.0;
	}
	while (range > 1)
	{
		mid = min + (max - min) / 2;
#ifndef NDEBUG

		// std::cout<<"variable t is:"<<t<<" The times[MIN] is:["<<times[min]<<"] min is:["<<min<<"] The times[MID] is:["<<times[mid]<<"]mid is:["<<mid<<"] The times[MAX] is:["<<times[max]<<"]max is:["<<max<<"]"<<"range is:["<<range<<"]"<<" The angles min:"<<angles[min]<<" The angles mid:"<<angles[mid]<<" The angles max:"<<angles[max]<<std::endl;
#endif
		if (times[mid] == t)
		{
			break;
		}
		else if (times[mid] < t)
		{
			min = mid;
		}
		else
		{
			max = mid;
		}
		range = max - min;
	}

	if (times[mid] != t)
	{
		if (t < times[min])
		{
			if (min > 0)
			{
				max = min;
				min = min - 1;
			}
			else
			{
				min = GetSize() - 1;
				max = 0;
			}
		}
		else if (t > times[max])
		{

			if (min < GetSize() - 1)
			{
				max = min + 1;
			}
			else
			{
				max = 0;
			}
		}
#ifndef NDEBUG
		std::cout << "variable t is:" << t << " The times[MIN] is:[" << times[min] << "] min is:[" << min <<
		"] The times[MID] is:[" << times[mid] << "]mid is:[" << mid << "] The times[MAX] is:[" << times[max] <<
		"]max is:[" << max << "]" << "range is:[" << range << "]" << " The angles min:" << angles[min] <<
		" The angles mid:" << angles[mid] << " The angles max:" << angles[max] << std::endl;
#endif

#ifndef NDEBUG
		std::cout << " Angles.size():" << GetSize() << std::endl;
#endif
		float rem = t - times[min];
		if (rem < 0.0)
		{
			rem = t;
		}
		while (rem > times[GetSize() - 1])
		{
			rem = rem - times[GetSize() - 1];
		}
		float dist = times[max] - times[min];
		if (dist <= 0.0)
		{
			dist = times[max];
		}

		while (dist > times[angles.size() - 1])
		{
			dist = dist - times[angles.size() - 1];
		}

		float rise = angles[max] - angles[min];
		value = angles[min] + (rem / dist) * rise;
		std::cout << "the distance is:" << dist << " and the rise is:" << rise << "The rem is:" << rem << std::endl;
#if 0
#ifndef NDEBUG
      std::cout<<"Min = "<<min<<" Mid ="<<mid<<" Max ="<<max<<std::endl;
      std::cout<<"Angels["<<min<<"] = "<< angles[min]<<"Angels["<<max<<"] = "<<angles[max]<<std::endl;
      std::cout<<"times[ "<<min<<"]= "<<times[min]<<" times["<<max<<"] ="<<times[max]<<std::endl;
      std::cout<< "t is ="<<t<<std::endl;
	    
#endif
#endif
	}
	else
	{
		value = angles[mid];

	}
#ifndef NDEBUG
	std::cout << "Interpolation:" << percentage << "," << value << std::endl;
#endif
	return value;
}

std::string
Trajectory::ToCommandString(void) const
{
	std::ostringstream os;
	os << "Arash Actuator " << joint << " Trajectory " << name << "=" << "[";
	for (size_t i = 0; i < GetSize(); i++)
	{
		if (i != 0)
		{
			os << ",";
		}
		os << "(" << angles[i] << "," << times[i] << " ) ";
	}
	os << "]";
	return os.str();
}

void
Trajectory::Print(std::ostream &os) const
{
	for (size_t i = 0; i < GetSize(); i++)
	{
		os << joint << " : ( " << angles[i] << "," << times[i] << " ) " << std::endl;
	}
}

int
Trajectory::Parse(std::string _joint, Json::Value &json)
{
	this->name = json["trajectory"].asString();
	this->joint = _joint;

	Clear();

	float angle, time;
	for (Json::Value &frame : json["frames"])
	{
		angle = frame["angle"].asFloat();
		time = frame["time"].asFloat();
		if (time < 0)
			return -1;

		VERBOSE("Angle: " << angle << ", Time: " << time);
		this->Add(angle, time);
	}
	return 0;
}

std::ostream &
operator<<(std::ostream &os, Trajectory const &t)
{
	os << t.joint << " " << t.name << " ";
	os << "[";
	for (size_t i = 0; i < t.GetSize(); i++)
	{
		if (i > 0)
		{
			os << ",";
		}
		os << "(" << t.angles[i] << "," << t.times[i] << ")";
	}
	os << "]";
	return os;
}
