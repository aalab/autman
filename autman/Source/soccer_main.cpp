
#include "config.h"

#include "position.hpp"
#include "robot.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
/* #include <boost/asio/signal_set.hpp> */
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>


#define DEFAULT_ROBOT_PORT  "1313"
#define DEFAULT_ROBOT_SERVER "localhost" 


using boost::asio::ip::udp;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );
  
  socket.send_to( boost::asio::buffer( command ), * iterator );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );
  
  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  std::cout << "Soccer::SendCommand Responses " << reply << std::endl;
#endif					   		      

  return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
  return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
  std::cout<<"from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif 
  return SendCommand( socket, iterator, trajectory.ToCommandString() );
}

static std::vector<Trajectory> motions;

void
LoadTrajectories( boost::property_tree::ptree & pt )
{
  using boost::property_tree::ptree;
  
  BOOST_FOREACH( ptree::value_type & v , pt.get_child("trajectories"))
    {
      std::string motionName = (std::string)v.first.data();
#ifndef NDEBUG
      std::cout << "Found Trajectory for: " << motionName << std::endl;
#endif
      BOOST_FOREACH( ptree::value_type & joint , v.second)
       { 
	 std::string jointName = (std::string)joint.first.data();
#ifndef NDEBUG
	 std::cout << "Found  joint: " << jointName << std::endl;
#endif
	 
	  Trajectory tmpT( motionName, jointName, joint.second );
          motions.push_back(tmpT);
       } 
    }
}

void 
LoadTrajectories(std::string const filename )
{
  using boost::property_tree::ptree;
  ptree pt;

  read_info( filename, pt );
  LoadTrajectories( pt );
}


unsigned long int tdiffUSec(struct timespec const start, struct timespec const end);

void addTimeNSec( struct timespec * time, long int disp );


float polynomialHold(float const amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage);


int 
main(int argc, char *argv[] )
{
  namespace po = boost::program_options;
  std::string robot_port;
  std::string robot_server;
  std::vector<std::string> trajectoryFiles;
  
  po::options_description commandLineOnlyOptions("Command Line Options");
  commandLineOnlyOptions.add_options()
    ( "version,v", "print version string" )
    ( "help,h"   , "print help message" )
    ( "robot_server" , po::value<std::string>( & robot_server )->default_value(DEFAULT_ROBOT_SERVER), "robot server address" )
    ( "robot_port" , po::value<std::string> ( & robot_port )->default_value(DEFAULT_ROBOT_PORT), "robot port number" )
    ( "trajectories,t", po::value<std::vector<std::string> >( & trajectoryFiles ), "file with trajectory information");
  
  std::cout << "Aluminium Testing.." << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
  
#ifndef NDEBUG
  std::cout << "Aluminium Debugging build" << std::endl;
#endif
  
  po::options_description commandLineOptions;
  commandLineOptions.add(commandLineOnlyOptions);
  
  po::positional_options_description p;
  p.add("trajectories",-1);
  
  try 
    {
      po::variables_map vm;
      po::store( po::command_line_parser( argc, argv).options(commandLineOptions).positional(p).run(), vm );
      po::notify( vm );
      
      if ( vm.count("help") )
	{
	  std::cout << commandLineOptions << "\n";
	  return 1;
	}
      
      if (vm.count("version") )
	{
	  std::cout << "Aluminium Test Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
	  return 1;
	}
    }
  catch( std::exception & e )
    {
      std::cerr << "boost::po exception " << e.what() << std::endl;
      return 1;
    }
    
  std::cout << "robot server " << robot_server << ", robot port " << robot_port  <<std::endl;
  std::cout << "Trajectory Files" << std::endl;
  
  BOOST_FOREACH( std::string s, trajectoryFiles )
    {
      if ( ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".info" ) && 
	   ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".json" ) &&
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".xml" ) && 
	   ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".ini" ) )
	{
	  s = s + ".info";
	}
      
      LoadTrajectories( std::string(CONFIG_DIR) + "/" + s );	 
    }
  
  std::cout << std::endl;

  boost::asio::io_service io_service;
  udp::socket robot_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
  
  udp::resolver robot_resolver( io_service );
  udp::resolver::query robot_query( udp::v4(), robot_server, robot_port );
  udp::resolver::iterator robot_iterator = robot_resolver.resolve( robot_query );
   
  Position saved;
  
  std::string reply;
  std::string com("ReadState");
  reply = SendCommand( robot_socket, robot_iterator, com );
  
#ifndef NDEBUG
  std::cout << "Reply for Read State " << reply << std::endl;
#endif  
  
  std::istringstream ireply( reply );
  
  if ( saved.parseFromStream( ireply ) < 0 )
    {
      std::cerr << "ERROR: Unable to parse position from response " << reply << std::endl;
      //      std::exit(34);
    }

#ifndef NDEBUG
  std::cout << "Saved position from the ReadStat commnad" << std::endl;
  saved.Print();
#endif
  
 
  BOOST_FOREACH( Trajectory  traj, motions ) 
    {
      std::string reply = SendTrajectory(robot_socket, robot_iterator, traj );
      
#ifndef NDEBUG
      std::cout << "Response from sending trajectory " << reply << std::endl;
#endif
    }
      
  com = "Trajectory=Walk CycleTimeUSec = 750000 Cycles=5\n";
  reply = SendCommand( robot_socket , robot_iterator, com );
  
#ifndef NDEBUG
  std::cout << "Trajectory returns with ret=" << reply << std::endl;
#endif      
  
  //sleep(1);
  /*
    reply = SendCommand( robot_socket , robot_iterator, "ReadySync");
    com = saved.ToCommandString();
    //com = "Actuator LeftHipTransversal Position Angle=0.0 Speed=10.0 & Actuator LeftHipFrontal Position Angle=0.0 Speed=10.0 & Actuator LeftHipLateral Position Angle=-0.462 Speed=10.0 & Actuator LeftKneeLateral Position Angle=0.612 Speed=10.0 & Actuator LeftAnkleFrontal Position Angle=0.0 Speed=10.0 & Actuator LeftAnkleLateral Position Angle=0.150 Speed=10.0 & Actuator RightHipTransversal Position Angle=0.0 Speed=10.0 & Actuator RightHipFrontal Position Angle=0.0 Speed=10.0 & Actuator RightHipLateral Position Angle=0.161 Speed=10.0 & Actuator RightKneeLateral Position Angle=-0.612 Speed=10.0 & Actuator RightAnkleFrontal Position Angle=0.0 Speed=10.0 & Actuator RightAnkleLateral Position Angle=-0.451 Speed=10.0 ";
  reply = SendCommand( robot_socket, robot_iterator, com);
  reply = SendCommand( robot_socket , robot_iterator, "SendSync"); 
#ifndef NDEBUG	        
  std::cout << " Reply from the SendSync :"<< reply << std::endl;
#endif
  */
    
  
  

  

#if 0
  sleep(20);
  com = "PowerOff";
#ifndef NDEBUG
  std::cout << "Testing mission accomplished.... " << std::endl;
  std::cout << "Sending command " << com << std::endl;
#endif
  
  reply = SendCommand (robot_socket, robot_iterator, com );
  std::cout <<"Reply is:"<< reply << std::endl;
  sleep(3);
#endif  // if 0
  return 0;
}
