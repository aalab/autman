/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 22, 2015
 **/

#include "interfacefactory.hpp"

#include <boost/algorithm/string/case_conv.hpp>
#include <dynamixel_interface.hpp>
#include <dynamixel2_interface.hpp>

#include "compilerdefinitions.h"

Interface *InterfaceFactory::create(std::string name, boost::property_tree::ptree &pt, Robot const *robot)
{
	Interface *iface = nullptr;

	std::string type = pt.get<std::string>("type");

	VERBOSE("Interface name " << name);
	boost::to_lower(type);

#if defined(ENABLE_RB110_SERVO_GPIO)
	if (  type == "rc_servo_gpio" )
	{
		iface = new RCServoGPIOInterface( name, pt, robot );
	}
	else
#endif
	if (type == "dynamixel")
	{
		VERBOSE("Creating " << type << " interface");
		iface = new DynamixelInterface(name, pt, robot);
	}
	else if (type == "dynamixel2")
	{
		VERBOSE("Creating " << type << " interface");
		iface = new Dynamixel2Interface(name, pt, robot);
	}
	else if (type == "v4l2")
	{
	}
	else
	{
		std::cerr << "Unknown interface type " << type << std::endl;
		std::exit(2);
	}
	return iface;
}
