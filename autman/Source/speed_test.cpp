
#include "config.h"

#include "position.hpp"
#include "robot.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <json/value.h>
#include "json.hpp"
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>
#include <chrono>
#include <vector>
#include "exceptions.hpp"

#define DEFAULT_SPEED_PORT "2134"
#define DEFAULT_SPEED_SERVER "127.0.0.1" 
#define DEFAULT_ROBOT_PORT  "1313"
#define DEFAULT_ROBOT_SERVER "127.0.0.1" 


using boost::asio::ip::udp;
using ns = std::chrono::nanoseconds;
using get_time = std::chrono::steady_clock;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
  char replyBuffer[4096];
  size_t replyBufferMaxLength = sizeof( replyBuffer );

  socket.send_to( boost::asio::buffer( command ), * iterator );
  
  udp::endpoint sender_endpoint;
  size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

  std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
  //std::cout << "Speed Responses " << reply << std::endl;
#endif					   		      

  return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
  return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
  //std::cout<<"from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif 
  return SendCommand( socket, iterator, trajectory.ToCommandString() );
}


void measure(std::string command, std::string alternate, int numTimes, std::string label, 
	udp::socket & robot_socket, udp::resolver::iterator & robot_iterator, int delay)
{
	Json::Value json;
	int total = 0;
	std::string reply = "";
	auto start = get_time::now();
	for (int i = 0; i < numTimes; i++){
		if (i % 2 == 0 && alternate != "")
			reply = SendCommand( robot_socket, robot_iterator, alternate );
		else
			reply = SendCommand( robot_socket, robot_iterator, command );
		try{
			if (json::unserialize(reply, json)){
				total++;
			}
		}catch(std::exception &){
			
		}
		if (delay > 0){
			usleep(delay);
		}
	}
	auto end = get_time::now();
	auto diff = end - start;
	std::cout << "Called " << label << " for " << numTimes << " iterations." << std::endl;
	std::cout << "Elapsed time is : " << std::chrono::duration_cast<ns>(diff).count() << " ns " << std::endl;
	std::cout << "There were " << total << " successful returns " << std::endl;
	std::cout << "There was a total delay of " << delay * numTimes << " ms " << std::endl;
}

int main(int argc, char **argv){
	/*
	 * This is used to test some common commands that are sent to and from the robot server
	 * 
	*/
	
	std::string robot_port = DEFAULT_ROBOT_PORT;
	std::string robot_server = DEFAULT_ROBOT_SERVER;
	
	boost::asio::io_service io_service;
	udp::socket robot_socket(io_service, udp::endpoint( udp::v4(), 0 ));
	udp::resolver robot_resolver(io_service);
	udp::resolver::query robot_query( udp::v4(), robot_server, robot_port );
	udp::resolver::iterator robot_iterator = robot_resolver.resolve( robot_query );
	
	
	
	std::string command, alternate;
	//Just because I don't wanna type the same thing ower and ower again
#define callM(times, alt, label, delayMilli) measure(command, alt, times, label, robot_socket, robot_iterator, delayMilli)
	
	//***************************************************************/
	//Long commands
	
	std::cout << "Now testing long commands for the robot" << std::endl;
	//Read State of the Robot
	command = "{ \"name\" : \"Arash\" , \"command\" : \"ReadState\" }";
	callM(10000, "", "ReadState", 0);
	
	//Move command - We only need one call
	command = "{\"name\":\"Arash\",\"command\":\"Move\",\"params\":{\"stop\": true,\"dx\": 0.0,\
        \"dy\": 0.0,\"dtheta\": 0.0,\"cycleTimeUSec\": 650000,\"cycles\": 10}}";
	callM(1, "", "Move on 10 cycles", 6500000);
	
	//Read the gains for the entire robot
	command = "{\"name\":\"Arash\",\"command\":\"ReadFullState\"}";
	callM(10000, "", "ReadFullState", 0);
	
	
	
	
	
	
	//******************************************************************/
	//Short commands 
	
	//Read State for one servo
	command= "{\"name\":\"Arash\",\"command\":\"ReadState\",\"params\":\
	{\"actuators\": [\"RightHipTransversal\"]}}";
	callM(10000, "", "Read State for one servo", 0);
	
        //Move a single servo back and forth
	command = "{\"name\": \"Arash\",\"command\": \"Actuator\",\
	\"params\":{\"command\":\"Position\",\"name\":\"NeckLateral\",\"angle\":0.0,\
	\"speed\":500,\"gainP\": 32,\"gainI\": 0,\"gainD\": 0}}";
        
	alternate = "{\"name\": \"Arash\",\"command\": \"Actuator\",\
	\"params\":{\"command\":\"Position\",\"name\":\"NeckLateral\",\"angle\":1.57,\
	\"speed\":500,\"gainP\": 32,\"gainI\": 0,\"gainD\": 0}}";
        callM(20, alternate, "Move servo to position", 500000);

        //Turn a servo on and off multiple times
        command = "{\"name\": \"Arash\",\"command\": \"Actuator\",\"params\":{\
        \"name\":\"RightShoulderLateral\",\"mode\":\"Off\"}}";
        alternate = "{\"name\": \"Arash\",\"command\": \"Actuator\",\"params\":{\
        \"name\":\"RightShoulderLateral\",\"mode\":\"On\"}}";
        callM(10000, alternate, "Turn on and off one servo", 0);
        
        //Read the FullState from one servo
        command = "{\"name\":\"Arash\",\"command\":\"ReadFullState\",\"params\":{\
        \"actuators\": [\"LeftHipTransversal\"]}}";
	callM(10000, "", "ReadFullState for one servo", 0);
        
	return 0;

}






































