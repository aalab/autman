#include "motion.hpp"
#include "robot.hpp"
#include "kinematics_humanoid.hpp"
#include "motion_sequence.hpp"

#include <boost/foreach.hpp>

#include <string>
#include <vector>

//size of frames is zero if any gait calculations fail
MotionSequence::MotionSequence( KinematicsHumanoid* humanoid, std::vector<MotionSequenceParams>& params )
  : frames()
{
  for(MotionSequenceParams& p:params) {
    HumanoidTrajectoryParams gaitParams(humanoid->gaitParams);
    
    gaitParams.stepX = p.stepX;
    gaitParams.stepY = p.stepY;
    gaitParams.stepAngle = p.stepAngle;
    
    std::unique_ptr<Motion> currMotion(humanoid->calcWalkingGait(gaitParams));
    if(currMotion) {
      frames.push_back(new MotionSequenceFrame(std::move(currMotion), p.cycleTimeUSec, p.numCycles));
    } else {
      frames.clear();
      break;
    }
  }
}

MotionSequenceParams::MotionSequenceParams(float stepX, float stepY, float stepAngle, unsigned long long int cycleTimeUSec, unsigned int numCycles) 
  : stepX(stepX), stepY(stepY), stepAngle(stepAngle), cycleTimeUSec(cycleTimeUSec), numCycles(numCycles)
{
}

MotionSequenceFrame::MotionSequenceFrame(std::unique_ptr<Motion> motion, unsigned long long int cycleTimeUSec, unsigned int numCycles) 
  : motion(std::move(motion)), cycleTimeUSec(cycleTimeUSec), numCycles(numCycles)
{
}
