#include "rb110_serial_port.hpp"

#include "interface.hpp"
#include "roboard.h"
#include "robotis_servo.hpp"
#include "robotis_servo_mx_series.hpp"
#include "robotis_servo_rx_series.hpp"
#include <boost/property_tree/ptree.hpp>
#include <cstdio>
#ifndef NDEBUG
#include <boost/io/ios_state.hpp>
#include <iomanip>
#endif


RB110SerialPort::RB110SerialPort( std::string name , boost::property_tree::ptree & pt)
:Channel( name , pt )
{
}

int
RB110SerialPort::Open( )
{
  if ( ! opened )
    {
      if ( interface == "/dev/ttyS2" ) 
	{
	  comPort = 1;
	}
      else if ( interface == "/dev/ttyS3" )
	{
	  comPort = 2;
	}
    }
  
  if ( comPort != UNDEFINED_COMPORT )
    {
      if( !com_Init(comPort, COM_HDUPLEX) )
	{
	  std::cerr << "ERROR: " << roboio_GetErrMsg() << std::endl;
	  return -1;
	}
	  
      // Set data format as 8 bits, 1 stop bit, no parity
      com_SetFormat(comPort, COM_BYTESIZE8,COM_STOPBIT1,COM_NOPARITY);
	  
      int baud = UNDEFINED_BAUDRATE;
      if ( baudrate == "500000" ) 
	{
	  baud = COMBAUD_499200BPS;
	}
      else if ( baudrate == "115200" )
	{
	  baud = COMBAUD_115200BPS;
	}
      else
	{
	  std::cerr << "Unknown baudrate " << baudrate << std::endl;
	  return -2;
	}
      if ( baud != UNDEFINED_BAUDRATE )
	{
	  com_SetBaud(comPort, baud );
	}
    
      opened = true;
    }
  return 0;
}

int
RB110SerialPort::Close()
{
  int res = 0;

  res = Channel::Close( );
  if ( opened )
    {
    }
  opened = false;

  return res;

}

int 
RB110SerialPort::CommandTxAndRx( uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[], unsigned int const replyLength )
{
  int ret = -1;
  if ( comPort != UNDEFINED_COMPORT )
    {
      for(unsigned int i = 0; i < replyLength; i++ )
	{
	  reply[i] = 0;
	}
      bool err = com_ServoTRX( comPort, const_cast<uint8_t *>( buffer ), bufferLength, reply, replyLength );
      if ( err )
	{
	  ret = replyLength;
	}
      else
	{
	  ret = -2;
	}
    }
  else
    {
      ret = -1;
    }
  return ret;

}
