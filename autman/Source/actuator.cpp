/*
 * Actuator Class
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#include "actuator.hpp"
#include "interface.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp> 

#include "robot.hpp"

#if defined(ENABLE__GENERIC_RC_SERVO_NO_FEEDBACK)
#   include "generic_rc_servo_no_feedback.hpp"
#endif

#include "trajectory.hpp"
#include "compilerdefinitions.h"
#include <boost/property_tree/xml_parser.hpp>


Actuator::Actuator(std::string name, Interface *interface, boost::property_tree::ptree &pt)
	: type(INVALID),
	  interface(interface),
	  name(name),
	  opened(false)
{
	VERBOSE("Actuator name " << name << " of type " << type);
}

Actuator *
Actuator::factory(std::string name, boost::property_tree::ptree &pt, Robot const *robot)
{
	Actuator *act = nullptr;

	std::string type = pt.get<std::string>("type", "auto");
	std::string interface_name = pt.get<std::string>("interface");
	Interface *iface = robot->FindInterfaceByName(interface_name);

	VERBOSE("id: " << pt.get<std::string>("id", "-----------------------------------"));
	VERBOSE("type: " << pt.get<std::string>("type", "-----------------------------------"));
	VERBOSE("interface: " << pt.get<std::string>("interface", "-----------------------------------"));
	VERBOSE("home: " << pt.get<std::string>("home", "-----------------------------------"));

	VERBOSE("Actuator name " << name);
	VERBOSE("Interface name " << interface_name);

	boost::to_lower(type);

	if (type == "auto")
	{
		if (iface != nullptr)
		{
			act = iface->factoryActuatorAuto(name, pt);
			if (act == nullptr)
			{
				ERROR("unable to determine auto type for servo " << name << " on interface type " << iface->GetType());
			}
		}
	}
#if defined(ENABLE__GENERIC_RC_SERVO_NO_FEEDBACK)
	else if ( type == "generic_rc_servo")
	{
		act = new GenericRCServoNoFeedback( name, iface, pt );
	}
#endif
	else if (type.substr(0, 13) == "robotis_servo")
	{
		return RobotisServo::factory(name, type, dynamic_cast<DynamixelInterface *>(iface), pt);
	}
	else
	{
		ERROR("Unknown actuator type " << type);
		std::exit(2);
	}
	return act;
}

int
Actuator::Open(void)
{
	VERBOSE("Actuator::Open()");

	if (!opened)
	{
		opened = true;

	}
	return 0;
}

int
Actuator::ProcessCommand(Json::Value &json)
{
	enum MotorState state;
	int ret = ParseActuatorMotorStateMessage(json, &state);
	if (ret == 0)
	{
		ret = SetMotorState(state);
	}
	return ret;
}

int
Actuator::ParseActuatorMotorStateMessage(Json::Value &json, enum MotorState *state)
{
	std::string keyword;
	// enum MotorState tmps;
	int ret = 0;

	Json::Value &value = json["mode"];
	if (!value.isNull())
	{
		std::string mode = value.asString();
		if (mode == "On")
			*state = MOTOR_ON;
		else if (mode == "Off")
			*state = MOTOR_OFF;
		else
		{
			ERROR(mode << " is not a valid state for actuator " << this->name << ".");
			// TODO: Throw an exception
			ret = -1;
		}
	}
	return ret;
}


int
Actuator::ReadState(Json::Value &jsonResult)
{
	ERROR("Actuator::ReadState..ERROR");
	return -1;
}

int
Actuator::ReadFullState(Json::Value &jsonResult)
{
	ERROR("Actuator::ReadState..ERROR");
	return -1;
}

int
Actuator::SetMotorState(enum MotorState state)
{
	std::cout << "ERROR..Actuator::SetMotorState..ERROR" << std::endl;
	return -1;
}

enum Actuator::MotorState
Actuator::GetMotorState(void) const
{
	std::cout << "ERROR..Actuator::GetMotorState..ERROR" << std::endl;
	return MOTOR_INVALID;
}

Trajectory const *
Actuator::FindTrajectoryByName(std::string const &name) const
{
	Trajectory const *tmp = nullptr;

	for (std::vector<Trajectory>::const_iterator i = trajectories.begin();
		 i != trajectories.end();
		 ++i)
	{
		if (name == i->GetName())
		{
			tmp = &(*i);
			break;
		}

	}
#ifndef NDEBUG
	if (tmp != nullptr)
	{
		std::cout << "FindTrajectoryByName returns  " << *tmp << " for joint " << GetName() << std::endl;
	}
	else
	{
		std::cout << "FindTrajectoryByName returns NULL for joint " << GetName() << std::endl;
	}
#endif

	return tmp;

}

int Actuator::AddTrajectory(Trajectory const &traj)
{
	int result = 0;

	std::vector<Trajectory>::iterator
	it = std::find_if(trajectories.begin(), trajectories.end(), boost::bind(&Trajectory::GetName, _1) == traj.GetName());

	if (it != trajectories.end())
	{
		trajectories.erase(it);
		result = -1;
	}

	trajectories.push_back(traj);
	return result;
}

int
Actuator::SendPositionAndSpeedMessage(float angle, float speed)
{
	std::cout << "ERROR..Actuator::SendPositionAndSpeedMessage..ERROR" << std::endl;
	return -1;
}

int Actuator::ReadGains(Json::Value &jsonResult)
{
	ERROR("Not implemented.");
	return -1;
}
