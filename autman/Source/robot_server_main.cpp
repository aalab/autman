#include "config.h"

#include "robot.hpp"
#include "utility.hpp"
#include "compilerdefinitions.h"

#include <cmath>
#include <iostream>
#include <vector>
#include <boost/program_options.hpp>

int
main(int argc, char **argv)
{
	namespace po = boost::program_options;
	std::string configFile = DEFAULT_CONFIG_FILE;

	std::cout << "Robot Server" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif
	/*po::options_description generic("Generic Options");
	generic.add_options()
		("version,v","print version string")
		("help","produce help message");

	po::options_description config;
	config.add(generic);
	config.add_options()
		("config_file,c", po::value<std::string>()->default_value("")->implicit_value("./"), "Set config file name");
	
	try
	{
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, config), vm);
		po::notify(vm);
		
		if (vm.count("config_file"))
		{
				
			std::cout << vm.count("config_file") << std::endl;
			std::cout << vm["config_file"].as<std::string>() << std::endl;
			std::cout<<"*******I just got it********";
		}
		if (vm.count("help"))
		{
			std::cout << "!!!!!!HEEEEEELP!!!!!!!!"<< generic << "\n";
			return 0;
		}

		if (vm.count("instructions"))
		{
			std::vector<std::string> instructions;
			instructions.push_back("1- Make the robot walk");
			instructions.push_back("2- Send position to individual Actuator");
			instructions.push_back("3- Turn-On all the Actuators");
			instructions.push_back("4- Turn-Off all the Actuators");
			instructions.push_back("5- Reading the States of all the Actuators");
			instructions.push_back("6- Send Trajectories to the servos");
			std::cout << "List of instructions to chose" << std::endl;
			for (auto i:instructions)
			{
				std::cout << std::endl << i << std::endl;
			}
			auto input = -1;
			std::cout << std::endl << "Please enter a number between 1 to " << instructions.size() << ": ";
			std::cin >> input;
			switch (input)
			{
				case 1:
					std::cout << std::endl << "Make robot walk just change the params:" <<
					"\n|\n|\n-->\t{ \"name\":\"Arash\", \"command\":\"Move\", \"params\": { \"stop\": true, \"dx\": 0.0, \"dy\": 0.0, \"dtheta\": 0.0, \"cycleTimeUSec\": 3000000, \"cycles\": 1 } }" <<
					std::endl;
					return 0;
				case 2:
					std::cout << std::endl <<
					"Send position to individual Actuator command:\n|\n|\n-->\t {\"name\": \"Arash\", \"command\": \"Actuator\", \"params\":{\"command\":\"Position\",\"name\":\"NeckLateral\",\"angle\":0.0,\"speed\":500,\"gainP\": 32,\"gainI\": 0,\"gainD\": 0 }  }" <<
					std::endl;
					return 0;
				case 3:
					std::cout << std::endl <<
					"Turn all the Actuators On command: \n|\n|\n-->\t {\"name\":\"Arash\",\"command\": \"Actuator\",\"params\":{ \"name\":\"NeckLateral\",\"mode\":\"On\" }  }" <<
					std::endl;
					return 0;
				case 4:
					std::cout << std::endl <<
					"Turn all the Actuators Off command: \n|\n|\n-->\t {\"name\":\"Arash\",\"command\": \"Actuator\",\"params\":{ \"name\":\"NeckLateral\",\"mode\":\"Off\" }  }" <<
					std::endl;
					return 0;
				case 5:
					std::cout << std::endl <<
					"Read the Status of the all Actuators: \n|\n|\n-->\t { \"name\":\"Arash\",\"command\":\"ReadState\",\"params\": { \"actuators\": [\"NeckLateral\",\"NeckTransversal\"] }  }" <<
					std::endl; // Select specific servos to be read. If null or empty read all.
					return 0;
				case 6:
					std::cout << std::endl <<
					"Send Trajectory to the motors: \n|\n|\n-->\t {\"name\": \"Arash\",\"command\": \"Actuator\",\"params\":{\"command\":\"Trajectory\",\"name\":\"NeckLateral\",\"trajectory\": \"trajectory1\",\"frames\": [{ \"angle\": 1.0, \"time\": 0.0 },{\"angle\": -1.0, \"time\": 1.0 }, { \"angle\": 1.0, \"time\": 2.0 },{ \"angle\": 0.0,\"time\": 2.5 } ] }  } " <<
					std::endl;
					return 0;
				default:
					std::cout << "wrong entry!" << std::endl;
					return -44;
			}

		}
		if (vm.count("version"))
		{
			std::cout << "Robot Server Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}
		std::cout << "Config file is " << configFile << std::endl;
	}
	catch (std::exception &e)
	{
		ERROR("boost::po exception " << e.what());
		return 1;
	}*/

	std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
	VERBOSE("Loading config file " << configFile << " config path " << configPath);

	Robot *robby = new Robot(configPath);

	//  float speed = 0.0;
#if 0
	for( float angle = Utility::DegreeToRadians(-60.0); angle < Utility::DegreeToRadians(0); angle += Utility::DegreeToRadians(2.0))
	  {
		std::string com;
		std::ostringstream os(com);
		os << "Actuator NeckTransversalJoint Position Angle=" << angle << " Speed=" << speed << "&";
		os << "Actuator NeckLateralJoint Position Angle=" << 0.0 << " Speed=" << speed << "&";
#ifndef NDEBUG
		std::cout << "Processing command from robot_server_main.cpp " << os.str() << std::endl;
#endif
		std::istringstream ins1( os.str() );
		robby->ProcessCommandStream( ins1 );
	  }
#endif

	robby->StartServer(true);
	robby->Close();

	return 0;
}
