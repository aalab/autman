/*
 * MX-64 Actuator Definition 
 *
 */

#include "robotis_servo_mx_64.hpp"

RobotisServoMX64::RobotisServoMX64( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
: RobotisServoMXSeries( name, interface, pt ),
  defaultSpeed(0)
{
  type = ROBOTIS_SERVO_MX_64;  

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo MX 64 Actuator " << std::endl;
#endif

  //maxSpeed = 11.938052; // this needs to be change later.it's not correct!!!!
}


