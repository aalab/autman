#include <iostream>
#include <cmath>
#include "robot.hpp"
#include "kinematics_object.hpp"
#include "kinematics_leg.hpp"

KinematicsLeg::KinematicsLeg(Robot const * robot, bool isLeft, bool useFlatFoot): KinematicsObject(robot) {
  this->isLeft = isLeft;
  this->useFlatFoot = useFlatFoot;
    
  if(isLeft) {
    //hip transversal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-2.22);
    upperServoBounds.push_back(1.20);
        
    //hip frontal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-0.72);
    upperServoBounds.push_back(0.67);
        
    //hip lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-1.82);
    upperServoBounds.push_back(1.76);
        
    //knee lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-2.52);
    upperServoBounds.push_back(2.19);
        
    //ankle lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-1.86);
    upperServoBounds.push_back(1.51);
        
    //ankle frontal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-0.68);
    upperServoBounds.push_back(0.66);
  } else {
    //hip transversal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-1.25);
    upperServoBounds.push_back(1.43);
        
    //hip frontal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-0.74);
    upperServoBounds.push_back(0.74);
        
    //hip lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-1.74);
    upperServoBounds.push_back(1.85);
        
    //knee lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-2.41);
    upperServoBounds.push_back(2.51);
        
    //ankle lateral
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-1.51);
    upperServoBounds.push_back(1.86);
        
    //ankle frontal
    servoResolutions.push_back(4096);
    lowerServoBounds.push_back(-0.67);
    upperServoBounds.push_back(0.67);
  }
}
        
KinematicsLeg::~KinematicsLeg() {}

//Calculates the angles in radians required of the servos for the head to reach a given position
//returnAngles - vector where the calculated angles will be stored
//params - vector that must contain exactly 4 values: the desired x value, followed by the desired y value, followed by the desired z value, followed by the desired hip transversal angle
//buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
//returns 0 on success, -1 on error
int KinematicsLeg::inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle) const {
  int successful = 0;
    
  if(params.size() == 4) {
    float goalX = params[0];
    float goalY = params[1];
    float goalZ = params[2];
    float desiredHipTrans = -params[3];
    std::vector<float> measurements;
    robot->getLegMeasurements(measurements);
    float torsoHipDistanceY = measurements[0];
    float torsoHipDistanceZ = measurements[1];
    float hipTransversalToHipFrontalZ = measurements[2];
    float hipLateralToKneeLateralZ = measurements[3];
    float kneeLateralToAnkleLateralZ = measurements[4];
    float ankleLateralToFootFrontalZ = measurements[5];
    //std::cout << torsoHipDistanceY << " " << torsoHipDistanceZ << " " << hipTransversalToHipFrontalZ << " " << kneeLateralToAnkleLateralZ << " " << ankleLateralToFootFrontalZ << std::endl;
    int signFactor;
    float deltaX, deltaY, deltaZ, effectiveLength, tempGoalX;
        
    if(isLeft) {
      signFactor = +1;
    } else {
      signFactor = -1;
    }
        
    if(useFlatFoot) {
      goalZ -= ankleLateralToFootFrontalZ;
    }
        
    //rotate the goal point into the frame of the desired hip transversal angle
    tempGoalX = goalX;
    goalX = goalX*std::cos(desiredHipTrans) - (goalY-signFactor*torsoHipDistanceY)*std::sin(desiredHipTrans);
    goalY = tempGoalX*std::sin(desiredHipTrans) + (goalY-signFactor*torsoHipDistanceY)*std::cos(desiredHipTrans) + signFactor*torsoHipDistanceY;
        
    deltaX = goalX;
    deltaY = goalY - signFactor*(torsoHipDistanceY);
    deltaZ = goalZ - (torsoHipDistanceZ + hipTransversalToHipFrontalZ);
    effectiveLength = std::sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);
        
    if(effectiveLength > std::abs(hipLateralToKneeLateralZ + kneeLateralToAnkleLateralZ)) {
      if(isLeft) {
	std::cerr << "ERROR: Left leg position is beyond reach" << std::endl;	
	std::cerr << "Exiting the program .... check the kinematics_leg.cpp";
      } else {
	std::cerr << "ERROR: Right leg position is beyond reach" << std::endl;
	std::cerr << "Exiting the program .... check the kinematics_leg.cpp";
      }
      
      successful = -1;
    } else {
      float hipTransversalOut, hipFrontalOut, hipLateralOut, kneeLateralOut, ankleLateralOut, ankleFrontalOut;
        
      float kneeLateral = std::acos((effectiveLength*effectiveLength - hipLateralToKneeLateralZ*hipLateralToKneeLateralZ - kneeLateralToAnkleLateralZ*kneeLateralToAnkleLateralZ) / (-2.0*std::abs(hipLateralToKneeLateralZ)*std::abs(kneeLateralToAnkleLateralZ)));
            
      float hipFrontal = std::asin(deltaY / std::sqrt(deltaY*deltaY + deltaZ*deltaZ));
            
      float hipLateral = std::asin(deltaX/effectiveLength) + std::asin(std::abs(kneeLateralToAnkleLateralZ)*std::sin(kneeLateral)/effectiveLength);
            
      float ankleLateral = M_PI - kneeLateral - hipLateral;
            
      float ankleFrontal = hipFrontal;
            
      hipTransversalOut = desiredHipTrans;
      if(isLeft) {
	kneeLateralOut = M_PI - kneeLateral;
	hipFrontalOut = -hipFrontal;
	hipLateralOut = -hipLateral;
	ankleLateralOut = ankleLateral;
	ankleFrontalOut = -ankleFrontal;
      } else {
	kneeLateralOut = -(M_PI - kneeLateral);
	hipFrontalOut = -hipFrontal;
	hipLateralOut = hipLateral;
	ankleLateralOut = -ankleLateral;
	ankleFrontalOut = -ankleFrontal;
      }
            
      returnAngles.clear();
      returnAngles.push_back(hipTransversalOut);
      returnAngles.push_back(hipFrontalOut);
      returnAngles.push_back(hipLateralOut);
      returnAngles.push_back(kneeLateralOut);
      returnAngles.push_back(ankleLateralOut);
      returnAngles.push_back(ankleFrontalOut);
            
      successful = validAngles(returnAngles, buffer_angle);
    }
  } else {
    if(isLeft) {
      std::cerr << "ERROR: Incorrect number of parameters given to left leg inverse kinematics" << std::endl;
    } else {
      std::cerr << "ERROR: Incorrect number of parameters given to right leg inverse kinematics" << std::endl;
    }
    successful = -1;
  }
    
  return successful;
}
