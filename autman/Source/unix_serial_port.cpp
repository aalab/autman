//-----------------------------------------
// PROJECT: MotionController
// AUTHOR : POSIX - Jacky Baltes <jacky@cs.umanitoba.ca>
//          Windows - Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------

#include <string.h>

#ifdef _WIN32
#include <windows.h>
#else

#include "unix_serial_port.hpp"
#include "channel.hpp"
#include "compilerdefinitions.h"
#include <iomanip>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#endif


//------------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------------
static BAUDRATE_TYPE BAUDRATES_SPEED[] =
	{
#ifdef _WIN32
    CBR_110,
    CBR_300,
    CBR_600,
    CBR_1200,
    CBR_2400,
    CBR_4800,
    CBR_9600,
    CBR_19200,
    CBR_38400,
    CBR_57600,
    CBR_115200,
    CBR_256000,
#else
		B110,
		B300,
		B600,
		B1200,
		B2400,
		B4800,
		B9600,
		B19200,
		B38400,
		B57600,
		B115200,
		B500000,
		B1000000,
#endif
	};
static const int BAUDRATES__LENGTH = (sizeof(BAUDRATES_SPEED) / sizeof(BAUDRATES_SPEED[0]));

static const char *BAUDRATES_STRING[] =
	{
		"B110",
		"B300",
		"B600",
		"B1200",
		"B2400",
		"B4800",
		"B9600",
		"B19200",
		"B38400",
		"B57600",
		"B115200",
#ifdef _WIN32
    "B256000",
#else
		"B500000",
		"B1000000",
#endif
	};


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
UnixSerialPort::UnixSerialPort(std::string name, boost::property_tree::ptree &pt)
	: Channel(name, pt)
{
	device = pt.get<std::string>("device");
	this->lastPrefix = pt.get<std::string>("device_prefix", "/dev/ttyUSB");
	baudrate = pt.get<std::string>("baudrate");

	VERBOSE("Config for " << device);
	VERBOSE("+ Device: " << device);
	VERBOSE("+ Baudrate: " << baudrate);
}

/*
#ifdef _WIN32
    _hCommPort = 0;
#else
    _fd = 0;
    
#endif
*/

UnixSerialPort::~UnixSerialPort(void)
{
	(void) Close();
}


int
UnixSerialPort::CloseWithoutLock(void)
{
	if (isValid())
	{
		flush();
#ifdef _WIN32
      SetCommState( _hCommPort, &_olddcb );
      CloseHandle( _hCommPort );
      _hCommPort = 0;
#else
		tcsetattr(_fd, TCSANOW, &_oldtio);
		::close(_fd);
		_fd = 0;
		this->opened = false;
#endif
	}
	return 1;
}


void UnixSerialPort::flush(void)
{
#ifdef _WIN32
    FlushFileBuffers( _hCommPort );
#else
	{
		tcflush(_fd, TCIOFLUSH);
		//mutex.unlock();
	}
#endif
}


void UnixSerialPort::flushRx(void)
{
#ifdef _WIN32
    FlushFileBuffers( _hCommPort );
#else
	{
		tcflush(_fd, TCIFLUSH);
		//mutex.unlock();
	}
#endif
}


void UnixSerialPort::flushTx(void)
{
#ifdef _WIN32
    FlushFileBuffers( _hCommPort );
#else
	{
		tcflush(_fd, TCOFLUSH);
		//mutex.unlock();
	}
#endif
}


bool UnixSerialPort::isValid(void) const
{
#ifdef _WIN32
    return _hCommPort != 0;
#else
	return _fd > 0;
#endif
}

int
UnixSerialPort::OpenWithoutLock(void)
{
	int ret = -1;
	if (this->OpenWithoutLock(device.c_str(), spdBaudrate(baudrate.c_str())))
	{
		ret = 0;
		this->opened = true;
	}
	return ret;
}

bool
UnixSerialPort::OpenWithoutLock(const char *devname, BAUDRATE_TYPE baudrateSpeed)
{
	bool success = false;
	int i = 0;

	this->CloseWithoutLock(); // Close opened serial port first

	while (i < SERIAL_MAX_DEVNAME && *devname != '\0')
	{
		_devname[i++] = *devname;
		++devname;
	}
	_devname[i] = '\0';
	_baudrateSpeed = baudrateSpeed;

#ifdef _WIN32
    // Taken from UnixSerialPort Communcation in Windows
    // http://www.codeproject.com/KB/system/serial_com.aspx
    DCB newdcb;
    _hCommPort = CreateFileA( _devname, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );
    if( _hCommPort == 0 || _hCommPort == INVALID_HANDLE_VALUE )
    {   // failed
    }
    else
    {
        memset( &_olddcb, 0, sizeof( DCB ) );
        _olddcb.DCBlength = sizeof( DCB );
        GetCommState( _hCommPort, &_olddcb );
        memcpy( &newdcb, &_olddcb, sizeof( DCB ) );

        newdcb.BaudRate = _baudrateSpeed;
        newdcb.ByteSize = 8;
        newdcb.Parity = NOPARITY;
        newdcb.StopBits = ONE5STOPBITS;
        SetCommState( _hCommPort, &newdcb );
        success = true;
    }
#else
	// Taken from the UnixSerialPort Programming HOWTO
	// Open modem device for reading and writing and not as controlling tty
	// because we don't want to get killed if linenoise sends CTRL-C.
	VERBOSE("Trying to open " << _devname);
	_fd = ::open(_devname, O_RDWR | O_NOCTTY);
	if (_fd < 0)
	{
		ERROR("Opening of serial port failed " << _devname << ". Error: " << _fd);
	}
	else
	{
		VERBOSE("Opened the first part.");
		tcgetattr(_fd, &_oldtio); /* save current serial port settings */
		memset(&this->currentTIO, 0, sizeof(this->currentTIO)); /* clear struct for new port settings */

		// BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
		// CRTSCTS : output hardware flow control (only used if the cable has all necessary lines. See sect. 7 of UnixSerialPort-HOWTO)
		// CS8     : 8n1 (8bit,no parity,1 stopbit)
		// CLOCAL  : local connection, no modem contol
		// CREAD   : enable receiving characters
		cfsetispeed(&this->currentTIO, _baudrateSpeed);
		cfsetospeed(&this->currentTIO, _baudrateSpeed);

		this->currentTIO.c_cflag = _baudrateSpeed | CS8 | CLOCAL | CREAD;

		// IGNPAR  : ignore bytes with parity errors
		// ICRNL   : map CR to NL (otherwise a CR input on the other computer will not terminate input)
		//           otherwise make device raw (no other input processing)
		this->currentTIO.c_iflag = IGNPAR | IXANY; // | IGNBRK; //| ICRNL;

		// Raw output
		this->currentTIO.c_oflag = 0;
		this->currentTIO.c_lflag = 0;

		// now clean the modem line and activate the settings for the port
		tcsetattr(_fd, TCSANOW, &this->currentTIO);
		flushTx();
		flushRx();

		success = true;
	}
#endif
	return success;
}


bool UnixSerialPort::readChar(char *pChar)
{
	bool success = false;

	if (isValid())
	{
#ifdef _WIN32
        DWORD nBytes = 0;
        success = (ReadFile( _hCommPort, pChar, 1, &nBytes, 0 ) && nBytes == 1);
    #else
		{
			std::lock_guard<std::mutex> lock(mutex);
			success = (read(_fd, pChar, 1) == 1);
		}
#endif
	}
	return success;
}

int
UnixSerialPort::readDataWithLock(uint8_t *data, unsigned int length, unsigned int timeout)
{
	std::lock_guard<std::mutex> lock(mutex);
	return readData(data, length, timeout);
}

int
UnixSerialPort::readData(uint8_t *data, unsigned int length, unsigned int timeout)
{
	unsigned int received = 0;

	if (!isValid())
	{
		this->CloseWithoutLock();
		this->scanOpen(0, 3);
	}

	if (isValid())
	{
#ifdef _WIN32
      COMMTIMEOUTS timeouts = {0};
      DWORD nBytes = 0;
      
      timeouts.ReadIntervalTimeout = timeout * 1000;
      timeouts.ReadTotalTimeoutMultiplier = 0;
      timeouts.ReadTotalTimeoutConstant = 10;
      timeouts.WriteTotalTimeoutMultiplier = 0;
      timeouts.WriteTotalTimeoutConstant = 0;
      SetCommTimeouts( _hCommPort, &timeouts );
      ReadFile( _hCommPort, data, length, &nBytes, 0 );
      received = nBytes;
#else

		ssize_t retVal;
		fd_set readfs; // file descriptor set
		int maxfd = _fd + 1;
		int nSources;
		struct timeval timeOut;

		FD_ZERO(&readfs);

		FD_SET(_fd, &readfs); // set testing
		timeOut.tv_sec = 0;
		timeOut.tv_usec = timeout * 1000;

		while ((received < length) && (0 < (nSources = select(maxfd, &readfs, 0, 0, &timeOut))))
		{
			if (FD_ISSET(_fd, &readfs))
			{
				retVal = read(_fd, data, (size_t) (length - received));
				if (retVal <= 0)
				{   // failed to read from serial port
					break;
				}
				data = data + retVal;
				received = received + static_cast<unsigned int>( retVal );
			}
		}
#endif
	}
	return received;
}


bool
UnixSerialPort::writeChar(char ch)
{
	bool success = false;

	if (isValid())
	{
		if (ch == '\n')
		{
			ch = '\r';
		}
#ifdef _WIN32
        DWORD nBytes = 0;
        success = (WriteFile( _hCommPort, &ch, 1, &nBytes, 0 ) && nBytes == 1);
    #else
		std::lock_guard<std::mutex> lock(mutex);
		success = (write(_fd, &ch, 1) == 1);
#endif
	}
	return success;
}


int
UnixSerialPort::writeDataWithLock(uint8_t const *data, unsigned int length)
{
	std::lock_guard<std::mutex> lock(mutex);
	return writeData(data, length);
}

int
UnixSerialPort::writeData(uint8_t const *data, unsigned int length)
{
	int written = 0;

	if (!isValid())
	{
		this->CloseWithoutLock();
		this->scanOpen(0, 3);
	}

	if (isValid())
	{
#ifdef _WIN32
      DWORD nBytes = 0;
      WriteFile( _hCommPort, data, length, &nBytes, 0 );
      written = nBytes;
#else
		for (unsigned int i = 0; (written <= 0) && (i < 3); i++)
		{
			written = write(_fd, data, (size_t) length);
			VERBOSE("Written " << written << " bytes");
			if (written <= 0)
			{
				ERROR("Error writting data, reopening port.");
				this->CloseWithoutLock();
				this->scanOpen(0, 3);
			}
		}
		this->flushTx();
#endif
	}
#ifndef DEBUG
  std::cout << "UnixSerialPort::writeData called and written=" << written << std::endl;
#endif
	return written;
}


int UnixSerialPort::writeSlowly(const char *data, int length, int milliseconds)
{
	int written = 0;

	std::lock_guard<std::mutex> lock(mutex);
	for (int i = 0; i < length; ++i)
	{
		if (writeChar(data[i]))
		{
			++written;
		}
#ifdef _WIN32
        Sleep( milliseconds );
    #else
		usleep(milliseconds * 1000);
#endif
	}
	return written;
}

int
UnixSerialPort::CommandTxAndRx(uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[],
							   unsigned int const replyLength)
{
	int write;
	int read = -1;

#ifndef NDEBUG
	std::cout << "UnixSerialPort::CommandTxAndRx: " << "time before: " << time(0) << std::endl;
#endif
	std::lock_guard<std::mutex> lock(mutex);
	write = static_cast<int>( writeData(buffer, bufferLength));
	if (write < 0)
	{
		std::cerr << "Write to serial port failed" << std::endl;
		read = -1;
	}
	else
	{
		if ((replyLength > 0) && (reply != nullptr))
		{
			read = static_cast<int>( readData(reply, replyLength));
			if (read == -1)
			{
				std::cerr << "failed to read from the serial port..." << std::endl;
			}
		}
		else
		{
			read = 0;
		}
	}
#ifndef NDEBUG
	std::cout << "UnixSerialPort::CommandTxAndRx: " << "time after: " << time(0) << std::endl;
#endif

	return read;
}

BAUDRATE_TYPE UnixSerialPort::spdBaudrate(const char *str)
{
	BAUDRATE_TYPE speed = 0;
	int index = 0;
	unsigned int off = 0;

	bool found = false;

	if (str[0] != 'B')
	{
		off = 1;
	}
	while (index < BAUDRATES__LENGTH)
	{
		if (strcmp(str, &BAUDRATES_STRING[index][off]) == 0)
		{
			speed = BAUDRATES_SPEED[index];
			index = BAUDRATES__LENGTH;
			found = true;
		}
		++index;
	}
	if (not found)
		std::cout << "ERROR: Cannot find baudrate " << str << std::endl;
	return speed;
}


const char *UnixSerialPort::strBaudrate(BAUDRATE_TYPE speed)
{
	const char *str = "";
	int index = 0;

	while (index < BAUDRATES__LENGTH)
	{
		if (speed == BAUDRATES_SPEED[index])
		{
			str = BAUDRATES_STRING[index];
			index = BAUDRATES__LENGTH;
		}
		++index;
	}
	return str;
}

void UnixSerialPort::lock()
{
	this->mutex.lock();
}

void UnixSerialPort::unlock()
{
	this->mutex.unlock();
}

int UnixSerialPort::Open()
{
	if (!this->opened)
	{
		VERBOSE("Non opened yet.");
		std::lock_guard<std::mutex> lock(mutex);
		return Channel::Open();
	}
	else
	{
		VERBOSE("Opened");
	}
	return -1;
}

int UnixSerialPort::Close()
{
	std::lock_guard<std::mutex> lock(mutex);
	return Channel::Close();
}

bool UnixSerialPort::scanOpen(std::string prefix, unsigned int from, unsigned int to)
{
	this->lastPrefix = prefix;
	std::string strDev;
	for (unsigned int devNumber = from; devNumber < to; devNumber++)
	{
		strDev = prefix + std::to_string(devNumber);
		this->device = strDev;
		if (this->OpenWithoutLock() >= 0)
		{
			return true;
		}
	}
	return false;
}

bool UnixSerialPort::scanOpen(unsigned int from, unsigned int to)
{
	return this->scanOpen(this->lastPrefix, from, to);
}
