
/*
 * RX-series Actuator Definition 
 *
 */

#include "channel.hpp"
#include "robotis_servo_rx_series.hpp"
#include "interface.hpp"
#include "dynamixel_interface.hpp"

#ifndef NDEBUG
#include <boost/io/ios_state.hpp>
#include <iomanip>
#endif

#include <iostream>

#include <cmath>
#include <sstream>

#include "interface.hpp"
#include "utility.hpp"
#include "compilerdefinitions.h"


RobotisServoRXSeries::RobotisServoRXSeries( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt )
  : RobotisServo( name, interface, pt )

{
  int id = pt.get<int>("id");

  this->id = id;
  
  home = pt.get<float>("home",defaultHome);
  min =  pt.get<float>("min",defaultMin);
  max = pt.get<float>("max",defaultMax);

#ifndef NDEBUG
  std::cout << "Creating Robotis Servo RX Series " << name << " with id " << this->id << std::endl;
#endif
  hasFeedback = true;
}

int 
RobotisServoRXSeries::SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
  unsigned int gainI, unsigned int gainP)
{

  ERROR("GAIN SET NOT IMPLEMENTED!!!!");

  int result = -1;

  if ( ( ticks < RobotisServoRXSeries::MAX_TICKS ) && ( ispeed < RobotisServoRXSeries::MAX_ISPEED) )
    {
      /*
      result = TorqueOn();
      */
      uint8_t data[4] = { ( static_cast <uint8_t> ( ticks  & 0xff) ), ( static_cast <uint8_t> ( (ticks >> 8)  & 0xff) ), ( static_cast <uint8_t> (ispeed & 0xff) ), ( static_cast <uint8_t>  ( (ispeed >> 8)  & 0xff ) ) };

      result =  WriteRegisters( GOAL_POSITION_L, data, sizeof(data) );
      if ( result >= 0 )
	{
	  result = 0;
	}
      else 
	{
	  std::cerr << "RobotisServoRxSeries::SendPositionTicksAndISpeed failed with " << result << " ticks=" << ticks << " and speed="<< ispeed << std::endl;
	}
    }
  else
    {
      std::cerr << "RobotisServoRxSeries::SendPositionTicksAndISpeed failed with ticks " << ticks << " and ispeed " << ispeed << std::endl;
    }
      
  return result;
}


unsigned int
RobotisServoRXSeries::AngleRadiansToPositionTicks( float angle )
{
  // Angle is offset from home and converted to range between 0 and 360 deg
  float a = Utility::NormalizeAngle(angle);
  if ( a < 0 )
    {
      a = 2.0 * M_PI + a;
    }
  // The total range for the robotis servo is only 300 deg.
  //1024 ticks/300 deg = 1228.8 ticks
   
  a = Utility::Clamp(Utility::DegreeToRadians(30.0),a,Utility::DegreeToRadians(330.0) );
  int ticks = static_cast<int>( ( a - Utility::DegreeToRadians(30.0)) / (2.0 * M_PI) * 1228.8);

#ifndef NDEBUG
  std::cout << "AngleRadiansToPositionTicks(" << angle << ")=" << " a= " << a << " ticks=" << ticks << std::endl;
#endif

 return static_cast<unsigned int>( Utility::Clamp(0,static_cast<int>(ticks),1023) );
}

float 
RobotisServoRXSeries::AnglePositionTicksToRadians( unsigned int ticks )
{
  float f;
  if ( ticks < 1024 )
    {
      f = Utility::DegreeToRadians(30.0) + (static_cast<float>(ticks)/1024.0 ) * Utility::DegreeToRadians(300.0);
    }
  else
    {
      f = nanf("ticksToRadians");
    }  
#ifndef NDEBUG
  std::cout << "RobotisServoRXSeries::AnglePositionTicksToRadians(" << ticks << ")=" << f << std::endl;
#endif
  return f;
}


int 
RobotisServoRXSeries::ReadPositionTicksAndISpeedAndILoad( int * ticks, int * speed, int * iload )
{
  int result = 1;
  uint8_t reply[6];

  result = ReadRegisters( RobotisServoRXSeries::PRESENT_POSITION_L, reply, sizeof(reply) );
  if ( result == 0 )
    {
      if ( ticks != 0 )
	{
	  * ticks = ( reply[1] << 8 )  | ( reply[0] );
#ifndef NDEBUG
	  std::cout << "RobotisservoRXSeries::ReadPositionTicksAndISpeedAndILoad ticks " << * ticks << std::endl; 
#endif
	}
      
      if ( speed != 0 )
	{
	  * speed = ( reply[3] << 8 ) | ( reply[2] );
	}
      
      if ( iload != 0 )
	{
	  * iload = ( reply[5] << 8 ) | ( reply[4] );
	}
      result = 0;
    }
  else
    {
      std::cerr << "RobotisservoRXSeries - ReadPositionTicksAndISpeedAndILoad failed with error " << result << std::endl;
      result = -1;
    }
  return result;
} 

