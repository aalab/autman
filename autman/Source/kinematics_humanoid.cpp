#include <iostream>
#include <string>

#include "interface.hpp"
#include "trajectory.hpp"
#include "actuator.hpp"
#include "motion.hpp"
#include "robot.hpp"
#include "kinematics_head.hpp"
#include "kinematics_arm.hpp"
#include "kinematics_leg.hpp"
#include "kinematics_humanoid.hpp"
#include "compilerdefinitions.h"

KinematicsHumanoid::KinematicsHumanoid(Robot const *robot)
	: gaitParams(), head(robot), leftArm(robot, true), rightArm(robot, false), leftLeg(robot, true, true),
	  rightLeg(robot, false, true), robot(robot)
{
}

KinematicsHumanoid::~KinematicsHumanoid()
{
}

std::unique_ptr<Motion> KinematicsHumanoid::calcWalkingGait(HumanoidTrajectoryParams &params) const
{
	return calcWalkingGait(params.stepX, params.stepY, params.stepAngle, params.maxHeightProp, params.transitionProp,
						   params.hipPitch, params.anklePitch, params.stepHeight, params.xOffset, params.yOffset,
						   params.zOffset, params.transferAnkleRoll, params.transferHipRoll, params.transferY,
						   params.bufferAngle);
}

std::unique_ptr<Motion> KinematicsHumanoid::calcWalkingGait(float stepX, float stepY, float stepAngle,
															float maxHeightProp, float transitionProp, float hipPitch,
															float anklePitch, float stepHeight, float xOffset,
															float yOffset, float zOffset, float transferAnkleRoll,
															float transferHipRoll, float transferY,
															float bufferAngle) const
{
	bool successful = true;
	std::unique_ptr<Motion> motion(new Motion(robot, ""));
	float stepAirTime = 2.0 * transitionProp + maxHeightProp;
	float bothFeetDownTime = 0.5 - stepAirTime;
	std::vector<float> legMeasurements;
	robot->getLegMeasurements(legMeasurements);
	float baseY = legMeasurements[0] + yOffset;
	float tolerance = 0.000001;

	if (2.0 * stepAirTime > 1.0 + tolerance)
	{
		std::cerr << "ERROR: total time proportion (" << 2.0 * stepAirTime << ") exceeds 1.0" << std::endl;
		successful = false;
	}
	else
	{
		float keyTimes[NUM_POSITIONS];
		float leftLegKeyPosns[NUM_POSITIONS][3];
		float rightLegKeyPosns[NUM_POSITIONS][3];
		std::vector<float> leftLegKeyAngles[NUM_POSITIONS];
		std::vector<float> rightLegKeyAngles[NUM_POSITIONS];
		std::vector<float> ikParams = std::vector<float>();

		for (int i = 0; i < NUM_POSITIONS; i++)
		{
			leftLegKeyAngles[i] = std::vector<float>();
			rightLegKeyAngles[i] = std::vector<float>();
		}

		//calculate key times
		keyTimes[0] = bothFeetDownTime / 2.0 + 0.25 - maxHeightProp / 2.0 - transitionProp;
		keyTimes[1] = bothFeetDownTime / 2.0 + 0.25 - maxHeightProp / 2.0;
		keyTimes[2] = bothFeetDownTime / 2.0 + 0.25 + maxHeightProp / 2.0;
		keyTimes[3] = bothFeetDownTime / 2.0 + 0.25 + maxHeightProp / 2.0 + transitionProp;
		keyTimes[4] = bothFeetDownTime / 2.0 + 0.75 - maxHeightProp / 2.0 - transitionProp;
		keyTimes[5] = bothFeetDownTime / 2.0 + 0.75 - maxHeightProp / 2.0;
		keyTimes[6] = bothFeetDownTime / 2.0 + 0.75 + maxHeightProp / 2.0;
		keyTimes[7] = bothFeetDownTime / 2.0 + 0.75 + maxHeightProp / 2.0 + transitionProp;

		//calculate key positions
		leftLegKeyPosns[0][0] = xOffset + stepX / 2.0;
		leftLegKeyPosns[0][1] = baseY - transferY - stepY;
		leftLegKeyPosns[0][2] = zOffset;
		rightLegKeyPosns[0][0] = xOffset - stepX / 2.0;
		rightLegKeyPosns[0][1] = -baseY - transferY;
		rightLegKeyPosns[0][2] = zOffset;

		leftLegKeyPosns[1][0] = xOffset + stepX / 2.0 - stepX * transitionProp / stepAirTime;
		leftLegKeyPosns[1][1] = baseY - transferY - stepY;
		leftLegKeyPosns[1][2] = zOffset;
		rightLegKeyPosns[1][0] = xOffset - stepX / 2.0 + stepX * transitionProp / stepAirTime;
		rightLegKeyPosns[1][1] = -baseY - transferY;
		rightLegKeyPosns[1][2] = zOffset + stepHeight;

		leftLegKeyPosns[2][0] = xOffset + stepX / 2.0 - stepX * (transitionProp + maxHeightProp) / stepAirTime;
		leftLegKeyPosns[2][1] = baseY - transferY - stepY;
		leftLegKeyPosns[2][2] = zOffset;
		rightLegKeyPosns[2][0] = xOffset - stepX / 2.0 + stepX * (transitionProp + maxHeightProp) / stepAirTime;
		rightLegKeyPosns[2][1] = -baseY - transferY - stepY;
		rightLegKeyPosns[2][2] = zOffset + stepHeight;

		leftLegKeyPosns[3][0] = xOffset - stepX / 2.0;
		leftLegKeyPosns[3][1] = baseY - transferY;
		leftLegKeyPosns[3][2] = zOffset;
		rightLegKeyPosns[3][0] = xOffset + stepX / 2.0;
		rightLegKeyPosns[3][1] = -baseY - transferY - stepY;
		rightLegKeyPosns[3][2] = zOffset;

		leftLegKeyPosns[4][0] = xOffset - stepX / 2.0;
		leftLegKeyPosns[4][1] = baseY + transferY;
		leftLegKeyPosns[4][2] = zOffset;
		rightLegKeyPosns[4][0] = xOffset + stepX / 2.0;
		rightLegKeyPosns[4][1] = -baseY + transferY - stepY;
		rightLegKeyPosns[4][2] = zOffset;

		leftLegKeyPosns[5][0] = xOffset - stepX / 2.0 + stepX * transitionProp / stepAirTime;
		leftLegKeyPosns[5][1] = baseY + transferY;
		leftLegKeyPosns[5][2] = zOffset + stepHeight;
		rightLegKeyPosns[5][0] = xOffset + stepX / 2.0 - stepX * transitionProp / stepAirTime;
		rightLegKeyPosns[5][1] = -baseY + transferY - stepY;
		rightLegKeyPosns[5][2] = zOffset;

		leftLegKeyPosns[6][0] = xOffset - stepX / 2.0 + stepX * (transitionProp + maxHeightProp) / stepAirTime;
		leftLegKeyPosns[6][1] = baseY + transferY - stepY;
		leftLegKeyPosns[6][2] = zOffset + stepHeight;
		rightLegKeyPosns[6][0] = xOffset + stepX / 2.0 - stepX * (transitionProp + maxHeightProp) / stepAirTime;
		rightLegKeyPosns[6][1] = -baseY + transferY - stepY;
		rightLegKeyPosns[6][2] = zOffset;

		leftLegKeyPosns[7][0] = xOffset + stepX / 2.0;
		leftLegKeyPosns[7][1] = baseY + transferY - stepY;
		leftLegKeyPosns[7][2] = zOffset;
		rightLegKeyPosns[7][0] = xOffset - stepX / 2.0;
		rightLegKeyPosns[7][1] = -baseY + transferY;
		rightLegKeyPosns[7][2] = zOffset;

		for (int i = 0; i < NUM_POSITIONS && successful; i++)
		{
			//do inverse kinematics for the left leg
			ikParams.clear();
			ikParams.push_back(leftLegKeyPosns[i][0]);
			ikParams.push_back(leftLegKeyPosns[i][1]);
			ikParams.push_back(leftLegKeyPosns[i][2]);
			if (i >= 6 || i <= 2)
			{
				ikParams.push_back(stepAngle);
			}
			else
			{
				ikParams.push_back(0.0);
			}
			successful = leftLeg.inverseKinematics(leftLegKeyAngles[i], ikParams, bufferAngle) == 0;
			if (successful)
			{
				//do inverse kinematics for the right leg
				ikParams.clear();
				ikParams.push_back(rightLegKeyPosns[i][0]);
				ikParams.push_back(rightLegKeyPosns[i][1]);
				ikParams.push_back(rightLegKeyPosns[i][2]);
				if (i >= 2 && i <= 6)
				{
					ikParams.push_back(stepAngle);
				}
				else
				{
					//break;
					ikParams.push_back(0.0);
				}
				successful = rightLeg.inverseKinematics(rightLegKeyAngles[i], ikParams, bufferAngle) == 0;

				if (successful)
				{
					//apply post-processing angles
					leftLegKeyAngles[i][KinematicsLeg::HIP_LATERAL] -= hipPitch;
					rightLegKeyAngles[i][KinematicsLeg::HIP_LATERAL] += hipPitch;
					leftLegKeyAngles[i][KinematicsLeg::ANKLE_LATERAL] += anklePitch;
					rightLegKeyAngles[i][KinematicsLeg::ANKLE_LATERAL] -= anklePitch;

					if (i == 1 || i == 2)
					{
						leftLegKeyAngles[i][KinematicsLeg::ANKLE_FRONTAL] += transferAnkleRoll;
						leftLegKeyAngles[i][KinematicsLeg::HIP_FRONTAL] -= transferHipRoll;
					}
					if (i == 5 || i == 6)
					{
						rightLegKeyAngles[i][KinematicsLeg::ANKLE_FRONTAL] -= transferAnkleRoll;
						rightLegKeyAngles[i][KinematicsLeg::HIP_FRONTAL] += transferHipRoll;
					}
				}
				else
				{
					//ERROR message should already be printed by inverseKinematics
				}
			}
			else
			{
				//break;
				//ERROR message should already be printed by inverseKinematics
			}
		} //for

		if (successful)
		{
			motion->trajectories.clear();
			motion->actuators.clear();
			motion->SetValid(true);
			std::string leftLegNames[] = {"LeftHipTransversal", "LeftHipFrontal", "LeftHipLateral", "LeftKneeLateral",
										  "LeftAnkleLateral", "LeftAnkleFrontal"};
			std::string rightLegNames[] = {"RightHipTransversal", "RightHipFrontal", "RightHipLateral",
										   "RightKneeLateral", "RightAnkleLateral", "RightAnkleFrontal"};

			std::cout << "Times" << std::endl;
			for (unsigned int i = 0; i < NUM_POSITIONS; i++)
			{
				std::cout << keyTimes[i] << std::endl;

				std::cout << "Position LEFT #" << i << " (";
				for (unsigned int j = 0; j < leftLegKeyAngles[i].size(); j++)
				{
					if (j > 0)
						std::cout << " ";
					std::cout << leftLegNames[j] << ":" << leftLegKeyAngles[i][j];
				}

				std::cout << ")" << std::endl << "Position RIGHT #" << i << " (";
				for (unsigned int j = 0; j < rightLegKeyAngles[i].size(); j++)
				{
					if (j > 0)
						std::cout << " ";
					std::cout << rightLegNames[j] << ":"  << rightLegKeyAngles[i][j];
				}

				std::cout << ")" << std::endl << "-----------------------------------------------------------" << std::endl;
			}


			successful = successful && updateMotion(motion, leftLegKeyAngles, keyTimes, leftLegNames, NUM_LEG_SERVOS);
			if (!successful)
				VERBOSE("!successful");
			successful = successful && updateMotion(motion, rightLegKeyAngles, keyTimes, rightLegNames, NUM_LEG_SERVOS);

			if (!successful)
			{
				VERBOSE("!successful");
				motion = nullptr;
			}
		}
	}
	return motion;
}

bool KinematicsHumanoid::updateMotion(std::unique_ptr<Motion> &motion, std::vector<float> *angles, float *times,
									  std::string *names, int numServos) const
{
	bool successful = true;

	for (int i = 0; i < numServos; i++)
	{
		Trajectory *currTrajectory = new Trajectory("OmniWalk", names[i]);
		Actuator *currActuator = nullptr;

		for (int j = 0; j < NUM_POSITIONS; j++)
		{
			currTrajectory->Add(angles[j][i], times[j]);
		}
		motion->trajectories.push_back(*currTrajectory);

		for (Interface *iface:robot->GetInterfaces())
		{
			currActuator = iface->FindActuatorByName(names[i]);
			if (currActuator != nullptr)
			{
				motion->actuators.push_back(currActuator);
				currActuator->AddTrajectory(*currTrajectory);
				//std::cout << names[i] << std::endl;

				break;
			}
		}

		if (currActuator == nullptr)
		{
			VERBOSE("currActuator == nullptr (" << names[i] << ")");
			successful = false;
			break;
		}
	}

	return successful;
}

HumanoidTrajectoryParams::HumanoidTrajectoryParams()
{
	stepX = DEFAULT_DELTA_X;
	stepY = DEFAULT_DELTA_Y;
	stepAngle = DEFAULT_DELTA_THETA;
	maxHeightProp = DEFAULT_MAX_HEIGHT_PROP;
	transitionProp = DEFAULT_TRANSITION_PROP;
	stepHeight = DEFAULT_STEP_HEIGHT;
	hipPitch = DEFAULT_HIP_PITCH;
	anklePitch = DEFAULT_ANKLE_PITCH;
	xOffset = DEFAULT_X_OFFSET;
	yOffset = DEFAULT_Y_OFFSET;
	zOffset = DEFAULT_Z_OFFSET;
	transferAnkleRoll = DEFAULT_TRANSFER_ANKLE_ROLL;
	transferHipRoll = DEFAULT_TRANSFER_HIP_ROLL;
	transferY = DEFAULT_TRANSFER_Y;
	bufferAngle = DEFAULT_BUFFER_ANGLE;
}

HumanoidTrajectoryParams::~HumanoidTrajectoryParams()
{ }
