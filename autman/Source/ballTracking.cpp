
#include "config.h"

#include "position.hpp"
#include "robot.hpp"
#include "utility.hpp"
#include "trajectory.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/array.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <syslog.h>
#include <unistd.h>
#include <string>
#include <json/value.h>
#include <json/reader.h>
#define DEFAULT_VISION_PORT "1515"
#define DEFAULT_VISION_SERVER "127.0.0.1"
#define DEFAULT_ROBOT_PORT  "1313"
#define DEFAULT_ROBOT_SERVER "127.0.0.1"

static bool ball_found = false;
static int ball_x      = -1;
static int ball_y      = -1;
static int topX        = -1;
static int topY        = -1;
static int bottomX     = -1;
static int bottomY     = -1;

using boost::asio::ip::udp;

std::string
SendCommand(  udp::socket & socket, udp::resolver::iterator & iterator, std::string const & command )
{
    char replyBuffer[4096];
    size_t replyBufferMaxLength = sizeof( replyBuffer );

    socket.send_to( boost::asio::buffer( command ), * iterator );

    udp::endpoint sender_endpoint;
    size_t replyLength = socket.receive_from(boost::asio::buffer( replyBuffer, replyBufferMaxLength), sender_endpoint );

    std::string reply(replyBuffer,replyLength);
#ifndef NDEBUG
    std::cout << "Soccer::SendCommand Responses " << reply << std::endl;
#endif

    return reply;
}

std::string
SendPosition( udp::socket & socket, udp::resolver::iterator & iterator, Position const & pos )
{
    return SendCommand( socket, iterator, pos.ToCommandString() );
}

std::string
SendTrajectory( udp::socket & socket, udp::resolver::iterator & iterator, Trajectory const & trajectory )
{
#ifndef NDEBUG
    std::cout<<"from SendTrajectory "<<trajectory.ToCommandString()<<std::endl;
#endif
    return SendCommand( socket, iterator, trajectory.ToCommandString() );
}

static std::vector<Trajectory> motions;

void
LoadTrajectories( boost::property_tree::ptree & pt )
{
    using boost::property_tree::ptree;

    BOOST_FOREACH( ptree::value_type & v , pt.get_child("trajectories"))
    {
        std::string motionName = (std::string)v.first.data();
#ifndef NDEBUG
        std::cout << "Found Trajectory for: " << motionName << std::endl;
#endif
        BOOST_FOREACH( ptree::value_type & joint , v.second)
        {
            std::string jointName = (std::string)joint.first.data();
#ifndef NDEBUG
            std::cout << "Found  joint: " << jointName << std::endl;
#endif
            Trajectory tmpT( motionName, jointName, joint.second );
            motions.push_back(tmpT);
        }
    }
}

void
LoadTrajectories(std::string const filename )
{
    using boost::property_tree::ptree;
    ptree pt;

    read_info( filename, pt );
    LoadTrajectories( pt );
}


unsigned long int tdiffUSec(struct timespec const start, struct timespec const end);

void addTimeNSec( struct timespec * time, long int disp );


float polynomialHold(float const amplitudes[], float const durations[], unsigned int N, float currentCyclePercentage);

int
ParseFromStreamVision(std::string const & message)
{
    int result = -1;
    std::string trim;
    std::string keyword;
    unsigned int size;
    unsigned int tx;
    unsigned int ty;
    unsigned int bx;
    unsigned int by;
    std::string target;
    std::string state;
    char sep;
    unsigned int tmpx;
    unsigned int tmpy;

    ball_found = false;

    std::ostringstream os;
    Json::Value value;
    Json::Reader reader;
    os <<  message;
    std::istringstream iss( os.str() );
    std::getline(iss, trim);
    bool success = reader.parse(trim, value);

    if ( success && value["success"].asBool() == true )
    {

#ifndef NDEBUG
        std::cout<< "-----trim----"<< trim << std::endl;
#endif
        if (value["data"].isArray()){
            if (value["data"].size() > 0){
                if (value["data"][0]["type"] == "ball"){
                    target = "ball";
                    size = value["data"][0]["size"].asInt();
                    result = 1;
                    ball_found = true;
                    ball_x = value["data"][0]["center"]["x"].asInt();
                    ball_y = value["data"][0]["center"]["y"].asInt();
                    tx = value["data"][0]["boundBox"]["topLeft"]["x"].asInt();
                    ty = value["data"][0]["boundBox"]["topLeft"]["y"].asInt();
                    bx = value["data"][0]["boundBox"]["bottomRight"]["x"].asInt();
                    by = value["data"][0]["boundBox"]["bottomRight"]["y"].asInt();

                }
            }
        }

        if( ball_found )
        {
#ifndef NDEBUG
            std::cout<<"ParseFromStreamVision found object " << "Target:" << target << ", size:" << size << ", center X" <<ball_x << ", center Y" << ball_y << ", top X"  << tx << ", top Y" << ty << ", bottom X"  << bx << ", bottom Y" << by << std::endl;
#endif
        }
        else
        {
            result = -1;
            goto err_exit;
        }
    }

    err_exit:
#ifndef NDEBUG
    std::cout<< "ParseFromStreamVision returns " << result << std::endl;
#endif
    return result;

}

float
ImageXCoordinateToRadians( float ball_x )
{
    float const cameraRange = Utility::DegreeToRadians( 45.0 );
    float const cameraWidth = 160.0;

    float dx = ( ball_x - cameraWidth/2.0f ) / (-cameraWidth/2.0f);
    float dRad = dx * cameraRange;

    return dRad;
}

int
main(int argc, char *argv[] )
{
    namespace po = boost::program_options;
    std::string vision_port;
    std::string vision_server;
    std::string robot_port;
    std::string robot_server;
    std::string gyro_port;
    std::string gyro_server;
    std::vector<std::string> trajectoryFiles;

    po::options_description commandLineOnlyOptions("Command Line Options");
    commandLineOnlyOptions.add_options()
            ( "version,v", "print version string" )
            ( "help,h"   , "print help message" )
            ( "robot_server" , po::value<std::string>( & robot_server )->default_value(DEFAULT_ROBOT_SERVER), "robot server address" )
            ( "robot_port" , po::value<std::string> ( & robot_port )->default_value(DEFAULT_ROBOT_PORT), "robot port number" )
            ("vision_server" , po::value<std::string> ( & vision_server )->default_value(DEFAULT_VISION_SERVER),"vision server" )
            ("vision_port" , po::value<std::string> ( & vision_port )->default_value(DEFAULT_VISION_PORT),"vision port number" )
            ( "trajectories,t", po::value<std::vector<std::string> >( & trajectoryFiles ), "file with trajectory information");

    std::cout << "Ball tracking" << " " << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;

#ifndef NDEBUG
    std::cout << "Soccer Debugging build" << std::endl;
#endif

    po::options_description commandLineOptions;
    commandLineOptions.add(commandLineOnlyOptions);

    po::positional_options_description p;
    p.add("trajectories",-1);

    try
    {
        po::variables_map vm;
        po::store( po::command_line_parser( argc, argv).options(commandLineOptions).positional(p).run(), vm );
        po::notify( vm );

        if ( vm.count("help") )
        {
            std::cout << commandLineOptions << "\n";
            return 1;
        }

        if (vm.count("version") )
        {
            std::cout << "Scoccer Rostam Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
            return 1;
        }
    }
    catch( std::exception & e )
    {
        std::cerr << "boost::po exception " << e.what() << std::endl;
        return 1;
    }

    std::cout << "robot server " << robot_server << ", robot port " << robot_port << "vision server " << vision_server << ", vision port " << vision_port << std::endl;
    std::cout << "Trajectory Files" << std::endl;

    for ( std::string s : trajectoryFiles )
    {
        if ( ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".info" ) &&
             ( s.substr( std::max( 5, static_cast<int>(s.size() - 5) ) ) != ".json" ) &&
             ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".xml" ) &&
             ( s.substr( std::max( 4, static_cast<int>(s.size() - 4) ) ) != ".ini" ) )
        {
            s = s + ".info";
        }

        LoadTrajectories( std::string(CONFIG_DIR) + "/" + s );
    }

    std::cout << std::endl;

    boost::asio::io_service io_service;
    udp::socket robot_socket( io_service, udp::endpoint( udp::v4(), 0 ) );
    udp::socket vision_socket( io_service, udp::endpoint( udp::v4(), 0 ) );

    udp::resolver robot_resolver( io_service );
    udp::resolver::query robot_query( udp::v4(), robot_server, robot_port );
    udp::resolver::iterator robot_iterator = robot_resolver.resolve( robot_query );

    udp::resolver vision_resolver( io_service );
    udp::resolver::query vision_query( udp::v4(), vision_server, vision_port );
    udp::resolver::iterator vision_iterator = vision_resolver.resolve( vision_query );

    Position saved;

    std::string reply;
    std::string replyVision;
    std::string com("ReadState");
    std::string newCom("{\"name\":\"Arash\", \"command\":\"ReadState\"}");
    reply = SendCommand( robot_socket, robot_iterator, newCom );

#ifndef NDEBUG
    std::cout << "Reply for Read State " << reply << std::endl;
#endif

    std::istringstream ireply( reply );

    if ( saved.parseFromStream( ireply ) < 0 )
    {
        std::cerr << "ERROR: Unable to parse position from response " << reply << std::endl;
//      std::exit(34);
    }

    for( Trajectory & traj : motions )
    {
        std::string reply = SendTrajectory( robot_socket, robot_iterator, traj );

#ifndef NDEBUG
        std::cout << "Response from sending trajectory " << reply << std::endl;
#endif
    }

#ifndef NDEBUG
    std::cout << "Saved position" << std::endl;
    saved.Print();
#endif

    int check       = -1;
    float angleTilt = 0.0;
    float anglePan  = 0.0;
    static float angleTiltStart = 0.0;
    static float anglePanStart  = 0.0;
    static bool cont;
    static int varCycleTime=0;
    static int varCycles=0;
    static double angleNt = 0.0;
    static double angleNl = 0.0;
    const double MIN_ANGLE_NL = -0.314;
    const double MAX_ANGLE_NL = 1.29;
    const double MIN_ANGLE_NT = -2.2;
    const double MAX_ANGLE_NT = 2.2;
    static float speed = 250.0; //how fast neck moves to track ball

    //direction , clockwise or anticlockwise
    int directionNt = 1; //
    int directionNl = 1;

    //should we send to necktransversal(1) or neck lateral(2)
    int toSend = 1;

    //how many frames to check/how long to run ball tracking
    int cycleCount;

    std::cout << "NeckTransversal " << angleNt << " Necklateral " << angleNl << std::endl;
    std::ostringstream  os;
    std::string newSend = "{\"name\": \"Arash\", \"command\": \"visionmode\", \"params\":{\"mode\":\"obstaclerun\"}}";
    replyVision = SendCommand( vision_socket, vision_iterator, newSend );
    std::cout << "Vision reply " << replyVision << std::endl;

    std::cout << "Enter a cycle count: ";
    std::cin >> cycleCount;

    for(int x = 0; x < cycleCount; x++)
    {
        std::ostringstream os;

        newSend = "{\"name\": \"Arash\", \"command\": \"fetch\"}";
        replyVision = SendCommand( vision_socket, vision_iterator, newSend );
#ifndef NDEBUG
        std::cout << "Vision reply " << replyVision << std::endl;
#endif
        usleep(100);
        check = ParseFromStreamVision( replyVision );
        std::string general="{\"name\":\"Arash\", \"command\":\"Actuator\",\"params\":{\"command\": \"Position\"," ;
        std::string replyFromSending;
        //if we found the ball, do nothing for now
        if( check >= 0)
        {

        }
        else{
            os.str("");
            angleNt += 0.005*directionNt;
            angleNl += 0.005*directionNl;
            if (angleNt >= MAX_ANGLE_NT || angleNt <= MIN_ANGLE_NT){
                directionNt *= -1;
                toSend = 2;
            }
            if (angleNl >= MAX_ANGLE_NL || angleNl <= MIN_ANGLE_NL){
                directionNl *= -1;
            }

            //Send command to move the neck transversal
            if (toSend == 1) {
                os << general << "\"name\":\"NeckTransversal\", \"angle\":" << angleNt << ", \"speed\": " << speed <<
                "}}";
                replyFromSending = SendCommand(robot_socket, robot_iterator, os.str());
                std::cout << replyFromSending << std::endl;
            }
            //Send command to move the neck lateral
            else {
                os << general << "\"name\":\"NeckLateral\", \"angle\":" << angleNl << ", \"speed\": " << speed << "}}";
                replyFromSending = SendCommand(robot_socket, robot_iterator, os.str());
                angleNl += 0.005 * directionNl;
                toSend = 1;
            }


        }
    }
}
