/*
 * Generic RC Servos without feedback
 * Jacky Baltes <jacky@cs.umanitoba.ca>
 */

#include "generic_rc_servo_no_feedback.hpp"
#include "interface.hpp"
#include "servo.hpp"
#include "utility.hpp"

#include "roboard.h"

#include <boost/property_tree/ptree.hpp>
#include <cmath>
#include <string>

#ifndef NDEBUG
#include <iostream>
#endif

GenericRCServoNoFeedback::GenericRCServoNoFeedback( std::string name, Interface * interface, boost::property_tree::ptree & pt )
  : Servo( name, interface, pt ),
    defaultHome( Utility::DegreeToRadians(180.0) ),
    defaultMin( Utility::DegreeToRadians(0.0) ),
    defaultMax( Utility::DegreeToRadians(360.0) ),
    savedTicks(-1)
{
  int id = pt.get<int>("id");

  this->id = id;

  home = pt.get<float>("home",defaultHome);
  min =  pt.get<float>("min",defaultMin);
  max = pt.get<float>("max",defaultMax);
  
#ifndef NDEBUG
  std::cout << "Creating GenericRCServoNoFeedback Servo " << name << " with id " << this->id << std::endl;
#endif
  type = GENERIC_RC_SERVO_NO_FEEDBACK;
  hasFeedback = false;
}

long int
AngleRadiansToPositionTicks( float angle )
{
    float a = Utility::NormalizeAngle(angle);
  if ( a < 0 )
    {
      a = 2.0 * M_PI + a;
    }

    a = Utility::Clamp(Utility::DegreeToRadians(90.0),a,Utility::DegreeToRadians(270.0) );
  long int ticks = static_cast<long int>( 0L + a / (2.0 * M_PI) * 3000.0);

#ifndef NDEBUG
  std::cout << "AngleRadiansToPositionTicks(" << angle << ")=" << " a= " << a << " ticks=" << ticks << std::endl;
#endif

 return static_cast<long int>( Utility::Clamp(700L ,static_cast<long int>(ticks),2300L ) );
}

int
GenericRCServoNoFeedback::ProcessCommand( std::string const & angleAndSpeed )
{
  int ret = 0;

  if ( interface != 0 ) 
    {
      float speed = -1.0;
      float angle = 0.0;

      ret = ParsePositionAndSpeedMessage( angleAndSpeed, & angle, & speed );
#ifndef NDEBUG
      std::cout << "GenericRCServoNoFeedback::ProcessCommand Angle " << angle << " speed " << speed << " Checking ret " << ret << std::endl;
#endif
      if ( ret == 0 )
	{
	  ret = SendPositionAndSpeedMessage( angle, speed );
	}
    }
  return ret;
}

int
GenericRCServoNoFeedback::SendPositionAndSpeedMessage( float angle, float speed )
{
  long ticks = AngleRadiansToPositionTicks( angle + home );
  int ret = 0;

  if ( ticks >= 0 )
    {
#ifndef NDEBUG
      std::cout << "GenericRCServoNoFeedback sending pos " << ticks << " for id " << id << std::endl;
#endif
      rcservo_SendCPWM( id, 25000L, ticks );  
      savedTicks = ticks;
    }
  return ret;
}

int
GenericRCServoNoFeedback::SetMotorState( enum MotorState state )
{
  if ( state == MOTOR_ON )
    {
      if ( savedTicks >= 0L )
	{
	  rcservo_SendCPWM( id, 25000L, savedTicks );
	}
      else
	{
	  return -1;
	}
    }
  else
    {
      rcservo_StopPWM( id );
      savedTicks = -1;
    }
  return 0;
}

int
GenericRCServoNoFeedback::ReadState( std::string & pos )
{ std::ostringstream os;
  int result = 0;

  if ( savedTicks >= 0 )
    {
      if ( (pos.length() > 0 ) && ( pos[pos.length()-1] != '&' ) )
	{
	  os << '&';
	}
      os << name << ":";
      float a = PositionTicksToRadians(savedTicks);
      float angle = Utility::NormalizeAngle( a - home );
      os << "angle=" <<  angle << " estimated=1";
      pos = pos + os.str();
#ifndef NDEBUG
      std::cout << "GenericRCServoNoFeedback::ReadState returns string " << pos << std::endl;
#endif  
    }
  else
    {
#ifndef NDEBUG
      std::cout << "GenericRCServoNoFeedback::ReadState failed with savedTicks " << savedTicks << std::endl;
#endif
      result = -1;
    }
  return result;
}

float
GenericRCServoNoFeedback::PositionTicksToRadians( int long ticks )
{
  float angle = (static_cast<float>(ticks) / 3000.0) * 2.0 * M_PI;
  return angle;
}
