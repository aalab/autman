#include "channel.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>
#include "channel.hpp"
#include "unix_serial_port.hpp"

#if defined(ENABLE_RB110_SERIAL)
#   include "rb110_serial_port.hpp"
#endif

#include <string>

#include "roboard.h"
#include "compilerdefinitions.h"
#include <boost/property_tree/ptree.hpp>
#include <cstdio>

#ifndef NDEBUG

#include <boost/io/ios_state.hpp>
#include <iomanip>

#endif


Channel::Channel(std::string name, boost::property_tree::ptree &pt)
	: opened(false),
	  type(INVALID_CHANNEL),
	  name(name)
{
	VERBOSE("Channel name " << name << " of type " << type);
}

Channel *
Channel::factory(std::string name, boost::property_tree::ptree &pt)
{
	Channel *channel = nullptr;

	std::string type = pt.get<std::string>("type");

#ifndef NDEBUG
	std::cout << "Channel name " << name << std::endl;
#endif
	boost::to_lower(type);

	if (type == "unix_serial")
	{
		channel = new UnixSerialPort(name, pt);
	}
#if defined(ENABLE_RB110_SERIAL)
  else if ( type == "rb110_serial" )
    {
      channel = new RB110SerialPort( name, pt );
    }
#endif
	else
	{
		ERROR("Unknown interface type " << type);

		std::exit(2);
	}
	return channel;
}

int
Channel::Open(void)
{
	if (!this->opened)
	{
		int i = this->OpenWithoutLock();
		return i;
	}
	return 0;
}

int
Channel::Close(void)
{
	opened = false;
	return this->CloseWithoutLock();
}

ChannelLockGuard::ChannelLockGuard(Channel &channel)
	: channel(channel)
{
	this->channel.lock();
}

ChannelLockGuard::~ChannelLockGuard()
{
	this->channel.unlock();
}

void Channel::dumpPortSettings()
{ }

void Channel::Reset()
{
	this->CloseWithoutLock();
	this->OpenWithoutLock();
}

int Channel::CloseWithoutLock(void)
{
	return -1;
}

int Channel::OpenWithoutLock()
{
	return -1;
}

bool Channel::scanOpen(std::string prefix, unsigned int from, unsigned int to)
{
	return false;
}

bool Channel::scanOpen(unsigned int from, unsigned int to)
{
	return false;
}
