Arash
=====

JSON Schemma
------------

All messages are passed through UDP using JSON format. The libjsoncpp is used to parse messages.

Each message complies the following standard:

	{
		"name": "Arash", 		// Robot name
		"command": "readstate", // Command
		"params":				// If applied (not in this case), object content depends each command.
		{ }
	}

Each response complies the following standard:

	{
		"success": true,						// If success
		"name": "Arash",						// Name of the robot
		"errcode": "E1",						// Error code, if not success
		"errmsg": "Error parsing the input"		// Error message, if not success
	}

Commands
--------

### ReadState

	{
		"name":"Arash",							// Name of the robot
		"command":"ReadState"
		"params":								// Optional
		{
			"actuators": ["NeckLateral","NeckTransversal"]		// Select specific servos to be read. If null or empty read all.
		}
	}

### Move

The move command with only 1 frame.

	{
		"name":"Arash",							// Name of the robot
		"command":"Move",
		"params":
		{
			"stop": true,						// Optional. If true, ends the current motion at the end of the cycle.
			"dx": 0.0,
			"dy": 0.0,
			"dtheta": 0.0,
			"cycleTimeUSec": 650000,
			"cycles": 10
		}
	}

Same command in a single line:

	{ "name":"Arash", "command":"Move", "params": { "stop": true, "dx": 0.0, "dy": 0.0, "dtheta": 0.0, "cycleTimeUSec": 650000, "cycles": 10 } }

The move command with multiple frame definitions.

	{
		"name":"Arash",
		"command":"Move",
		"params":
		{
			"stop": true,						// Optional. If true, ends the current motion at the end of the cycle.
			"dx": [0.01, 0.1],
			"dy": [0, 0],
			"dtheta": [0, 0],
			"cycleTimeUSec": [650000, 650000],
			"cycles": [10, 10]
		}
	}

### Actuator

#### Position

Set the position and speed of the actuator.

	{
		"name": "Arash",
		"command": "Actuator",
		"params":
		{
			"command":"Position",
			"name":"NeckLateral",
			"angle":0.0,
			"speed":500,

			"gainP": 32,						// Change gains to the actuator. if one filled, all must be.
			"gainI": 0,							// All values has to be between 0 and 254
			"gainD": 0
		}
	}

#### Mode

Turn the actuator On or Off.

	{
		"name": "Arash",
		"command": "Actuator",
		"params":
		{
			"name":"NeckLateral",
			"mode":"On" 								// "On" or "Off" values accepted
		}
	}

#### Trajectory

Set a trajectory for the actuator.

	{
		"name": "Arash",
		"command": "Actuator",
		"params":
		{
			"command": "Trajectory",
			"name":"NeckLateral",
			"trajectory": "trajectory1",
			"frames": [
				{ "angle": 1.0, "time": 0.0 },
				{ "angle": -1.0, "time": 1.0 },
				{ "angle": 1.0, "time": 2.0 },
				{ "angle": 0.0, "time": 2.5 }
			]
		}
	}

### ReadGains

Read gains from all actuators:

	{
		"name":"Arash",
		"command":"ReadGains"
	}

Read gains from specified actuators:

	{
		"name":"Arash",
		"command":"ReadGains",
		"params":
		{
			"actuators": ["NeckLateral", "NeckTransversal"]
		}
	}

Error codes
-----------

| Error Code	| Description																							|
|---------------|---------------------------------------																|
| E0			| Unexpected exception. Usually a system exception that throws from the command processing.				|
| E1			| Error parsing the input.																				|
| E2-1			| Wrong robot name.																						|
| 				| 																										|