#ifndef __RB110_SERIAL_PORT_HPP__
#define __RB110_SERIAL_PORT_HPP__

#include <boost/property_tree/ptree.hpp>

#include "channel.hpp"
#include "sync_command.hpp"

#include <cinttypes>


class RB110SerialPort:public Channel 
{
private:
  enum Constants
    {
      UNDEFINED_COMPORT = 255,
      UNDEFINED_BAUDRATE = -1
    };
public:
  RB110SerialPort( std::string name , boost::property_tree::ptree & pt);
  virtual int Open( void );
  virtual int Close( void );
  void SetBaudrate( std::string baud ) { baudrate = baud; };
  std::string GetBaudrate( void ) { return baudrate; };
  virtual int CommandTxAndRx( uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[], unsigned int const replyLength );

private:
  std::string baudrate;
  unsigned int comPort;
};


#endif /* __RB110_SERIAL_PORT_HPP__ */
