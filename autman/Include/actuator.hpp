/* 
 * Actuator Class
 * encapsulates an actuator of our robot.
 */

#ifndef __ACTUATOR_HPP__
#define __ACTUATOR_HPP__

#include "trajectory.hpp"

#include <string>
#include <json/value.h>
#include <boost/property_tree/ptree.hpp>
#include <iostream>

class Interface;

class Robot;

class Actuator
{
	friend class Robot;

	// Type Defintions
public:
	enum Type
	{
		GENERIC_RC_SERVO_NO_FEEDBACK,
		ROBOTIS_SERVO_RX_28,
		ROBOTIS_SERVO_RX_64,
		ROBOTIS_SERVO_MX_106,
		ROBOTIS_SERVO_MX_64,
		ROBOTIS_SERVO_MX_28,
		INVALID
	};

	enum MotorState
	{
		MOTOR_INVALID = -1,
		MOTOR_ON,
		MOTOR_OFF
	};

	// Methods
public:
	static Actuator *factory(std::string name, boost::property_tree::ptree &pt, Robot const *robot);

	Interface *GetInterface(void) const
	{ return interface; };

	std::string GetName(void) const
	{ return name; };

	int GetID(void) const
	{ return id; };

	virtual int Open(void);

	virtual int ProcessCommand(Json::Value &json);

	virtual int SendPositionAndSpeedMessage(float angle, float speed);

	virtual int ReadState(Json::Value &jsonResult);

	virtual int ReadFullState(Json::Value &jsonResult);

	virtual int ReadGains(Json::Value &jsonResult);

	virtual int SetMotorState(enum MotorState state);

	virtual enum MotorState GetMotorState(void) const;

	virtual int PowerOn(void)
	{ return SetMotorState(MOTOR_ON); };

	virtual int PowerOff(void)
	{ return SetMotorState(MOTOR_OFF); };

	virtual enum Type GetType(void) const
	{ return type; };

	int ParseActuatorMotorStateMessage(Json::Value &json, enum MotorState *state);

	std::vector<Trajectory> *GetTrajectories(void)
	{ return &trajectories; };

	virtual int AddTrajectory(Trajectory const &traj);

	virtual void ClearTrajectories(void)
	{ trajectories.clear(); };

	virtual Trajectory const *FindTrajectoryByName(std::string const &name) const;

protected:
	Actuator(std::string name, Interface *interface, boost::property_tree::ptree &pt);

	enum Type type;

protected:
	int id;
	Interface *interface;
	std::vector<Trajectory> trajectories;
	std::string name;
	bool opened;
};

#endif /* __ACTUATOR_HPP__ */

