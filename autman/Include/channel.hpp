#ifndef __CHANNEL_HPP__
#define __CHANNEL_HPP__

#include <string>
#include <cinttypes>
#include <boost/property_tree/ptree.hpp>
#include <mutex>
#include "sync_command.hpp"
#include "dynamixel_interface.hpp"
#include <iostream>

class Channel
{
public:
	Channel(std::string name, boost::property_tree::ptree &pt);

	enum ChannelType
	{
		INVALID_CHANNEL = 0,
		UNIX_SERIAL_PORT,
		RB110_SERIAL_PORT
	};

	Channel *factory(std::string name, boost::property_tree::ptree &pt);

	virtual int Open(void);

	virtual int Close(void);

	void SetInterface(std::string diface)
	{
		interface = diface;
	};

	virtual int CommandTxAndRx(uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[],
							   unsigned int const replyLength) = 0;

	virtual std::string GetName(void) const
	{ return name; };

	virtual int readDataWithLock(uint8_t *data, unsigned int length, unsigned int timeout = 50) = 0;

	virtual int readData(uint8_t *data, unsigned int length, unsigned int timeout = 50) = 0;

	virtual int writeData(uint8_t const *data, unsigned int length) = 0;

	virtual int writeDataWithLock(uint8_t const *data, unsigned int length) = 0;

	virtual void dumpPortSettings();

	virtual void lock() = 0;

	virtual void unlock() = 0;

	void Reset();

	virtual bool scanOpen(std::string prefix, unsigned int from, unsigned int to);
	virtual bool scanOpen(unsigned int from, unsigned int to);

public:
	bool opened;
	enum ChannelType type;
	std::string name;
protected:
	std::string interface;

	virtual int CloseWithoutLock();
	virtual int OpenWithoutLock();
};

/**
 * Class helper to ensure safety on locking channels.
 */
class ChannelLockGuard
{
private:
	Channel &channel;
public:
	ChannelLockGuard(Channel &channel);
	~ChannelLockGuard();
};

#endif

/* __CHANNEL_HPP__ */
