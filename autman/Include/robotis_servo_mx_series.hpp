/*
 * Actuator Class for Robotis servo mx series
 */

#ifndef __ROBOTIS_SERVO_MX_SERIES_HPP__
#define __ROBOTIS_SERVO_MX_SERIES_HPP__

#include "robotis_servo.hpp"
#include "dynamixel_protocol.hpp"
#include <boost/property_tree/ptree.hpp>

#include <cinttypes>

class DynamixelInterface;

class RobotisServoMXSeries
	: public RobotisServo
{
	friend class Actuator; // Needed for Actuator::factory

public:
	enum MaxValues
	{
		MAX_TICKS = 4096,
		MAX_ISPEED = 2047
	};

	enum RobotisProtocolCommands
	{
		HEADER = 0xff,
		BROADCAST_ID = 0xfe,

		CMD_PING = 0x01,
		CMD_READ_DATA = 0x02,
		CMD_WRITE_DATA = 0x03,
		CMD_SYNC_WRITE = 0x83
	};

	enum RobotisProtocolIndexes
	{
		HEADER1_INDEX = 0,
		HEADER2_INDEX = 1,

		ID_INDEX = 2,

		LENGTH_INDEX = 3,

		INSTRUCTION_INDEX = 4,
		ERROR_INDEX = 4,

		READ_ANSWER_CONTENT_INDEX = 5
	};

	// This is the mx series table which uses pid and
	//seems to support all mx series.

	enum RobotisProtocolAddresses
	{
		// EEPROM
			MODEL_NUMBER_L = 0,
		MODEL_NUMBER_H = 1,

		FIRMWARE_VERSION = 2,

		ID = 3,

		BAUD_RATE = 4,

		RETURN_DELAY_TIME = 5,

		CW_ANGLE_LIMIT_L = 6,
		CW_ANGLE_LIMIT_H = 7,

		CCW_ANGLE_LIMIT_L = 8,
		CCW_ANGLE_LIMIT_H = 9,

		DRIVE_MODE = 10,

		LIMIT_TEMPERATURE = 11,

		LOW_LIMIT_VOLTAGE = 12,

		HIGH_LIMIT_VOLTAGE_ = 13,

		MAX_TORQUE_L = 14,
		MAX_TORQUE_H = 15,

		STATUS_RETURN = 16,

		ALARM_LED = 17,

		ALARM_SHUTDOWN = 18,

		// RAM
			TORQUE_ENABLE = 24,

		LED = 25,

		D_GAIN = 26,

		I_GAIN = 27,

		P_GAIN = 28,

		GOAL_POSITION_L = 30,
		GOAL_POSITION_H = 31,

		MOVING_SPEED_L = 32,
		MOVING_SPEED_H = 33,

		TORQUE_LIMIT_L = 34,
		TORQUE_LIMIT_H = 35,

		PRESENT_POSITION_L = 36,
		PRESENT_POSITION_H = 37,

		PRESENT_SPEED_L = 38,
		PRESENT_SPEED_H = 39,

		PRESENT_LOAD_L = 40,
		PRESENT_LOAD_H = 41,

		PRESENT_VOLTAGE = 42,

		PRESENT_TEMPERATURE = 43,

		REGISTERED = 44,

		MOVING = 46,

		LOCK = 47,

		PUNCH_L = 48,
		PUNCH_H = 49,

		CURRENT_L = 68,
		CURRENT_H = 69,

		TORQUE_CONTROL_MODE = 70,

		GOAL_TORQUE_L = 71,
		GOAL_TORQUE_H = 72,

		GOAL_ACCELERATION = 73

	};

public:
	virtual int SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed);

	virtual int SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
										   unsigned int gainI, unsigned int gainP) override;

	virtual unsigned int AngleRadiansToPositionTicks(float angle);

	virtual float AnglePositionTicksToRadians(unsigned int ticks);
public:
	RobotisServoMXSeries(std::string name, DynamixelInterface *interface, boost::property_tree::ptree &pt);

protected:
	virtual int ReadPositionTicksAndISpeedAndILoad(servos::dynamixel::mx::ReadStatePackage *resultPackage,
		 protocols::dynamixel1::ErrorByte *error);

	virtual int ReadFullData(servos::dynamixel::mx::ReadFullStatePackage *resultPackage,
		 protocols::dynamixel1::ErrorByte *error);

	virtual int SendGains(servos::dynamixel::mx::Gains *params);

	virtual int ReadGains(servos::dynamixel::mx::Gains *params, protocols::dynamixel1::ErrorByte *error);

	virtual int SendPositionTicksISpeedGains(servos::dynamixel::mx::PositionSpeedGains *params);

};

#endif /* __ROBOTIS_SERVO_MX_SERIES_HPP__ */

