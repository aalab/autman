/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 05, 2015
 */

#ifndef MOTIONLIBRARY_ROBOT_EXCEPTIONS_HPP
#define MOTIONLIBRARY_ROBOT_EXCEPTIONS_HPP

#include <json/value.h>
#include <string>

namespace exceptions
{
	class CommandException:
		public std::exception
	{
	private:
		std::string errCode;
		std::string errMsg;

	public:
		CommandException(std::string errCode, std::string errMsg);
		~CommandException() noexcept;

		virtual std::string &code();
		virtual std::string &msg();
	};

	namespace robot
	{
		class NameException
			: public CommandException
		{
		public:
			NameException(std::string name);
		};
	}
}

Json::Value & operator<<(Json::Value &result, exceptions::CommandException &e);

#endif //MOTIONLIBRARY_ROBOT_EXCEPTIONS_HPP
