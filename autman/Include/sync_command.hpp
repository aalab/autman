/*
 * SyncCommand Class
 */

#ifndef __SYNC_COMMAND_HPP__
#define __SYNC_COMMAND_HPP__

#include <cinttypes>

struct __attribute__((__packed__)) SyncCommand
{
public:
  uint8_t id;

  uint16_t ticks;
  uint16_t ispeed;
  /*
  uint8_t ticks_L;
  uint8_t ticks_H;
  uint8_t ispeed_L;
  uint8_t ispeed_H;
  */
};

#endif /* __SYNC_COMMAND_HPP__ */
