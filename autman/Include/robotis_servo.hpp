/*
 * Actuator Class for Robotis servo
 */

#ifndef __ROBOTIS_SERVO_HPP__
#define __ROBOTIS_SERVO_HPP__

#include "servo.hpp"
#include "dynamixel_protocol.hpp"

#include <boost/property_tree/ptree.hpp>

#include <cinttypes>

#include <json/value.h>

class DynamixelInterface;

class Channel;

#ifndef override
#define override
#endif

class RobotisServo : public Servo
{
	friend class Actuator; // Needed for Actuator::factory
public:
	enum RobotisProtocol
	{
		FULL_HEADER = 0xffff,
		HEADER = 0xff,

		BROADCAST_ID = 0xfe,

		CMD_READ_DATA = 0x02,
		CMD_WRITE_DATA = 0x03,
		CMD_REG_WRITE = 0x04,
		CMD_ACTION = 0x05,
		CMD_RESET = 0x06,
		CMD_SYNC_WRITE = 0x83,

		MODEL_NUMBER_L = 0x00,
		MODEL_NUMBER_H = 0x01,
		VERSION_FIRMWARE = 0x02,

		STATUS_RETURN_LEVEL = 0x10,
		TORQUE_ENABLE = 0x18,
		GOAL_POSITION_L = 0x1e,
		PRESENT_POSITION_L = 0x24,

		NO_ERROR = 0x00
	};

	enum RobotisErrorBits
	{
		ERROR_NOERROR = 0x00,
		ERROR_INSTRUCTION = 0x40,
		ERROR_OVERLOAD = 0x20,
		ERROR_CHECKSUM = 0x10,
		ERROR_RANGE = 0x08,
		ERROR_OVERHEATING = 0x04,
		ERROR_ANGLELIMIT = 0x02,
		ERROR_INPUTVOLTAGE = 0x01
	};

	enum RobotisStatusReturnLevel
	{
		RESPONSE_INVALID = -1,
		RESPONSE_NONE = 0x00,
		RESPONSE_READ = 0x01,
		RESPONSE_ALL = 0x02
	};

	unsigned int gainP;
	unsigned int gainI;
	unsigned int gainD;

	int TorqueOn(void);

	virtual int SendPositionAndSpeedMessage(float angle, float speed);


	virtual int SendPositionAndSpeedMessage(float angle, float speed, unsigned int gainD, unsigned int gainI,
		unsigned int gainP) override;

	virtual int SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed);

	virtual int SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
		unsigned int gainI, unsigned int gainP);

	virtual int ProcessCommand(Json::Value &json) override;

	virtual unsigned int AngleRadiansToPositionTicks(float angle);

	static unsigned int SpeedToISpeed(float speed);

	virtual int ReadPositionTicksAndISpeedAndILoad(servos::dynamixel::mx::ReadStatePackage *resultPackage,
												   protocols::dynamixel1::ErrorByte *error);

	virtual int ReadFullData(servos::dynamixel::mx::ReadFullStatePackage *resultPackage, protocols::dynamixel1::ErrorByte *error);

	virtual int ReadState(Json::Value &jsonResult);

	virtual int ReadFullState(Json::Value &jsonResult);

	virtual int ReadGains(Json::Value &jsonResult) override;

	virtual int ReadGains(servos::dynamixel::mx::Gains *gains, protocols::dynamixel1::ErrorByte *error);

	virtual float AnglePositionTicksToRadians(unsigned int ticks);

	static float ISpeedToSpeed(unsigned int ispeed);

	static float ILoadToLoad(unsigned int iload);

	virtual int SetMotorState(enum MotorState state) override;

	virtual Channel *GetChannel(void) const;

	enum RobotisStatusReturnLevel ReadStatusReturnLevel(void) const;

	virtual enum MotorState GetMotorState(void) const override;

	virtual int ReadRegisters(uint8_t startRegister, uint8_t *reply, uint8_t numRegisters) const;

	virtual int WriteRegisters(uint8_t startRegister, uint8_t *registers, uint8_t numRegisters);

	virtual int Open(void);

	static Actuator *factory(std::string name, std::string type, DynamixelInterface *diface,
							 boost::property_tree::ptree &pt);

	static void byteToStructError(uint8_t errorByte, protocols::dynamixel1::ErrorByte *error);

protected:
	RobotisServo(std::string name, DynamixelInterface *interface, boost::property_tree::ptree &pt);

protected:
	bool echoOn;
	float defaultHome;
	float defaultMin;
	float defaultMax;

	unsigned int defaultGainP;
	unsigned int defaultGainI;
	unsigned int defaultGainD;

	float currentPosition;
	static float maxSpeed;
private:
	enum RobotisStatusReturnLevel statusReturnLevel;

	void init_error(protocols::dynamixel1::ErrorByte *err);
};

#endif /* __ROBOTIS_SERVO_HPP__ */

