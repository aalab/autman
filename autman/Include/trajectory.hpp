#ifndef __TRAJECTORY_HPP__
#define __TRAJECTORY_HPP__

#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <json/value.h>

class Trajectory
{
public:
	// Trajectory( );
	Trajectory(std::string name, std::string joint);

	Trajectory(std::string name, std::string joint, boost::property_tree::ptree &pt);

	void Clear();

	void Add(float angle, float time);

	std::string ToCommandString(void) const;

	void Print(std::ostream &os) const;

	int Parse(std::string jointName, Json::Value &json);

	float Interpolate(float percentage) const;

	std::string GetName(void) const
	{ return name; };

	friend std::ostream &operator<<(std::ostream &os, Trajectory const &t);

	unsigned int GetSize() const;

private:
	std::string name;
	std::string joint;
	std::vector<float> angles; // in radians
	std::vector<float> times;  // in percentages
	float totalTime;
};

std::ostream &operator<<(std::ostream &os, Trajectory const &t);

#endif /* __TRAJECTORY_HPP__ */
