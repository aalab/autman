#ifndef __CONFIG_H__
#define __CONFIG_H__

#define VERSION_MAJOR 0
#define VERSION_MINOR 25

#define CONFIG_DIR "/home/autman/old_ws/autman/autman/Configs"
#define DEFAULT_CONFIG_FILE "polaris.info"
#define DEF_CONFIG_FILE "polaris.info"
#define DEFAULT_ROBOT_SERVER_PORT 1313

#endif /* __CONFIG_H__ */
