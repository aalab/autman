/*
 * Various Utility classes
 */

#ifndef __UTILITY_HPP__
#define __UTILITY_HPP__

#include <boost/algorithm/string.hpp>

#include <cmath>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <string>

class Utility
{
public:
  static std::string ReplaceChars( std::string str, std::string repl, char newChar );
  static float NormalizeAngle( float angle );
 
  static void Trim(std::string & s)
  {
    boost::algorithm::trim(s);
  } 
  static void TrimRight(std::string & s)
  {
    boost::algorithm::trim_right(s);
  } 
  
  template<typename T>
  static T Clamp(T const min, T val, T const max)
  {
    if ( val < min )
      {
	val = min;
      }
    if ( val > max )
      {
	val = max;
      }
    return val;
  }

  static float DegreeToRadians( float deg )
  {
    return deg/180.0 * M_PI;
  }

  static float RadiansToDegree( float rad )
  {
    return rad/M_PI * 180.0;
  }
};


#endif /* __UTILITY_HPP__ */
