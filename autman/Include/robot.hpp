#ifndef __ROBOT_HPP__
#define __ROBOT_HPP__

#include "actuator.hpp"
#include "motion.hpp"
#include "motion_sequence.hpp"

#include <string>
#include <list>
#include <sstream>
#include "../Source/autgait.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#include <json/value.h>

#include <mutex>

class KinematicsHumanoid;

class Channel;

class Interface;

class UDPRobotServer;

class Robot
{
public:
	Robot(std::string fname);

	std::unique_ptr<UnixSerialPort> serialPort;

	std::unique_ptr<AUTGait> autGait;

	inline std::string GetName(void) const
	{ return name; };

	void LoadConfiguration(std::string filename);

	void LoadConfiguration(boost::property_tree::ptree &pt);

	virtual int Open();

	virtual int Close();

	int AddInterface(Interface *new_iface);

	int AddChannel(Channel *channel);

	int ReadySyncCommand(void);

	int SendSyncCommand(void);

	Interface *FindInterfaceByName(std::string name) const;

	Actuator *FindActuatorByName(std::string name) const;

	Channel *FindChannelByName(std::string name) const;

	int NumberOfActuators(void) const;

	int ReadState(Json::Value &params, Json::Value &jsonResult) const;

	int ReadFullState(Json::Value &params, Json::Value &jsonResult) const;

	int PowerOff(void)
	{ return SetMotorState(Actuator::MOTOR_OFF); };

	int PowerOn(void)
	{ return SetMotorState(Actuator::MOTOR_ON); };

	int SetMotorState(enum Actuator::MotorState onoff);

	void SetInterface(std::string iface)
	{ interface = iface; };

	int ProcessCommand(Json::Value &json, Json::Value &result);

	void StartServer(bool foreground);

	int StartMotion(std::string tName, unsigned long long int cycleTimeUSec, unsigned int nCycles,
					unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed);

	int StartMotion(float dx, float dy, float dtheta, unsigned long long int cycleTimeUSec, unsigned int nCycles,
					unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed);

	int StartMotionSequence(std::vector<float> &dx, std::vector<float> &dy, std::vector<float> &dtheta,
							std::vector<unsigned long long int> &cycleTimeUSec, std::vector<unsigned int> &nCycles,
							unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed);

	std::string &GetState(void)
	{ return state; };

	void RunMotion(std::unique_ptr<Motion> &motion, unsigned long long int cycleTimeUSec, unsigned int nCycles,
		unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed);

	static void RunMotionSequenceTrampoline(Robot *robot, MotionSequence *motionSeq,
		unsigned long int entryTimeUSec, float motionSpeed, float motionEntrySpeed);

	void RunMotionSequence(MotionSequence *motionSeq, unsigned long int entryTimeUSec, float motionSpeed,
						   float motionEntrySpeed);

	std::vector<Interface *> const &GetInterfaces(void) const
	{ return interfaces; };

	void getHeadMeasurements(std::vector<float> &returnVals) const;

	void getArmMeasurements(std::vector<float> &returnVals) const;

	void getLegMeasurements(std::vector<float> &returnVals) const;

	int setHeadMeasurements(std::vector<float> &newVals);

	int setArmMeasurements(std::vector<float> &newVals);

	int setLegMeasurements(std::vector<float> &newVals);

protected:
	int InternalProcessCommand(Json::Value &command, Json::Value &jsonResult);
private:
	bool isNumber(std::string &potentialNumber);

	AUTGaitEulerData eulerData;

	std::string readNumsToVector(std::vector<float> &receiveVector, std::istringstream &ins);

	std::string readNumsToVector(std::vector<unsigned int> &receiveVector, std::istringstream &ins);

	std::string readNumsToVector(std::vector<unsigned long long int> &receiveVector, std::istringstream &ins);

	std::string name;
	int port;
	std::string interface;
	bool opened;
	std::string system;
	std::vector<Channel *> channels;
	std::vector<Interface *> interfaces;
	boost::asio::io_service io_service;
	UDPRobotServer *server;
	std::string state;
	float defaultMotionSpeed;
	float defaultMotionEntrySpeed;
	KinematicsHumanoid *kin;

	float torsoNeckDistanceX;
	float torsoNeckDistanceZ;
	float neckTransversalToNeckLateralZ;
	float neckLateralToCameraLateralZ;
	float torsoShoulderDistanceY;
	float torsoShoulderDistanceZ;
	float shoulderLateralToShoulderFrontalY;
	float shoulderFrontalToElbowLateralY;
	float elbowLateralToHandFrontalY;
	float elbowLateralToHandFrontalZ;
	float torsoHipDistanceY;
	float torsoHipDistanceZ;
	float hipTransversalToHipFrontalZ;
	float hipLateralToKneeLateralZ;
	float kneeLateralToAnkleLateralZ;
	float ankleLateralToFootFrontalZ;

	unsigned int defaultGainP;
	unsigned int defaultGainI;
	unsigned int defaultGainD;

	volatile enum Motion::MotionTerminationRequest terminateMotion;

	MotionSequence *currentMotionSequence;

	std::mutex nextMotionSeqMutex;
	MotionSequence *nextMotionSeq;

	int ReadGains(Json::Value &params, Json::Value &result);

	void startReadingIMU();
public:
	void readIMU();

	static void RunReadIMUTrampoline(Robot *robot);
};

#endif /* __ROBOT_HPP__ */
