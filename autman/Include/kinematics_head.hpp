#ifndef __KINEMATICS_HEAD_HPP__
#define __KINEMATICS_HEAD_HPP__

#include "kinematics_object.hpp"

#define NUM_HEAD_SERVOS 2

class KinematicsHead : public KinematicsObject {
    public:
        KinematicsHead(Robot const * robot);
        ~KinematicsHead();
        
        //Calculates the angles in radians required of the servos for the head to reach a given position
        //returnAngles - vector where the calculated angles will be stored
        //params - vector that must contain exactly 3 values: the desired x value, followed by the desired y value, followed by the desired z value
        //buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
        //returns 0 on success, -1 on error
        virtual int inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle = DEFAULT_KINEMATIC_BUFFER_ANGLE) const;
        
        enum head_servo_t {
            NECK_TRANSVERSAL = 0,
            NECK_LATERAL = 1
        };
};

#endif
