#ifndef __GENERIC_RC_SERVO_NO_FEEDBACK_HPP__
#define __GENERIC_RC_SERVO_NO_FEEDBACK_HPP__

#include "servo.hpp"

#include <string>

#include <boost/property_tree/ptree.hpp>

class Interface;
class Robot;

class GenericRCServoNoFeedback : public Servo
{
  friend class Actuator;

  // Methods
public:
  virtual int ProcessCommand( std::string const & pos );
  virtual int SetMotorState( enum MotorState state );
  virtual int ReadState( std::string & pos );
  virtual int SendPositionAndSpeedMessage( float angle, float speed );
  static float PositionTicksToRadians( int long ticks );

protected:
  GenericRCServoNoFeedback( std::string name, Interface * interface, boost::property_tree::ptree & pt );

private:
  float defaultHome;
  float defaultMin;
  float defaultMax;

  int long savedTicks;
};

#endif /* __GENERIC_RC_SERVO_NO_FEEDBACK_HPP__ */
