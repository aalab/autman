/*
 * Actuator Class for Robotis RX 64 servo
 */

#ifndef __ROBOTIS_SERVO_MX_106_HPP__
#define __ROBOTIS_SERVO_MX_106_HPP__

#include "robotis_servo_mx_series.hpp"

#include <boost/property_tree/ptree.hpp>

class DynamixelInterface;

class RobotisServoMX106 : public RobotisServoMXSeries
{
  friend class Actuator;

public:
  float DefaultSpeed() const { return defaultSpeed; };
  RobotisServoMX106( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

private:
    float defaultSpeed;
};

#endif /* __ROBOTIS_SERVO_MX_106_HPP__ */

