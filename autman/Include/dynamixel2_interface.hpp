/*
 * Interfaces Class for Dynamixel Servos on Serial Line
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#ifndef __DYNAMIXEL2_INTERFACE_HPP__
#define __DYNAMIXEL2_INTERFACE_HPP__

#include "dynamixel_interface.hpp"

class Dynamixel2Interface : public DynamixelInterface
{
public:
	Dynamixel2Interface(std::string name, boost::property_tree::ptree pt, Robot const *robot);
protected:

	virtual unsigned int prepareHeader(uint8_t *buffer, unsigned int bufferSize, uint8_t id) override;

	virtual unsigned int prepareBufferWriteRegisters(
		uint8_t *buffer, unsigned int bufferSize, uint8_t id, uint8_t startRegister
	) override;

	virtual unsigned int prepareBufferReadRegisters(
			uint8_t *buffer, unsigned int bufferSize, uint8_t id, unsigned int startRegister, unsigned int numRegisters
	) override;

	virtual unsigned int prepareBufferSyncCommand(uint8_t *buffer, unsigned int bufferSize, unsigned int startAddress, unsigned int dataLength) override;

	virtual unsigned int calculateBufferLength(unsigned int packetSize) override;


	virtual unsigned int calculateReadRegisterBufferLength(unsigned int numRegisters) override;

	virtual void calculateBufferChecksum(uint8_t *buffer, unsigned int bufferLength) override;

	static uint16_t crcTable[256];

	static uint16_t calculateChecksum(uint16_t crc_accum, uint8_t *buffer, unsigned int bufferSize);
};

#endif /* __DYNAMIXEL2_INTERFACE_HPP__ */
