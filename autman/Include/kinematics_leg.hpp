#ifndef __KINEMATICS_LEG_HPP__
#define __KINEMATICS_LEG_HPP__

#include "kinematics_object.hpp"

#define NUM_LEG_SERVOS 6

class KinematicsLeg : public KinematicsObject {
    public:
        KinematicsLeg(Robot const * robot, bool isLeft, bool useFlatFoot = true);
        ~KinematicsLeg();
        
        //Calculates the angles in radians required of the servos for the head to reach a given position
        //returnAngles - vector where the calculated angles will be stored
        //params - vector that must contain exactly 4 values: the desired x value, followed by the desired y value, followed by the desired z value, followed by the desired hip transversal angle
        //buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
        //returns 0 on success, -1 on error
        virtual int inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle = DEFAULT_KINEMATIC_BUFFER_ANGLE) const;
        
        enum leg_servo_t {
            HIP_TRANSVERSAL = 0,
            HIP_FRONTAL = 1,
            HIP_LATERAL = 2,
            KNEE_LATERAL = 3,
            ANKLE_LATERAL = 4,
            ANKLE_FRONTAL = 5
        };
        
    private:
		bool isLeft;
		bool useFlatFoot; //if true, take desired position to be the bottom of the foot and assume the foot is flat relative to the ground
		                  //if false, take desired position to be the position of the ankle lateral motor
};

#endif
