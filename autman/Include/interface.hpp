/*
 * Interfaces Class
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#ifndef __INTERFACES_HPP__
#define __INTERFACES_HPP__

#include "actuator.hpp"
#include <boost/property_tree/ptree.hpp>

#include <cinttypes>

#include <vector>

#include <json/value.h>

class Robot;

class Channel;

class Interface
{
public:
	enum Type
	{
		RC_SERVO_GPIO,
		DYNAMIXEL,
		DYNAMIXEL2,
		INVALID
	};


public:
	virtual int Open(void);

	virtual int Close(void);

	std::string GetName(void) const
	{ return name; };

	int AddActuator(Actuator *new_act);

	enum Type GetType(void)
	{ return type; };

	virtual int ReadySyncCommand(void)
	{ return -1; };

	virtual int SendSyncCommand(void)
	{ return -1; };

	Actuator *FindActuatorByName(std::string name);

	virtual int PulseMotion(std::string mName, float percentage, float motionSpeed);

	virtual int NumberOfActuators(void) const;

	virtual int ReadState(Json::Value &jsonCommand, Json::Value &jsonResult);

	virtual int ReadFullState(Json::Value &jsonCommand, Json::Value &jsonResult);

	/**
	 * Read a entire package from the Channel and returns the length of it. If nothing was found it returns negative
	 * values as errors.
	 *
	 * In all case of errors, the content of the buffer is undefined.
	 *
	 * Error table:
	 * -1: No package found.
	 * -2: Package bigger than the buffer (the entire package will be skipped).
	 */
	virtual int readPackage(uint8_t buffer[], const unsigned int bufferLength) const
	{ return -1; };

	virtual int SetMotorState(enum Actuator::MotorState onoff);

	virtual int PowerOn(void)
	{ return SetMotorState(Actuator::MOTOR_ON); };

	virtual int PowerOff(void)
	{ return SetMotorState(Actuator::MOTOR_OFF); };

	virtual Channel *GetChannel(void) const
	{ return nullptr; };

	std::vector<Actuator *> const &GetActuators(void)
	{ return actuators; };

	virtual Actuator *factoryActuatorAuto(std::string name, boost::property_tree::ptree &pt)
	{ return nullptr; };
protected:
	Interface(std::string name, boost::property_tree::ptree pt, Robot const *robot);

protected:
	bool opened;
	enum Type type;
	std::vector<Actuator *> actuators;

private:
	std::string name;

	class Robot const *robot;
};

#endif /* __INTERFACES_HPP__ */
