/*
 * Interfaces Class for Dynamixel Servos on Serial Line
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#ifndef __DYNAMIXEL_INTERFACE_HPP__
#define __DYNAMIXEL_INTERFACE_HPP__

#include "interface.hpp"
#include "sync_command.hpp"
#include "robotis_servo.hpp"

#include <cinttypes>

class Channel;

class Robot;

class Actuator;

class DynamixelInterface : public Interface
{
	friend class Interface;

public:
	enum Constants
	{
		UNDEFINED_COMPORT = 255,
		UNDEFINED_BAUDRATE = -1
	};

public:
	DynamixelInterface(std::string name, boost::property_tree::ptree pt, Robot const *robot);

	virtual int Open(void);

	virtual int Close(void);

	void SetInterface(std::string iface)
	{ interface = iface; };

	std::string GetInterface(void)
	{ return interface; };

	virtual int CommandTxAndRx(uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[],
							   unsigned int const replyLength);

	int AddSyncCommand(unsigned int id, unsigned int gainD, unsigned int gainI, unsigned int gainP, unsigned int ticks,
		unsigned int ispeed);

	int ReadySyncCommand(void);

	int SendSyncCommand(void);

	bool IsSyncCommandReady(void)
	{ return syncCommandReady; };

	virtual Channel *GetChannel(void) const
	{ return channel; };

	virtual Actuator *factoryActuatorAuto(std::string name, boost::property_tree::ptree &pt);

	virtual int ReadRegisters(uint8_t id, uint8_t startRegister, uint8_t *reply, uint8_t numRegisters);

	virtual int WriteRegisters(
		uint8_t id, uint8_t startRegister, uint8_t *registers, uint8_t numRegisters,
		enum RobotisServo::RobotisStatusReturnLevel statusReturnLevel
	);

	int OpenChannel(void);

	virtual int readPackage(uint8_t buffer[], const unsigned int bufferLength) const;

	static uint8_t CalculateCheckSum(uint8_t buffer[], unsigned int len);

protected:
	virtual unsigned int prepareHeader(uint8_t *buffer, unsigned int bufferSize, uint8_t id);
	virtual unsigned int calculateBufferLength(unsigned int packetSize);
	virtual unsigned int calculateReadRegisterBufferLength(unsigned int numRegisters);
	virtual unsigned int prepareBufferWriteRegisters(
			uint8_t *buffer, unsigned int bufferSize, uint8_t id, uint8_t startRegister
	);
	virtual unsigned int prepareBufferReadRegisters(
			uint8_t *buffer, unsigned int bufferSize, uint8_t id, unsigned int startRegister, unsigned int numRegisters
	);
	virtual unsigned int prepareBufferSyncCommand(
		uint8_t *buffer, unsigned int bufferLength, unsigned int startAddress, unsigned int dataLength
	);
	virtual void calculateBufferChecksum(uint8_t *buffer, unsigned int bufferLength);
	virtual int readRegistersReply(uint8_t *treply, unsigned int treplySize, uint8_t *reply, uint8_t id, uint8_t numRegisters);

	bool syncCommandReady;
	protocols::dynamixel1::SyncWriteGainsPosition syncCommands[256];
	unsigned int syncOut;
private:
	std::string interface;
	Channel *channel;
};

#endif /* __DYNAMIXEL_INTERFACE_HPP__ */
