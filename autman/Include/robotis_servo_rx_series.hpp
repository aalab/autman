/*
 * Actuator Class for Robotis servo RX series
 */

#ifndef __ROBOTIS_SERVO_RX_SERIES_HPP__
#define __ROBOTIS_SERVO_RX_SERIES_HPP__

#include "robotis_servo.hpp"

#include <boost/property_tree/ptree.hpp>

#include <cinttypes>

class Interface;

class RobotisServoRXSeries : public RobotisServo
{
  friend class Actuator; // Needed for Actuator::factory

public:
  enum MaxValues
    {
      MAX_TICKS  = 1024,
      MAX_ISPEED = 1023
    };

  enum RobotisProtocolCommands
    {
      HEADER = 0xff,
      BROADCAST_ID = 0xfe,

      CMD_PING = 0x01,
      CMD_READ_DATA = 0x02,
      CMD_WRITE_DATA = 0x03,
      CMD_SYNC_WRITE = 0x83
    };

  
  
  enum RobotisProtocolAddresses
    {
      // EEPROM
      MODEL_NUMBER_L = 0,
      MODEL_NUMBER_H = 1,

      FIRMWARE_VERSION = 2,
      
      ID = 3,

      BAUD_RATE = 4,

      RETURN_DELAY_TIME = 5,

      CW_ANGLE_LIMIT_L = 6,
      CW_ANGLE_LIMIT_H = 7,

      CCW_ANGLE_LIMIT_L = 8,
      CCW_ANGLE_LIMIT_H = 9,

      LIMIT_TEMPERATURE = 11,

      LOW_LIMIT_VOLTAGE = 12,
      
      HIGH_LIMIT_VOLTAGE_= 13,

      MAX_TORQUE_L = 14,
      MAX_TORQUE_H = 15,

      STATUS_RETURN = 16,

      ALARM_LED = 17,

      ALARM_SHUTDOWN = 18,

      // RAM
      TORQUE_ENABLE = 24,

      LED = 25,

      CW_COMPLIANCE_MARGIN = 26,

      CCW_COMPLIANCE_MARGIN = 27,

      CW_COMPLIANCE_SLOPE = 28,
      
      CCW_COMPLIANCE_SLOPE = 29,

      GOAL_POSITION_L = 30,
      GOAL_POSITION_H = 31,

      MOVING_SPEED_L = 32,
      MOVING_SPEED_H = 33,

      TORQUE_LIMIT_L = 34,
      TORQUE_LIMIT_H = 35,

      PRESENT_POSITION_L = 36,
      PRESENT_POSITION_H = 37,

      PRESENT_SPEED_L = 38,
      PRESENT_SPEED_H = 39,

      PRESENT_LOAD_L = 40,
      PRESENT_LOAD_H = 41,

      PRESENT_VOLTAGE = 42,

      PRESENT_TEMPERATURE = 43,

      REGISTERED = 44,

      MOVING = 45,

      LOCK = 47,

      PUNCH_L = 48,
      PUNCH_H = 49
    };

public:
  virtual int SendPositionTicksAndISpeed(unsigned int ticks, unsigned int ispeed, unsigned int gainD,
    unsigned int gainI, unsigned int gainP);
  virtual unsigned int AngleRadiansToPositionTicks( float angle );
    
  virtual float AnglePositionTicksToRadians( unsigned int ticks );

public:
  RobotisServoRXSeries( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

protected:
 virtual int ReadPositionTicksAndISpeedAndILoad( int * ticks, int * speed, int * load );
 
};

#endif /* __ROBOTIS_SERVO_HPP__ */

