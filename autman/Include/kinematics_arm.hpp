#ifndef __KINEMATICS_ARM_HPP__
#define __KINEMATICS_ARM_HPP__

#include "kinematics_object.hpp"

#define NUM_ARM_SERVOS 3

class KinematicsArm : public KinematicsObject {
    public:
        KinematicsArm(Robot const * robot, bool isLeft);
        ~KinematicsArm();
        
        //Calculates the angles in radians required of the servos for the head to reach a given position
        //returnAngles - vector where the calculated angles will be stored
        //params - vector that must contain exactly 3 values: the sign of the desired x value, followed by the desired y value, followed by the desired z value
        //buffer_angle - the minimum angle in radians for the difference between the calculated angles and the absolute limiting angles of the servos
        //returns 0 on success, -1 on error
        virtual int inverseKinematics(std::vector<float>& returnAngles, const std::vector<float>& params, float buffer_angle = DEFAULT_KINEMATIC_BUFFER_ANGLE) const;
        
        enum arm_servo_t {
            SHOULDER_LATERAL = 0,
            SHOULDER_FRONTAL = 1,
            ELBOW_LATERAL = 2
        };
        
    private:
		bool isLeft;
};

#endif
