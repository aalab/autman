//-----------------------------------------
// PROJECT: MotionController
// AUTHOR : POSIX - Jacky Baltes <jacky@cs.umanitoba.ca>
//          Windows - Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef __UNIX_SERIAL_PORT_HPP__
#define __UNIX_SERIAL_PORT_HPP__


//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#ifdef _WIN32
#include <windows.h>
#else

#include "channel.hpp"

#include <termios.h>
#include <cinttypes>
#include <mutex>

#include <boost/property_tree/ptree.hpp>

#endif


//------------------------------------------------------------------------------
// CONSTANTS / TYPES
//------------------------------------------------------------------------------
#define SERIAL_MAX_DEVNAME 32
#ifdef _WIN32
typedef DWORD BAUDRATE_TYPE;
#else
typedef speed_t BAUDRATE_TYPE;
#endif


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class UnixSerialPort : public Channel
{
private:
	char _devname[SERIAL_MAX_DEVNAME]; // device name
	BAUDRATE_TYPE _baudrateSpeed; // baudrate
#ifdef _WIN32
    DCB _olddcb;
    HANDLE _hCommPort;
#else
	struct termios _oldtio;
	int _fd;
#endif

public:
	//------------------------------------------------------
	// PURPOSE: Construct serial port communicator
	// PARAMETERS:
	// REMARKS:
	//------------------------------------------------------
	UnixSerialPort(std::string name, boost::property_tree::ptree &pt);

	//------------------------------------------------------
	// PURPOSE: Destruct serial port communicator
	// PARAMETERS:
	// REMARKS:
	//------------------------------------------------------
	virtual ~UnixSerialPort(void);

	//------------------------------------------------------
	// PURPOSE: Flushes both data received but not read and data written but not transmitted
	// PARAMETERS:
	// REMARKS: flushRx - flushes data recieved only
	//          flushTx - flushes data written only
	//------------------------------------------------------
	virtual void flush(void);

	virtual void flushRx(void);

	virtual void flushTx(void);

	//------------------------------------------------------
	// PURPOSE: Check if this serial is valid
	// PARAMETERS:
	//   [bool]<OUT> True, if serial is valid; otherwise, false
	// REMARKS:
	//------------------------------------------------------
	virtual bool isValid(void) const;

	//------------------------------------------------------
	// PURPOSE: Read character from serial port
	// PARAMETERS:
	//   [pChar]<OUT> Pointer to character to receive the result
	//   [bool]<OUT> True, if succeeded; otherwise, false
	// REMARKS:
	//------------------------------------------------------
	virtual bool readChar(char *pChar);

	virtual int readDataWithLock(uint8_t *data, unsigned int length, unsigned int timeout = 50);

	//------------------------------------------------------
	// PURPOSE: Read data from serial port
	// PARAMETERS:
	//   [data]<OUT> Bytes buffer
	//   [length]<IN> Length of the array
	//   [timeout]<IN> Timeout of reading in seconds
	//   [int]<OUT> Number of received bytes
	// REMARKS:
	//------------------------------------------------------
	virtual int readData(uint8_t *data, unsigned int length, unsigned int timeout = 50);

	//------------------------------------------------------
	// PURPOSE: Write character onto serial port
	// PARAMETERS:
	//   [ch]<IN> Character to write
	//   [bool]<OUT> True, if succeeded; otherwise, false
	// REMARKS:
	//------------------------------------------------------
	virtual bool writeChar(char ch);

	//------------------------------------------------------
	// PURPOSE: Write data onto serial port
	// PARAMETERS:
	//   [data]<IN> Bytes to write
	//   [length]<IN> Length of the array
	//   [int]<OUT> Number of written bytes
	// REMARKS:
	//------------------------------------------------------
	virtual int writeDataWithLock(uint8_t const *data, unsigned int length);

	//------------------------------------------------------
	// PURPOSE: Write data onto serial port slowly
	// PARAMETERS:
	//   [data]<IN> Bytes to write
	//   [length]<IN> Length of the array
	//   [milliseconds]<IN> Milliseconds delay between each bytes
	//   [int]<OUT> Number of written bytes
	// REMARKS:
	//------------------------------------------------------
	virtual int writeSlowly(const char *data, int length, int milliseconds = 10);

	virtual int Open();

	virtual bool scanOpen(std::string prefix, unsigned int from, unsigned int to) override;

	virtual int Close();

	//------------------------------------------------------
	// PURPOSE: Write data onto serial port
	// PARAMETERS:
	//   [data]<IN> Bytes to write
	//   [length]<IN> Length of the array
	//   [int]<OUT> Number of written bytes
	// REMARKS:
	//------------------------------------------------------
	virtual int writeData(uint8_t const *data, unsigned int length);

	virtual int CommandTxAndRx(uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[],
							   unsigned int const replyLength);

protected:
	virtual void lock();

	virtual void unlock();

//------------------------------------------------------
	// PURPOSE: Retrieve baudrate of serial port
	// PARAMETERS:
	//   [BAUDRATE_TYPE]<OUT> Baudrate of serial port
	// REMARKS:
	//------------------------------------------------------
	virtual BAUDRATE_TYPE GetBaudrateSpeed(void) const
	{ return _baudrateSpeed; }

	//------------------------------------------------------
	// PURPOSE: Retrieve serial port device name
	// PARAMETERS:
	//   [const char*]<OUT> Serial port device name
	// REMARKS:
	//------------------------------------------------------
	virtual const char *deviceName(void) const
	{ return _devname; }

	virtual int OpenWithoutLock(void);

	//------------------------------------------------------
	// PURPOSE: Close opened serial port
	// PARAMETERS:
	// REMARKS:
	//------------------------------------------------------
	virtual int CloseWithoutLock(void);

	//------------------------------------------------------
	// PURPOSE: Open serial port
	// PARAMETERS:
	//   [devname]<IN> Device name to use
	//   [baudrate]<IN> Baudrate of serial port
	//   [bool]<OUT> True, if succeeded; otherwise, false
	// REMARKS:
	//------------------------------------------------------
	virtual bool OpenWithoutLock(const char *devname, BAUDRATE_TYPE baudrateSpeed);

public:
	//------------------------------------------------------
	// PURPOSE: Convert baudrate string into speed
	// PARAMETERS:
	//   [str]<IN> String to convert
	//   [BAUDRATE_TYPE]<OUT> Convert value
	// REMARKS:
	//------------------------------------------------------
	static BAUDRATE_TYPE spdBaudrate(const char *str);

	//------------------------------------------------------
	// PURPOSE: Convert baudrate speed into string
	// PARAMETERS:
	//   [speed]<IN> Speed to convert
	//   [const char*]<OUT> Convert string
	// REMARKS:
	//------------------------------------------------------
	static const char *strBaudrate(BAUDRATE_TYPE baudrateSpeed);

private:

	// no support to copy the object
	// UnixSerialPort( const UnixSerialPort& ) { }
	// virtual UnixSerialPort& operator=( const UnixSerialPort& ) { return *this; }
	std::mutex mutex;

public:
	std::string device;
	bool scanOpen(unsigned int from, unsigned int to);
protected:
	std::string baudrate;
	termios currentTIO;
	std::string lastPrefix;

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif /* __UNIX_SERIAL_PORT_HPP__ */
