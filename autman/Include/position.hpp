#ifndef __POSITION_HPP__
#define __POSITION_HPP__

#include <string>
#include <istream>

class Position
{
public:
  enum {
    NUM_JOINTS = 20
  };

  Position() : valid(false) { };

  int parseFromStream( std::istream & is );

  static int FindJointIndexByName( std::string jointName );
  int parseJointFromString( std::string const & joint );
  std::string ToCommandString( void ) const;
  bool IsValid( void ) { return valid; };
  void Print( void ) const;
  static std::string const jointNames[NUM_JOINTS];

  float joints[NUM_JOINTS];

private:
  bool valid;
};
#endif /* __POSITION_HPP__ */
