/*
 * Actuator Class for Robotis MX 64 servo
 */

#ifndef __ROBOTIS_SERVO_MX_64_HPP__
#define __ROBOTIS_SERVO_MX_64_HPP__

#include "robotis_servo_mx_series.hpp"

#include <boost/property_tree/ptree.hpp>

class DynamixelInterface;

class RobotisServoMX64 : public RobotisServoMXSeries
{
  friend class Actuator;

public:
  float DefaultSpeed() const { return defaultSpeed; };
  RobotisServoMX64( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

private:
    float defaultSpeed;
};

#endif /* __ROBOTIS_SERVO_MX_64_HPP__ */

