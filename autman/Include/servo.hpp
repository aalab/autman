/*
 * Servo Motor Class
 */

#ifndef __SERVO_HPP__
#define __SERVO_HPP__

#include "actuator.hpp"
#include "trajectory.hpp"

#include <string>

#include <boost/property_tree/ptree.hpp>

#ifndef override
#define override
#endif

class Servo : public Actuator
{
public:
	Servo(std::string name, Interface *interface, boost::property_tree::ptree &pt);

	float GetMin(void)
	{ return min; };

	float GetMax(void)
	{ return max; };

	virtual int ProcessCommand(Json::Value &json) override;

	virtual int ReadState(std::string &pos) override;

	virtual int SetMotorState(enum MotorState state) override;

	virtual enum MotorState GetMotorState(void) const override;

	virtual int ParsePositionAndSpeedMessage(Json::Value &json, float *pangle, float *pspeed, int *gainD,
											 int *gainI, int *gainP);

	virtual int SendPositionAndSpeedMessage(float angle, float speed);

	virtual int SendPositionAndSpeedMessage(float angle, float speed, int gainD, int gainI, int gainP);

protected:
	// These angles are all in radians. 180 deg/M_PI should be the centered position
	float min;
	float max;
	float home;
	bool hasFeedback;
};

#endif /* __SERVO_HPP__ */
