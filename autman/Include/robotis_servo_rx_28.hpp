/*
 * Actuator Class for Robotis RX 28 servo
 */

#ifndef __ROBOTIS_SERVO_RX_28_HPP__
#define __ROBOTIS_SERVO_RX_28_HPP__

#include "robotis_servo.hpp"
#include "robotis_servo_rx_series.hpp"
#include <boost/property_tree/ptree.hpp>

class DynamixelInterface;

class RobotisServoRX28 : public RobotisServoRXSeries
{
  friend class Actuator;


public:

  float DefaultSpeed() const { return defaultSpeed; };
  RobotisServoRX28( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

private:
  float defaultSpeed;
};

#endif /* __ROBOTIS_SERVO_RX_28_HPP__ */

