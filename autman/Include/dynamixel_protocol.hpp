/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 02, 2015
 */

#ifndef MOTIONLIBRARY_DYNAMIXELPROTOCOL_HPP
#define MOTIONLIBRARY_DYNAMIXELPROTOCOL_HPP


#include <stdint.h>

namespace servos
{
namespace dynamixel
{
namespace mx
{
struct __attribute__((__packed__)) PositionSpeed
{
	uint8_t position_LO;
	uint8_t position_HI;
	uint8_t speed_LO;
	uint8_t speed_HI;
};

struct __attribute__((__packed__)) Gains
{
	uint8_t d;
	uint8_t i;
	uint8_t p;
};

struct __attribute__((__packed__)) ReadStatePackage
{
	PositionSpeed positionSpeed;
	uint8_t load_LO;
	uint8_t load_HI;
	uint8_t voltage;
	uint8_t temperature;
};

struct __attribute__((__packed__)) ReadFullStatePackage
{
	uint8_t torque;
	uint8_t led;
	Gains gains;
	uint8_t padding1;
	uint8_t goalPosition_LO;
	uint8_t goalPosition_HI;
	uint8_t movingSpeed_LO;
	uint8_t movingSpeed_HI;
	uint8_t torqueLimit_LO;
	uint8_t torqueLimit_HI;
	PositionSpeed positionSpeed;
	uint8_t presentLoad_LO;
	uint8_t presentLoad_HI;
	uint8_t presentVoltage;
	uint8_t presentTemperature;
	uint8_t moving;
};

struct __attribute__((__packed__)) PositionSpeedGains
{
	Gains gains;
	uint8_t padding1;
	PositionSpeed positionSpeed;
};
}
}
}

namespace protocols
{
namespace dynamixel1
{
struct __attribute__((__packed__)) Header
{
	uint16_t header;
	uint8_t id;
	uint8_t length;
};

struct __attribute__((__packed__)) ReadRegisters
{
	Header header;
	uint8_t instruction;
	uint8_t registerAddress;
	uint8_t length;
	uint8_t checksum;
};

struct __attribute__((__packed__)) StatusPackageHeader
{
	Header header;
	uint8_t error;
	uint8_t data;
};

struct __attribute__((__packed__)) SyncWriteGainsPosition
{
	uint8_t id;
	servos::dynamixel::mx::PositionSpeedGains data;
};

struct ErrorByte
{
	bool noError;
	bool instruction;
	bool overload;
	bool checksum;
	bool range;
	bool overheating;
	bool anglelimit;
	bool inputvoltage;
};

void init(Header *header);

void init(ReadRegisters *command, uint8_t startAddress, uint8_t readDataLength);

uint8_t checksum(uint8_t *command, unsigned int length);

void checksum(ReadRegisters *command);
}

namespace dynamixel2
{

struct __attribute__((__packed__)) Header
{
	uint8_t header1;
	uint8_t header2;
	uint8_t header3;
	uint8_t header4;
	uint8_t id;
	uint8_t length_lo;
	uint8_t length_hi;
};

struct __attribute__((__packed__)) WriteData
{
	uint8_t instruction;
	uint8_t startAddres_lo;
	uint8_t startAddres_hi;
};

struct __attribute__((__packed__)) ReadData
{
	uint8_t instruction;
	uint8_t startAddres_lo;
	uint8_t startAddres_hi;
	uint8_t length_lo;
	uint8_t length_hi;
};

struct __attribute__((__packed__)) SyncWrite
{
	uint8_t instruction;
	uint8_t startAddres_lo;
	uint8_t startAddres_hi;
	uint8_t length_lo;
	uint8_t length_hi;
};

struct __attribute__((__packed__)) SyncWriteGainsPositionSpeed
{
	uint8_t gainD;
	uint8_t gainI;
	uint8_t gainP;
	uint8_t padding1;
	uint8_t goalPosition_lo;
	uint8_t goalPosition_hi;
	uint8_t movingSpeed_lo;
	uint8_t movingSpeed_hi;
};

enum class Fields
{
	HEADER1			= 0,
	HEADER2			= 1,
	HEADER3			= 2,
	HEADER4			= 3,
	ID				= 4,
	LENGTH			= 5,
	INSTRUCTION		= 6
};

void init(Header *header, uint8_t id, uint16_t length);
}
}

#endif //MOTIONLIBRARY_DYNAMIXELPROTOCOL_HPP
