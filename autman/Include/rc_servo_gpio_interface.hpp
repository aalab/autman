/*
 * Interfaces Class for RC Servos connected to the GPIO bus
 */

#ifndef __RCSERVO_GPIO_INTERFACE_HPP__
#define __RCSERVO_GPIO_INTERFACE_HPP__

#include "interface.hpp"

#include <boost/property_tree/ptree.hpp>

class Robot;

class RCServoGPIOInterface : public Interface
{
  friend class Interface;

  RCServoGPIOInterface( std::string name, boost::property_tree::ptree & pt, Robot const * robot );

  virtual int Open( void );
  virtual int Close( void );
};

#endif /* __RCSERVO_GPIO_INTERFACE_HPP__ */
