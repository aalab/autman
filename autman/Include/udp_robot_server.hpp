#ifndef __UDP_ROBOT_SERVER_HPP__
#define __UDP_ROBOT_SERVER_HPP__

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
/* #include <boost/asio/signal_set.hpp> */
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <json/value.h>
#include "json.hpp"

class Robot;

class UDPRobotServer
{
public:
  UDPRobotServer(boost::asio::io_service & io_service, unsigned int port, Robot * robot );
  void StartServer();

private:
  int SendResponse( std::string &msg );
  int SendResponse( Json::Value &json );

private:
  boost::asio::ip::udp::socket socket;
  boost::asio::ip::udp::endpoint remoteEP;
  boost::array<char,1> receiveBuffer;

  Robot * robot;
};

#endif /* __UDP_ROBOT_SERVER_HPP__ */
