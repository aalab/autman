/*
 * Interfaces Class for Dynamixel Servos on an RS485 Serial Line
 * encapsulates one communication bus for the various types of interfaces that we need
 * to control on the robot.
 */

#ifndef __RB110_SERIAL_INTERFACE_HPP__
#define __RB110_SERIAL_INTERFACE_HPP__

#include "interface.hpp"
#include "sync_command.hpp"
#include <cinttypes>

class SerialPort;

class Rb110SerialInterface : public Interface
{
  friend class Interface;

public:
  DynamixelInterface( std::string name, boost::property_tree::ptree pt );
  virtual int Open( void );
  virtual int Close( void );
  void SetInterface( std::string iface ) { interface = iface; };
  std::string GetInterface( void ) { return interface; };
  void SetBaudrate( std::string baud ) { baudrate = baud; };
  std::string GetBaudrate( void ) { return baudrate; };
  virtual int CommandTxAndRx( uint8_t const buffer[], unsigned int const bufferLength, uint8_t reply[], unsigned int const replyLength );
  int AddSyncCommand( unsigned int id, unsigned int ticks, unsigned int ispeed );
  int ReadySyncCommand( void );
  int SendSyncCommand( void );
  bool IsSyncCommandReady( void ) { return syncCommandReady; };

private:
  std::string interface;
  std::string baudrate;
  unsigned int comPort;
  bool syncCommandReady;
  SyncCommand syncCommands[256];
  unsigned int syncOut;
  SerialPort * serialPort;
};

#endif /* __RB110_SERIAL_INTERFACE_HPP__ */
