/*
 * Actuator Class for Robotis MX 28 servo
 */

#ifndef __ROBOTIS_SERVO_MX_28_HPP__
#define __ROBOTIS_SERVO_MX_28_HPP__

#include "robotis_servo_mx_series.hpp"

#include <boost/property_tree/ptree.hpp>

class DynamixelInterface;

class RobotisServoMX28 : public RobotisServoMXSeries
{
  friend class Actuator;

public:
  float DefaultSpeed() const { return defaultSpeed; };
  RobotisServoMX28( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

private:
    float defaultSpeed;
};

#endif /* __ROBOTIS_SERVO_MX_28_HPP__ */

