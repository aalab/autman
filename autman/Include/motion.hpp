#ifndef __MOTION_HPP__
#define __MOTION_HPP__

#include "trajectory.hpp"

#include <string>
#include <vector>

class Robot;
class Actuator;

class Motion
{
public:
  enum MotionTerminationRequest
    {
      Continue,
      ImmediateTerminate,
      FinishCycle
    };

  Motion( Robot const * robot, std::string name );
  int PulseMotion( float percentage, float motionSpeed ) const;

  std::vector<Trajectory> trajectories;
  std::vector<Actuator *> actuators;

  std::string const & GetName( void ) const { return name; };

  bool IsValid( void ) const { return valid; };

  void SetValid(bool newValue) { valid = newValue;};

private:
  std::string name;
  bool valid;
};

#endif /* __MOTION_HPP__ */
