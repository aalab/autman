
#ifndef __LOCAL_ROBOT_FILTER_HPP__
#define __LOCAL_ROBOT_FILTER_HPP__

#include <iostream>

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#if defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)

class LocalRobotFilter
{
public:
  LocalRobotFilter( boost::asio::io_service & ioService )
    : socket( ioService ) { }

  ~LocalRobotFilter() { }

  boost::asio::local::stream_protocol::socket & GetSocket() { return socket; }

  void Start( void );
  static void Run( boost::asio::io_service * ioService );

private:
  void ProcessRead( boost::system::error_code const & ec, std::size_t size );
  void ProcessWrite( boost::system::error_code const & ec );

private:
  boost::asio::local::stream_protocol::socket socket;
  boost::array<uint8_t, 4096> data;
};

#endif /* BOOST_ASIO_HAS_LOCAL_SOCKETS */
#endif /* __LOCAL_ROBOT_FILTER_HPP__ */
