#ifndef __KINEMATICS_HUMANOID_HPP__
#define __KINEMATICS_HUMANOID_HPP__

#include <cmath>
#include <memory>

#include "kinematics_head.hpp"
#include "kinematics_arm.hpp"
#include "kinematics_leg.hpp"

#define DEFAULT_DELTA_X 0
#define DEFAULT_DELTA_Y 0
#define DEFAULT_DELTA_THETA 0

#define DEFAULT_MAX_HEIGHT_PROP 0.1
#define DEFAULT_TRANSITION_PROP 0.1

#define DEFAULT_HIP_PITCH 9.0*M_PI/180.0
#define DEFAULT_ANKLE_PITCH 0.0*M_PI/180.0
#define DEFAULT_STEP_HEIGHT 0.03

#define DEFAULT_X_OFFSET 0.01
#define DEFAULT_Y_OFFSET 0.0
#define DEFAULT_Z_OFFSET -0.5

#define DEFAULT_TRANSFER_ANKLE_ROLL 0.0*M_PI/180.0
#define DEFAULT_TRANSFER_HIP_ROLL 5.0*M_PI/180.0
#define DEFAULT_TRANSFER_Y 0.02

#define DEFAULT_BUFFER_ANGLE 0.2

#define NUM_POSITIONS 8


class Robot;
class Motion;
class HumanoidTrajectoryParams;

class HumanoidTrajectoryParams {
public:
  float stepX, stepY, stepAngle, maxHeightProp, transitionProp, hipPitch, anklePitch, stepHeight, xOffset, yOffset, zOffset, transferAnkleRoll, transferHipRoll, transferY, bufferAngle;
  
  HumanoidTrajectoryParams();
  ~HumanoidTrajectoryParams();
};

class KinematicsHumanoid {
public:
  KinematicsHumanoid( Robot const * robot );
  ~KinematicsHumanoid();
  
  std::unique_ptr<Motion> calcWalkingGait( HumanoidTrajectoryParams& params)const;
  
  std::unique_ptr<Motion> calcWalkingGait( float stepX = DEFAULT_DELTA_X, float stepY = DEFAULT_DELTA_Y, float stepAngle = DEFAULT_DELTA_THETA, float maxHeightProp = DEFAULT_MAX_HEIGHT_PROP, float transitionProp = DEFAULT_TRANSITION_PROP, float hipPitch = DEFAULT_HIP_PITCH, float anklePitch = DEFAULT_ANKLE_PITCH, float stepHeight = DEFAULT_STEP_HEIGHT, float xOffset = DEFAULT_X_OFFSET, float yOffset = DEFAULT_Y_OFFSET, float zOffset = DEFAULT_Z_OFFSET, float transferAnkleRoll = DEFAULT_TRANSFER_ANKLE_ROLL, float transferHipRoll = DEFAULT_TRANSFER_HIP_ROLL, float transferY = DEFAULT_TRANSFER_Y, float bufferAngle = DEFAULT_BUFFER_ANGLE)const;
  
  HumanoidTrajectoryParams gaitParams;
  
private:
  KinematicsHead head;
  KinematicsArm leftArm;
  KinematicsArm rightArm;
  KinematicsLeg leftLeg;
  KinematicsLeg rightLeg;
  Robot const * robot;
  bool updateMotion(std::unique_ptr<Motion>& motion, std::vector<float>* angles, float* times, std::string* names, int numServos) const;
};

#endif
