#ifndef __MOTION_SEQUENCE_HPP__
#define __MOTION_SEQUENCE_HPP__

#include <vector>
#include "motion.hpp"
#include "kinematics_humanoid.hpp"

class Robot;

class MotionSequenceParams
{
public:
  //size of frames is zero if any gait calculations fail
  MotionSequenceParams(float stepX, float stepY, float stepAngle, unsigned long long int cycleTimeUSec, unsigned int numCycles);
  ~MotionSequenceParams() {}

  float stepX;
  float stepY;
  float stepAngle;
  unsigned long long int cycleTimeUSec;
  unsigned int numCycles;
};

class MotionSequenceFrame
{
public:
  MotionSequenceFrame(std::unique_ptr<Motion> motion, unsigned long long int cycleTimeUSec, unsigned int numCycles);
  ~MotionSequenceFrame() {};

  std::unique_ptr<Motion> motion;
  unsigned long long int cycleTimeUSec;
  unsigned int numCycles;
};

class MotionSequence
{
public:
  MotionSequence( KinematicsHumanoid* humanoid, std::vector<MotionSequenceParams>& params );
  ~MotionSequence() {for(MotionSequenceFrame* f:frames){delete f;}};

  std::vector<MotionSequenceFrame*> frames;
};

#endif /* __MOTION_SEQUENCE_HPP__ */
