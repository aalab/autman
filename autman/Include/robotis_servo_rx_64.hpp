/*
 * Actuator Class for Robotis RX 64 servo
 */

#ifndef __ROBOTIS_SERVO_RX_64_HPP__
#define __ROBOTIS_SERVO_RX_64_HPP__

#include "robotis_servo.hpp"
#include "robotis_servo_rx_series.hpp"
#include <boost/property_tree/ptree.hpp>

class Interface;

class RobotisServoRX64 : public RobotisServoRXSeries
{
  friend class Actuator;


public:
  float DefaultSpeed() const { return defaultSpeed; };
  RobotisServoRX64( std::string name, DynamixelInterface * interface, boost::property_tree::ptree & pt );

private:
   float defaultSpeed;
};

#endif /* __ROBOTIS_SERVO_RX_64_HPP__ */

