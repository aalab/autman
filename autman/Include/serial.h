//-----------------------------------------
// PROJECT: MotionController
// AUTHOR : POSIX - Jacky Baltes <jacky@cs.umanitoba.ca>
//          Windows - Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// DEFINE HEADER
//------------------------------------------------------------------------------
#ifndef SERIAL_H
#define SERIAL_H


//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------
#ifdef _WIN32
#include <windows.h>
#else
#include <termios.h>
#endif


//------------------------------------------------------------------------------
// CONSTANTS / TYPES
//------------------------------------------------------------------------------
class QString;
#define SERIAL_MAX_DEVNAME 32
#ifdef _WIN32
typedef DWORD BAUDRATE_TYPE;
#else
typedef speed_t BAUDRATE_TYPE;
#endif


//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class Serial
{
private:
    char _devname[SERIAL_MAX_DEVNAME]; // device name
    BAUDRATE_TYPE _baudrate; // baudrate
#ifdef _WIN32
    DCB _olddcb;
    HANDLE _hCommPort;
#else
    struct termios _oldtio;
    int _fd;
#endif

public:
    //------------------------------------------------------
    // PURPOSE: Construct serial port communicator
    // PARAMETERS:
    // REMARKS:
    //------------------------------------------------------
    Serial( void );

    //------------------------------------------------------
    // PURPOSE: Destruct serial port communicator
    // PARAMETERS:
    // REMARKS:
    //------------------------------------------------------
    virtual ~Serial( void );

    //------------------------------------------------------
    // PURPOSE: Close opened serial port
    // PARAMETERS:
    // REMARKS:
    //------------------------------------------------------
    virtual void close( void );

    //------------------------------------------------------
    // PURPOSE: Flushes both data received but not read and data written but not transmitted
    // PARAMETERS:
    // REMARKS: flushRx - flushes data recieved only
    //          flushTx - flushes data written only
    //------------------------------------------------------
    virtual void flush( void );
    virtual void flushRx( void );
    virtual void flushTx( void );

    //------------------------------------------------------
    // PURPOSE: Check if this serial is valid
    // PARAMETERS:
    //   [bool]<OUT> True, if serial is valid; otherwise, false
    // REMARKS:
    //------------------------------------------------------
    virtual bool isValid( void ) const;

    //------------------------------------------------------
    // PURPOSE: Open serial port
    // PARAMETERS:
    //   [devname]<IN> Device name to use
    //   [baudrate]<IN> Baudrate of serial port
    //   [bool]<OUT> True, if succeeded; otherwise, false
    // REMARKS:
    //------------------------------------------------------
    virtual bool open( const char* devname, BAUDRATE_TYPE baudrate );

    //------------------------------------------------------
    // PURPOSE: Read character from serial port
    // PARAMETERS:
    //   [pChar]<OUT> Pointer to character to receive the result
    //   [bool]<OUT> True, if succeeded; otherwise, false
    // REMARKS:
    //------------------------------------------------------
    virtual bool readChar( char* pChar );

    //------------------------------------------------------
    // PURPOSE: Read data from serial port
    // PARAMETERS:
    //   [data]<OUT> Bytes buffer
    //   [length]<IN> Length of the array
    //   [timeout]<IN> Timeout of reading in seconds
    //   [int]<OUT> Number of received bytes
    // REMARKS:
    //------------------------------------------------------
    virtual int readData( char* data, int length, int timeout = 5 );

    //------------------------------------------------------
    // PURPOSE: Write character onto serial port
    // PARAMETERS:
    //   [ch]<IN> Character to write
    //   [bool]<OUT> True, if succeeded; otherwise, false
    // REMARKS:
    //------------------------------------------------------
    virtual bool writeChar( char ch );

    //------------------------------------------------------
    // PURPOSE: Write data onto serial port
    // PARAMETERS:
    //   [data]<IN> Bytes to write
    //   [length]<IN> Length of the array
    //   [int]<OUT> Number of written bytes
    // REMARKS:
    //------------------------------------------------------
    virtual int writeData( const char* data, int length );

    //------------------------------------------------------
    // PURPOSE: Write data onto serial port slowly
    // PARAMETERS:
    //   [data]<IN> Bytes to write
    //   [length]<IN> Length of the array
    //   [milliseconds]<IN> Milliseconds delay between each bytes
    //   [int]<OUT> Number of written bytes
    // REMARKS:
    //------------------------------------------------------
    virtual int writeSlowly( const char* data, int length, int milliseconds = 10 );

    //------------------------------------------------------
    // PURPOSE: Retrieve baudrate of serial port
    // PARAMETERS:
    //   [BAUDRATE_TYPE]<OUT> Baudrate of serial port
    // REMARKS:
    //------------------------------------------------------
    virtual BAUDRATE_TYPE baudrate( void ) const { return _baudrate; }

    //------------------------------------------------------
    // PURPOSE: Retrieve serial port device name
    // PARAMETERS:
    //   [const char*]<OUT> Serial port device name
    // REMARKS:
    //------------------------------------------------------
    virtual const char* deviceName( void ) const { return _devname; }

public:
    //------------------------------------------------------
    // PURPOSE: Convert baudrate string into speed
    // PARAMETERS:
    //   [str]<IN> String to convert
    //   [BAUDRATE_TYPE]<OUT> Convert value
    // REMARKS:
    //------------------------------------------------------
    static BAUDRATE_TYPE spdBaudrate( const char* str );

    //------------------------------------------------------
    // PURPOSE: Convert baudrate speed into string
    // PARAMETERS:
    //   [speed]<IN> Speed to convert
    //   [const char*]<OUT> Convert string
    // REMARKS:
    //------------------------------------------------------
    static const char* strBaudrate( BAUDRATE_TYPE speed );

private:
    // no support to copy the object
    Serial( const Serial& ) { }
    virtual Serial& operator=( const Serial& ) { return *this; }

};


//------------------------------------------------------------------------------
// END OF DEFINE HEADER
//------------------------------------------------------------------------------
#endif
