import re

def isNumber(num):
    try:
        float(num)
        return True
    except:
        return False
        
def tokenizeTrajectory(trajFile):
    fText = trajFile.read()
    
    fTextNoComments = ''
    commentStarted = False
    for c in fText:
        if commentStarted:
            if c == '\n':
                commentStarted = False
        else:
            if c == ';':
                commentStarted = True
            else:
                fTextNoComments = fTextNoComments + c
        
    removeChars = '{}[](),'
    for i in range(len(removeChars)):
        fTextNoComments = fTextNoComments.replace(removeChars[i], '')
    
    fTokens = re.split(' |\n|\t', fTextNoComments)
    fUsefulTokens = []
    
    for t in fTokens:
        if t != None and t != '':
            fUsefulTokens.append(t)
            
    return fUsefulTokens
