import math
import numpy as np
import argparse
import sys

import Trajectory as traj
import UDPSocket as udp
import utility as util

import robotModelling as rm

def main( argv = None ):
    round_digits = 3

    parser = argparse.ArgumentParser()
    parser.add_argument( '-f', '--fileName', help="name of the file to load" )
    parser.add_argument( '-t', '--trajectory', help="name of the trajectory (if different from the one in the file)" )
    parser.add_argument( '-v', '--verbose', help="enable verbose output", action='count', default = 0 )
    parser.add_argument( '-i', '--ip', help="host ip" )
    parser.add_argument( '-p', '--port', type=int, help='port number of server', default='1313' )
    
    if ( argv == None ):
        argv = sys.argv[1:] 
    
    args = parser.parse_args( argv )
    
    if(args.fileName == None):
        print("ERROR: Must be provided the name the file to load")
        quit()
    
    fName = args.fileName
    if ( not args.fileName.endswith( '.info' ) ):
        fName = fName + '.info'
    
    f = open( fName, 'r')
        
    fUsefulTokens = util.tokenizeTrajectory(f)
    fUsefulTokens.append('End')
     
    fIndex = 2
    angles = []
    groupNames = []
    moveTimes = []
    
    while fIndex < len(fUsefulTokens) - 1:
        currGroup = []
        currGroupName = fUsefulTokens[fIndex]
        groupNames.append(currGroupName)
        fIndex = fIndex + 1

        while util.isNumber(fUsefulTokens[fIndex]):
            currGroup.append(float(fUsefulTokens[fIndex]))
            if len(angles) == 0:
                moveTimes.append(float(fUsefulTokens[fIndex+1]))
            fIndex = fIndex + 2
        
        angles.append(currGroup)
        
    f.close()
    
    trajName = args.trajectory
    if trajName == None:
        trajName = fUsefulTokens[1]
    
    numKeyframes = len(angles[0])
    trajectories = []
    for j in range(len(angles)):
        trajectories.append( traj.Trajectory( trajName, groupNames[j], [ ( round(float(angles[j][i]), round_digits), round(moveTimes[i], round_digits) ) for i in range(numKeyframes) ] ) )

    if ( args.ip ):
        transmitSuccessful = udp.transmitTrajectory(args.ip, args.port, trajectories, args.verbose>0)
            
        if(transmitSuccessful):
            print("Trajectory transmitted to " + args.ip + " port " + str(args.port) + " with name " + trajName)
        else:
            print("WARNING: Transmission failed. Restart the robot server and send the trajectory again.")
                

if ( __name__ == '__main__' ):
    main()
