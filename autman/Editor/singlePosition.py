import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import robotModelling as rm

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_aspect('equal')

robot = rm.Robot("SomeRobot")

forward_first = False
output_mode = 'radian' #options are 'degree' or 'tick' or 'radian' (default)
buffer_angle = 0.2
ll_desired_hip_transversal_angle = -10.0/180.0*math.pi
rl_desired_hip_transversal_angle = -10.0/180.0*math.pi

if forward_first:
    #Forward Kinematics
    robot.applyPositionLeftArm(10.0/180.0 * math.pi, 30.0/180.0 * math.pi, -30.0/180.0 * math.pi)
    #                         (LeftShoulderLateralAngle, LeftShoulderFrontalAngle, LeftElbowLateralAngle)
    robot.applyPositionRightArm(-10.0/180.0 * math.pi, -30.0/180.0 * math.pi, 30.0/180.0 * math.pi)
    #                         (RightShoulderLateralAngle, RightShoulderFrontalAngle, RightElbowLateralAngle)
    robot.applyPositionLeftLeg(ll_desired_hip_transversal_angle, -10.0/180.0*math.pi, -30.0/180.0*math.pi, 60.0/180.0*math.pi, 30.0/180.0*math.pi, -10.0/180.0*math.pi)
    #                                     (LeftHipTransveralAngle, LeftHipFrontalAngle, LeftHipLateralAngle, LeftKneeLateralAngle, LeftAnkleLateralAngle, LeftAnkleFrontalAngle)
    robot.applyPositionRightLeg(rl_desired_hip_transversal_angle, 10.0/180.0*math.pi, 30.0/180.0*math.pi, -60.0/180.0*math.pi, -30.0/180.0*math.pi, 10.0/180.0*math.pi)
    #                                   (RightHipTransveralAngle, RightHipFrontalAngle, RightHipLateralAngle, RightKneeLateralAngle, RightAnkleLateralAngle, RightAnkleFrontalAngle)
    robot.applyPositionHead(30.0/180.0*math.pi, -30.0/180.0*math.pi)
    #                         (HeadTransversalAngle, HeadLateralAngle)
    
    #Inverse Kinematics
    leftHandPosn = [robot.LeftHandFrontalHome[0,3], robot.LeftHandFrontalHome[1,3], robot.LeftHandFrontalHome[2,3]]
    rightHandPosn = [robot.RightHandFrontalHome[0,3], robot.RightHandFrontalHome[1,3], robot.RightHandFrontalHome[2,3]]
    leftFootPosn = [robot.LeftFootFrontalHome[0,3], robot.LeftFootFrontalHome[1,3], robot.LeftFootFrontalHome[2,3]]
    rightFootPosn = [robot.RightFootFrontalHome[0,3], robot.RightFootFrontalHome[1,3], robot.RightFootFrontalHome[2,3]]
    la_ik_result = robot.LeftArmIK(leftHandPosn, buffer_angle, output_mode)
    ra_ik_result = robot.RightArmIK(rightHandPosn, buffer_angle, output_mode)
    ll_ik_result = robot.LeftLegIK(leftFootPosn, ll_desired_hip_transversal_angle, buffer_angle, output_mode)
    rl_ik_result = robot.RightLegIK(rightFootPosn, rl_desired_hip_transversal_angle, buffer_angle, output_mode)
    h_ik_result = robot.HeadIK(robot.robot_torso_head_distance_x-robot.CameraFrontalHome[0,3], robot.CameraFrontalHome[1,3], robot.CameraFrontalHome[2,3], buffer_angle, output_mode)
    
else:
    #Inverse Kinematics
    leftHandPosn = [0.0, 0.4, -0.1]
    rightHandPosn = [0.0, -0.4, -0.1]
    leftFootPosn = [0.1, 0.2, -0.4]
    rightFootPosn = [-0.1, -0.2, -0.4]
    la_ik_result = robot.LeftArmIK(leftHandPosn, buffer_angle, output_mode)
    ra_ik_result = robot.RightArmIK(rightHandPosn, buffer_angle, output_mode)
    ll_ik_result = robot.LeftLegIK(leftFootPosn, ll_desired_hip_transversal_angle, buffer_angle, output_mode)
    rl_ik_result = robot.RightLegIK(rightFootPosn, rl_desired_hip_transversal_angle, buffer_angle, output_mode)
    h_ik_result = robot.HeadIK(+1, -0.02, 0.3, buffer_angle, output_mode)

    #Forward Kinematics
    robot.applyPositionLeftArm(la_ik_result[0], la_ik_result[1], la_ik_result[2])
    robot.applyPositionRightArm(ra_ik_result[0], ra_ik_result[1], ra_ik_result[2])
    robot.applyPositionLeftLeg(ll_ik_result[0], ll_ik_result[1], ll_ik_result[2], ll_ik_result[3], ll_ik_result[4], ll_ik_result[5])
    robot.applyPositionRightLeg(rl_ik_result[0], rl_ik_result[1], rl_ik_result[2], rl_ik_result[3], rl_ik_result[4], rl_ik_result[5])
    robot.applyPositionHead(h_ik_result[0], h_ik_result[1])
    
robot.draw(ax, True)
plt.show()
