import math
import numpy as np
import argparse
import sys
import re

import Trajectory as traj
import UDPSocket as udp
import utility as util

import robotModelling as rm

servoNames = ['LeftShoulderLateral', 'LeftShoulderFrontal', 'LeftElbowLateral', 'RightShoulderLateral', 'RightShoulderFrontal', 'RightElbowLateral', 'NeckTransversal', 'NeckLateral', 'LeftHipTransversal', 'LeftHipFrontal', 'LeftHipLateral', 'LeftKneeLateral', 'LeftAnkleLateral', 'LeftAnkleFrontal', 'RightHipTransversal', 'RightHipFrontal', 'RightHipLateral', 'RightKneeLateral', 'RightAnkleLateral', 'RightAnkleFrontal']

servoShortNames = [ 'lsl', 'lsf', 'lel', 'rsl', 'rsf', 'rel', 'nt', 'nl', 'lht', 'lhf', 'lhl', 'lkl', 'lal', 'laf', 'rht', 'rhf', 'rhl', 'rkl', 'ral', 'raf' ]

def jointShortNameToName( short ):
    long = ""
    if ( len(servoNames) != len( servoShortNames ) ):
        print("ERROR **** ServoNames and ServoShortNames do not match")
    for i in range(len(servoShortNames) ):
        if ( servoShortNames[i] == short ):
            long = servoNames[i]
            break
    return long

def convertMacroServos( servos ):
    tmp = servos.split()
    
    for i in range(len(tmp)):
        if tmp[i] == "knees":
            tmp[i] = "lkl rkl"
        elif tmp[i] == "elbows":
            tmp[i] = "lel rel"
        elif tmp[i] == "shoulderLaterals":
            tmp[i] = "lsl rsl"
        elif tmp[i] == "shoulderFrontals":
            tmp[i] = "lsl rsl"
        elif tmp[i] == "hipTransversals":
            tmp[i] = "lht rht"
        elif tmp[i] == "hipFrontals":
            tmp[i] = "lhf rhf"
        elif tmp[i] == "hipLaterals":
            tmp[i] = "lhl rhl"
        elif tmp[i] == "ankleLaterals":
            tmp[i] = "lal ral"
        elif tmp[i] == "ankleFrontals":
            tmp[i] = "laf raf"
        elif tmp[i] == "ll":
            tmp[i] = "lht lhf lhl lkl lal laf"
        elif tmp[i] == "rl": 
            tmp[i] = "rht rhf rhl rkl ral raf"
        elif tmp[i] == "ra": 
            tmp[i] = "rsl rsf rel"
        elif tmp[i] == "la": 
            tmp[i] = "lsl lsf lel"
    return " ".join(tmp) + " "

def printAngles(angles):
    for i in range(len(servoNames)):
        print(servoNames[i] + " " + str(angles[i]))

def main( argv = None ):
    num_servos = len(servoNames)
    round_digits = 3

    parser = argparse.ArgumentParser()
    parser.add_argument( '-t', '--trajectory', default='traj', help="name of the trajectory" )
    parser.add_argument( '-v', '--verbose', help="enable verbose output", action='count', default=0 )
    parser.add_argument( '-i', '--ip', help="host ip" )
    parser.add_argument( '-p', '--port', type=int, help='port number of server', default='1313' )
    parser.add_argument( '-s', '--save', help='save file name', default='default' )
    parser.add_argument( '-n', '--name', help=' The robot name', default='unknown')
    if ( argv == None ):
        argv = sys.argv[1:] 
    
    args = parser.parse_args( argv )
    robot_name = args.name
    print("robot name is:",robot_name)
    keyframes = []
    moveTimes = []
    
    if ( args.ip ):
        sock = udp.UDPSocket( args.ip, args.port )
        done = False
        print("Commands are: \'s+Enter\' to save the current position as a keyframe with the default time proportion of 1")
        print("              \'on [servos+]\' to turn torque on a set of servos") 
        print("              \'off [servos+]\' to turn torque off a set of servos") 
        print("              \'NUMBER+Enter\' to save the current position as a keyframe with a time proportion of NUMBER")
        print("              \'l+Enter'\' to list the servo angles of the last keyframe")
        print("              \'d+Enter'\' to delete the last keyframe")
        print("              \'q+Enter\' to save and quit")
        print("Note: time proportion will be ignored for the first keyframe (because you start there)")
        
        while not done:
            userInput = input("Enter command: ")
            inputIsNumber = util.isNumber(userInput)
            if userInput == "s" or inputIsNumber:
                currMoveTime = 1
                if inputIsNumber:
                    currMoveTime = float(userInput)
                
                print("Reading Angles...")
                
                msg = robot_name+" ReadState "
                if ( args.verbose > 0 ):
                    print('Send', msg )
                sock.SendMessage( msg )
                
                reply,_ = sock.ReceiveMessage( 2.0 )
                print(reply)
                if(reply == None):
                    print("WARNING: Read failed. Keyframe not recorded. Robot server restart recommended.")
                else:
                    if ( args.verbose > 0 ):
                        print('Reply', reply )
                    replyString = reply.decode('utf-8')
                    #print("Here:",replyString)
                    tokens = re.split(' |=|&', replyString)
                    angles = []
                    valuesValid = True
                    deadZone = False
                    print("The robot_name is ",tokens[0]," and you entered:", robot_name)
                    if(tokens[0] != robot_name):
                        
                        print("We got a problem in here!! My name is not ",robot_name," do you want I call you a cucumber?? :P")
                        quit()

                    for i in range(num_servos):
                        angles.append(tokens[(3 + 6*i)])
                        
                        if angles[-1] == 'nan':
                            print("WARNING: NaNs detected. Keyframe not recorded. Robot server restart recommended.")
                            valuesValid = False
                            break
                        if abs(round(float(angles[-1]), 3)) == 2.618 or abs(round(float(angles[-1]), 3)) == 3.142:
                            deadZone = True
                            
                    if valuesValid and deadZone:
                        print("WARNING: One or more servos may be in a dead zone. Or maybe you're skirting a dead zone. Or maybe the robot server needs a restart.")
                        resolved = False
                        while not resolved:
                            userInput = input("Press \'k+Enter\' to keep the keyframe, \'d+Enter\' to delete it, or \'l+Enter\' to list the angles of the last keyframe: ")
                            if userInput == "k":
                                resolved = True
                            elif userInput == "d":
                                resolved = True
                                valuesValid = False
                                print("Keyframe discarded")
                            elif userInput == "l":
                                printAngles(angles)
                            else:
                                print("Unknown command received")
                                
                    if valuesValid:
                        keyframes.append(angles)
                        moveTimes.append(currMoveTime)
                        print("Keyframe saved")
                        
            elif userInput == "l":
                if len(keyframes) > 0:
                    printAngles(keyframes[-1])
                else:
                    print("No previous keyframe to print")
                    
            elif userInput == "d":
                if len(keyframes) > 0:
                    keyframes.pop()
                    moveTimes.pop()
                    print("Last keyframe deleted")
                else:
                    print("No previous keyframe to delete")
                
            elif userInput == "q":
                print("Quitting program")
                done = True
            
            elif userInput == "powerOff":
                print("Powering off all the servos")
                command = robot_name+ " PowerOff " + "&"
                sock.SendMessage(command)
                reply,_ = sock.ReceiveMessage( 2.0 )
            
            elif userInput == "powerOn":
                print("Powering on all the servos")
                command = robot_name+ " PowerOn " + "&"
                sock.SendMessage(command)
                reply,_ = sock.ReceiveMessage( 2.0 )
            
                
            elif userInput[0:2] == "on":
                command = ""
                servos = convertMacroServos(userInput[2:]).split()
                print(servos)
                for s in servos:
                    l = jointShortNameToName( s )
                    if ( l != "" ):
                        command = command + " " +robot_name+ " Actuator " + l + " On" + "&"
                        sock.SendMessage(command)
                        reply,_ = sock.ReceiveMessage( 2.0 )
                        print(command)
                    else:
                        print("ERROR unable to find joint " + s )
                print("Sending command " + command )
            elif userInput[0:3] == "off":
                command = ""
                servos = convertMacroServos(userInput[3:]).split()
                print(servos)
                for s in servos:
                    l = jointShortNameToName( s )
                    if ( l != "" ):
                        command = command + " " +robot_name+ " Actuator " + l + " Off" + "&"
                        sock.SendMessage(command)
                        reply,_ = sock.ReceiveMessage( 2.0 )
                        print(command)
                    else:
                        print("ERROR unable to find joint " + s )
                
            else:
                print("Unknown command received")
        
        if(len(keyframes) <= 0):
            print("WARNING: No keyframes. Producing empty trajectory file. Trajectory will not be transmitted.")
        elif(len(keyframes) == 1):
            print("WARNING: Only one keyframe. Copying it to beginning and end of trajectory.")
            keyframes.append(keyframes[0])
            moveTimes.append(1)
        
        totalMoveTime = 0.0
        for i in range(len(moveTimes)):
            if i != 0:
                totalMoveTime = totalMoveTime + moveTimes[i]
        timeSoFar = 0.0
        keyTimes = []
        for i in range(len(moveTimes)):
            if i != 0:
                timeSoFar = timeSoFar + moveTimes[i]
            keyTimes.append(timeSoFar / totalMoveTime)
        if len(keyTimes) > 0:
            keyTimes[0] = 0.0
            keyTimes[-1] = 1.0
        
        trajectories = []
        for j in range(len(servoNames)):
            trajectories.append(traj.Trajectory(args.trajectory, servoNames[j], [(round(float(keyframes[i][j]), round_digits), round(keyTimes[i], round_digits)) for i in range(len(keyframes))]))
        
        fn = args.save
        if ( not args.save.endswith( '.info' ) ):
            fn = fn + '.info'
        
        f = open( fn, 'w')
        
        f.write('trajectories\n')
        f.write('{\n')
        f.write('    ' + args.trajectory + '\n')
        f.write('    {\n')

        for t in trajectories:
            msg = t.ToConfigString( 2 )
            f.write(msg)
        f.write('    }\n')
        f.write('}\n')
        
        f.close()
        print("Trajectory saved to " + fn)
        
        #Transmit trajectory by UDP
        if(len(keyframes) > 0):
            transmitSuccessful = udp.transmitTrajectory(args.ip, args.port, trajectories, args.verbose>0)
            
            if(transmitSuccessful):
                print("Trajectory transmitted to " + args.ip + " port " + str(args.port) + " with name " + args.trajectory)
            else:
                print("WARNING: Transmission failed. Restart the robot server and send the trajectory with loadTrajectory.py.")
        
    else:
        print("ERROR: A valid IP must be entered")

if ( __name__ == '__main__' ):
    main()
