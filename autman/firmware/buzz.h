/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 27, 2015
 **/

#ifndef MOTIONLIBRARY_BUZZ_H
#define MOTIONLIBRARY_BUZZ_H

#include "definitions.h"

void buzz(unsigned int beepDuration)
{f
	digitalWrite(BUZZER_PIN, HIGH);
	delay(beepDuration);
	digitalWrite(BUZZER_PIN, LOW);
}

void buzzError(unsigned int errorCode)
{
	buzz(700);
	delay(200);

	unsigned int
		longs = (errorCode % 3),
		shorts = (errorCode / 3),
		i;
	for (i = 0; i < longs; i++)
	{
		buzz(700);
		delay(200);
	}
	for (i = 0; i < shorts; i++)
	{
		buzz(200);
		delay(200);
	}
}

#endif //MOTIONLIBRARY_BUZZ_H
