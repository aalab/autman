/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 27, 2015
 **/

#ifndef MOTIONLIBRARY_DEFINITIONS_H
#define MOTIONLIBRARY_DEFINITIONS_H

#define BAUDRATE_1Mpbs							3

#define BUFFER_SIZE 1024
#define BUZZER_PIN 2

#define DXL_BUS_SERIAL1 1
#define DXL_BUS_SERIAL2 2
#define DXL_BUS_SERIAL3 3

#endif //MOTIONLIBRARY_DEFINITIONS_H
