#include "Wire.h"

byte SerialBuffer1[256];
byte SerialBuffer3[256];

byte SerialBufferWritePointer1 = 0, SerialBufferReadPointer1 = 0;
byte SerialBufferWritePointer3 = 0, SerialBufferReadPointer3 = 0;

Dynamixel Dxl(1);
// Dynamixel Dxl(DXL_BUS_SERIAL3);

void setup()
{
	// Dxl.begin(Boudrate_1000000bps);
	// Dxl.begin(3);
	Dxl.begin(1);

	// Serial3.begin(9600);
	// Serial3.begin(115200);
	SerialUSB.begin();

	/*
	pinMode(BOARD_LED_PIN     , OUTPUT);   //config onboard led
	pinMode(BOARD_BUTTON_PIN  , INPUT_PULLDOWN);  //configh onboard micro switch

	pinMode(10, OUTPUT);
	pinMode(11, INPUT_PULLDOWN);
	digitalWrite(10, HIGH);
	*/

	// Serial1.attachInterrupt(serialInterrupt1);
	// Serial3.attachInterrupt(serialInterrupt3);
	// SerialUSB.attachInterrupt(serialInterrupt3);
}

void serialInterrupt3(byte buffer)
{
	SerialBuffer3[SerialBufferWritePointer3++] = buffer;
}

void serialInterrupt1(byte buffer)
{
	SerialBuffer1[SerialBufferWritePointer1++] = buffer;
}

void loop()
{
	/*
	if (SerialBufferWritePointer3 != SerialBufferReadPointer3)
	{
		Dxl.writeRaw(SerialBuffer3[SerialBufferReadPointer3++]);
	}

	if (SerialBufferWritePointer1 != SerialBufferReadPointer1)
	{
		SerialUSB.print(static_cast<unsigned int>(SerialBuffer1[SerialBufferReadPointer1]));
		//SerialUSB.print(' ');
		//Serial3.print(SerialBuffer1[SerialBufferReadPointer1++],BYTE);
	}

	 */
	byte b;
	while (SerialUSB.available())
	{
		b = SerialUSB.read();
		Dxl.writeRaw(b);
		// SerialUSB.write(b);
	}

	while (Dxl.available())
	{
		SerialUSB.write(Dxl.readRaw());
		// SerialUSB.print(' ');
		// Serial3.print(b, BYTE);
	}
}