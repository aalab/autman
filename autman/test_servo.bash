#!/bin/bash
set -e
echo "Usage: $0 <tries> <servo name>"
movewait=0
readystatewait=0
for i in `seq 1 ${1}`; do
	echo "Try #${i}"
	cmd="Arash Actuator ${2} Position Angle=1.0 Speed=500.000"
	echo " > " $cmd
	echo $cmd | nc localhost -u 1313 -q 1
	sleep $movewait

	cmd="Arash ReadState"
	echo " > " $cmd
	state=$(echo $cmd | nc localhost -u 1313 -q 1)
	echo "State: "
	echo $state

#	if [ $? -eq 0 ]; then
#           echo "Error"
#        fi
	sleep $readystatewait

	cmd="Arash Actuator ${2} Position Angle=-1.0 Speed=500.000"
	echo " > " $cmd
	echo $cmd | nc 10.10.31.1 -u 1313 -q 1
	sleep $movewait

	cmd="Arash ReadState"
        echo " > " $cmd
        echo $cmd | nc localhost -u 1313 -q 1
        sleep $readystatewait
	echo "---------------------"
done

