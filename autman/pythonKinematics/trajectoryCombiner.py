import math
import numpy as np
import argparse
import sys
import re

import Trajectory as traj
import UDPSocket as udp
import utility as util

import robotModelling as rm

def main( argv = None ):
    round_digits = 3

    parser = argparse.ArgumentParser()
    parser.add_argument( '-1', '--firstFile', help="name of the first file" )
    parser.add_argument( '-2', '--secondFile', help="name of the second file" )
    parser.add_argument( '-%', '--firstProportion', help="proportion of the total time spent in the first motion", default=0.5)
    parser.add_argument( '-s', '--save', help='save file name' )
    parser.add_argument( '-t', '--trajectory', help="name of the trajectory" )
    parser.add_argument( '-v', '--verbose', help="enable verbose output", action='count', default=0 )
    parser.add_argument( '-i', '--ip', help="host ip" )
    parser.add_argument( '-p', '--port', type=int, help='port number of server', default='1313' )
    
    if ( argv == None ):
        argv = sys.argv[1:] 
    
    args = parser.parse_args( argv )
    
    if(args.firstFile == None or args.secondFile == None or args.save == None):
        print("ERROR: Must be provided the names of 2 files to combine and 1 to output to")
        quit()
      
    if(not util.isNumber(args.firstProportion) or float(args.firstProportion) <= 0 or float(args.firstProportion) >= 1):
        print("ERROR: First motion time proportion must be a number in the range (0, 1)")
        quit()
    
    f1Name = args.firstFile
    f2Name = args.secondFile
    fsName = args.save
    if ( not args.firstFile.endswith( '.info' ) ):
        f1Name = f1Name + '.info'
    if ( not args.secondFile.endswith( '.info' ) ):
        f2Name = f2Name + '.info'
    if ( not args.save.endswith( '.info' ) ):
        fsName = fsName + '.info'
    
    f1 = open( f1Name, 'r')
    f2 = open( f2Name, 'r')
    
    f1UsefulTokens = util.tokenizeTrajectory(f1)
    f2UsefulTokens = util.tokenizeTrajectory(f2)
    f1UsefulTokens.append('End')
    f2UsefulTokens.append('End')
     
    f1Index = 2
    f2Index = 2
    angles = []
    groupNames = []
    f1MoveTimes = []
    f2MoveTimes = []
    
    while f1Index < len(f1UsefulTokens) - 1:
        currGroup = []
        currGroupName = f1UsefulTokens[f1Index]
        groupNames.append(currGroupName)
        f1Index = f1Index + 1
        try:
            f2Index = f2UsefulTokens.index(currGroupName) + 1
        except:
            print("ERROR: Servo in file 1 not found in file 2")
            quit()
        
        while util.isNumber(f1UsefulTokens[f1Index]):
            currGroup.append(float(f1UsefulTokens[f1Index]))
            if len(angles) == 0:
                f1MoveTimes.append(float(f1UsefulTokens[f1Index+1]))
            f1Index = f1Index + 2
            
        firstNum = True
        while util.isNumber(f2UsefulTokens[f2Index]):
            if not firstNum: #skip the first keyframe of the second file, assuming it overlaps with the last keyframe of the first file
                currGroup.append(float(f2UsefulTokens[f2Index]))
                if len(angles) == 0:
                    f2MoveTimes.append(float(f2UsefulTokens[f2Index+1]))
            else:
                firstNum = False
            f2Index = f2Index + 2
        
        angles.append(currGroup)
        
    f1.close()
    f2.close()
    
    keyTimes = []
    for t in f1MoveTimes:
        keyTimes.append(t*float(args.firstProportion))
    for t in f2MoveTimes:
        keyTimes.append(float(args.firstProportion) + t*(1.0-float(args.firstProportion)))
    if len(keyTimes) > 0:
        keyTimes[0] = 0.0
        keyTimes[-1] = 1.0
    
    trajName = args.trajectory
    if trajName == None:
        trajName = f1UsefulTokens[1]
    
    numKeyframes = len(angles[0])
    trajectories = []
    for j in range(len(angles)):
        trajectories.append( traj.Trajectory( trajName, groupNames[j], [ ( round(float(angles[j][i]), round_digits), round(keyTimes[i], round_digits) ) for i in range(numKeyframes) ] ) )
    
    fs = open( fsName, 'w')
    
    fs.write('trajectories\n')
    fs.write('{\n')
    fs.write('    ' + trajName + '\n')
    fs.write('    {\n')

    for t in trajectories:
        msg = t.ToConfigString( 2 )
        fs.write(msg)
    fs.write('    }\n')
    fs.write('}\n')
    
    fs.close()
    
    if ( args.ip ):
        transmitSuccessful = udp.transmitTrajectory(args.ip, args.port, trajectories, args.verbose>0)
            
        if(transmitSuccessful):
            print("Trajectory transmitted to " + args.ip + " port " + str(args.port) + " with name " + trajName)
        else:
            print("WARNING: Transmission failed. Restart the robot server and send the trajectory with loadTrajectory.py.")

if ( __name__ == '__main__' ):
    main()
