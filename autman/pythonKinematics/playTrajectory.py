import math
import numpy as np
import argparse
import time
import sys

import Trajectory as traj
import UDPSocket as udp
import utility as util

import robotModelling as rm


def main(argv=None):
	round_digits = 3

	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--fileName', help="name of the file to load")
	parser.add_argument('-a', '--actuators', help="name of the actuators enabled (NeckLateral, NeckTransversal)")
	parser.add_argument('-v', '--verbose', help="enable verbose output", action='count', default=0)
	parser.add_argument('-i', '--ip', help="host ip")
	parser.add_argument('-p', '--port', type=int, help='port number of server', default='1313')

	if argv == None:
		argv = sys.argv[1:]

	args = parser.parse_args(argv)

	if args.fileName == None:
		print("ERROR: Must be provided the name the file to load")
		quit()

	if args.actuators != None:
		actuators = args.actuators.split(',')
	else:
		actuators = []

	print(actuators);

	fName = args.fileName
	if (not args.fileName.endswith('.info')):
		fName = fName + '.info'

	f = open(fName, 'r')

	fUsefulTokens = util.tokenizeTrajectory(f)
	fUsefulTokens.append('End')

	fIndex = 2
	angles = {}
	times = []

	while fIndex < len(fUsefulTokens) - 1:
		currGroupName = fUsefulTokens[fIndex]
		angles[currGroupName] = []

		fIndex = fIndex + 1
		while util.isNumber(fUsefulTokens[fIndex]):
			t = float(fUsefulTokens[fIndex + 1])
			if t not in times:
				times.append(t)
			angles[currGroupName].append({
				'angle': float(fUsefulTokens[fIndex]),
				'time': t
			})
			fIndex = fIndex + 2

	f.close()

	if args.ip:
		currentFrame = -1
		while currentFrame < len(times):
			userinput = False
			p1 = False
			tmp = input("Enter command: ").split(' ')
			if len(tmp) > 0:
				userinput = tmp[0]
				if len(tmp) > 1:
					p1 = tmp[1]
			if userinput == 'h':
				print("<numbers> - Go directly the frame")
				print("h - this help")
				print("n - Next frame")
				print("p - Previus frame")
				print("d [<frame>] - Dump the angles for the selected actuators. If <frame> is not set, it get the current frame.")
				print("i - Info")
				print("q - Quit")
			elif util.isNumber(userinput):
				currentFrame = int(userinput)
			elif userinput == 'q':
				print("bye")
				break
			elif userinput == 'd':
				print('Actuators')
				try:
					f = int(p1)
				except:
					f = currentFrame
				for actuator in angles:
					if (len(actuators) == 0) or (actuator in actuators):
						print('\t%s: %s' % (actuator, str(angles[actuator][f]['angle'])))
				continue
			elif userinput == 'n':
				currentFrame = currentFrame + 1
			elif userinput == 'p':
				if currentFrame > 0:
					currentFrame = currentFrame - 1
			elif userinput == 'i':
				print("Current frame is", currentFrame)
				continue
			time = times[currentFrame]
			print('Sending frame ', currentFrame, ' time ', time)
			for actuator in angles:
				if (len(actuators) == 0) or (actuator in actuators):
					udp.transmitData(args.ip, args.port, '{ "name": "Arash", "command": "Actuator", "params": { "command":"Position", "name":"%s", "angle":%s, "speed":200}}' % (actuator, str(angles[actuator][currentFrame]['angle'])), args.verbose > 0)

if __name__ == '__main__':
	main()
