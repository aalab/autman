from multiprocessing import Process
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from  matplotlib.animation import FuncAnimation

from mpl_toolkits.mplot3d import Axes3D
import robotModelling as rm
import re
import time

parseType = "keyframes"

def findarg(arg):
	i = 0
	for a in sys.argv:
		if (a == arg):
			return i
		i += 1
	return False

if (findarg("-interpolation")):
	parseType = "interpolation"

fileArg = findarg('-f')
if (fileArg == False):
	inputFile = sys.stdin
else:
	inputFile = open(sys.argv[fileArg + 1], mode='r')

intervalArg = findarg('-t')
if (intervalArg == False):
	interval = 300
else:
	interval = int(sys.argv[intervalArg + 1])

imageArg = findarg('-image') != False

keys = []
line = 1
i = 0
oldpercentage = 0
percentage = 0
while (line):
	line = inputFile.readline()
	if (parseType == "keyframes"):
		r = re.search("Position ([^ ]+) #([0-9]+) \\(([^\\)]+)\\)", line)
		if r:
			i = int(r.group(2))
			if (len(keys) <= i):
				keys.append({});
			tmp = r.group(3).split(' ');
			for value in tmp:
				actuator, angle = value.split(":")
				keys[i][actuator] = {
					'angle': float(angle),
					'speed': 100
				}
	elif (parseType == "interpolation"):
		r = re.search("Actuator Name: ([^ ]+) Angle:([^ ]+) Speed ([^ ]+) Percentage is:([^ ]+) t.GetSize", line)
		if r:
			if (len(keys) <= i):
				keys.append({})
			percentage = float(r.group(4))
			keys[i][r.group(1)] = {
				'angle': float(r.group(2)),
				'speed': int(r.group(3)),
				'percentage': percentage
			}
			if (percentage != oldpercentage):
				oldpercentage = percentage
				i += 1

robot = rm.Robot("SomeRobot")

forward_first = True
output_mode = 'radian'  # options are 'degree' or 'tick' or 'radian' (default)
buffer_angle = 0.2
ll_desired_hip_transversal_angle = -10.0 / 180.0 * math.pi
rl_desired_hip_transversal_angle = -10.0 / 180.0 * math.pi

def update(k):
	key = k % len(keys)

	if (parseType == "keyframes"):
		sys.stderr.write('# Step %d\n' % (key))
	elif (parseType == "interpolation"):
		sys.stderr.write('# Step %d (%f %%)\n' % (key, keys[key]['RightHipTransversal']['percentage']))
	sys.stderr.flush()

	if forward_first:
		# Forward Kinematics
		robot.applyPositionLeftArm(10.0 / 180.0 * math.pi, 30.0 / 180.0 * math.pi, -30.0 / 180.0 * math.pi)
		#                         (LeftShoulderLateralAngle, LeftShoulderFrontalAngle, LeftElbowLateralAngle)
		robot.applyPositionRightArm(-10.0 / 180.0 * math.pi, -30.0 / 180.0 * math.pi, 30.0 / 180.0 * math.pi)
		#                         (RightShoulderLateralAngle, RightShoulderFrontalAngle, RightElbowLateralAngle)
		# robot.applyPositionLeftLeg(ll_desired_hip_transversal_angle, -10.0/180.0*math.pi, -30.0/180.0*math.pi, 60.0/180.0*math.pi, 30.0/180.0*math.pi, -10.0/180.0*math.pi)
		#                                     (LeftHipTransveralAngle, LeftHipFrontalAngle, LeftHipLateralAngle, LeftKneeLateralAngle, LeftAnkleLateralAngle, LeftAnkleFrontalAngle)
		# robot.applyPositionRightLeg(rl_desired_hip_transversal_angle, 10.0/180.0*math.pi, 30.0/180.0*math.pi, -60.0/180.0*math.pi, -30.0/180.0*math.pi, 10.0/180.0*math.pi)
		#                                   (RightHipTransveralAngle, RightHipFrontalAngle, RightHipLateralAngle, RightKneeLateralAngle, RightAnkleLateralAngle, RightAnkleFrontalAngle)
		robot.applyPositionHead(30.0 / 180.0 * math.pi, -30.0 / 180.0 * math.pi)
		#                         (HeadTransversalAngle, HeadLateralAngle)

		robot.applyPositionLeftLeg(
			keys[key]['LeftHipTransversal']['angle'],
			keys[key]['LeftHipFrontal']['angle'],
			keys[key]['LeftHipLateral']['angle'],
			keys[key]['LeftKneeLateral']['angle'],
			keys[key]['LeftAnkleLateral']['angle'],
			keys[key]['LeftAnkleFrontal']['angle']
		)
		robot.applyPositionRightLeg(
			keys[key]['RightHipTransversal']['angle'],
			keys[key]['RightHipFrontal']['angle'],
			keys[key]['RightHipLateral']['angle'],
			keys[key]['RightKneeLateral']['angle'],
			keys[key]['RightAnkleLateral']['angle'],
			keys[key]['RightAnkleFrontal']['angle']
		)

		for k in keys[key]:
			# if (k in ("LeftAnkleLateral", "RightAnkleLateral")):
			v = keys[key][k]
			sys.stdout.write('{"name":"Arash","command":"Actuator","params":{"command":"Position","name":"%s","angle":%f,"speed":%f}}\r\n' % (k, v['angle'], 1000))
			# sys.stdout.write('{"name":"Arash","command":"Actuator","params":{"command":"Position","name":"%s","angle":%f,"speed":%f}}\r\n' % (k, v['angle'], v['speed']))
			sys.stdout.flush()
			time.sleep(0.01)

		# #Inverse Kinematics
		# leftHandPosn = [robot.LeftHandFrontalHome[0,3], robot.LeftHandFrontalHome[1,3], robot.LeftHandFrontalHome[2,3]]
		# rightHandPosn = [robot.RightHandFrontalHome[0,3], robot.RightHandFrontalHome[1,3], robot.RightHandFrontalHome[2,3]]
		# leftFootPosn = [robot.LeftFootFrontalHome[0,3], robot.LeftFootFrontalHome[1,3], robot.LeftFootFrontalHome[2,3]]
		# rightFootPosn = [robot.RightFootFrontalHome[0,3], robot.RightFootFrontalHome[1,3], robot.RightFootFrontalHome[2,3]]
		# la_ik_result = robot.LeftArmIK(leftHandPosn, buffer_angle, output_mode)
		# ra_ik_result = robot.RightArmIK(rightHandPosn, buffer_angle, output_mode)
		# ll_ik_result = robot.LeftLegIK(leftFootPosn, ll_desired_hip_transversal_angle, buffer_angle, output_mode)
		# rl_ik_result = robot.RightLegIK(rightFootPosn, rl_desired_hip_transversal_angle, buffer_angle, output_mode)
		# h_ik_result = robot.HeadIK(robot.robot_torso_head_distance_x-robot.CameraFrontalHome[0,3], robot.CameraFrontalHome[1,3], robot.CameraFrontalHome[2,3], buffer_angle, output_mode)

	else:
		# #Inverse Kinematics
		leftHandPosn = [0.0, 0.3, -0.1]
		rightHandPosn = [0.0, -0.4, -0.0]
		leftFootPosn = [0.1, 0.2, -0.4]
		rightFootPosn = [-0.1, -0.2, -0.4]
		la_ik_result = robot.LeftArmIK(leftHandPosn, buffer_angle, output_mode)
		ra_ik_result = robot.RightArmIK(rightHandPosn, buffer_angle, output_mode)
		ll_ik_result = robot.LeftLegIK(leftFootPosn, ll_desired_hip_transversal_angle, buffer_angle, output_mode)
		rl_ik_result = robot.RightLegIK(rightFootPosn, rl_desired_hip_transversal_angle, buffer_angle, output_mode)
		h_ik_result = robot.HeadIK(+1, -0.02, 0.3, buffer_angle, output_mode)

		# Forward Kinematics
		robot.applyPositionLeftArm(la_ik_result[0], la_ik_result[1], la_ik_result[2])
		robot.applyPositionRightArm(ra_ik_result[0], ra_ik_result[1], ra_ik_result[2])
		# robot.applyPositionLeftLeg(ll_ik_result[0], ll_ik_result[1], ll_ik_result[2], ll_ik_result[3], ll_ik_result[4], ll_ik_result[5])
		# robot.applyPositionRightLeg(rl_ik_result[0], rl_ik_result[1], rl_ik_result[2], rl_ik_result[3], rl_ik_result[4], rl_ik_result[5])
		# changing to get the LeftLeg and RightLeg position from the server
		robot.applyPositionLeftLeg(ll_ik_result[0], ll_ik_result[1], ll_ik_result[2], ll_ik_result[3], ll_ik_result[4],
								   ll_ik_result[5])
		robot.applyPositionRightLeg(rl_ik_result[0], rl_ik_result[1], rl_ik_result[2], rl_ik_result[3], rl_ik_result[4],
									rl_ik_result[5])

		robot.applyPositionHead(h_ik_result[0], h_ik_result[1])
	robot.draw(ax, True, not imageArg)

if (imageArg):
	for key in range(0, len(keys)):
		fig = plt.figure()
		ax = fig.add_subplot(111, projection='3d')
		ax.set_aspect('equal')

		keyIndex = 0

		update(key)
		plt.show()

else:
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.set_aspect('equal')

	keyIndex = 0

	animation = FuncAnimation(fig, update, interval=interval)
	plt.show()
