import math
import numpy as np

verbose_robot_modelling = False


class Robot:
	def __init__(self, name):
		self.name = name
		self.plotObjects = None

		"""self.robot_torso_shoulder_distance_y = 0.1315
		self.robot_torso_shoulder_distance_z = 0.1950
		self.shoulder_lateral_to_shoulder_frontal_y = 0.0360
		self.shoulder_frontal_to_elbow_lateral_y = 0.2000
		self.elbow_lateral_to_hand_frontal_y = 0.1670
		self.elbow_lateral_to_hand_frontal_z = 0.0000 #0.0200

		self.robot_torso_hip_distance_y = 0.0930
		self.robot_torso_hip_distance_z = -0.0950
		self.hip_transversal_to_hip_frontal_z = -0.0350
		self.hip_lateral_to_knee_lateral_z = -0.1720
		self.knee_lateral_to_ankle_lateral_z = -0.1780
		self.ankle_lateral_to_foot_frontal_z = -0.0400

		self.robot_torso_head_distance_x = 0.0420
		self.robot_torso_head_distance_z = 0.2170
		self.head_transversal_to_head_lateral_z = 0.0330
		self.head_lateral_to_camera_lateral_z = 0.0700

		self.center_to_torso_pos_x = 0.0650
		self.center_to_torso_neg_x = -0.0650
		self.center_to_torso_pos_y = 0.1250
		self.center_to_torso_neg_y = -0.1250
		self.center_to_torso_pos_z = 0.2170
		self.center_to_torso_neg_z = -0.0950"""

		self.robot_torso_shoulder_distance_y = 0.0900
		self.robot_torso_shoulder_distance_z = 0.2530
		self.shoulder_lateral_to_shoulder_frontal_y = 0.0350
		self.shoulder_frontal_to_elbow_lateral_y = 0.2000
		self.elbow_lateral_to_hand_frontal_y = 0.2400
		self.elbow_lateral_to_hand_frontal_z = 0.0000  # 0.0200

		self.robot_torso_hip_distance_y = 0.0800
		self.robot_torso_hip_distance_z = -0.0010
		self.hip_transversal_to_hip_frontal_z = -0.0410
		self.hip_lateral_to_knee_lateral_z = -0.2450
		self.knee_lateral_to_ankle_lateral_z = -0.2210
		self.ankle_lateral_to_foot_frontal_z = -0.0290

		self.foot_size = 0.2300

		self.robot_torso_head_distance_x = 0.0000
		self.robot_torso_head_distance_z = 0.2890
		self.head_transversal_to_head_lateral_z = 0.0340
		self.head_lateral_to_camera_lateral_z = 0.0540

		self.center_to_torso_pos_x = 0.0650
		self.center_to_torso_neg_x = -0.0650
		self.center_to_torso_pos_y = 0.1250
		self.center_to_torso_neg_y = -0.1250
		self.center_to_torso_pos_z = 0.3600
		self.center_to_torso_neg_z = -0.0000

		# 'headLateral':[1024,0.005,0.265] TODO: Remeasure these values
		# entries are of the form [motor_resolution, lower physical limit, upper physical limit]
		# Note: RX series dead zones are included in these limits (and also checked separately)
		self.servo_info = {
			'headTransversal': [1024, -5.0 * math.pi / 6.0, 5.0 * math.pi / 6.0],
			'headLateral': [1024, -5.0 * math.pi / 6.0, 5.0 * math.pi / 6.0],
			'leftShoulderLateral': [1024, -5.0 * math.pi / 6.0, 5.0 * math.pi / 6.0],
			'leftShoulderFrontal': [1024, -1.88, 1.60],
			'leftElbowLateral': [1024, -1.93, 1.88],
			'rightShoulderLateral': [1024, -5.0 * math.pi / 6.0, 5.0 * math.pi / 6.0],
			'rightShoulderFrontal': [1024, -1.61, 1.90],
			'rightElbowLateral': [1024, -1.95, 1.88],
			'leftHipTransversal': [4096, -2.22, 1.20],
			'leftHipFrontal': [4096, -0.72, 0.67],
			'leftHipLateral': [4096, -1.82, 1.76],
			'leftKneeLateral': [4096, -2.52, 2.19],
			'leftAnkleLateral': [4096, -1.86, 1.51],
			'leftAnkleFrontal': [4096, -0.68, 0.66],
			'rightHipTransversal': [4096, -1.25, 1.43],
			'rightHipFrontal': [4096, -0.74, 0.74],
			'rightHipLateral': [4096, -1.74, 1.85],
			'rightKneeLateral': [4096, -2.41, 2.51],
			'rightAnkleLateral': [4096, -1.51, 1.86],
			'rightAnkleFrontal': [4096, -0.67, 0.67]}

	def applyPositionLeftArm(self, LeftShoulderLateralAngle, LeftShoulderFrontalAngle, LeftElbowLateralAngle):
		self.LeftShoulderLateralHome = Translation(0.0, self.robot_torso_shoulder_distance_y,
												   self.robot_torso_shoulder_distance_z).dot(
			RotationX(-90.0 / 180.0 * math.pi).dot(RotationZ(90.0 / 180.0 * math.pi)))
		self.LeftShoulderLateralJoint = self.LeftShoulderLateralHome.dot(RotationZ(LeftShoulderLateralAngle))

		self.LeftShoulderFrontalHome = self.LeftShoulderLateralJoint.dot(
			Translation(0.0, 0.0, self.shoulder_lateral_to_shoulder_frontal_y).dot(
				RotationX(-90.0 / 180.0 * math.pi).dot(RotationZ(-90.0 / 180.0 * math.pi))))
		self.LeftShoulderFrontalJoint = self.LeftShoulderFrontalHome.dot(RotationZ(LeftShoulderFrontalAngle))

		self.LeftElbowLateralHome = self.LeftShoulderFrontalJoint.dot(
			Translation(self.shoulder_frontal_to_elbow_lateral_y, 0.0, 0.0).dot(RotationX(90.0 / 180.0 * math.pi)))
		self.LeftElbowLateralJoint = self.LeftElbowLateralHome.dot(RotationZ(LeftElbowLateralAngle))

		self.LeftHandFrontalHome = self.LeftElbowLateralJoint.dot(
			Translation(self.elbow_lateral_to_hand_frontal_y, 0.0, self.elbow_lateral_to_hand_frontal_z).dot(
				RotationX(90.0 / 180.0 * math.pi)))
		self.LeftHandFrontalJoint = self.LeftHandFrontalHome

		return self.LeftHandFrontalJoint

	def applyPositionRightArm(self, RightShoulderLateralAngle, RightShoulderFrontalAngle, RightElbowLateralAngle):
		self.RightShoulderLateralHome = Translation(0.0, -self.robot_torso_shoulder_distance_y,
													self.robot_torso_shoulder_distance_z).dot(
			RotationX(90.0 / 180.0 * math.pi).dot(RotationZ(-90.0 / 180.0 * math.pi)))
		self.RightShoulderLateralJoint = self.RightShoulderLateralHome.dot(RotationZ(RightShoulderLateralAngle))

		self.RightShoulderFrontalHome = self.RightShoulderLateralJoint.dot(
			Translation(0.0, 0.0, self.shoulder_lateral_to_shoulder_frontal_y).dot(
				RotationX(90.0 / 180.0 * math.pi).dot(RotationZ(90.0 / 180.0 * math.pi))))
		self.RightShoulderFrontalJoint = self.RightShoulderFrontalHome.dot(RotationZ(RightShoulderFrontalAngle))

		self.RightElbowLateralHome = self.RightShoulderFrontalJoint.dot(
			Translation(self.shoulder_frontal_to_elbow_lateral_y, 0.0, 0.0).dot(RotationX(-90.0 / 180.0 * math.pi)))
		self.RightElbowLateralJoint = self.RightElbowLateralHome.dot(RotationZ(RightElbowLateralAngle))

		self.RightHandFrontalHome = self.RightElbowLateralJoint.dot(
			Translation(self.elbow_lateral_to_hand_frontal_y, 0.0, self.elbow_lateral_to_hand_frontal_z).dot(
				RotationX(-90.0 / 180.0 * math.pi)))
		self.RightHandFrontalJoint = self.RightHandFrontalHome

		return self.RightHandFrontalJoint

	def applyPositionLeftLeg(self, LeftHipTransveralAngle, LeftHipFrontalAngle, LeftHipLateralAngle,
							 LeftKneeLateralAngle, LeftAnkleLateralAngle, LeftAnkleFrontalAngle):
		self.LeftHipTransversalHome = Translation(0.0, self.robot_torso_hip_distance_y,
												  self.robot_torso_hip_distance_z).dot(
			RotationY(180.0 / 180.0 * math.pi))
		self.LeftHipTransversalJoint = self.LeftHipTransversalHome.dot(RotationZ(LeftHipTransveralAngle))

		self.LeftHipFrontalHome = self.LeftHipTransversalJoint.dot(
			Translation(0.0, 0.0, -self.hip_transversal_to_hip_frontal_z).dot(RotationY(-90.0 / 180.0 * math.pi)).dot(
				RotationX(180.0 / 180.0 * math.pi)))
		self.LeftHipFrontalJoint = self.LeftHipFrontalHome.dot(RotationZ(LeftHipFrontalAngle))

		self.LeftHipLateralHome = self.LeftHipFrontalJoint.dot(RotationX(90.0 / 180.0 * math.pi))
		self.LeftHipLateralJoint = self.LeftHipLateralHome.dot(RotationZ(LeftHipLateralAngle))

		self.LeftKneeLateralHome = self.LeftHipLateralJoint.dot(
			Translation(-self.hip_lateral_to_knee_lateral_z, 0.0, 0.0))
		self.LeftKneeLateralJoint = self.LeftKneeLateralHome.dot(RotationZ(LeftKneeLateralAngle))

		self.LeftAnkleLateralHome = self.LeftKneeLateralJoint.dot(
			# Translation(-self.knee_lateral_to_ankle_lateral_z, 0.0, 0.0).dot(RotationX(180.0 / 180.0 * math.pi)))
			Translation(-self.knee_lateral_to_ankle_lateral_z, 0.0, 0.0).dot(RotationX(180.0 / 180.0 * math.pi)))
		self.LeftAnkleLateralJoint = self.LeftAnkleLateralHome.dot(RotationZ(LeftAnkleLateralAngle))

		self.LeftAnkleFrontalHome = self.LeftAnkleLateralJoint.dot(RotationX(-90.0 / 180.0 * math.pi))
		self.LeftAnkleFrontalJoint = self.LeftAnkleFrontalHome.dot(RotationZ(LeftAnkleFrontalAngle))

		self.LeftFootFrontalHome = self.LeftAnkleFrontalJoint.dot(
			Translation(-self.ankle_lateral_to_foot_frontal_z, 0.0, 0.0))
		self.LeftFootFrontalJoint = self.LeftFootFrontalHome

		# self.LeftFootHome = self.LeftFootFrontalJoint.dot(RotationY(LeftAnkleLateralAngle + math.pi/4).dot(Translation(self.foot_size*-0.3, 0.0, self.ankle_lateral_to_foot_frontal_z)))
		# self.LeftFootHome2 = self.LeftFootFrontalJoint.dot(RotationY(LeftAnkleLateralAngle + math.pi/4).dot(Translation(self.foot_size*0.6, 0.0, self.ankle_lateral_to_foot_frontal_z)))

		return self.LeftFootFrontalJoint

	def applyPositionRightLeg(self, RightHipTransveralAngle, RightHipFrontalAngle, RightHipLateralAngle,
							  RightKneeLateralAngle, RightAnkleLateralAngle, RightAnkleFrontalAngle, ):
		self.RightHipTransversalHome = Translation(0.0, -self.robot_torso_hip_distance_y,
												   self.robot_torso_hip_distance_z).dot(
			RotationY(180.0 / 180.0 * math.pi))
		self.RightHipTransversalJoint = self.RightHipTransversalHome.dot(RotationZ(RightHipTransveralAngle))

		self.RightHipFrontalHome = self.RightHipTransversalJoint.dot(
			Translation(0.0, 0.0, -self.hip_transversal_to_hip_frontal_z).dot(RotationY(-90.0 / 180.0 * math.pi)).dot(
				RotationX(180.0 / 180.0 * math.pi)))
		self.RightHipFrontalJoint = self.RightHipFrontalHome.dot(RotationZ(RightHipFrontalAngle))

		self.RightHipLateralHome = self.RightHipFrontalJoint.dot(RotationX(-90.0 / 180.0 * math.pi))
		self.RightHipLateralJoint = self.RightHipLateralHome.dot(RotationZ(RightHipLateralAngle))

		self.RightKneeLateralHome = self.RightHipLateralJoint.dot(
			Translation(-self.hip_lateral_to_knee_lateral_z, 0.0, 0.0))
		self.RightKneeLateralJoint = self.RightKneeLateralHome.dot(RotationZ(RightKneeLateralAngle))

		self.RightAnkleLateralHome = self.RightKneeLateralJoint.dot(
			Translation(-self.knee_lateral_to_ankle_lateral_z, 0.0, 0.0).dot(RotationX(180.0 / 180.0 * math.pi)))
		self.RightAnkleLateralJoint = self.RightAnkleLateralHome.dot(RotationZ(RightAnkleLateralAngle))

		self.RightAnkleFrontalHome = self.RightAnkleLateralJoint.dot(RotationX(90.0 / 180.0 * math.pi))
		self.RightAnkleFrontalJoint = self.RightAnkleFrontalHome.dot(RotationZ(RightAnkleFrontalAngle))

		self.RightFootFrontalHome = self.RightAnkleFrontalJoint.dot(
			Translation(-self.ankle_lateral_to_foot_frontal_z, 0.0, 0.0))
		self.RightFootFrontalJoint = self.RightFootFrontalHome

		# TODO: Put the distribution in the feet in variables
		# self.RightFootHome = Translation(self.foot_size*-0.3, 0.0, self.ankle_lateral_to_foot_frontal_z).dot(self.RightFootFrontalJoint.dot(RotationY(90.0 / 180.0 * math.pi)))
		# self.RightFootHome2 = Translation(self.foot_size*0.6, 0.0, self.ankle_lateral_to_foot_frontal_z).dot(self.RightFootFrontalJoint.dot(RotationY(90.0 / 180.0 * math.pi)))

		return self.RightFootFrontalJoint

	def applyPositionHead(self, HeadTransversalAngle, HeadLateralAngle):
		self.HeadTransversalHome = Translation(self.robot_torso_head_distance_x, 0.0, self.robot_torso_head_distance_z)
		self.HeadTransversalJoint = self.HeadTransversalHome.dot(RotationZ(HeadTransversalAngle))

		self.HeadLateralHome = self.HeadTransversalJoint.dot(
			Translation(0.0, 0.0, self.head_transversal_to_head_lateral_z).dot(
				RotationX(90.0 / 180.0 * math.pi).dot(RotationZ(90.0 / 180.0 * math.pi))))
		self.HeadLateralJoint = self.HeadLateralHome.dot(RotationZ(HeadLateralAngle))

		self.CameraFrontalHome = self.HeadLateralJoint.dot(
			Translation(self.head_lateral_to_camera_lateral_z, 0.0, 0.0).dot(RotationX(90.0 / 180.0 * math.pi)))
		self.CameraFrontalJoint = self.CameraFrontalHome

		return self.CameraFrontalJoint

	def draw(self, ax, draw_coords=False, updatable=False):
		if (updatable):
			if (not self.plotObjects):
				self.plotObjects = {}
		else:
			self.plotObjects = None

		if draw_coords:
			# Draw Axes
			A = np.eye(4)
			DrawCoordinateAxis(A, ax, self.plotObjects)
			DrawCoordinateAxis(self.LeftShoulderLateralJoint, ax, 'LeftShoulderLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftShoulderFrontalJoint, ax, 'LeftShoulderFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftElbowLateralJoint, ax, 'LeftElbowLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftHandFrontalJoint, ax, 'LeftHandFrontalJoint', self.plotObjects)

			DrawCoordinateAxis(self.RightShoulderLateralJoint, ax, 'RightShoulderLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightShoulderFrontalJoint, ax, 'RightShoulderFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightElbowLateralJoint, ax, 'RightElbowLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightHandFrontalJoint, ax, 'RightHandFrontalJoint', self.plotObjects)

			DrawCoordinateAxis(self.LeftHipTransversalJoint, ax, 'LeftHipTransversalJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftHipFrontalJoint, ax, 'LeftHipFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftHipLateralJoint, ax, 'LeftHipLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftKneeLateralJoint, ax, 'LeftKneeLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftAnkleFrontalJoint, ax, 'LeftAnkleFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftAnkleLateralJoint, ax, 'LeftAnkleLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.LeftFootFrontalJoint, ax, 'LeftFootFrontalJoint', self.plotObjects)

			DrawCoordinateAxis(self.RightHipTransversalJoint, ax, 'RightHipTransversalJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightHipFrontalJoint, ax, 'RightHipFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightHipLateralJoint, ax, 'RightHipLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightKneeLateralJoint, ax, 'RightKneeLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightAnkleFrontalJoint, ax, 'RightAnkleFrontalJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightAnkleLateralJoint, ax, 'RightAnkleLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.RightFootFrontalJoint, ax, 'RightFootFrontalJoint', self.plotObjects)

			DrawCoordinateAxis(self.HeadTransversalJoint, ax, 'HeadTransversalJoint', self.plotObjects)
			DrawCoordinateAxis(self.HeadLateralJoint, ax, 'HeadLateralJoint', self.plotObjects)
			DrawCoordinateAxis(self.CameraFrontalJoint, ax, 'CameraFrontalJoint', self.plotObjects)

		# Draw Robot
		DrawRobot(ax, [self.LeftShoulderLateralHome[0, 3], self.LeftShoulderFrontalHome[0, 3]],
				[self.LeftShoulderLateralHome[1, 3], self.LeftShoulderFrontalHome[1, 3]],
				[self.LeftShoulderLateralHome[2, 3], self.LeftShoulderFrontalHome[2, 3]], "y-", 10.0,
				"LeftShoulderLateral:LeftShoulderFrontal", self.plotObjects)
		DrawRobot(ax, [self.LeftShoulderFrontalHome[0, 3], self.LeftElbowLateralHome[0, 3]],
				[self.LeftShoulderFrontalHome[1, 3], self.LeftElbowLateralHome[1, 3]],
				[self.LeftShoulderFrontalHome[2, 3], self.LeftElbowLateralHome[2, 3]], "y-", 10.0,
				"LeftShoulderFrontal:LeftElbowLateral", self.plotObjects)
		DrawRobot(ax, [self.LeftElbowLateralHome[0, 3], self.LeftHandFrontalHome[0, 3]],
				[self.LeftElbowLateralHome[1, 3], self.LeftHandFrontalHome[1, 3]],
				[self.LeftElbowLateralHome[2, 3], self.LeftHandFrontalHome[2, 3]], "y-", 10.0,
				"LeftElbowLateral:LeftHandFrontal", self.plotObjects)

		DrawRobot(ax, [self.RightShoulderLateralHome[0, 3], self.RightShoulderFrontalHome[0, 3]],
				[self.RightShoulderLateralHome[1, 3], self.RightShoulderFrontalHome[1, 3]],
				[self.RightShoulderLateralHome[2, 3], self.RightShoulderFrontalHome[2, 3]], "y-", 10.0,
				"RightShoulderLateral:RightShoulderFrontal", self.plotObjects)
		DrawRobot(ax, [self.RightShoulderFrontalHome[0, 3], self.RightElbowLateralHome[0, 3]],
				[self.RightShoulderFrontalHome[1, 3], self.RightElbowLateralHome[1, 3]],
				[self.RightShoulderFrontalHome[2, 3], self.RightElbowLateralHome[2, 3]], "y-", 10.0,
				"RightShoulderFrontal:RightElbowLateral", self.plotObjects)
		DrawRobot(ax, [self.RightElbowLateralHome[0, 3], self.RightHandFrontalHome[0, 3]],
				[self.RightElbowLateralHome[1, 3], self.RightHandFrontalHome[1, 3]],
				[self.RightElbowLateralHome[2, 3], self.RightHandFrontalHome[2, 3]], "y-", 10.0,
				"RightElbowLateral:RightHandFrontal", self.plotObjects)

		DrawRobot(ax, [self.LeftHipTransversalHome[0, 3], self.LeftHipFrontalHome[0, 3]],
				[self.LeftHipTransversalHome[1, 3], self.LeftHipFrontalHome[1, 3]],
				[self.LeftHipTransversalHome[2, 3], self.LeftHipFrontalHome[2, 3]], "y-", 10.0,
				"LeftHipTransversal:LeftHipFrontal", self.plotObjects)
		DrawRobot(ax, [self.LeftHipFrontalHome[0, 3], self.LeftHipLateralHome[0, 3]],
				[self.LeftHipFrontalHome[1, 3], self.LeftHipLateralHome[1, 3]],
				[self.LeftHipFrontalHome[2, 3], self.LeftHipLateralHome[2, 3]], "y-", 10.0,
				"LeftHipFrontal:LeftHipLateral", self.plotObjects)
		DrawRobot(ax, [self.LeftHipLateralHome[0, 3], self.LeftKneeLateralHome[0, 3]],
				[self.LeftHipLateralHome[1, 3], self.LeftKneeLateralHome[1, 3]],
				[self.LeftHipLateralHome[2, 3], self.LeftKneeLateralHome[2, 3]], "y-", 10.0,
				"LeftHipLateral:LeftKneeLateral", self.plotObjects)
		DrawRobot(ax, [self.LeftKneeLateralHome[0, 3], self.LeftAnkleFrontalHome[0, 3]],
				[self.LeftKneeLateralHome[1, 3], self.LeftAnkleFrontalHome[1, 3]],
				[self.LeftKneeLateralHome[2, 3], self.LeftAnkleFrontalHome[2, 3]], "y-", 10.0,
				"LeftKneeLateral:LeftAnkleFrontal", self.plotObjects)
		DrawRobot(ax, [self.LeftAnkleFrontalHome[0, 3], self.LeftAnkleLateralHome[0, 3]],
				[self.LeftAnkleFrontalHome[1, 3], self.LeftAnkleLateralHome[1, 3]],
				[self.LeftAnkleFrontalHome[2, 3], self.LeftAnkleLateralHome[2, 3]], "y-", 10.0,
				"LeftAnkleFrontal:LeftAnkleLateral", self.plotObjects)
		DrawRobot(ax, [self.LeftAnkleLateralHome[0, 3], self.LeftFootFrontalHome[0, 3]],
				[self.LeftAnkleLateralHome[1, 3], self.LeftFootFrontalHome[1, 3]],
				[self.LeftAnkleLateralHome[2, 3], self.LeftFootFrontalHome[2, 3]], "m-", 10.0,
				"LeftAnkleLateral:LeftFootFrontal", self.plotObjects)

		# DrawRobot(ax, [self.LeftFootHome[0, 3], self.LeftFootHome2[0, 3]],
		# 		[self.LeftFootHome[1, 3], self.LeftFootHome2[1, 3]],
		# 		[self.LeftFootHome[2, 3], self.LeftFootHome2[2, 3]], "b-", 3.0,
		# 		"LeftFootHome:LeftFootHome2", self.plotObjects)

		DrawRobot(ax, [self.RightHipTransversalHome[0, 3], self.RightHipFrontalHome[0, 3]],
				[self.RightHipTransversalHome[1, 3], self.RightHipFrontalHome[1, 3]],
				[self.RightHipTransversalHome[2, 3], self.RightHipFrontalHome[2, 3]], "y-", 10.0,
				"RightHipTransversal:RightHipFrontal", self.plotObjects)
		DrawRobot(ax, [self.RightHipFrontalHome[0, 3], self.RightHipLateralHome[0, 3]],
				[self.RightHipFrontalHome[1, 3], self.RightHipLateralHome[1, 3]],
				[self.RightHipFrontalHome[2, 3], self.RightHipLateralHome[2, 3]], "y-", 10.0,
				"RightHipFrontal:RightHipLateral", self.plotObjects)
		DrawRobot(ax, [self.RightHipLateralHome[0, 3], self.RightKneeLateralHome[0, 3]],
				[self.RightHipLateralHome[1, 3], self.RightKneeLateralHome[1, 3]],
				[self.RightHipLateralHome[2, 3], self.RightKneeLateralHome[2, 3]], "y-", 10.0,
				"RightHipLateral:RightKneeLateral", self.plotObjects)
		DrawRobot(ax, [self.RightKneeLateralHome[0, 3], self.RightAnkleFrontalHome[0, 3]],
				[self.RightKneeLateralHome[1, 3], self.RightAnkleFrontalHome[1, 3]],
				[self.RightKneeLateralHome[2, 3], self.RightAnkleFrontalHome[2, 3]], "y-", 10.0,
				"RightKneeLateral:RightAnkleFrontal", self.plotObjects)
		DrawRobot(ax, [self.RightAnkleFrontalHome[0, 3], self.RightAnkleLateralHome[0, 3]],
				[self.RightAnkleFrontalHome[1, 3], self.RightAnkleLateralHome[1, 3]],
				[self.RightAnkleFrontalHome[2, 3], self.RightAnkleLateralHome[2, 3]], "y-", 10.0,
				"RightAnkleFrontal:RightAnkleLateral", self.plotObjects)
		DrawRobot(ax, [self.RightAnkleLateralHome[0, 3], self.RightFootFrontalHome[0, 3]],
				[self.RightAnkleLateralHome[1, 3], self.RightFootFrontalHome[1, 3]],
				[self.RightAnkleLateralHome[2, 3], self.RightFootFrontalHome[2, 3]], "m-", 10.0,
				"RightAnkleLateral:RightFootFrontal", self.plotObjects)

		# DrawRobot(ax, [self.RightFootHome[0, 3], self.RightFootHome2[0, 3]],
		# 		[self.RightFootHome[1, 3], self.RightFootHome2[1, 3]],
		# 		[self.RightFootHome[2, 3], self.RightFootHome2[2, 3]], "b-", 3.0,
		# 		"RightFootHome:RightFootHome2", self.plotObjects)

		DrawRobot(ax, [self.HeadTransversalHome[0, 3], self.HeadLateralHome[0, 3]],
				[self.HeadTransversalHome[1, 3], self.HeadLateralHome[1, 3]],
				[self.HeadTransversalHome[2, 3], self.HeadLateralHome[2, 3]], "y-", 10.0,
				"HeadTransversal:HeadLateral", self.plotObjects)
		DrawRobot(ax, [self.HeadLateralHome[0, 3], self.CameraFrontalHome[0, 3]],
				[self.HeadLateralHome[1, 3], self.CameraFrontalHome[1, 3]],
				[self.HeadLateralHome[2, 3], self.CameraFrontalHome[2, 3]], "y-", 10.0,
				"HeadLateral:CameraFrontal", self.plotObjects)

		# Draw Body Rectangle
		DrawRobot(ax, [self.center_to_torso_pos_x, self.center_to_torso_pos_x],
				[self.center_to_torso_neg_y, self.center_to_torso_pos_y],
				[self.center_to_torso_pos_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_1", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_neg_x],
				[self.center_to_torso_neg_y, self.center_to_torso_pos_y],
				[self.center_to_torso_pos_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_2", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_pos_x],
				[self.center_to_torso_neg_y, self.center_to_torso_neg_y],
				[self.center_to_torso_pos_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_3", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_pos_x],
				[self.center_to_torso_pos_y, self.center_to_torso_pos_y],
				[self.center_to_torso_pos_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_4", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_pos_x, self.center_to_torso_pos_x],
				[self.center_to_torso_neg_y, self.center_to_torso_pos_y],
				[self.center_to_torso_neg_z, self.center_to_torso_neg_z], "y-", 2.5,
				"body_5", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_neg_x],
				[self.center_to_torso_neg_y, self.center_to_torso_pos_y],
				[self.center_to_torso_neg_z, self.center_to_torso_neg_z], "y-", 2.5,
				"body_6", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_pos_x],
				[self.center_to_torso_neg_y, self.center_to_torso_neg_y],
				[self.center_to_torso_neg_z, self.center_to_torso_neg_z], "y-", 2.5,
				"body_7", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_pos_x],
				[self.center_to_torso_pos_y, self.center_to_torso_pos_y],
				[self.center_to_torso_neg_z, self.center_to_torso_neg_z], "y-", 2.5,
				"body_8", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_pos_x, self.center_to_torso_pos_x],
				[self.center_to_torso_neg_y, self.center_to_torso_neg_y],
				[self.center_to_torso_neg_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_9", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_neg_x],
				[self.center_to_torso_neg_y, self.center_to_torso_neg_y],
				[self.center_to_torso_neg_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_10", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_pos_x, self.center_to_torso_pos_x],
				[self.center_to_torso_pos_y, self.center_to_torso_pos_y],
				[self.center_to_torso_neg_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_11", self.plotObjects)
		DrawRobot(ax, [self.center_to_torso_neg_x, self.center_to_torso_neg_x],
				[self.center_to_torso_pos_y, self.center_to_torso_pos_y],
				[self.center_to_torso_neg_z, self.center_to_torso_pos_z], "y-", 2.5,
				"body_12", self.plotObjects)

	def HeadIK(self, desired_sign_x, goal_y, goal_z, buffer_angle, output_mode):
		deltaZ = goal_z - self.robot_torso_head_distance_z - self.head_transversal_to_head_lateral_z
		effective_length = math.sqrt(goal_y ** 2 + deltaZ ** 2)

		if effective_length > self.head_lateral_to_camera_lateral_z:
			print('ERROR: Head position is beyond reach')
			quit()

		lateral = math.acos(deltaZ / self.head_lateral_to_camera_lateral_z)
		if desired_sign_x < 0:
			lateral_rad = lateral
		else:
			lateral_rad = -lateral
		lateral_output = ConvertOutput(lateral_rad, 'headLateral', self.servo_info, buffer_angle, output_mode)

		xy_proj = abs(self.head_lateral_to_camera_lateral_z * math.sin(lateral))
		transversal = math.asin(goal_y / xy_proj)
		if desired_sign_x < 0:
			transversal_rad = -transversal
		else:
			transversal_rad = transversal
		transversal_output = ConvertOutput(transversal_rad, 'headTransversal', self.servo_info, buffer_angle,
										   output_mode)

		if verbose_robot_modelling:
			print('head lateral: ' + str(lateral_output))
			print('head transversal: ' + str(transversal_output))

		result = [transversal_rad, lateral_rad]
		return result

	def LeftArmIK(self, goal_posn, buffer_angle, output_mode):
		goal_x, goal_y, goal_z = goal_posn
		effective_length = math.sqrt((goal_x - 0.0) ** 2 + (
		goal_y - (self.robot_torso_shoulder_distance_y + self.shoulder_lateral_to_shoulder_frontal_y)) ** 2 + (
									 goal_z - self.robot_torso_shoulder_distance_z) ** 2)

		if effective_length > self.shoulder_frontal_to_elbow_lateral_y + self.elbow_lateral_to_hand_frontal_y:
			print('ERROR: Left arm position is beyond reach')
			quit()

		elbow_lateral = math.acos((
								  effective_length ** 2 - self.shoulder_frontal_to_elbow_lateral_y ** 2 - self.elbow_lateral_to_hand_frontal_y ** 2) / (
								  -2.0 * self.shoulder_frontal_to_elbow_lateral_y * self.elbow_lateral_to_hand_frontal_y))
		elbow_lateral_rad = -(math.pi - elbow_lateral)
		elbow_lateral_output = ConvertOutput(elbow_lateral_rad, 'leftElbowLateral', self.servo_info, buffer_angle,
											 output_mode)

		shoulder_frontal = math.acos(
			(goal_y - (self.robot_torso_shoulder_distance_y + self.shoulder_lateral_to_shoulder_frontal_y)) / (
			self.shoulder_frontal_to_elbow_lateral_y + self.elbow_lateral_to_hand_frontal_y * math.sin(
				elbow_lateral - math.pi / 2)))
		shoulder_frontal_rad = shoulder_frontal
		shoulder_frontal_output = ConvertOutput(shoulder_frontal_rad, 'leftShoulderFrontal', self.servo_info,
												buffer_angle, output_mode)

		shoulder_to_elbow_projection = self.shoulder_frontal_to_elbow_lateral_y * math.sin(shoulder_frontal)
		elbow_to_hand_projection = math.sqrt(
			(self.elbow_lateral_to_hand_frontal_y * math.cos(elbow_lateral - math.pi / 2.0)) ** 2 + (
			self.elbow_lateral_to_hand_frontal_y * math.sin(elbow_lateral - math.pi / 2.0) * math.sin(
				shoulder_frontal)) ** 2)
		effectective_length_x_z_projection = math.sqrt(
			(goal_x - 0.0) ** 2 + (goal_z - self.robot_torso_shoulder_distance_z) ** 2)
		effective_shoulder_lateral = math.asin((goal_x - 0.0) / effectective_length_x_z_projection)
		elbow_lateral_x_z_projection = math.acos((
												 effectective_length_x_z_projection ** 2 - shoulder_to_elbow_projection ** 2 - elbow_to_hand_projection ** 2) / (
												 -2.0 * shoulder_to_elbow_projection * elbow_to_hand_projection))
		shoulder_lateral = effective_shoulder_lateral - math.asin(
			elbow_to_hand_projection * math.sin(elbow_lateral_x_z_projection) / effectective_length_x_z_projection)
		shoulder_lateral_rad = -shoulder_lateral
		shoulder_lateral_output = ConvertOutput(shoulder_lateral_rad, 'leftShoulderLateral', self.servo_info,
												buffer_angle, output_mode)

		if verbose_robot_modelling:
			print('left shoulder lateral: ' + str(shoulder_lateral_output))
			print('left shoulder frontal: ' + str(shoulder_frontal_output))
			print('left elbow: ' + str(elbow_lateral_output))

		result = [shoulder_lateral_rad, shoulder_frontal_rad, elbow_lateral_rad]
		return result

	def RightArmIK(self, goal_posn, buffer_angle, output_mode):
		goal_x, goal_y, goal_z = goal_posn
		effective_length = math.sqrt((goal_x - 0.0) ** 2 + (
		goal_y - (-self.robot_torso_shoulder_distance_y - self.shoulder_lateral_to_shoulder_frontal_y)) ** 2 + (
									 goal_z - self.robot_torso_shoulder_distance_z) ** 2)

		if effective_length > self.shoulder_frontal_to_elbow_lateral_y + self.elbow_lateral_to_hand_frontal_y:
			print('ERROR: Right arm position is beyond reach')
			quit()

		elbow_lateral = math.acos((
								  effective_length ** 2 - self.shoulder_frontal_to_elbow_lateral_y ** 2 - self.elbow_lateral_to_hand_frontal_y ** 2) / (
								  -2.0 * self.shoulder_frontal_to_elbow_lateral_y * self.elbow_lateral_to_hand_frontal_y))
		elbow_lateral_rad = math.pi - elbow_lateral
		elbow_lateral_output = ConvertOutput(elbow_lateral_rad, 'rightElbowLateral', self.servo_info, buffer_angle,
											 output_mode)

		shoulder_frontal = math.acos(
			(goal_y - (-self.robot_torso_shoulder_distance_y - self.shoulder_lateral_to_shoulder_frontal_y)) / (
			self.shoulder_frontal_to_elbow_lateral_y + self.elbow_lateral_to_hand_frontal_y * math.sin(
				elbow_lateral - math.pi / 2.0)))
		shoulder_frontal_rad = -(math.pi - shoulder_frontal)
		shoulder_frontal_output = ConvertOutput(shoulder_frontal_rad, 'rightShoulderFrontal', self.servo_info,
												buffer_angle, output_mode)

		shoulder_to_elbow_projection = self.shoulder_frontal_to_elbow_lateral_y * math.sin(shoulder_frontal)
		elbow_to_hand_projection = math.sqrt(
			(self.elbow_lateral_to_hand_frontal_y * math.cos(elbow_lateral - math.pi / 2.0)) ** 2 + (
			self.elbow_lateral_to_hand_frontal_y * math.sin(elbow_lateral - math.pi / 2.0) * math.sin(
				shoulder_frontal)) ** 2)
		effectective_length_x_z_projection = math.sqrt(
			(goal_x - 0.0) ** 2 + (goal_z - self.robot_torso_shoulder_distance_z) ** 2)
		effective_shoulder_lateral = math.asin((goal_x - 0.0) / effectective_length_x_z_projection)
		elbow_lateral_x_z_projection = math.acos((
												 effectective_length_x_z_projection ** 2 - shoulder_to_elbow_projection ** 2 - elbow_to_hand_projection ** 2) / (
												 -2 * shoulder_to_elbow_projection * elbow_to_hand_projection))
		shoulder_lateral = effective_shoulder_lateral - math.asin(
			elbow_to_hand_projection * math.sin(elbow_lateral_x_z_projection) / effectective_length_x_z_projection)
		shoulder_lateral_rad = shoulder_lateral
		shoulder_lateral_output = ConvertOutput(shoulder_lateral_rad, 'rightShoulderLateral', self.servo_info,
												buffer_angle, output_mode)

		if verbose_robot_modelling:
			print('right shoulder lateral: ' + str(shoulder_lateral_output))
			print('right shoulder frontal: ' + str(shoulder_frontal_output))
			print('right elbow: ' + str(elbow_lateral_output))

		result = [shoulder_lateral_rad, shoulder_frontal_rad, elbow_lateral_rad]
		return result

	def LeftLegIK(self, goal_posn, desired_hip_transversal_angle, buffer_angle, output_mode):
		# foot angle IK started, but not finished
		goal_x, goal_y, goal_z = goal_posn

		# currently, must be set to 0 as foot angle IK is not implemented (0 assumes a flat foot placement)
		desired_ankle_lateral_angle_from_flat = 0.0 / 180.0 * math.pi
		desired_ankle_frontal_angle_from_flat = 0.0 / 180.0 * math.pi

		# print(goal_x)
		# print(goal_y)
		# print(goal_z)

		# part of not-fully-implemented foot angle IK (these 3 lines behave properly so long as the desired ankle angles are 0)
		goal_x = goal_x - self.ankle_lateral_to_foot_frontal_z * math.sin(
			desired_ankle_lateral_angle_from_flat) * math.cos(desired_ankle_frontal_angle_from_flat)
		goal_y = goal_y - self.ankle_lateral_to_foot_frontal_z * math.sin(
			desired_ankle_frontal_angle_from_flat) * math.cos(desired_ankle_lateral_angle_from_flat)
		goal_z = goal_z - self.ankle_lateral_to_foot_frontal_z * math.cos(
			desired_ankle_lateral_angle_from_flat) * math.cos(desired_ankle_frontal_angle_from_flat)

		# rotate the goal point into the frame of the desired hip transversal angle
		temp_x = goal_x
		goal_x = goal_x * math.cos(-desired_hip_transversal_angle) - (
																	 goal_y - self.robot_torso_hip_distance_y) * math.sin(
			-desired_hip_transversal_angle)
		goal_y = temp_x * math.sin(-desired_hip_transversal_angle) + (
																	 goal_y - self.robot_torso_hip_distance_y) * math.cos(
			-desired_hip_transversal_angle) + self.robot_torso_hip_distance_y

		# print(goal_x)
		# print(goal_y)
		# print(goal_z)
		# print(self.LeftAnkleLateralHome[0,3])
		# print(self.LeftAnkleLateralHome[1,3])
		# print(self.LeftAnkleLateralHome[2,3])

		effective_length = math.sqrt((goal_x - 0.0) ** 2 + (goal_y - self.robot_torso_hip_distance_y) ** 2 + (
		goal_z - (self.robot_torso_hip_distance_z + self.hip_transversal_to_hip_frontal_z)) ** 2)

		if effective_length > math.fabs(self.hip_lateral_to_knee_lateral_z + self.knee_lateral_to_ankle_lateral_z):
			print('ERROR: Left leg position is beyond reach')
			quit()

		knee_lateral = math.acos((
								 effective_length ** 2 - self.hip_lateral_to_knee_lateral_z ** 2 - self.knee_lateral_to_ankle_lateral_z ** 2) / (
								 -2.0 * math.fabs(self.hip_lateral_to_knee_lateral_z) * math.fabs(
									 self.knee_lateral_to_ankle_lateral_z)))
		knee_lateral_rad = math.pi - knee_lateral
		knee_lateral_output = ConvertOutput(knee_lateral_rad, 'leftKneeLateral', self.servo_info, buffer_angle,
											output_mode)

		hip_frontal = math.asin((goal_y - self.robot_torso_hip_distance_y) / math.sqrt(
			(goal_y - self.robot_torso_hip_distance_y) ** 2 + (
			goal_z - (self.robot_torso_hip_distance_z + self.hip_transversal_to_hip_frontal_z)) ** 2))
		hip_frontal_rad = -hip_frontal
		hip_frontal_output = ConvertOutput(hip_frontal_rad, 'leftHipFrontal', self.servo_info, buffer_angle,
										   output_mode)

		hip_lateral = math.asin((goal_x - 0.0) / effective_length) + math.asin(
			math.fabs(self.knee_lateral_to_ankle_lateral_z) * math.sin(knee_lateral) / effective_length)
		hip_lateral_rad = -hip_lateral
		hip_lateral_output = ConvertOutput(hip_lateral_rad, 'leftHipLateral', self.servo_info, buffer_angle,
										   output_mode)

		ankle_lateral_rad = (math.pi - knee_lateral) - hip_lateral + desired_ankle_lateral_angle_from_flat
		ankle_lateral_output = ConvertOutput(ankle_lateral_rad, 'leftAnkleLateral', self.servo_info, buffer_angle,
											 output_mode)

		ankle_frontal_rad = -hip_frontal - desired_ankle_frontal_angle_from_flat
		ankle_frontal_output = ConvertOutput(ankle_frontal_rad, 'leftAnkleFrontal', self.servo_info, buffer_angle,
											 output_mode)

		hip_transversal_output = ConvertOutput(-desired_hip_transversal_angle, 'leftHipTransversal', self.servo_info,
											   buffer_angle, output_mode)

		if verbose_robot_modelling:
			print('left hip transversal: ' + str(hip_transversal_output))
			print('left hip frontal: ' + str(hip_frontal_output))
			print('left hip lateral: ' + str(hip_lateral_output))
			print('left knee lateral: ' + str(knee_lateral_output))
			print('left ankle lateral: ' + str(ankle_lateral_output))
			print('left ankle frontal: ' + str(ankle_frontal_output))

		result = [-desired_hip_transversal_angle, hip_frontal_rad, hip_lateral_rad, knee_lateral_rad, ankle_lateral_rad,
				  ankle_frontal_rad]
		return result

	def LeftLegCalcAngles(self, goal_posn, desired_hip_transversal_angle, hip_pitch, ankle_pitch, hip_roll, ankle_roll,
						  buffer_angle, output_mode):
		ikResult = self.LeftLegIK(goal_posn, desired_hip_transversal_angle, buffer_angle, output_mode)
		ikResult[2] = ikResult[2] - hip_pitch
		ikResult[4] = ikResult[4] + ankle_pitch
		ikResult[1] = ikResult[1] - hip_roll
		ikResult[5] = ikResult[5] + ankle_roll
		return ikResult

	def RightLegIK(self, goal_posn, desired_hip_transversal_angle, buffer_angle, output_mode):
		goal_x, goal_y, goal_z = goal_posn
		goal_z = goal_z - self.ankle_lateral_to_foot_frontal_z

		# rotate the goal point into the frame of the desired hip transversal angle
		temp_x = goal_x
		goal_x = goal_x * math.cos(-desired_hip_transversal_angle) - (
																	 goal_y + self.robot_torso_hip_distance_y) * math.sin(
			-desired_hip_transversal_angle)
		goal_y = temp_x * math.sin(-desired_hip_transversal_angle) + (
																	 goal_y + self.robot_torso_hip_distance_y) * math.cos(
			-desired_hip_transversal_angle) - self.robot_torso_hip_distance_y

		effective_length = math.sqrt((goal_x - 0.0) ** 2 + (goal_y + self.robot_torso_hip_distance_y) ** 2 + (
		goal_z - (self.robot_torso_hip_distance_z + self.hip_transversal_to_hip_frontal_z)) ** 2)

		if effective_length > math.fabs(self.hip_lateral_to_knee_lateral_z + self.knee_lateral_to_ankle_lateral_z):
			print('ERROR: Right leg position is beyond reach')
			quit()

		knee_lateral = math.acos((
								 effective_length ** 2 - self.hip_lateral_to_knee_lateral_z ** 2 - self.knee_lateral_to_ankle_lateral_z ** 2) / (
								 -2.0 * math.fabs(self.hip_lateral_to_knee_lateral_z) * math.fabs(
									 self.knee_lateral_to_ankle_lateral_z)))
		knee_lateral_rad = -(math.pi - knee_lateral)
		knee_lateral_output = ConvertOutput(knee_lateral_rad, 'rightKneeLateral', self.servo_info, buffer_angle,
											output_mode)

		hip_frontal = math.asin((goal_y + self.robot_torso_hip_distance_y) / math.sqrt(
			(goal_y + self.robot_torso_hip_distance_y) ** 2 + (
			goal_z - (self.robot_torso_hip_distance_z + self.hip_transversal_to_hip_frontal_z)) ** 2))
		hip_frontal_rad = -hip_frontal
		hip_frontal_output = ConvertOutput(hip_frontal_rad, 'rightHipFrontal', self.servo_info, buffer_angle,
										   output_mode)

		hip_lateral = math.asin((goal_x - 0.0) / effective_length) + math.asin(
			math.fabs(self.knee_lateral_to_ankle_lateral_z) * math.sin(knee_lateral) / effective_length)
		hip_lateral_rad = hip_lateral
		hip_lateral_output = ConvertOutput(hip_lateral_rad, 'rightHipLateral', self.servo_info, buffer_angle,
										   output_mode)

		ankle_lateral_rad = -((math.pi - knee_lateral) - hip_lateral)
		ankle_lateral_output = ConvertOutput(ankle_lateral_rad, 'rightAnkleLateral', self.servo_info, buffer_angle,
											 output_mode)

		ankle_frontal_rad = -hip_frontal
		ankle_frontal_output = ConvertOutput(ankle_frontal_rad, 'rightAnkleFrontal', self.servo_info, buffer_angle,
											 output_mode)

		hip_transversal_output = ConvertOutput(-desired_hip_transversal_angle, 'rightHipTransversal', self.servo_info,
											   buffer_angle, output_mode)

		if verbose_robot_modelling:
			print('right hip transversal: ' + str(hip_transversal_output))
			print('right hip frontal: ' + str(hip_frontal_output))
			print('right hip lateral: ' + str(hip_lateral_output))
			print('right knee lateral: ' + str(knee_lateral_output))
			print('right ankle lateral: ' + str(ankle_lateral_output))
			print('right ankle frontal: ' + str(ankle_frontal_output))

		result = [-desired_hip_transversal_angle, hip_frontal_rad, hip_lateral_rad, knee_lateral_rad, ankle_lateral_rad,
				  ankle_frontal_rad]
		return result

	def RightLegCalcAngles(self, goal_posn, desired_hip_transversal_angle, hip_pitch, ankle_pitch, hip_roll, ankle_roll,
						   buffer_angle, output_mode):
		ikResult = self.RightLegIK(goal_posn, desired_hip_transversal_angle, buffer_angle, output_mode)
		ikResult[2] = ikResult[2] + hip_pitch
		ikResult[4] = ikResult[4] - ankle_pitch
		ikResult[1] = ikResult[1] + hip_roll
		ikResult[5] = ikResult[5] - ankle_roll
		return ikResult


# End of Robot

def DrawCoordinateAxis(A, ax, name=None, plotObject=None):
	xaxis = np.array([[0.0, 0.0, 0.0, 1.0], [0.1, 0.0, 0.0, 1.0]]).T
	yaxis = np.array([[0.0, 0.0, 0.0, 1.0], [0.0, 0.1, 0.0, 1.0]]).T
	zaxis = np.array([[0.0, 0.0, 0.0, 1.0], [0.0, 0.0, 0.1, 1.0]]).T

	xaxis_d = A.dot(xaxis)
	yaxis_d = A.dot(yaxis)
	zaxis_d = A.dot(zaxis)

	if (plotObject == None):
		ax.plot(xaxis_d[0, :], xaxis_d[1, :], xaxis_d[2, :], "r-")
		ax.plot(yaxis_d[0, :], yaxis_d[1, :], yaxis_d[2, :], "g-")
		ax.plot(zaxis_d[0, :], zaxis_d[1, :], zaxis_d[2, :], "b-")
	else:
		if (name + '_axis_x' in plotObject):
			plotObject[name + '_axis_x'].set_data(xaxis_d[0, :], xaxis_d[1, :])
			plotObject[name + '_axis_x'].set_3d_properties(xaxis_d[2, :])

			plotObject[name + '_axis_y'].set_data(yaxis_d[0, :], yaxis_d[1, :])
			plotObject[name + '_axis_y'].set_3d_properties(yaxis_d[2, :])

			plotObject[name + '_axis_z'].set_3d_properties(zaxis_d[0, :], zaxis_d[1, :])
			plotObject[name + '_axis_z'].set_3d_properties(zaxis_d[2, :])
		else:
			plotObject[name + '_axis_x'], = ax.plot(xaxis_d[0, :], xaxis_d[1, :], xaxis_d[2, :], "r-")
			plotObject[name + '_axis_y'], = ax.plot(yaxis_d[0, :], yaxis_d[1, :], yaxis_d[2, :], "g-")
			plotObject[name + '_axis_z'], = ax.plot(zaxis_d[0, :], zaxis_d[1, :], zaxis_d[2, :], "b-")

def DrawRobot(ax, a, b, c, color, line, name=None, plotObject=None):
	if (plotObject == None):
		ax.plot(a, b, c, color, linewidth=line)
	else:
		if (name + '_drawrobot' in plotObject):
			plotObject[name + '_drawrobot'].set_data(a, b)
			plotObject[name + '_drawrobot'].set_3d_properties(c)
		else:
			plotObject[name + '_drawrobot'], = ax.plot(a, b, c, color, linewidth=line)


def Translation(dx, dy, dz):
	m = np.eye(4)
	m[0, 3] = dx
	m[1, 3] = dy
	m[2, 3] = dz
	m[3, 3] = 1.0
	return m


def RotationZ(theta):
	m = np.array([[math.cos(theta), -math.sin(theta), 0.0, 0.0],
				  [math.sin(theta), math.cos(theta), 0.0, 0.0],
				  [0.0, 0.0, 1.0, 0.0],
				  [0.0, 0.0, 0.0, 1.0]])
	return m


def RotationX(theta):
	m = np.array([[1.0, 0.0, 0.0, 0.0],
				  [0.0, math.cos(theta), - math.sin(theta), 0.0],
				  [0.0, math.sin(theta), math.cos(theta), 0.0],
				  [0.0, 0.0, 0.0, 1.0]])
	return m


def RotationY(theta):
	m = np.array([[math.cos(theta), 0.0, math.sin(theta), 0.0],
				  [0.0, 1.0, 0.0, 0.0],
				  [- math.sin(theta), 0.0, math.cos(theta), 0.0],
				  [0.0, 0.0, 0.0, 1.0]])
	return m


# Converts an angle in radians to another form and checks that servos are in bounds
# radianValue -> angle to convert, in radians
# servo_name -> string that identifies the server, eg. 'leftAnkleLateral'
# servo_info -> dictionary of the form {servo_name:[servo_resolution,lower_limit_in_radians,upper_limit_in_radians], ...}
# buffer_angle -> number of radians that must remain between the servo position and its absolute limit
# output_mode -> one of 'degree', 'tick', or 'radian' (with radian being the default)
def ConvertOutput(radianValue, servo_name, servo_info, buffer_angle, output_mode):
	servo_resolution = servo_info[servo_name][0]
	lowLimit = servo_info[servo_name][1]
	highLimit = servo_info[servo_name][2]
	tickValue = -1
	degreeValue = radianValue * 180.0 / math.pi

	if radianValue < lowLimit + buffer_angle or radianValue > highLimit - buffer_angle:
		print('ERROR: Servo ' + servo_name + ' is outside of its safety bounds (angle: ' + str(radianValue) + ')')
		quit()

	if servo_resolution == 4096:
		tickValue = radianValue * 4096.0 / (2.0 * math.pi) + 2048
	elif servo_resolution == 1024:
		if radianValue < -5.0 * math.pi / 6.0 or radianValue > 5.0 * math.pi / 6.0:
			print('ERROR: Servo' + servo_name + ' is in the dead zone (angle: ' + str(radianValue) + ')')
			quit()
		else:
			tickValue = radianValue * 1024.0 / (5.0 * math.pi / 3.0) + 512
	else:
		print('ERROR: Bad servo resolution (' + str(servo_resolution) + ') given for ' + servo_name)
		quit()

	if output_mode == 'degree':
		return degreeValue
	elif output_mode == 'tick':
		return tickValue
	else:
		return radianValue