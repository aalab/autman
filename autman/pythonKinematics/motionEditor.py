import math
import numpy as np
import argparse
import sys
import re

import json

import Trajectory as traj
import UDPSocket as udp
import utility as util

import robotModelling as rm

#servoNames = ['LeftShoulderLateral', 'LeftShoulderFrontal', 'LeftElbowLateral', 'RightShoulderLateral',
#			  'RightShoulderFrontal', 'RightElbowLateral', 'NeckTransversal', 'NeckLateral', 'LeftHipTransversal',
#			  'LeftHipFrontal', 'LeftHipLateral', 'LeftKneeLateral', 'LeftAnkleLateral', 'LeftAnkleFrontal',
#			  'RightHipTransversal', 'RightHipFrontal', 'RightHipLateral', 'RightKneeLateral', 'RightAnkleLateral',
#			  'RightAnkleFrontal']

servoNames = []


def printAngles(angles):
	for i in angles:
		print("%30s %f" % (i, angles[i]))


def main(argv=None):
	round_digits = 3

	parser = argparse.ArgumentParser()
	parser.add_argument('-t', '--trajectory', default='traj', help="name of the trajectory")
	parser.add_argument('-v', '--verbose', help="enable verbose output", action='count', default=0)
	parser.add_argument('-i', '--ip', help="host ip")
	parser.add_argument('-p', '--port', type=int, help='port number of server', default='1313')
	parser.add_argument('-s', '--save', help='save file name', default='default')

	if (argv == None):
		argv = sys.argv[1:]

	args = parser.parse_args(argv)

	keyframes = []
	moveTimes = []

	if (args.ip):
		sock = udp.UDPSocket(args.ip, args.port)
		done = False
		print(
			"Commands are: \'Enter\' to save the current position as a keyframe with the default time proportion of 1")
		print("    \'NUMBER+Enter\' to save the current position as a keyframe with a time proportion of NUMBER")
		print("    \'l+Enter'\' to list the servo angles of the last keyframe")
		print("    \'d+Enter'\' to delete the last keyframe")
		print("    \'on +servoNames +Enter'\' to turn on and off motors. (* means all servos)")
		print("    \'off +servoNames +Enter'\' to turn on and off motors. (* means all servos)")
		print("    \'q+Enter\' to save and quit")
		print("Note: time proportion will be ignored for the first keyframe (because you start there)")

		while not done:
			userInput = input("Enter command: ")
			inputIsNumber = util.isNumber(userInput)
			arguments = userInput.split(' ')
			if userInput == "" or inputIsNumber:
				currMoveTime = 1
				if inputIsNumber:
					currMoveTime = float(userInput)

				print("Reading Angles...")
				msg = '{"name":"Arash","command":"ReadState"}'
				if (args.verbose > 0):
					print('Send', msg)
				sock.SendMessage(msg)

				response = sock.ReceiveMessage(2.0)
				if (response == None):
					print("WARNING: Read failed. Keyframe not recorded. Robot server restart recommended.")
				else:
					reply, endpoint = response
					if (args.verbose > 0):
						print('Reply', reply)
					replyString = str(reply, 'ascii')
					replyObject = json.loads(replyString)

					angles = {}
					valuesValid = True
					deadZone = False
					for i in replyObject['data']:
						angle = replyObject['data'][i]['angle']
						if i not in servoNames:
							servoNames.append(i)
						angles[i] = angle
						if angle == 'NaN':
							print("WARNING: NaNs detected. Keyframe not recorded. Robot server restart recommended.")
							valuesValid = False
							break
						if abs(round(angle, 3)) == 2.618 or abs(round(angle, 3)) == 3.142:
							deadZone = True

					if valuesValid and deadZone:
						print(
							"WARNING: One or more servos may be in a dead zone. Or maybe you're skirting a dead zone. Or maybe the robot server needs a restart.")
						resolved = False
						while not resolved:
							userInput = input(
								"Press \'k+Enter\' to keep the keyframe, \'d+Enter\' to delete it, or \'l+Enter\' to list the angles of the last keyframe: ")
							if userInput == "k":
								resolved = True
							elif userInput == "d":
								resolved = True
								valuesValid = False
								print("Keyframe discarded")
							elif userInput == "l":
								printAngles(angles)
							else:
								print("Unknown command received")

					if valuesValid:
						keyframes.append(angles)
						moveTimes.append(currMoveTime)
						print("Keyframe saved")

			elif userInput == "l":
				if len(keyframes) > 0:
					printAngles(keyframes[-1])
				else:
					print("No previous keyframe to print")

			elif userInput == "d":
				if len(keyframes) > 0:
					keyframes.pop()
					moveTimes.pop()
					print("Last keyframe deleted")
				else:
					print("No previous keyframe to delete")


			elif arguments[0] == "on":
				for servo in range(1, len(arguments)):
					if (arguments[servo] == "*"):
						for servoName in servoNames:
							udp.transmitData(args.ip, args.port, '{"name":"Arash","command": "Actuator","params":{"name":"%s","mode":"On"}}' % (servoName))
						break
					else:
						udp.transmitData(args.ip, args.port, '{"name":"Arash","command": "Actuator","params":{"name":"%s","mode":"On"}}' % (arguments[servo]))

			elif arguments[0] == "off":
				for servo in range(1, len(arguments)):
					if (arguments[servo] == "*"):
						for servoName in servoNames:
							udp.transmitData(args.ip, args.port, '{"name":"Arash","command": "Actuator","params":{"name":"%s","mode":"Off"}}' % (servoName))
						break
					else:
						udp.transmitData(args.ip, args.port, '{"name":"Arash","command": "Actuator","params":{"name":"%s","mode":"Off"}}' % (arguments[servo]))

			elif arguments[0] == "play" or arguments[0] == "p":
				f = int(arguments[1])
				for x in keyframes[f]:
					udp.transmitData(args.ip, args.port, '{"name": "Arash", "command": "Actuator", "params":{"command":"Position","name":"%s","angle":%f,"speed":500,"gainP": 32,"gainI": 0,"gainD": 0 }  }' % (servoNames[x], keyframes[f][x]))

			elif userInput == "q":
				print("Quitting program")
				done = True

			else:
				print("Unknown command received")

		if (len(keyframes) <= 0):
			print("WARNING: No keyframes. Producing empty trajectory file. Trajectory will not be transmitted.")
		elif (len(keyframes) == 1):
			print("WARNING: Only one keyframe. Copying it to beginning and end of trajectory.")
			keyframes.append(keyframes[0])
			moveTimes.append(1)

		totalMoveTime = 0.0
		for i in range(len(moveTimes)):
			if i != 0:
				totalMoveTime = totalMoveTime + moveTimes[i]
		timeSoFar = 0.0
		keyTimes = []
		for i in range(len(moveTimes)):
			if i != 0:
				timeSoFar = timeSoFar + moveTimes[i]
			keyTimes.append(timeSoFar / totalMoveTime)
		if len(keyTimes) > 0:
			keyTimes[0] = 0.0
			keyTimes[-1] = 1.0

		trajectories = []
		for j in servoNames:
			trajectories.append(traj.Trajectory(args.trajectory, j, [
				(round(float(keyframes[i][j]), round_digits), round(keyTimes[i], round_digits)) for i in range(len(keyframes))
			]))

		fn = args.save
		if (not args.save.endswith('.info')):
			fn = fn + '.info'

		f = open(fn, 'w')

		f.write('trajectories\n')
		f.write('{\n')
		f.write('    ' + args.trajectory + '\n')
		f.write('    {\n')

		for t in trajectories:
			msg = t.ToConfigString(2)
			f.write(msg)
		f.write('    }\n')
		f.write('}\n')

		f.close()
		print("Trajectory saved to " + fn)

		# Transmit trajectory by UDP
		if (len(keyframes) > 0):
			transmitSuccessful = udp.transmitTrajectory(args.ip, args.port, trajectories, args.verbose > 0)

			if (transmitSuccessful):
				print("Trajectory transmitted to " + args.ip + " port " + str(
					args.port) + " with name " + args.trajectory)
			else:
				print(
					"WARNING: Transmission failed. Restart the robot server and send the trajectory with loadTrajectory.py.")

	else:
		print("ERROR: A valid IP must be entered")


if (__name__ == '__main__'):
	main()
