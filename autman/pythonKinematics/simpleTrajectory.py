import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import argparse
import sys

import Trajectory as traj
import UDPSocket as udp

import robotModelling as rm

#Mapping from output of IK array index to joint name of the leg
HipTransversal, HipFrontal, HipLateral, KneeLateral, AnkleLateral, AnkleFrontal = ( 0, 1, 2, 3, 4, 5 )
ShoulderLateral, ShoulderFrontal, ElbowLateral = ( 0, 1, 2 )

def main( argv = None ):
    parser = argparse.ArgumentParser()
    parser.add_argument( '-t', '--trajectory', default='Walk', help="name of the trajectory" )
    parser.add_argument( '-v', '--verbose', help="enable verbose output", action='count', default=0 )
    parser.add_argument( '-i', '--ip', help="host ip" )
    parser.add_argument( '-p', '--port', type=int, help='port number of server', default='1313' )
    parser.add_argument( '-s', '--save', help='save file name' )
    parser.add_argument( '-r', '--robot', default='robot', help='name of the robot' )
    
    if ( argv == None ):
        argv = sys.argv[1:] 
    
    args = parser.parse_args( argv )
    
    #robotImportName = args.robot + "Model"
    # trajectory file name
    #if ( args.robot != None ):
    #    eval('import ' + robotImportName + ' as rm')
    #else:
    #    print("ERROR: unknown robot model")

    robot = rm.Robot(args.robot)

    #Adjustable parameters
    output_mode = 'radian' #options are 'degree' or 'tick' or 'radian' (default)
    round_digits = 3
    buffer_angle = 0.2
    tolerance = 0.000001
    cycle_time = 1000000
    num_cycles = 10

    max_height_time_proportion = 0.1 # % time spent in each max-height phase (2 total)
    transition_time_proportion = 0.1 # % time spent in each low-high transition phase (4 total)

    x_step_distance = 0.03
    y_step_distance = 0.0
    turn_hip_angle = 0.2 #0.0*math.pi/180.0
    max_step_height = 0.05

    default_x_offset = 0.01
    default_y_offset = 0.02
    default_z_offset = -0.5
    default_hip_pitch = 8.0*math.pi/180.0
    default_ankle_pitch = 0.0*math.pi/180.0

    weight_transfer_ankle_roll = 0.0*math.pi/180.0
    weight_transfer_hip_roll = 7.0*math.pi/180.0
    weight_transfer_y = 0.02

    #Non-adjustable parameters
    step_air_time = 2.0*transition_time_proportion+max_height_time_proportion
    both_feet_down_time = 0.5 - step_air_time
    base_y = robot.robot_torso_hip_distance_y + default_y_offset

    #Begin processing
    if max_height_time_proportion*2 + transition_time_proportion*4 > 1.0 + tolerance:
        print("ERROR: total air time proportion (" + str(max_height_time_proportion*2 + transition_time_proportion*4) + ") exceeds 1.0")
        quit()

    keyTimes = []
    keyTimes.append(both_feet_down_time/2.0 + 0.25 - max_height_time_proportion/2.0 - transition_time_proportion)
    keyTimes.append(both_feet_down_time/2.0 + 0.25 - max_height_time_proportion/2.0)
    keyTimes.append(both_feet_down_time/2.0 + 0.25 + max_height_time_proportion/2.0)
    keyTimes.append(both_feet_down_time/2.0 + 0.25 + max_height_time_proportion/2.0 + transition_time_proportion)
    keyTimes.append(both_feet_down_time/2.0 + 0.75 - max_height_time_proportion/2.0 - transition_time_proportion)
    keyTimes.append(both_feet_down_time/2.0 + 0.75 - max_height_time_proportion/2.0)
    keyTimes.append(both_feet_down_time/2.0 + 0.75 + max_height_time_proportion/2.0)
    keyTimes.append(both_feet_down_time/2.0 + 0.75 + max_height_time_proportion/2.0 + transition_time_proportion)

    leftLegKeyAngles = []
    rightLegKeyAngles = []
    leftArmKeyAngles = []
    rightArmKeyAngles = []

    #position 0 - Shift Weight Left
    leftLegPosn = [default_x_offset + x_step_distance/2.0, base_y - weight_transfer_y - y_step_distance, default_z_offset]
    rightLegPosn = [default_x_offset - x_step_distance/2.0, -base_y - weight_transfer_y, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 1 - Lift Right Foot
    leftLegPosn = [default_x_offset + x_step_distance/2.0 - x_step_distance*transition_time_proportion/step_air_time, base_y - weight_transfer_y - y_step_distance, default_z_offset]
    rightLegPosn = [default_x_offset - x_step_distance/2.0 + x_step_distance*transition_time_proportion/step_air_time, -base_y - weight_transfer_y, default_z_offset + max_step_height]
    llik = robot.LeftLegCalcAngles(leftLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, weight_transfer_hip_roll, weight_transfer_ankle_roll, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 2 - Swing Right Foot Forward
    leftLegPosn = [default_x_offset + x_step_distance/2.0 - x_step_distance*(transition_time_proportion + max_height_time_proportion)/step_air_time, base_y - weight_transfer_y - y_step_distance, default_z_offset]
    rightLegPosn = [default_x_offset - x_step_distance/2.0 + x_step_distance*(transition_time_proportion + max_height_time_proportion)/step_air_time, -base_y - weight_transfer_y - y_step_distance, default_z_offset + max_step_height]
    llik = robot.LeftLegCalcAngles(leftLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, weight_transfer_hip_roll, weight_transfer_ankle_roll, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 3 - Land Right Foot
    leftLegPosn = [default_x_offset - x_step_distance/2.0, base_y - weight_transfer_y, default_z_offset]
    rightLegPosn = [default_x_offset + x_step_distance/2.0, -base_y - weight_transfer_y - y_step_distance, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 4 - Shift Weight Right
    leftLegPosn = [default_x_offset - x_step_distance/2.0, base_y + weight_transfer_y, default_z_offset]
    rightLegPosn = [default_x_offset + x_step_distance/2.0, -base_y + weight_transfer_y - y_step_distance, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 5 - Lift Left Foot
    leftLegPosn = [default_x_offset - x_step_distance/2.0 + x_step_distance*transition_time_proportion/step_air_time, base_y + weight_transfer_y, default_z_offset + max_step_height]
    rightLegPosn = [default_x_offset + x_step_distance/2.0 - x_step_distance*transition_time_proportion/step_air_time, -base_y + weight_transfer_y - y_step_distance, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, weight_transfer_hip_roll, weight_transfer_ankle_roll, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 6 - Swing Left Foot Forward
    leftLegPosn = [default_x_offset - x_step_distance/2.0 + x_step_distance*(transition_time_proportion + max_height_time_proportion)/step_air_time, base_y + weight_transfer_y - y_step_distance, default_z_offset + max_step_height]
    rightLegPosn = [default_x_offset + x_step_distance/2.0 - x_step_distance*(transition_time_proportion + max_height_time_proportion)/step_air_time, -base_y + weight_transfer_y - y_step_distance, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, weight_transfer_hip_roll, weight_transfer_ankle_roll, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )

    #position 7 - Land Left Foot
    leftLegPosn = [default_x_offset + x_step_distance/2.0, base_y + weight_transfer_y - y_step_distance, default_z_offset]
    rightLegPosn = [default_x_offset - x_step_distance/2.0, -base_y + weight_transfer_y, default_z_offset]
    llik = robot.LeftLegCalcAngles(leftLegPosn, -turn_hip_angle, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    rlik = robot.RightLegCalcAngles(rightLegPosn, 0.0, default_hip_pitch, default_ankle_pitch, 0.0, 0.0, buffer_angle, output_mode)
    leftLegKeyAngles.append(llik)
    rightLegKeyAngles.append(rlik)
    
    laik = [ 0.0277045, 1.47885, -1.19181 ]
    raik = [ -0.0183151, -1.52775, 1.034 ]
    leftArmKeyAngles.append( laik )
    rightArmKeyAngles.append( raik )
    
    if(args.verbose):
        print('Times: ' + str(keyTimes) + '\n')
        print('Left leg angles: ' + str(leftLegKeyAngles) + '\n')
        print('Right leg angles: ' + str(rightLegKeyAngles) + '\n')
        
    trajectories = []
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftShoulderLateral', [ ( leftArmKeyAngles[i][ShoulderLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftShoulderFrontal', [ ( leftArmKeyAngles[i][ShoulderFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftElbowLateral', [ ( leftArmKeyAngles[i][ElbowLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )

    trajectories.append( traj.Trajectory( args.trajectory, 'RightShoulderLateral', [ ( rightArmKeyAngles[i][ShoulderLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightShoulderFrontal', [ ( rightArmKeyAngles[i][ShoulderFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightElbowLateral', [ ( rightArmKeyAngles[i][ElbowLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
     
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftHipTransversal', [ ( leftLegKeyAngles[i][HipTransversal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftHipFrontal', [ ( leftLegKeyAngles[i][HipFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftHipLateral', [ ( leftLegKeyAngles[i][HipLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftKneeLateral', [ ( leftLegKeyAngles[i][KneeLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftAnkleLateral', [ ( leftLegKeyAngles[i][AnkleLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'LeftAnkleFrontal', [ ( leftLegKeyAngles[i][AnkleFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightHipTransversal', [ ( rightLegKeyAngles[i][HipTransversal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightHipFrontal', [ ( rightLegKeyAngles[i][HipFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightHipLateral', [ ( rightLegKeyAngles[i][HipLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightKneeLateral', [ ( rightLegKeyAngles[i][KneeLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightAnkleLateral', [ ( rightLegKeyAngles[i][AnkleLateral], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )
    trajectories.append( traj.Trajectory( args.trajectory, 'RightAnkleFrontal', [ ( rightLegKeyAngles[i][AnkleFrontal], keyTimes[i] ) for i in range(len(keyTimes) ) ] ) )

    if ( args.save ):
        fn = args.save
        if ( not args.save.endswith( '.info' ) ):
            fn = fn + '.info'
        
        f = open( fn, 'w')
        f.write('trajectories\n')
        f.write('{\n')
        f.write('    Walk\n')
        f.write('    {\n')
        f.write('        ;Python parameters\n')
        f.write('        ;max_height_time proportion = ' + str(max_height_time_proportion) + '\n')
        f.write('        ;transition_time_proportion = ' + str(transition_time_proportion) + '\n')
        f.write('        ;x_step_distance = ' + str(x_step_distance) + '\n')
        f.write('        ;max_step_height = ' + str(max_step_height) + '\n')
        f.write('        ;default_x_offset = ' + str(default_x_offset) + '\n')
        f.write('        ;default_y_offset = ' + str(default_y_offset) + '\n')
        f.write('        ;default_z_offset = ' + str(default_z_offset) + '\n')
        f.write('        ;default_hip_pitch = ' + str(default_hip_pitch) + '\n')
        f.write('        ;default_ankle_pitch = ' + str(default_ankle_pitch) + '\n')
        f.write('        ;default_hip_transversal = ' + str(-turn_hip_angle) + '\n')
        f.write('        ;weight_transfer_ankle_roll = ' + str(weight_transfer_ankle_roll) + '\n')
        f.write('        ;weight_transfer_hip_roll = ' + str(weight_transfer_hip_roll) + '\n')
        f.write('        ;weight_transfer_y = ' + str(weight_transfer_y) + '\n\n')

        for t in trajectories:
            msg = t.ToConfigString( 2 )
            f.write(msg)
        f.write('    }\n')
        f.write('}\n')

        f.close()

    if ( args.ip ):
        sock = udp.UDPSocket( args.ip, args.port )

        for t in trajectories:
            msg = t.ToCommandString( )
            if ( args.verbose > 0 ):
                print('Send', msg )
            sock.SendMessage( msg )
            reply = sock.ReceiveMessage( 0.2 )
            if ( args.verbose > 0 ):
                print('Reply', reply )

if ( __name__ == '__main__' ):
    main()

#i = 0
#while(True):
#    robot.applyPositionLeftArm(10.0/180.0 * math.pi, 30.0/180.0 * math.pi, -30.0/180.0 * math.pi)
#    robot.applyPositionRightArm(-10.0/180.0 * math.pi, -30.0/180.0 * math.pi, 30.0/180.0 * math.pi)
#    robot.applyPositionLeftLeg(l_key_angles[i][0], l_key_angles[i][1], l_key_angles[i][2], l_key_angles[i][3], l_key_angles[i][4], l_key_angles[i][5])
#    robot.applyPositionRightLeg(r_key_angles[i][0], r_key_angles[i][1], r_key_angles[i][2], r_key_angles[i][3], r_key_angles[i][4], r_key_angles[i][5])
#    robot.applyPositionHead(0.0, -30.0*math.pi/180)
#    i = (i+1) % num_positions
#    
#    fig = plt.figure()
#    ax = fig.add_subplot(111, projection='3d')
#    ax.set_aspect('equal')
#    robot.draw(ax, True)
#    plt.show()
#    time.sleep(0)
