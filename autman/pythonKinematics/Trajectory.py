# Trajectory 

class Trajectory:
	def __init__( self, name, jointName, points ):
		self.name = name
		self.jointName = jointName
		self.points = points
		pass
		
	def ToCommandString( self ):
		msg = '{"name":"Arash","command":"Actuator","params":{"command":"Trajectory","name":"' + self.jointName + '",'
		msg += '"trajectory":"' + self.name + '","frames":['
		for i in range(len(self.points) ):
			a,t = self.points[i]
			if i != 0:
				msg = msg + ','
			msg = msg + '{"angle":' + str(a) + ', "time":' + str(t) + '}'
		msg = msg + ']}}'
		return msg

	def ToConfigString( self, indent = 0 ):
		msg = ( '    ' * indent ) + self.jointName + '\n'
		msg = msg + ( '    ' * indent ) + "{" + '\n'
		for p in self.points:
			s,t = p
			msg = msg + ( '    ' * (indent + 1 ) ) + str(s) + ' ' + str(t) + '\n'
		msg = msg + ( '    ' * indent ) + '}' + '\n\n'
		return msg

"""def startTrajectory(trajName, cycleTime, numCycles):
    msg = 'Trajectory=' + trajName + ' CycleTimeUSec = ' + str(cycleTime) + ' Cycles=' + str(100) + '\n'
    sock.sendto(bytes(msg, 'UTF-8'), (UDP_IP, UDP_PORT))
    if wait_for_reply:
        getReply()"""
