import socket
import select

reply_wait_time = 0.2


class UDPSocket:
	def __init__(self, ip, port):
		self.ip = ip
		self.port = port
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.socket.setblocking(False)

	def SendMessage(self, msg):
		self.socket.sendto(bytes(msg, 'ASCII'), (self.ip, self.port))

	def ReceiveMessage(self, timeout):
		reply = None
		ready = select.select([self.socket], [], [], timeout)
		if (ready[0]):
			reply = self.socket.recvfrom(4096)
		return reply


# return True if successful, False otherwise
def transmitTrajectory(ip, port, trajectories, verbose=False):
	sock = UDPSocket(ip, port)
	successful = True

	for t in trajectories:
		msg = t.ToCommandString()
		if (verbose):
			print('Send', msg)
		sock.SendMessage(msg)
		reply = sock.ReceiveMessage(reply_wait_time)
		if (verbose):
			print('Reply', reply)
		if reply == None:
			successful = False

	return successful


# return True if successful, False otherwise
def transmitData(ip, port, data, verbose=False):
	sock = UDPSocket(ip, port)
	successful = True

	if (verbose):
		print('Send', data)
	sock.SendMessage(data)
	reply = sock.ReceiveMessage(reply_wait_time)
	if (verbose):
		print('Reply', reply)
	if reply == None:
		successful = False
	return successful
