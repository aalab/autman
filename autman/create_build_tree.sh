#!/bin/sh

if [ ! -d Build ]; then
   mkdir Build
fi

if [ ! -d Build/Debug ]; then
   mkdir Build/Debug
fi

if [ ! -d Build/Release ]; then
   mkdir Build/Release
fi

rm -fr Build/Debug/CMake* Build/Debug/cmake*

rm -fr Build/Release/CMake* Build/Release/cmake*

(cd Build/Debug; cmake -DCMAKE_BUILD_TYPE=Debug)

(cd Build/Release; cmake -DCMAKE_BUILD_TYPE=Release)

