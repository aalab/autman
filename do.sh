#!/bin/bash

function argumentError {
	echo "Usage: $0 <from> <to>"
	echo "Options:"
	echo "<from>    : Source host"
	echo "<to>      : SSH URI. Eg: jose@192.168.123.1"
	exit 1
}

if [ -z "$1" ]; then
	argumentError
fi

if [ -z "$2" ]; then
	argumentError
fi

./pull.sh $1
rc=$?
if [ $rc != 0 ]; then
	echo "Error pulling files from computer"
	exit 2
fi
./compile.sh
rc=$?
if [ $rc != 0 ]; then
	echo "Error compiling"
	exit 2
fi
./push.sh $2
