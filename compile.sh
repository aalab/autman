#!/bin/bash

scriptpath="`dirname \"$0\"`"
scriptpath="`( cd \"$scriptpath\" && pwd )`"

writeHelp()
{
	echo "$0 <to> [options]"
	echo ""
	echo "Options:"
	echo "<to>            : RSync connection URI of the computer where it should be pushed"
	echo "--cmake         : CMake before compile"
	echo "--no-vm         : Flag to not rsync the vison module"
	echo "--no-darwin     : Flag to not rsync the darwin robot server"
	echo "--no-teleop     : Flag to not rsync the teleop"
}

cmake=1
while test $# -gt 0; do
	case "$1" in
		--cmake) cmake= ;;
		--no-vm) vm=1 ;;
		--no-darwin) darwin=1 ;;
		--no-teleop) teleop=1 ;;
		-h)
			writeHelp
			exit 0
		;;
		--help)
			writeHelp
			exit 0
		;;
		*)
			if [ -z $to ] ; then
				to=$1
			else
				echo "Unexpected option ${flag}"
				exit 1
			fi
		;;
	esac
	shift
done

if [ -z $vm ]; then
	echo ${scriptpath}/visionmodule/bin/debug/
	cd ${scriptpath}/visionmodule/bin/debug/
	if [ -z $cmake ] ; then
		./do_cmake.sh
	fi
	make vision_module
fi

if [ -z $darwin ]; then
	cd ${scriptpath}/darwin/bin/debug/
	if [ -z $cmake ] ; then
		./do_cmake.sh
	fi
	make darwin_server
fi

if [ -z $teleop ]; then
	echo ${scriptpath}/modules/teleop/bin/debug/
	cd ${scriptpath}/modules/teleop/bin/debug/
	if [ -z $cmake ] ; then
		./do_cmake.sh
	fi
	make teleop
fi


