
#include "compilerdefinitions.h"

#include "config.h"
#include "darwinudpserver.h"

#include <minIni.h>
#include <Action.h>
#include <Head.h>
#include <MotionManager.h>
#include <LinuxCM730.h>
#include <LinuxMotionTimer.h>
#include <CM730.h>

#include <iostream>

#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <Walking.h>

void signalsHandler(const boost::system::error_code& error, int signal_number)
{
	VERBOSE("Signal!");
}

int main(int argc, char* argv[])
{
	namespace po = boost::program_options;

	std::string configFile, modality;
	configFile += "darwin_server_";
	configFile += std::getenv("USER");
	configFile += + ".info";

#ifndef DEBUG
	VERBOSE("Debugging build");
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string.")
		("help,h", "print help message.")
		("config_file,c", po::value<std::string>(&configFile)->default_value(configFile), "darwin config file.");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "RoboCup Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	auto ptServer = pt.get_child_optional("server");
	if (!ptServer)
	{
		ERROR("'server' configuration was not found.");
		return 2;
	}

	auto ptMove = pt.get_child_optional("move");
	if (!ptMove)
	{
		ERROR("'move' configuration was not found.");
		return 3;
	}

	std::string iniPath = std::string(CONFIG_DIR) + std::string("/darwin/") + std::getenv("USER") + ".ini";

	Robot::LinuxCM730 linuxCm730(pt.get<std::string>("device", "/dev/ttyUSB0").c_str());
	Robot::CM730 cm730(&linuxCm730);

	VERBOSENEL("Reading ini \"" << iniPath << "\"");
	minIni* ini = new minIni(iniPath);
	VERBOSEB(" OK.");

	VERBOSE("Initializing MotionManager...");

	unsigned int tryConnect = 1;

	ERROR("Opening port: " << linuxCm730.GetPortName());
	while(Robot::MotionManager::GetInstance()->Initialize(&cm730) == false)
	{
		ERROR("Trying again ...");
		if(Robot::MotionManager::GetInstance()->Initialize(&cm730) == false)
		{
			ERROR("Fail to initialize Motion Manager!");
			return 4;
		}

		ERROR("Trying to connect to CM730 [" << tryConnect << "] ...");
		tryConnect++;
		sleep(1);
	}

	Robot::MotionManager::GetInstance()->AddModule(Robot::Action::GetInstance());
	Robot::MotionManager::GetInstance()->AddModule(Robot::Walking::GetInstance());
	Robot::MotionManager::GetInstance()->AddModule(Robot::Head::GetInstance());

	Robot::LinuxMotionTimer motionTimer(Robot::MotionManager::GetInstance());
	motionTimer.Start();

	int firm_ver = 0;
	if(cm730.ReadByte(Robot::JointData::ID_HEAD_PAN, Robot::MX28::P_VERSION, &firm_ver, 0)  != Robot::CM730::SUCCESS)
	{
		ERROR("Can't read firmware version from Dynamixel ID " << Robot::JointData::ID_HEAD_PAN << "!");
		return 5;
	}

	std::string motionFilePath = std::string(CONFIG_DIR) + std::string("/darwin/") + (std::getenv("USER") + std::string("_motion_4096.bin")) ;

	if(0 < firm_ver && firm_ver < 27)
	{
#ifdef MX28_1024
		Action::GetInstance()->LoadFile(MOTION_FILE_PATH);
#else
		ERROR("MX-28's firmware is not support 4096 resolution!!");
		ERROR("Upgrade MX-28's firmware to version 27(0x1B) or higher.");
		return 6;
#endif
	}
	else if(27 <= firm_ver)
	{
#ifdef MX28_1024
		ERROR("MX-28's firmware is not support 1024 resolution!!");
		ERROR("Remove '#define MX28_1024' from 'MX28.h' file and rebuild.");
		return 7;
#else
		VERBOSE("Loading '" << motionFilePath << "' as motion file...");
		if (!Robot::Action::GetInstance()->LoadFile((char*)motionFilePath.c_str()))
		{
			ERROR("Can't load " << motionFilePath);
			return 7;
		}
		VERBOSE("Motion file loaded.");
#endif
	}
	else
	{
		ERROR("Firmware not supported");
		return 8;
	}

	VERBOSENEL("Loading MotionModule configuration...");
	Robot::MotionManager::GetInstance()->LoadINISettings(ini);
	VERBOSEB(" OK.");

	VERBOSENEL("Loading Head configuration...");
	Robot::Head::GetInstance()->LoadINISettings(ini);
	Robot::Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
	VERBOSEB(" OK.");

	VERBOSENEL("Loading Walking configuration...");
	Robot::Walking::GetInstance()->LoadINISettings(ini);
	VERBOSE("Configuration:");
	VERBOSEB("\tX_OFFSET:" << Robot::Walking::GetInstance()->X_OFFSET);
	VERBOSEB("\tY_OFFSET:" << Robot::Walking::GetInstance()->Y_OFFSET);
	VERBOSEB("\tA_OFFSET:" << Robot::Walking::GetInstance()->A_OFFSET);
	VERBOSEB("\tHIP_PITCH_OFFSET:" << Robot::Walking::GetInstance()->HIP_PITCH_OFFSET);
	VERBOSEB(" OK.");

	VERBOSENEL("Enabling Action...");
	Robot::Action::GetInstance()->m_Joint.SetEnableBody(true, true);
	Robot::MotionManager::GetInstance()->SetEnable(true);
	VERBOSEB(" OK.");

	VERBOSENEL("Entering in a safe position...");
	Robot::Action::GetInstance()->Start(15);
	VERBOSEB(" Waiting end of action.");
	while(Robot::Action::GetInstance()->IsRunning())
		usleep(16*1000);
	VERBOSEB(" OK.");


	VERBOSENEL("Standing...");
	Robot::Action::GetInstance()->Start(16);
	VERBOSEB(" Waiting end of action.");
	while(Robot::Action::GetInstance()->IsRunning())
		usleep(8*1000);
	VERBOSEB(" OK.");

	boost::asio::io_service ioService;

	darwin::DarwinUDPServer server(ioService, (*ptServer).get<unsigned int>("port", 1313));

	server.configuration.xMax = (*ptMove).get<double>("x_max");
	server.configuration.yMax = (*ptMove).get<double>("y_max");
	server.configuration.thetaMax = (*ptMove).get<double>("theta_max");
	server.configuration.hipForwardPitchOffset = Robot::Walking::GetInstance()->HIP_PITCH_OFFSET;
	server.configuration.hipBackwardPitchOffset = (*ptMove).get<double>("hip_backward_pitch_offset");

	boost::asio::signal_set signals(ioService);
	signals.add(SIGINT);
	signals.add(SIGTERM);
#if defined(SIGQUIT)
	signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
	signals.async_wait(signalsHandler);

	server.start();

	return 0;
}