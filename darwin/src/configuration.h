/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 18, 2015
 **/

#ifndef DARWIN_CONFIGURATION_HPP
#define DARWIN_CONFIGURATION_HPP

namespace darwin
{
class Configuration
{
public:
	double xMax;
	double yMax;
	double thetaMax;
	double hipForwardPitchOffset;
	double hipBackwardPitchOffset;
};
}


#endif //DARWIN_CONFIGURATION_HPP
