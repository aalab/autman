/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 29, 2015
 */

#include "darwinudpserver.h"
#include "compilerdefinitions.h"

#include <services/data/robot/move.h>
#include <services/base/json.h>

#include <Walking.h>
#include <Action.h>
#include <Head.h>

#include <iostream>

#define RATIO 1.0

#define FULLRANGE(x) ( (x < -1.0) ? -1.0 : ( (x > 1.0) ? 1.0 : x ) )

namespace rdata = services::data::robot;

darwin::DarwinUDPServer::DarwinUDPServer(boost::asio::io_service &io_service, unsigned int port)
	: UDPServer(io_service, port), isWalking(false)
{
	this->defaultPeriodTime = Robot::Walking::GetInstance()->PERIOD_TIME;
	Robot::Walking::GetInstance()->m_Joint.SetEnableBody(true, true);
}

void darwin::DarwinUDPServer::processCommand(Json::Value &jsonCommand)
{
	std::string command = jsonCommand["command"].asString();
	VERBOSE("Command: " << command);
	if (!Robot::Action::GetInstance()->IsRunning())
	{
		if (command == "Head")
		{
			rdata::Head head;
			services::json::factory(jsonCommand, head);

			Robot::Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
			Robot::Head::GetInstance()->MoveByAngle(head.pan, head.tilt);

			this->send("{\"success\":true}");
		}
		else if (command == "Move")
		{
			rdata::Move move;
			services::json::factory(jsonCommand, move);

			/*
			if (move.motion)
			{
				this->stopWalking();
				//
				Robot::Action::GetInstance()->m_Joint.SetEnableBody(true, true);
				// Robot::MotionManager::GetInstance()->SetEnable(true);
				//
				VERBOSE("Starting motion " << move.motion);
				Robot::Action::GetInstance()->Start(move.motion);
				while (Robot::Action::GetInstance()->IsRunning())
				{
					this->waitAction();
				}
				VERBOSE("Stopped motion " << move.motion);
			}
			else
			{
			 */

			if (move.frames.empty())
			{
				ERROR("No frames to be applied.");
				this->send("{\"success\":false,\"message\":\"No frame specified.\"}");
			}
			else
			{
				services::data::robot::Frame &frame = move.frames[0];
				Robot::Walking::GetInstance()->X_MOVE_AMPLITUDE = FULLRANGE(frame.x) * this->configuration.xMax;
				Robot::Walking::GetInstance()->Y_MOVE_AMPLITUDE = FULLRANGE(frame.y) * this->configuration.yMax;
				Robot::Walking::GetInstance()->A_MOVE_AMPLITUDE = FULLRANGE(frame.theta) * this->configuration.thetaMax;

				VERBOSE("Walking params updated:");
				VERBOSEB("\t+ X: " << Robot::Walking::GetInstance()->X_MOVE_AMPLITUDE);
				VERBOSEB("\t+ Y: " << Robot::Walking::GetInstance()->Y_MOVE_AMPLITUDE);
				VERBOSEB("\t+ A: " << Robot::Walking::GetInstance()->A_MOVE_AMPLITUDE);
				VERBOSEB("\t+ X: " << frame.cycles);

				if ((frame.x == 0) && (frame.y == 0) && (frame.theta == 0))
				{
					this->stopWalking();
				}
				else
				{
					if (Robot::Walking::GetInstance()->X_MOVE_AMPLITUDE < 0)
						Robot::Walking::GetInstance()->HIP_PITCH_OFFSET = this->configuration.hipBackwardPitchOffset;
					else
						Robot::Walking::GetInstance()->HIP_PITCH_OFFSET = this->configuration.hipForwardPitchOffset;
					this->startWalking();

					if (frame.cycleTimeUSec != 0)
						Robot::Walking::GetInstance()->PERIOD_TIME = frame.cycleTimeUSec / 1000;

					if (frame.cycles > 0)
					{
						this->startWalking();
						usleep((Robot::Walking::GetInstance()->PERIOD_TIME * frame.cycles)*1000);
						this->stopWalking();
						Robot::Walking::GetInstance()->PERIOD_TIME = this->defaultPeriodTime;
					}
				}

				this->send("{\"success\":true}");
			}
			// }
		}
		else
		{
			this->send("{\"success\":false,\"message\":\"Unknown command.\"}");
		}
	}
	else
	{
		VERBOSE("Ignoring package due to Action runnning.");
		this->send("{\"success\":false,\"message\":\"There is one action running.\"}");
	}
}

void darwin::DarwinUDPServer::startWalking()
{
	if (!this->isWalking)
	{
		VERBOSE("Start walking");

		// Robot::Walking::GetInstance()->m_Joint.SetEnableBody(true, true);
		// Robot::Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);

		Robot::Walking::GetInstance()->Start();

		this->isWalking = true;
	}
}

void darwin::DarwinUDPServer::stopWalking()
{
	if (this->isWalking)
	{
		VERBOSE("Stop walking");
		if (Robot::Walking::GetInstance()->IsRunning())
		{
			Robot::Walking::GetInstance()->Stop();
			this->waitAction();
		}
		this->isWalking = false;
	}
}

void darwin::DarwinUDPServer::waitAction()
{
	usleep(8000);
}
