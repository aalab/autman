/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 29, 2015
 */

#ifndef DARWIN_DARWINUDPSERVER_H
#define DARWIN_DARWINUDPSERVER_H

#include <services/base/udpserver.h>
#include "configuration.h"

#ifndef override
#define override
#endif

namespace darwin
{
class DarwinUDPServer
	: public services::base::UDPServer
{
private:
	bool isWalking;
	double defaultPeriodTime;

	void startWalking();
	void stopWalking();

	void waitAction();

protected:
	virtual void processCommand(Json::Value &jsonCommand) override;
public:
	DarwinUDPServer(boost::asio::io_service &io_service, unsigned int port);

	Configuration configuration;
};
}


#endif //DARWIN_DARWINUDPSERVER_H
