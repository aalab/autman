// AutoLineTracker.h
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier

#ifndef __AUTOLINETRACKER_H__
#define __AUTOLINETRACKER_H__

#include "mjpg_streamer.h"
#include "LinuxDARwIn.h"
#include "MotionStatus.h"
// #include "../../../../libvideo/framebuffer.h"


class AutoLineTracker
{
	public:
		AutoLineTracker();
		
		static AutoLineTracker* GetInstance() {return m_UniqueInstance;}
		static int Initialize();
		static void Track();
		static int Initialize_from_videostream();
		static void Track_from_videostream(Robot::FrameBuffer* frameBuffer,Robot::Image* rgb_line_videostream);
		static void Standup();

	private:
		static AutoLineTracker* m_UniqueInstance;
		static Robot::Image* rgb_line;
		static Robot::FrameBuffer* frameBuffer;
		static minIni* ini;
		static mjpg_streamer* streamer;
		static Robot::ColorFinder* line_finder;
		static Robot::MarkerFinder* marker_finder;
		static Robot::LineTracker tracker;
		static Robot::LineFollower follower;
		static Robot::LinuxMotionTimer* motion_timer;
		static bool runOnce;
		static bool enterPressed;
};

#endif /* __AUTOLINETRACKER_H__ */