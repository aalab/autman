// AutoLineTracker.cpp
// Author: Tik Wai Poon a.k.a Kiral, Paul-Emile Crevier

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include "Camera.h"
#include "AutoLineTracker.h"
#include "mjpg_streamer.h"
#include "LinuxDARwIn.h"
#include "MotionManager.h"

#include "Voice.h"
// #include "TeleOperation.h"
// #include "../../../../libvideo/framebuffer.h"
// #include "../../../../src/framebuffer.h"

#ifdef MX28_1024
#define MOTION_FILE_PATH    "../../../Data/motion_1024.bin"
#else
#define MOTION_FILE_PATH    "../../../Data/motion_4096.bin"
#endif

#define INI_FILE_PATH       "../../../Data/config.ini"
// #define INI_FILE_PATH       "config.ini"
#define U2D_DEV_NAME        "/dev/ttyUSB0"
Robot::LinuxCM730 linux_cm730_autotrack(U2D_DEV_NAME);
Robot::CM730 cm730_autotrack(&linux_cm730_autotrack);

using namespace std;
using namespace Robot;

AutoLineTracker* AutoLineTracker::m_UniqueInstance = new AutoLineTracker();
Image* AutoLineTracker::rgb_line;
FrameBuffer* AutoLineTracker::frameBuffer = new Robot::FrameBuffer (Robot::Camera::WIDTH, Robot::Camera::HEIGHT);
minIni* AutoLineTracker::ini;
mjpg_streamer* AutoLineTracker::streamer;
ColorFinder* AutoLineTracker::line_finder;
MarkerFinder* AutoLineTracker::marker_finder;
LineTracker AutoLineTracker::tracker = LineTracker();
LineFollower AutoLineTracker::follower;
LinuxMotionTimer* AutoLineTracker::motion_timer;
bool AutoLineTracker::runOnce;
bool AutoLineTracker::enterPressed = false;


void change_current_dir_autotrack()
{
    char exepath[1024] = {0};
    if(readlink("/proc/self/exe", exepath, sizeof(exepath)) != -1)
	{
        if(chdir(dirname(exepath)))
			fprintf(stderr, "chdir error!! \n");
	}
}

AutoLineTracker::AutoLineTracker(){}

int AutoLineTracker::Initialize()
{
	runOnce=false;
	printf( "\nTRACKER: ===== Auto Line Tracker for DARwIn OP2 =====\n\n");

    change_current_dir_autotrack();

    rgb_line = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);

    ini = new minIni(INI_FILE_PATH);

    LinuxCamera::GetInstance()->Initialize(0);
    LinuxCamera::GetInstance()->LoadINISettings(ini);

    streamer = new mjpg_streamer(Camera::WIDTH, Camera::HEIGHT);

    line_finder = new ColorFinder();
    line_finder->LoadINISettings(ini);
    httpd::ball_finder = line_finder;

	marker_finder = new MarkerFinder();
	marker_finder->LoadINISettings(ini);
	httpd::marker_finder = marker_finder;
	
    follower = LineFollower();
	follower.DEBUG_PRINT = true;

	//////////////////// Framework Initialize ////////////////////////////
	if(MotionManager::GetInstance()->Initialize(&cm730_autotrack) == false)
	{
		printf("Fail to initialize Motion Manager!\n");
			return 0;
	}
    MotionManager::GetInstance()->LoadINISettings(ini);
    Walking::GetInstance()->LoadINISettings(ini);


    // MotionManager::GetInstance()->AddModule((MotionModule*)Action::GetInstance());//add for action module
	MotionManager::GetInstance()->AddModule((MotionModule*)Head::GetInstance());
	MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
    motion_timer->Start();
	


    /////////////////////////////////////////////////////////////////////


	MotionManager::GetInstance()->LoadINISettings(ini);


	int firm_ver = 0;
	if(cm730_autotrack.ReadByte(JointData::ID_HEAD_PAN, MX28::P_VERSION, &firm_ver, 0)  != CM730::SUCCESS)
	{
		fprintf(stderr, "TRACKER: Can't read firmware version from Dynamixel ID %d!! \n\n", JointData::ID_HEAD_PAN);
		exit(0);
	}

	if(0 < firm_ver && firm_ver < 27)
	{
#ifdef MX28_1024
		Action::GetInstance()->LoadFile(MOTION_FILE_PATH);
#else
		fprintf(stderr, "TRACKER: MX-28's firmware is not support 4096 resolution!! \n");
		fprintf(stderr, "TRACKER: Upgrade MX-28's firmware to version 27(0x1B) or higher.\n\n");
		exit(0);
#endif
	}
	else if(27 <= firm_ver)
	{
#ifdef MX28_1024
		fprintf(stderr, "TRACKER: MX-28's firmware is not support 1024 resolution!! \n");
		fprintf(stderr, "TRACKER: Remove '#define MX28_1024' from 'MX28.h' file and rebuild.\n\n");
		exit(0);
#else
		Action::GetInstance()->LoadFile((char*)MOTION_FILE_PATH);
#endif
	}
	else
		exit(0);

	

	
	Voice::Initialize("en-us+m7");
	Voice::Voice::Speak("Ready to Love All Humans");




	/////////////////////////////////////////////////////////////////////
	int n = 0;
	int param[JointData::NUMBER_OF_JOINTS * 5];
	int wGoalPosition, wStartPosition, wDistance;

	for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
	{
		wStartPosition = MotionStatus::m_CurrentJoints.GetValue(id);
		wGoalPosition = Walking::GetInstance()->m_Joint.GetValue(id);
		if( wStartPosition > wGoalPosition )
			wDistance = wStartPosition - wGoalPosition;
		else
			wDistance = wGoalPosition - wStartPosition;

		wDistance >>= 2;
		if( wDistance < 8 )
			wDistance = 8;

		param[n++] = id;
		param[n++] = CM730::GetLowByte(wGoalPosition);
		param[n++] = CM730::GetHighByte(wGoalPosition);
		param[n++] = CM730::GetLowByte(wDistance);
		param[n++] = CM730::GetHighByte(wDistance);
	}
	cm730_autotrack.SyncWrite(MX28::P_GOAL_POSITION_L, 5, JointData::NUMBER_OF_JOINTS - 1, param);	

	printf("TRACKER: Press the ENTER key to begin!\n");
	std::cout<<(char)getchar()<<std::endl; //check echo
	
	// Action::GetInstance()->m_Joint.SetEnableBody(true, true);
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	MotionManager::GetInstance()->SetEnable(true);

	int Status = MotionStatus::FALLEN;
	std::cout<<"TRACKER: Status "<<Status<<std::endl;

	sleep(8);

	// std::cout<<"HEAD TOPLIMIT"<<Head::GetInstance()->m_TopLimit<<std::endl;
	return 0;
}

void AutoLineTracker::Track()
{
	Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
	Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	MotionManager::GetInstance()->SetEnable(true);
	Point2D pos;
	Point2D pos2;
	LinuxCamera::GetInstance()->CaptureFrame();
	Image* temp_rgb_line = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);
	unsigned int* integralBuffer = new unsigned int[rgb_line->m_ImageSize];
	
	memcpy(rgb_line->m_ImageData, LinuxCamera::GetInstance()->fbuffer->m_RGBFrame->m_ImageData, LinuxCamera::GetInstance()->fbuffer->m_RGBFrame->m_ImageSize);

	
	// if(runOnce == false){
		// m
		// std::cout<<(char)getchar()<<std::endl; //check echo	

	// }
	
	// if(enterPressed == true){


	pos = line_finder->GetPosition(LinuxCamera::GetInstance()->fbuffer->m_HSVFrame);
	pos2 = marker_finder->GetPosition(LinuxCamera::GetInstance()->fbuffer->m_HSVFrame);

	// std::cout<<"TRACKER: Image Size of integralBuffer"<<rgb_line->m_ImageSize<< "temp_rgb_line size "<<(Camera::WIDTH*Camera::HEIGHT*3)<<std::endl;
	cout<<"TRACKER: Line Position X: "<<pos.X <<" Y: "<<pos.Y <<endl;
	cout<<"TRACKER: Marker Position X: "<<pos2.X <<" Y: "<<pos2.Y <<endl;
    tracker.Process(pos);
	
	follower.Process(tracker.ball_position);

	
	cout << "\nTRACKER: Line Exitsts = " << follower.LineExists() << endl;
	//if red line not exits and not walking

	if(follower.LineExists()){
		runOnce = true;
	}
	if(runOnce && !follower.LineExists() && !Walking::GetInstance()->IsRunning()){
			unsigned int marker=ImgProcess::IdentifyMarker(rgb_line,marker_finder, line_finder,pos2, pos);
			if(marker ==1 ){
				Voice::Voice::Speak("Forward Marker Detected");
				cout <<"TRACKER: Moving forward !"<<endl;

				Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
				Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;

				Walking::GetInstance()->X_MOVE_AMPLITUDE = 15;
				Walking::GetInstance()->Start();
				sleep(4);
		

				runOnce = false;
			}
			//left
			if(marker ==2 ){
				Voice::Voice::Speak("Left Marker Detected");
				// LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
				cout <<"TRACKER: Rotate left !"<<endl;
				Walking::GetInstance()->A_MOVE_AMPLITUDE = 15;
				Walking::GetInstance()->Start();
				sleep(4);

				cout <<"TRACKER: Moving left !"<<endl;

				Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
				Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;

				Walking::GetInstance()->X_MOVE_AMPLITUDE = 15;
				Walking::GetInstance()->Start();
				sleep(2);
		

				runOnce = false;
			}
			//right
			if(marker ==3 ){
				Voice::Voice::Speak("Right Marker Detected");
				cout <<"TRACKER: Rotate Right !"<<endl;
				Walking::GetInstance()->A_MOVE_AMPLITUDE = -15;
				Walking::GetInstance()->Start();
				sleep(4);



				cout <<"TRACKER: Moving right !"<<endl;

				Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
				Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
				Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;

				Walking::GetInstance()->X_MOVE_AMPLITUDE = 15;
				Walking::GetInstance()->Start();
				sleep(2);
		

				runOnce = false;
			}
		
	}
	// unsigned int marker=ImgProcess::IdentifyMarker(rgb_line,marker_finder, line_finder,pos2, pos);




	// ImgProcess::AvgIntensity(rgb_line,temp_rgb_line);
	
	// ImgProcess::IntegralImage(temp_rgb_line,integralBuffer);

	//to check if integralImage correct
	// ImgProcess::ScaleIntegralImage(integralBuffer, temp_rgb_line, temp_rgb_line->m_WidthStep, temp_rgb_line->m_Width, temp_rgb_line->m_Height);
	

	// rgb_line->m_ImageData[(unsigned int)(pos.Y*320*3+pos.X + 0)] = 255;
	// rgb_line->m_ImageData[(unsigned int)(pos.Y*320*3+pos.X + 1)] = 0;
	// rgb_line->m_ImageData[(unsigned int)(pos.Y*320*3+pos.X + 2)] = 0;
	// rgb_line->m_ImageData[(unsigned int)((pos.Y-1)*320*3+pos.X + 0)] = 255;
	// rgb_line->m_ImageData[(unsigned int)((pos.Y-1)*320*3+pos.X + 1)] = 0;
	// rgb_line->m_ImageData[(unsigned int)((pos.Y-1)*320*3+pos.X + 2)] = 0;
	



	//show cailibrated line finder
	for(int i = 0; i < rgb_line->m_NumberOfPixels; i++)
	{
		if(line_finder->m_result->m_ImageData[i] == 1)
		{
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 0] = 255;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 1] = 0;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 2] = 0;
		}
		
		if(marker_finder->m_result->m_ImageData[i] == 1)
		{
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 0] = 0;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 1] = 255;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 2] = 0;
		}
	}




	//show position detected 
	// std::cout << "\n//////////////////////////////////////////////////////" << std::endl;
	// std::cout << "Drawing position point " << std::endl;
	for( unsigned int y=0; y<(rgb_line->m_Height-1); y++ )
	{
		int d_range = 4;
		if( y > pos.Y-d_range && y < pos.Y+d_range )
		{
			// std::cout << "[ ";
			for(int x=0; x<(rgb_line->m_WidthStep-1); x = x + 3 )
			{
				if( x > pos.X-d_range && x< pos.X+d_range  ){
										rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +0] = 200;
										rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +1] = 0;
										rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +2] = 255;

				}
					// rgb_line->m_ImageData[y*rgb_line->m_widthStep + 3*x +0] = 0;
			}
			// std::cout << "]" << std::endl;
		}
	}
	// std::cout << "//////////////////////////////////////////////////////" << std::endl;


	//marker range filter on the path
	// int m_range = 50;
	// for( unsigned int y=0; y<(rgb_line->m_Height-1); y++ )
	// {
	// 	// int d_range = 4;
	// 	if( y> m_range+20 && y < pos.Y  )
	// 	{
	// 		// std::cout << "[ ";
	// 		for(int x=0; x<(rgb_line->m_WidthStep-1); x = x + 1 )
	// 		{
	// 			if( x > pos.X-m_range*1.5 && x< pos.X+m_range*1.5  ){
	// 									rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +0] = 0;
	// 									rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +1] = 0;
	// 									rgb_line->m_ImageData[y*rgb_line->m_WidthStep + 3*x +2] = 255;

	// 			}
	// 				// rgb_line->m_ImageData[y*rgb_line->m_widthStep + 3*x +0] = 0;
	// 		}
	// 		// std::cout << "]" << std::endl;
	// 	}
	// }
	// std::cout << "//////////////////////////////////////////////////////" << std::endl;




	
	// }

	//if line not exists then keep going after identification
	//do not run marker commend
	
	// streamer->send_image(rgb_line);
	streamer->send_image(rgb_line);
	// Standup();
	delete(temp_rgb_line);
	delete(integralBuffer);
}


int AutoLineTracker::Initialize_from_videostream()
{
	printf( "\nTRACKER: ===== Auto Line Tracker for DARwIn OP2 =====\n\n");

    change_current_dir_autotrack();

    rgb_line = new Image(Camera::WIDTH, Camera::HEIGHT, Image::RGB_PIXEL_SIZE);

    ini = new minIni(INI_FILE_PATH);

    // streamer = new mjpg_streamer(Camera::WIDTH, Camera::HEIGHT);

    line_finder = new ColorFinder();
    line_finder->LoadINISettings(ini);
    // httpd::ball_finder = line_finder;

    follower = LineFollower();
	follower.DEBUG_PRINT = true;

	//////////////////// Framework Initialize ////////////////////////////
	if(MotionManager::GetInstance()->Initialize(&cm730_autotrack) == false)
	{
		printf("Fail to initialize Motion Manager!\n");
			return 0;
	}
    MotionManager::GetInstance()->LoadINISettings(ini);
    Walking::GetInstance()->LoadINISettings(ini);

	MotionManager::GetInstance()->AddModule((MotionModule*)Head::GetInstance());
	MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
    motion_timer->Start();
	/////////////////////////////////////////////////////////////////////

	int n = 0;
	int param[JointData::NUMBER_OF_JOINTS * 5];
	int wGoalPosition, wStartPosition, wDistance;

	for(int id=JointData::ID_R_SHOULDER_PITCH; id<JointData::NUMBER_OF_JOINTS; id++)
	{
		wStartPosition = MotionStatus::m_CurrentJoints.GetValue(id);
		wGoalPosition = Walking::GetInstance()->m_Joint.GetValue(id);
		if( wStartPosition > wGoalPosition )
			wDistance = wStartPosition - wGoalPosition;
		else
			wDistance = wGoalPosition - wStartPosition;

		wDistance >>= 2;
		if( wDistance < 8 )
			wDistance = 8;

		param[n++] = id;
		param[n++] = CM730::GetLowByte(wGoalPosition);
		param[n++] = CM730::GetHighByte(wGoalPosition);
		param[n++] = CM730::GetLowByte(wDistance);
		param[n++] = CM730::GetHighByte(wDistance);
	}
	cm730_autotrack.SyncWrite(MX28::P_GOAL_POSITION_L, 5, JointData::NUMBER_OF_JOINTS - 1, param);	

	printf("TRACKER: Ready to Start\n");
	// std::cout<<(char)getchar()<<std::endl; //check echo
	sleep(2);

    Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	MotionManager::GetInstance()->SetEnable(true);
	
	return 0;
}

void AutoLineTracker::Track_from_videostream(FrameBuffer* frameBuffer_videostream, Image* rgb_line_videostream)
{
	Point2D pos;
	memcpy(frameBuffer->m_RGBFrame->m_ImageData, frameBuffer_videostream->m_RGBFrame->m_ImageData, frameBuffer->m_RGBFrame->m_ImageSize);
	
	Robot::ImgProcess::RGBtoHSV(frameBuffer);


	//copy video stream value to ball local
	memcpy(rgb_line->m_ImageData, rgb_line_videostream->m_ImageData, frameBuffer->m_RGBFrame->m_ImageSize);


	memcpy(rgb_line->m_ImageData, frameBuffer->m_RGBFrame->m_ImageData, frameBuffer->m_RGBFrame->m_ImageSize);

	pos = line_finder->GetPosition(frameBuffer->m_HSVFrame);
	cout<<"TRACKER: Line Position X: "<<pos.X <<" Y: "<<pos.Y <<endl;
    tracker.Process(pos);

	follower.Process(tracker.ball_position);

	for(int i = 0; i < rgb_line->m_NumberOfPixels; i++)
	{
		if(line_finder->m_result->m_ImageData[i] == 1)
		{
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 0] = 255;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 1] = 0;
			rgb_line->m_ImageData[i*rgb_line->m_PixelSize + 2] = 0;
		}
	}

	memcpy(rgb_line_videostream->m_ImageData, rgb_line->m_ImageData, frameBuffer->m_RGBFrame->m_ImageSize);

}

void AutoLineTracker::Standup(){
	int Status = MotionStatus::FALLEN;
	if(Status==1){
		Action::GetInstance()->m_Joint.SetEnableBody(true, true);
		MotionManager::GetInstance()->SetEnable(true);
		std::cout << "TRACKER: Standing up...from the front" << std::endl;
		Action::GetInstance()->Start(18);

		while(Action::GetInstance()->IsRunning())
		{
			usleep(8*1000);
		}

		LinuxActionScript::PlayMP3("../../../Data/mp3/Wow.mp3");
	}else if(Status==-1){
		Action::GetInstance()->m_Joint.SetEnableBody(true, true);
		MotionManager::GetInstance()->SetEnable(true);
		std::cout << "TRACKER: Standing up...from the back" << std::endl;
		Action::GetInstance()->Start(11);

		while(Action::GetInstance()->IsRunning())
		{
			usleep(8*1000);
		}
		LinuxActionScript::PlayMP3("../../../Data/mp3/Wow.mp3");

	}
	else{
		Head::GetInstance()->m_Joint.SetEnableHeadOnly(true, true);
		Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
	}
	std::cout<<"TRACKER: FALLEN:"<< Status<<std::endl;
}

// void AutoLineTracker::WalkInitialize()
// {
// 	Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true, true);
// 	Walking::GetInstance()->X_MOVE_AMPLITUDE = 0;
// 	Walking::GetInstance()->Y_MOVE_AMPLITUDE = 0;
// 	Walking::GetInstance()->A_MOVE_AMPLITUDE = 0;
// }

// void AutoLineTracker::WalkForward(const long duration, const long step_size)
// {
// 	Walking::GetInstance()->X_MOVE_AMPLITUDE = step_size;
// 	Walking::GetInstance()->Start();
// 	sleep(duration);
// 	StopWalking();
// }
// void AutoLineTracker::RotateLeft(const long duration, const long step_size)
// {
// 	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
// 	Walking::GetInstance()->A_MOVE_AMPLITUDE = step_size;
// 	Walking::GetInstance()->Start();
// 	sleep(duration);
// 	StopWalking();
// }

// void AutoLineTracker::RotateRight(const long duration, const long step_size)
// {
// 	LinuxActionScript::PlayMP3("../../../Data/mp3/I_am_not_fast.mp3");
// 	Walking::GetInstance()->A_MOVE_AMPLITUDE = -step_size;
// 	Walking::GetInstance()->Start();
// 	sleep(duration);
// 	StopWalking();
// }
// void TeleOperation::StopWalking()
// {
// 	if(Walking::GetInstance()->IsRunning() ==1 )
// 	{
// 		// cout<<"STOP called"<<endl;
// 		Walking::GetInstance()->Stop();   
// 		sleep(1);
//       }
// }

