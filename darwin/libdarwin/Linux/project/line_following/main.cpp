/* line_following
 * main.cpp
 *
 *  
 *      Author: Kiral Poon and Paul Crevier
 *      Version: 0.1
 */

 #include "AutoLineTracker.h"
 
int main(void)
{
    AutoLineTracker::GetInstance()->Initialize();

    while(1)
    {
		AutoLineTracker::GetInstance()->Track();
    }

    return 0;
}
