/*
 *   LineTracker.cpp
 *
 *   Author: Tik Wai Poon a.k.a Kiral
 *
 */

#include <math.h>
#include "Head.h"
#include "Camera.h"
#include "ImgProcess.h"
#include "LineTracker.h"

using namespace Robot;


LineTracker::LineTracker() :
        ball_position(Point2D(-1.0, -1.0))
{
	NoBallCount = 0;
}

LineTracker::~LineTracker()
{
}

void LineTracker::Process(Point2D pos)
{
	if(pos.X < 0 || pos.Y < 0)
	{
		ball_position.X = -1;
		ball_position.Y = -1;
		if(NoBallCount < NoBallMaxCount)
		{
			Head::GetInstance()->MoveTracking();
			NoBallCount++;
		}
		else
			Head::GetInstance()->InitTracking();
	}
	else
	{
		NoBallCount = 0;
		Point2D center = Point2D(Camera::WIDTH/2, Camera::HEIGHT/2);
		Point2D offset = pos - center;
		offset *= -1; // Inverse X-axis, Y-axis
		offset.X *= (Camera::VIEW_H_ANGLE / (double)Camera::WIDTH); // pixel per angle
		offset.Y *= (Camera::VIEW_V_ANGLE / (double)Camera::HEIGHT); // pixel per angle
		ball_position = offset;
		// fprintf(stderr, " TRACKING HEAD offset X: %f, Y: %f \r", ball_position.X, ball_position.Y);
		
		Head::GetInstance()->MoveTracking(ball_position);
	}
}
