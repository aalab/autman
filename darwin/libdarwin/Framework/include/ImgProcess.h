/*
 *   ImgProcess.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _IMAGE_PROCESS_H_
#define _IMAGE_PROCESS_H_

#include "Image.h"
#include "Point.h"
#include "MarkerFinder.h"
#include "Camera.h"

namespace Robot
{
	class ImgProcess
	{
	public:
		static void YUVtoRGB(FrameBuffer *buf);
		static void RGBtoHSV(FrameBuffer *buf);

		static void Erosion(Image* img);
        static void Erosion(Image* src, Image* dest);
		static void Dilation(Image* img);
        static void Dilation(Image* src, Image* dest);
		static void AvgIntensity(Image* src, Image* dest);
		static void IntegralImage(Image* src, unsigned int* dest);
		static void ScaleIntegralImage(unsigned int* buffer, Image* dest, int widthStep, int width, int height);
		static unsigned int IntegralImageArea(unsigned int* integralBuffer,Image* src,Point2D tl, Point2D tr, Point2D ll, Point2D lr );
		static unsigned int IdentifyMarker(Image* src,MarkerFinder* marker_finder, ColorFinder*  line_finder,Point2D marker_pos, Point2D  line_pos);


        static void HFlipYUV(Image* img);
        static void VFlipYUV(Image* img);

// ***   WEBOTS PART  *** //

		static void BGRAtoHSV(FrameBuffer *buf);
	};
}

#endif
