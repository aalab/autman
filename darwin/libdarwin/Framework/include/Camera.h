/*
 *   Camera.h
 *
 *   Author: ROBOTIS
 *
 */

#ifndef _CAMERA_H_
#define _CAMERA_H_


namespace Robot
{
	class Camera
	{
	
	public:
		// static const double VIEW_V_ANGLE = 46.0; //degree
		// static const double VIEW_H_ANGLE = 58.0; //degree
	#if __cplusplus <= 199711L
		static const double VIEW_V_ANGLE = 46.0; // changed for new compilar
		static const double VIEW_H_ANGLE = 58.0; // changed for new compilar
	 
	#endif	

	#if __cplusplus > 199711L
		static constexpr double VIEW_V_ANGLE = 46.0; // changed for new compilar
		static constexpr double VIEW_H_ANGLE = 58.0; // changed for new compilar
	 
	#endif	
		
		static int WIDTH;
		static int HEIGHT;
	};

}

#endif
