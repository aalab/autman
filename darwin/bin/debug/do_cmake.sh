#!/bin/bash

BUILD_VER=Debug
CMAKE_DIR=../..

cmake -DCMAKE_BUILD_TYPE=${BUILD_VER} ${CMAKE_DIR}
exit
