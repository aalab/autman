#Param $1 = account name of machine to copy from
#Param $2 = ip address of machine to copy from

#KEEP THESE CONSTANT
srcMainLaptop="/home/votick/workspace/aalab/KyleSpace/" #directory on my laptop containing stuff
srcMainVM="/home/kyle/aalab/KyleSpace/" #directory on virtual machine containing stuff
srcDarwinVM="/home/darwin/KyleSpace"
srcClara="/home/robotisop2/"
srcJennifer="/home/darwin/"

#CONFIGURE THIS using variables created above
srcDir=$srcJennifer #change this to adjust where to copy from
targetDir="KyleSpace" #directory to copy everything to

echo "Attempting to copy from $srcDir from machine $1 to $targetDir"
#rsync -aAxv $1@$2:$srcDir $targetDir
rsync -aAxv $targetDir $1@$2:$srcDir
