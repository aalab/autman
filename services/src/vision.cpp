/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/base/json.h>
#include <services/vision.h>
#include <services/data/vision/fetch.h>
#include "compilerdefinitions.h"

#include <iostream>

services::Vision::Vision(std::string address, unsigned int port) : UDPClient(address, port)
{ }

bool services::Vision::fetch(services::data::vision::FetchResponse &response)
{
	Json::Value fetchJson;
	services::data::vision::Fetch fetch;
	json::factory(fetch, fetchJson);
	if (this->send(fetchJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			try
			{
				return json::factory(responseJson, response);
			}
			catch (services::exceptions::InvalidMemberName &e)
			{
				ERROR("Exception: " << e.msg());
			}
			catch (std::exception &e)
			{
				ERROR("Exception: " << e.what());
			}
		}
	}
	return false;
}

bool services::Vision::mode(services::data::vision::Mode &mode)
{
	Json::Value modeJson;
	json::factory(mode, modeJson);
	if (this->send(modeJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			return true;
		}
	}
	return false;
}

bool services::Vision::updateCameraCalibrations()
{
	Json::Value cameraCalibrationsJson;
	cameraCalibrationsJson["command"] = "cameracalibrations";
	if (this->send(cameraCalibrationsJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			json::factory(responseJson, this->cameraCalibrations);
			return true;
		}
	}
	return false;
}
