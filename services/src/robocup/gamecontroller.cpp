/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <services/robocup/gamecontroller.h>

#include "../compilerdefinitions.h"

::services::robocup::GameController::GameController()
	: m_teamNumber(30), m_robotNumber(4)
{
	this->initResponse();
}


services::robocup::GameController::~GameController()
{ }

bool ::services::robocup::GameController::initNetwork()
{
	memset(&this->m_addr, 0, sizeof(this->m_addr));
	this->m_addr.sin6_family = AF_INET;
	this->m_addr.sin6_port = htons(3838);
	this->m_addr.sin6_addr = in6addr_any;

	if ((this->m_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		return false;
	}
	int bind_err;

	if ((bind_err = bind(this->m_fd, (sockaddr *)&this->m_addr, sizeof(sockaddr_in6))) != 0)
	{
		close(this->m_fd);
		return false;
	}
	return true;
}

bool ::services::robocup::GameController::update()
{
	if (! this->communicate())
	{
		return false;
	}

	if (memcmp(this->m_data.header, GAMECONTROLLER_STRUCT_HEADER, 4 * sizeof(char)) != 0)
	{
		ERROR("GameController: corrupt package");
		return false;
	}
	this->parseData();
	return true;
}

bool ::services::robocup::GameController::communicate()
{
	int ret, req, send_err;

	timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	FD_ZERO(&this->m_rfds);
	FD_SET(this->m_fd, &this->m_rfds);

	req = select(this->m_fd + 1, &this->m_rfds, 0, 0, &timeout);
	if (req < 0)
	{
		ERROR("GameController: Select failed!");
		return false;
	}
	else if (req == 0)
	{
		return false;
	}

	m_ret_addr_len = sizeof(m_ret_addr);
	if ((ret = recvfrom(this->m_fd, &this->m_data, sizeof(this->m_data), 0, (sockaddr *)&m_ret_addr, &m_ret_addr_len) < 0))
	{
		ERROR("GameController: Recieving a packet failed.: " << strerror(errno));
		return false;
	}

	createResponse();

	// Reply to the sending IP, but with the correct port.
	if(m_ret_addr_len == sizeof(sockaddr_in))
	{
		sockaddr_in* in_addr = (sockaddr_in*)&m_ret_addr;
		in_addr->sin_port = htons(GAMECONTROLLER_PORT);
	}
	else if(m_ret_addr_len == sizeof(sockaddr_in6))
	{
		m_ret_addr.sin6_port = htons(GAMECONTROLLER_PORT);
	}

	if ((send_err = sendto(m_fd, &m_response, sizeof(m_response), 0, (sockaddr *)&m_ret_addr, m_ret_addr_len)) < 0)
	{
		ERROR("GameController: Sending a response failed.");
		return false;
	}

	return true;
}

void ::services::robocup::GameController::parseData()
{
	if (m_data.state == STATE_INITIAL)
	{
		VERBOSE("INITIAL!");
	}
	else if (m_data.state == STATE_READY)
	{
		VERBOSE("STATE_READY!");
	}
	else if (m_data.state == STATE_SET)
	{
		VERBOSE("STATE_SET!");
	}
	else if (m_data.state == STATE_READY)
	{
		VERBOSE("STATE_READY!");
	}
	else if (m_data.state == STATE_PLAYING)
	{
		VERBOSE("STATE_PLAYING!");
	}
	else if (m_data.state == STATE_FINISHED)
	{
		VERBOSE("STATE_FINISHED!");
	}
	/*
	const int numberOfTeams = 2;
	m_pubData->playerPerTeam = m_data.playersPerTeam;
	m_pubData->state = m_data.state;
	m_pubData->secondaryState = m_data.secondaryState;
	m_pubData->firstHalf = m_data.firstHalf;
	m_pubData->remainingSeconds = m_data.secondaryState;
	m_pubData->kickOffTeam = m_data.kickOffTeam;
	m_pubData->dropInTeam = m_data.dropInTeam;
	m_pubData->dropInTime = m_data.dropInTime;
	m_pubData->timeStamp = ros::Time::now();
	std::cout << "Parsing Data..." << std::endl;

	m_pubData->teams.resize(numberOfTeams);
	for (int i = 0; i < numberOfTeams; i++)
	{
		GCTeamInfo tInfo;
		tInfo.goalDirection = m_data.teams[i].goalColour;
		tInfo.score = m_data.teams[i].score;
		tInfo.teamColour = m_data.teams[i].teamColour;
		tInfo.teamNumber = m_data.teams[i].teamNumber;
		tInfo.player.resize(MAX_NUM_PLAYERS);
		for (int j = 0; j < MAX_NUM_PLAYERS; j++)
		{
			GCRobotInfo rInfo;
			rInfo.penalty = m_data.teams[i].players[j].penalty;
			rInfo.remainingPenaltySec = m_data.teams[i].players[j].secsTillUnpenalised;
			tInfo.player[j] = rInfo;
		}
		m_pubData->teams[i] = tInfo;
	}
	 */
}

void ::services::robocup::GameController::createResponse(uint32 msg)
{
	m_response.team = (uint16)m_teamNumber;
	m_response.player = (uint16)m_robotNumber;
	m_response.message = msg;
}

void ::services::robocup::GameController::initResponse()
{
	memcpy(m_response.header, GAMECONTROLLER_RETURN_STRUCT_HEADER, 4 * sizeof(char));
	m_response.version = GAMECONTROLLER_RETURN_STRUCT_VERSION;
}

unsigned int ::services::robocup::GameController::getState()
{
	return this->m_data.state;
}
