/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <services/base/json.h>
#include <services/gyro.h>

services::Gyro::Gyro(std::string address, unsigned int port) : UDPClient(address, port)
{ }

bool services::Gyro::calibrate()
{
	Json::Value calibrateJson;
	services::data::gyro::Calibration calibration;
	json::factory(calibration, calibrateJson);
	if (this->send(calibrateJson))
	{
		Json::Value response;
		if (this->read(response))
			return response["success"].asBool();
	}
	return false;
}

bool services::Gyro::data(data::gyro::DataResponse &response)
{
	Json::Value dataJson;
	services::data::gyro::Data data;
	json::factory(data, dataJson);
	if (this->send(dataJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && responseJson["success"].asBool())
		{
			json::factory(responseJson, response);
			return true;
		}
	}
	return false;
}
