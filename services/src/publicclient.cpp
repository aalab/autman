/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 28, 2015
 **/

#include <services/publicclient.h>

services::PublicClient::PublicClient(const std::string &address, unsigned int port)
	: UDPClient(address, port)
{ }

int services::PublicClient::send(std::string const &msg)
{
	return services::base::UDPClient::send(msg);
}

bool services::PublicClient::send(Json::Value &json)
{
	return services::base::UDPClient::send(json);
}

std::size_t services::PublicClient::read(
	char *buffer, unsigned int bufferLength, boost::posix_time::time_duration timeout, boost::system::error_code &ec
)
{
	return services::base::UDPClient::read(buffer, bufferLength, timeout, ec);
}

bool services::PublicClient::read(Json::Value &json, boost::posix_time::time_duration timeout,
								  boost::system::error_code &ec)
{
	return services::base::UDPClient::read(json, timeout, ec);
}

bool services::PublicClient::read(Json::Value &json)
{
	return services::base::UDPClient::read(json);
}
