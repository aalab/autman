/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/point.h>

services::data::Point2D::Point2D()
	: x(0), y(0)
{ }

services::data::Point2D::Point2D(double x, double y)
	: x(x), y(y)
{ }

services::data::Point3D::Point3D()
	: Point2D(), z(0)
{ }

services::data::Point3D::Point3D(double x, double y, double z)
	: Point2D(x, y), z(z)
{ }

void ::services::json::factory(Json::Value &source, services::data::Point2D &dest)
{
	std::string name;
	for (Json::ValueIterator member = source.begin(); member != source.end(); member++)
	{
		name = member.name();
		if (name == "x")
			dest.x = member->asDouble();
		else if (name == "y")
			dest.y = member->asDouble();
	}
}

void ::services::json::factory(Json::Value &source, services::data::Point3D &dest)
{
	std::string name;
	for (Json::ValueIterator member = source.begin(); member != source.end(); member++)
	{
		name = member.name();
		if (name == "x")
			dest.x = member->asDouble();
		else if (name == "y")
			dest.y = member->asDouble();
		else if (name == "z")
			dest.z = member->asDouble();
	}
}

services::data::Point3D &operator<<(services::data::Point3D &result, Json::Value &json)
{
	result.x = json["x"].asDouble();
	result.y = json["y"].asDouble();
	result.z = json["z"].asDouble();
	return result;
}

void ::services::json::factory(services::data::Point3D &source, Json::Value &dest)
{
	dest["x"] = source.x;
	dest["y"] = source.y;
	dest["z"] = source.z;
}
