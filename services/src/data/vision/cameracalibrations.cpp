/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 15, 2015
 */

#include <services/data/vision/cameracalibrations.h>

boost::optional<services::data::vision::TsaiWrapper&>
services::data::vision::CameraCalibrations::calibrationByName(std::string name)
{
	for (TsaiWrapper& cc : this->calibrations)
	{
		if (cc.name == name)
			return cc;
	}
	return boost::optional<services::data::vision::TsaiWrapper&>();
}