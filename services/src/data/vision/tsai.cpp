/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <services/data/vision/tsai.h>

services::data::vision::TsaiWrapper::TsaiWrapper(struct tsai_camera_parameters cameraParameters)
	: cameraParameters(cameraParameters)
{ }

void
services::data::vision::TsaiWrapper::i2w(unsigned int x, unsigned y, double *iX, double *iY)
{
	image_coord_to_world_coord(x, y, 0, iX, iY, &this->cameraParameters, &this->data);
}

void services::data::vision::TsaiWrapper::i2w(services::data::Line2D &source, services::data::Line2D &dest)
{
	this->i2w(source.a.x,source.a.y, &dest.a.x, &source.a.y);
	this->i2w(source.b.x,source.b.y, &dest.b.x, &source.b.y);
}
