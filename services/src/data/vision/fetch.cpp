/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <services/data/vision/fetch.h>

void services::data::vision::FetchResponse::classify(services::data::vision::VisionObject const &object)
{ }

bool services::data::vision::FetchResponse::classifyJson(Json::Value &json)
{
	return false;
}
