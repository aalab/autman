/**
 * @author J. Santos <jamillo@gmail.com>
 * @created September 14, 2015
 */

#include <services/data/teleop/gamepad.h>

services::data::teleop::GamepadData::~GamepadData()
{ }

services::data::teleop::Axis::Axis() : Point2D()
{ }

services::data::teleop::Axis::Axis(double x, double y) : Point2D(x, y)
{ }

void ::services::json::factory(services::data::teleop::Axis &source, Json::Value &dest)
{
	dest["x"] = source.x;
	dest["y"] = source.y;
}

void ::services::json::factory(Json::Value &source, services::data::teleop::Axis &dest)
{
	std::string name;
	for (Json::ValueIterator member = source.begin(); member != source.end(); member++)
	{
		name = member.name();
		if (name == "x")
			dest.x = member->asDouble();
		else if (name == "y")
			dest.y = member->asDouble();
	}
}
