/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 16, 2015
 **/

#include <services/data/teleop/touch.h>

services::data::teleop::Axis &services::data::teleop::Touch::left()
{
	return this->_left;
}

services::data::teleop::Touch &services::data::teleop::Touch::left(services::data::teleop::Axis &left)
{
	this->_left = left;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::Touch::right()
{
	return this->_right;
}

services::data::teleop::Touch &services::data::teleop::Touch::right(services::data::teleop::Axis &right)
{
	this->_right = right;
	return *this;
}

void ::services::json::factory(Json::Value &source, services::data::teleop::Touch &dest)
{
	factory(dest.left(), source["left"]);
	factory(dest.right(), source["right"]);
}

void ::services::json::factory(services::data::teleop::Touch &source, Json::Value &dest)
{
	factory(source.left(), dest["left"]);
	factory(source.right(), dest["right"]);
}
