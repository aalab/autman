/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 14, 2015
 **/

#include <services/data/teleop/xbox.h>

#include <services/data/teleop/gamepad.h>

const float services::data::teleop::XboxGamepad::a() const
{
	return this->_a;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::a(float a)
{
	this->_a = a;
	return *this;
}

const float services::data::teleop::XboxGamepad::b() const
{
	return this->_b;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::b(float b)
{
	this->_b = b;
	return *this;
}

const float services::data::teleop::XboxGamepad::x() const
{
	return this->_x;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::x(float x)
{
	this->_x = x;
	return *this;
}

const float services::data::teleop::XboxGamepad::y() const
{
	return this->_y;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::y(float y)
{
	this->_y = y;
	return *this;
}

const float services::data::teleop::XboxGamepad::back() const
{
	return this->_back;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::back(float back)
{
	this->_back = back;
	return *this;
}

const float services::data::teleop::XboxGamepad::start() const
{
	return this->_start;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::start(float start)
{
	this->_start = start;
	return *this;
}

const float services::data::teleop::XboxGamepad::xbox() const
{
	return this->_xbox;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::xbox(float xbox)
{
	this->_xbox = xbox;
	return *this;
}

const float services::data::teleop::XboxGamepad::ljoystickButton() const
{
	return this->_ljoystickButton;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::ljoystickButton(float ljoystickButton)
{
	this->_ljoystickButton = ljoystickButton;
	return *this;
}

const float services::data::teleop::XboxGamepad::rjoystickButton() const
{
	return this->_rjoystickButton;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::rjoystickButton(float rjoystickButton)
{
	this->_rjoystickButton = rjoystickButton;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::XboxGamepad::ljoystick()
{
	return this->_ljoystick;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::ljoystick(
	services::data::teleop::Axis &ljoystick
)
{
	this->_ljoystick = ljoystick;
	return *this;
}

const float services::data::teleop::XboxGamepad::ltrigger() const
{
	return this->_ltrigger;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::ltrigger(float ltrigger)
{
	this->_ltrigger = ltrigger;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::XboxGamepad::rjoystick()
{
	return this->_rjoystick;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::rjoystick(
	services::data::teleop::Axis &rjoystick
)
{
	this->_rjoystick = rjoystick;
	return *this;
}

const float services::data::teleop::XboxGamepad::rtrigger() const
{
	return this->_rtrigger;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::rtrigger(float rtrigger)
{
	this->_rtrigger = rtrigger;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::XboxGamepad::cross()
{
	return this->_cross;
}

services::data::teleop::XboxGamepad &services::data::teleop::XboxGamepad::cross(services::data::teleop::Axis &cross)
{
	this->_cross = cross;
	return *this;
}

void ::services::json::factory(Json::Value &source, services::data::teleop::XboxGamepad &dest)
{
	std::string memberName;
	for (Json::ValueIterator itr = source.begin(); itr != source.end(); ++itr)
	{
		memberName = itr.name();

		if (memberName == "a")
			dest.a(itr->asFloat());
		else if (memberName == "b")
			dest.b(itr->asFloat());
		else if (memberName == "x")
			dest.x(itr->asFloat());
		else if (memberName == "y")
			dest.y(itr->asFloat());
		else if (memberName == "back")
			dest.back(itr->asFloat());
		else if (memberName == "start")
			dest.start(itr->asFloat());
		else if (memberName == "xbox")
			dest.xbox(itr->asFloat());
		else if (memberName == "ljoystickButton")
			dest.ljoystickButton(itr->asFloat());
		else if (memberName == "rjoystickButton")
			dest.rjoystickButton(itr->asFloat());
		else if (memberName == "ljoystick")
			factory(*itr, dest.ljoystick());
		else if (memberName == "ltrigger")
			dest.ltrigger(itr->asFloat());
		else if (memberName == "rjoystick")
			factory(*itr, dest.rjoystick());
		else if (memberName == "rtrigger")
			dest.rtrigger(itr->asFloat());
		else if (memberName == "cross")
			factory(*itr, dest.cross());
	}
}

void ::services::json::factory(services::data::teleop::XboxGamepad &source, Json::Value &dest)
{
	dest["a"] = source.a();
	dest["b"] = source.b();
	dest["x"] = source.x();
	dest["y"] = source.y();
	dest["back"] = source.back();
	dest["start"] = source.start();
	dest["xbox"] = source.xbox();
	dest["ljoystickButton"] = source.ljoystickButton();
	dest["rjoystickButton"] = source.rjoystickButton();
	factory(source.ljoystick(), dest["ljoystick"]);
	dest["ltrigger"] = source.ltrigger();
	factory(source.rjoystick(), dest["rjoystick"]);
	dest["rtrigger"] = source.rtrigger();
	factory(source.cross(), dest["cross"]);
}
