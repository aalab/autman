/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include <services/data/teleop/keyboard.h>

const float services::data::teleop::Keyboard::control() const
{
	return this->_control;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::control(float control)
{
	this->_control = control;
	return *this;
}

const float services::data::teleop::Keyboard::shift() const
{
	return this->_shift;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::shift(float shift)
{
	this->_shift = shift;
	return *this;
}

const float services::data::teleop::Keyboard::alt() const
{
	return this->_alt;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::alt(float alt)
{
	this->_alt = alt;
	return *this;
}

const float services::data::teleop::Keyboard::space() const
{
	return this->_space;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::space(float space)
{
	this->_space = space;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::Keyboard::arrowLetters()
{
	return this->_arrowLetters;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::arrowLetters(services::data::teleop::Axis &arrowLetters)
{
	this->_arrowLetters = arrowLetters;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::Keyboard::arrows()
{
	return this->_arrows;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::arrows(services::data::teleop::Axis &arrows)
{
	this->_arrows = arrows;
	return *this;
}

void ::services::json::factory(Json::Value &source, services::data::teleop::Keyboard &dest)
{
	std::string memberName;
	for (Json::ValueIterator itr = source.begin(); itr != source.end(); ++itr)
	{
		memberName = itr.name();

		if (memberName == "control")
			dest.control(itr->asFloat());
		else if (memberName == "shift")
			dest.shift(itr->asFloat());
		else if (memberName == "alt")
			dest.alt(itr->asFloat());
		else if (memberName == "space")
			dest.space(itr->asFloat());
		else if (memberName == "arrowLetters")
			factory(*itr, dest.arrowLetters());
		else if (memberName == "arrows")
			factory(*itr, dest.arrows());
	}
}

void ::services::json::factory(services::data::teleop::Keyboard &source, Json::Value &dest)
{
	dest["control"] = source.control();
	dest["shift"] = source.shift();
	dest["alt"] = source.alt();
	dest["space"] = source.space();

	factory(source.arrowLetters(), dest["arrowLetters"]);
	factory(source.arrows(), dest["arrows"]);

	if (source.p() > 0)
		dest["p"] = source.p();
	if (source.a() > 0)
		dest["a"] = source.a();
	if (source.z() > 0)
		dest["z"] = source.z();
	if (source.u() > 0)
		dest["u"] = source.u();
	if (source.j() > 0)
		dest["j"] = source.j();
	if (source.m() > 0)
		dest["m"] = source.m();
}

const float ::services::data::teleop::Keyboard::p() const
{
	return this->_p;
}

services::data::teleop::Keyboard &services::data::teleop::Keyboard::p(float p)
{
	this->_p = p;
	return *this;
};

const float ::services::data::teleop::Keyboard::a() const
{
	return this->_a;
}

services::data::teleop::Keyboard &::services::data::teleop::Keyboard::a(float a)
{
	this->_a = a;
	return *this;
}

const float ::services::data::teleop::Keyboard::z() const
{
	return this->_z;
}

services::data::teleop::Keyboard &::services::data::teleop::Keyboard::z(float z)
{
	this->_z = z;
	return *this;
}

const float ::services::data::teleop::Keyboard::u() const
{
	return this->_u;
}

services::data::teleop::Keyboard &::services::data::teleop::Keyboard::u(float u)
{
	this->_u = u;
	return *this;
}

const float ::services::data::teleop::Keyboard::j() const
{
	return this->_j;
}

services::data::teleop::Keyboard &::services::data::teleop::Keyboard::j(float j)
{
	this->_j = j;
	return *this;
}

const float ::services::data::teleop::Keyboard::m() const
{
	return this->_m;
}

services::data::teleop::Keyboard &::services::data::teleop::Keyboard::m(float m)
{
	this->_m = m;
	return *this;
}

