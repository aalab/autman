/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include <services/data/teleop/playstation.h>

const float services::data::teleop::PSGamepad::square() const
{
	return this->_square;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::square(float square)
{
	this->_square = square;
	return *this;
}

const float services::data::teleop::PSGamepad::triangle() const
{
	return this->_triangle;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::triangle(float triangle)
{
	this->_triangle = triangle;
	return *this;
}

const float services::data::teleop::PSGamepad::circle() const
{
	return this->_circle;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::circle(float circle)
{
	this->_circle = circle;
	return *this;
}

const float services::data::teleop::PSGamepad::x() const
{
	return this->_x;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::x(float x)
{
	this->_x = x;
	return *this;
}

const float services::data::teleop::PSGamepad::select() const
{
	return this->_select;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::select(float select)
{
	this->_select = select;
	return *this;
}

const float services::data::teleop::PSGamepad::start() const
{
	return this->_start;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::start(float start)
{
	this->_start = start;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::PSGamepad::cross()
{
	return this->_cross;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::cross(services::data::teleop::Axis &cross)
{
	this->_cross = cross;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::PSGamepad::rjoystick()
{
	return this->_rjoystick;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::rjoystick(services::data::teleop::Axis &rjoystick)
{
	this->_rjoystick = rjoystick;
	return *this;
}

const float services::data::teleop::PSGamepad::left1() const
{
	return this->_left1;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::left1(float left1)
{
	this->_left1 = left1;
	return *this;
}

const float services::data::teleop::PSGamepad::left2() const
{
	return this->_left2;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::left2(float left2)
{
	this->_left2 = left2;
	return *this;
}

services::data::teleop::Axis &services::data::teleop::PSGamepad::ljoystick()
{
	return this->_ljoystick;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::ljoystick(services::data::teleop::Axis &ljoystick)
{
	this->_ljoystick = ljoystick;
	return *this;
}

const float services::data::teleop::PSGamepad::right1() const
{
	return this->_right1;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::right1(float right1)
{
	this->_right1 = right1;
	return *this;
}

const float services::data::teleop::PSGamepad::right2() const
{
	return this->_right2;
}

services::data::teleop::PSGamepad &services::data::teleop::PSGamepad::right2(float right2)
{
	this->_right2 = right2;
	return *this;
}

void ::services::json::factory(Json::Value &source, services::data::teleop::PSGamepad &dest)
{
	std::string memberName;
	for (Json::ValueIterator itr = source.begin(); itr != source.end(); ++itr)
	{
		memberName = itr.name();

		if (memberName == "square")
			dest.square(itr->asFloat());
		else if (memberName == "triangle")
			dest.triangle(itr->asFloat());
		else if (memberName == "circle")
			dest.circle(itr->asFloat());
		else if (memberName == "x")
			dest.x(itr->asFloat());
		else if (memberName == "select")
			dest.select(itr->asFloat());
		else if (memberName == "start")
			dest.start(itr->asFloat());
		else if (memberName == "cross")
			factory(*itr, dest.cross());
		else if (memberName == "rjoystick")
			factory(*itr, dest.rjoystick());
		else if (memberName == "left1")
			dest.left1(itr->asFloat());
		else if (memberName == "left2")
			dest.left2(itr->asFloat());
		else if (memberName == "ljoystick")
			factory(*itr, dest.ljoystick());
		else if (memberName == "right1")
			dest.right1(itr->asFloat());
		else if (memberName == "right2")
			dest.right2(itr->asFloat());
	}
}

void ::services::json::factory(services::data::teleop::PSGamepad &source, Json::Value &dest)
{
	dest["square"] = source.square();
	dest["triangle"] = source.triangle();
	dest["circle"] = source.circle();
	dest["x"] = source.x();
	dest["select"] = source.select();
	dest["start"] = source.start();
	factory(source.cross(), dest["cross"]);
	factory(source.rjoystick(), dest["rjoystick"]);
	dest["left1"] = source.left1();
	dest["left2"] = source.left2();
	factory(source.ljoystick(), dest["ljoystick"]);
	dest["right1"] = source.right1();
	dest["right2"] = source.right2();
}
