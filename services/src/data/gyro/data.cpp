/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <services/data/gyro/data.h>

void ::services::json::factory(services::data::gyro::Data &data, Json::Value &dest)
{
	dest["command"] = "data";
}

void ::services::json::factory(Json::Value &source, services::data::gyro::DataResponse &dest)
{
	Json::Value &data = source["data"];
	dest.accelerometer << data["accelerometer"];
	dest.gyro << data["gyro"];
	dest.filtered << data["filtered"];
}