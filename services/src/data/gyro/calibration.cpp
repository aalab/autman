/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <services/data/gyro/calibration.h>


void ::services::json::factory(services::data::gyro::Calibration &calibration, Json::Value &dest)
{
	dest["command"] = "calibrate";
}
