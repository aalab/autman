/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/boundbox.h>

double services::data::BoundBox2D::area()
{
	return this->width() * this->height();
}
