/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/line.h>

#include <cmath>
#include <iostream>

double services::data::Line2D::hypot()
{
	double
		_x = (this->b.x - this->a.x),
		_y = (this->b.y - this->a.y);
	return std::sqrt(_x*_x + _y*_y);
}

double services::data::Line2D::inclination()
{
	return std::atan2((this->b.y - this->a.y), (this->b.x - this->a.x));
}

double services::data::Line2D::width()
{
	return (this->b.x - this->a.x);
}

double services::data::Line2D::height()
{
	return (this->b.y - this->a.y);
}

void services::data::Line2D::centerPoint(services::data::Point2D &point)
{
	point.x = this->a.x + (this->b.x - this->a.x)/2.0;
	point.y = this->a.y + (this->b.y - this->a.y)/2.0;
}

services::data::Point2D services::data::Line2D::centerPoint()
{
	services::data::Point2D result;
	this->centerPoint(result);
	return result;
}

services::data::Line2D::Line2D()
{

}

services::data::Line2D::Line2D(services::data::Point2D &a, services::data::Point2D &b)
	: a(a), b(b)
{

}

services::data::Line2D::Line2D(double aX, double aY, double bX, double bY)
	: a(aX, aY), b(bX, bY)
{

}
