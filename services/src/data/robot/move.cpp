/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/robot/move.h>

#include "../../compilerdefinitions.h"

#include <iostream>

services::data::robot::AutMove::AutMove()
	: vx(0), vy(0), vt(0)
{ }

services::data::robot::AutMove::AutMove(float vx, float vy, float vt)
	: vx(vx), vy(vy), vt(vt)
{ }

services::data::robot::Head::Head()
	: pan(0), tilt(0)
{ }

services::data::robot::Head::Head(float pan, float tilt)
	: pan(pan), tilt(tilt)
{ }

services::data::robot::AutFullMove::AutFullMove()
{ }

services::data::robot::AutFullMove::AutFullMove(float vx, float vy, float vt, unsigned int motion, float pan, float tilt)
	: move(vx, vy, vt), motion(motion), head(pan, tilt)
{ }

void ::services::json::factory(services::data::robot::Move &move, Json::Value &dest)
{
	dest["command"] = "Move";
	Json::Value &params = dest["params"];
	if (move.stop)
		params["stop"] = *move.stop;
	if (move.entryTimeUSec)
		params["entryTimeUSec"] = *move.entryTimeUSec;
	if (move.motionSpeed)
		params["motionSpeed"] = *move.motionSpeed;
	if (move.motionEntrySpeed)
		params["motionEntrySpeed"] = *move.motionEntrySpeed;

	if (!move.frames.empty())
	{
		params["dx"] = Json::arrayValue;
		params["dy"] = Json::arrayValue;
		params["dtheta"] = Json::arrayValue;
		params["cycles"] = Json::arrayValue;
		params["cycleTimeUSec"] = Json::arrayValue;
		for (data::robot::Frame &frame : move.frames)
			factory(frame, params);
	}
}

void ::services::json::factory(Json::Value &source, services::data::robot::Move &dest)
{
	std::string name;
	Json::Value &params = source["params"];
	for (Json::Value::iterator member = params.begin(); member != params.end(); ++member)
	{
		name = member.name();

		VERBOSE("Member name = " << name);

		if (name == "entryTimeUSec")
			dest.entryTimeUSec = (*member).asLargestUInt();
		else if (name == "motionEntrySpeed")
			dest.motionEntrySpeed = (*member).asLargestUInt();
		else if (name == "motionSpeed")
			dest.motionSpeed = (*member).asFloat();
		else if (name == "stop")
			dest.stop = (*member).asBool();
		else if (name == "dx")
		{
			if ((*member).isArray())
			{
				unsigned int i = 0;
				for (Json::Value &dx : (*member))
				{
					if (i <= dest.frames.size())
						dest.frames.emplace_back();
					dest.frames[i].x = dx.asDouble();
					i++;
				}
			}
			else
			{
				dest.frames.emplace_back();
				dest.frames[0].x = (*member).asDouble();
			}
		}
		else if (name == "dy")
		{
			if ((*member).isArray())
			{
				unsigned int i = 0;
				for (Json::Value &dx : (*member))
				{
					if (i <= dest.frames.size())
						dest.frames.emplace_back();
					dest.frames[i].y = dx.asDouble();
					i++;
				}
			}
			else
			{
				dest.frames.emplace_back();
				dest.frames[0].y = (*member).asDouble();
			}
		}
		else if (name == "dtheta")
		{
			if ((*member).isArray())
			{
				unsigned int i = 0;
				for (Json::Value &dx : (*member))
				{
					if (i <= dest.frames.size())
						dest.frames.emplace_back();
					dest.frames[i].theta = dx.asDouble();
					i++;
				}
			}
			else
			{
				dest.frames.emplace_back();
				dest.frames[0].theta = (*member).asDouble();
			}
		}
		else if (name == "cycles")
		{
			if ((*member).isArray())
			{
				unsigned int i = 0;
				for (Json::Value &cycles : (*member))
				{
					if (i <= dest.frames.size())
						dest.frames.emplace_back();
					dest.frames[i].cycles = cycles.asUInt();
					i++;
				}
			}
			else
			{
				dest.frames.emplace_back();
				dest.frames[0].cycles = (*member).asUInt();
			}
		}
		else if (name == "cycleTimeUSec")
		{
			if ((*member).isArray())
			{
				unsigned int i = 0;
				for (Json::Value &cycleTimeUSec : (*member))
				{
					if (i <= dest.frames.size())
						dest.frames.emplace_back();
					dest.frames[i].cycleTimeUSec = cycleTimeUSec.asLargestUInt();
					i++;
				}
			}
			else
			{
				dest.frames.emplace_back();
				dest.frames[0].cycleTimeUSec = (*member).asLargestUInt();
			}
		}
	}
}

void ::services::json::factory(services::data::robot::AutMove &move, Json::Value &dest)
{
	dest["command"] = "OmniWalk";
	Json::Value &params = dest["params"];

	params["vx"] = move.vx;
	params["vy"] = move.vy;
	params["vt"] = move.vt;
}


void ::services::json::factory(Json::Value &source, services::data::robot::AutMove &dest)
{
	std::string name;
	Json::Value &params = source["params"];
	for (Json::Value::iterator member = params.begin(); member != params.end(); ++member)
	{
		name = member.name();
		if (name == "vx")
			dest.vx = (*member).asFloat();
		else if (name == "vy")
			dest.vy = (*member).asFloat();
		else if (name == "vt")
			dest.vt = (*member).asFloat();
	}
}

void ::services::json::factory(services::data::robot::AutFullMove &fullMove, Json::Value &dest)
{
	dest["command"] = "FullMove";
	Json::Value &params = dest["params"];

	params["vx"] = fullMove.move.vx;
	params["vy"] = fullMove.move.vy;
	params["vt"] = fullMove.move.vt;
	params["motion"] = fullMove.motion;
	params["pan"] = fullMove.head.pan;
	params["tilt"] = fullMove.head.tilt;
}

void ::services::json::factory(Json::Value &source, services::data::robot::Head &dest)
{
	Json::Value &params = source["params"];
	std::string name;
	for (Json::Value::iterator member = params.begin(); member != params.end(); ++member)
	{
		name = member.name();
		if (name == "pan")
			dest.pan = (*member).asFloat();
		else if (name == "tilt")
			dest.tilt = (*member).asFloat();
	}
}

void ::services::json::factory(services::data::robot::Head &head, Json::Value &dest)
{
	dest["command"] = "Head";
	Json::Value &params = dest["params"];

	params["pan"] = head.pan;
	params["tilt"] = head.tilt;
}

void ::services::json::factory(Json::Value &source, services::data::robot::AutFullMove &dest)
{
	Json::Value &params = source["params"];
	std::string name;
	for (Json::Value::iterator member = params.begin(); member != params.end(); ++member)
	{
		name = member.name();
		if (name == "vx")
			dest.move.vx = (*member).asFloat();
		else if (name == "vy")
			dest.move.vy = (*member).asFloat();
		else if (name == "vt")
			dest.move.vt = (*member).asFloat();
		else if (name == "motion")
			dest.motion = (*member).asUInt();
		else if (name == "pan")
			dest.head.pan = (*member).asFloat();
		else if (name == "tilt")
			dest.head.tilt = (*member).asFloat();
	}
}
