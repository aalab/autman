/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 28, 2015
 **/

#include <services/data/robot/kinematicoptions.h>

boost::optional<float> services::data::robot::KinematicOptions::stepX()
{
	return this->_stepX;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::stepX(float stepX)
{
	this->_stepX = stepX;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::stepY()
{
	return this->_stepY;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::stepY(float stepY)
{
	this->_stepY = stepY;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::stepAngle()
{
	return this->_stepAngle;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::stepAngle(float stepAngle)
{
	this->_stepAngle = stepAngle;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::maxHeightProp()
{
	return this->_maxHeightProp;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::maxHeightProp(float maxHeightProp)
{
	this->_maxHeightProp = maxHeightProp;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::transitionProp()
{
	return this->_transitionProp;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::transitionProp(float transitionProp)
{
	this->_transitionProp = transitionProp;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::stepHeight()
{
	return this->_stepHeight;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::stepHeight(float stepHeight)
{
	this->_stepHeight = stepHeight;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::hipPitch()
{
	return this->_hipPitch;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::hipPitch(float hipPitch)
{
	this->_hipPitch = hipPitch;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::anklePitch()
{
	return this->_anklePitch;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::anklePitch(float anklePitch)
{
	this->_anklePitch = anklePitch;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::xOffset()
{
	return this->_xOffset;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::xOffset(float xOffset)
{
	this->_xOffset = xOffset;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::yOffset()
{
	return this->_yOffset;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::yOffset(float yOffset)
{
	this->_yOffset = yOffset;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::zOffset()
{
	return this->_zOffset;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::zOffset(float zOffset)
{
	this->_zOffset = zOffset;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::transferAnkleRoll()
{
	return this->_transferAnkleRoll;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::transferAnkleRoll(float transferAnkleRoll)
{
	this->_transferAnkleRoll = transferAnkleRoll;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::transferHipRoll()
{
	return this->_transferHipRoll;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::transferHipRoll(float transferHipRoll)
{
	this->_transferHipRoll = transferHipRoll;
	return *this;
}
boost::optional<float> services::data::robot::KinematicOptions::transferY()
{
	return this->_transferY;
}

services::data::robot::KinematicOptions &services::data::robot::KinematicOptions::transferY(float transferY)
{
	this->_transferY = transferY;
	return *this;
}

void ::services::json::factory(services::data::robot::KinematicOptions &options, Json::Value &dest)
{
	dest["command"] = "KinematicsOptions";
	Json::Value &params = dest["params"];
	if (options.stepX())
		params["stepX"] = *options.stepX();
	if (options.stepY())
		params["stepY"] = *options.stepY();
	if (options.stepAngle())
		params["stepAngle"] = *options.stepAngle();
	if (options.maxHeightProp())
		params["maxHeightProp"] = *options.maxHeightProp();
	if (options.transitionProp())
		params["transitionProp"] = *options.transitionProp();
	if (options.stepHeight())
		params["stepHeight"] = *options.stepHeight();
	if (options.hipPitch())
		params["hipPitch"] = *options.hipPitch();
	if (options.anklePitch())
		params["anklePitch"] = *options.anklePitch();
	if (options.xOffset())
		params["xOffset"] = *options.xOffset();
	if (options.yOffset())
		params["yOffset"] = *options.yOffset();
	if (options.zOffset())
		params["zOffset"] = *options.zOffset();
	if (options.transferAnkleRoll())
		params["transferAnkleRoll"] = *options.transferAnkleRoll();
	if (options.transferHipRoll())
		params["transferHipRoll"] = *options.transferHipRoll();
	if (options.transferY())
		params["transferY"] = *options.transferY();
}

void ::services::json::factory(Json::Value &source, services::data::robot::KinematicOptions &dest)
{
	Json::Value &params = source["params"];
	std::string memberName;
	for (Json::ValueIterator member = params.begin(); member != params.end(); ++member)
	{
		memberName = member.name();
		if (memberName == "stepX")
			dest.stepX((*member).asFloat());
		if (memberName == "stepY")
			dest.stepY((*member).asFloat());
		if (memberName == "stepAngle")
			dest.stepAngle((*member).asFloat());
		if (memberName == "maxHeightProp")
			dest.maxHeightProp((*member).asFloat());
		if (memberName == "transitionProp")
			dest.transitionProp((*member).asFloat());
		if (memberName == "stepHeight")
			dest.stepHeight((*member).asFloat());
		if (memberName == "hipPitch")
			dest.hipPitch((*member).asFloat());
		if (memberName == "anklePitch")
			dest.anklePitch((*member).asFloat());
		if (memberName == "xOffset")
			dest.xOffset((*member).asFloat());
		if (memberName == "yOffset")
			dest.yOffset((*member).asFloat());
		if (memberName == "zOffset")
			dest.zOffset((*member).asFloat());
		if (memberName == "transferAnkleRoll")
			dest.transferAnkleRoll((*member).asFloat());
		if (memberName == "transferHipRoll")
			dest.transferHipRoll((*member).asFloat());
		if (memberName == "transferY")
			dest.transferY((*member).asFloat());
	}
}
