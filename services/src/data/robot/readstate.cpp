/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/robot/readstate.h>

services::data::robot::ReadStateError::ReadStateError()
{ }

services::data::robot::ReadStateError::ReadStateError(bool instruction, bool overload, bool checksum, bool range,
	bool overheating, bool anglelimit, bool inputVoltage)
	: instruction(instruction), overload(overload), checksum(checksum), range(range), overheating(overheating),
	anglelimit(anglelimit), inputVoltage(inputVoltage)
{ }

services::data::robot::ServoState::ServoState()
	: angle(0)
{}

services::data::robot::ServoState::ServoState(std::string name)
	: name(name)
{ }

void services::data::robot::ServoState::assign(services::data::robot::ServoState &o)
{
	this->name = o.name;
	this->angle = o.angle;
	this->speed = o.speed;
	this->load = o.load;
	this->temperature = o.temperature;
	this->voltage = o.voltage;
	this->error = o.error;
}

services::data::robot::ReadStateResponse::~ReadStateResponse()
{
	for (boost::unordered_map<std::string, services::data::robot::ServoState*>::iterator it = this->states.begin(); it != this->states.end(); ++it)
		delete it->second;
}

void services::data::robot::ServoState::update(float angle)
{
	this->lastUpdate = std::chrono::system_clock::now();
	this->angle = angle;
}

bool services::data::robot::ServoState::isExpired()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - this->lastUpdate).count() >= 300;
}

services::data::robot::ReadFullStateResponse::~ReadFullStateResponse()
{
	for (boost::unordered_map<std::string, services::data::robot::FullServoState*>::iterator it = this->states.begin(); it != this->states.end(); ++it)
		delete it->second;
}

void services::data::robot::FullServoState::assign(services::data::robot::FullServoState &o)
{
	this->name = o.name;
	this->angle = o.angle;
	this->speed = o.speed;
	this->load = o.load;
	this->temperature = o.temperature;
	this->voltage = o.voltage;
	this->error = o.error;
	//
	this->gains = o.gains;
	this->goalPosition = o.goalPosition;
	this->moving = o.moving;
	this->movingSpeed = o.movingSpeed;
	this->torqueLimit = o.torqueLimit;
}

void ::services::json::factory(Json::Value &source, services::data::robot::ReadStateResponse &dest)
{
	Json::Value &servos = source["data"];
	std::string name;
	boost::unordered_map<std::string, services::data::robot::ServoState *>::iterator state;
	services::data::robot::ServoState *statePtr;
	for (Json::ValueIterator member = servos.begin(); member != servos.end(); ++member)
	{
		name = member.name();
		Json::Value &error = (*member)["error"];

		state = dest.states.find(name);
		if (state == dest.states.end())
			dest.states.emplace(name, statePtr = new data::robot::ServoState(name));
		else
		{
			statePtr = state->second;
		}
		statePtr->angle = (*member)["angle"].asFloat();
		statePtr->speed = (*member)["speed"].asFloat();
		statePtr->load = (*member)["load"].asFloat();
		statePtr->temperature = (*member)["temperature"].asInt();
		statePtr->voltage = (*member)["voltage"].asFloat();
		statePtr->error.instruction = error["instruction"].asBool();
		statePtr->error.overload = error["overload"].asBool();
		statePtr->error.checksum = error["checksum"].asBool();
		statePtr->error.range = error["range"].asBool();
		statePtr->error.overheating = error["overheating"].asBool();
		statePtr->error.anglelimit = error["anglelimit"].asBool();
		statePtr->error.inputVoltage = error["inputVoltage"].asBool();
	}
}

void ::services::json::factory(Json::Value &source, services::data::robot::ReadFullStateResponse &dest)
{
	Json::Value &servos = source["data"];
	std::string name;
	boost::unordered_map<std::string, services::data::robot::FullServoState *>::iterator state;
	services::data::robot::FullServoState *statePtr;
	for (Json::ValueIterator member = servos.begin(); member != servos.end(); ++member)
	{
		name = member.name();
		Json::Value &error = (*member)["error"];

		state = dest.states.find(name);
		if (state == dest.states.end())
		{
			statePtr = new data::robot::FullServoState(name);
			dest.states.emplace(name, statePtr);
		}
		else
			statePtr = state->second;

		Json::Value &gains = (*member)["gains"];

		statePtr->angle = (*member)["angle"].asFloat();
		statePtr->speed = (*member)["speed"].asFloat();
		statePtr->gains.p = gains["p"].asInt();
		statePtr->gains.i = gains["i"].asInt();
		statePtr->gains.d = gains["d"].asInt();
		statePtr->load = (*member)["load"].asFloat();
		statePtr->temperature = (*member)["temperature"].asInt();
		statePtr->voltage = (*member)["voltage"].asFloat();
		statePtr->goalPosition = (*member)["goalPosition"].asFloat();
		statePtr->moving = (*member)["moving"].asBool();
		statePtr->movingSpeed = (*member)["movingSpeed"].asBool();
		statePtr->torqueLimit = (*member)["torqueLimit"].asBool();

		statePtr->error.instruction = error["instruction"].asBool();
		statePtr->error.overload = error["overload"].asBool();
		statePtr->error.checksum = error["checksum"].asBool();
		statePtr->error.range = error["range"].asBool();
		statePtr->error.overheating = error["overheating"].asBool();
		statePtr->error.anglelimit = error["anglelimit"].asBool();
		statePtr->error.inputVoltage = error["inputVoltage"].asBool();
	}
}

void ::services::json::factory(services::data::robot::ReadState &readState, Json::Value &dest)
{
	dest["command"] = "ReadState";
	Json::Value &params = dest["params"];
	if (!readState.actuators.empty())
	{
		Json::Value &actuators = params["actuators"];
		for (std::string a : readState.actuators)
			actuators.append(a);
	}
}

void ::services::json::factory(services::data::robot::ReadFullState &readState, Json::Value &dest)
{
	dest["command"] = "ReadFullState";
	Json::Value &params = dest["params"];
	if (!readState.actuators.empty())
	{
		Json::Value &actuators = params["actuators"];
		for (std::string a : readState.actuators)
			actuators.append(a);
	}
}

services::data::robot::FullServoState::FullServoState()
	: ServoState()
{ }

services::data::robot::FullServoState::FullServoState(const std::string &name)
	: ServoState(name)
{ }


unsigned long long int ::services::data::robot::msec(double msec)
{
	return msec * 1000;
}

unsigned long long int ::services::data::robot::sec(double seconds)
{
	return seconds * 1000000;
}
