/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/robot/frame.h>

services::data::robot::Frame::Frame()
	: x(0), y(0), theta(0), cycleTimeUSec(0), cycles(0)
{ }

services::data::robot::Frame::Frame(double x, double y, double theta)
	: x(x), y(y), theta(theta), cycleTimeUSec(0), cycles(0)
{ }

services::data::robot::Frame::Frame(double x, double y, double theta, unsigned long long int cycleTimeUSec, unsigned int cycles)
	: x(x), y(y), theta(theta), cycleTimeUSec(cycleTimeUSec), cycles(cycles)
{ }

void ::services::json::factory(services::data::robot::Frame &frame, Json::Value &dest)
{
	Json::Value jsonX(frame.x);
	dest["dx"].append(jsonX);
	Json::Value jsonY(frame.y);
	dest["dy"].append(jsonY);
	Json::Value jsonTheta(frame.theta);
	dest["dtheta"].append(jsonTheta);
	Json::Value jsonCycleTime(frame.cycleTimeUSec);
	dest["cycleTimeUSec"].append(jsonCycleTime);
	Json::Value jsonCycles(frame.cycles);
	dest["cycles"].append(jsonCycles);
}

void ::services::json::factory(Json::Value &source, services::data::robot::Frame &dest)
{
	std::string name;
	for (Json::Value::iterator member = source.begin(); member != source.end(); ++member)
	{
		name = member.name();
		if (name == "dx")
			dest.x = (*member).asFloat();
		else if (name == "dy")
			dest.y = (*member).asFloat();
		else if (name == "dtheta")
			dest.theta = (*member).asFloat();
		else if (name == "cycleTimeUSec")
			dest.cycleTimeUSec = (*member).asLargestUInt();
		else if (name == "cycles")
			dest.cycles = (*member).asUInt();
	}
}
