/**
 * @author J. Santos <jamillo@gmail.com>
 * @date November 02, 2015
 **/

#include <services/data/robot/basecommand.h>
#include <services/data/robot/basecommand_json.h>
#include <services/data/robot/actuator.h>
#include <services/data/robot/readstate.h>

void ::services::json::factory(data::robot::BaseCommand *source, Json::Value &dest)
{
	data::robot::Actuator *actuator = dynamic_cast<data::robot::Actuator*>(source);
	if (actuator)
	{
		factory(*actuator, dest);
	}
	else
	{
		data::robot::ReadState *readState = dynamic_cast<data::robot::ReadState*>(source);
		if (readState)
		{
			factory(*readState, dest);
		}
		else
		{
			// TODO: Decide what to do if there is no implementation.
		}
	}
}

void ::services::json::factory(Json::Value &source, services::data::robot::BaseCommand *dest)
{
	data::robot::Actuator *actuator = dynamic_cast<data::robot::Actuator*>(dest);
	if (actuator)
	{
		factory(source, *actuator);
	}
	else
	{
		data::robot::ReadStateResponse *readState = dynamic_cast<data::robot::ReadStateResponse*>(dest);
		if (readState)
		{
			factory(source, *readState);
		}
		else
		{
			// TODO: Decide what to do if there is no implementation.
		}
	}
}
