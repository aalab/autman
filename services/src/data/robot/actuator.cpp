/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/data/robot/actuator.h>
#include <json/value.h>

void services::data::robot::Actuator::reset()
{
	this->angle.reset();
	this->speed.reset();

	this->gainP.reset();
	this->gainI.reset();
	this->gainD.reset();
}

services::data::robot::Actuator::Actuator()
{ }

services::data::robot::Actuator::Actuator(std::string name)
	: name(name)
{ }

services::data::robot::Actuator::Actuator(std::string name, float angle)
	: name(name), angle(angle)
{ }

void ::services::json::factory(services::data::robot::Actuator &actuator, Json::Value &dest)
{
	dest["command"] = "Actuator";
	Json::Value &params = dest["params"];
	params["name"] = actuator.name;
	if (actuator.mode && ((*actuator.mode == services::data::robot::ActuatorMode::OFF) || (!actuator.angle)))
	{
		params["command"] = "mode";
		params["mode"] = (*actuator.mode == services::data::robot::ActuatorMode::ON ? "On" : "Off");
	}
	else
	{
		if (actuator.angle)
		{
			params["angle"] = *actuator.angle;
			params["command"] = "Position";
		}
		if (actuator.speed)
			params["speed"] = *actuator.speed;

		if (actuator.gainP)
			params["gainP"] = *actuator.gainP;
		if (actuator.gainI)
			params["gainI"] = *actuator.gainI;
		if (actuator.gainD)
			params["gainD"] = *actuator.gainD;
	}
}

void ::services::json::factory(Json::Value &source, services::data::robot::Actuator &dest)
{
	Json::Value &params = source["params"];
	std::string memberName;
	dest.reset();
	for (Json::ValueIterator member = params.begin(); member != params.end(); ++member)
	{
		memberName = member.name();
		if (memberName == "name")
			dest.name = member->asString();
		if (memberName == "mode")
			dest.mode = (member->asString() == "ON" ? services::data::robot::ActuatorMode::ON : services::data::robot::ActuatorMode::OFF);
		if (memberName == "angle")
			dest.angle = member->asFloat();
		else if (memberName == "speed")
			dest.speed = member->asFloat();
		else if (memberName == "gainP")
			dest.gainP = member->asUInt();
		else if (memberName == "gainI")
			dest.gainI = member->asUInt();
		else if (memberName == "gainD")
			dest.gainD = member->asUInt();
	}
}
