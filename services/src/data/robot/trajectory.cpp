/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 28, 2015
 **/

#include <json/value.h>
#include <services/data/robot/trajectory.h>


services::data::robot::Trajectory::Trajectory()
{ }

services::data::robot::Trajectory::Trajectory(std::string name, unsigned long long int cycleTimeUSec)
	: _name(name), _cycleTimeUSec(cycleTimeUSec)
{ }

std::string services::data::robot::Trajectory::name()
{
	return this->_name;
}

services::data::robot::Trajectory services::data::robot::Trajectory::name(std::string name)
{
	this->_name = name;
	return *this;
}

boost::optional<unsigned long long int> services::data::robot::Trajectory::cycleTimeUSec()
{
	return this->_cycleTimeUSec;
}

services::data::robot::Trajectory services::data::robot::Trajectory::cycleTimeUSec(unsigned long long int cycleTimeUSec)
{
	this->_cycleTimeUSec = cycleTimeUSec;
	return *this;
}

boost::optional<unsigned int> services::data::robot::Trajectory::cycles()
{
	return this->_cycles;
}

services::data::robot::Trajectory services::data::robot::Trajectory::cycles(unsigned int cycles)
{
	this->_cycles = cycles;
	return *this;
}

boost::optional<unsigned long long int> services::data::robot::Trajectory::entryCycleTimeUSec()
{
	return this->_entryCycleTimeUSec;
}

services::data::robot::Trajectory services::data::robot::Trajectory::entryCycleTimeUSec(unsigned long long int entryCycleTimeUSec)
{
	this->_entryCycleTimeUSec = entryCycleTimeUSec;
	return *this;
}

boost::optional<float> services::data::robot::Trajectory::motionSpeed()
{
	return this->_motionSpeed;
}

services::data::robot::Trajectory services::data::robot::Trajectory::motionSpeed(float motionSpeed)
{
	this->_motionSpeed = motionSpeed;
	return *this;
}

boost::optional<float> services::data::robot::Trajectory::motionEntrySpeed()
{
	return this->_motionEntrySpeed;
}

services::data::robot::Trajectory services::data::robot::Trajectory::motionEntrySpeed(float motionEntrySpeed)
{
	this->_motionEntrySpeed = motionEntrySpeed;
	return *this;
}

void ::services::json::factory(services::data::robot::Trajectory &source, Json::Value &dest)
{
	dest["command"] = "Trajectory";
	Json::Value &params = dest["params"];
	params["name"] = source.name();
	if (source.cycleTimeUSec())
		params["cycleTimeUSec"] = *source.cycleTimeUSec();
	if (source.cycles())
		params["cycles"] = *source.cycles();
	if (source.entryCycleTimeUSec())
		params["entryCycleTimeUSec"] = *source.entryCycleTimeUSec();
	if (source.motionSpeed())
		params["motionSpeed"] = *source.motionSpeed();
	if (source.motionEntrySpeed())
		params["motionEntrySpeed"] = *source.motionEntrySpeed();
}

void ::services::json::factory(Json::Value &source, services::data::robot::Trajectory &dest)
{
	Json::Value &params = source["params"];
	std::string memberName;
	for (Json::ValueIterator member = params.begin(); member != params.end(); ++member)
	{
		memberName = member.name();
		if (memberName == "name")
			dest.name(member->asString());
		else if (memberName == "cycleTimeUSec")
			dest.cycleTimeUSec(member->asUInt64());
		else if (memberName == "cycles")
			dest.cycles(member->asUInt());
		else if (memberName == "entryCycleTimeUSec")
			dest.entryCycleTimeUSec(member->asUInt64());
		else if (memberName == "motionSpeed")
			dest.motionSpeed(member->asFloat());
		else if (memberName == "motionEntrySpeed")
			dest.motionEntrySpeed(member->asFloat());
	}
}
