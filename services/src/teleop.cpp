/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include <services/teleop.h>

services::Teleop::Teleop(const std::string &address, unsigned int port)
	: UDPClient(address, port)
{ }

bool services::Teleop::inputstate(services::data::teleop::XboxGamepad &gamepad, data::robot::Head &head)
{
	Json::Value gamepadJson;
	gamepadJson["command"] = "inputstate";
	gamepadJson["type"] = "xbox";
	json::factory(gamepad, gamepadJson["data"]);
	if (this->send(gamepadJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson))
		{
			if (responseJson["data"]["head"].isObject())
				json::factory(responseJson["data"]["head"], head);
			return responseJson["success"].asBool();
		}
	}
	return false;
}

bool services::Teleop::inputstate(services::data::teleop::PSGamepad &gamepad, data::robot::Head &head)
{
	Json::Value gamepadJson;
	gamepadJson["command"] = "inputstate";
	gamepadJson["type"] = "playstation";
	json::factory(gamepad, gamepadJson["data"]);
	if (this->send(gamepadJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson))
		{
			if (responseJson["data"]["head"].isObject())
				json::factory(responseJson["data"]["head"], head);
			return responseJson["success"].asBool();
		}
	}
	return false;
}

bool services::Teleop::inputstate(services::data::teleop::Keyboard &keyboard, data::robot::Head &head)
{
	Json::Value keyboardJson;
	keyboardJson["command"] = "inputstate";
	keyboardJson["type"] = "keyboard";
	json::factory(keyboard, keyboardJson["data"]);
	if (this->send(keyboardJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson))
		{
			if (responseJson["data"]["head"].isObject())
				json::factory(responseJson["data"]["head"], head);
			return responseJson["success"].asBool();
		}
	}
	return false;
}

bool services::Teleop::inputstate(services::data::teleop::GamepadData &gamepad, data::robot::Head &head)
{
	if (services::data::teleop::Keyboard *data = dynamic_cast<services::data::teleop::Keyboard*>(&gamepad))
		return this->inputstate(*data, head);
	else if (services::data::teleop::XboxGamepad *data = dynamic_cast<services::data::teleop::XboxGamepad*>(&gamepad))
		return this->inputstate(*data, head);
	else if (services::data::teleop::PSGamepad *data = dynamic_cast<services::data::teleop::PSGamepad*>(&gamepad))
		return this->inputstate(*data, head);
	return false;
}
