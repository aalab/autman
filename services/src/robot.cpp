/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/robot.h>
#include <services/base/json.h>
#include <services/data/robot/basecommand_json.h>

services::Robot::Robot(std::string robotName, std::string address, unsigned int port)
	: UDPClient(address, port), robotName(robotName)
{ }

bool services::Robot::move(services::data::robot::Move &move)
{
	Json::Value moveJson;
	json::factory(move, moveJson);
	if (this->send(moveJson))
	{
		Json::Value responseJson;
		return (this->read(responseJson) && (responseJson["success"].asBool()));
	}
	return false;
}

bool services::Robot::actuator(services::data::robot::Actuator &actuator)
{
	Json::Value actuatorJson;
	json::factory(actuator, actuatorJson);
	if (this->send(actuatorJson))
	{
		Json::Value jsonResponse;
		return (this->read(jsonResponse) && (jsonResponse["success"].asBool()));
	}
	return false;
}

bool services::Robot::readState(data::robot::ReadState &readState, data::robot::ReadStateResponse &response)
{
	Json::Value readStateJson;
	json::factory(readState, readStateJson);
	if (this->send(readStateJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			json::factory(responseJson, response);
			return true;
		}
	}
	return false;
}

bool services::Robot::send(Json::Value &json)
{
	json["name"] = this->robotName;
	return services::base::UDPClient::send(json);
}

bool services::Robot::move(services::data::robot::AutMove &move)
{
	services::base::UDPClientLocker(*this);

	Json::Value moveJson;
	json::factory(move, moveJson);
	if (this->send(moveJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			return true;
		}
	}
	return false;
}

bool services::Robot::move(services::data::robot::Head &head)
{
	services::base::UDPClientLocker(*this);

	Json::Value headJson;
	json::factory(head, headJson);
	if (this->send(headJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			return true;
		}
	}
	return false;
}

bool services::Robot::move(services::data::robot::AutFullMove &move)
{
	services::base::UDPClientLocker(*this);

	Json::Value moveJson;
	json::factory(move, moveJson);
	if (this->send(moveJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			return true;
		}
	}
	return false;
}

bool services::Robot::readFullState(services::data::robot::ReadFullState &readState,
									services::data::robot::ReadFullStateResponse &response)
{
	Json::Value readStateJson;
	json::factory(readState, readStateJson);
	if (this->send(readStateJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			json::factory(responseJson, response);
			return true;
		}
	}
	return false;
}

bool services::Robot::kinematicOptions(data::robot::KinematicOptions &options, data::robot::KinematicOptions &result)
{
	Json::Value commandJson;
	json::factory(options, commandJson);
	if (this->send(commandJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			json::factory(responseJson, result);
			return true;
		}
	}
	return false;
}

bool services::Robot::trajectory(services::data::robot::Trajectory &options)
{
	Json::Value commandJson;
	json::factory(options, commandJson);
	if (this->send(commandJson))
	{
		Json::Value responseJson;
		if (this->read(responseJson) && (responseJson["success"].asBool()))
		{
			return true;
		}
	}
	return false;
}

bool services::Robot::multiCommands(std::vector<std::unique_ptr<services::data::robot::BaseCommand>> commands)
{
	Json::Value json;
	json["command"] = "MultiCommand";
	Json::Value &jsonCommands = (json["commands"] = Json::arrayValue);
	for (std::unique_ptr<services::data::robot::BaseCommand> &cmd : commands)
	{
		Json::Value& jsonCmd = jsonCommands.append(Json::objectValue);
		services::json::factory(cmd.get(), jsonCmd);
	}
	if (this->send(json))
	{
		Json::Value jsonResponse;
		if (this->read(jsonResponse))
		{
			return true;
		}
	}
	return false;
}
