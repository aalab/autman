/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/base/json.h>
#include "../compilerdefinitions.h"

#include <string>
#include <iostream>


services::data::Point2D &operator<<(services::data::Point2D &result, Json::Value &json)
{
	result.x = json["x"].asDouble();
	result.y = json["y"].asDouble();
	return result;
}

services::data::Line2D &operator<<(services::data::Line2D &result, Json::Value &json)
{
	result.a << json["a"];
	result.b << json["b"];
	return result;
}

services::data::BoundBox2D &operator<<(services::data::BoundBox2D &result, Json::Value &json)
{
	result.a << json["a"];
	result.b << json["b"];
	return result;
}

bool ::services::json::unserialize(std::string &json, Json::Value &dest)
{
	Json::Reader reader;
	return reader.parse(json, dest, false);
}

bool ::services::json::unserialize(char *buffer, unsigned int bufferLength, Json::Value &dest)
{
	std::string json(buffer, bufferLength);
	VERBOSE("[INCOMING] << " << json);
	return unserialize(json, dest);
}

void ::services::json::factory(Json::Value &source, services::data::vision::VisionObject &dest)
{
	std::string name;
	for (Json::ValueIterator member = source.begin(); member != source.end(); ++member)
	{
		name = member.name();
		if (name == "type")
			dest.type = member->asString();
		else if (name == "size")
			dest.size = member->asUInt();
		else if (name == "boundBox")
			factory((*member), dest.boundBox);
		else if (name == "center")
			factory((*member), dest.center);
		else if (name == "class")	// Ignore for now
		{ }
	}
}


void ::services::json::factory(Json::Value &source, services::data::vision::Mode &dest)
{
	dest.mode = source["params"]["mode"].asString();
}

void ::services::json::factory(services::data::vision::Mode &mode, Json::Value &dest)
{
	dest["command"] = "mode";
	dest["params"]["mode"] = mode.mode;
}

void ::services::json::factory(Json::Value &source, services::data::Line2D &dest)
{
	std::string name;
	for (Json::ValueIterator member = source.begin(); member != source.end(); member++)
	{
		name = member.name();
		if ((name == "a") || (name == "topLeft"))
			factory((*member), dest.a);
		else if ((name == "b") || (name == "bottomRight"))
			factory((*member), dest.b);
	}
}

void ::services::json::serialize(Json::Value &dest, std::string &json)
{
	Json::FastWriter writer;
	json = writer.write(dest);
}

void ::services::json::factory(data::vision::Fetch &data, Json::Value &dest)
{
	dest["command"] = "fetch";
}

std::chrono::system_clock::time_point epoch = std::chrono::system_clock::now();

void ::services::json::factory(Json::Value &source, services::data::vision::GoalAreaResponse &dest)
{
	// TODO: Extract the time
	std::chrono::system_clock::time_point t = epoch;
	long long int ti = (int) source["time"].asDouble();
	t -= (std::chrono::seconds(epoch.time_since_epoch().count()) + std::chrono::seconds(ti));
	for (Json::Value &line : source["lines"])
	{
		dest.lines.emplace_back();
		dest.lines.back() << line;
	}
}

bool ::services::json::factory(Json::Value &source, services::data::vision::FetchResponse &dest)
{
	if (!source.isNull())
	{
		Json::Value &jsonData = source["data"];
		if ((!jsonData.isNull()) && (jsonData.isArray()))
		{
			for (Json::Value &jsonVo : jsonData)
			{
				if (!dest.classifyJson(jsonVo))
				{
					dest.objects.emplace_back();
					factory(jsonVo, dest.objects.back());
					dest.classify(dest.objects.back());
				}
			}
			return true;
		}
	}
	return false;
}

void ::services::json::factory(Json::Value &source, services::data::vision::TsaiWrapper &dest)
{
	dest.name = source["name"].asString();
	factory(source["data"], dest.data);
}

void ::services::json::factory(Json::Value &source, tsai_calibration_constants &dest)
{
	dest.f = source["f"].asDouble();
	dest.kappa1 = source["kappa1"].asDouble();

	dest.p1 = source["p1"].asDouble();
	dest.p2 = source["p2"].asDouble();

	dest.Tx = source["Tx"].asDouble();
	dest.Ty = source["Ty"].asDouble();
	dest.Tz = source["Tz"].asDouble();

	dest.Rx = source["Rx"].asDouble();
	dest.Ry = source["Ry"].asDouble();
	dest.Rz = source["Rz"].asDouble();

	dest.r1 = source["r1"].asDouble();
	dest.r2 = source["r2"].asDouble();
	dest.r3 = source["r3"].asDouble();
	dest.r4 = source["r4"].asDouble();
	dest.r5 = source["r5"].asDouble();
	dest.r6 = source["r6"].asDouble();
	dest.r7 = source["r7"].asDouble();
	dest.r8 = source["r8"].asDouble();
	dest.r9 = source["r9"].asDouble();

	dest.is_load_constants = source["is_load_constants"].asInt();
}

void ::services::json::factory(Json::Value &source, tsai_camera_parameters &dest)
{
	dest.Ncx = source["Ncx"].asDouble();
	dest.Nfx = source["Nfx"].asDouble();

	dest.dx = source["dx"].asDouble();
	dest.dy = source["dy"].asDouble();

	dest.dpx = source["dpx"].asDouble();
	dest.dpy = source["dpy"].asDouble();

	dest.Cx = source["Cx"].asDouble();
	dest.Cy = source["Cy"].asDouble();

	dest.sx = source["sx"].asDouble();

	dest.is_load_paremeters = source["is_load_paremeters"].asInt();
}

void ::services::json::factory(Json::Value &source, services::data::vision::CameraCalibrations &dest)
{
	if (source["camera"].isObject())
		factory(source["camera"], dest.cameraParameters);
	for (Json::Value &ccJson : source["configurations"])
	{
		boost::optional<services::data::vision::TsaiWrapper&> cc = dest.calibrationByName(ccJson["name"].asString());
		if (!cc)
		{
			dest.calibrations.emplace_back(dest.cameraParameters);
			factory(ccJson, dest.calibrations.back());
		}
		else
			factory(ccJson, (*cc));
	}
}
