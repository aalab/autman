/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 29, 2015
 */

#include <services/base/udpserver.h>
#include <services/base/json.h>

#include "../compilerdefinitions.h"

#include <iostream>

using boost::asio::ip::udp;

services::base::UDPServer::UDPServer(boost::asio::io_service &io_service, unsigned int port)
	: socket(io_service, udp::endpoint(udp::v4(), port))
{ }

void services::base::UDPServer::start()
{
	char data[4096];
	size_t maxLength = sizeof(data);

	boost::system::error_code ec;

	this->running = true;
	while (this->running)
	{
		size_t len = this->socket.receive_from(boost::asio::buffer(data, maxLength), this->remoteEP, 0, ec);
		if (ec)
		{
			VERBOSE("[" << ec.category().name() << "] " << ec.message());
			if (ec == boost::system::errc::interrupted)
			{
				this->stop();
			}
		}
		else
		{
			std::string command(data, len);
			VERBOSE("Received command (" << len << "): " << command);
			try
			{
				this->processCommand(command);
			}
			catch (services::exceptions::InvalidJSON &e)
			{
				this->handleInvalidJSON(command, e);
			}
		}
	}
}

void services::base::UDPServer::stop()
{
	VERBOSE("Stopping UDPServer...");
	this->running = false;
	this->socket.cancel();
}

int services::base::UDPServer::send(const char *msg)
{
	std::string message(msg);
	return this->send(message);
}

int services::base::UDPServer::send(std::string &msg)
{
	const boost::asio::const_buffers_1 &bf = boost::asio::buffer(msg, msg.size());
	int
		sent = 0,
		remaining = msg.size(),
		ret;

	VERBOSE("Command sent: " << msg);
	while (remaining > 0)
	{
		ret = this->socket.send_to(bf, this->remoteEP);
		remaining -= ret;
		sent += ret;
	}
	return sent;
}

int services::base::UDPServer::send(Json::Value &json)
{
	std::string response;
	json::serialize(json, response);
	return this->send(response);
}

void services::base::UDPServer::processCommand(std::string &command)
{
	Json::Value json;
	if (json::unserialize(command, json))
	{
		this->processCommand(json);
	}
	else
	{
		ERROR("Invalid json input: " << command);
		throw services::exceptions::InvalidJSON(command);
	}
}

void services::base::UDPServer::processCommand(Json::Value &jsonCommand)
{ }

void services::base::UDPServer::handleInvalidJSON(std::string &command, services::exceptions::InvalidJSON &e)
{
	ERROR("Cannot parse: " << command);
	std::string message("{\"success\":false,\"message\":\"Error parsing request. It is not a valid JSON.\"}");
	this->send(message);
}
