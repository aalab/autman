/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/base/exceptions.h>

services::exceptions::ServiceException::ServiceException(std::string msg)
	: _msg(msg)
{ }

std::string services::exceptions::ServiceException::msg()
{
	return this->_msg;
}

services::exceptions::InvalidMemberName::InvalidMemberName(std::string memberName)
	: ServiceException("Invalid member name '" + memberName + '"'), memberName(memberName)
{ }

services::exceptions::InvalidJSON::InvalidJSON(std::string &json)
	: ServiceException("Invalid JSON: " + json)
{ }
