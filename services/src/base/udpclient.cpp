/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include <services/base/udpclient.h>
#include <services/base/json.h>
#include "../compilerdefinitions.h"

#include <iostream>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/bind/bind.hpp>

#define VERBOSEUDPCLIENT(x) VERBOSE(x)
// #define VERBOSEUDPCLIENT(x)

using boost::asio::ip::udp;

services::base::UDPClient::UDPClient(std::string address, unsigned int port)
	: address(address), port(port), _defaultTimeout(boost::posix_time::milliseconds(1000)), io_service(),
	  socket(io_service, udp::endpoint( udp::v4(), 0)), deadlineTimer(io_service), resolver(io_service),
	  query( udp::v4(), address, std::to_string(port) ), udpIterator(resolver.resolve( query ))
{
	this->deadlineTimer.expires_at(boost::posix_time::pos_infin);
	this->checkDeadline();
}

int services::base::UDPClient::send(std::string const &msg)
{
	const boost::asio::const_buffers_1 &bf = boost::asio::buffer(msg, msg.size());
	int
		sent = 0,
		remaining = msg.size(),
		ret;

	while (remaining > 0)
	{
		ret = this->socket.send_to(bf, *this->udpIterator);
		VERBOSEUDPCLIENT("OUT(" << this->address << ":" << this->port << ")>> " << msg);
		remaining -= ret;
		sent += ret;
	}
	return sent;
}

bool services::base::UDPClient::send(Json::Value &json)
{
	std::string msg;
	json::serialize(json, msg);
	int ret = this->send(msg);
	return (ret > 0);
}

std::size_t services::base::UDPClient::read(
	char *buffer, unsigned int bufferLength, boost::posix_time::time_duration timeout, boost::system::error_code& ec
)
{
	this->deadlineTimer.expires_from_now(timeout);

	ec = boost::asio::error::would_block;
	std::size_t length = 0;

	this->socket.async_receive(boost::asio::buffer(buffer, bufferLength), boost::bind(&UDPClient::handleReceive, _1, _2, &ec, &length));

	// Block until the asynchronous operation has completed.
	do {
		this->io_service.run_one();
	} while (ec == boost::asio::error::would_block);

	return length;

	/*
	boost::system::error_code ec;
	udp::endpoint sender_endpoint;
	size_t len = this->socket.receive_from(boost::asio::buffer(buffer, bufferLength), sender_endpoint, 0, ec);
	if ((!ec) && (len > 0))
	{
		VERBOSEUDPCLIENT("IN(" << this->address << ":" << this->port << ")<< " << buffer);
		return true;
	}
	else
	{
		VERBOSEUDPCLIENT("IN(" << this->address << ":" << this->port << ")<< FAILED! (" << len << ")(" << ec.category().name() << ":" << ec.value() << ":" << ec.message() << ")");
		return false;
	}
	 */
}

bool services::base::UDPClient::read(
	Json::Value &json, boost::posix_time::time_duration timeout, boost::system::error_code& ec
)
{
	char buffer[4096];
	unsigned int len = this->read(buffer, sizeof(buffer), timeout, ec);
	VERBOSE(ec.category().name());
	if (!ec)
	{
		VERBOSE("OK!");
		return json::unserialize(buffer, len, json);
	}
	else
	{
		VERBOSE("EC Error: " << ec.message());
		return false;
	}
}

void services::base::UDPClient::checkDeadline()
{
	if (this->deadlineTimer.expires_at() <= deadline_timer::traits_type::now())
	{
		this->socket.cancel();
		this->deadlineTimer.expires_at(boost::posix_time::pos_infin);
	}
	this->deadlineTimer.async_wait(boost::bind(&UDPClient::checkDeadline, this));
}

void services::base::UDPClient::handleReceive(
	const boost::system::error_code &ec, std::size_t length, boost::system::error_code *out_ec, std::size_t *out_length
)
{
	*out_ec = ec;
	*out_length = length;
}

bool services::base::UDPClient::read(Json::Value &json)
{
	boost::system::error_code ec;
	return this->read(json, this->_defaultTimeout, ec);
}

services::base::UDPClientLocker::UDPClientLocker(UDPClient &client)
	: client(client)
{
	client.transactionMutex.lock();
}

services::base::UDPClientLocker::~UDPClientLocker()
{
	client.transactionMutex.unlock();
}

const boost::posix_time::time_duration &services::base::UDPClient::defaultTimeout() const
{
	return this->_defaultTimeout;
}

services::base::UDPClient &services::base::UDPClient::defaultTimeout(boost::posix_time::time_duration duration)
{
	this->_defaultTimeout = duration;
	return *this;
}
