/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_ROBOT_H
#define ROBOCUP_ROBOT_H

#include <boost/asio/io_service.hpp>
#include <services/data/robot/readstate.h>
#include <mutex>
#include <services/data/robot/kinematicoptions.h>
#include <services/data/robot/trajectory.h>
#include <services/base/udpclient.h>
#include <services/data/robot/move.h>
#include <services/data/robot/actuator.h>

#ifndef override
#define override
#endif

namespace services
{
class Robot
	: public base::UDPClient
{
protected:
	virtual bool send(Json::Value &json) override;
public:
	std::string &robotName;

	Robot(std::string robotName, std::string address, unsigned int port);

	bool move(data::robot::Move &move);
	bool move(data::robot::AutMove &move);
	bool move(data::robot::Head &head);
	bool move(data::robot::AutFullMove &move);
	bool actuator(data::robot::Actuator &actuator);
	bool readState(data::robot::ReadState &readState, data::robot::ReadStateResponse &response);
	bool readFullState(data::robot::ReadFullState &readState, data::robot::ReadFullStateResponse &response);
	bool kinematicOptions(data::robot::KinematicOptions &options, data::robot::KinematicOptions &result);
	bool trajectory(data::robot::Trajectory &options);

	bool multiCommands(std::vector<std::unique_ptr<data::robot::BaseCommand>> commands);
};
}

#endif //ROBOCUP_ROBOT_H
