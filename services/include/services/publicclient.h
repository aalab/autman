/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 28, 2015
 **/

#ifndef SERVICES_PROXY_H
#define SERVICES_PROXY_H

#include <services/base/udpclient.h>
#include <string>

#ifndef override
#define override
#endif

namespace services
{
class PublicClient
	: public base::UDPClient
{
public:
	PublicClient(const std::string &address, unsigned int port);

	virtual int send(std::string const &msg) override;

	virtual bool send(Json::Value &json) override;

	virtual std::size_t read(char *buffer, unsigned int bufferLength, boost::posix_time::time_duration timeout,
							 boost::system::error_code &ec) override;

	virtual bool read(Json::Value &json, boost::posix_time::time_duration timeout,
					  boost::system::error_code &ec) override;

	virtual bool read(Json::Value &json) override;
};
}

#endif //SERVICES_PROXY_H
