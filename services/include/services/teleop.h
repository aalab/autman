/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef SERVICES_TELEOP_HPP
#define SERVICES_TELEOP_HPP

#include <services/base/udpclient.h>
#include <services/data/teleop/xbox.h>
#include <services/data/teleop/playstation.h>
#include <services/data/teleop/keyboard.h>
#include <services/data/teleop/touch.h>
#include <services/data/robot/move.h>

namespace services
{
class Teleop
	: public base::UDPClient
{
public:
	Teleop(const std::string &address, unsigned int port);

	/**
	 * Sends an `inputstate` command for either Keyboard or XBox or Playstation gamepads.
	 */
	bool inputstate(data::teleop::GamepadData &gamepad, data::robot::Head &head);

	/**
	 * Sends an `inputstate` command for XBox gamepads.
	 */
	bool inputstate(data::teleop::XboxGamepad &gamepad, data::robot::Head &head);

	/**
	 * Sends an `inputstate` command for Playstation gamepads.
	 */
	bool inputstate(data::teleop::PSGamepad &gamepad, data::robot::Head &head);

	/**
	 * Sends an `inputstate` command for keyboard.
	 */
	bool inputstate(data::teleop::Keyboard &keyboard, data::robot::Head &head);
};
}


#endif //SERVICES_TELEOP_HPP
