/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_VISION_H
#define ROBOCUP_VISION_H

#include "base/udpclient.h"

#include "data/vision/fetch.h"
#include "data/vision/mode.h"
#include "data/vision/cameracalibrations.h"

namespace services
{
class Vision
	: public base::UDPClient
{
public:
	Vision(std::string address, unsigned int port);

	data::vision::CameraCalibrations cameraCalibrations;

	bool fetch(data::vision::FetchResponse &response);
	bool mode(data::vision::Mode &mode);
	bool updateCameraCalibrations();
};
}


#endif //ROBOCUP_VISION_H
