/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_GAMECONTROLLER_H
#define ROBOCUP_GAMECONTROLLER_H

#include <netinet/in.h>

#include "../data/robocup/gamecontroller.h"

namespace services
{
namespace robocup
{
class GameController
{
public:
	GameController();

	virtual ~GameController();

	bool initNetwork();

	void createResponse(uint32 msg = 2);

	void initResponse();

	bool update();

	unsigned int getState();

private:

	int m_fd;
	sockaddr_in6 m_addr;
	sockaddr_in6 m_ret_addr;
	socklen_t m_ret_addr_len;

	fd_set m_rfds;

	data::robocup::RoboCupGameControlData m_data;
	data::robocup::RoboCupGameControlReturnData m_response;

	const int m_teamNumber; // = 30;
	const unsigned int m_robotNumber; // = 4;

	bool communicate();

	void parseData();
};
}
}

#endif //ROBOCUP_GAMECONTROLLER_H
