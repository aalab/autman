/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_UDPREMOTE_H
#define ROBOCUP_UDPREMOTE_H

#include <string>

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <json/value.h>
#include <mutex>

using boost::asio::ip::udp;
using boost::asio::deadline_timer;

namespace services
{
namespace base
{
class UDPClient;

class UDPClientLocker
{
private:
	UDPClient &client;
public:
	UDPClientLocker(UDPClient &client);
	~UDPClientLocker();
};

class UDPClient
{

friend class UDPClientLocker;

private:
	std::string address;
	unsigned int port;

	boost::posix_time::time_duration _defaultTimeout;

	boost::asio::io_service io_service;
	udp::socket socket;
	deadline_timer deadlineTimer;

	udp::resolver resolver;
	udp::resolver::query query;
	udp::resolver::iterator udpIterator;

protected:
	std::mutex transactionMutex;

	virtual int send(std::string const &msg);
	virtual bool send(Json::Value &json);
	virtual std::size_t read(char *buffer, unsigned int bufferLength, boost::posix_time::time_duration timeout, boost::system::error_code& ec);
	virtual bool read(Json::Value &json, boost::posix_time::time_duration timeout, boost::system::error_code& ec);

	virtual bool read(Json::Value &json);

	virtual void checkDeadline();

	static void handleReceive(
		const boost::system::error_code& ec, std::size_t length, boost::system::error_code* out_ec,
		std::size_t* out_length
	);
public:
	UDPClient(std::string address, unsigned int port);

	const boost::posix_time::time_duration &defaultTimeout() const;
	UDPClient &defaultTimeout(boost::posix_time::time_duration duration);
};
};
};


#endif //ROBOCUP_UDPREMOTE_H
