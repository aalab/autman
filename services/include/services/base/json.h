/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_JSON_H
#define ROBOCUP_JSON_H

#include <json/value.h>
#include <json/reader.h>
#include <json/writer.h>

#include "exceptions.h"

#include "../data/robot/actuator.h"
#include "../data/robot/move.h"

#include "../data/gyro/calibration.h"
#include "../data/gyro/data.h"

#include "../data/vision/visionobject.h"
#include "../data/vision/fetch.h"
#include "../data/vision/tsai.h"
#include "../data/vision/cameracalibrations.h"

#include <tsai2d/tsai2D.h>
#include <services/data/vision/mode.h>

services::data::Point2D &operator<<(services::data::Point2D &result, Json::Value &json);

services::data::Line2D &operator<<(services::data::Line2D &result, Json::Value &json);

services::data::BoundBox2D &operator<<(services::data::BoundBox2D &result, Json::Value &json);

namespace services
{
namespace json
{
bool unserialize(std::string &json, Json::Value &dest);

bool unserialize(char *buffer, unsigned int bufferLength, Json::Value &dest);

void serialize(Json::Value &dest, std::string &json);

void factory(Json::Value &source, data::vision::VisionObject &dest);

void factory(Json::Value &source, data::vision::Mode &dest);

void factory(data::vision::Mode &mode, Json::Value &dest);

void factory(Json::Value &source, data::Line2D &dest);

void factory(data::gyro::Calibration &calibration, Json::Value &dest);

void factory(data::gyro::Data &data, Json::Value &dest);

void factory(data::vision::Fetch &data, Json::Value &dest);

void factory(Json::Value &source, data::gyro::DataResponse &dest);

void factory(Json::Value &source, data::vision::GoalAreaResponse &dest);

bool factory(Json::Value &source, data::vision::FetchResponse &dest);

void factory(Json::Value &source, data::vision::TsaiWrapper &dest);

void factory(Json::Value &source, tsai_calibration_constants &dest);

void factory(Json::Value &source, tsai_camera_parameters &dest);

void factory(Json::Value &source, data::vision::CameraCalibrations &dest);
}
}


#endif //ROBOCUP_JSON_H
