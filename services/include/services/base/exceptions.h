/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_EXCEPTIONS_H
#define ROBOCUP_EXCEPTIONS_H

#include <string>
#include <exception>

#include "../../../src/compilerdefinitions.h"

namespace services
{
namespace exceptions
{

class ServiceException
	: public std::exception
{
private:
	std::string _msg;
public:
	ServiceException(std::string msg);

#ifdef LOOSER_THROW
	virtual ~ServiceException() throw() {};
#endif

	virtual std::string msg();
};

class InvalidMemberName
	: public ServiceException
{
public:
	std::string memberName;

	InvalidMemberName(std::string memberName);
#ifdef LOOSER_THROW
	virtual ~InvalidMemberName() throw() {};
#endif
};

class InvalidJSON
	: public ServiceException
{
public:
	InvalidJSON(std::string &json);
#ifdef LOOSER_THROW
	virtual ~InvalidJSON() throw() {};
#endif
};
}
}


#endif //ROBOCUP_EXCEPTIONS_H
