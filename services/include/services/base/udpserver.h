/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 29, 2015
 */

#ifndef SERVICES_UDPSERVER_H
#define SERVICES_UDPSERVER_H

#include <string>
#include <json/value.h>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include "exceptions.h"

namespace services
{
namespace base
{
class UDPServer
{
private:
	boost::asio::ip::udp::socket socket;
	boost::asio::ip::udp::endpoint remoteEP;
	bool running;
public:
	UDPServer(boost::asio::io_service &io_service, unsigned int port);

	void start();
	void stop();
protected:
	virtual int send(const char *msg);
	virtual int send(std::string &msg);
	virtual int send(Json::Value &json);

	virtual void processCommand(std::string &command);
	virtual void processCommand(Json::Value &jsonCommand);

	virtual void handleInvalidJSON(std::string &command, services::exceptions::InvalidJSON &e);
};
}
}

#endif //SERVICES_UDPSERVER_H
