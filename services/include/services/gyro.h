/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef SERVICES_GYRO_H
#define SERVICES_GYRO_H

#include <services/data/gyro/data.h>
#include <services/base/udpclient.h>

namespace services
{
class Gyro
	: public base::UDPClient
{
public:
	Gyro(std::string address, unsigned int port);

	bool calibrate();
	bool data(data::gyro::DataResponse &response);
};
}

#endif //SERVICES_GYRO_H
