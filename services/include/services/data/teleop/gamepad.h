/**
 * @author J. Santos <jamillo@gmail.com>
 * @created September 14, 2015
 */
#ifndef SERVICES_DATA_TELEOP_GAMEPAD_H
#define SERVICES_DATA_TELEOP_GAMEPAD_H

#include <services/data/point.h>
#include <json/value.h>

namespace services
{
namespace data
{
namespace teleop
{

class GamepadData
{
public:
	virtual ~GamepadData();
};

class Axis
	: public Point2D
{
public:
	Axis();

	Axis(double x, double y);
};
}
}

namespace json
{
void factory(data::teleop::Axis &source, Json::Value &dest);
void factory(Json::Value &source, data::teleop::Axis &dest);
}
}

#endif // SERVICES_DATA_TELEOP_GAMEPAD_H
