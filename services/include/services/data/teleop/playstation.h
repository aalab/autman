/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef SERVICES_DATA_TELEOP_PLAYSTATION_H
#define SERVICES_DATA_TELEOP_PLAYSTATION_H

#include "gamepad.h"

namespace services
{
namespace data
{
namespace teleop
{
class PSGamepad
	: public GamepadData
{
private:
	float _square;
	float _triangle;
	float _circle;
	float _x;
	float _select;
	float _start;
	Axis _cross;
	Axis _rjoystick;
	float _left1;
	float _left2;
	Axis _ljoystick;
	float _right1;
	float _right2;
public:
	const float square() const;
	PSGamepad &square(float square);

	const float triangle() const;
	PSGamepad &triangle(float triangle);

	const float circle() const;
	PSGamepad &circle(float circle);

	const float x() const;
	PSGamepad &x(float x);

	const float select() const;
	PSGamepad &select(float select);

	const float start() const;
	PSGamepad &start(float start);

	Axis &cross();
	PSGamepad &cross(Axis &cross);

	Axis &rjoystick();
	PSGamepad &rjoystick(Axis &rjoystick);

	const float left1() const;
	PSGamepad &left1(float left1);

	const float left2() const;
	PSGamepad &left2(float left2);

	Axis &ljoystick();
	PSGamepad &ljoystick(Axis &ljoystick);

	const float right1() const;
	PSGamepad &right1(float right1);

	const float right2() const;
	PSGamepad &right2(float right2);

};
}
}
namespace json
{
void factory(Json::Value &source, data::teleop::PSGamepad &dest);
void factory(data::teleop::PSGamepad &source, Json::Value &dest);
}
}


#endif // SERVICES_DATA_TELEOP_PLAYSTATION_H
