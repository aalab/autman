/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef SERVICES_DATA_TELEOP_KEYBOARD_H
#define SERVICES_DATA_TELEOP_KEYBOARD_H

#include <services/data/teleop/gamepad.h>

namespace services
{
namespace data
{
namespace teleop
{
class Keyboard
	: public GamepadData
{
private:
	float _control;
	float _shift;
	float _alt;
	float _space;
	Axis _arrowLetters;
	Axis _arrows;

	float _p;
	float _a;
	float _z;
	float _u;
	float _j;
	float _m;
public:
	const float control() const;
	Keyboard &control(float control);

	const float shift() const;
	Keyboard &shift(float shift);

	const float alt() const;
	Keyboard &alt(float alt);

	const float space() const;
	Keyboard &space(float space);

	Axis &arrowLetters();
	Keyboard &arrowLetters(Axis &arrowLetters);

	Axis &arrows();
	Keyboard &arrows(Axis &arrows);

	const float p() const;
	Keyboard &p(float p);

	const float a() const;
	Keyboard &a(float a);

	const float z() const;
	Keyboard &z(float z);

	const float u() const;
	Keyboard &u(float u);

	const float j() const;
	Keyboard &j(float j);

	const float m() const;
	Keyboard &m(float m);
};
}
}

namespace json
{
	void factory(Json::Value &source, data::teleop::Keyboard &dest);
	void factory(data::teleop::Keyboard &source, Json::Value &dest);
}
}

#endif //SERVICES_DATA_TELEOP_KEYBOARD_H
