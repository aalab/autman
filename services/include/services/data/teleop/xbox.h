/**
 * @author J. Santos <jamillo@gmail.com>
 * @date 09 14, 2015
 **/

#ifndef SERVICES_DATA_TELEOP_XBOX_H
#define SERVICES_DATA_TELEOP_XBOX_H

#include "gamepad.h"

namespace services
{
namespace data
{
namespace teleop
{
class XboxGamepad
	: public GamepadData
{
private:
	float _a;
	float _b;

	float _x;
	float _y;

	float _back;
	float _start;

	float _xbox;

	float _ljoystickButton;
	float _rjoystickButton;

	Axis _ljoystick;
	float _ltrigger;

	Axis _rjoystick;
	float _rtrigger;

	Axis _cross;
public:
	const float a() const;
	XboxGamepad &a(float a);

	const float b() const;
	XboxGamepad &b(float b);

	const float x() const;
	XboxGamepad &x(float x);

	const float y() const;
	XboxGamepad &y(float y);

	const float back() const;
	XboxGamepad &back(float back);

	const float start() const;
	XboxGamepad &start(float start);

	const float xbox() const;
	XboxGamepad &xbox(float xbox);

	const float ljoystickButton() const;
	XboxGamepad &ljoystickButton(float ljoystickButton);

	const float rjoystickButton() const;
	XboxGamepad &rjoystickButton(float rjoystickButton);

	Axis &ljoystick();
	XboxGamepad &ljoystick(Axis &ljoystick);

	const float ltrigger() const;
	XboxGamepad &ltrigger(float ltrigger);

	Axis &rjoystick();
	XboxGamepad &rjoystick(Axis &rjoystick);

	const float rtrigger() const;
	XboxGamepad &rtrigger(float rtrigger);

	Axis &cross();
	XboxGamepad &cross(Axis &cross);
};
}
}

namespace json
{
void factory(Json::Value &source, data::teleop::XboxGamepad &dest);
void factory(data::teleop::XboxGamepad &source, Json::Value &dest);
}
}

#endif // SERVICES_DATA_TELEOP_XBOX_H
