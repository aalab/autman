/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 16, 2015
 **/

#ifndef SERVICES_DATA_TELEOP_TOUCH_H
#define SERVICES_DATA_TELEOP_TOUCH_H

#include <services/data/teleop/gamepad.h>

namespace services
{
namespace data
{
namespace teleop
{
class Touch
	: public GamepadData
{
private:
	Axis _left;
	Axis _right;
public:
	Axis &left();
	Touch &left(Axis &left);

	Axis &right();
	Touch &right(Axis &right);
};
}
}

namespace json
{
	void factory(Json::Value &source, data::teleop::Touch &dest);
	void factory(data::teleop::Touch &source, Json::Value &dest);
}
}


#endif //SERVICES_DATA_TELEOP_TOUCH_H
