/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef SERVICES_DATA_BOUNDBOX_H
#define SERVICES_DATA_BOUNDBOX_H

#include "line.h"

namespace services
{
namespace data
{
class BoundBox2D : public Line2D
{
public:
	double area();
};
}
}


#endif // SERVICES_DATA_BOUNDBOX_H
