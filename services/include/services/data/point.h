/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef SERVICES_DATA_POINT_H
#define SERVICES_DATA_POINT_H

#include <json/value.h>

namespace services
{
namespace data
{
class Point2D
{
public:
	Point2D();

	Point2D(double x, double y);

	double x;
	double y;
};

class Point3D
	: public Point2D
{
public:
	Point3D();

	Point3D(double x, double y, double z);

	double z;

	Point3D &operator=(const Point3D &source)
	{
		this->x = source.x;
		this->y = source.y;
		this->z = source.z;
		return *this;
	}
};
}

namespace json
{
void factory(Json::Value &source, data::Point2D &dest);
void factory(data::Point2D &source, Json::Value &dest);
void factory(Json::Value &source, data::Point3D &dest);
void factory(data::Point3D &source, Json::Value &dest);
}
}
::services::data::Point3D &operator<<(services::data::Point3D &result, Json::Value &json);

#endif // SERVICES_DATA_POINT_H
