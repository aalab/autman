/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_DATA_GYRO_DATA_H
#define ROBOCUP_DATA_GYRO_DATA_H

#include "../point.h"

namespace services
{
namespace data
{
namespace gyro
{
class Data
{ };

class DataResponse
{
public:
	float deltaT;
	Point3D accelerometer;
	Point3D filtered;
	Point3D gyro;
};

}
}

namespace json
{
void factory(services::data::gyro::Data &data, Json::Value &dest);

void factory(Json::Value &source, services::data::gyro::DataResponse &dest);
}
}


#endif //ROBOCUP_DATA_GYRO_DATA_H
