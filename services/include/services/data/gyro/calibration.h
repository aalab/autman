/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_CALIBRATION_H
#define ROBOCUP_CALIBRATION_H

#include <json/value.h>

namespace services
{
namespace data
{
namespace gyro
{
class Calibration
{ };
}
}
namespace json
{
void factory(data::gyro::Calibration &calibration, Json::Value &dest);
}
}


#endif //ROBOCUP_CALIBRATION_H
