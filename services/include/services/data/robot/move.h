/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_MOVE_H
#define ROBOCUP_MOVE_H

#include <vector>
#include <boost/optional/optional.hpp>
#include <json/value.h>

#include "frame.h"
#include "basecommand.h"

namespace services
{
namespace data
{
namespace robot
{
class AutMove
{
public:
	AutMove();

	AutMove(float vx, float vy, float vt);

	float vx;
	float vy;
	float vt;
};

class Head
{
public:
	Head();

	Head(float pan, float tilt);

	float pan;
	float tilt;
};

class AutFullMove
{
public:
	AutFullMove();

	AutFullMove(float vx, float vy, float vt, unsigned int motion, float pan, float tilt);

	AutMove move;

	unsigned int motion;

	Head head;
};

class Move
	: public BaseCommand
{
public:
	boost::optional<bool> stop;
	boost::optional<unsigned long long int> entryTimeUSec;
	boost::optional<float> motionSpeed;
	boost::optional<float> motionEntrySpeed;
	std::vector<Frame> frames;
};
}
}

namespace json
{
void factory(data::robot::Move &move, Json::Value &dest);

void factory(Json::Value &source, data::robot::Move &dest);

void factory(data::robot::AutMove &move, Json::Value &dest);

void factory(Json::Value &source, data::robot::AutMove &dest);

void factory(data::robot::AutFullMove &fullMove, Json::Value &dest);

void factory(Json::Value &source, data::robot::Head &dest);

void factory(data::robot::Head &head, Json::Value &dest);

void factory(Json::Value &source, data::robot::AutFullMove &dest);
}
}

#endif //ROBOCUP_MOVE_H
