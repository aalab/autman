/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_READSTATE_H
#define ROBOCUP_READSTATE_H

#include <string>
#include <vector>
#include <boost/unordered_map.hpp>
#include <chrono>
#include <json/value.h>
#include "basecommand.h"

namespace services
{
namespace data
{
namespace robot
{

unsigned long long int msec(double msec);
unsigned long long int sec(double seconds);

class ReadState
	: public BaseCommand
{
public:
	std::vector<std::string> actuators;
};

class ReadFullState
	: public ReadState
{
};

class ReadStateError
{
public:
	ReadStateError();
	ReadStateError(
		bool instruction, bool overload, bool checksum, bool range, bool overheating, bool anglelimit,
		bool inputVoltage
	);

	bool instruction;
	bool overload;
	bool checksum;
	bool range;
	bool overheating;
	bool anglelimit;
	bool inputVoltage;
};

class ServoState
{
public:
	ServoState();
	ServoState(std::string name);

	std::chrono::system_clock::time_point lastUpdate;

	std::string name;

	float angle;
	float speed;
	float load;
	int temperature;
	float voltage;

	ReadStateError error;

	void assign(ServoState &o);

	void update(float angle);

	bool isExpired();
};

class Gains
{
public:
	int p;
	int i;
	int d;

	Gains operator=(const Gains &gain)
	{
		this->p = gain.p;
		this->i = gain.i;
		this->d = gain.d;
	}
};

class FullServoState
	: public ServoState
{
public:
	FullServoState();

	FullServoState(const std::string &name);

	Gains gains;

	float goalPosition;

	bool moving;

	float movingSpeed;

	float torqueLimit;

	void assign(FullServoState &o);
};

class ReadStateResponse
	: public BaseCommand
{
public:
	~ReadStateResponse();

	boost::unordered_map<std::string, ServoState*> states;
};

class ReadFullStateResponse
{
public:
	~ReadFullStateResponse();

	boost::unordered_map<std::string, FullServoState*> states;
};
}
}

namespace json
{
	void factory(Json::Value &source, data::robot::ReadStateResponse &dest);
	void factory(Json::Value &source, data::robot::ReadFullStateResponse &dest);
	void factory(data::robot::ReadState &readState, Json::Value &dest);
	void factory(data::robot::ReadFullState &readState, Json::Value &dest);
}
}

#endif //ROBOCUP_READSTATE_H
