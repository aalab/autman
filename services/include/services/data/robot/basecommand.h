/**
 * @author J. Santos <jamillo@gmail.com>
 * @date November 02, 2015
 **/

#ifndef SERVICES_DATA_ROBOT_BASECOMMAND_H
#define SERVICES_DATA_ROBOT_BASECOMMAND_H

namespace services
{
namespace data
{
namespace robot
{
class BaseCommand
{
public:
	virtual ~BaseCommand();
};
}
}
}


#endif //SERVICES_DATA_ROBOT_BASECOMMAND_H
