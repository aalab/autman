/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_FRAME_H
#define ROBOCUP_FRAME_H

#include <json/value.h>

namespace services
{
namespace data
{
namespace robot
{
class Frame
{
public:
	Frame();
	Frame(double x, double y, double theta);
	Frame(
		double x, double y, double theta, unsigned long long int cycleTimeUSec, unsigned int cycles
	);

	double x;
	double y;
	double theta;
	unsigned long long int cycleTimeUSec;
	unsigned int cycles;
};
}
}
namespace json
{
	void factory(data::robot::Frame &source, Json::Value &dest);

	void factory(Json::Value &source, data::robot::Frame &dest);
}
}

#endif //ROBOCUP_FRAME_H
