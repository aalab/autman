/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_ACTUATOR_H
#define ROBOCUP_ACTUATOR_H

#include <string>
#include <boost/optional/optional.hpp>
#include <json/value.h>
#include "basecommand.h"

namespace services
{
namespace data
{
namespace robot
{

enum class ActuatorMode
{
	ON,
	OFF
};

class Actuator
	: public BaseCommand
{
public:
	Actuator();

	Actuator(std::string name);

	Actuator(std::string name, float angle);

	void reset();

	std::string name;

	boost::optional<ActuatorMode> mode;

	boost::optional<float> angle;
	boost::optional<unsigned int> speed;

	boost::optional<uint8_t> gainP;
	boost::optional<uint8_t> gainI;
	boost::optional<uint8_t> gainD;
};
}
}

namespace json
{
void factory(data::robot::Actuator &actuator, Json::Value &dest);
void factory(Json::Value &source, data::robot::Actuator &dest);
}
}

#endif //ROBOCUP_ACTUATOR_H
