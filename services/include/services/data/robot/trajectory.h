/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 28, 2015
 **/

#ifndef SERVICES_DATA_ROBOT_TRAJECTORY_H
#define SERVICES_DATA_ROBOT_TRAJECTORY_H

#include <boost/optional/optional.hpp>
#include <string>
#include "basecommand.h"

namespace services
{
namespace data
{
namespace robot
{
	class Trajectory
		: public BaseCommand
	{
	private:
		std::string _name;
		boost::optional<unsigned long long int> _cycleTimeUSec;
		boost::optional<unsigned int> _cycles;
		boost::optional<unsigned long long int> _entryCycleTimeUSec;
		boost::optional<float> _motionSpeed;
		boost::optional<float> _motionEntrySpeed;
	public:
		Trajectory();
		Trajectory(std::string name, unsigned long long int cycleTimeUSec);

		std::string name();
		Trajectory name(std::string name);

		boost::optional<unsigned long long int> cycleTimeUSec();
		Trajectory cycleTimeUSec(unsigned long long int cycleTimeUSec);

		boost::optional<unsigned int> cycles();
		Trajectory cycles(unsigned int cycles);

		boost::optional<unsigned long long int> entryCycleTimeUSec();
		Trajectory entryCycleTimeUSec(unsigned long long int entryCycleTimeUSec);

		boost::optional<float> motionSpeed();
		Trajectory motionSpeed(float motionSpeed);

		boost::optional<float> motionEntrySpeed();
		Trajectory motionEntrySpeed(float motionEntrySpeed);
	};
}
}

namespace json
{
void factory(data::robot::Trajectory &source, Json::Value &dest);
void factory(Json::Value &source, data::robot::Trajectory &dest);
}
}


#endif // SERVICES_DATA_ROBOT_TRAJECTORY_H
