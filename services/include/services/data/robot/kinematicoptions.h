/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 28, 2015
 **/

#ifndef SERVICES_DATA_ROBOT_KINEMATICOPTIONS_HPP
#define SERVICES_DATA_ROBOT_KINEMATICOPTIONS_HPP

#include <boost/optional/optional.hpp>
#include <json/value.h>
#include "basecommand.h"

namespace services
{
namespace data
{
namespace robot
{
class KinematicOptions
	: public BaseCommand
{
private:
	boost::optional<float> _stepX;
	boost::optional<float> _stepY;
	boost::optional<float> _stepAngle;
	boost::optional<float> _maxHeightProp;
	boost::optional<float> _transitionProp;
	boost::optional<float> _stepHeight;
	boost::optional<float> _hipPitch;
	boost::optional<float> _anklePitch;
	boost::optional<float> _xOffset;
	boost::optional<float> _yOffset;
	boost::optional<float> _zOffset;
	boost::optional<float> _transferAnkleRoll;
	boost::optional<float> _transferHipRoll;
	boost::optional<float> _transferY;
public:
	boost::optional<float> stepX();
	KinematicOptions &stepX(float stepX);

	boost::optional<float> stepY();
	KinematicOptions &stepY(float stepY);

	boost::optional<float> stepAngle();
	KinematicOptions &stepAngle(float stepAngle);

	boost::optional<float> maxHeightProp();
	KinematicOptions &maxHeightProp(float maxHeightProp);

	boost::optional<float> transitionProp();
	KinematicOptions &transitionProp(float transitionProp);

	boost::optional<float> stepHeight();
	KinematicOptions &stepHeight(float stepHeight);

	boost::optional<float> hipPitch();
	KinematicOptions &hipPitch(float hipPitch);

	boost::optional<float> anklePitch();
	KinematicOptions &anklePitch(float anklePitch);

	boost::optional<float> xOffset();
	KinematicOptions &xOffset(float xOffset);

	boost::optional<float> yOffset();
	KinematicOptions &yOffset(float yOffset);

	boost::optional<float> zOffset();
	KinematicOptions &zOffset(float zOffset);

	boost::optional<float> transferAnkleRoll();
	KinematicOptions &transferAnkleRoll(float transferAnkleRoll);

	boost::optional<float> transferHipRoll();
	KinematicOptions &transferHipRoll(float transferHipRoll);

	boost::optional<float> transferY();
	KinematicOptions &transferY(float transferY);
};
}
}

namespace json
{
void factory(data::robot::KinematicOptions &options, Json::Value &dest);
void factory(Json::Value &source, data::robot::KinematicOptions &dest);
}
}

#endif //SERVICES_DATA_ROBOT_KINEMATICOPTIONS_HPP
