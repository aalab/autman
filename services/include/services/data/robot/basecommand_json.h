/**
 * @author J. Santos <jamillo@gmail.com>
 * @date November 02, 2015
 **/

#ifndef SERVICES_DATA_ROBOT_BASECOMMAND_JSON_H
#define SERVICES_DATA_ROBOT_BASECOMMAND_JSON_H


#include <json/value.h>
#include "basecommand.h"

namespace services
{
namespace json
{
void factory(data::robot::BaseCommand* source, Json::Value &dest);
void factory(Json::Value &source, data::robot::BaseCommand* dest);
}
}

#endif //SERVICES_DATA_ROBOT_BASECOMMAND_JSON_H
