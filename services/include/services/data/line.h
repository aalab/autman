/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef SERVICES_LINE_H
#define SERVICES_LINE_H

#include "point.h"

namespace services
{
namespace data
{
class Line2D
{
public:
	Line2D();
	Line2D(Point2D &a, Point2D &b);
	Line2D(double aX, double aY, double bX, double bY);

	Point2D a;
	Point2D b;

	double hypot();

	double inclination();

	double width();

	double height();

	void centerPoint(Point2D &point);

	Point2D centerPoint();
};
}
}

#endif //SERVICES_LINE_H
