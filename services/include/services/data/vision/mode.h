/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 06, 2015
 */

#ifndef SERVICES_MODE_H
#define SERVICES_MODE_H

#include <string>

namespace services
{
namespace data
{
namespace vision
{
class Mode
{
public:
	std::string mode;
};
}
}
}

#endif //SERVICES_MODE_H
