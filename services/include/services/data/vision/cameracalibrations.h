/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 15, 2015
 */

#ifndef ROBOCUP_CAMERACALIBRATIONS_H
#define ROBOCUP_CAMERACALIBRATIONS_H

#include <boost/optional.hpp>
#include <tsai2d/tsai2D.h>

#include "tsai.h"

namespace services
{
namespace data
{
namespace vision
{
class CameraCalibrations
{
public:
	tsai_camera_parameters cameraParameters;
	std::vector<TsaiWrapper> calibrations;

	boost::optional<TsaiWrapper&> calibrationByName(std::string name);
};
}
}
}


#endif //ROBOCUP_CAMERACALIBRATIONS_H
