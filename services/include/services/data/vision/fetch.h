/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_FETCH_H
#define ROBOCUP_FETCH_H

#include <chrono>
#include <vector>
#include <json/value.h>
#include "../line.h"
#include "visionobject.h"

namespace services
{
namespace data
{
namespace vision
{
class Fetch
{ };

class ExportDataBase
{
public:
	std::chrono::system_clock time;
};

class GoalAreaResponse
	: public ExportDataBase
{
public:
	std::vector<Line2D> lines;
	std::vector<VisionObject> balls;
};

class FetchResponse
{
public:
	std::vector<VisionObject> objects;

	virtual void classify(VisionObject const &object);
	virtual bool classifyJson(Json::Value &json);
};

}
}
}


#endif //ROBOCUP_FETCH_H
