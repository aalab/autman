/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_TSAIWRAPPER_H
#define ROBOCUP_TSAIWRAPPER_H

#include <string>
#include <tsai2d/tsai2D.h>
#include <services/data/line.h>

namespace services
{
namespace data
{
namespace vision
{
class TsaiWrapper
{
private:
	struct tsai_camera_parameters cameraParameters;
public:
	TsaiWrapper(struct tsai_camera_parameters cameraParameters);

	std::string name;
	tsai_calibration_constants data;

	void i2w(unsigned int x, unsigned y, double *iX, double *iY);
	void i2w(data::Line2D &source, data::Line2D &dest);
};
}
}
}

#endif //ROBOCUP_TSAIWRAPPER_H
