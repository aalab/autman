/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef ROBOCUP_VISIONOBJECT_H
#define ROBOCUP_VISIONOBJECT_H

#include "../boundbox.h"

#include <string>

namespace services
{
namespace data
{
namespace vision
{
class VisionObject
{
public:
	VisionObject();

	std::string type;

	BoundBox2D boundBox;
	Point2D center;

	unsigned int size;
};
}
}
}

#endif //ROBOCUP_VISIONOBJECT_H
