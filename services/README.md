
Services library
================

The service library was designed to perform all the serialization and unserialization need for all services provided
by our plataform: Robo Server (autman and darwin), Vision Module, etc.

Design
------

### base namespace

Provides the base implementation of the `services::base::UDPClient` for fecthing. Also, provides the
`services::base::UDPServer` implementation to boost up the implementation of the UDP servers to provide data.

Additionally, it has the `services::json::factory` (multi overloaded method) that implements all the
serialization/unserialization from `Json::Value` to `services::data::**` classes.


### data namespace

The data namespace keeps all the packets that can be exchanged categoryzed by modules in sub-namespaces.

#### FullMove command

Json format:

	{
		"command":"FullMove",			// Command name
		"params":
		{
			"vx": 0,					// X velocity (forward)
			"vy": 0,					// Y velocity (side)
			"vt": 0,					// Turning velocity
			"motion": 0,				// Id of the motion to be performed
			"pan": 30,					// Pan angle (horizontal)
			"tilt": 0					// Tilt angle (vertical)
		}
	}

One line json:

	{ "command":"FullMove", "params": { "vx": 0, "vy": 0, "vt": 0, "motion": 0, "pan": 0, "tilt": 0 }}

#### OmniWalk command

Json format:

	{
		"command":"OmniWalk",			// Command name
		"params":
		{Move
			"vx": 0,					// X velocity (forward)
			"vy": 0,					// Y velocity (side)
			"vt": 0					// Turning velocity
		}
	}

One line json:

	{ "command":"OmniWalk", "params": { "vx": 0, "vy": 0, "vt": 0 }}

#### Head command

Json format:

	{
		"command":"Head",			// Command name
		"params":
		{
			"pan": 0,					// Pan angle (horizontal)
			"tilt": 0					// Tilt angle (vertical)
		}
	}

One line json:

	{ "command":"Head", "params": { "pan": 0, "tilt": 0 }}
