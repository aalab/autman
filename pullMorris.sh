#TAKE THIS SCRIPT AND PLACE IT OUTSIDE OF AALAB DIRECTORY 
#Param $1 = account name of machine to copy from
#Param $2 = ip address of machine to copy from
#Param $3 = PUSH/PULL: PUSH - take local changes and update external, PULL - take external changes and sync with local

#KEEP THESE CONSTANT. This is the directory location you are copying from
srcMainLaptop="/home/votick/workspace/aalab/KyleSpace/" #directory on my laptop containing stuff
srcMainVM="/home/kyle/aalab/KyleSpace/" #directory on virtual machine containing stuff
srcDarwinVM="/home/darwin/KyleSpace"
srcClara="/home/robotisop2/"
srcJennifer="/home/darwin/"
srcAutman="/home/autman/aalab"


#CONFIGURE THIS using variables created above
srcDir=$srcAutman #change this to adjust where to copy from
targetDir="aalab" #directory to copy everything to

#echo "Attempting to copy from $srcDir from machine $1 to $targetDir\"
#rsync -aAxv $1@$2:$srcDir $targetDir copy from some other machine to your own pc
#rsync -aAxv $targetDir $1@$2:$srcDir reverse, copy from machine you are currently on to some other target

pushPull=$3; #are we pushing or pulling changes here?
echo $pushPull

#for autman, specifically
if [ "$pushPull" == "PULL" ]; then
echo "P1LLING CHANGES"
rsync -aAxv $1@$2:"$srcDir/" $targetDir \
    --exclude "visionmodule/bin/debug/www" \
    --exclude "visionmodule/bin/debug/CMakeFiles" \
    --exclude "visionmodule/bin/debug/cmake_install.cmake" \
    --exclude "visionmodule/bin/debug/CMakeCache.txt" \
    --exclude "visionmodule/bin/debug/Makefile" \
    --exclude "visionmodule/bin/debug/jsoncpp" \
    --exclude "visionmodule/bin/debug/tsai2d" \
    --exclude "visionmodule/bin/debug/libtsai2d.a" \
    --exclude "visionmodule/bin/debug/find_*" \
    --exclude "visionmodule/bin/debug/bulk_*" \
    --exclude "visionmodule/bin/debug/vision_module" \
    --exclude "visionmodule/bin/debug/ppms" \
    --exclude "visionmodule/bin/debug/services" \
    --exclude "visionmodule/bin/debug/www.sh" \
    --exclude "visionmodule/bin/debug/*_find_grid" \
    --exclude "visionmodule/bin/debug/src" \
    \
    --exclude "controlmodule/bin/debug/CMakeFiles" \
    --exclude "controlmodule/bin/debug/cmake_install.cmake" \
    --exclude "controlmodule/bin/debug/CMakeCache.txt" \
    --exclude "controlmodule/bin/debug/Makefile" \
    \
    --exclude "autman/bin/debug/CMakeFiles" \
    --exclude "autman/bin/debug/cmake_install.cmake" \
    --exclude "autman/bin/debug/CMakeCache.txt" \
    --exclude "autman/bin/debug/Makefile" \
    --exclude "autman/bin/debug/Include" \
    --exclude "autman/bin/debug/imumodule" \
    --exclude "autman/bin/debug/jsoncpp" \
    --exclude "autman/bin/debug/robocup_test" \
    --exclude "autman/bin/debug/robot_server" \
    --exclude "autman/bin/debug/*.a" \
    \
    --exclude "darwin/bin/debug/CMakeFiles" \
    --exclude "darwin/bin/debug/cmake_install.cmake" \
    --exclude "darwin/bin/debug/CMakeCache.txt" \
    --exclude "darwin/bin/debug/Makefile" \
    --exclude "darwin/bin/debug/services" \
    --exclude "darwin/bin/debug/libdarwin" \
    --exclude "darwin/bin/debug/include" \
    --exclude "darwin/bin/debug/darwin_server" \
    --exclude "darwin/bin/debug/.gitignore" \
    \
    --exclude "modules/teleop/bin/debug/.gitignore" \
    --exclude "modules/teleop/bin/debug/Makefile" \
    --exclude "modules/teleop/bin/debug/CMakeFiles" \
    --exclude "modules/teleop/bin/debug/cmake_install" \
    --exclude "modules/teleop/bin/debug/CMakeCache.txt" \
    --exclude "modules/teleop/bin/debug/cmake_install.cmake" \
    --exclude "modules/teleop/bin/debug/teleop" \
    --exclude "modules/teleop/bin/debug/brain" \
    --exclude "modules/teleop/bin/debug/services" \
    --exclude "modules/teleop/bin/debug/include" \
    \
    --exclude "modules/teleop_silverback/bin/debug/.gitignore" \
    --exclude "modules/teleop_silverback/bin/debug/Makefile" \
    --exclude "modules/teleop_silverback/bin/debug/CMakeFiles" \
    --exclude "modules/teleop_silverback/bin/debug/cmake_install" \
    --exclude "modules/teleop_silverback/bin/debug/CMakeCache.txt" \
    --exclude "modules/teleop_silverback/bin/debug/cmake_install.cmake" \
    --exclude "modules/teleop_silverback/bin/debug/teleop" \
    --exclude "modules/teleop_silverback/bin/debug/brain" \
    --exclude "modules/teleop_silverback/bin/debug/services" \
    --exclude "modules/teleop_silverback/bin/debug/include" \
    \
    --exclude "modules/puppet/bin/debug/.gitignore" \
    --exclude "modules/puppet/bin/debug/Makefile" \
    --exclude "modules/puppet/bin/debug/CMakeFiles" \
    --exclude "modules/puppet/bin/debug/cmake_install" \
    --exclude "modules/puppet/bin/debug/CMakeCache.txt" \
    --exclude "modules/puppet/bin/debug/cmake_install.cmake" \
    --exclude "modules/puppet/bin/debug/puppet" \
    --exclude "modules/puppet/bin/debug/brain" \
    --exclude "modules/puppet/bin/debug/services" \
    --exclude "modules/puppet/bin/debug/include" \
    \
    --exclude "modules/bowling/bin/debug/.gitignore" \
    --exclude "modules/bowling/bin/debug/Makefile" \
    --exclude "modules/bowling/bin/debug/CMakeFiles" \
    --exclude "modules/bowling/bin/debug/cmake_install" \
    --exclude "modules/bowling/bin/debug/CMakeCache.txt" \
    --exclude "modules/bowling/bin/debug/cmake_install.cmake" \
    --exclude "modules/bowling/bin/debug/bowling" \
    --exclude "modules/bowling/bin/debug/brain" \
    --exclude "modules/bowling/bin/debug/services" \
    --exclude "modules/bowling/bin/debug/include" \
    \
    --exclude "modules/weight_lifting/bin/debug/.gitignore" \
    --exclude "modules/weight_lifting/bin/debug/Makefile" \
    --exclude "modules/weight_lifting/bin/debug/CMakeFiles" \
    --exclude "modules/weight_lifting/bin/debug/cmake_install" \
    --exclude "modules/weight_lifting/bin/debug/CMakeCache.txt" \
    --exclude "modules/weight_lifting/bin/debug/cmake_install.cmake" \
    --exclude "modules/weight_lifting/bin/debug/weight_lifting" \
    --exclude "modules/weight_lifting/bin/debug/brain" \
    --exclude "modules/weight_lifting/bin/debug/services" \
    --exclude "modules/weight_lifting/bin/debug/include" \
    \
    --exclude "robocup/bin/debug/include/config.h" \
    --exclude "robocup/bin/debug/cmake_install.cmake" \
    --exclude "robocup/bin/debug/CMakeFiles" \
    --exclude "robocup/bin/debug/Makefile" \
    --exclude "robocup/bin/debug/CMakeCache.txt" \
    --exclude "robocup/bin/debug/robocup" \
    --exclude "hurocup/bin/debug/include" \
    --exclude "hurocup/bin/debug/cmake_install.cmake" \
    --exclude "hurocup/bin/debug/services" \
    --exclude "hurocup/bin/debug/brain" \
    --exclude "hurocup/bin/debug/tsai2d" \
    --exclude "hurocup/bin/debug/jsoncpp" \
    --exclude "hurocup/bin/debug/CMakeFiles" \
    --exclude "hurocup/bin/debug/Makefile" \
    --exclude "hurocup/bin/debug/CMakeCache.txt" \
    --exclude "hurocup/bin/debug/hurocup"
elif [ "$pushPull" == "PUSH" ]; then
echo "PUSHING CHANGES"
rsync -aAxv "$targetDir/" $1@$2:$srcDir \
    --exclude "visionmodule/bin/debug/www" \
    --exclude "visionmodule/bin/debug/CMakeFiles" \
    --exclude "visionmodule/bin/debug/cmake_install.cmake" \
    --exclude "visionmodule/bin/debug/CMakeCache.txt" \
    --exclude "visionmodule/bin/debug/Makefile" \
    --exclude "visionmodule/bin/debug/jsoncpp" \
    --exclude "visionmodule/bin/debug/tsai2d" \
    --exclude "visionmodule/bin/debug/libtsai2d.a" \
    --exclude "visionmodule/bin/debug/find_*" \
    --exclude "visionmodule/bin/debug/bulk_*" \
    --exclude "visionmodule/bin/debug/vision_module" \
    --exclude "visionmodule/bin/debug/ppms" \
    --exclude "visionmodule/bin/debug/services" \
    --exclude "visionmodule/bin/debug/www.sh" \
    --exclude "visionmodule/bin/debug/*_find_grid" \
    --exclude "visionmodule/bin/debug/src" \
    \
    --exclude "controlmodule/bin/debug/CMakeFiles" \
    --exclude "controlmodule/bin/debug/cmake_install.cmake" \
    --exclude "controlmodule/bin/debug/CMakeCache.txt" \
    --exclude "controlmodule/bin/debug/Makefile" \
    \
    --exclude "autman/bin/debug/CMakeFiles" \
    --exclude "autman/bin/debug/cmake_install.cmake" \
    --exclude "autman/bin/debug/CMakeCache.txt" \
    --exclude "autman/bin/debug/Makefile" \
    --exclude "autman/bin/debug/Include" \
    --exclude "autman/bin/debug/imumodule" \
    --exclude "autman/bin/debug/jsoncpp" \
    --exclude "autman/bin/debug/robocup_test" \
    --exclude "autman/bin/debug/robot_server" \
    --exclude "autman/bin/debug/*.a" \
    \
    --exclude "darwin/bin/debug/CMakeFiles" \
    --exclude "darwin/bin/debug/cmake_install.cmake" \
    --exclude "darwin/bin/debug/CMakeCache.txt" \
    --exclude "darwin/bin/debug/Makefile" \
    --exclude "darwin/bin/debug/services" \
    --exclude "darwin/bin/debug/libdarwin" \
    --exclude "darwin/bin/debug/include" \
    --exclude "darwin/bin/debug/darwin_server" \
    --exclude "darwin/bin/debug/.gitignore" \
    \
    --exclude "modules/teleop/bin/debug/.gitignore" \
    --exclude "modules/teleop/bin/debug/Makefile" \
    --exclude "modules/teleop/bin/debug/CMakeFiles" \
    --exclude "modules/teleop/bin/debug/cmake_install" \
    --exclude "modules/teleop/bin/debug/CMakeCache.txt" \
    --exclude "modules/teleop/bin/debug/cmake_install.cmake" \
    --exclude "modules/teleop/bin/debug/teleop" \
    --exclude "modules/teleop/bin/debug/brain" \
    --exclude "modules/teleop/bin/debug/services" \
    --exclude "modules/teleop/bin/debug/include" \
    \
    --exclude "modules/teleop_silverback/bin/debug/.gitignore" \
    --exclude "modules/teleop_silverback/bin/debug/Makefile" \
    --exclude "modules/teleop_silverback/bin/debug/CMakeFiles" \
    --exclude "modules/teleop_silverback/bin/debug/cmake_install" \
    --exclude "modules/teleop_silverback/bin/debug/CMakeCache.txt" \
    --exclude "modules/teleop_silverback/bin/debug/cmake_install.cmake" \
    --exclude "modules/teleop_silverback/bin/debug/teleop" \
    --exclude "modules/teleop_silverback/bin/debug/brain" \
    --exclude "modules/teleop_silverback/bin/debug/services" \
    --exclude "modules/teleop_silverback/bin/debug/include" \
    \
    --exclude "modules/puppet/bin/debug/.gitignore" \
    --exclude "modules/puppet/bin/debug/Makefile" \
    --exclude "modules/puppet/bin/debug/CMakeFiles" \
    --exclude "modules/puppet/bin/debug/cmake_install" \
    --exclude "modules/puppet/bin/debug/CMakeCache.txt" \
    --exclude "modules/puppet/bin/debug/cmake_install.cmake" \
    --exclude "modules/puppet/bin/debug/puppet" \
    --exclude "modules/puppet/bin/debug/brain" \
    --exclude "modules/puppet/bin/debug/services" \
    --exclude "modules/puppet/bin/debug/include" \
    \
    --exclude "modules/bowling/bin/debug/.gitignore" \
    --exclude "modules/bowling/bin/debug/Makefile" \
    --exclude "modules/bowling/bin/debug/CMakeFiles" \
    --exclude "modules/bowling/bin/debug/cmake_install" \
    --exclude "modules/bowling/bin/debug/CMakeCache.txt" \
    --exclude "modules/bowling/bin/debug/cmake_install.cmake" \
    --exclude "modules/bowling/bin/debug/bowling" \
    --exclude "modules/bowling/bin/debug/brain" \
    --exclude "modules/bowling/bin/debug/services" \
    --exclude "modules/bowling/bin/debug/include" \
    \
    --exclude "modules/weight_lifting/bin/debug/.gitignore" \
    --exclude "modules/weight_lifting/bin/debug/Makefile" \
    --exclude "modules/weight_lifting/bin/debug/CMakeFiles" \
    --exclude "modules/weight_lifting/bin/debug/cmake_install" \
    --exclude "modules/weight_lifting/bin/debug/CMakeCache.txt" \
    --exclude "modules/weight_lifting/bin/debug/cmake_install.cmake" \
    --exclude "modules/weight_lifting/bin/debug/weight_lifting" \
    --exclude "modules/weight_lifting/bin/debug/brain" \
    --exclude "modules/weight_lifting/bin/debug/services" \
    --exclude "modules/weight_lifting/bin/debug/include" \
    \
    --exclude "robocup/bin/debug/include/config.h" \
    --exclude "robocup/bin/debug/cmake_install.cmake" \
    --exclude "robocup/bin/debug/CMakeFiles" \
    --exclude "robocup/bin/debug/Makefile" \
    --exclude "robocup/bin/debug/CMakeCache.txt" \
    --exclude "robocup/bin/debug/robocup" \
    --exclude "hurocup/bin/debug/include" \
    --exclude "hurocup/bin/debug/cmake_install.cmake" \
    --exclude "hurocup/bin/debug/services" \
    --exclude "hurocup/bin/debug/brain" \
    --exclude "hurocup/bin/debug/tsai2d" \
    --exclude "hurocup/bin/debug/jsoncpp" \
    --exclude "hurocup/bin/debug/CMakeFiles" \
    --exclude "hurocup/bin/debug/Makefile" \
    --exclude "hurocup/bin/debug/CMakeCache.txt" \
    --exclude "hurocup/bin/debug/hurocup" 
fi
