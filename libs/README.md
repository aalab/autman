Library dependencies
====================

This documentation intends to keep all the instructions for instaling all the libraries needed for each
project contained on this repository.

boost
-----

This library is widely used across several projects.

**Website**: [boost.org](http://boost.org/)

**Current version**: 1.59.0

```bash
cd ~/downloads
wget http://skylineservers.dl.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.tar.bz2
tar -xvf boost_1_59_0.tar.bz2
cd boost_1_59_0
mkdir -p $HOME/libs/libboost_1_59_0/
./bootstrap.sh
./b2 --prefix=$HOME/libs/libboost_1_59_0/ install
```

jsoncpp
-----

This library is used for serialize and deserialize JSON data for couple projects.

**Repository**: https://github.com/open-source-parsers/jsoncpp/

**Current version**: 0.10.5

```bash
cd ~/Downloads
wget https://github.com/open-source-parsers/jsoncpp/archive/0.10.5.tar.gz
mkdir -p ~/libs/jsoncpp
tar -xvzf 0.10.5.tar.gz
cd jsoncpp-0.10.5
mkdir -p bin/release
cd bin/release
cmake -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=OFF -DCMAKE_INSTALL_PREFIX=~/libs/jsoncpp ../..
make all install
```

tsai2d
-----

This library is used to calibrate the camera distortions and to calculate the image to real world coordinates,
also the real world to image coordinates.

**Respository**: the source code is [./libs/tsai2d](./libs/tsai2d)

**Current version**: N/A

```bash
cd ~/downloads
git clone https://gitlab.com/aalab/autman.git
cd autman/libs/tsai2d
mkdir -p bin/release
mkdir -p ~/libs/tsai2d/
cd bin/release
cmake ../.. -DCMAKE_INSTALL_DIR:PATH=~/libs/tsai2d/
make all install
```