Tsai2D
======

Installation
------------

	mkdir -p bin/release
	mkdir -p ~/libs/tsai2d/
	cd bin/release
	cmake ../.. -DCMAKE_INSTALL_DIR:PATH=~/libs/tsai2d/
	make all install
