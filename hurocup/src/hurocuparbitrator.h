/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ROBOCUPBRAIN_H
#define ROBOCUP_ROBOCUPBRAIN_H

#include <brain/arbitrator.h>
#include <services/data/gyro/data.h>
#include <services/gyro.h>
#include "behaviors/sprint.h"
#include "arashrobotstate.h"

namespace hurocup
{

namespace behaviors
{
class Sprint;
class RobotStateReader;
}

namespace rdata = services::data::robot;
namespace gdata = services::data::gyro;

class Arbitrator
	: public brain::Arbitrator
{
protected:
	services::Robot &robot;
	services::Gyro &gyro;

public:
	virtual void terminate() override;

	virtual void process();
public:
	Arbitrator(services::Vision &vision, services::Robot &robot);
	virtual ~Arbitrator();

	services::data::vision::TsaiWrapper* currentCameraCalibration;

	services::Vision &vision;

	ArashRobotState state;

	void headMoveByStep(float pan, float tilt);
	void headMoveTo(float pan, float tilt);

	bool move(rdata::Move &move);
	bool move(rdata::Head &head);
	bool move(rdata::FullMove &move);

	bool gyroData(services::data::gyro::DataResponse &data);
};
}


#endif //ROBOCUP_ROBOCUPBRAIN_H
