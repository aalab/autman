/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include "arashrobotstate.h"
#include "compilerdefinitions.h"

#include <iostream>

// #define VERBOSEARASHROBOTSTATE(x) VERBOSE(x)
#define VERBOSEARASHROBOTSTATE(x)

hurocup::ArashRobotState::ArashRobotState(services::Robot &robotService)
	: brain::RobotState(), robotService(robotService), neckLateral(), neckTransversal(), camera(320, 240)
{ }

void hurocup::ArashRobotState::fetch(services::data::robot::ReadStateResponse &response)
{
	brain::RobotState::fetch(response);

	bool neckLateralExpired = this->neckLateral.isExpired();
	bool neckTransversalExpired = this->neckTransversal.isExpired();

	for (
		boost::unordered_map<std::string, ::services::data::robot::ServoState *>::iterator it = response.states.begin();
		it != response.states.end(); ++it
	)
	{
		if ((neckLateralExpired) && (it->first == "NeckLateral"))
		{
			VERBOSE("Updating actuator: " << it->first);
			this->neckLateral.update((*it->second).angle);
		}
		else if ((neckTransversalExpired) && (it->first == "NeckTransversal"))
		{
			VERBOSE("Updating actuator: " << it->first);
			this->neckTransversal.update((*it->second).angle);
		}
	}
}

bool hurocup::ArashRobotState::update()
{
	services::data::robot::ReadState request;
	services::data::robot::ReadStateResponse response;

	VERBOSEARASHROBOTSTATE("Reading data...");

	if (this->robotService.readState(request, response))
	{
		VERBOSEARASHROBOTSTATE("OK");
		this->fetch(response);
		return true;
	}
	else
	{
		ERROR("Error reading state.");
		return false;
	}
}
