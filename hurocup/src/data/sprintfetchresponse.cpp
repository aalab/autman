/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 27, 2015
 */

#include "sprintfetchresponse.h"

void hurocup::data::SprintFetchResponse::classify(services::data::vision::VisionObject const &object)
{
	vdata::FetchResponse::classify(object);
	if (object.type == "sprint-left")
		this->lefts.push_back(object);
	else if (object.type == "sprint-right")
		this->rights.push_back(object);
}
