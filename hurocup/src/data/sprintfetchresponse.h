/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 27, 2015
 */

#ifndef HUROCUP_SPRINTFETCHRESPONSE_H
#define HUROCUP_SPRINTFETCHRESPONSE_H

#include <services/data/vision/fetch.h>

namespace hurocup
{
namespace data
{

namespace vdata = services::data::vision;

class SprintFetchResponse
	: public services::data::vision::FetchResponse
{
public:
	std::vector<vdata::VisionObject> lefts;
	std::vector<vdata::VisionObject> rights;

	virtual void classify(vdata::VisionObject const &object) override;
};
}
}


#endif //HUROCUP_SPRINTFETCHRESPONSE_H
