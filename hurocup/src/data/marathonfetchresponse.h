/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 09, 2015
 */

#ifndef HUROCUP_MARATHONFETCHRESPONSE_H
#define HUROCUP_MARATHONFETCHRESPONSE_H

#include <services/data/vision/fetch.h>

#ifndef override
#define override
#endif

namespace hurocup
{
namespace data
{

namespace vdata = services::data::vision;

class MarathonFetchResponse
	: public services::data::vision::FetchResponse
{
public:
	services::data::Line2D line;
	std::string marker;
	services::data::Point2D markerCenter;

	virtual bool classifyJson(Json::Value &json) override;
};
}
}


#endif //HUROCUP_MARATHONFETCHRESPONSE_H
