/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 05, 2015
 */

#include "gyromonitor.h"

hurocup::behaviors::GyroMonitor::GyroMonitor(hurocup::Arbitrator &arbitrator)
	: Behavior(), arbitrator(arbitrator)
{ }

void hurocup::behaviors::GyroMonitor::process()
{
	this->arbitrator.gyroData(data);
}
