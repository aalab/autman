/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 05, 2015
 */

#ifndef HUROCUP_GYROMONITOR_H
#define HUROCUP_GYROMONITOR_H

#include <brain/base/behavior.h>
#include <services/data/gyro/data.h>
#include "../hurocuparbitrator.h"

#ifndef override
#define override
#endif


namespace hurocup
{
namespace behaviors
{
namespace gdata = services::data::gyro;

class GyroMonitor
	: public brain::Behavior
{
public:
	static GyroMonitor *instance;

protected:
	hurocup::Arbitrator &arbitrator;

public:
	GyroMonitor(hurocup::Arbitrator &arbitrator);

	gdata::DataResponse data;
protected:
	virtual void process() override;
};
}
}


#endif //HUROCUP_GYROMONITOR_H
