/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ALIGNMENT_H
#define ROBOCUP_ALIGNMENT_H

#include <brain/base/behavior.h>
#include <services/vision.h>
#include <services/robot.h>
#include "../hurocuparbitrator.h"

#ifndef override
#define override
#endif

namespace hurocup
{

class Arbitrator;

namespace behaviors
{
class Sprint
	: public brain::Behavior
{
enum SprintDirection
{
	FORWARD,
	BACKWARD
};

enum SprintState
{
	NONE,
	NOTFOUND,
	WALKING,
	ALIGNING,
	SCANNING
};

public:
	static Sprint *instance;
protected:
	std::chrono::system_clock::time_point notFoundStartTime;

	Arbitrator &arbitrator;

	SprintDirection direction;
	SprintState state;

	services::data::Point2D scanningAngle;

	std::chrono::system_clock::time_point scanningStart;

	bool shapeFits(services::data::vision::VisionObject &object);

	virtual void process() override;
public:
	Sprint(hurocup::Arbitrator &arbitrator);
	virtual void disable() override;

};
}
}


#endif //ROBOCUP_ALIGNMENT_H
