/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 28, 2015
 */

#include "headtracker.h"
#include "../hurocuparbitrator.h"
#include "../compilerdefinitions.h"
#include "../vision.h"

#include <thread>
#include <iostream>

hurocup::behaviors::HeadTracker *hurocup::behaviors::HeadTracker::instance = nullptr;

hurocup::behaviors::HeadTracker::HeadTracker(hurocup::Arbitrator &arbitrator)
	: arbitrator(arbitrator), pan(0), tilt(0), minTilt(-1.0), maxTilt(1.3), x(-1), y(-1), tolerance(20)
{
	instance = this;
}

void hurocup::behaviors::HeadTracker::process()
{
	if (this->dumpUiData)
	{
		VERBOSE("HeadTracker");
	}
	if (this->x >= 0)
	{
		if (this->dumpUiData)
		{
			VERBOSE(this->x << ":" << this->y);
		}
		float
			dX = ((Vision::camera.x / 2.0) - this->x),
			dY = (this->y - (Vision::camera.y / 2.0));

		if (std::sqrt(dX * dX + dY * dY) > this->tolerance)
		{
			float
				nPan = dX * ((Vision::cameraAngles.x / Vision::camera.x) / 2.0),
				nTilt = dY * ((Vision::cameraAngles.y / Vision::camera.y) / 2.0);

			nPan /= 3 + std::abs(6 * (dX / Vision::camera.x / 2.0));
			nTilt /= 3 + std::abs(6 * (dY / Vision::camera.y / 2.0));

			// this->pan = this->arbitrator.state.neckTransversal.angle + nPan;
			this->tilt = this->arbitrator.state.neckLateral.angle + nTilt;

			if (this->tilt < this->minTilt)
				this->tilt = this->minTilt;
			else if (this->tilt > this->maxTilt)
				this->tilt = this->maxTilt;

			rdata::Head headCommand(this->pan, this->tilt);
			if (this->arbitrator.move(headCommand))
			{
				if (this->dumpUiData)
				{
					VERBOSE("Command sent!");
				}
			}
			else
			{
				if (this->dumpUiData)
				{
					ERROR("Error sending move command.");
				}
			}

			if (this->dumpUiData)
			{
				VERBOSE("Headtrack information:");
				VERBOSEB("Position: " << this->x << ":" << this->y);
				VERBOSEB("    Diff: " << dX << ":" << dY);
				VERBOSEB("     Pan: " << this->pan);
				VERBOSEB("    Tilt: " << this->tilt);
			}

		}

		if (this->dumpUiData)
		{
			VERBOSEB("    Tilt: " << this->arbitrator.state.neckLateral.angle);
		}
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

bool hurocup::behaviors::HeadTracker::isUnique()
{
	return true;
}
