/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 09, 2015
 */

#ifndef HUROCUP_MARATHON_H
#define HUROCUP_MARATHON_H


#include <brain/base/behavior.h>
#include "../data/marathonfetchresponse.h"

#ifndef override
#define override
#endif

#define DEFAULT_HEAD_TILT 1.0

namespace hurocup
{
class Arbitrator;
namespace behaviors
{
class Marathon
	: public brain::Behavior
{
protected:
	Arbitrator &arbitrator;

public:
	Marathon(hurocup::Arbitrator &arbitrator);

	virtual bool isUnique() override;

protected:
	virtual void process() override;

	void followLine(hurocup::data::MarathonFetchResponse &fetchResponse);
};
}
}


#endif //HUROCUP_MARATHON_H
