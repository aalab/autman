/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 06, 2015
 */

#ifndef HUROCUP_OBSTACLERUN_H
#define HUROCUP_OBSTACLERUN_H

#include <brain/base/behavior.h>
#include "../data/globe.h"

#ifndef override
#define override
#endif

namespace hurocup
{
class Arbitrator;
namespace behaviors
{
class ObstacleRun
	: public brain::Behavior
{
private:
	Arbitrator &arbitrator;

	data::Globe globe;
protected:
	bool scanGlobe(float angleTilt);

	float coordToAngle(float coord, float coordLength, float angleLength);

	virtual void process() override;
public:
	ObstacleRun(Arbitrator &arbitrator);
};
}
}


#endif //HUROCUP_OBSTACLERUN_H
