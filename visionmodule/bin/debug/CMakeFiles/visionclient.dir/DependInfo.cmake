# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/autman/old_ws/autman/visionmodule/src/visionclientmain.cpp" "/home/autman/old_ws/autman/visionmodule/bin/debug/CMakeFiles/visionclient.dir/src/visionclientmain.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DEBUG__PRINT_IMAGE__GOITS"
  "ENABLE_V4L2"
  "VERBOSE__GOITS"
  "__PROJECT_VERSION__=\"0.2\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/autman/libs/libboost_1_61_0/include"
  "/home/autman/libs/tsai2d/include"
  "/home/autman/libs/jsoncpp/include"
  "../../../services/include"
  "../../../libvideo"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
