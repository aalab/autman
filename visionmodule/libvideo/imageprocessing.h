// $Id$
// Some basic image processing functions
// Jacky Baltes <jacky@cs.umanitoba.ca> Mon Nov 10 22:20:41 CST 2003

#ifndef _IMAGEPROCESSING_H_
#define _IMAGEPROCESSING_H_

#include <vector>

#include "pixel.h"
#include "point.h"
#include "colourdefinition.h"
#include "rect.h"
#include "visionobject.h"
#include "../src/line.h"

class FloodFillState;

class FrameBuffer;

class IntegralImage;

class ImageProcessing
{
public:

	enum ErrorCode
	{
		NO_ERROR = 0,
		OUT_OF_BOUNDS,
		INVALID_SEED_POINT,
		STACK_OVERFLOW
	};

	static bool isInBounds(FrameBuffer *frame, double x, double y);

	static double overlapArea(
		int x1, int y1, int width1, int height1,
		int x2, int y2, int width2, int height2
	);

	/**
	 * Returns the overlap factor based on the bigger object.
	 */
	static double overlapMax(
		int x1, int y1, int width1, int height1,
		int x2, int y2, int width2, int height2
	);

	/**
	 * Returns the overlap factor based on the smaller object.
	 */
	static double overlapMin(
		int x1, int y1, int width1, int height1,
		int x2, int y2, int width2, int height2
	);

	static void segmentScanLines(FrameBuffer *frame, FrameBuffer *outFrame, unsigned int threshold,
		unsigned int minLength, unsigned int maxLength, unsigned int minSize, RawPixel const &mark,
		unsigned int subsample, ColourDefinition const *target, std::vector<VisionObject> &results,
		bool drawOriginalFrame, bool drawOutFrame);

	static void segmentForBall(FrameBuffer *frame, FrameBuffer *outFrame, unsigned int threshold,
		unsigned int minLength, unsigned int maxLength, unsigned int minSize, unsigned int maxSize,RawPixel const &mark, unsigned int subSample,
	 	ColourDefinition const *target, std::vector<VisionObject> &results, bool drawOriginalFrame, bool drawOutFrame);

	/*
	static void segmentScanLines2(FrameBuffer *frame, FrameBuffer *outFrame, unsigned int threshold,
		unsigned int minLength, unsigned int maxLength, unsigned int minSize, RawPixel const &mark,
		unsigned int subsample, ColourDefinition const *target, bool drawOriginalFrame = true);
	*/

	static void SegmentColours(
		FrameBuffer *frame, FrameBuffer *outFrame, unsigned int threshold, unsigned int minLength, unsigned int minSize,
		unsigned int subSample, ColourDefinition const &target, RawPixel const &mark, std::vector<VisionObject> &results,
		bool drawOriginalFrame, bool drawOutFrame
	);

	static void medianFilter(FrameBuffer const *frame, FrameBuffer *outFrame, unsigned int subSample = 1);

	static enum ErrorCode doFloodFill(FrameBuffer *frame,
		FrameBuffer *outFrame,
		Point p,
		RawPixel seed,
		unsigned int threshold,
		ColourDefinition const *target,
		unsigned int subSample,
		FloodFillState *state);

	static enum ErrorCode doFloodFill(FrameBuffer *frame,
		FrameBuffer *outFrame,
		unsigned int pX,
		unsigned int pY,
		RawPixel seed,
		unsigned int threshold,
		ColourDefinition const *target,
		unsigned int subSample,
		FloodFillState *state);

	static void drawBresenhamLine(FrameBuffer *frame, Point const start, Point const end, RawPixel const colour);

	static void drawBresenhamLine(FrameBuffer *frame, Line2D const &line, RawPixel const colour);

	static void drawBresenhamLine(FrameBuffer *frame, int x1, int y1, int x2, int y2, RawPixel const colour);

	static void drawRectangle(FrameBuffer *frame, Rect const &rect, RawPixel const &colour);

	static void drawRectangle(FrameBuffer *frame, unsigned int x, unsigned int y, unsigned int width, unsigned int height, RawPixel const &colour);

	static void calcIntegralImage(FrameBuffer const *frame, IntegralImage *image);

	static void swapColours(FrameBuffer const *inFrame, FrameBuffer *outFrame, Rect bbox, unsigned int subSample, ColourDefinition const &find, RawPixel const &replace);

	static void calcQuadTreeDecomposition(FrameBuffer const *inFrame, FrameBuffer *outFrame, IntegralImage const *integralImage);

	static void convertBuffer(FrameBuffer const *frame, FrameBuffer *outFrame, unsigned int subSample = 1);

	static void fillBlackBuffer(FrameBuffer const *frame, FrameBuffer *outFrame, unsigned int subSample = 1);

	static void drawCross(FrameBuffer *frameBuffer, double x, double y, RawPixel &colour, unsigned int size);

	static void drawCross(FrameBuffer *frameBuffer, Point2D &point, RawPixel &colour, unsigned int size);
};


#endif
