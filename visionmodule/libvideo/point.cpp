#include "point.h"

Point2D::Point2D(double x, double y)
	: x(x), y(y)
{ }

Point2D::Point2D()
	: x(0), y(0)
{ }

double Point2D::hypot()
{
	return ::hypot(this->x, this->y);
}

double Point2D::distance(double px, double py)
{
	return ::hypot((px - this->x), (py - this->y));
}

double Point2D::distance(Point2D &p)
{
	return ::hypot(p.x - this->x, p.y - this->y);
}

unsigned int Point::y(void) const
{
	return _y;
}

unsigned int Point::x(void) const
{
	return _x;
}

void Point::setX(unsigned int x)
{
	_x = x;
}

void Point::setY(unsigned int y)
{
	_y = y;
}

void Point::setXY(unsigned int x, unsigned int y)
{
	_x = x;
	_y = y;
}

double Point::hypot(void) const
{
	return ::hypot(x(), y());
}

Point::Point(unsigned int x, unsigned int y)
	: _x(x), _y(y)
{ }

std::ostream &operator<<( std::ostream & os, Point const & point )
{
	os << "{\"x\": " << point.x() << ",\"y\": " << point.y() << "}";
	return os;
}

std::ostream &operator<<( std::ostream & os, Point2D const & point )
{
	os << "{\"x\": " << point.x << ",\"y\": " << point.y << "}";
	return os;
}

Json::Value &operator<<(Json::Value &json, Point const &point)
{
	json["x"] = point.x();
	json["y"] = point.y();
	return json;
}
