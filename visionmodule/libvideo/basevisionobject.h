/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef VISIONMODULE_BASEVISIONOBJECT_H
#define VISIONMODULE_BASEVISIONOBJECT_H

#include <string>

#include "rect.h"

class BaseVisionObject2D
{
public:
	BaseVisionObject2D();
	BaseVisionObject2D(std::string type, const Rect &boundBox);

	std::string type;

	Rect boundBox;

	virtual std::string &serializeClassName() = 0;

	virtual std::ostream &writeJsonProperties(std::ostream &os);
};

std::ostream &operator<<(std::ostream &os, BaseVisionObject2D &vo);
std::ostream &operator<<(std::ostream &os, BaseVisionObject2D *vo);

Json::Value &operator<<(Json::Value &json, BaseVisionObject2D &vo);

#endif //VISIONMODULE_BASEVISIONOBJECT_H
