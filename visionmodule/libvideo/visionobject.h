#ifndef __VISION_OBJECT_H__
#define __VISION_OBJECT_H__

#include <string>
#include <ostream>

#include "basevisionobject.h"
#include "pixel.h"
#include "rect.h"

#include <json/value.h>

#ifndef override
#define override
#endif

class VisionObject
	: public BaseVisionObject2D
{
private:
	static std::string visinoObjectClassName;
public:
	VisionObject();
	VisionObject(
		std::string _type, unsigned int _size, unsigned int centerX, unsigned int centerY, RawPixel _average,
		RawPixel _seedColour, Rect &_boundBox
	);

	virtual std::string &serializeClassName();

	Point2D center;

	unsigned int size;

	RawPixel average;
	RawPixel seedColour;


	virtual std::ostream &writeJsonProperties(std::ostream &os) override;
};

Json::Value & operator<<(Json::Value &json, VisionObject const &vo);

#endif /* __VISION_OBJECT_H__ */
