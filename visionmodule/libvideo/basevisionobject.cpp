/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <json/value.h>
#include "basevisionobject.h"

BaseVisionObject2D::BaseVisionObject2D()
{ }

BaseVisionObject2D::BaseVisionObject2D(std::string type, const Rect &boundBox)
	: type(type), boundBox(boundBox)
{ }

std::ostream
&operator<<(std::ostream &os, BaseVisionObject2D &vo)
{
	os << "{\"class\":\"" << vo.serializeClassName() << "\", \"type\":\"" << vo.type << "\",\"boundBox\": "
		<< vo.boundBox;
	vo.writeJsonProperties(os);
	os << "}";
	return os;
}

std::ostream
&operator<<(std::ostream &os, BaseVisionObject2D *vo)
{
	os << *vo;
	return os;
}

Json::Value
&operator<<(Json::Value &json, BaseVisionObject2D &vo)
{
	json["type"] = vo.type;
	json["boundBox"] << vo.boundBox;
	return json;
}

std::ostream
&BaseVisionObject2D::writeJsonProperties(std::ostream &os)
{
	return os;
}
