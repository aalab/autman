#include <string>

#include "visionobject.h"
#include "pixel.h"
#include "rect.h"

std::string VisionObject::visinoObjectClassName("VisionObject");

VisionObject::VisionObject()
	: BaseVisionObject2D()
{ }

VisionObject::VisionObject(
	std::string _type, unsigned int _size, unsigned int centerX, unsigned int centerY, RawPixel _average,
	RawPixel _seedColour, Rect &_boundBox
)
	: BaseVisionObject2D(_type, _boundBox), center(centerX, centerY), size(_size), average(_average),
		seedColour(_seedColour)
{ }

std::string &
VisionObject::serializeClassName()
{
	return visinoObjectClassName;
}

Json::Value &operator<<(Json::Value &json, VisionObject const &vo)
{
	json["boundBox"] << vo.boundBox;
	json["size"] = vo.size;
	json["center"] << vo.center;
	json["type"] = vo.type;
	return json;
}

std::ostream &VisionObject::writeJsonProperties(std::ostream &os)
{
	BaseVisionObject2D::writeJsonProperties(os);
	os << ",\"size\":" << this->size << ",\"center\":" << this->center;
	return os;
}
