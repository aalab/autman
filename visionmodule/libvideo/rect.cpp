#include "rect.h"


Rect::Rect()
	: _topLeft(0, 0), _bottomRight(0, 0)
{ }

Rect::Rect(Point const &tl, Point const &br)
{
	topLeft() = tl;
	bottomRight() = br;
}

Rect::Rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
	: _topLeft(x1, y1), _bottomRight(x2, y2)
{ }

std::ostream &operator<<(std::ostream &os, Rect const &rect)
{
	os << "{\"topLeft\": " << rect.topLeft() << ",\"bottomRight\": " << rect.bottomRight() << "}";
	return os;
}

Json::Value &operator<<(Json::Value &json, Rect const &rect)
{
	json["a"] << rect.topLeft();
	json["b"] << rect.bottomRight();
	return json;
}

bool Rect::inBound(Point2D &p)
{
	return this->inBound(p.x, p.y);
}

bool Rect::inBound(int x, int y)
{
	return
		(x >= this->_topLeft.x()) && (x < this->_bottomRight.x())
		&& (y >= this->_topLeft.y()) && (y < this->_bottomRight.y());
}

unsigned int Rect::bbCenterY()
{
	return topLeft().y() + height()/2;
}

unsigned int Rect::bbCenterX()
{
	return topLeft().x() + width()/2;
}

unsigned int Rect::ratio(void) const
{
	if(width() != 0){
		return height()/width();
	}
	else{
		return 0;
	}
}

unsigned int Rect::size(void) const
{
	return width() * height();
}

unsigned int Rect::height(void) const
{
	return bottomRight().y() - topLeft().y();
}

unsigned int Rect::width(void) const
{
	return bottomRight().x() - topLeft().x();
}

Point &Rect::bottomRight(void)
{
	return _bottomRight;
}

Point &Rect::topLeft(void)
{
	return _topLeft;
}

Point const &Rect::bottomRight(void) const
{
	return _bottomRight;
}

Point const &Rect::topLeft(void) const
{
	return _topLeft;
}