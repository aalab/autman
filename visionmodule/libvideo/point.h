#ifndef _POINT_H_
#define _POINT_H_

#include <cmath>
#include <ostream>
#include <json/value.h>

class Point2D
{
public:
	Point2D();
	Point2D(double x, double y);

	double x;
	double y;

	double hypot();

	/**
	 * Calculates the distance from this point to a coordinate.
	 */
	double distance(double px, double py);

	/**
	 * Calculates the distance between this point and another instance.
	 */
	double distance(Point2D &p);
};

class Point
{
public:
	Point(unsigned int x = 0, unsigned int y = 0);

	unsigned int y(void) const;

	unsigned int x(void) const;

	void setX(unsigned int x);

	void setY(unsigned int y);

	void setXY(unsigned int x, unsigned int y);;

	double hypot(void) const;

private:
	unsigned int _x;
	unsigned int _y;
};

std::ostream &operator<<( std::ostream & os, Point const & point );
std::ostream &operator<<( std::ostream & os, Point2D const & point );
Json::Value &operator<<(Json::Value &json, Point const & point );
Json::Value &operator<<(Json::Value &json, Point2D const & point );

#endif /* _POINT_H_ */
