#ifndef _RECT_H_
#define _RECT_H_

#include <iostream>
#include <json/value.h>

#include "point.h"

class Rect
{
public:
	Rect();
	Rect(Point const &topLeft, Point const &bottomRight);
	Rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);

	Point const &topLeft(void) const;

	Point const &bottomRight(void) const;

	Point &topLeft();

	Point &bottomRight();

	unsigned int width() const;
	unsigned int height() const;
	unsigned int size() const;
	unsigned int ratio() const;
	unsigned int bbCenterX();

	unsigned int bbCenterY();

	bool inBound(Point2D &p);

	bool inBound(int x, int y);

	friend std::ostream &operator<<(std::ostream &os, Rect const &p);

private:
	Point _topLeft;
	Point _bottomRight;
};

std::ostream &operator<<( std::ostream & os, Rect const & rect );
Json::Value &operator<<(Json::Value & json, Rect const & rect);

#endif /* _RECT_H_ */
