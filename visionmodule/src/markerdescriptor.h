#ifndef __MARKERDESCRIPTOR_H__
#define __MARKERDESCRIPTOR_H__

#include <string>
#include <cstring>
#include <vector>
#include <cmath>
#include <json/value.h>
#include "integralcircle.h"

using namespace std;

class MarkerDescriptorCircle
{
public:
	std::map<int, double> angles;

	double isSimilar(IntegralCircle &circle);
};

class MarkerDescriptor
{
public:
	string name;

	MarkerDescriptorCircle internal;
	MarkerDescriptorCircle external;
	MarkerDescriptorCircle extra1;

	double isSimilar(IntegralCircle &internal, IntegralCircle &external, IntegralCircle &extra1);

	double train(IntegralCircle &internal, IntegralCircle &external, IntegralCircle &extra1);

protected:
	double train(MarkerDescriptorCircle &descriptor, IntegralCircle &internal, int minSize);
};

#endif
