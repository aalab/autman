#include <cstdio>
#include "findmarker.h"
#include "../libvideo/visionobject.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/framebufferrgb24be.h"
#include "../libvideo/integralimage.h"
#include "findgrid.h"
#include "../libvideo/imageprocessing.h"
#include "markerdescriptor.h"
#include "globals.h"
#include "configuration.h"
#include "colors.h"
#include "compilationdefinitions.h"
#include "http/actions/messages.h"
#include "findline.h"
#include "text.h"

#define M_PI_TIMES_2l M_PIl*2

using namespace std;

bool FindMarker::find(std::vector<VisionObject> &objects, Point &out, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults)
{
	if (objects.size() > 0)
	{
		double
			markerWidth = 10.0, markerHeight = 10.0,
			x, y, x2, y2,
			w, h,
			realCX, realCY;

		for (auto &vobject : objects)
		{
			vobject.center.x = (vobject.boundBox.topLeft().x() + (vobject.boundBox.width() / 2));
			vobject.center.y = (vobject.boundBox.topLeft().y() + (vobject.boundBox.height() / 2));

			image_coord_to_world_coord(vobject.boundBox.topLeft().x(), vobject.boundBox.bottomRight().y(), 0, &x, &y, &cameraParameters, &calibrationResults.getResult());
			image_coord_to_world_coord(vobject.boundBox.bottomRight().x(), vobject.boundBox.bottomRight().y(), 0, &x2, &y2, &cameraParameters, &calibrationResults.getResult());
			w = x2 - x;

			// cout << "Width: " << w << endl;
			if (w >= 8.0 && w <= 14.0)
			{
				realCX = x + w / 2;
				image_coord_to_world_coord(vobject.center.x, vobject.boundBox.bottomRight().y(), 0, &x, &y, &cameraParameters, &calibrationResults.getResult());
				image_coord_to_world_coord(vobject.center.x, vobject.boundBox.topLeft().y(), 0, &x2, &y2, &cameraParameters, &calibrationResults.getResult());
				h = y2 - y;
				realCY = y + h / 2;

				if (
					((w / h) >= 0.9) && ((w / h) <= 1.4)
						&& (((w * h) / (markerWidth * markerHeight)) >= 0.75) && ((w * h) / (markerWidth * markerHeight) <= 1.1)
					)
				{

					world_coord_to_image_coord(realCX, realCY, 0, &x, &y, &cameraParameters, &calibrationResults.getResult());

					out.setXY(x, y);
					return true;
				}
			}
		}
	}
	return false;
}

void FindMarker::extractMarker(FrameBuffer *in, Point &center, FrameBuffer *out, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults)
{
	unsigned int
		newWidth = out->width, newHeight = out->height,
		ifromX, ifromY,
		ix, iy;

	double
		markerWidth = 10.0,
		markerHeight = 10.0,
		imgWidth = (markerWidth) * 1.5,
		imgHeight = (markerHeight) * 1.5,
		halfImgWidth = imgWidth / 2.0,
		halfImgHeight = imgHeight / 2.0,
		pX = imgWidth / newWidth,
		pY = imgHeight / newHeight;

	double
		rcx, rcy,        // real center
		realX, realY,    // Real coordinates for the X and Y on real world
		imgX, imgY;        // Image coordinates for realX and realY

	image_coord_to_world_coord(
		center.x(), center.y(), 0,
		&rcx, &rcy, &cameraParameters, &calibrationResults.getResult()
	);

	RawPixel px;
	for (unsigned int y = 0; y < newHeight; y++)
	{
		for (unsigned int x = 0; x < newWidth; x++)
		{
			realX = rcx + (x * pX) - halfImgWidth;
			realY = rcy + (y * pY) - halfImgHeight;

			world_coord_to_image_coord(realX, realY, 0, &imgX, &imgY, &cameraParameters, &calibrationResults.getResult());
			if (
				(imgX >= 0) && (imgY < in->width)
					&& (imgY >= 0) && (imgY < in->height)
				)
			{
				in->getPixel((uint) imgY, (uint) imgX, &px);
				out->setPixel(newHeight - y, x, px);
			}
			else
				return;
		}
	}

	ix = (markerWidth * newWidth / imgWidth);
	iy = (markerHeight * newHeight / imgHeight);

	ifromX = (newWidth / 2.0) - ix / 2.0;
	ifromY = (newHeight / 2.0) - iy / 2.0;

	uint
		areasX = 4,
		areasY = 4;

	IntegralImage ii(out->width, out->height);

	{
		RawPixel r;
		for (uint x = 0; x < out->width; x++)
		{
			for (uint y = 0; y < out->height; y++)
			{
				out->getPixel(y, x, &r);
				if (r.intIntensity() > 128)
				{
					r.red = 255;
					r.green = 255;
					r.blue = 255;
				}
				else
				{
					r.red = 0;
					r.green = 0;
					r.blue = 0;
				}
				out->setPixel(y, x, r);
			}
		}
	}

	ImageProcessing::calcIntegralImage(out, &ii);

	Point
		tl(0, 0),
		br(0, 0);
	Rect area(tl, br);

	if (Globals::GetGlobals()->configuration->markers.size() > 0)
	{
		cout << Globals::GetGlobals()->configuration->markers.size() << endl;
		// int markersPoints[Globals::GetGlobals()->configuration->markers.size()];

		/*
		for (i = 0; i < Globals::GetGlobals()->configuration->markers.size(); i++)
			markersPoints[i] = 0;
		*/
		for (uint x = 0; x < areasX; x++)
		{
			for (uint y = 0; y < areasY; y++)
			{
				area.topLeft().setXY(ifromX + (x * (ix / areasX)), ifromY + (y * (iy / areasY)));
				area.bottomRight().setXY(ifromX + ((x + 1) * (ix / areasX)), ifromY + ((y + 1) * (iy / areasY)));

				VERBOSE("Area (" << x << "," << y << ") (" << area.topLeft().x() << "," << area.topLeft().y() << ") -> (" << area.bottomRight().x() << "," << area.bottomRight().y() << "): " << ((ii.avgColour(area) / (double) (area.size())) / 255.0));

				/*
				i = 0;
				for (auto& marker : Globals::GetGlobals()->configuration->markers)
				{
					if (marker.isSimilar(x, y, avgc) > 0.9)
					{
						markersPoints[i]++;
						cout << "\t" << marker.name << " error: " << (avgc) << " is similar" << endl;
					}
					else
						cout << "\t" << marker.name << " error: " << (avgc) << " failed" << endl;
					i++;
				}
				*/

				ImageProcessing::drawBresenhamLine(out, ifromX + (x * (ix / areasX)), ifromY + (y * (iy / areasY)), ifromX + (x * (ix / areasX)), ifromY + ((y + 1) * (iy / areasY)), Colors::red);
				ImageProcessing::drawBresenhamLine(out, ifromX + (x * (ix / areasX)), ifromY + ((y + 1) * (iy / areasY)), ifromX + ((x + 1) * (ix / areasX)), ifromY + ((y + 1) * (iy / areasY)), Colors::red);
			}
		}

		cout << 1 << endl;

		tl.setXY(0, 0);
		br.setXY(in->width - 1, in->height - 1);
		/*
		for (auto hits : markersPoints)
		{
			marker = &Globals::GetGlobals()->configuration->markers[i];
			if (hits == MARKER_SIZE)
			{
				cout << "MARKER IDENTIFIED AS " << marker->getName() << "!" << endl;
				if (marker->getName() == "right")
				{
					px.red = 255;
					px.green = px.blue = 0;
				}
				else if (marker->getName() == "left")
				{
					px.red = px.blue = 0;
					px.green = 255;
				}
				else if (marker->getName() == "forward")
				{
					px.red = px.green = 0;
					px.blue = 255;
				}
				ImageProcessing::drawBresenhamLine(in, tl, br, px);
			}
			else
				cout << marker->getName() << " was ignored!" << hits << " from " << MARKER_SIZE << endl;
			i++;
		}
		*/
	}
	// ii.toFrame(out);
	// out->outToPPM("ppms/marker_result_ii.ppm");
}

void FindMarker::GetImageLineTop(double *lineTopCenterX, double *lineTopY, unsigned int offset, VisionObject *line, FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, ColourDefinition *tapeColour)
{
	unsigned int count;
	Pixel cPixel;
	unsigned int col, sumRowX;
	FrameBufferIterator it(frame);
	*lineTopCenterX = -1;
	*lineTopY = -1;

	for (unsigned int row = line->boundBox.topLeft().y() + offset; row < line->boundBox.bottomRight().y(); row = row + subsample)
	{
		it.goPosition(row, line->boundBox.topLeft().x());
		sumRowX = 0;

		count = 0;
		for (col = line->boundBox.topLeft().x(); col < line->boundBox.bottomRight().x(); col = col + subsample, it.goRight(subsample))
		{
			it.getPixel(&cPixel);

			// if (tapeColour->isMatch(cPixel))
			if (cPixel.isSameColor(&line->seedColour))
			{
				if (*lineTopCenterX == -1 && *lineTopY == -1)
				{
					count++;
					sumRowX += col;
				}
			}
		}
		// check at the end of each row
		if (*lineTopCenterX == -1 && *lineTopY == -1 && count > minLength)
		{
			*lineTopCenterX = sumRowX / count;
			*lineTopY = row;
			break;
		}
	}
}

//TODO combine with GetImageLineTop
void FindMarker::GetImageLineBottom(double *lineBottomCenterX, double *lineBottomY, unsigned int offset, VisionObject *line, FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, ColourDefinition *tapeColour)
{
	unsigned int count;
	Pixel cPixel;
	unsigned int col, sumRowX;
	FrameBufferIterator it(frame);
	*lineBottomCenterX = -1;
	*lineBottomY = -1;
	for (unsigned int row = line->boundBox.bottomRight().y() - offset; row > line->boundBox.topLeft().y(); row = row - subsample)
	{
		it.goPosition(row, line->boundBox.topLeft().x());
		sumRowX = 0;

		count = 0;
		for (col = line->boundBox.topLeft().x(); col < line->boundBox.bottomRight().x(); col = col + subsample, it.goRight(subsample))
		{
			it.getPixel(&cPixel);

			// if (tapeColour->isMatch(cPixel))
			if (cPixel.isSameColor(&line->seedColour))
			{
				if (*lineBottomCenterX == -1 && *lineBottomY == -1)
				{
					count++;
					sumRowX += col;
				}
			}
		}

		// check at the end of each row
		if (*lineBottomCenterX == -1 && *lineBottomY == -1 && count > minLength)
		{
			*lineBottomCenterX = sumRowX / count;
			*lineBottomY = row;
			break;
		}
	}
}

void FindMarker::DrawFlag(FrameBuffer *frame, double flagDim, double worldLineTopX, double worldLineTopY, double worldLineMiddleX, double worldLineMiddleY)
{
	cout << "worldLineX " << worldLineTopX << ", worldLineTopY " << worldLineTopY << endl;

	double worldLineSlope = (worldLineTopY - worldLineMiddleY) / (worldLineTopX - worldLineMiddleX);
	// Get the x and y offset from the top of the line to the middle of the flag
	double x = ((double) flagDim / 2.0) / (sqrt(worldLineSlope * worldLineSlope + 1.0));
	double y = x * worldLineSlope;
	cout << "x " << x << ", y " << y << endl;
	double imageFlagCenterX, imageFlagCenterY;
	Globals *glob = Globals::GetGlobals();

	// For now, assume the flag is always above the line
	if (y > 0)
		y *= -1;

	world_coord_to_image_coord(worldLineTopX + x, worldLineTopY - y, 0, &imageFlagCenterX, &imageFlagCenterY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());

	FrameBuffer *undistortedImage = 0;
	undistortedImage = new FrameBufferRGB24BE();
	undistortedImage->initialize(flagDim, flagDim);
	Point imFlagTopLeft = Point();
	Point imFlagTopRight = Point();
	Point imFlagBottomRight = Point();
	Point imFlagBottomLeft = Point();
	bool saveUndistortedImage = false;

	FindMarker::GetUndistortedImage(frame, undistortedImage, imageFlagCenterX, imageFlagCenterY, worldLineSlope, flagDim, &imFlagTopLeft, &imFlagTopRight, &imFlagBottomRight, &imFlagBottomLeft, saveUndistortedImage);
	FindMarker::ClassifyArrow(frame, undistortedImage, imFlagTopLeft, imFlagTopRight, imFlagBottomRight, imFlagBottomLeft, flagDim);
}

void FindMarker::GetUndistortedImage(FrameBuffer *cameraFrame, FrameBuffer *undistortedImage, double imageCx, double imageCy, double worldLineSlope, int fullDim, Point *imFlagTopLeft, Point *imFlagTopRight, Point *imFlagBottomRight, Point *imFlagBottomLeft, bool save)
{
	Globals *glob = Globals::GetGlobals();
	int halfDim = fullDim / 2;

	double worldCx;
	double worldCy;
	double worldTopLeftX;
	double worldTopLeftY;

	// TODO switch to using Points:
	double imageTopLeftX = -1;
	double imageTopLeftY = -1;
	double imageTopRightX = -1;
	double imageTopRightY = -1;
	double imageBottomLeftX = -1;
	double imageBottomLeftY = -1;
	double imageBottomRightX = -1;
	double imageBottomRightY = -1;
	double imageTempX;
	double imageTempY;
	//double x;
	//double y;
	double imageTempX2;
	double imageTempY2;

	// get the center of the point of interest in world coord
	image_coord_to_world_coord(imageCx, imageCy, 0, &worldCx, &worldCy, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());

	worldTopLeftX = worldCx - halfDim;
	worldTopLeftY = worldCy - halfDim;

	//crosshair
	// TODO remove this crosshair once things it appears to be working
	world_coord_to_image_coord(worldCx - halfDim, worldCy, 0, &imageTempX, &imageTempY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	world_coord_to_image_coord(worldCx + halfDim - 1, worldCy, 0, &imageTempX2, &imageTempY2, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	ImageProcessing::drawBresenhamLine(cameraFrame, imageTempX, imageTempY, imageTempX2, imageTempY2, RawPixel(0, 255, 0));
	world_coord_to_image_coord(worldCx, worldCy - halfDim, 0, &imageTempX, &imageTempY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	world_coord_to_image_coord(worldCx, worldCy + halfDim - 1, 0, &imageTempX2, &imageTempY2, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	ImageProcessing::drawBresenhamLine(cameraFrame, imageTempX, imageTempY, imageTempX2, imageTempY2, RawPixel(0, 255, 0));

	world_coord_to_image_coord(worldTopLeftX, worldTopLeftY + fullDim, 0, &imageBottomLeftX, &imageBottomLeftY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	world_coord_to_image_coord(worldTopLeftX + fullDim, worldTopLeftY + fullDim, 0, &imageBottomRightX, &imageBottomRightY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	world_coord_to_image_coord(worldTopLeftX, worldTopLeftY, 0, &imageTopLeftX, &imageTopLeftY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
	world_coord_to_image_coord(worldTopLeftX + fullDim, worldTopLeftY, 0, &imageTopRightX, &imageTopRightY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());

	// continue if image is in bounds
	if (ImageProcessing::isInBounds(cameraFrame, imageTopLeftX, imageTopLeftY) &&
		ImageProcessing::isInBounds(cameraFrame, imageTopRightX, imageTopRightY) &&
		ImageProcessing::isInBounds(cameraFrame, imageBottomRightX, imageBottomRightY) &&
		ImageProcessing::isInBounds(cameraFrame, imageBottomLeftX, imageBottomLeftY))
	{
		RawPixel tempPixel = RawPixel(0, 0, 0);
		// DBG("worldTopLeftX %f, worldTopLeftY %f\n", worldTopLeftX, worldTopLeftY);
		for (int y = 0; y < fullDim; ++y)
		{
			for (int x = 0; x < fullDim; ++x)
			{

				world_coord_to_image_coord(worldTopLeftX + x, worldTopLeftY + y, 0, &imageTempX, &imageTempY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
				// DBG("imageTempX %f frame width %f\n", imageTempX, (double)cameraFrame->width);
				assert(imageTempX < (double) cameraFrame->width);
				assert(imageTempY < (double) cameraFrame->height);
				cameraFrame->getPixel((int) imageTempY, (int) imageTempX, &tempPixel);

				undistortedImage->setPixelAt((unsigned int) y, (unsigned int) x, tempPixel);
			}
		}

		if (save)
		{
			undistortedImage->outToPPM("undistortedImage.ppm");
		}
		imFlagTopLeft->setX((unsigned int) imageTopLeftX);
		imFlagTopLeft->setY((unsigned int) imageTopLeftY);
		imFlagTopRight->setX((unsigned int) imageTopRightX);
		imFlagTopRight->setY((unsigned int) imageTopRightY);
		imFlagBottomRight->setX((unsigned int) imageBottomRightX);
		imFlagBottomRight->setY((unsigned int) imageBottomRightY);
		imFlagBottomLeft->setX((unsigned int) imageBottomLeftX);
		imFlagBottomLeft->setY((unsigned int) imageBottomLeftY);
	}
}

void FindMarker::ClassifyArrow(FrameBuffer *cameraFrame, FrameBuffer *undistortedImage, Point imageTopLeft, Point imageTopRight, Point imageBottomRight, Point imageBottomLeft, int flagDim)
{
	// TODO these values were set on the web client
	double upContrast = 2.5;
	double sideContrast = 1.1;

	IntegralImage integralImage = IntegralImage(flagDim, flagDim);
	ImageProcessing::calcIntegralImage(undistortedImage, &integralImage);
	const int numIntegralSections = 3;
	int integralSectionWidth = integralImage.width / 3;
	uint64_t avgColour[numIntegralSections];
	for (int i = 0; i < numIntegralSections; i++)
	{
		Point topLeft = Point(i * integralSectionWidth, 0);
		Point bottomRight = Point((i + 1) * integralSectionWidth, integralImage.height);
		avgColour[i] = integralImage.avgColour(Rect(topLeft, bottomRight));
	}

	RawPixel borderColour;
	if (avgColour[1] > avgColour[0] * upContrast && avgColour[1] > avgColour[2] * upContrast)
	{
		// Up Arrow
		borderColour = RawPixel(255, 0, 0);
	}
	else if (avgColour[0] > avgColour[1] && avgColour[1] > avgColour[2] && avgColour[0] > avgColour[2] * sideContrast)
	{
		// Right Arrow
		borderColour = RawPixel(0, 255, 0);
	}
	else if (avgColour[2] > avgColour[1] && avgColour[1] > avgColour[0] && avgColour[2] > avgColour[0] * sideContrast)
	{
		// Left Arrow
		borderColour = RawPixel(0, 0, 255);
	}
	else
	{
		// Unknown Arrow
		borderColour = RawPixel(255, 255, 255);
	}

	// draw boundaries
	if (ImageProcessing::isInBounds(cameraFrame, imageTopLeft.x() - 2, imageTopLeft.y() - 2) && ImageProcessing::isInBounds(cameraFrame, imageTopRight.x() + 2, imageTopRight.y() - 2))
	{
		//top left to top right
		ImageProcessing::drawBresenhamLine(cameraFrame, imageTopLeft.x() - 2, imageTopLeft.y() - 2, imageTopRight.x() + 2, imageTopRight.y() - 2, borderColour);
	}
	if (ImageProcessing::isInBounds(cameraFrame, imageTopRight.x() + 2, imageTopRight.y() - 2) && ImageProcessing::isInBounds(cameraFrame, imageBottomRight.x() + 2, imageBottomRight.y() + 2))
	{
		//top right to bottom right
		ImageProcessing::drawBresenhamLine(cameraFrame, imageTopRight.x() + 2, imageTopRight.y() - 2, imageBottomRight.x() + 2, imageBottomRight.y() + 2, borderColour);
	}
	if (ImageProcessing::isInBounds(cameraFrame, imageBottomRight.x() + 2, imageBottomRight.y() + 2) && ImageProcessing::isInBounds(cameraFrame, imageBottomLeft.x() - 2, imageBottomLeft.y() + 2))
	{
		//bottom right to bottom left
		ImageProcessing::drawBresenhamLine(cameraFrame, imageBottomRight.x() + 2, imageBottomRight.y() + 2, imageBottomLeft.x() - 2, imageBottomLeft.y() + 2, borderColour);
	}
	if (ImageProcessing::isInBounds(cameraFrame, imageBottomLeft.x() - 2, imageBottomLeft.y() + 2), ImageProcessing::isInBounds(cameraFrame, imageTopLeft.x() - 2, imageTopLeft.y() - 2))
	{
		//bottom left to top left
		ImageProcessing::drawBresenhamLine(cameraFrame, imageBottomLeft.x() - 2, imageBottomLeft.y() + 2, imageTopLeft.x() - 2, imageTopLeft.y() - 2, borderColour);
	}
}

void FindMarker::getImageLine(
	double *centerX, double *centerY, unsigned int x, unsigned int y, int offset, VisionObject *line,
	FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, ColourDefinition *tapeColour,
	const double angleLine
)
{
	Pixel cPixel;
	int
		count = 0,
		col, row;
	*centerX = -1;
	*centerY = -1;

	/*
	RawPixel
		cyanPixel(0, 255, 255),
		yellowPixel(255, 255, 0),
		blackPixel(0, 0, 0);
	*/

	double
		angle = angleLine - M_PI_2l,
		slope = std::tan(angle),
		s = std::sin(angleLine),
		c = std::cos(angleLine);

	/*
	ImageProcessing::drawRectangle(outFrame, line->bBox, cyanPixel);

	cout << "#FindMarker::getImageLine" << endl;
	cout << "\tSlope: " << slope << endl;
	cout << "\tOffset: " << offset << endl;
	cout << "\tAngle line: " << R2D(angleLine) << endl;
	cout << "\tAngle: " << R2D(angle) << endl;
	*/

	double
		start_y = line->boundBox.topLeft().y(),
		start_x = (start_y - (y + s * offset)) / slope + (x + c * offset),
		end_y = line->boundBox.bottomRight().y(),
		end_x = (end_y - (y + s * offset)) / slope + (x + c * offset);

	if (start_x < line->boundBox.topLeft().x())
	{
		// cout << "1" << endl;
		start_x = line->boundBox.topLeft().x();
		start_y = (slope * (start_x - (x + c * offset))) + (y + s * offset);
	}
	if (start_x > line->boundBox.bottomRight().x())
	{
		// cout << "2" << endl;
		start_x = line->boundBox.bottomRight().x();
		start_y = (slope * (start_x - (x + c * offset))) + (y + s * offset);
	}
	if (end_x > line->boundBox.bottomRight().x())
	{
		// cout << "3" << endl;
		end_x = line->boundBox.bottomRight().x();
		end_y = (slope * (end_x - (x + c * offset))) + (y + s * offset);
	}
	if (end_x < line->boundBox.topLeft().x())
	{
		// cout << "4" << endl;
		end_x = line->boundBox.topLeft().x();
		end_y = (slope * (end_x - (x + c * offset))) + (y + s * offset);
	}

	/*
	FindMarker::drawCross(outFrame, x, y, cyanPixel);
	FindMarker::drawCross(outFrame, x + c*offset, y + s*offset, yellowPixel);

	cout << "P: (" << x << " : " << y << ")" << endl;
	cout << "P: (" << c << " : " << s << ")" << endl;
	cout << "P: (" << offset << " : " << offset << ")" << endl;
	cout << "P: (" << c*offset << " : " << s*offset << ")" << endl;

	cout << "Start: (" << start_x << " : " << (unsigned int)start_y << ")" << endl;
	cout << "End: (" << end_x << " : " << end_y << ")" << endl;

	FindMarker::drawCross(outFrame, start_x, start_y, cyanPixel);
	FindMarker::drawCross(outFrame, end_x, end_y, cyanPixel);
	*/

	c = std::cos(angle);
	s = std::sin(angle);

	double
		d = std::sqrt(((end_x - start_x) * (end_x - start_x)) + ((end_y - start_y) * (end_y - start_y))),
		tmpD;
	for (unsigned int i = 0; i < d; i++)
	{
		col = start_x + (end_x - start_x) / d * (i * subsample);
		row = start_y + (end_y - start_y) / d * (i * subsample);

		// col = start_x + c*(i*subsample);
		// row = start_y + s*(i*subsample);

		// cout << "Looking at: (" << col << " : " << row << ") " << d << endl;

		frame->getPixel(row, col, &cPixel);
		cPixel.update();
		// if (tapeColour->isMatch(cPixel) && ((i + 1) < d))
		if (cPixel.isSameColor(&line->seedColour) && ((i + 1) < d))
		{
			outFrame->setPixel(row, col, Colors::cyan);
			if (*centerX == -1 && *centerY == -1)
			{
				// cout << "Start found! (" << col << " : " << row << ")" << endl;
				*centerX = col;
				*centerY = row;
				count++;
			}
		}
		else if ((count > 0))
		{
			tmpD = std::sqrt(((col - *centerX) * (col - *centerX)) + ((row - *centerY) * (row - *centerY)));
			// cout << "tmpD: " << tmpD << endl;
			if (tmpD > minLength)
			{
				// cout << "End found! (" << col << " : " << row << ")" << endl;
				*centerX = *centerX + ((col - *centerX) / 2);
				*centerY = *centerY + ((row - *centerY) / 2);

				// FindMarker::drawCross(outFrame, *centerX, *centerY, blackPixel);
				break;
			}
			else
			{
				*centerX = *centerY = -1;
				count = 0;
			}
		}
	}
}

void FindMarker::expandImageLine(
	double *pointX, double *pointY, unsigned int x, unsigned int y, VisionObject &line, FrameBuffer *frame,
	FrameBuffer *outFrame, ColourDefinition &tapeColour, ColourDefinition &markerColour, const double angle,
	unsigned int threshold
)
{
	Pixel p;
	double
		pX, pY,
		c = std::cos(angle),
		s = std::sin(angle);
	unsigned int markerMatchCount = 0;
	for (unsigned int i = 0; ; i++)
	{
		pX = x - (c * i);
		pY = y - (s * i);
		if (
			(pX >= 0) && (pX < frame->width)
				&& (pY >= 0) && (pY < frame->height)
			)
		{
			frame->getPixel(pY, pX, &p);
			p.update();
#ifdef FINDMARKER__OUTPUTDRAW
			outFrame->setPixel(pY, pX, Colors::pink);
#endif
			if (markerColour.isMatch(p))
			{
				if (++markerMatchCount >= threshold)
					break;
			}
			else
			{
				markerMatchCount = 0;
				*pointX = pX;
				*pointY = pY;
			}
		}
		else
			break;
	}
}

void FindMarker::calculateIntegralCircle(
	FrameBuffer *frame, FrameBuffer *outFrame, IntegralCircle &circle, double angle, double radius, double centerX,
	double centerY, Pixel &p, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults
)
{
	double
		imgX, imgY,
		sum = 0,
		// end = M_PI_TIMES_2l - D2R(1);
		end = M_PI_TIMES_2l - D2R(1);

	angle -= M_PI_2l;

	unsigned int
		ad = 0;
	bool inbound;
	for (double a = 0; a < end; a += D2R(1))
	{
		world_coord_to_image_coord(
			(centerX + (radius * std::cos(angle + a))),
			(centerY + (radius * std::sin(angle + a))),
			0, &imgX, &imgY, &cameraParameters, &calibrationResults.getResult()
		);
		inbound = (imgX >= 0) && (imgX < frame->width) && (imgY >= 0) && (imgY < frame->height);
		if (inbound)
		{
			frame->getPixel(imgY, imgX, &p);
			circle.values[ad] = p.intIntensity();    // Black or white
		}
		else
			circle.values[ad] = 0; // unsigned

		if (ad == 0)
		{
			// cout << "\tSetting min and max to " << circle.values[0] << " at " << ad << endl;
			circle.maxValue = circle.minValue = circle.values[0];
		}
		else if (circle.minValue > circle.values[ad])
		{
			// cout << "\tSetting min to " << circle.minValue << " at " << ad << endl;
			circle.minValue = circle.values[ad];
		}
		else if (circle.maxValue < circle.values[ad])
		{
			// cout << "\tSetting max to " << circle.maxValue << " at " << ad << endl;
			circle.maxValue = circle.values[ad];
		}
#ifdef FINDMARKER__OUTPUTDRAW
		if (inbound)
			outFrame->setPixel(imgY, imgX, Colors::cyan);
#endif
		ad++;
	}

	double
		diff = (circle.maxValue - circle.minValue);

//	cout << "\tMIN: " << circle.minValue << endl;
//	cout << "\tMAX: " << circle.maxValue << endl;

	if (diff > 0)
	{
		// cout << "\tProportion" << diff << endl;
		for (ad = 0; ad < 360; ad++)
		{
//			if (!(ad % 10))
//				cout << "\t" << (ad) <<  "º: " << ((circle.values[ad] - circle.minValue)/diff) << endl;
			sum += ((circle.values[ad] - circle.minValue)/diff);
			circle.values[ad] = sum;
		}
	}
	else
	{
		for (ad = 0; ad < 360; ad++)
			circle.values[ad] = 0;
	}
}

// IMPORTANT! NOT THREAD SAFE!
std::chrono::time_point<std::chrono::system_clock>
	findmarkerCurrentDetection, findmarkerPriorDetection;
std::chrono::duration<double> findmarkerDurationDetection;

void FindMarker::classifyIntegralCircle(
	IntegralCircle &integralCircleInternal, IntegralCircle &integralCircleExternal, IntegralCircle &extra1,
	MarkerDescriptor **markerDescriptor
)
{
	findmarkerCurrentDetection = std::chrono::system_clock::now();
	findmarkerDurationDetection = (findmarkerCurrentDetection - findmarkerPriorDetection);
	cout << "Diff: " << findmarkerDurationDetection.count() << endl;
	*markerDescriptor = NULL;
	double error;
	if (findmarkerDurationDetection.count() > 0.3)	// For interface
	{
		try
		{
			lock_guard<mutex> lock(http::actions::Messages::mutex);

			Json::Value &json = http::actions::Messages::emplace();
			json["command"] = "markers";
			for (auto &marker : Globals::GetGlobals()->configuration->markers)
			{
				error = marker.isSimilar(integralCircleInternal, integralCircleExternal, extra1);
				if (error == 0.0)
					*markerDescriptor = &marker;
				json["params"][marker.name] = error;
			}
		}
		catch (http::actions::Messages::FullQueue &e)
		{ }
		findmarkerPriorDetection = findmarkerCurrentDetection;
	}
	else
	{
		for (auto &marker : Globals::GetGlobals()->configuration->markers)
		{
			error = marker.isSimilar(integralCircleInternal, integralCircleExternal, extra1);
			if (error == 0.0)
				*markerDescriptor = &marker;
			cout << error << ": " << marker.name << endl;
		}
	}
}

bool FindMarker::MarkerExists(FrameBuffer *frame, FrameBuffer *outFrame, unsigned int subsample, ColourDefinition *markerColour, unsigned int markerSizeMin, unsigned int checkForMarkerRadius, double markerCX, double markerCY)
{
	bool markerExists = false;
	Globals *glob = Globals::GetGlobals();
		
	// If the marker colour exists, check to see if there is a large marker coloured object
	if (markerColour != NULL)
	{
		std::vector<VisionObject> markerResults;

		ImageProcessing::SegmentColours(frame, outFrame, 50, 5, 20, subsample, *markerColour, Colors::green, markerResults, false, true);
		
		cout << "After ImageProcessing; results size " << markerResults.size() << std::endl;
		if (markerResults.size() > 0)
		{
			double radiusLength;
			double worldMarkerX, worldMarkerY;
			for (auto &marker : markerResults)
			{
				image_coord_to_world_coord(marker.center.x, marker.center.y, 0, &worldMarkerX, &worldMarkerY, &FindGrid::cameraParameters, &glob->configuration->currentCameraCalibration->getResult());
				radiusLength = sqrt(pow((worldMarkerX - markerCX), 2.0) + pow((worldMarkerY - markerCY), 2.0));
				cout << "\tradiusLength " << radiusLength << std::endl;

				// If the object is found and is large, then return true
				// TODO consider checking for marker.size > markerSizeMin &&
				if (radiusLength < checkForMarkerRadius)
				{
					cout << "marker exists" << std::endl;
					markerExists = true;
					// sleep(4);
					break;
				}
			}
		}
	}
	
	return markerExists;
}


// TODO Set the worldLineMiddle
void FindMarker::getLineInfo(
	FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, unsigned int lineOffset, double *worldLineTopX,
	double *worldLineTopY, double *worldLineMiddleX, double *worldLineMiddleY, double *worldLineBottomX,
	double *worldLineBottomY, double *markerCenterX, double *markerCenterY, double *lineAngle, unsigned int minLength,
	unsigned int subsample, ColourDefinition *tapeColour, ColourDefinition *markerColour, CameraCalibrationData& calibrationData, double markerSize
)
{
	double
		imageLineTopX, imageLineTopY,
		imageLineBottomX, imageLineBottomY,
		nX, nY;

	// TODO set a max distance in front to search
	// Get the top line

	FindMarker::GetImageLineTop(
		&imageLineTopX, &imageLineTopY, lineOffset, &vo, frame, outFrame, minLength, subsample, tapeColour
	);
	if (imageLineTopX != -1 && imageLineTopY != -1)
	{
		image_coord_to_world_coord(imageLineTopX, imageLineTopY, 0, worldLineTopX, worldLineTopY, &FindGrid::cameraParameters, &calibrationData.getResult());

		// Get the bottom line
		FindMarker::GetImageLineBottom(
			&imageLineBottomX, &imageLineBottomY, lineOffset, &vo, frame, outFrame, minLength, subsample, tapeColour
		);
		if (imageLineBottomX != -1 && imageLineBottomY != -1)
		{
			/*
			if (imageLineTopY == glob->configuration->marker.lineOffset)                // If the tape came from upper (not from down)
			{
				nX = imageLineTopX;
				imageLineTopX = imageLineBottomX;
				imageLineBottomX = nX;

				nY = imageLineTopY;
				imageLineTopY = imageLineBottomY;
				imageLineBottomY = nY;
			}
			*/

			// ImageProcessing::drawBresenhamLine(outFb, imageLineTopX, imageLineTopY, imageLineBottomX, imageLineBottomY, Colors::blue);

			double plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
			FindMarker::getImageLine(
				&nX, &nY, imageLineTopX, imageLineTopY, lineOffset, &vo, frame, outFrame, 1, 1, tapeColour, plineAngle
			);
			if (nX != -1)
			{
				imageLineTopX = nX;
				imageLineTopY = nY;
				// cout << "NEW TOP: (" << imageLineTopX << " : " << imageLineTopY << ")" << endl;
				// cout << "Prior Angle: " << R2D(plineAngle) << endl;
				plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
				// cout << "Pos Angle: " << R2D(plineAngle) << endl;
			}
			/*
			else
				cout << "New top wasn't found!!!" << endl;
			*/

			FindMarker::getImageLine(
				&nX, &nY, imageLineBottomX, imageLineBottomY, -lineOffset, &vo, frame, outFrame, 1, 1, tapeColour,
				plineAngle
			);
			if (nX != -1)
			{
				imageLineBottomX = nX;
				imageLineBottomY = nY;
				plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
				// cout << "NEW BOTTOM!!!" << endl;
			}
			/*
			else
				cout << "New bottom wasn't found!!!" << endl;
			*/

			// ImageProcessing::drawBresenhamLine(outFb, imageLineTopX, imageLineTopY, imageLineBottomX, imageLineBottomY, Colors::yellow);

			if (markerColour != NULL)
			{
				FindMarker::expandImageLine(
					&nX, &nY, imageLineTopX, imageLineTopY, vo, frame, outFrame, *tapeColour, *markerColour, plineAngle,
					0
				);

				// FindMarker::drawCross(fb, nX, nY, markerPixel);
				// cout << "New top again: (" << nX << " : " << nY << ")" << endl;

				imageLineTopX = nX;
				imageLineTopY = nY;
			}

			image_coord_to_world_coord(imageLineTopX, imageLineTopY, 0, worldLineTopX, worldLineTopY, &FindGrid::cameraParameters, &calibrationData.getResult());
			image_coord_to_world_coord(imageLineBottomX, imageLineBottomY, 0, worldLineBottomX, worldLineBottomY, &FindGrid::cameraParameters, &calibrationData.getResult());

			double
				d = markerSize / 2.0; // Marker center

			*lineAngle = atan2((*worldLineTopY - *worldLineBottomY), (*worldLineTopX - *worldLineBottomX));
			*markerCenterX = *worldLineTopX + std::cos(*lineAngle) * (d);
			*markerCenterY = *worldLineTopY + std::sin(*lineAngle) * (d);

			/*{	// Draw perpendicular line
				double plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
				cout << "Image line angle: " << R2D(plineAngle) << endl;
			}*/

			/*
			cout << "\tLine length: " << d << endl;
			cout << "\tLine angle: " << 180*lineAngle/M_PIl << "(" << lineAngle << ")" << endl;
			cout << "\tNew length: " << (d+5) << endl;

			cout << "\tNew point: (" << (pointX) << " : " << pointY << ")" << endl;
			*/
		}
		else
		{
			*worldLineBottomX = *worldLineBottomY = *worldLineMiddleX = *worldLineMiddleY =
				*markerCenterX = *markerCenterY = -1;
		}
	}
	else
	{
		*worldLineTopX = *worldLineTopY = *worldLineBottomX = *worldLineBottomY = *worldLineMiddleX = *worldLineMiddleY =
			*markerCenterX = *markerCenterY = -1;
	}
}

bool FindMarker::find2(
	FrameBuffer *frame, FrameBuffer *outFrame, std::vector<VisionObject> &tapes, std::vector<VisionObject> &markers,
	std::vector<VisionObject> &markersInside, tsai_camera_parameters &cameraParameters,
	CameraCalibrationData &calibrationResults, FindMarkerResult &result
)
{
	unsigned int subsampleLine = 5;

	FindLineData lineData;

	double inclination, inclinationParallel, s, c, pS, pC, d;

	FrameBufferRGB24BE tmp;
	tmp.initialize(100, 100);

	RawPixel p;

	Line2D parallel;

	result.marker = "notfound";

	VisionObject *markerCandidate;
	if (FindLine::find(frame, outFrame, tapes, lineData, false, 20))
	{
		if (!lineData.lines.empty())
			result.line = lineData.lines[0];

		for (Line2D line : lineData.lines)
		{
			ImageProcessing::drawBresenhamLine(outFrame, line, Colors::pink);

			inclination = line.inclination() + M_PIl;
			s = std::sin(inclination);
			c = std::cos(inclination);

			inclinationParallel = inclination + M_PI_2l;
			pS = std::sin(inclinationParallel);
			pC = std::cos(inclinationParallel);

			markerCandidate = nullptr;
			d = 0;
			for (
				int x = line.a.x, y = line.a.y;
				(markerCandidate == nullptr) && outFrame->inBounds(x, y)/* && (d < 20) / * limit marker distance */;
				d += subsampleLine,
				x = line.a.x + (d*c), y = line.a.y + (d*s)
			)
			{
				for (VisionObject &object : markers)
				{
					// ImageProcessing::drawCross(outFrame, x, y, Colors::green, 3);
					if (object.boundBox.inBound(x, y))
					{
						for (VisionObject &arrow : markersInside)
						{
							if (object.boundBox.inBound(arrow.center))
							{
								// VERBOSE("Marker found!");
								markerCandidate = &object;

								ImageProcessing::drawRectangle(frame, object.boundBox, Colors::blue);
								ImageProcessing::drawRectangle(frame, arrow.boundBox, Colors::green);

								unsigned int state[3] = { 0, 0, 0 };
								bool stateOpen[3] = { true, true, true };

								for (;
									object.boundBox.inBound(x, y);
									d += 1,
									x = line.a.x + (d*c), y = line.a.y + (d*s)
								)
								{
									if (outFrame->inBounds(x - pC*(arrow.boundBox.width()/6*2), y - pS*(arrow.boundBox.width()/6*2)))
									{
										outFrame->getPixel(y - pS*(arrow.boundBox.width()/6*2), x - pC*(arrow.boundBox.width()/6*2), &p);
										if (stateOpen[0] && p.isSameColor(&arrow.seedColour))
										{
											state[0]++;
										}
										frame->setPixel(y - pS*(arrow.boundBox.width()/6*2), x - pC*(arrow.boundBox.width()/6*2), Colors::yellow);
									}

									outFrame->getPixel(y, x, &p);
									if (stateOpen[1] && p.isSameColor(&arrow.seedColour))
									{
										state[1]++;
									}
									frame->setPixel(y, x, Colors::red);

									if (outFrame->inBounds(x + pC*(arrow.boundBox.width()/6*2), y + pS*(arrow.boundBox.width()/6*2))){
										outFrame->getPixel(y + pS*(arrow.boundBox.width()/6*2), x + pC*(arrow.boundBox.width()/6*2), &p);
										if (stateOpen[2] && p.isSameColor(&arrow.seedColour))
										{
											state[2]++;
										}
										frame->setPixel(y + pS*(arrow.boundBox.width()/6*2), x + pC*(arrow.boundBox.width()/6*2), Colors::pink);
									}
								}

								result.line = line;

								VERBOSE("Sizes: " << std::endl <<
									"0: " << state[0] << std::endl <<
									"1: " << state[1] << std::endl <<
									"2: " << state[2]);

								double ratios[3][3];

								for (unsigned int i = 0; i < 3; i++)
								{
									for (unsigned int j = 0; j < 3; j++)
									{
										if (i == j)
											ratios[i][j] = 1;
										else
										{
											if (state[i] == 0)
												ratios[i][j] = INFINITY;
											else
												ratios[i][j] = static_cast<double>(state[j]) / static_cast<double>(state[i]);
										}
									}
								}

								result.markerCenter.x = object.boundBox.bbCenterX();
								result.markerCenter.y = object.boundBox.bbCenterY();

								VERBOSE(" Left: " << ratios[1][0]);
								VERBOSE("Middle: " << ratios[1][1]);
								VERBOSE("Right: " << ratios[1][2]);
								if (state[1] == 0)
								{
									result.marker = "unknown";
									vision::Text::draw(arrow.center.x, arrow.center.y, "NOT FOUND!", Colors::green, frame);
								}
								else if (isBigger(ratios[1][0]))
								{
									VERBOSE("RIGHT");
									result.marker = "right";
									vision::Text::draw(arrow.center.x, arrow.center.y, "Right!", Colors::green, frame);
								}
								else if (isBigger(ratios[1][2]))
								{
									result.marker = "left";
									VERBOSE("LEFT");
									vision::Text::draw(arrow.center.x, arrow.center.y, "Left!", Colors::green, frame);
								}
								else if (
									(isZero(ratios[1][0]) && isZero(ratios[1][2]))
									|| (isOne(ratios[1][0]) || isOne(ratios[1][2]))
								)
								{
									result.marker = "forward";
									VERBOSE("FORWARD");
									vision::Text::draw(arrow.center.x, arrow.center.y, "Forward!", Colors::green, frame);
								}
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

bool FindMarker::isOne(double ratio)
{
	return (ratio >= 0.8) && (ratio <= 1.2);
}

bool FindMarker::isZero(double ratio)
{
	return (ratio < 0.1);
}

bool FindMarker::isPartial(double ratio)
{
	return (ratio >= 0.1);
}

bool FindMarker::isBigger(double ratio)
{
	return (ratio > 1.5);
}
