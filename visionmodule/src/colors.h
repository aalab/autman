/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 3/31/15.
 */

#ifndef _VISIONMODULE_COLORS_H_
#define _VISIONMODULE_COLORS_H_


#include "../libvideo/pixel.h"

class Colors
{
public:
	static RawPixel black;
	static RawPixel white;
	static RawPixel red;
	static RawPixel blue;
	static RawPixel green;
	static RawPixel cyan;
	static RawPixel pink;
	static RawPixel yellow;

	static unsigned int length;
};


#endif //_VISIONMODULE_COLORS_H_
