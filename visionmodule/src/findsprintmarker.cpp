/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 5/16/15.
 */

#include "findsprintmarker.h"
#include "../libvideo/imageprocessing.h"
#include "colors.h"
#include "compilationdefinitions.h"

#define SPRINT_MARKER_TOLERANCE 0.1
#define SPRINT_MARKER_PROPORTION 20.0/20.0
#define SPRINT_MARKER_TOLERANCE_VALUE SPRINT_MARKER_PROPORTION * SPRINT_MARKER_TOLERANCE

SprintMarker::SprintMarker()
: left()
{ }

bool FindSprintMarker::shapeFits(VisionObject &object)
{
	return (object.boundBox.width() / (float) object.boundBox.height()) < 5.0;
}

bool FindSprintMarker::find(
	FrameBuffer* in, FrameBuffer* out, vector<VisionObject> &leftMarkers, vector<VisionObject> &rightMarkers,
	SprintMarker *last, SprintMarker *result, tsai_camera_parameters &cameraParameters, CameraCalibrationData& calibrationData
)
{
	result->left = nullptr,
	result->right = nullptr;

	float ratio;
	int diff;
	for (VisionObject &lfm : leftMarkers)
	{
		if (shapeFits(lfm))
		{
			for (VisionObject &rfm : rightMarkers)
			{
				if (
					shapeFits(rfm)
					&& ((ratio = (lfm.boundBox.height() / (float) rfm.boundBox.height())) >= 0.9) && (ratio <= 1.1)
					&& ((diff = (lfm.boundBox.bottomRight().x() - rfm.boundBox.topLeft().x())) < 5)
					)
				{
					result->left = &lfm;
					result->right = &rfm;
					result->centerX = result->left->boundBox.bottomRight().x() +
									  (result->left->boundBox.bottomRight().x() -
									   result->right->boundBox.topLeft().x()) / 2.0;
					result->centerY = result->left->boundBox.bbCenterY();
					result->height = (result->left->boundBox.height() + result->right->boundBox.height()) / 2.0;
					VERBOSE("Markers accepted (" << result->centerX << ":" << result->centerY << ")");
					return true;
				}
				else
				{
					VERBOSE("Skipping RM with " << ratio << " ratio AND diff " << (diff));
				}
			}
		}
	}
	return false;
}
