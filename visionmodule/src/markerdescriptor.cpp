#include <iostream>
#include "markerdescriptor.h"

#define MARKER 3

double MarkerDescriptorCircle::isSimilar(IntegralCircle &circle)
{
	double
		tmp,
		result = 0;

	for (auto &angle : this->angles)
	{
		tmp = (circle.extract(angle.first, angle.first+1) > 0.5) ? 255.0 : 0.0;
		cout << "\ttmp: " << tmp << " (" << (circle.extract(angle.first, angle.first+1)) << ") at " << angle.first << " expected: " << angle.second << " got: " << tmp << endl;
		cout << "\terror: " << std::abs(angle.second - tmp)/255.0 / this->angles.size() << endl;
		result += std::abs(angle.second - tmp)/255.0 / this->angles.size();
	}
	return result;
}

double MarkerDescriptor::isSimilar(IntegralCircle &internal, IntegralCircle &external, IntegralCircle &extra1)
{
	 cout << "Internal:" << endl;
	double ei = this->internal.isSimilar(internal)/3.0;
	 cout << "External:" << endl;
	double ee = this->external.isSimilar(external)/3.0;
	 cout << "Extra1:" << endl;
	double ee1 = this->extra1.isSimilar(extra1)/3.0;

	return ei + ee + ee1;
}

double MarkerDescriptor::train(IntegralCircle &internal, IntegralCircle &external, IntegralCircle &extra1)
{
	this->extra1.angles[315] = extra1.extract(315) > 0.5 ? 255.0 : 0.0;
	this->extra1.angles[225] = extra1.extract(225) > 0.5 ? 255.0 : 0.0;

	return
		this->train(this->internal, internal, 5)
		+ this->train(this->external, external, 5);
}

double MarkerDescriptor::train(MarkerDescriptorCircle &descriptor, IntegralCircle &internal, int minSize)
{
	// cout << "MarkerDescriptor::train" << endl;
	double result = 0;
	int
		angle = -1;
	bool
		c = (internal.extract(0, 1)) > 0.5,
		c2;

	// cout << "\tFirst color is (" << internal.extract(0, 1) << ")" << endl;
	for (int i = 0; i < 360; i++)
	{
		c2 = internal.extract(i, i+1) > 0.5;
		// cout << "\tC2 is (" << internal.extract(i, i+1) << ") at " << i << endl;
		if (c != c2) // Different
		{
			// cout << "\tFirst difference at " << i << "º (" << internal.extract(i, i+1) << ")" << endl;
			angle = i;
			i += 361; // In order to return to the same first difference to close the cycle
			c = c2;
			// cout << "\ti = " << i << "º (" << internal.extract(i, i+1) << ") at " << angle << endl;
			for (int j = angle; j < i; j++)
			{
				c2 = internal.extract(j, j+1) > 0.5;
				// cout << "\tC2 is (" << internal.extract(j, j+1) << ") at " << j << endl;
				if (c != c2) // Different
				{
					// cout << "\tDifference at " << j << "º" << endl;
					if ((j - angle) > minSize)
					{
						// cout << "\t" << (j - angle) << " > " << minSize << endl;
						// cout << "\tStored at " << ((angle + (j - angle)/2) + 360) % 360 << "º with " << (c > 0.5 ? 255 : 0) << endl;
						descriptor.angles.insert(std::make_pair(((angle + (j - angle)/2) + 360) % 360, (c > 0.5 ? 255 : 0)));
					}
					angle = j;
					c = c2;
				}
			}
		}
	}
}
