/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#include "control.h"
#include "../globals.h"
#include "../configuration.h"
#include "../compilationdefinitions.h"

#include <boost/array.hpp>
#include <boost/asio.hpp>

using namespace modules;
using boost::asio::ip::udp;

void modules::Control::markerWalkTowards(double markerDistance, double degrees, double forwardDistance)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), Globals::GetGlobals()->configuration->controlModule.hostName,
							   std::to_string(Globals::GetGlobals()->configuration->controlModule.port));
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	udp::socket socket(io_service);
	socket.open(udp::v4());

	stringstream str;
	str << "{ \"name\" : \"enqueue\",";
	str << "\"params\" : ["
	<< "{\"name\": \"walk\",\"distance\" : " << markerDistance << "},"
	<< "{\"name\": \"turn\",\"angle\" : " << degrees << "},"
	<< "{\"name\": \"walk\",\"distance\":" << forwardDistance << "}"
	<< "]}";
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
}

void modules::Control::aut_Movement(double vx, double vy, double vt,int motion,double pan, double tilt)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), "10.10.7.10",
							   std::to_string(1313));
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	cout<<"Start udp sending "<<endl;

	udp::socket socket(io_service);
	socket.open(udp::v4());
// echo '{"name":"Arash","command":"AutGait","params":{"vx":0,"vy":0,"vt":0,"motion":100,"pan":0,"tilt":0.6}}' | nc localhost -u 1313 -q 1
	stringstream str;
	str << "{ \"name\" : \"Arash\",";
	str << "\"command\":";
	str << "\"AutGait\","
	<<"\"params\":"
	<< "{\"vx\":" << vx << ","
	<< "\"vy\":" << vy << ","
	<< "\"vt\":" << vt << ","
	<< "\"motion\":" << motion << ","
	<< "\"pan\":" << pan << ","
	<< "\"tilt\":" << tilt << "}}";

	cout<<"udp done"<<endl;
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
	cout<<"udp: " <<str.str()<<endl;
}



void modules::Control::headScan(double pan, double tilt)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), Globals::GetGlobals()->configuration->controlModule.hostName,
							   std::to_string(Globals::GetGlobals()->configuration->controlModule.port));
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	udp::socket socket(io_service);
	socket.open(udp::v4());

	stringstream str;
	str << "{ \"name\" : \"enqueue\",";
	str << "\"params\" : ["
	<< "{\"name\": \"headhome\"},"
	<< "{\"name\": \"headpan\",\"pan\":" << pan << ", \"duration\": 0.0},"
	<< "{\"name\": \"headtilt\",\"pan\":" << tilt << ", \"duration\": 3.0}"
	<< "]}";
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
}

void modules::Control::stopFollowingLine()
{
	followLine(-1, -1, -1, -1, -1, -1, false);
}

void modules::Control::followLine(double lineTopX, double lineTopY, double lineMiddleX, double lineMiddleY,
								  double lineBottomX, double lineBottomY, bool isLineFound)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), Globals::GetGlobals()->configuration->controlModule.hostName,
							   std::to_string(Globals::GetGlobals()->configuration->controlModule.port));;
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	udp::socket socket(io_service);
	socket.open(udp::v4());

	stringstream str;
	str << "{ \"name\" : \"followline\", \"params\" : { ";
	str << "\"line\" : { ";
	str << "\"top\" : { \"x\": " << lineTopX << ", " << "\"y\": " << lineTopY << " }, ";
	str << "\"middle\": { \"x\": " << lineMiddleX << ", \"y\": " << lineMiddleY << " }, ";
	str << "\"bottom\" : { \"x\" : " << lineBottomX << ", \"y\" : " << lineBottomY << " } }, ";
	str << "\"found\" : " << (isLineFound ? "true" : "false") << "}}";
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);

}

std::chrono::system_clock::time_point lastMessage = std::chrono::system_clock::now();
std::chrono::system_clock::time_point currentTimeMessage;

char lastCommand = 0;

long controlWaitTime = 0;
long controlExtraTime = 1000000;

void modules::Control::followSprint(bool markerFound, double markerCenterX, double markerCenterY, unsigned int markerHeight,
									string mode)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), Globals::GetGlobals()->configuration->controlModule.hostName,
							   std::to_string(Globals::GetGlobals()->configuration->controlModule.port));
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	udp::socket socket(io_service);
	socket.open(udp::v4());

	stringstream str;
	/*
	// str << "Arash Move = OmniWalk Dx=0.03 0.06 0.03 Dy=0 0 0  Dtheta=0 -0.2 0 CycleTimeUSec= 650000 500000 650000 Cycles=4 100 4";
	str << "Arash Move = OmniWalk Dx=0.03 Dy=0 Dtheta=" << dtheta << " CycleTimeUSec=650000 Cycles=1";
	*/

	// double dtheta = (atan2(240.0 - markerCenterY, markerCenterX - 320/2.0) - M_PI_2l);
	/*

	std::cout << "Turn: " << R2D(dtheta) << endl;
	std::cout << "Arash Move = OmniWalk Dx=0.03 Dy=0 Dtheta=" << dtheta << " CycleTimeUSec=650000 Cycles=10" << endl;
	 */
	Globals* globals = Globals::GetGlobals();

#ifdef AUTMAN
	currentTimeMessage = std::chrono::system_clock::now();
	long
		cycleTime = 650000,
		diff = std::chrono::duration_cast<std::chrono::microseconds>(currentTimeMessage - lastMessage).count();

	double
		hCameraAngle = D2R(60),
		dtheta = ((320/2.0 - markerCenterX)*(hCameraAngle/2.0))/(320/2.0),
		stepAngle;

	VERBOSE("DTheta = " << dtheta << " (" << R2D(dtheta) << ")" << endl);

	VERBOSE("Diff: " << diff);

	if (markerFound)
	{
		if (diff > controlWaitTime)
		{
			VERBOSE("DIFF OK!");
			if (std::abs(dtheta) < hCameraAngle / 3.0)
			{
				// Walk forward
				if (lastCommand == 1)
				{
					str << "Arash Move = OmniWalk Dx=0.04 0.1 Dy=0 0 Dtheta=0 -0.7 CycleTimeUSec=" << cycleTime <<
					" " << cycleTime << " Cycles=4 4" << endl;
					controlWaitTime = (8) * cycleTime + controlExtraTime;
				}
				else
				{
					str << "Arash Move = OmniWalk Dx=0.1 Dy=0 Dtheta=-0.07 CycleTimeUSec=" << cycleTime <<
					" Cycles=4" << endl;
					controlWaitTime = 4 * cycleTime + controlExtraTime;
				}
				lastCommand = 0;
			}
			else
			{
				// Angle change
				double a;
				if (dtheta < 0)
				{
					stepAngle = -0.2;
				}
				else
				{
					stepAngle = 0.1;
				}
				unsigned int cycles = static_cast<unsigned int>(std::abs(dtheta / stepAngle));
				controlWaitTime = cycles * cycleTime + controlExtraTime;
				str << "Arash Move = OmniWalk Dx=0.0 Dy=0 Dtheta=" << stepAngle << " CycleTimeUSec=" << cycleTime <<
				" Cycles=" << cycles << endl;
			}
			VERBOSE(str.str() << endl);
	}
	else
		return;
	lastMessage = currentTimeMessage;
		}
#else
	str << "{ \"name\" : \"followsprintmarker\", \"params\" : { ";
	str << "\"mode\" : \"" << mode << "\", \"found\" : " << (markerFound?"true":"false");
	if (markerFound)
		str << ",\"center\":{\"x\":" << markerCenterX << ",\"y\": " << markerCenterY << "}, \"height\": " << markerHeight;
	str << "}}";
#endif
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
}
//TO-DO set function to control vx,vy,va and tilt
void modules::Control::headTrackerMove(bool found, unsigned int x, unsigned int y)
{
	boost::asio::io_service io_service;

	udp::resolver resolver(io_service);
	udp::resolver::query query(udp::v4(), Globals::GetGlobals()->configuration->controlModule.hostName,
							   std::to_string(Globals::GetGlobals()->configuration->controlModule.port));
	udp::endpoint receiver_endpoint = *resolver.resolve(query);

	udp::socket socket(io_service);
	socket.open(udp::v4());

	stringstream str;
	str << "{ \"name\" : \"headtrackermove\", \"params\" : { ";
	str << "\"found\" : " << (found?"true":"false");
	if (found)
		str << ", \"x\":" << x << ",\"y\": " << y << " ";
	str << "}}";
	socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);
}
