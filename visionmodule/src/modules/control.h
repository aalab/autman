/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 12, 2015
 */

#ifndef VISIONMODULE_CONTROL_H
#define VISIONMODULE_CONTROL_H

#include <string>

using namespace std;

namespace modules
{
	class Control
	{
	public:
		static void followLine(double lineTopX, double lineTopY, double lineMiddleX, double lineMiddleY, double lineBottomX,
							   double lineBottomY, bool isLineFound);

		static void stopFollowingLine();

		static void followSprint(bool markerFound, double markerCenterX, double markerCenterY, unsigned int markerHeight,
								 string mode);

		static void markerWalkTowards(double markerDistance, double angle, double forwardDistance);

		static void headTrackerMove(bool found, unsigned int x, unsigned int y);

		void headScan(double pan, double tilt);

		static void aut_Movement(double vx, double vy, double vt,int motion,double pan, double tilt);
	};
}

#endif //VISIONMODULE_CONTROL_H
