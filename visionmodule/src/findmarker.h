#ifndef __FINDMARKER_H__
#define __FINDMARKER_H__

#include <vector>
#include "cameracalibrationdata.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/visionobject.h"
#include "../libvideo/colourdefinition.h"
#include "integralcircle.h"
#include "markerdescriptor.h"
#include "line.h"

class FindMarkerResult
{
public:
	Line2D line;
	std::string marker;
	Point2D markerCenter;
};

class FindMarker
{
public:
	/*
	 * @deprecated
	 */
	static bool find(std::vector<VisionObject> &objects, Point &out, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults);

	static bool find2(
		FrameBuffer *frame, FrameBuffer *outFrame, std::vector<VisionObject> &tapes, std::vector<VisionObject> &markers,
		std::vector<VisionObject> &markersInside, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults,
		FindMarkerResult &result
	);

	static bool isOne(double ratio);


	static bool isZero(double ratio);

	static bool isPartial(double ratio);

	static bool isBigger(double ratio);

	/*
	 * @deprecated
	 */
	static void extractMarker(
		FrameBuffer* in, Point &center, FrameBuffer *out, tsai_camera_parameters &cameraParameters,
		CameraCalibrationData &calibrationResults
	);

	static void calculateIntegralCircle(
		FrameBuffer *frame, FrameBuffer *outFrame, IntegralCircle &circle, double angle, double radius, double centerX,
		double centerY, Pixel &p, tsai_camera_parameters &cameraParameters, CameraCalibrationData &calibrationResults
	);

	static void classifyIntegralCircle(
		IntegralCircle &integralCircleInternal, IntegralCircle &integralCircleExternal, IntegralCircle &extra1,
		MarkerDescriptor **markerDescriptor
	);

	/*
	 * Based in the angle, tries to optimize the line finding a perpendicular line. Them, finds the middle point using
	 * the color match.
	 */
	static void getImageLine(
		double *centerX, double *centerY, unsigned int x, unsigned int y, int offset,
		VisionObject *line, FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
		ColourDefinition *tapeColour, const double angle
	);

	static void getLineInfo(
		FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, unsigned int lineOffset, double *worldLineTopX,
		double *worldLineTopY, double *worldLineMiddleX, double *worldLineMiddleY, double *worldLineBottomX,
		double *worldLineBottomY, double *markerCenterX, double *markerCenterY, double *lineAngle, unsigned int minLength,
		unsigned int subsample, ColourDefinition *tapeColour, ColourDefinition *markerColour,
		CameraCalibrationData& calibrationData, double markerSize
	);

	/*
	 * Using the angle of the line, tries to expand the line towards the marker to find a closer point.
	 */
	static void expandImageLine(
		double *pointX, double *pointY, unsigned int x, unsigned int y, VisionObject &line, FrameBuffer *frame,
		FrameBuffer *outFrame, ColourDefinition &tapeColour, ColourDefinition &markerColour, const double angle,
		unsigned int threshold
	);

	static void GetImageLineTop(
		double *lineTopCenterX, double *lineTopY, unsigned int offset, VisionObject *line, FrameBuffer *frame,
		FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, ColourDefinition *tapeColour
	);

	static void GetImageLineBottom(
		double *lineBottomCenterX, double *lineBottomY, unsigned int offset, VisionObject *line, FrameBuffer *frame,
		FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, ColourDefinition *tapeColour
	);

	static void ClassifyArrow(FrameBuffer *cameraFrame, FrameBuffer *undistortedImage, Point imageTopLeft, Point imageTopRight, Point imageBottomRight, Point imageBottomLeft, int flagDim);

	static void GetUndistortedImage(FrameBuffer *cameraFrame, FrameBuffer *undistortedImage, double imageCx, double imageCy, double worldLineSlope, int fullDim, Point *imFlagTopLeft, Point *imFlagTopRight, Point *imFlagBottomRight, Point *imFlagBottomLeft, bool save);

	static void DrawFlag(FrameBuffer *frame, double flagDim, double worldLineTopX, double worldLineTopY, double worldLineMiddleX, double worldLineMiddleY);

	static bool MarkerExists(FrameBuffer *frame, FrameBuffer *outFrame, unsigned int subsample, ColourDefinition *markerColour, unsigned int markerSizeMin, unsigned int checkForMarkerRadius, double markerCX, double markerCY);
};

#endif