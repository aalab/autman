/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 16, 2015
 */

#ifndef VISIONMODULE_TEXT_H
#define VISIONMODULE_TEXT_H

#include <string>
#include "../libvideo/framebuffer.h"

using namespace std;

namespace vision
{
	class Text
	{
	public:
		static void draw(unsigned int x, unsigned int y, string text, RawPixel &colour, FrameBuffer *buffer);
	};
}



#endif //VISIONMODULE_TEXT_H
