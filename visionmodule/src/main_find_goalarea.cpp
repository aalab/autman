/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "../tsai3d/tsai2D.h"
#include "http/json.h"
#include "findmarker.h"
#include "../libvideo/framebufferrgb565.h"
#include "../libvideo/framebufferrgb32.h"
#include "../libvideo/framebufferbayer.h"
#include "globals.h"
#include "colors.h"
#include "findgoalarea.h"
#include "compilationdefinitions.h"

int main(int argc, char * argv[])
{
/*	Line2D t(0, 0, 100, 0);
	std::cout << "d: " << R2D(t.inclination()) << " = " << 0 << std::endl;
	t.b = Point2D(100, 100);
	std::cout << "d: " << R2D(t.inclination()) << " = " << 45 << std::endl;
	t.b = Point2D(0, 100);
	std::cout << "d: " << R2D(t.inclination()) << " = " << 90 << std::endl;
	t.b = Point2D(-100, 100);
	std::cout << "d: " << R2D(t.inclination()) << " = " << 135 << std::endl;
	t.b = Point2D(-100, 0);
	std::cout << "d: " << R2D(t.inclination()) << " = " << 180 << std::endl;

	return;*/
	if (argc < 3)
	{
		cout << "Invalid params. Usage: " << argv[0] << " <config> <raw_image>" << endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	std::vector<ColourDefinition> &colourDefs = config.colours;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);

	unsigned int subsample = 1;

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	fb->inFromPPM(argv[2]);

	ColourDefinition
		*tape = config.getColour("tape");

	assert(tape != NULL);

	std::vector<VisionObject>
		tapeResults;

	ImageProcessing::SegmentColours(fb, outFb, 255, 5, 20, subsample, *tape, Colors::blue, tapeResults, false, false);

	FindGoalAreaData goalAreaData;
	if (FindGoalArea::find(fb, outFb, tapeResults, goalAreaData, false, 20))
	{
		cout << "Area found." << endl;
		for (Line2D &l : goalAreaData.lines)
		{
			ImageProcessing::drawBresenhamLine(fb, l.a.x, l.a.y, l.b.x, l.b.y, Colors::green);
		}
	}
	else
		cout << "Area not found." << endl;

	fb->outToPPM("result.ppm");
	outFb->outToPPM("resultOut.ppm");

	delete fb;
	delete outFb;
}
