/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#ifndef VISIONMODULE_COLOURDESCRIPTOR_H
#define VISIONMODULE_COLOURDESCRIPTOR_H

#include "../libvideo/colourdefinition.h"
#include "../libvideo/pixel.h"

namespace vision
{
	class ColourSearchParams
	{
	public:
		ColourSearchParams();
		ColourSearchParams(
			unsigned int subsample, unsigned int min, unsigned int max, unsigned int threshold,
			ColourDefinition *colour, RawPixel *mark
		);

		unsigned int subsample;
		unsigned int min;
		unsigned int max;
		unsigned int threshold;
		ColourDefinition *colour;
		RawPixel *mark;
	};
}

#endif //VISIONMODULE_COLOURDESCRIPTOR_H
