/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 20, 2015
 */

#include <chrono>
#include "findball.h"
#include "../libvideo/imageprocessing.h"
#include "text.h"
#include "findgrid.h"

#define DIST M_PIl / 8
#define M_2xPIl (M_PIl + M_PIl)

#define M_2xPIl_FROM (0)
#define M_2xPIl_TO (M_PIl)

// #define M_2xPIl_FROM (M_PIl)
// #define M_2xPIl_TO (M_2xPIl)

bool FindBall::find(
	FrameBuffer *frame, FrameBuffer *outFrame, int subsample, ColourDefinition *ballColourDefinition,
	std::vector<VisionObject> &balls, std::vector<VisionObject*> &results, std::vector<VisionObject> &vectorList,
	std::chrono::system_clock::time_point &last_time, CameraCalibrationData &cameraCalibration
)
{
	double ratio_threshold = 0.6;
	int
		distance_threshold = 2, //if two points are two close then they might be a same object
		min_size_threshold = 30;

	/*
	// Clusterizing method to help segmentForBall.

	std::vector<VisionObject> clusterized;
	FindGrid::clusterizeObjects(results, clusterized, 10);
	for (VisionObject &c : clusterized)
	{
		ImageProcessing::drawRectangle(frame, c.boundBox.topLeft().x()+1, c.boundBox.topLeft().y()+1, c.boundBox.width(), c.boundBox.height(), Colors::green);
	}
	*/

//		cout<<"Results: " << results.size() <<endl;

	//filter is not implement for more then one object
	//highest probablility of ball need to be sorted to the first position of the list of results
	std::string temp = "Socccer Detection: OK";

	if (balls.size() >= 1)
	{
		VERBOSE("max boundbox size: " << balls[0].boundBox.size());
		VERBOSE("max result ratio: " << balls[0].boundBox.ratio());
		if ((balls[0].boundBox.size() > min_size_threshold) && (balls[0].boundBox.ratio() > ratio_threshold))
		{
//			vision::Text::draw(20, 20, to_string(results.size()), Colors::green, frame);
			vision::Text::draw(20, 20, "Soccer Detection: OK", Colors::green, frame);
			// vision::Text::draw(20, 35,this->resultString, Colors::green, frame);
			vision::Text::draw(balls[0].boundBox.bbCenterX(), balls[0].boundBox.bbCenterY(),
							   (to_string(balls[0].center.x) + "," + to_string(balls[0].center.y)),
							   Colors::green, frame);

			if (vectorList.size() == 0)
			{
				last_time = std::chrono::system_clock::now();
				results.push_back(&balls[0]);
				vectorList.emplace(vectorList.begin(), balls[0]);
			}
			else if (vectorList.size() == 1)
			{

				int dX0 = vectorList[0].boundBox.bbCenterX();
				int dY0 = vectorList[0].boundBox.bbCenterY();
				int dX1 = balls[0].boundBox.bbCenterX();
				int dY1 = balls[0].boundBox.bbCenterY();
				int diff = 0;

				double distance = sqrt((dX1 - dX0) * (dX1 - dX0) + (dY1 - dY0) * (dY1 - dY0));
//				vision::Text::draw(20, 35,"distance travel: "+to_string(distance), Colors::green, frame);

				if (distance > distance_threshold)
				{
					diff = std::chrono::duration_cast<std::chrono::milliseconds>(
						std::chrono::system_clock::now() - last_time).count();
					vision::Text::draw(20, 35, "Time diff: " + to_string(diff) + " ms", Colors::green, frame);
					last_time = std::chrono::system_clock::now();

					vectorList.insert(vectorList.begin(), balls[0]);

					double speed = distance / (diff * 0.001);
//					vision::Text::draw(20, 65,"Speed: "+to_string(speed) + " pixel/s", Colors::green, frame);
					// results.push_back(&balls[0]);

					int dispX = dX1 - dX0;
					int dispY = dY1 - dY0;

//					double speed = distance/(diff*0.001);
//					vision::Text::draw(20, 65,"Speed: "+to_string(speed) + " pixel/s", Colors::green, frame);
					double velX = dispX / (diff * 0.001);
//					vision::Text::draw(20, 65,"Velocity X: "+to_string(velX) + " pixel/s", Colors::green, frame);
					int angle = (int) (atan2(dispY, dispX) * 180 / M_PIl);
//					vision::Text::draw(20, 80,"Angle: "+to_string(angle) + " degree", Colors::green, frame);

					tsai_calibration_constants calib_const = cameraCalibration.getResult();

					double wx0, wy0, wx1, wy1;

					image_coord_to_world_coord(dX0, dY0, 0, &wx0, &wy0, &FindGrid::cameraParameters,
											   &cameraCalibration.getResult());
//					cout<< "world x: "<<wx<<"world y: "<<wy<<endl;
//					vision::Text::draw(20, 95,"W_X: "+to_string(wx0) + " W_Y: " + to_string(wy0), Colors::green, frame);

					image_coord_to_world_coord(dX1, dY1, 0, &wx1, &wy1, &FindGrid::cameraParameters,
											   &cameraCalibration.getResult());
					double w_dispX = wx1 - wx0;
					double w_dispY = wy1 - wy0;
					double w_velX = w_dispX / (diff * 0.001);
					double w_velY = w_dispY / (diff * 0.001);
					double w_angle = (int) (atan2(w_dispY, w_dispX) * 180 / M_PIl);

					distance = sqrt((wx1 - wx0) * (wx1 - wx0) + (wy1 - wy0) * (wy1 - wy0));
					vision::Text::draw(20, 35, "Max Size: " + to_string(balls[0].boundBox.size()), Colors::cyan,
									   frame);
					vision::Text::draw(20, 50, "distance travel w: " + to_string(distance), Colors::cyan, frame);
					vision::Text::draw(20, 65, "Velocity X: " + to_string(w_velX) + " cm/s", Colors::cyan, frame);
					vision::Text::draw(20, 80, "Velocity Y: " + to_string(w_velY) + " cm/s", Colors::cyan, frame);
					vision::Text::draw(20, 95, "Angle: " + to_string(w_angle) + " degree", Colors::cyan, frame);
					vision::Text::draw(20, 110, "W_X: " + to_string(wx1) + " W_Y: " + to_string(wy1), Colors::cyan,
									   frame);

				}

//				this->resultString = this->ConvertVectorToString(vector_list);
//				vision::Text::draw(20, 35,this->resultString, Colors::green, frame);
//				cout<<this->resultString<<endl;
			}
			else if (vectorList.size() == 2)
			{
				//assume soccer toward robot is positive y-axis
				//assume to the right is positive x-axis

				int dX0 = vectorList[0].boundBox.bbCenterX();
				int dY0 = vectorList[0].boundBox.bbCenterY();
				int dX1 = balls[0].boundBox.bbCenterX();
				int dY1 = balls[0].boundBox.bbCenterY();
				int diff = 0;

				double distance = sqrt((dX1 - dX0) * (dX1 - dX0) + (dY1 - dY0) * (dY1 - dY0));
//				vision::Text::draw(20, 35,"distance travel: "+to_string(distance), Colors::green, frame);

				if (distance > distance_threshold)
				{
					diff = std::chrono::duration_cast<std::chrono::milliseconds>(
						std::chrono::system_clock::now() - last_time).count();
//					vision::Text::draw(20, 50,"Time diff: "+to_string(diff) + " ms", Colors::green, frame);
					last_time = std::chrono::system_clock::now();

					vectorList.pop_back();
					vectorList.insert(vectorList.begin(), balls[0]);

					int dispX = dX1 - dX0;
					int dispY = dY1 - dY0;

//					double speed = distance/(diff*0.001);
//					vision::Text::draw(20, 65,"Speed: "+to_string(speed) + " pixel/s", Colors::green, frame);
					double velX = dispX / (diff * 0.001);
//					vision::Text::draw(20, 65,"Velocity X: "+to_string(velX) + " pixel/s", Colors::green, frame);
					if (dispX != 0)
					{
						int angle = (int) (atan(dispY / dispX) * 180 / M_PIl);
//						vision::Text::draw(20, 80,"Angle: "+to_string(angle) + " degree", Colors::green, frame);
					}


					//cam calib
					tsai_calibration_constants calib_const = cameraCalibration.getResult();

					double wx0, wy0, wx1, wy1;

					image_coord_to_world_coord(dX0, dY0, 0, &wx0, &wy0, &FindGrid::cameraParameters,
											   &cameraCalibration.getResult());
//					cout<< "world x: "<<wx<<"world y: "<<wy<<endl;
//					vision::Text::draw(20, 95,"W_X: "+to_string(wx) + " W_Y" + to_string(wy), Colors::green, frame);

					image_coord_to_world_coord(dX1, dY1, 0, &wx1, &wy1, &FindGrid::cameraParameters,
											   &cameraCalibration.getResult());
					double w_dispX = wx1 - wx0;
					double w_dispY = wy1 - wy0;
					double w_velX = w_dispX / (diff * 0.001);
					double w_velY = w_dispY / (diff * 0.001);
					double w_angle = (int) (atan(w_dispY / w_dispX) * 180 / M_PIl);

					distance = sqrt((wx1 - wx0) * (wx1 - wx0) + (wy1 - wy0) * (wy1 - wy0));
					// vision::Text::draw(20, 35, "Max Size: " + to_string(results[0].boundBox.size()), Colors::cyan, frame);
					vision::Text::draw(20, 50, "distance travel w: " + to_string(distance), Colors::cyan, frame);
					vision::Text::draw(20, 65, "Velocity X: " + to_string(w_velX) + " cm/s", Colors::cyan, frame);
					vision::Text::draw(20, 80, "Velocity Y: " + to_string(w_velY) + " cm/s", Colors::cyan, frame);
					vision::Text::draw(20, 95, "Angle: " + to_string(w_angle) + " degree", Colors::cyan, frame);
					vision::Text::draw(20, 110, "W_X: " + to_string(wx1) + " W_Y: " + to_string(wy1), Colors::cyan,
									   frame);
					//center point of the camera.
					int ccx = frame->width / 2;
					int ccy = frame->height / 2;
					double w_ccx, w_ccy;

					image_coord_to_world_coord(ccx, ccy, 0, &w_ccx, &w_ccy, &FindGrid::cameraParameters,
											   &cameraCalibration.getResult());
					//send commend to fix the neck
					//thsi code may not be in this node
					//if object is in front of the robot
					//size big enough kick
					//size size smaller block
					//if object is not too far away from the robot
					//positioning

					int diff_center = frame->width / 2 - dX1;
					int chase_bound = 1;
					vision::Text::draw(20, 35, "DIff center: " + to_string(diff_center), Colors::cyan, frame);

					//

					/*
					if(abs(diff_center)>chase_bound){
						double p_vy= 0.3;
						double vx =0.0;
						double vy =0.35; //max is 0.5
						double vt =0;
						int motion = 100;
						double pan = 0;
						double tilt = 0.6;
						if(diff_center<0){
							vy = -1*vy;
						}
						// double vy = p_vy*vy;
						// modules::Control::aut_Movement( vx, vy, vt, motion, pan, tilt);

					}else
					{
						modules::Control::aut_Movement( 0.0, 0.0,0.0 , 100, 0, 0.6);
					}
					 */


					//if object is far away
					//ignore
					//if object is moving
					//go to projected position need 3 points!!
					//m = tan(theta),

				}


//				this->resultString = this->ConvertVectorToString(vector_list);
//				vision::Text::draw(20, 35,this->resultString, Colors::green, frame);
//				cout<<this->resultString<<endl;
			}
		}//didn't tidy the code
	}
}

bool FindBall::find2(
	FrameBuffer *frame, FrameBuffer *callOutFrame, FrameBuffer *grassOutFrame, ColourDefinition *tape, FrameBuffer *tapeOutFrame,
	ColourDefinition *ball, FrameBuffer *ballOutFrame, std::vector<VisionObject> &grass,
	std::vector<VisionObject> &tapes, std::vector<VisionObject> &balls
)
{

	unsigned int subsample = 1;
	RawPixel pixel, pxGrass, pxTape, pxBall;
	Rect r(0, 0, ballOutFrame->width, ballOutFrame->height);
	ballOutFrame->fill(r, Colors::black);

	for (unsigned int x = 0; x < frame->width; x++)
	{
		for (unsigned int y = 0; y < frame->height; y++)
		{
			tapeOutFrame->getPixel(y, x, &pxTape);
			grassOutFrame->getPixel(y, x, &pxGrass);
			if (pxTape.isSameColor(&Colors::black) && pxGrass.isSameColor(&Colors::black))
			{
				frame->getPixel(y, x, &pxBall);
				//if (ball->isMatch(pxBall))
				ballOutFrame->setPixel(y, x, Colors::white);
			}
		}
	}

	std::vector<VisionObject> calResults;
	// grassOutFrame->fill(Colors::black);

	ImageProcessing::SegmentColours(frame, callOutFrame, 255, 5, 20, subsample, *ball, Colors::pink,
		calResults, false, false);
	double a, c, s, radius, tmpX, tmpY;
	RawPixel colourIn, colourOut;
	unsigned int isBall;
	unsigned int i = 0;
	for (VisionObject &ball : calResults)
	{
		i++;

		vision::Text::draw(ball.boundBox.bbCenterX(), ball.boundBox.bbCenterY(), std::to_string(i), Colors::red, ballOutFrame);
		isBall = 0;
		if (std::abs(1.0 - (ball.boundBox.width() / (float)ball.boundBox.height())) > 0.1)
			continue;

		vision::Text::draw(ball.boundBox.bbCenterX(), ball.boundBox.bbCenterY(), std::to_string(i), Colors::green, callOutFrame);
		radius = (ball.boundBox.width() + ball.boundBox.height())/4.0;
		for (a = M_2xPIl_FROM; a < M_2xPIl_TO; a += DIST)
		{
			c = std::cos(a);
			s = std::sin(a);

			tmpY = ball.boundBox.bbCenterY()+s*(radius+(radius+3));
			tmpX = ball.boundBox.bbCenterX()+c*(radius+3);
			if (r.inBound(tmpX, tmpY))
			{
				ballOutFrame->getPixel(ball.boundBox.bbCenterY() + s * (radius - (radius - 3)),
									   ball.boundBox.bbCenterX() + c * (radius - 3), &colourIn);    // 5% inside
				ballOutFrame->getPixel(ball.boundBox.bbCenterY() + s * (radius + (radius + 3)),
									   ball.boundBox.bbCenterX() + c * (radius + 3), &colourOut);    // 5% outside

				ImageProcessing::drawCross(ballOutFrame, ball.boundBox.bbCenterX() + c * (radius - 3),
										   ball.boundBox.bbCenterY() + s * (radius - 3), Colors::yellow, 3);
				ImageProcessing::drawCross(ballOutFrame, ball.boundBox.bbCenterX() + c * (radius + 3),
										   ball.boundBox.bbCenterY() + s * (radius + 3), Colors::red, 3);

				if (colourIn.isSameColor(&Colors::white) && colourOut.isSameColor(&Colors::black))
				{
					isBall++;
				}
			}
			else
				isBall++;
		}
		if (isBall >= 5)
		{
			ImageProcessing::drawRectangle(ballOutFrame, ball.boundBox, Colors::cyan);
			balls.push_back(ball);
			VERBOSE("Ball FOUND!!");
		}
		else
		{
			VERBOSE("Ball rejected with " << isBall << " errors");
		}
	}
	/*
	if (balls.empty())
	{
		ImageProcessing::SegmentColours(frame, grassOutFrame, 255, 5, 30, subsample, *ball, Colors::pink, balls, false, false);
	}
	 */
	// ImageProcessing::convertBuffer(ballOutFrame, frame, 1);
	return !balls.empty();
}
