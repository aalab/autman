#ifndef __TRAPEZOID_H__
#define __TRAPEZOID_H__

#include "../libvideo/point.h"

#define SW 0
#define SE 1
#define NE 2
#define NW 3

class Trapezoid
{
public:
	Trapezoid();

	Point coords[4];

	inline Point* sw() { return &this->coords[SW]; }
	inline Point* se() { return &this->coords[SE]; }
	inline Point* ne() { return &this->coords[NE]; }
	inline Point* nw() { return &this->coords[NW]; }

	bool point(unsigned int row, unsigned int column, Point &p, unsigned int rows, unsigned int cols);

	static int line2(unsigned int a, unsigned int b, unsigned int count, unsigned int index);

	static int line(unsigned int a, unsigned int b, unsigned int count, unsigned int index);
};

#endif