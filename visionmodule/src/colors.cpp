/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 31, 2015
 */

#include "colors.h"

RawPixel Colors::black(0, 0, 0);
RawPixel Colors::white(255, 255, 255);
RawPixel Colors::red(255, 0, 0);
RawPixel Colors::green(0, 255, 0);
RawPixel Colors::blue(0, 0, 255);
RawPixel Colors::cyan(0, 255, 255);
RawPixel Colors::pink(255, 0, 255);
RawPixel Colors::yellow(255, 255, 0);

unsigned int Colors::length = 8;
