#ifndef __CAMERACALIBRATIONDATA_H__
#define __CAMERACALIBRATIONDATA_H__

#include <cstring>
#include <string>

#include <tsai2d/tsai2D.h>
#include "../libvideo/point.h"

class CameraCalibrationParams
{
public:
	int rows;
	int columns;
	Point2D origin;
	Point2D distance;
	unsigned int clusteringThreshold;
	unsigned int brightnessThreshold;
};

class CameraCalibrationData
{
private:
	tsai_calibration_constants result;
public:
	std::string name;
	CameraCalibrationParams params;
	inline tsai_calibration_constants& getResult() { return this->result; };
};

#endif