#ifndef __FINDGRID_H__
#define __FINDGRID_H__

#include <vector>

#include <tsai2d/tsai2D.h>

#include "../libvideo/visionobject.h"
#include "cameracalibrationdata.h"
#include "trapezoid.h"
#include "../libvideo/framebuffer.h"

class FindGrid
{
private:
public:
	static struct tsai_camera_parameters cameraParameters;
	static struct tsai_camera_parameters& getCameraParameters();

	static VisionObject* closest(unsigned int x, unsigned int y, std::vector<VisionObject> &objects);
	static VisionObject* closest(unsigned int x, unsigned int y, std::vector<VisionObject*> &objects);

	static void clusterizeObjects(std::vector<VisionObject> &src, std::vector<VisionObject> &dest,
		unsigned int threshold);

	static bool find(FrameBuffer *in, FrameBuffer *out, Trapezoid &trapezoid, std::vector<VisionObject> &results, std::vector<VisionObject *> &grid,
		unsigned int rowCount, unsigned int colCount);

	static bool calibrate(
		FrameBuffer* frame, FrameBuffer* outFrame, std::vector<VisionObject*> grid, CameraCalibrationData &cameraCalibration,
		struct tsai_calibration_constants &calibrationResults
	);
};


#endif
