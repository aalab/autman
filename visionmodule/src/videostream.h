/*
 * videostream.h
 *
 */

#ifndef __VIDEOSTREAM_H__
#define __VIDEOSTREAM_H__

#include <string>
#include <vector>
#include <mutex>
#include <boost/program_options.hpp>
#include <pthread.h>
#include <chrono>

#include "../libvideo/visionobject.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/integralimage.h"
#include "goits/stage.h"
#include "findobstacles.h"
#include "findmarker.h"

using namespace std;
namespace po = boost::program_options;

class VideoDevice;

class VideoStream
{
public:

	enum ProcessType
	{
		GoalArea,
		Raw,
		ShowColours,
		SegmentColours,
		Scanlines,
		Segmentation,
		CalibrationProcessing,
		FindMarkers,
		FollowLine,
		Marathon,
		Sprint,
		HeadTracking,
		GoitsTest,
		GoalKeeper,
		ObstacleRun,
        SoccerBallTracker
	};

	enum MarathonMode
	{
		Marathon_FollowLine,				// follow the line
		Marathon_ScanForLine,				// looking for lost line
		Marathon_CheckForLine,				// if line found, start following
		Marathon_WaitToReceive,				// sent actions and waiting for response

		Marathon_ScanForLine_Head1,			// Sends head to position 1
		Marathon_ScanForLine_Head1_Scan,
	};

	enum VideoControl
	{
		IllegalControl,
		Brightness,
		Hue,
		Saturation,
		Contrast,
		Sharpness,
		Gain
	};

	VideoStream(
		string driver, string name, string input, string standard, unsigned int fps, unsigned int width,
		unsigned int height, unsigned int depth, unsigned int numBuffers, unsigned int subsample, int brightness,
		int contrast, int saturation, int sharpness, int gain
	);

	virtual ~VideoStream();

	static void *server_thread(void *arg);

	void run();

	static void run_trampoline(VideoStream *vs);

	int input_init();

	//int input_cmd(in_cmd_type cmd, int value);
	int sendImage(FrameBuffer *img);

	string sprintMode;

	//int output_init();
	//int output_run();

	void setDone(bool done);

	bool getDone(void) const;

	//  pthread_t               cam;
	pthread_mutex_t controls_mutex;

	// IMPORTANT! NOT THREAD SAFE!
	std::chrono::system_clock::time_point
		currentProcessFrameClock, priorProcessFrameClock;
	std::chrono::duration<double> processFrameClockDuration;


	void ProcessFrame(enum ProcessType ptype,
		FrameBuffer *frame,
		FrameBuffer *outFrame,
		unsigned int subsample,
		std::vector<ColourDefinition> colours,
		RawPixel mark);

	std::vector<VisionObject> results;
	std::string resultString;
	std::vector<VisionObject> vectorList;
	std::chrono::system_clock::time_point lastTime;

public:
	VideoDevice *device;
	FrameBuffer *frame;
	vision::goits::Stage stage;

private:
	FrameBuffer *findMarkerFrame;
	struct timeval prev;
	bool done;
	bool isActionQComplete;

public:
	pthread_t threadID;

public:
	static struct Command const Commands[];

	volatile enum MarathonMode marathonMode; // initialized to follow line
private:
	volatile enum ProcessType mode;

public:
	void UpdateColour(ColourDefinition const colour);

public:
	enum ProcessType GetMode(void) const;

	void SetMode(enum ProcessType mode);

public:
	void SetColours(std::vector<ColourDefinition> colours);

private:
	// std::vector<ColourDefinition> colours;

public:
	std::vector<ColourDefinition> *nextColours;

private:
	unsigned int subsample;

public:
	unsigned int GetSubsample(void) const;

	void SetSubsample(unsigned int subsample);

public:
	std::string ReadRunningConfiguration(void);

private:
	po::options_description configOptions;

	void SendActionsToControlHeadScan(double pan, double tilt);

	void buildResultString();
	void buildResultString(std::vector<ObstacleVisionObject> &objects);
	void buildResultString(FindMarkerResult &result);
public:
	std::mutex frameMutex;
	FrameBuffer *findBall2TapeFrame;
	FrameBuffer *findBall2BallFrame;
	FrameBuffer *findBall2CalFrame;
};

#endif /* __VIDEOSTREAM_H__ */
