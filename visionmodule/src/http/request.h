/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 21, 2015.
 */

#ifndef _VISIONMODULE_REQUEST_H_
#define _VISIONMODULE_REQUEST_H_

// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <boost/asio/streambuf.hpp>
#include <regex>
#include <string>
#include <vector>
#include <istream>
#include <boost/regex.hpp>

using namespace std;

namespace http
{

	typedef pair<string, string> Header;

	/// A request received from a client.
	class Request
	{
	private:
		vector<Header> headers;

	public:
		stringstream content;

		string method;
		string path;
		string version;

		boost::smatch path_match;
	public:
		Request();
		~Request();

		bool hasHeader(string header);
		void header(string header, string value);
		string header(string header);
	};
} // namespace http


#endif //_VISIONMODULE_REQUEST_H_
