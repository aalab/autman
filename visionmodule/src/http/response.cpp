/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 21, 2015.
 */

#include "response.h"

#include <unistd.h>
#include <ostream>
#include <streambuf>

#include <json/writer.h>

using namespace http;
using namespace std;

fdoutbuf::fdoutbuf(int _fd) : fd(_fd)
{ }

fdoutbuf::int_type fdoutbuf::overflow(fdoutbuf::int_type c)
{
	if (c != EOF)
	{
		char z = c;
		if (write(fd, &z, 1) != 1)
			return EOF;
	}
	return c;
}

std::streamsize fdoutbuf::xsputn(const char *s, std::streamsize num)
{
	struct pollfd pfd = {
		fd: this->fd,
		events: POLLERR
	};
	if (poll(&pfd, 1, 5) >= 0)
	{
		if (pfd.revents & POLLERR)
		{
			cout << "pipe is broken" << endl;
		}
		else
			return write(fd, s, num);
	}
}

Response::Response(int fd)
	: socketbuf(fd), socketStream(&socketbuf)
{
}

void Response::flush()
{
	this->socketStream.flush();
}

string Result::endh("\r\n");

Result::Result(ResultStatus status)
	: status(status), noContentLength(false)
{ }

Result::Result()
	: status(ResultStatus::ok)
{ }

Result::Result(ResultStatus status, string content)
	: status(status)
{
	this->stream << content;
}

Result::Result(string contentType, string content)
	: status(ResultStatus::ok), contentType(contentType), noContentLength(false)
{
	this->stream << content;
}

Result::Result(string content)
	: status(ResultStatus::ok)
{
	this->stream << content;
}

void Result::writeTo(ostream &s)
{
	if (this->status == 200)
		s << "HTTP/1.1 200 OK" << endh;
	else if (this->status == 400)
		s << "HTTP/1.1 400 Bad Request" << endh;
	else if (this->status == 500)
	{
		s << "HTTP/1.1 500 Internal Server Error" << endh;
	}

	this->header("Server", "Vision Module Server");
	if (!this->contentType.empty())
		this->header("Content-Type", this->contentType);
	if (!this->noContentLength)
	{
		this->stream.seekp(0, ios::end);
		this->header("Content-Length", to_string(this->stream.tellp()));
	}

	for (auto header : this->headers)
		s << header.first << ": " << header.second << endh;

	s << endh;

	s << this->stream.rdbuf();
}

void Result::header(string header, string value)
{
	this->headers.emplace_back(header, value);
}

/*

template<class T> Result &Result::operator<<(const T &t)
{
	this->stream << t;
	return *this;
}

Result &Result::operator<<(std::ostream &(*manip)(std::ostream &))
{
	stream << manip;
	return *this;
}

Result &Result::operator<<(Result &(*manip)(Result &))
{
	return manip(*this);
}
*/
void Result::writeTo(Response &response)
{
	this->writeTo(response.socketStream);
}

Result& Result::operator<<(const Json::Value &json)
{
	this->stream << json;
	this->contentType = "application/json";
}
