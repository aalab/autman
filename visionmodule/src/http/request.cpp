
/*
 * @author J. Santos <jamillo@gmail.com>
 * @date March 21, 2015
 */

#include <iostream>
#include "request.h"

using namespace http;


Request::Request()
{ }

Request::~Request()
{ }

bool Request::hasHeader(string header)
{
	for (Header &h : this->headers)
	{
		if (h.first == header)
			return true;
	}
	return false;
}

void Request::header(string name, string value)
{
	this->headers.emplace_back(name, value);
}

string Request::header(string header)
{
	for (Header &h : this->headers)
	{
		// cout << h.first << " = " << h.second << endl;
		if (h.first == header)
			return h.second;
	}
	return "";
}
