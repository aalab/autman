/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date March 21, 2015.
 */

#ifndef _VISIONMODULE_RESULT_H_
#define _VISIONMODULE_RESULT_H_

#include <iostream>
#include <streambuf>
#include <cstdio>
#include <string>
#include <vector>
#include <ostream>
#include <boost/asio/streambuf.hpp>
#include <sstream>
#include <sys/poll.h>
#include <json/value.h>

using namespace std;

namespace http
{
	class fdoutbuf
		: public std::streambuf
	{
	protected:
		int fd;    // file descriptor
	public:
		fdoutbuf(int _fd);

	protected:

		virtual int_type overflow(int_type c);

		std::streamsize xsputn(const char *s, std::streamsize num);
	};

	class Response
	{
	private:
		fdoutbuf socketbuf;
	public:
		ostream socketStream;
	public:
		Response(int fd);

		void flush();
	};

	enum ResultStatus
	{
		ok = 200,
		badRequest = 400,
		notFound = 404,
		internalServerError = 500
	};

	class Result
	{
	private:
		static string endh;

		vector<pair<string, string> > headers;
	public:
		stringstream stream;
	public:
		Result();
		Result(ResultStatus status);
		Result(ResultStatus status, string content);
		Result(string content);
		Result(string contentType, string content);

		ResultStatus status;
		string contentType;

		bool noContentLength;

		void header(string header, string value);

		void writeTo(ostream &s);
		void writeTo(Response &response);

		Result& operator<<(const Json::Value &json);
	};
}

#endif //_VISIONMODULE_RESULT_H_
