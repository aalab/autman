#ifndef __HTTP__PTREE_H__
#define __HTTP__PTREE_H__

#include <json/value.h>
#include <json/writer.h>

#include <tsai2d/tsai2D.h>

#include "../configuration.h"
#include "../../libvideo/point.h"
#include "../../libvideo/colourdefinition.h"
#include "../markerdescriptor.h"
#include "../trapezoid.h"
#include "../line.h"

Json::Value& operator<<(Json::Value &result, Configuration &conf);

Configuration& operator<<(Configuration &result, Json::Value& json);

Json::Value &operator<<(Json::Value &json, config::Server &server);

Configuration& operator<<(Configuration &result, Json::Value& json);

Json::Value& operator<<(Json::Value &result, ConfigurationFindMarker &confMarker);

ConfigurationFindMarker& operator<<(ConfigurationFindMarker &result, Json::Value& json);

Json::Value &operator<<(Json::Value &result, ColourDefinition &colour);

ColourDefinition& operator<<(ColourDefinition &colour, Json::Value &json);

Json::Value &operator<<(Json::Value &result, Point &t);

Point &operator<<(Point &p, Json::Value &json);

Trapezoid& operator<<(Trapezoid &trapezoid, Json::Value &pt);

Json::Value& operator<<(Json::Value &result, tsai_calibration_constants &calibrationConstants);

tsai_calibration_constants& operator<<(tsai_calibration_constants &result, Json::Value &json);

Json::Value& operator<<(Json::Value &result, CameraCalibrationData &calibrationData);

CameraCalibrationData& operator<<(CameraCalibrationData &result, Json::Value &json);

CameraCalibrationParams& operator<<(CameraCalibrationParams &result, Json::Value &json);

Json::Value& operator<<(Json::Value &json, CameraCalibrationParams &result);

tsai_camera_parameters& operator<<(tsai_camera_parameters &result, Json::Value &json);

Json::Value& operator<<(Json::Value &result, tsai_camera_parameters &cameraParameters);

MarkerDescriptor& operator<<(MarkerDescriptor &result, Json::Value &json);

Json::Value& operator<<(Json::Value &result, MarkerDescriptor &descriptor);

MarkerDescriptorCircle &operator<<(MarkerDescriptorCircle& result, Json::Value &json);

MarkerDescriptor &operator<<(MarkerDescriptor& result, Json::Value &json);

Json::Value &operator<<(Json::Value &result, MarkerDescriptorCircle &circle);

Json::Value &operator<<(Json::Value &result, MarkerDescriptor &markerDescriptor);

#endif