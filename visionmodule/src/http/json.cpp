
#include "json.h"

#include "../serialization/json.h"
#include "../compilationdefinitions.h"

Json::Value& operator<<(Json::Value&result, Configuration &conf)
{
	result["subsample"] = conf.subsample;
	result["udp_port"] = conf.udp_port;

	result["serial"]["device"] = conf.deviceVideo;
	result["serial"]["name"] = conf.deviceSerial;
	result["serial"]["baudrate"] = conf.baudrate;

	result["camera"]["width"] = conf.width;
	result["camera"]["height"] = conf.height;
	result["camera"]["depth"] = conf.depth;
	result["camera"]["brightness"] = conf.brightness;
	result["camera"]["contrast"] = conf.contrast;
	result["camera"]["saturation"] = conf.saturation;
	result["camera"]["sharpness"] = conf.sharpness;
	result["camera"]["gain"] = conf.gain;
	result["camera"]["flip"] = conf.flipImage;

	result["server"]["port"] = conf.httpPort;
	result["server"]["addr"] = conf.httpAddr;
	result["server"]["docroot"] = conf.docroot;
	result["server"]["index"] = conf.index;
	result["server"]["threads"] = conf.httpThreads;

	result["controlModule"] << conf.controlModule;

	for (auto& colour : conf.colours)
	{
		Json::Value jsonColour;
		jsonColour << colour;
		result["colours"].append(jsonColour);
	}

	for (auto& marker : conf.markers)
	{
		Json::Value jsonMarker;
		jsonMarker << marker;
		result["markers"].append(jsonMarker);
	}

	if (conf.cameraCalibrations.empty())
	{
		result["cameraCalibrations"] = Json::arrayValue;
	}
	else
	{
		for (auto &cc : conf.cameraCalibrations)
		{
			Json::Value jsonCC;
			jsonCC << cc;
			result["cameraCalibrations"].append(jsonCC);
		}
	}

	result["marker"] << conf.marker;

	return result;
}

Configuration& operator<<(Configuration &result, Json::Value& json)
{
	result.subsample = json["subsample"].asUInt();
	result.udp_port = json["udp_port"].asUInt();

	result.deviceVideo = json["serial"]["device"].asString();
	result.deviceSerial = json["serial"]["name"].asString();
	result.baudrate = json["serial"]["baudrate"].asString();

	result.width = json["camera"]["width"].asUInt();
	result.height = json["camera"]["height"].asUInt();
	result.depth = json["camera"]["depth"].asUInt();
	result.brightness = json["camera"]["brightness"].asInt();
	result.contrast = json["camera"]["contrast"].asInt();
	result.saturation = json["camera"]["saturation"].asInt();
	result.sharpness = json["camera"]["sharpness"].asInt();
	result.gain = json["camera"]["gain"].asInt();
	result.flipImage = json["camera"]["flip"].asBool();

	if (result.flipImage)
		cout << "FLIPPED" << endl;
	else
		cout << "NON FLIPPED" << endl;

	result.httpPort = json["server"]["port"].asUInt();
	result.httpThreads = json["server"]["threads"].asUInt();
	result.httpAddr = json["server"]["addr"].asString();
	result.docroot = json["server"]["docroot"].asString();
	result.index = json["server"]["index"].asString();

	if (json.isObject())
		result.controlModule << json["controlModule"];

	result.colours.clear();
	if (json["colours"].isArray())
	{
		for (auto& jsonColour : json["colours"])
		{
			result.colours.emplace_back();
			result.colours.back() << jsonColour;
		}
	}
	if (!result.colours.empty())
		result.currentColour = &result.colours[0];

	result.markers.clear();
	if (json["markers"].isArray())
	{
		for (auto& jsonMarker : json["markers"])
		{
			result.markers.emplace_back();
			result.markers.back() << jsonMarker;
		}
	}

	result.cameraCalibrations.clear();
	if (json["cameraCalibrations"].isArray())
	{
		for (auto& jsonCC : json["cameraCalibrations"])
		{
			result.cameraCalibrations.emplace_back();
			result.cameraCalibrations.back() << jsonCC;
		}
	}
	if (result.cameraCalibrations.size() > 0)
		result.currentCameraCalibration = &result.cameraCalibrations[0];
	else
		result.currentCameraCalibration = nullptr;

	result.marker << json["marker"];

	Json::Value &services = json["services"];
	std::string memberName;
	for (Json::ValueIterator member = services.begin(); member != services.end(); ++member)
	{
		memberName = member.name();
		if (memberName == "teleop")
		{
			VERBOSE("Creating teleop service ...");
			result.teleop.reset(new config::Server());
			*result.teleop << *member;
		}
		else if (memberName == "robot")
		{
			VERBOSE("Creating robot service ...");
			result.robot.reset(new config::Server());
			*result.robot << *member;
		}
	}

	return result;
}

Json::Value& operator<<(Json::Value &result, ConfigurationFindMarker &confMarker)
{
	result["angle"] = confMarker.angle;
	result["internalRadius"] = confMarker.internalRadius;
	result["externalRadius"] = confMarker.externalRadius;
	result["extra1Radius"] = confMarker.extra1Radius;
	result["size"] = confMarker.size;
	result["lineOffset"] = confMarker.lineOffset;
	return result;
}

ConfigurationFindMarker& operator<<(ConfigurationFindMarker &result, Json::Value& json)
{
	result.angle = json["angle"].asDouble();
	result.internalRadius = json["internalRadius"].asDouble();
	result.externalRadius = json["externalRadius"].asDouble();
	result.extra1Radius = json["extra1Radius"].asDouble();
	result.size = json["size"].asDouble();
	result.lineOffset = json["lineOffset"].asInt();
	return result;
}

Json::Value& operator<<(Json::Value &result, ColourDefinition &colour)
{
	result["name"] = colour.name;

	result["min"]["red"] = colour.min.red;
	result["min"]["green"] = colour.min.green;
	result["min"]["blue"] = colour.min.blue;
	result["min"]["red_green"] = colour.min.red_green;
	result["min"]["red_blue"] = colour.min.red_blue;
	result["min"]["green_blue"] = colour.min.green_blue;
	result["min"]["red_ratio"] = colour.min.red_ratio;
	result["min"]["green_ratio"] = colour.min.green_ratio;
	result["min"]["blue_ratio"] = colour.min.blue_ratio;

	result["max"]["red"] = colour.max.red;
	result["max"]["green"] = colour.max.green;
	result["max"]["blue"] = colour.max.blue;
	result["max"]["red_green"] = colour.max.red_green;
	result["max"]["red_blue"] = colour.max.red_blue;
	result["max"]["green_blue"] = colour.max.green_blue;
	result["max"]["red_ratio"] = colour.max.red_ratio;
	result["max"]["green_ratio"] = colour.max.green_ratio;
	result["max"]["blue_ratio"] = colour.max.blue_ratio;

	return result;
}

ColourDefinition& operator<<(ColourDefinition &colour, Json::Value &json)
{
	colour.name = json["name"].asString();

	colour.min.red = json["min"]["red"].asUInt();
	colour.min.green = json["min"]["green"].asUInt();
	colour.min.blue = json["min"]["blue"].asUInt();
	colour.min.red_green = json["min"]["red_green"].asInt();
	colour.min.red_blue = json["min"]["red_blue"].asInt();
	colour.min.green_blue = json["min"]["green_blue"].asInt();

	colour.min.red_ratio = 0;
	colour.min.green_ratio = 0;
	colour.min.blue_ratio = 0;

	colour.max.red = json["max"]["red"].asUInt();
	colour.max.green = json["max"]["green"].asUInt();
	colour.max.blue = json["max"]["blue"].asUInt();
	colour.max.red_green = json["max"]["red_green"].asInt();
	colour.max.red_blue = json["max"]["red_blue"].asInt();
	colour.max.green_blue = json["max"]["green_blue"].asInt();

	colour.max.red_ratio = 255;
	colour.max.green_ratio = 255;
	colour.max.blue_ratio = 255;

//	colour.max.red_ratio = json["max"]["red_ratio"].asUInt();
//	colour.max.green_ratio = json["max"]["green_ratio"].asUInt();
//	colour.max.blue_ratio = json["max"]["blue_ratio"].asUInt();

	return colour;
}

Json::Value& operator<<(Json::Value &result, Point &p)
{
	result["x"] = p.x();
	result["y"] = p.y();

	return result;
}

Point &operator<<(Point &p, Json::Value &json)
{
	p.setX(json["x"].asUInt());
	p.setY(json["y"].asUInt());

	return p;
}

Trapezoid& operator<<(Trapezoid &trapezoid, Json::Value &pt)
{
	*trapezoid.sw() << pt[0];
	*trapezoid.se() << pt[1];
	*trapezoid.ne() << pt[2];
	*trapezoid.nw() << pt[3];
	return trapezoid;
}

Json::Value& operator<<(Json::Value &result, tsai_calibration_constants &calibrationConstants)
{
	result["f"] = calibrationConstants.f;
	result["kappa1"] = calibrationConstants.kappa1;

	result["p1"] = calibrationConstants.p1;
	result["p2"] = calibrationConstants.p2;

	result["Tx"] = calibrationConstants.Tx;
	result["Ty"] = calibrationConstants.Ty;
	result["Tz"] = calibrationConstants.Tz;

	result["Rx"] = calibrationConstants.Rx;
	result["Ry"] = calibrationConstants.Ry;
	result["Rz"] = calibrationConstants.Rz;

	result["r1"] = calibrationConstants.r1;
	result["r2"] = calibrationConstants.r2;
	result["r3"] = calibrationConstants.r3;
	result["r4"] = calibrationConstants.r4;
	result["r5"] = calibrationConstants.r5;
	result["r6"] = calibrationConstants.r6;
	result["r7"] = calibrationConstants.r7;
	result["r8"] = calibrationConstants.r8;
	result["r9"] = calibrationConstants.r9;

	result["is_load_constants"] = calibrationConstants.is_load_constants;
	return result;
}

tsai_calibration_constants& operator<<(tsai_calibration_constants &result, Json::Value &json)
{
	result.f = json["f"].asDouble();
	result.kappa1 = json["kappa1"].asDouble();

	result.p1 = json["p1"].asDouble();
	result.p2 = json["p2"].asDouble();

	result.Tx = json["Tx"].asDouble();
	result.Ty = json["Ty"].asDouble();
	result.Tz = json["Tz"].asDouble();

	result.Rx = json["Rx"].asDouble();
	result.Ry = json["Ry"].asDouble();
	result.Rz = json["Rz"].asDouble();

	result.r1 = json["r1"].asDouble();
	result.r2 = json["r2"].asDouble();
	result.r3 = json["r3"].asDouble();
	result.r4 = json["r4"].asDouble();
	result.r5 = json["r5"].asDouble();
	result.r6 = json["r6"].asDouble();
	result.r7 = json["r7"].asDouble();
	result.r8 = json["r8"].asDouble();
	result.r9 = json["r9"].asDouble();

	result.is_load_constants = json["is_load_constants"].asInt();
	return result;
}

Json::Value& operator<<(Json::Value &result, tsai_camera_parameters &cameraParameters)
{
	result["Ncx"] = cameraParameters.Ncx;
	result["Nfx"] = cameraParameters.Nfx;

	result["dx"] = cameraParameters.dx;
	result["dy"] = cameraParameters.dy;

	result["dpx"] = cameraParameters.dpx;
	result["dpy"] = cameraParameters.dpy;

	result["Cx"] = cameraParameters.Cx;
	result["Cy"] = cameraParameters.Cy;

	result["sx"] = cameraParameters.sx;

	result["is_load_paremeters"] = cameraParameters.is_load_paremeters;

	return result;
}

tsai_camera_parameters& operator<<(tsai_camera_parameters &result, Json::Value &json)
{
	result.Ncx = json["Ncx"].asDouble();
	result.Nfx = json["Nfx"].asDouble();

	result.dx = json["dx"].asDouble();
	result.dy = json["dy"].asDouble();

	result.dpx = json["dpx"].asDouble();
	result.dpy = json["dpy"].asDouble();

	result.Cx = json["Cx"].asDouble();
	result.Cy = json["Cy"].asDouble();

	result.sx = json["sx"].asDouble();

	result.is_load_paremeters = json["is_load_paremeters"].asInt();

	return result;
}

Json::Value& operator<<(Json::Value &result, CameraCalibrationData &calibrationData)
{
	result["name"] = calibrationData.name;
	result["params"] << calibrationData.params;
	result["data"] << calibrationData.getResult();
	return result;
}

CameraCalibrationData& operator<<(CameraCalibrationData &result, Json::Value &json)
{
	result.name = json["name"].asString();
	if (json["params"].isObject())
		result.params << json["params"];
	if (json["data"].isObject())
		result.getResult() << json["data"];
	return result;
}

MarkerDescriptorCircle &operator<<(MarkerDescriptorCircle& result, Json::Value &json)
{
	for (Json::ValueIterator itr = json.begin() ; itr != json.end() ; itr++)
		result.angles[stoi(itr.key().asString())] = (*itr).asDouble();
	return result;
}

Json::Value &operator<<(Json::Value &result, MarkerDescriptorCircle &circle)
{
	for (auto &angle : circle.angles)
		result[to_string(angle.first)] = angle.second;
	return result;
}

MarkerDescriptor &operator<<(MarkerDescriptor& result, Json::Value &json)
{
	result.name = json["name"].asString();
	result.internal << json["internal"];
	result.external << json["external"];
	result.extra1 << json["extra1"];
	return result;
}

Json::Value &operator<<(Json::Value &result, MarkerDescriptor &markerDescriptor)
{
	result["name"] = markerDescriptor.name;
	result["internal"] << markerDescriptor.internal;
	result["external"] << markerDescriptor.external;
	result["extra1"] << markerDescriptor.extra1;
	return result;
}

Json::Value &operator<<(Json::Value &json, config::Server &server)
{
	json["host"] = server.hostName;
	json["port"] = server.port;
	return json;
}

CameraCalibrationParams &operator<<(CameraCalibrationParams &result, Json::Value &json)
{
	result.rows = json["rows"].asUInt();
	result.columns = json["columns"].asUInt();
	result.origin << json["origin"];
	result.distance << json["distance"];
	result.clusteringThreshold = json["thresholds"]["clustering"].asUInt();
	result.brightnessThreshold = json["thresholds"]["brightness"].asUInt();
	return result;
}

Json::Value &operator<<(Json::Value &json, CameraCalibrationParams &result)
{
	json["rows"] = result.rows;
	json["columns"] = result.columns;
	json["origin"] << result.origin;
	json["distance"] << result.distance;
	json["thresholds"]["clustering"] = result.clusteringThreshold;
	json["thresholds"]["brightness"] = result.brightnessThreshold;
	return json;
}
