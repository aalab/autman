
#include "commands.h"

#include <sstream>

#include <json/value.h>
#include <json/reader.h>
#include <json/writer.h>

#include "commands.h"

#include "../../videostream.h"
#include "../../globals.h"
#include "../../configuration.h"
#include "../json.h"
#include "../../findgrid.h"
#include "../../utils/finally.hpp"
#include "../../../libvideo/framebuffer.h"
#include "../../../libvideo/videodevice.h"
#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

using boost::asio::ip::udp;

using namespace http;
using namespace std;

void actions::Commands::action(Response& response, Request& request)
{
	string command = request.path_match[1];
	Json::Reader reader;
	if (command == "processingmode")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value json;
			request.content >> json;
			string mode = json["mode"].asString();

			if (mode == "raw")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::Raw);
			else if (mode == "goalarea")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::GoalArea);
			else if (mode == "showcolours")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::ShowColours);
			else if (mode == "segmentcolours")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::SegmentColours);
			else if (mode == "calibrationprocessing")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::CalibrationProcessing);
			else if (mode == "findmarker")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::FindMarkers);
			else if (mode == "followline")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::FollowLine);
			else if (mode == "marathon")
			{
				globals->GetVideo()->SetMode(VideoStream::ProcessType::Marathon);
				globals->GetVideo()->marathonMode = VideoStream::MarathonMode::Marathon_FollowLine;
			}
			else if (mode == "sprint")
			{
				globals->GetVideo()->SetMode(VideoStream::ProcessType::Sprint);
				globals->GetVideo()->sprintMode = "forward";
			}
			else if (mode == "headtracking")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::HeadTracking);
			else if (mode == "goistest")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::GoitsTest);
			else if (mode == "goalkeeper")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::GoalKeeper);
			else if (mode == "obstaclerun")
				globals->GetVideo()->SetMode(VideoStream::ProcessType::ObstacleRun);
			else
			{
				Result r(ResultStatus::badRequest);
				Json::Value jsonResponse;
				jsonResponse["success"] = false;
				jsonResponse["message"] = "The mode was not found";
				r.stream << json;
				r.writeTo(response);
				return;
			}
			Json::Value jsonResponse;
			jsonResponse["success"] = true;

			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch (exception &e)
		{
			Json::Value jsonResponse;
			jsonResponse["success"] = false;
			jsonResponse["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << jsonResponse;
			r.writeTo(response);
		}
	}
	else if (command == "query")
	{
		Globals *globals = Globals::GetGlobals();
		Json::Value json;
		json << *globals->configuration;

		if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::Raw)
			json["processingmode"] = "raw";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::ShowColours)
			json["processingmode"] = "showcolours";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::SegmentColours)
			json["processingmode"] = "segmentcolours";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::CalibrationProcessing)
			json["processingmode"] = "calibrationprocessing";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::FindMarkers)
			json["processingmode"] = "findmarker";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::Sprint)
			json["processingmode"] = "sprint";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::HeadTracking)
			json["processingmode"] = "headtracking";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::GoitsTest)
			json["processingmode"] = "goistest";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::GoalKeeper)
			json["processingmode"] = "goalkeeper";
		else if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::ObstacleRun)
			json["processingmode"] = "obstaclerun";
		else
			json["processingmode"] = "unknown";

		Result r;
		r.header("Content-Type", "application/json");
		r.stream << json;
		r.writeTo(response);
	}
	else if (command == "controlmodule")
	{
		try
		{
			string json;
			request.content >> json;

			boost::asio::io_service io_service;

			udp::resolver resolver(io_service);
			udp::resolver::query query(udp::v4(), "127.0.0.1", "12345");
			udp::endpoint receiver_endpoint = *resolver.resolve(query);

			udp::socket socket(io_service);
			socket.open(udp::v4());

			socket.send_to(boost::asio::buffer(json), receiver_endpoint);

			boost::array<char, 1024> recv_buf;
			udp::endpoint sender_endpoint;
			client udpC(io_service, socket, sender_endpoint);
			boost::system::error_code ec;
			size_t len = udpC.receive(
				boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec
			);

			if (ec)
			{
				cerr << "TIMEOUT" << endl;
				Result r(ResultStatus::internalServerError);
				r.writeTo(response);
			}
			else
			{
				Result r;
				r.stream.write(recv_buf.data(), len + 1);
				r.writeTo(response);
			}
			/*
			Json::Value jsonResponse;
			jsonResponse["success"] = true;
			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
			*/
		}
		catch (exception &e)
		{
			Json::Value jsonResponse;
			jsonResponse["success"] = false;
			jsonResponse["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << jsonResponse;
			r.writeTo(response);
		}
	}
	else if (command == "removecolour")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;
			int index = pt["index"].asInt();

			if (index < globals->configuration->colours.size())
			{
				globals->configuration->colours.erase(globals->configuration->colours.begin() + index);

				Result r;
				Json::Value jsonResponse;
				jsonResponse["success"] = true;
				r.stream << jsonResponse;
				r.writeTo(response);
			}
			else
			{
				Result r(ResultStatus::badRequest);
				Json::Value jsonResponse;
				jsonResponse["success"] = false;
				jsonResponse["message"] = "The colour was not found";
				r.stream << jsonResponse;
				r.writeTo(response);
			}
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "savecolour")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;
			int index = pt["index"].asInt();

			if (index < globals->configuration->colours.size())
			{
				ColourDefinition &colour = globals->configuration->colours[index];
				colour << pt;
			}
			else
			{
				globals->configuration->colours.emplace_back();
				globals->configuration->colours.back() << pt;
			}

			Result r;
			Json::Value jsonResponse;
			jsonResponse["success"] = true;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch (exception &e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "addpixelcolourdefinition")
	{
		Globals *globals = Globals::GetGlobals();

		Json::Value json;
		request.content >> json;

		globals->exportFrameBuffer = true;

		std::unique_lock<std::mutex> lock(globals->exportMutex);
		globals->exportConditionVariable.wait(lock);

		int index = json["index"].asInt();
		if (index < globals->configuration->colours.size())
		{
			ColourDefinition *colour = &globals->configuration->colours[index];
			Pixel pixel;
			for (Json::Value &p : json["points"])
			{
				globals->frameBufferExported->getPixel(p["y"].asInt(), p["x"].asInt(), &pixel);
				pixel.update();
				colour->addPixel(pixel);
			}

			Json::Value jsonResult;
			jsonResult["success"] = true;
			jsonResult["colour"] << *colour;
			Result r(ResultStatus::ok);
			r.stream << jsonResult;
			r.writeTo(response);
		}
		else
		{
			Result r(ResultStatus::badRequest,
					 "{\"success\":false,\"message\":\"The selected colour was not found.\"}");
			r.writeTo(response);
		}
	}
	else if (command == "selectcolour")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;
			int colourIndex = pt["index"].asInt();
			if (colourIndex < globals->configuration->colours.size())
			{
				globals->configuration->currentColour = &globals->configuration->colours[colourIndex];

				Json::Value jsonResult;
				jsonResult["success"] = true;
				jsonResult["colour"]["index"] = colourIndex;
				jsonResult["colour"]["name"] = globals->configuration->currentColour->name;
				Result r(ResultStatus::ok);
				r.stream << jsonResult;
				r.writeTo(response);
			}
			else
			{
				Result r(ResultStatus::badRequest, "{\"success\":false,\"message\":\"The colour was not found.\"}");
				r.writeTo(response);
			}
		}
		catch (exception &e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "cameracalibrationremove")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;

			Json::Value jsonResponse;
			if (globals->configuration->removeCameraCalibration(pt["index"].asInt()))
			{
				jsonResponse["success"] = true;
			}
			else
			{
				jsonResponse["success"] = false;
				jsonResponse["message"] = "Calibration index out of bounds.";
			}
			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r;
			r.status = ResultStatus::badRequest;
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "setcurrentcameracalibration")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;

			Json::Value jsonResponse;
			if (pt["index"].asInt() < globals->configuration->cameraCalibrations.size())
			{
				globals->configuration->currentCameraCalibration = &globals->configuration->cameraCalibrations[pt["index"].asInt()];
				jsonResponse["success"] = true;
			}
			else
			{
				jsonResponse["success"] = false;
				jsonResponse["message"] = "Calibration index out of bounds.";
			}
			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r;
			r.status = ResultStatus::badRequest;
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "cameracalibrationsettings")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;

			if (pt["index"].asInt() < globals->configuration->cameraCalibrations.size())
			{
				globals->configuration->cameraCalibrations[pt["index"].asInt()] << pt;
			}
			else
			{
				globals->configuration->cameraCalibrations.emplace_back();
				globals->configuration->cameraCalibrations.back() << pt;
			}

			Json::Value jsonResponse;
			jsonResponse["success"] = true;
			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r;
			r.status = ResultStatus::badRequest;
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "saveppm")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;

			string fname = pt["filename"].asString();

			globals->exportFrameBuffer = true;

			std::unique_lock<std::mutex> lock(globals->exportMutex);
			globals->exportConditionVariable.wait(lock);

			globals->frameBufferExported->outToPPM(fname + ".ppm");

			Result r;
			Json::Value jsonResponse;
			jsonResponse["success"] = true;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "savecameraparameters")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;

			globals->GetVideo()->device->SetBrightness(pt["brightness"].asInt());
			globals->GetVideo()->device->SetContrast(pt["contrast"].asInt());
			globals->GetVideo()->device->SetSaturation(pt["saturation"].asInt());
			globals->GetVideo()->device->SetSharpness(pt["sharpness"].asInt());
			globals->GetVideo()->device->SetGain(pt["gain"].asInt());

			globals->configuration->brightness = globals->GetVideo()->device->GetBrightness();
			globals->configuration->contrast = globals->GetVideo()->device->GetContrast();
			globals->configuration->saturation = globals->GetVideo()->device->GetSaturation();
			globals->configuration->sharpness = globals->GetVideo()->device->GetSharpness();
			globals->configuration->gain = globals->GetVideo()->device->GetGain();

			globals->configuration->flipImage = pt["flip"].asBool();

			pt.clear();
			pt["success"] = true;
			pt["brightness"] = globals->GetVideo()->device->GetBrightness();
			pt["contrast"] = globals->GetVideo()->device->GetContrast();
			pt["saturation"] = globals->GetVideo()->device->GetSaturation();
			pt["sharpness"] = globals->GetVideo()->device->GetSharpness();
			pt["gain"] = globals->GetVideo()->device->GetGain();
			pt["flip"] = globals->configuration->flipImage;
			Result r;
			r.stream << pt;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value json;
			json["success"] = false;
			json["message"] = e.what();
			Result r(ResultStatus::badRequest);
			r.stream << json;
			r.writeTo(response);
		}
	}
	else if (command == "calibrationtrapezoid")
	{
		Globals *globals = Globals::GetGlobals();
		try
		{
			Json::Value pt;
			request.content >> pt;
			globals->trapezoid << pt["trapezoid"];

			Json::Value jsonResponse;
			jsonResponse["success"] = true;

			Result r;
			r.stream << jsonResponse;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();

			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "downloadconfiguration")
	{
		Globals* globals = Globals::GetGlobals();
		try
		{
			Json::Value json;
			json << *globals->configuration;
			Result r;
			r.header("Content-Disposition", "attachment; filename=configuration.json");
			r.contentType = "application/octet-stream";
			r.stream << json;
			r.writeTo(response);
		}
		catch(exception& e)
		{
			Result r(ResultStatus::badRequest);
			r.stream << "<html><body><h1>Error downloading the configuration</h1><p>" << e.what();
			r.stream << "</p><a href=\"javascript:history.back(1)\">Return</a></body></html>";
			r.writeTo(response);
		}
	}
	else if (command == "storemarker")
	{
		Globals* globals = Globals::GetGlobals();
		try
		{
			if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::FindMarkers)
			{
				Json::Value json;
				request.content >> json;

				Json::Value jsonResponse;
				jsonResponse["success"] = true;
				// TODO Race condition between VideoStream (FindMarkers processing mode) and here. I'm sorry!
				jsonResponse["marker"] << globals->lastFoundMarker;

				Result r;
				r.stream << jsonResponse;
				r.writeTo(response);

				globals->markersTrain.push_back(globals->lastFoundMarker); // Yeah! Duplicate marker.
			}
			else
			{
				Json::Value p;
				p["success"] = false;
				p["message"] = "Wrong processing mode. Find marker needed.";

				Result r(ResultStatus::badRequest);
				r.stream << p;
				r.writeTo(response);
			}
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();

			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "removemarker")
	{
		Globals* globals = Globals::GetGlobals();
		try
		{
			if (globals->GetVideo()->GetMode() == VideoStream::ProcessType::FindMarkers)
			{
				if (globals->markersTrain.size() > 0)
				{
					globals->markersTrain.pop_back();

					Json::Value jsonResponse;
					jsonResponse["success"] = true;
					// TODO Race condition between VideoStream (FindMarkers processing mode) and here. I'm sorry!
					if (globals->markersTrain.size() > 0)
						jsonResponse["marker"] << globals->lastFoundMarker;
					else
						jsonResponse["marker"] = false;

					Result r;
					r.stream << jsonResponse;
					r.writeTo(response);
				}
				else
				{
					Json::Value p;
					p["success"] = false;
					p["message"] = "There is no marker stored.";

					Result r(ResultStatus::badRequest);
					r.stream << p;
					r.writeTo(response);
				}
			}
			else
			{
				Json::Value p;
				p["success"] = false;
				p["message"] = "Wrong processing mode. Find marker needed.";

				Result r(ResultStatus::badRequest);
				r.stream << p;
				r.writeTo(response);
			}
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();

			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "trainmarker")
	{
		Globals* globals = Globals::GetGlobals();
		try
		{
			Json::Value json;
			request.content >> json;

			globals->trainMarker = true;

			unique_lock<mutex> lock(globals->trainMarkerMutex);
			if (globals->trainMarkerCondition.wait_for(lock, std::chrono::seconds(10)) == cv_status::no_timeout)
			{
				globals->configuration->markers.back().name = json["name"].asString();

				Json::Value p;
				p["success"] = true;
				p["marker"] << globals->configuration->markers.back();

				globals->markersTrain.clear();

				Result r;
				r.stream << p;
				r.writeTo(response);
			}
			else
			{
				Json::Value p;
				p["success"] = false;
				p["message"] = "Timeout! No marker was found.";

				Result r(ResultStatus::badRequest);
				r.stream << p;
				r.writeTo(response);
			}
		}
		catch(exception& e)
		{
			Json::Value p;
			p["success"] = false;
			p["message"] = e.what();

			Result r(ResultStatus::badRequest);
			r.stream << p;
			r.writeTo(response);
		}
	}
	else if (command == "gyrodata")
	{
		boost::asio::io_service io_service;

		udp::resolver resolver(io_service);
		udp::resolver::query query(udp::v4(), "localhost",
								   "1414");
		udp::endpoint receiver_endpoint = *resolver.resolve(query);

		udp::socket socket(io_service);
		socket.open(udp::v4());

		stringstream str;
		str << "{ \"command\" : \"data\"}";
		socket.send_to(boost::asio::buffer(str.str()), receiver_endpoint);

		usleep(100000);
		char buffer[4048];
		int r = socket.receive(boost::asio::buffer(buffer), sizeof(buffer));
		if (r > 0)
		{
			std::string content(buffer, r);
			Result r(ResultStatus::ok, content);
			r.writeTo(response);
		}
	}
	else
	{
		Json::Value jsonResponse;
		jsonResponse["success"] = false;
		jsonResponse["message"] = "Unknown command.";

		Result r(ResultStatus::notFound);
		r.stream << jsonResponse;
		r.writeTo(response);
	}
}

actions::client::client(boost::asio::io_service& ioService, boost::asio::ip::udp::socket& socket,
	boost::asio::ip::udp::endpoint& listen_endpoint)
	: ioService_(ioService), socket_(socket), deadline_(ioService_)
{
	// No deadline is required until the first socket operation is started. We
	// set the deadline to positive infinity so that the actor takes no action
	// until a specific deadline is set.
	deadline_.expires_at(boost::posix_time::pos_infin);

	// Start the persistent actor that checks for deadline expiry.
	check_deadline();
}

std::size_t actions::client::receive(const boost::asio::mutable_buffer& buffer,
	boost::posix_time::time_duration timeout, boost::system::error_code& ec)
{
	// Set a deadline for the asynchronous operation.
	deadline_.expires_from_now(timeout);

	// Set up the variables that receive the result of the asynchronous
	// operation. The error code is set to would_block to signal that the
	// operation is incomplete. Asio guarantees that its asynchronous
	// operations will never fail with would_block, so any other value in
	// ec indicates completion.
	ec = boost::asio::error::would_block;
	std::size_t length = 0;

	// Start the asynchronous operation itself. The handle_receive function
	// used as a callback will update the ec and length variables.
	socket_.async_receive(boost::asio::buffer(buffer),
		boost::bind(&client::handle_receive, _1, _2, &ec, &length));

	// Block until the asynchronous operation has completed.
	do ioService_.run_one(); while (ec == boost::asio::error::would_block);

	return length;
}

void actions::client::check_deadline()
{
	// Check whether the deadline has passed. We compare the deadline against
	// the current time since a new asynchronous operation may have moved the
	// deadline before this actor had a chance to run.
	if (deadline_.expires_at() <= boost::asio::deadline_timer::traits_type::now())
	{
		// The deadline has passed. The outstanding asynchronous operation needs
		// to be cancelled so that the blocked receive() function will return.
		//
		// Please note that cancel() has portability issues on some versions of
		// Microsoft Windows, and it may be necessary to use close() instead.
		// Consult the documentation for cancel() for further information.
		socket_.cancel();

		// There is no longer an active deadline. The expiry is set to positive
		// infinity so that the actor takes no action until a new deadline is set.
		deadline_.expires_at(boost::posix_time::pos_infin);
	}

	// Put the actor back to sleep.
	deadline_.async_wait(boost::bind(&client::check_deadline, this));
}

void actions::client::handle_receive(
	const boost::system::error_code& ec, std::size_t length,
	boost::system::error_code* out_ec, std::size_t* out_length)
{
	*out_ec = ec;
	*out_length = length;
}