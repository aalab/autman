#ifndef __HTTP__ACTIONS__STATICFILES_H__
#define __HTTP__ACTIONS__STATICFILES_H__

#include "../response.h"
#include "../request.h"

namespace http
{
	namespace actions
	{
		class StaticFiles
		{
		public:
			static void action(Response& response, Request &request);
		};
	}
}

#endif