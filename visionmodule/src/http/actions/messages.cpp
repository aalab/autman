/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 3, 2015
 */

#include "messages.h"
#include <json/writer.h>

using namespace http;
using namespace std;

std::mutex http::actions::Messages::mutex;

std::vector<Json::Value> http::actions::Messages::messages;

void actions::Messages::action(Response& response, Request& request)
{
	std::lock_guard<std::mutex> lock(actions::Messages::mutex);
	Json::Value jsonResponse;
	jsonResponse["success"] = true;
	for (auto message : messages)
		jsonResponse["messages"].append(message);

	Result r;
	r.stream << jsonResponse;
	r.writeTo(response);

	messages.clear();
}

Json::Value &actions::Messages::emplace()
{
	if (messages.size() < 20)
	{
		messages.emplace_back();
		return messages.back();
	}
	else
		throw FullQueue();
}
