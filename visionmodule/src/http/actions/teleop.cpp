/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include "teleop.h"
#include "../../globals.h"

#include <json/reader.h>
#include <json/writer.h>

#include <services/base/json.h>

#include <services/data/teleop/touch.h>
#include <services/data/teleop/keyboard.h>
#include <services/data/teleop/xbox.h>
#include <services/data/teleop/playstation.h>

void http::actions::Teleop::action(http::Response &response, http::Request &request)
{
	Globals* globals = Globals::GetGlobals();
	if (globals->teleopService)
	{
		Json::Value json;
		request.content >> json;

		std::string type = json["type"].asString();

		std::unique_ptr<services::data::teleop::GamepadData> gamepadData;
		if (type == "keyboard")
		{
			gamepadData.reset(new services::data::teleop::Keyboard());
			services::json::factory(json["data"], *static_cast<services::data::teleop::Keyboard*>(&(*gamepadData)));
		}
		else if (type == "xbox")
		{
			gamepadData.reset(new services::data::teleop::XboxGamepad());
			services::json::factory(json["data"], *static_cast<services::data::teleop::XboxGamepad*>(&(*gamepadData)));
		}
		else if (type == "touch")
		{
			gamepadData.reset(new services::data::teleop::Touch());
			services::json::factory(json["data"], *static_cast<services::data::teleop::Touch*>(&(*gamepadData)));
		}
		else if (type == "playstation")
		{
			gamepadData.reset(new services::data::teleop::PSGamepad());
			services::json::factory(json["data"], *static_cast<services::data::teleop::PSGamepad*>(&(*gamepadData)));
		}
		services::data::robot::Head head;
		if (globals->teleopService->inputstate(*gamepadData, head))
		{
			Json::Value resultJson;
			resultJson["success"] = true;
			services::json::factory(head, resultJson["head"]);
			std::string resultJsonString;
			services::json::serialize(resultJson, resultJsonString);
			Result r(ResultStatus::ok, resultJsonString);
			r.writeTo(response);
		}
		else
		{
			Result r(ResultStatus::internalServerError, "{\"success\":false,\"message\":\"Could send the message to the teleop module.\"}");
			r.writeTo(response);
		}
	}
	else
	{
		Result r(ResultStatus::internalServerError, "{\"success\":false,\"message\":\"Teleop is not enabled.\"}");
		r.writeTo(response);
	}
}
