#ifndef __HTTP__ACTIONS__COMMANDS_H__
#define __HTTP__ACTIONS__COMMANDS_H__

#include <boost/date_time/posix_time/posix_time_config.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ip/udp.hpp>
#include "../response.h"
#include "../request.h"

namespace http
{
	namespace actions
	{
		class client
		{
		public:
			client(boost::asio::io_service& ioService, boost::asio::ip::udp::socket& socket,
				boost::asio::ip::udp::endpoint& listen_endpoint);

			std::size_t receive(
				const boost::asio::mutable_buffer& buffer,
				boost::posix_time::time_duration timeout, boost::system::error_code& ec);
		private:
			void check_deadline();

			static void handle_receive(
				const boost::system::error_code& ec, std::size_t length,
				boost::system::error_code* out_ec, std::size_t* out_length
			);
		private:
			boost::asio::io_service& ioService_;
			boost::asio::ip::udp::socket& socket_;
			boost::asio::deadline_timer deadline_;
		};

		class Commands
		{
		public:
			static void action(Response& response, Request& request);
		};
	}
}

#endif