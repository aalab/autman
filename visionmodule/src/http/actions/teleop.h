/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef VISIONMODULE_TELEOP_H
#define VISIONMODULE_TELEOP_H

#include "../response.h"
#include "../request.h"

namespace http
{
namespace actions
{
class Teleop
{
public:
	static void action(http::Response &response, http::Request &request);
};
}
}

#endif //VISIONMODULE_TELEOP_H
