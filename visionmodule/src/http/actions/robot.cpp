/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 28, 2015
 **/

#include "robot.h"
#include "../../globals.h"

#include <json/reader.h>
#include <json/writer.h>

void http::actions::Robot::action(http::Response &response, http::Request &request)
{
	try
	{
		Globals* glob = Globals::GetGlobals();

		if (glob->robotService)
		{
			Json::Value json;
			request.content >> json;
			if (glob->robotService->send(json))
			{
				Json::Value jsonResponse;
				if (glob->robotService->read(jsonResponse))
				{
					Result r(ResultStatus::ok);
					r.header("Access-Control-Allow-Origin", "*");
					r << jsonResponse;
					r.writeTo(response);
				}
				else
				{
					Json::Value jsonResponse;
					jsonResponse["success"] = false;
					jsonResponse["message"] = "Cannot read response from UDP server.";
					Result r(ResultStatus::badRequest);
					r.header("Access-Control-Allow-Origin", "*");
					r << jsonResponse;
					r.writeTo(response);
				}
			}
			else
			{
				Json::Value jsonResponse;
				jsonResponse["success"] = false;
				jsonResponse["message"] = "Cannot send the UDP to the server.";
				Result r(ResultStatus::badRequest);
				r.header("Access-Control-Allow-Origin", "*");
				r << jsonResponse;
				r.writeTo(response);
			}
		}
		else
		{
			Json::Value jsonResponse;
			jsonResponse["success"] = false;
			jsonResponse["message"] = "Robot service not configured.";
			Result r(ResultStatus::badRequest);
			r.header("Access-Control-Allow-Origin", "*");
			r << jsonResponse;
			r.writeTo(response);
		}
	}
	catch (exception &e)
	{
		Json::Value jsonResponse;
		jsonResponse["success"] = false;
		jsonResponse["message"] = e.what();
		Result r(ResultStatus::badRequest);
		r.header("Access-Control-Allow-Origin", "*");
		r << jsonResponse;
		r.writeTo(response);
	}
}

void http::actions::Robot::options(http::Response &response, http::Request &request)
{
	Result r(ResultStatus::ok);
	r.header("Access-Control-Allow-Origin", "*");
	r.header("Access-Control-Allow-Headers", "Content-Type");
	// r.header("Access-Control-Allow-Methods", "POST");
	r.writeTo(response);
}
