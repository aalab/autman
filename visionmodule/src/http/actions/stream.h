#ifndef __HTTP__ACTIONS__STREAM_H__
#define __HTTP__ACTIONS__STREAM_H__

#include "../response.h"
#include "../request.h"

namespace http
{
	namespace actions
	{
		class Stream
		{
		public:
			static void action(Response& response, Request &request);
		};
	}
}

#endif