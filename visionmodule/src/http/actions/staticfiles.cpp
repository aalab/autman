
#include <sstream>
#include <fstream>

#include "../../globals.h"
#include "../../configuration.h"
#include "staticfiles.h"
#include "../response.h"

void http::actions::StaticFiles::action(Response& response, Request& request)
{
	using namespace std;

	string filename = "www";

	string path = request.path;

	//Replace all ".." with "." (so we can't leave the web-directory)
	size_t pos;
	while ((pos = path.find("..")) != string::npos)
		path.erase(pos, 1);

	filename += path;
	ifstream ifs;
	if (filename.find('.') == string::npos)
	{
		if (filename[filename.length() - 1] != '/')
			filename += '/';
		filename += Globals::GetGlobals()->configuration->index;
	}
	ifs.open(filename, ifstream::in);

	if (ifs)
	{
		ifs.seekg(0, ios::end);
		size_t length = ifs.tellg();

		ifs.seekg(0, ios::beg);

		response.socketStream << "HTTP/1.1 200 OK\r\nContent-Length: " << length << "\r\n\r\n";

		//read and send 128 KB at a time if file-size>buffer_size
		size_t buffer_size = 131072;
		if (length > buffer_size)
		{
			vector<char> buffer(buffer_size);
			size_t read_length;
			while ((read_length = ifs.read(&buffer[0], buffer_size).gcount()) > 0)
			{
				response.socketStream.write(&buffer[0], read_length);
				response.flush();
			}
		}
		else
			response.socketStream << ifs.rdbuf();

		ifs.close();
	}
	else
	{
		string content = "Could not open file " + filename;
		response.socketStream << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
	}
}
