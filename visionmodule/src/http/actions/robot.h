/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 28, 2015
 **/

#ifndef __HTTP_ACTIONS_ROBOT_H__
#define __HTTP_ACTIONS_ROBOT_H__

#include <services/base/udpclient.h>
#include "../request.h"
#include "../response.h"
namespace http
{
namespace actions
{
class Robot
{
public:
	static void action(Response &response, Request &request);
	static void options(Response &response, Request &request);
};
}
}

#endif //__HTTP_ACTIONS_ROBOT_H__
