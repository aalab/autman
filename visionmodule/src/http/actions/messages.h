/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 4/3/15.
 */

#ifndef _VISIONMODULE_MESSAGES_H_
#define _VISIONMODULE_MESSAGES_H_

#include <queue>
#include <json/value.h>
#include <mutex>
#include "../response.h"
#include "../request.h"

namespace http
{
	namespace actions
	{

		class Messages
		{
		public:
			class FullQueue: public exception
			{ };

		private:
			static std::vector<Json::Value> messages;
		public:
			static void action(Response& response, Request& request);

			static std::mutex mutex;
			static Json::Value &emplace();
		};
	}
}


#endif //_VISIONMODULE_MESSAGES_H_
