#ifndef __HTTP__ACTIONS__SNAPSHOT_H__
#define __HTTP__ACTIONS__SNAPSHOT_H__

#include "../response.h"
#include "../request.h"

namespace http
{
	namespace actions
	{
		class Snapshot
		{
		public:
			static void action(Response& response, Request& request);
		};
	}
}

#endif