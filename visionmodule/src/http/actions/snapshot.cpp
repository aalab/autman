
#include <sstream>
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/ptree.hpp>

#include "snapshot.h"
#include "../../globals.h"
#include "../../utils/finally.hpp"
#include "../../httpd.h"

using namespace http;
using namespace std;

void actions::Snapshot::action(Response& response, Request& request)
{
	unsigned char *frame = NULL;
	int frame_size = 0;

	Globals *globals = Globals::GetGlobals();
	globals->SetClientRequest(true);

	globals->CondWaitForBuffer();
	{
		frame_size = globals->GetSize();

		if ((frame = (uint8_t *) malloc(frame_size + 1)) == NULL)
		{
			Result error(ResultStatus::internalServerError);
			error.writeTo(response);
			return;
		}

		memcpy(frame, (void const *) globals->GetBuffer(), frame_size);
		globals->UnlockBuffer();
	}

	response.socketStream << "HTTP/1.1 200 OK\r\n";
	response.socketStream << "Content-Length: " << frame_size << "\r\n\r\n";
	response.socketStream.write((char*)frame, frame_size);
	free(frame);
}
