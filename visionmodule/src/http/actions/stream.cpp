
#include <sstream>
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/ptree.hpp>

#include "stream.h"
#include "../../globals.h"
#include "../../utils/finally.hpp"
#include "../../httpd.h"
#include "../response.h"

using namespace http;
using namespace std;

void actions::Stream::action(Response & response, Request& request)
{
	response.socketStream << "HTTP/1.1 200 Document follows\r\n"
		"Content-Type: multipart/x-mixed-replace;boundary=" BOUNDARY "\r\n"
		"Connection: close\r\n\r\n";

	unsigned char *frame = NULL;
	int frame_size = 0;
	char buffer[BUFFER_SIZE] = {0};
	while (true)							// The SimpleWeb will end this automatically when the connection gone.
	{
		response.socketStream << "--" BOUNDARY "\r\n";

		Globals *globals = Globals::GetGlobals();
		globals->SetClientRequest(true);

		globals->CondWaitForBuffer();
		{
			auto d = finally([&] {
				globals->UnlockBuffer();
			});
			frame_size = globals->GetSize();

			if ((frame = (uint8_t *) malloc(frame_size + 1)) == NULL)
			{
				Result error(ResultStatus::internalServerError);
				error.writeTo(response);
				return;
			}

			memcpy(frame, (void const *) globals->GetBuffer(), frame_size);
		}

		response.socketStream << "Content-Type: image/jpeg\r\n";
		response.socketStream << "Content-Length: " << frame_size << "\r\n\r\n";
		response.socketStream.write((char*)frame, frame_size);
		response.flush();
		free(frame);
		usleep(100); // TODO: Adjust according to the server load. Maybe implement a producer/consumer. The producer
					 // would care about the server loading.
	}
}
