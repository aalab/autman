/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "http/json.h"
#include "globals.h"
#include "colors.h"
#include "findball.h"
#include "compilationdefinitions.h"
#include "findobstacles.h"

int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		cout << "Invalid params. Usage: " << argv[0] << " <config> <raw_image>" << endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE(),
		*outFloorFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);
	outFloorFb->initialize(config.width, config.height);

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	fb->inFromPPM(argv[2]);

	ColourDefinition
		*obstacle = config.getColour("obstacle"),
		*tape = config.getColour("obstacle-tape"),
		*floor = config.getColour("obstacle-floor"),
		*gate = config.getColour("obstacle-gate");

	assert(obstacle != NULL);
	assert(tape != NULL);
	assert(gate != NULL);
	assert(floor != NULL);

	std::vector<VisionObject> obstacleResults;

	Rect area;

	if (FindObstacles::find(fb, outFb, outFloorFb, floor, obstacle, gate, tape, obstacleResults, area))
	{
		for (VisionObject &ball : obstacleResults)
		{
			ImageProcessing::drawCross(fb, ball.center.x, ball.center.y, Colors::green, ball.boundBox.width()/2);
		}
		cout << "Obstacle found." << endl;
	}
	else
		cout << "Obstacles not found." << endl;

	fb->outToPPM("result.ppm");
	outFb->outToPPM("resultOut.ppm");

	delete fb;
	delete outFb;
}
