/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 25, 2015
 */

#include <json/writer.h>
#include "json.h"

Point2D &operator<<(Point2D &point, const Json::Value &json)
{
	point.x = json["x"].asDouble();
	point.y = json["y"].asDouble();
	return point;
}

Json::Value &operator<<(Json::Value &json, const Point2D &point)
{
	json["x"] = point.x;
	json["y"] = point.y;
	return json;
}

Json::Value &operator<<(Json::Value &result, Line2D &line)
{
	result["a"] << line.a;
	result["b"] << line.b;
	return result;
}

void ::json::serialize(Json::Value &json, std::string &result)
{
	Json::FastWriter writer;
	result = writer.write(json);
}
