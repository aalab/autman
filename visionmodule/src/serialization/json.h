/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 25, 2015
 */

#ifndef VISIONMODULE_SERIALIZATION_JSON_H
#define VISIONMODULE_SERIALIZATION_JSON_H

#include <json/value.h>
#include "../../libvideo/point.h"
#include "../line.h"

Point2D & operator<<(Point2D &point, const Json::Value &json);
Json::Value & operator<<(Json::Value &json, const Point2D &point);

Json::Value &operator<<(Json::Value &result, Line2D &line);

namespace json
{
	void serialize(Json::Value &json, std::string &result);
}

#endif //VISIONMODULE_SERIALIZATION_JSON_H
