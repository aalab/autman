/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include "findgoalarea.h"

#include <vector>

#include "../libvideo/visionobject.h"
#include "../libvideo/imageprocessing.h"
#include "colors.h"
#include "compilationdefinitions.h"
#include "text.h"

bool FindGoalArea::find(FrameBuffer *in, FrameBuffer *out, std::vector<VisionObject> &tapes, FindGoalAreaData &data,
	bool ressegment, unsigned int squareSize)
{
	for (VisionObject &tape : tapes)
	{
		if (tape.size > 50)
		{
			getLineInfo(
				in, out, tape, 5, 2, 1, &tape.seedColour, false, data.lines, ressegment, squareSize
			);
		}
	}
	return true;
}

void FindGoalArea::filterTapes(std::vector<VisionObject> &tapeList, std::vector<VisionObject *> &filtered)
{
	for (VisionObject &o : tapeList)
	{
		if (((o.boundBox.width() / (double) o.boundBox.height()) < 0.4) ||
			((o.boundBox.width() / (double) o.boundBox.width()) < 0.4) ||
			(o.size / (double) o.boundBox.size() < 0.4))
			filtered.push_back(&o);
	}
}

void FindGoalArea::getLineInfo(
	FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, unsigned int lineOffset,
	unsigned int minLength, unsigned int subsample, RawPixel *tapePixel, bool expand, std::vector<Line2D> &resultLines,
	bool ressegment, unsigned int squareSize
)
{
	double
		imageLineTopX, imageLineTopY,
		imageLineBottomX, imageLineBottomY,
		nX, nY;

	FindGoalArea::getImageLineTop(
		vo, &imageLineTopX, &imageLineTopY, lineOffset, &vo, frame, outFrame, minLength, subsample, tapePixel
	);
	if (imageLineTopX != -1 && imageLineTopY != -1)
	{
		std::vector<Line2D> lineSegments;
		getLineSegments(frame, outFrame, vo, imageLineTopX, imageLineTopY, squareSize, lineSegments);

		// Get the bottom line
		getImageLineBottom(
			vo, &imageLineBottomX, &imageLineBottomY, lineOffset, &vo, frame, outFrame, minLength, subsample, tapePixel
		);
		if (imageLineBottomX != -1 && imageLineBottomY != -1)
		{
			double plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
			float lineWidth;
			getImageLine(
				&nX, &nY, imageLineTopX, imageLineTopY, lineOffset, &vo, frame, outFrame, 1, 1, tapePixel, plineAngle,
				&lineWidth
			);
			float firstLineLength = std::sqrt(
				((imageLineBottomX - imageLineTopX) * (imageLineBottomX - imageLineTopX))
				+ ((imageLineBottomY - imageLineTopY) * (imageLineBottomY - imageLineTopY))
			);
			if (firstLineLength * 0.25 < lineWidth)
			{
				VERBOSE("Rejecting by line width(1): " << lineWidth << " : Line length: " << firstLineLength);
				return;
			}
			if (nX != -1)
			{
				imageLineTopX = nX;
				imageLineTopY = nY;
				plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
			}
			if (firstLineLength * 0.25 < lineWidth)
			{
				VERBOSE("Rejecting by line width(2): " << lineWidth << " : Line length: " << firstLineLength);
				return;
			}

			getImageLine(
				&nX, &nY, imageLineBottomX, imageLineBottomY, -lineOffset, &vo, frame, outFrame, 1, 1, tapePixel,
				plineAngle, &lineWidth
			);

			if (nX != -1)
			{
				imageLineBottomX = nX;
				imageLineBottomY = nY;
				plineAngle = atan2((imageLineBottomY - imageLineTopY), (imageLineBottomX - imageLineTopX));
			}

			if (expand)
			{
				expandImageLineWhileMatch(
					&nX, &nY, imageLineTopX, imageLineTopY, vo, frame, outFrame, tapePixel, plineAngle, 0
				);

				imageLineTopX = nX;
				imageLineTopY = nY;
			}

			/*
			ImageProcessing::drawCross(frame, imageLineTopX, imageLineTopY, Colors::white, 3);
			ImageProcessing::drawCross(frame, imageLineBottomX, imageLineBottomY, Colors::green, 3);
			*/

			nX = imageLineBottomX - imageLineTopX;
			nY = imageLineBottomY - imageLineTopY;
			if (ressegment && (std::sqrt(nX*nX + nY*nY) > 20))
			{
				// Reference line found
				resegmentLine(frame, outFrame, vo, tapePixel, imageLineTopX, imageLineTopY, imageLineBottomX,
							  imageLineBottomY, resultLines);

				unsigned int i = 0;
				for (Line2D &l : resultLines)
				{
					// ImageProcessing::drawBresenhamLine(frame, l.a.x, l.a.y, l.b.x, l.b.y, Colors::green);
					// vision::Text::draw(l.a.x, l.a.y, std::to_string(i++), Colors::green, frame);
				}
			}
			else
			{
				resultLines.emplace_back(imageLineTopX, imageLineTopY, imageLineBottomX, imageLineBottomY);
			}
		}
	}
}

void FindGoalArea::getImageLineTop(
	VisionObject &vo, double *lineTopCenterX, double *lineTopY, unsigned int offset, VisionObject *line,
	FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample, RawPixel *tapePixel
)
{
	unsigned int count;
	Pixel cPixel;
	unsigned int col, row, sumLine;
	FrameBufferIterator it(outFrame);
	*lineTopCenterX = -1;
	*lineTopY = -1;

	float bbRatio = vo.boundBox.width() / static_cast<float>(vo.boundBox.height());

	VERBOSE("bbRatio = " << bbRatio);

	if (bbRatio <= 1.0)
	{
		for (row = line->boundBox.topLeft().y() + offset;
			 row < line->boundBox.bottomRight().y(); row = row + subsample)
		{
			it.goPosition(row, line->boundBox.topLeft().x());
			sumLine = 0;

			count = 0;
			for (col = line->boundBox.topLeft().x();
				 col < line->boundBox.bottomRight().x(); col = col + subsample, it.goRight(subsample))
			{
				it.getPixel(&cPixel);

				if (tapePixel->isSameColor(&cPixel))
				{
					if (*lineTopCenterX == -1 && *lineTopY == -1)
					{
						count++;
						sumLine += col;
					}
				}
			}
			// check at the end of each row
			if (*lineTopCenterX == -1 && *lineTopY == -1 && (count > minLength))
			{
				*lineTopCenterX = sumLine / count;
				*lineTopY = row;
				break;
			}
		}
	}
	else
	{
		for (unsigned int col = line->boundBox.topLeft().x() + offset;
			 col < line->boundBox.bottomRight().x(); col = col + subsample)
		{
			it.goPosition(line->boundBox.topLeft().y(), col);
			sumLine = 0;

			count = 0;
			for (row = line->boundBox.topLeft().y();
				 row < line->boundBox.bottomRight().y(); row += subsample, it.goDown(subsample))
			{
				it.getPixel(&cPixel);

				if (tapePixel->isSameColor(&cPixel))
				{
					// it.setPixel(Colors::yellow);
					if (*lineTopCenterX == -1 && *lineTopY == -1)
					{
						count++;
						sumLine += row;
					}
				}
			}
			// check at the end of each row
			if (*lineTopCenterX == -1 && *lineTopY == -1 && count > minLength)
			{
				*lineTopY = sumLine / count;
				*lineTopCenterX = col;
				break;
			}
		}
	}
}

void FindGoalArea::getImageLineBottom(
	VisionObject &vo, double *lineBottomCenterX, double *lineBottomY, unsigned int offset, VisionObject *line,
	FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
	RawPixel *tapePixel
)
{
	unsigned int count;
	Pixel cPixel;
	unsigned int row, col, sumLine;
	FrameBufferIterator it(outFrame);
	*lineBottomCenterX = -1;
	*lineBottomY = -1;

	float bbRatio = vo.boundBox.width() / static_cast<float>(vo.boundBox.height());

	if (bbRatio <= 1.0)
	{
		for (unsigned int row = line->boundBox.bottomRight().y() - offset;
			 row > line->boundBox.topLeft().y(); row = row - subsample)
		{
			it.goPosition(row, line->boundBox.topLeft().x());
			sumLine = 0;

			count = 0;
			for (col = line->boundBox.topLeft().x();
				 col < line->boundBox.bottomRight().x(); col = col + subsample, it.goRight(subsample))
			{
				it.getPixel(&cPixel);

				if (tapePixel->isSameColor(&cPixel))
				{
					if (*lineBottomCenterX == -1 && *lineBottomY == -1)
					{
						count++;
						sumLine += col;
					}
				}
			}

			// check at the end of each row
			if (*lineBottomCenterX == -1 && *lineBottomY == -1 && count > minLength)
			{
				*lineBottomCenterX = sumLine / count;
				*lineBottomY = row;
				break;
			}
		}
	}
	else
	{
		for (col = line->boundBox.bottomRight().x() - offset;
			 col > line->boundBox.topLeft().x(); col = col - subsample)
		{
			it.goPosition(line->boundBox.topLeft().y(), col);
			sumLine = 0;

			count = 0;
			for (row = line->boundBox.topLeft().y();
				 row < line->boundBox.bottomRight().y(); row = row + subsample, it.goDown(subsample))
			{
				it.getPixel(&cPixel);

				if (tapePixel->isSameColor(&cPixel))
				{
					// it.setPixel(Colors::white);
					if (*lineBottomCenterX == -1 && *lineBottomY == -1)
					{
						count++;
						sumLine += row;
					}
				}
			}

			// check at the end of each row
			if (*lineBottomCenterX == -1 && *lineBottomY == -1 && count > minLength)
			{
				*lineBottomCenterX = col;
				*lineBottomY = sumLine / count;
				break;
			}
		}
	}
}

bool FindGoalArea::getImageLine(
	double *centerX, double *centerY, unsigned int x, unsigned int y, int offset, VisionObject *line,
	FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
	RawPixel *tapePixel, const double angleLine, float *width)
{
	Pixel cPixel;
	int
		count = 0,
		col, row;
	*centerX = -1;
	*centerY = -1;

	double
		angle = angleLine - M_PI_2l,
		slope = std::tan(angle),
		s = std::sin(angleLine),
		c = std::cos(angleLine);

	double
		start_y = line->boundBox.topLeft().y(),
		start_x = (start_y - (y + s * offset)) / slope + (x + c * offset),
		end_y = line->boundBox.bottomRight().y(),
		end_x = (end_y - (y + s * offset)) / slope + (x + c * offset);

	if (start_x < line->boundBox.topLeft().x())
	{
		start_x = line->boundBox.topLeft().x();
		start_y = (slope * (start_x - (x + c * offset))) + (y + s * offset);
	}
	if (start_x > line->boundBox.bottomRight().x())
	{
		start_x = line->boundBox.bottomRight().x();
		start_y = (slope * (start_x - (x + c * offset))) + (y + s * offset);
	}
	if (end_x > line->boundBox.bottomRight().x())
	{
		end_x = line->boundBox.bottomRight().x();
		end_y = (slope * (end_x - (x + c * offset))) + (y + s * offset);
	}
	if (end_x < line->boundBox.topLeft().x())
	{
		end_x = line->boundBox.topLeft().x();
		end_y = (slope * (end_x - (x + c * offset))) + (y + s * offset);
	}

	c = std::cos(angle);
	s = std::sin(angle);

	int
		distanceUp = 1,
		distanceDown = -1;

	double
		d = std::sqrt(((end_x - start_x) * (end_x - start_x)) + ((end_y - start_y) * (end_y - start_y))),
		tmpD;
	for (unsigned int i = 0; i < d; i++)
	{
		col = start_x + (end_x - start_x) / d * (i * subsample);
		row = start_y + (end_y - start_y) / d * (i * subsample);

		// col = start_x + c*(i*subsample);
		// row = start_y + s*(i*subsample);

		// cout << "Looking at: (" << col << " : " << row << ") " << d << endl;

		outFrame->getPixel(row, col, &cPixel);
		cPixel.update();
		if ((tapePixel->isSameColor(&cPixel)) && ((i + 1) < d))
		{
			// frame->setPixel(row, col, Colors::cyan);
			if (*centerX == -1 && *centerY == -1)
			{
				// cout << "Start found! (" << col << " : " << row << ")" << endl;
				*centerX = col;
				*centerY = row;
				count++;
			}
		}
		else if ((count > 0))
		{
			tmpD = std::sqrt(((col - *centerX) * (col - *centerX)) + ((row - *centerY) * (row - *centerY)));
			// cout << "tmpD: " << tmpD << endl;
			if (tmpD > minLength)
			{
				*width = std::sqrt(((col - *centerX)*(col - *centerX)) + ((row - *centerY)*(row - *centerY)));
				*centerX = *centerX + ((col - *centerX) / 2);
				*centerY = *centerY + ((row - *centerY) / 2);
				// vision::Text::draw(*centerX, *centerY, std::to_string(width), Colors::green, frame);
				return true;
				// FindMarker::drawCross(outFrame, *centerX, *centerY, blackPixel);
				// break;
			}
			else
			{
				*centerX = *centerY = -1;
				count = 0;
			}
		}
	}
	return false;
}

void FindGoalArea::expandImageLineWhileMatch(
	double *pointX, double *pointY, unsigned int x, unsigned int y, VisionObject &line, FrameBuffer *frame,
	FrameBuffer *outFrame, RawPixel *tapePixel, const double angle, unsigned int threshold
)
{
	Pixel p;
	double
		pX, pY,
		c = std::cos(angle),
		s = std::sin(angle);
	unsigned int markerMatchCount;
	for (unsigned int i = 0; ; i++)
	{
		pX = x - (c * i);
		pY = y - (s * i);
		if (
			(pX >= 0) && (pX < frame->width)
			&& (pY >= 0) && (pY < frame->height)
			)
		{
			outFrame->getPixel(pY, pX, &p);
			p.update();
/*
#ifdef FINDMARKER__OUTPUTDRAW
			outFrame->setPixel(pY, pX, Colors::pink);
#endif
*/
			if (!(tapePixel->isSameColor(&p)))
			{
				markerMatchCount++;
				if (markerMatchCount >= threshold)
					break;
			}
			else
			{
				markerMatchCount = 0;
				*pointX = pX;
				*pointY = pY;
			}
		}
		else
			break;
	}
}

void FindGoalArea::resegmentLine(FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, RawPixel *tapePixel,
	unsigned int imageLineTopX, unsigned int imageLineTopY, unsigned int imageLineBottomX, unsigned int imageLineBottomY,
	std::vector<Line2D> &resultLines)
{
	double plineAngle, nX, nY;
	std::vector<Line2D> lines;
	lines.emplace_back(imageLineTopX, imageLineTopY, imageLineBottomX, imageLineBottomY);

	double
		angleTolerance = D2R(5),
		minimumSegmentSize = 25;

	double plineAngle2, bX, bY;
	unsigned int
		i = 0,
		maxIterations = 50,
		iterations = 0;
	double h, plineAngle3;
	Line2D *line;
	while (i < lines.size() && (iterations < maxIterations))
	{
		// VERBOSE("Stack size: " << lines.size());
		line = &lines[i];
		plineAngle = line->inclination();
		h = line->hypot();
		float lineWidth;
		if (getImageLine(
			&nX, &nY, line->a.x, line->a.y, h/2, &vo, frame, outFrame, 1, 1, tapePixel,
			plineAngle, &lineWidth
		))
		{
			plineAngle2 = atan2((nY - line->a.y), (nX - line->a.x));
			if (i > 0)
				plineAngle3 = std::atan2((nY - lines[i-1].a.y), (nX - line->b.x)) - lines[i-1].inclination();
			// VERBOSE("Line angle: " << R2D(plineAngle) << " Candidate angle: " << R2D(plineAngle2) << " Previous angle: " << R2D(plineAngle3));
			if (															// Tolerance to split line :
				(h > minimumSegmentSize) 									// Too close one each other
				&& (std::abs(plineAngle - plineAngle2) > angleTolerance)	// If adding the new point doesn't change the inclination.
				&& (
					(i==0)
					|| (std::abs(plineAngle3) > angleTolerance)	// If from the previous line to the new point doesn't change the inclination.
				)
			)
			{
				// VERBOSE("Angle diff: " << R2D(std::abs(plineAngle - plineAngle2)) << " is not a straight line.");

				/*
				ImageProcessing::drawCross(frame, nX, nY, Colors::green);
				ImageProcessing::drawBresenhamLine(frame, line->a.x, line->a.y, nX, nY, Colors::yellow);
				ImageProcessing::drawBresenhamLine(frame, nX, nY, line->b.x, line->b.y, Colors::black);
				*/

				// frame->outToPPM("result.ppm");
				// outFrame->outToPPM("resultOut.ppm");
				// VERBOSE("Replacing line: [" << line->a.x << ":" << line->a.y << " " << nX << ":" << nY << "]");

				bX = line->b.x;
				bY = line->b.y;
				line->b.x = nX;
				line->b.y = nY;

				if ((i == 0) || (std::abs(lines[i-1].inclination() - line->inclination()) > angleTolerance))
				{
					// VERBOSE("Emplacing line: [" << nX << ":" << nY << " " << line->b.x << ":" << line->b.y << "]");
					lines.emplace(lines.begin() + (i + 1), nX, nY, bX, bY);
				}
				else
				{
					lines[i-1].b.x = nX;
					lines[i-1].b.y = nY;
					line->a.x = nX;
					line->a.y = nY;
					line->b.x = bX;
					line->b.y = bY;
				}
			}
			else if ((i > 0) && ((i+1) == lines.size()) && (std::abs(line->inclination() - line[i-1].inclination()) < angleTolerance))
			{
				lines[i-1].b.x = line->b.x;
				lines[i-1].b.y = line->b.y;
				lines.pop_back();
				i++;
			}
			else
			{
				// frame->outToPPM("result.ppm");
				// outFrame->outToPPM("resultOut.ppm");
				// VERBOSE("Angle diff: " << R2D(std::abs(plineAngle - plineAngle2)) << " accepted as straight line.");
				i++;
			}
		}
		else
			break;
		iterations++;
	}
	for (Line2D &line : lines)
	{
		if (line.a.x > -1)
		{
			resultLines.push_back(line);
		}
	}
}

void FindGoalArea::getLineSegments(
	FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, double cX, double cY, unsigned int squareSize,
	std::vector<Line2D> &segments
)
{
	/*
	RawPixel pixel;

	double
		nBound = std::max(static_cast<double>(vo.boundBox.topLeft().y()), cY - squareSize/2),
		sBound = std::min(static_cast<double>(vo.boundBox.bottomRight().y()), cY + squareSize/2),
		wBound = std::max(static_cast<double>(vo.boundBox.topLeft().x()), cX - squareSize/2),
		eBound = std::min(static_cast<double>(vo.boundBox.bottomRight().x()), cX + squareSize/2),
		x,
		y;

	unsigned int
		xIncrease = 1,
		yIncrease = 0;

	// Trances the square from Starting from NW -> NE -> SE -> SW -> NW.

	// TODO: CHeck if it starts on a line already!

	FindGoalAreaSegmentContext context;

	y = nBound;
	for (x = wBound; x < eBound; x++)
	{
		outFrame->getPixel(y, x, &pixel);
		// frame->setPixel(y, x, Colors::green);
		if (context.colourFound)
		{
			if (!pixel.isSameColor(&vo.seedColour))
			{
				segments.emplace_back(context.segmentStartX, context.segmentStartY);
			}
		}
		else if (pixel.isSameColor(&vo.seedColour) && (!context.colourFound))
		{
			context.segmentStartX = x;
			context.segmentStartY = y;
			context.colourFound = true;
		}
	}
	x = eBound;
	for (y = nBound; y < sBound; y++)
	{
		frame->setPixel(y, x, Colors::blue);
	}
	y = sBound;
	for (x = eBound; x >= wBound; x--)
	{
		frame->setPixel(y, x, Colors::pink);
	}
	x = wBound;
	for (y = sBound; y >= nBound; y--)
	{
		frame->setPixel(y, x, Colors::red);
	}
	*/
}
