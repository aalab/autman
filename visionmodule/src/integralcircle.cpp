/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 1, 2015
 */

#include <cmath>
#include <iostream>
#include "integralcircle.h"

using namespace std;

double IntegralCircle::extract(int from, int to)
{
	unsigned int sum = 0;

	// Make sure that angles will be between 0->359
	from = ((from % 360) + 360) % 360;
	to = ((to % 360) + 360) % 360;

	/*
	cout << "IntegralCircle::extract" << endl;
	cout << "\tFrom: " << from << endl;
	cout << "\tTo: " << to << endl;
	*/

	if (to < from)
		return (this->values[from] - this->values[359]) + (this->values[to] - this->values[0]);
	else
		return this->values[to] - this->values[from];
}

double IntegralCircle::extract(int angle)
{
	return this->extract(angle, angle+1);
}

double IntegralCircle::extractAverage(int from, int to)
{
	return this->extract(from, to) / std::abs(to - from);
}

double IntegralCircle::proportion(int angle)
{
	return (this->extract(angle, angle + 1) - this->minValue)/(double)this->maxValue;
}
