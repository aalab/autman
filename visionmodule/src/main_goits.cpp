/**
 *
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date Fri Feb 6, 2015
 */

#include <sstream>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "configuration.h"
#include "goits/stage.h"
#include "globals.h"
#include "goits/descriptors/ball.h"
#include "compilationdefinitions.h"
#include "colors.h"

/*
 * Load a data file dumped from a frame.
 * Do the identification of the shapes by the colour.
 * Locates the grid.
 */
int main(int argc, char * argv[])
{
	if (argc < 4)
	{
		cout << "Invalid params. Usage: " << argv[0] << " <config> <frame_from> <frame_to>" << endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		std::ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	unsigned int
		fromFrame = stoi(argv[2]),
		toFrame = stoi(argv[3]);

	vision::goits::Stage stage;

	stage.info.in = fb;
	stage.info.out = outFb;
	stage.info.colours = &config.colours;
	stage.descriptors.push_back(new vision::goits::descriptors::BallDescriptor());

	std::chrono::system_clock::time_point currentTime = std::chrono::system_clock::now();

	for (unsigned int i = fromFrame; i <= toFrame; i++)
	{
		VERBOSE("Loading " << i << "º frame.");
		if (fb->inFromPPM("ppms/goits/" + std::to_string(i) + ".ppm"))
		{
			stage.process(currentTime);

			fb->outToPPM("ppms/result" + std::to_string(i) + ".ppm");
			outFb->outToPPM("ppms/resultOut" + std::to_string(i) + ".ppm");
			outFb->fill(Colors::black);
		}
		else
			std::cerr << "Error opening the " << i << "º file." << std::endl;
		usleep(100000);
		currentTime += std::chrono::milliseconds(100);
	}

	delete fb;
	delete outFb;
}
