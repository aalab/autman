/*
 * videostream.cpp
 * 
 * Jacky Baltes <jacky@cs.umanitoba.ca> Thu Dec 13 01:09:15 CST 2012
 * Kiral Poon 2015
 * Jamillo 2015
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <float.h>

#include <string>
#include <iostream>
#include <cmath>
#define PI 3.14159265

#if defined(ENABLE_DC1394)
#   include "dc1394device.h"
#endif

#if defined(ENABLE_FILE)
#   include "filedevice.h"
#endif

#if defined(ENABLE_V4L2)

#   include "../libvideo/v4l2device.h"

#endif

#ifdef DEBUG

#include <iostream>

using namespace std;
#endif

#include <arpa/inet.h>
#include <linux/videodev2.h>
#include <glob.h>
#include <iomanip>

#include "../libvideo/colourdefinition.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/pixel.h"
#include "configuration.h"
#include "globals.h"
#include "httpd.h"
#include "serial.h"
#include "findgrid.h"
#include "videostream.h"
#include "findmarker.h"
#include "colors.h"
#include "http/actions/messages.h"
#include "findsprintmarker.h"
#include "modules/control.h"
#include "text.h"
#include "goits/descriptors/ball.h"
#include "compilationdefinitions.h"
#include "findgoalarea.h"
#include "serialization/json.h"
#include "findball.h"
#include "findobstacles.h"

using namespace std;

VideoStream::VideoStream(
	string driver, string name, string input, string standard, unsigned int fps, unsigned int width,
	unsigned int height, unsigned int depth, unsigned int numBuffers, unsigned int subsample, int brightness,
	int contrast, int saturation, int sharpness, int gain
)
	: done(true), mode(VideoStream::Raw), subsample(subsample)
{
	if (0)
	{
	}
#if defined(ENABLE_V4L2)
	else if (driver == "V4L2")
	{
		device = new V4L2Device(name, input, standard, fps, width, height, depth, numBuffers);
	}
#endif
#if defined(ENABLE_DC1394)
  else if ( driver == "DC1394" )
    {
      device = new DC1394Device( name, input, standard, bayer, fps, width, height, depth, numBuffers );
    }
#endif
#if defined(ENABLE_FILE)
  else if ( driver == "File" )
    {
      device = new FileDevice( name, fps, width, height, depth );
    }
#endif
	else
	{
		std::cerr << __FILE__ << __LINE__ << ":" << __PRETTY_FUNCTION__
		<< "Unknown video capture device " << driver << std::endl;
		std::exit(1);
	}

	if (device == 0)
	{
		std::cerr << "ERROR: unable to create video device";
		perror(":");
		std::exit(EXIT_FAILURE);
	}
	else if (device->getError())
	{
		std::cerr << "ERROR: video device creation failed, error code = " << endl;
		perror(":");
		std::exit(EXIT_FAILURE);
	}

	device->SetBrightness(brightness);
	device->SetContrast(contrast);
	device->SetSaturation(saturation);
	device->SetSharpness(sharpness);
	device->SetGain(gain);

	if (gettimeofday(&prev, 0) != 0)
	{
		std::cerr << "videostream gettimeofday failed\n";
		perror("videoStream gettimeofday ");
	}

	this->findMarkerFrame = new FrameBufferRGB24BE();
	this->findMarkerFrame->initialize(100, 100);

	marathonMode = Marathon_FollowLine;
	isActionQComplete = false;
	tsai2D_define_camera_parameters(NEW_CAMERA, device->GetWidth(), device->GetWidth(), 1.0, 1.0,
									device->GetWidth() / 2, device->GetWidth() / 2, 1.0, &FindGrid::cameraParameters);

	this->stage.descriptors.push_back(new vision::goits::descriptors::BallDescriptor());
}

unsigned long long fpsTicks = 0;
double fpsRate = 0.0;
std::chrono::system_clock::time_point fpsStart;

void VideoStream::run()
{
	this->frame = 0;
	FrameBuffer *outFrame = 0;

	device->startCapture();

	Globals *globals = Globals::GetGlobals();

	device->SetBrightness(globals->configuration->brightness);
	device->SetContrast(globals->configuration->contrast);
	device->SetSaturation(globals->configuration->saturation);
	device->SetSharpness(globals->configuration->sharpness);
	device->SetGain(globals->configuration->gain);

	device->nextFrame(&this->frame);

	this->findBall2TapeFrame = new FrameBufferRGB24BE();
	this->findBall2TapeFrame->initialize(frame->width, frame->height);
	this->findBall2BallFrame = new FrameBufferRGB24BE();
	this->findBall2BallFrame->initialize(frame->width, frame->height);
	this->findBall2CalFrame = new FrameBufferRGB24BE();
	this->findBall2CalFrame->initialize(frame->width, frame->height);

	if (this->frame != 0)
	{
		outFrame = new FrameBufferRGB24BE();
		outFrame->initialize(this->frame->width, this->frame->height);

		globals->frameBufferExported = new FrameBufferRGB24BE();
		globals->frameBufferExported->initialize(this->frame->width, this->frame->height);
	}

	RawPixel mark = RawPixel(255, 0, 0);

	RawPixel px;
	done = false;
	while (!done)
	{
		std::lock_guard<std::mutex> lock(this->frameMutex);
		device->nextFrame(&this->frame);

		Globals::GetGlobals()->configuration->frame = this->frame;

		if (this->frame != 0)
		{
			/*
			if (nextColours != 0)
			{Text
				// Possible race condition?
				colours = *nextColours;
				nextColours = 0;
			}
			 */

			this->currentProcessFrameClock = std::chrono::system_clock::now();
			this->processFrameClockDuration = (this->currentProcessFrameClock - this->priorProcessFrameClock);

			if (fpsTicks == 0)
				fpsStart = this->currentProcessFrameClock;
			else
				fpsRate = fpsTicks / (std::chrono::duration_cast<std::chrono::milliseconds>(
					this->currentProcessFrameClock - fpsStart).count() / 1000.0);
			fpsTicks++;

			if (fpsTicks == 100)
			{
				fpsTicks = 0;
			}
			this->ProcessFrame(mode, this->frame, outFrame, subsample, globals->configuration->colours, mark);
			this->priorProcessFrameClock = this->currentProcessFrameClock;
		}
		device->releaseCurrentBuffer();
		sendImage(outFrame);
	}

	if (outFrame != 0)
		delete outFrame;
	device->stopCapture();
}

void vsSetClusteredColor(RawPixel *px)
{
	px->red = 0;
	px->green = 255;
	px->blue = 0;
}

void VideoStream::ProcessFrame(
	enum ProcessType ptype, FrameBuffer *frame, FrameBuffer *outFrame, unsigned int subsample,
	std::vector<ColourDefinition> colours, RawPixel mark
) {
	Globals *glob = Globals::GetGlobals();

	if (glob->configuration->flipImage) {
		frame->buffer = frame->bufferStart + std::abs(frame->bytesPerLine) * (frame->height - 1);
		glob->frameBufferExported->buffer = glob->frameBufferExported->bufferStart +
											std::abs(glob->frameBufferExported->bytesPerLine) *
											(glob->frameBufferExported->height - 1);

		frame->bytesPerLine = -std::abs(frame->bytesPerLine);
		glob->frameBufferExported->bytesPerLine = -std::abs(glob->frameBufferExported->bytesPerLine);
	}
	else {
		glob->frameBufferExported->buffer = glob->frameBufferExported->bufferStart;
		glob->frameBufferExported->bytesPerLine = std::abs(glob->frameBufferExported->bytesPerLine);
	}


	/*
	// Loads a sequence of images, each one after 10 seconds
	static int tcount = 0;
	static std::chrono::system_clock::time_point lastFrameChanged;

	int tttt = std::chrono::duration_cast<std::chrono::seconds>(this->currentProcessFrameClock - lastFrameChanged).count();

	std::ostringstream ostringstream1;

	ostringstream1 << "/home/jsantos/projects/aalabUofM/madmax/visionmodule/bin/debug/ppms/sprint/4/";
	ostringstream1 << std::setw(2) << std::setfill('0') << (tcount+1);
	ostringstream1 << ".ppm";
	frame->inFromPPM(ostringstream1.str());

	if (tttt > 5)
	{
		tcount = (tcount+1) % 15;
		lastFrameChanged = this->currentProcessFrameClock;
	}

	frame->inFromPPM("/home/jsantos/projects/aalabUofM/madmax/visionmodule/bin/debug/ppms/marathon-arash/03.ppm");
	*/

	outFrame->fill(Colors::black);

	if (glob->exportFrameBuffer) {
		memcpy(glob->frameBufferExported->bufferStart, frame->bufferStart, frame->frameSize);
		glob->exportFrameBuffer = false;
		glob->exportConditionVariable.notify_all();
	}

	if ((ptype == Raw) || (ptype == ShowColours)) {
		ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}

	if (ptype == ShowColours) {
		ImageProcessing::swapColours(
				outFrame, 0, Rect(Point(0, 0), Point(outFrame->width, outFrame->height)), subsample,
				*glob->configuration->currentColour,
				mark
		);
	}

	if (ptype == SegmentColours) {
		this->results.clear();
		ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 10, subsample, *glob->configuration->currentColour,
										mark, results, true, true);
		// ImageProcessing::segmentScanLines(frame, outFrame, 255, 5, 20, 10, mark, subsample, glob->configuration->currentColour, this->results, true, true);

		ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}

	if (ptype == GoalArea) {
		ColourDefinition
				*tape = glob->configuration->getColour("tape")/*,
			*grass = glob->configuration->getColour("grass"),
			*ball = glob->configuration->getColour("ball"),
			*cal = glob->configuration->getColour("cal")*/;
		if (tape) {
			this->results.clear();

			/*
			std::vector<VisionObject> ballCandidates;
			std::vector<VisionObject> grassResults, tapeResults, ballResults2;

			double ratio_threshold = 0.6;
			int
				distance_threshold = 2, //if two points are two close then they might be a same object
				min_size_threshold = 30,
				max_size_threshold = 100000000;
			*/

			std::vector <VisionObject>
					tapeResults;

			ImageProcessing::SegmentColours(frame, outFrame, 500, 10, 20, subsample, *tape, Colors::blue, tapeResults,
											true, false);
			// ImageProcessing::segmentScanLines(frame, outFrame, 255, 5, 20, 20, Colors::blue, subsample, tape, tapeResults, false, false);

			FindGoalAreaData goalAreaData;
			if (FindGoalArea::find(frame, outFrame, tapeResults, goalAreaData, false, 20)) {
				cout << "Area found." << endl;
				for (Line2D &l : goalAreaData.lines) {
					ImageProcessing::drawBresenhamLine(frame, l.a.x, l.a.y, l.b.x, l.b.y, Colors::red);
					VERBOSE("Line: " << l.a.x << " " << l.a.y << " " << l.b.x << " " << l.b.y);
				}
			}
			else
				cout << "Area not found." << endl;

			if (glob->exportData) {
				Json::Value *json = new Json::Value();
				glob->prepareExportData(json);

				glob->exportDataGoalArea.data.clear();
				glob->exportDataGoalArea.data["time"] =
						this->currentProcessFrameClock.time_since_epoch().count() / 1000000.0;
				Json::Value &lines = glob->exportDataGoalArea.data["lines"];
				unsigned int i = 0;
				for (Line2D &l : goalAreaData.lines) {
					ImageProcessing::drawBresenhamLine(outFrame, l.a.x, l.a.y, l.b.x, l.b.y, Colors::green);
					Json::Value line;
					line << l;
					lines.append(line);
					if (i > 5)
						break;
					i++;
				}
				glob->exportDataGoalArea.balls.clear();
				/*
				i = 0;
				for (VisionObject &vo : ballCandidates)
			    {
					Json::Value json;
					json << vo;
					glob->exportDataGoalArea.balls.append(json);
					if (i > 5)
						break;
					i++;
				}*/
				glob->doExportData();
			}
		}
		else {
			vision::Text::draw(10, 10, "Colour 'tape' was not found.", Colors::red, frame);
		}

		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
	}

	if (ptype == GoitsTest) {
		this->stage.info.in = frame;
		this->stage.info.out = outFrame;
		this->stage.info.colours = &colours;
		this->stage.process(this->currentProcessFrameClock);

		ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}

	if (ptype == HeadTracking) {
		std::vector <VisionObject> results;
		ImageProcessing::SegmentColours(frame, outFrame, 50, 5, 10, subsample, *glob->configuration->currentColour,
										Colors::green, results, true, true);

		if (!results.empty())
			modules::Control::headTrackerMove(true, results[0].boundBox.bbCenterX(), results[0].boundBox.bbCenterY());

		ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}

	if (ptype == CalibrationProcessing) {
		RawPixel p;
		for (unsigned int x = 0; x < frame->width; ++x) {
			for (unsigned int y = 0; y < frame->height; ++y) {
				frame->getPixel(y, x, &p);
				p.red = p.green = p.blue = ((p.red + p.green + p.blue) / 3 >
											glob->configuration->currentCameraCalibration->params.brightnessThreshold)
										   ? 0 : 255;
				frame->setPixel(y, x, p);
			}
		}

		if (
				(glob->trapezoid.coords[0].x() != 0) && (glob->trapezoid.coords[0].y() != 0)
				&& (glob->trapezoid.coords[1].x() != 0) && (glob->trapezoid.coords[1].y() != 0)
				&& (glob->trapezoid.coords[2].x() != 0) && (glob->trapezoid.coords[2].y() != 0)
				&& (glob->trapezoid.coords[3].x() != 0) && (glob->trapezoid.coords[3].y() != 0)
				)    // Abort if there is no start point defined.
		{
			ColourDefinition
					*colour = NULL,
					*tmpColour = NULL;
			for (unsigned int i = 0; i < colours.size(); i++) {
				tmpColour = &colours[i];
				if (tmpColour->name == "cal") {
					colour = tmpColour;
					break;
				}
			}

			if (colour != NULL) {
				results.clear();

				ImageProcessing::SegmentColours(
						frame, outFrame, 50, 1, 3, subsample, *colour, mark, results, true, true
				);

				std::vector <VisionObject> clusterized;

				// That could be done inside ImageProcessing::SegmentColours.
				FindGrid::clusterizeObjects(results, clusterized,
											glob->configuration->currentCameraCalibration->params.clusteringThreshold);

				RawPixel cl;

				Json::Value *jsonMessage;

				bool locked = false;
				if (this->processFrameClockDuration.count() > 0.3) {
					http::actions::Messages::mutex.lock();
					locked = true;
					try {
						jsonMessage = &http::actions::Messages::emplace();
						(*jsonMessage)["command"] = "calibration";
					}
					catch (std::exception &e) {
						jsonMessage = nullptr;
					}
				}

				try {
					std::vector < VisionObject * > grid;
					if (FindGrid::find(frame, outFrame, glob->trapezoid, clusterized, grid,
									   glob->configuration->currentCameraCalibration->params.rows,
									   glob->configuration->currentCameraCalibration->params.columns)) {
						if (locked && (jsonMessage != nullptr))
							(*jsonMessage)["params"]["found"] = true;
						// GRID
						cl.red = 0;
						cl.green = 255;
						cl.blue = 0;
						for (std::vector<VisionObject *>::iterator i = grid.begin(); i != grid.end(); ++i) {
							ImageProcessing::drawBresenhamLine(frame, (*i)->boundBox.topLeft().x(),
															   (*i)->boundBox.topLeft().y(),
															   (*i)->boundBox.bottomRight().x(),
															   (*i)->boundBox.topLeft().y(), cl);
							ImageProcessing::drawBresenhamLine(frame, (*i)->boundBox.bottomRight().x(),
															   (*i)->boundBox.topLeft().y(),
															   (*i)->boundBox.bottomRight().x(),
															   (*i)->boundBox.bottomRight().y(), cl);
							ImageProcessing::drawBresenhamLine(frame, (*i)->boundBox.bottomRight().x(),
															   (*i)->boundBox.bottomRight().y(),
															   (*i)->boundBox.topLeft().x(),
															   (*i)->boundBox.bottomRight().y(), cl);
							ImageProcessing::drawBresenhamLine(frame, (*i)->boundBox.topLeft().x(),
															   (*i)->boundBox.bottomRight().y(),
															   (*i)->boundBox.topLeft().x(),
															   (*i)->boundBox.topLeft().y(), cl);
						}

						if (FindGrid::calibrate(frame, outFrame, grid, *glob->configuration->currentCameraCalibration,
												glob->configuration->currentCameraCalibration->getResult())) {
							double cx, cy, cx2, cy2;

							if (this->processFrameClockDuration.count() > 0.3 && (jsonMessage != nullptr))
								(*jsonMessage)["params"]["calibrated"] = true;

							unsigned int count = 0;
							for (std::vector<VisionObject *>::iterator i = grid.begin(); i != grid.end(); ++i) {
								ImageProcessing::drawRectangle(frame, (*i)->boundBox, Colors::green);
								// vision::Text::draw((*i)->boundBox.bbCenterX(), (*i)->boundBox.bbCenterY(), std::to_string(count), Colors::blue, frame);
								count++;
							}

							float d = 500;

							world_coord_to_image_coord(
									0,
									0,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawCross(frame, cx, cy, Colors::pink, 5);

							world_coord_to_image_coord(
									1000,
									0,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawCross(frame, cx, cy, Colors::pink, 5);

							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x - d,
									glob->configuration->currentCameraCalibration->params.origin.y - d,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x + d,
									glob->configuration->currentCameraCalibration->params.origin.y - d,
									0, &cx2, &cy2, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
															   Colors::red);

							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x + d,
									glob->configuration->currentCameraCalibration->params.origin.y - d,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x + d,
									glob->configuration->currentCameraCalibration->params.origin.y + d,
									0, &cx2, &cy2, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
															   Colors::red);

							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x + d,
									glob->configuration->currentCameraCalibration->params.origin.y + d,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x - d,
									glob->configuration->currentCameraCalibration->params.origin.y + d,
									0, &cx2, &cy2, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
															   Colors::red);

							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x - d,
									glob->configuration->currentCameraCalibration->params.origin.y + d,
									0, &cx, &cy, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							world_coord_to_image_coord(
									glob->configuration->currentCameraCalibration->params.origin.x - d,
									glob->configuration->currentCameraCalibration->params.origin.y - d,
									0, &cx2, &cy2, &FindGrid::cameraParameters,
									&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
															   Colors::red);

							for (int i = 0; i < glob->configuration->currentCameraCalibration->params.columns; i++) {
								world_coord_to_image_coord(
										glob->configuration->currentCameraCalibration->params.origin.x +
										i * glob->configuration->currentCameraCalibration->params.distance.x,
										glob->configuration->currentCameraCalibration->params.origin.y,
										0, &cx, &cy, &FindGrid::cameraParameters,
										&glob->configuration->currentCameraCalibration->getResult()
								);
								world_coord_to_image_coord(
										glob->configuration->currentCameraCalibration->params.origin.x
										+ glob->configuration->currentCameraCalibration->params.distance.x * (i),
										glob->configuration->currentCameraCalibration->params.origin.y
										+ glob->configuration->currentCameraCalibration->params.distance.y *
										  (glob->configuration->currentCameraCalibration->params.rows - 1),
										0, &cx2, &cy2, &FindGrid::cameraParameters,
										&glob->configuration->currentCameraCalibration->getResult()
								);
								ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
																   Colors::pink);
							}
							for (int i = 0; i < glob->configuration->currentCameraCalibration->params.rows; i++) {
								world_coord_to_image_coord(
										glob->configuration->currentCameraCalibration->params.origin.x,
										glob->configuration->currentCameraCalibration->params.origin.y + i
																										 *
																										 glob->configuration->currentCameraCalibration->params.distance.y,
										0, &cx, &cy, &FindGrid::cameraParameters,
										&glob->configuration->currentCameraCalibration->getResult()
								);
								world_coord_to_image_coord(
										glob->configuration->currentCameraCalibration->params.origin.x
										+ glob->configuration->currentCameraCalibration->params.distance.x
										  * (glob->configuration->currentCameraCalibration->params.columns - 1),
										glob->configuration->currentCameraCalibration->params.origin.y
										+ glob->configuration->currentCameraCalibration->params.distance.y * i,
										0, &cx2, &cy2, &FindGrid::cameraParameters,
										&glob->configuration->currentCameraCalibration->getResult()
								);
								ImageProcessing::drawBresenhamLine(frame, (int) cx, (int) cy, (int) cx2, (int) cy2,
																   Colors::pink);
							}
						}
						else if ((this->processFrameClockDuration.count() > 0.3) && (jsonMessage != nullptr))
							(*jsonMessage)["params"]["calibrated"] = false;
					}
					else if ((this->processFrameClockDuration.count()) > 0.3 && (jsonMessage != nullptr))
						(*jsonMessage)["params"]["found"] = false;

				}
				catch (std::exception &e) { }

				if (locked) {
					http::actions::Messages::mutex.unlock();
					this->priorProcessFrameClock = this->currentProcessFrameClock;
				}
			}
			else {
				DBG("Cannot find 'cal' colour.\n");
			}
		}
		else {
			if (glob->configuration->currentCameraCalibration != nullptr) {
				/*
					double cx, cy, cx2, cy2;

					world_coord_to_image_coord(
						-10,
						-10,
						0, &cx, &cy, &FindGrid::cameraParameters,
						&glob->configuration->currentCameraCalibration->getResult()
					);
					world_coord_to_image_coord(
						-10,
						-10,
						0, &cx2, &cy2, &FindGrid::cameraParameters,
						&glob->configuration->currentCameraCalibration->getResult()
					);

					ImageProcessing::drawBresenhamLine(frame, cx, cy, cx2, cy2, Colors::green);

					for (int x = -10; x < 10; x++)
					{
						world_coord_to_image_coord(
							x*10,
							-10*10,
							0, &cx, &cy, &FindGrid::cameraParameters,
							&glob->configuration->currentCameraCalibration->getResult()
						);

						for (int y = -10; y < 10; y++)
						{
							world_coord_to_image_coord(
								x*10.0,
								y*10.0,
								0, &cx2, &cy2, &FindGrid::cameraParameters,
								&glob->configuration->currentCameraCalibration->getResult()
							);
							ImageProcessing::drawBresenhamLine(frame, cx, cy, cx2, cy2, Colors::red);
							cx = cx2;
							cy = cy2;
						}
					}
				 */
			}
			else {
				DBG("Start point not defined.\n");
			}
		}
		ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}
	else if (ptype == VideoStream::ProcessType::FindMarkers) {
		ColourDefinition
				*tapeColour = glob->configuration->getColour("tape"),
				*markerColour = glob->configuration->getColour("marker");
		if (tapeColour != NULL) {
			std::vector <VisionObject>
					tapeResultsO,
					tapeResults;

			// ImageProcessing::SegmentColours(frame, outFrame, 50, 2, 20, subsample, *tapeColour, Colors::green, tapeResults, false);
			ImageProcessing::segmentScanLines(frame, outFrame, 50, 10, 10, 10, Colors::green, 2, tapeColour,
											  tapeResults, false, true);
			// FindGrid::clusterizeObjects(tapeResultsO, tapeResults, 10);

			if (tapeResults.size() > 0) {
				// TODO Load it from configuration
				unsigned int
						minLength = 5;

				VisionObject *selected = NULL;

				for (VisionObject &o : tapeResults) {
					if ((selected == NULL) || (o.boundBox.size() > selected->boundBox.size()))
						selected = &o;
				}

				VisionObject &line = *selected;

				double
						worldLineTopX, worldLineTopY,
						worldLineBottomX, worldLineBottomY,
						worldLineMiddleX, worldLineMiddleY,
						pointX, pointY,
						lineAngle;

				FindMarker::getLineInfo(frame, outFrame, line, glob->configuration->marker.lineOffset, &worldLineTopX,
										&worldLineTopY,
										&worldLineMiddleX, &worldLineMiddleY, &worldLineBottomX, &worldLineBottomY,
										&pointX, &pointY, &lineAngle,
										minLength, subsample, tapeColour, markerColour,
										*glob->configuration->currentCameraCalibration, glob->configuration->marker.size
				);

				if (worldLineTopX != -1) {
					if (worldLineBottomX != -1) {
						IntegralCircle
								integralCircleInternal,
								integralCircleExternal,
								integralCircleExtra1;
						Pixel p;

						FindMarker::calculateIntegralCircle(
								frame, outFrame, integralCircleInternal, lineAngle,
								glob->configuration->marker.internalRadius, pointX, pointY,
								p, FindGrid::cameraParameters, *glob->configuration->currentCameraCalibration
						);

						FindMarker::calculateIntegralCircle(
								frame, outFrame, integralCircleExternal, lineAngle,
								glob->configuration->marker.externalRadius, pointX, pointY,
								p, FindGrid::cameraParameters, *glob->configuration->currentCameraCalibration
						);

						FindMarker::calculateIntegralCircle(
								frame, outFrame, integralCircleExtra1, lineAngle,
								glob->configuration->marker.extra1Radius,
								pointX, pointY,
								p, FindGrid::cameraParameters, *glob->configuration->currentCameraCalibration
						);

						if (glob->trainMarker) {
							lock_guard <mutex> lock(glob->trainMarkerMutex);
							glob->configuration->markers.emplace_back();
							glob->configuration->markers.back().train(integralCircleInternal, integralCircleExternal,
																	  integralCircleExtra1);

							glob->trainMarker = false;
						}
						glob->trainMarkerCondition.notify_all();

						cout << "Classifying circles .." << endl;
						MarkerDescriptor *markerDescriptor;
						FindMarker::classifyIntegralCircle(integralCircleInternal, integralCircleExternal,
														   integralCircleExtra1, &markerDescriptor);
					}
				}
			}
		}
		// For debugging purposes
		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
		// ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}
	else if (ptype == FollowLine || ptype == Marathon) {
		ColourDefinition
				*markerCD = glob->configuration->getColour("marker"),
				*markerInsideCD = glob->configuration->getColour("marker-inside"),
				*tapeCD = glob->configuration->getColour("tape");

		CameraCalibrationData *cameraCalibrationData = glob->configuration->cameraCalibrationByName("arash-marathon");

		if (
				(cameraCalibrationData != nullptr) && (markerCD != nullptr) && (markerInsideCD != nullptr)
				&& (tapeCD != nullptr)
				) {
			std::vector <VisionObject>
					tapeResults,
					markerResults,
					markerInsideResults;

			ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 20, subsample, *tapeCD, Colors::red, tapeResults,
											false, false);
			ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 20, subsample, *markerCD, Colors::blue,
											markerResults, false, false);
			ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 20, subsample, *markerInsideCD, Colors::green,
											markerInsideResults, false, false);

			FindMarkerResult findMarkerResult;
			if (FindMarker::find2(
					frame, outFrame, tapeResults, markerResults, markerInsideResults, FindGrid::cameraParameters,
					*cameraCalibrationData, findMarkerResult
			)) {
				//
			}
			else {
				// Just follow line
			}
			this->buildResultString(findMarkerResult);
		}
		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
	}
	else if (ptype == Marathon && marathonMode == Marathon_WaitToReceive) {
		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
	}
	else if (ptype == Sprint) {
		ColourDefinition
				*leftSprintMarker = glob->configuration->getColour("sprint-left"),
				*rightSprintMarker = glob->configuration->getColour("sprint-right");

		this->results.clear();

		ImageProcessing::SegmentColours(
				frame, outFrame, 255, 5, 20, subsample, *leftSprintMarker, Colors::green, this->results, false, false
		);
		ImageProcessing::SegmentColours(
				frame, outFrame, 255, 5, 20, subsample, *rightSprintMarker, Colors::pink, this->results, false, false
		);
		this->buildResultString();
		for (VisionObject &vo : this->results) {
			if (vo.type == "sprint-left") {
				ImageProcessing::drawRectangle(frame, vo.boundBox, Colors::green);
			}
			else {
				ImageProcessing::drawRectangle(frame, vo.boundBox, Colors::red);
			}
		}
		/*
		SprintMarker marker;
		if (
			FindSprintMarker::find(
				frame, outFrame, leftSprintMarkers, rightSprintMarkers, NULL, &marker, FindGrid::cameraParameters,
				*glob->configuration->currentCameraCalibration
			)
		)
		{
			ImageProcessing::drawCross(outFrame, marker.centerX, marker.centerY, Colors::cyan, 20);

			if ((this->sprintMode == "forward") && (marker.height >= frame->height / 2))
				this->sprintMode = "backward";

			vision::Text::draw(10, 10, this->sprintMode, Colors::cyan, outFrame);
		}
		else
		{
			cout << "Marker not found." << endl;
		}
		*/


		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
	}
	else if (ptype == GoalKeeper) {

//		this->findBall2TapeFrame->fill(Colors::black);
//		this->findBall2BallFrame->fill(Colors::black);
//		this->findBall2CalFrame->fill(Colors::black);
//
//		ColourDefinition
//			*grass = glob->configuration->getColour("grass"),
//			*tape = glob->configuration->getColour("tape"),
//			*ballCD = glob->configuration->getColour("cal");
//
//		assert(tape != NULL);
//
//		std::vector<VisionObject>
//			grassResults,
//			tapeResults,
//			ballResults;
//
//		ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 20, subsample, *grass, Colors::red, grassResults, false, false);
//		ImageProcessing::SegmentColours(frame, this->findBall2TapeFrame, 255, 5, 20, subsample, *tape, Colors::red, tapeResults, false, true);
//		if (FindBall::find2(frame, this->findBall2CalFrame, outFrame, tape, this->findBall2TapeFrame, ballCD, this->findBall2BallFrame, grassResults, tapeResults, ballResults))
//		{
//			for (VisionObject &ball : ballResults)
//			{
//				// ImageProcessing::drawCross(frame, ball.center.x, ball.center.y, Colors::green, ball.boundBox.width()/2);
//				ImageProcessing::drawRectangle(outFrame, ball.boundBox, Colors::green);
//			}
//			cout << "Ball found." << endl;
//		}
//		else
//			cout << "Ball not found." << endl;
//
//		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);

		// FindBall::find(frame, outFrame, 1, )

		ColourDefinition
				*ball = glob->configuration->getColour("ball");
//				*field = glob->configuration->getColour("field") //this will be used to make sure the ball is in the field
		assert(ball != NULL);

//		cout<<calib_const.Rx<<endl;

//		vector<CameraCalibrationData> calib = glob->configuration->cameraCalibrations;
//		for ( int i = 0; i < calib.size(); i++) {
//			cout << calib[i] << " \n";
//		}
// 		ImageProcessing::swapColours(
//				frame, 0, Rect(Point(0, 0), Point(outFrame->width, outFrame->height)), subsample, *ball,
//				mark
//		);

//		for (ColourDefinition &c : colours)
//		{
		this->results.clear();

		if (ball != nullptr) {

			int
					min_size_threshold = 30,
					max_size_threshold = 100000000;

			vector <VisionObject> ballCandidates;
			std::vector < VisionObject * > balls;

			ImageProcessing::segmentForBall(
					frame, outFrame, 50, 5, 20, min_size_threshold, max_size_threshold, Colors::red, subsample,
					ball, ballCandidates, true, true
			);

			if (FindBall::find(
					frame, outFrame, subsample, ball, ballCandidates, balls, this->vectorList, this->lastTime,
					*glob->configuration->currentCameraCalibration
			)) {
				VERBOSE("Ball found");
			}
			else {
				ERROR("Ball not found");
			}
		}

		if (glob->exportData) {
			Json::Value *json = new Json::Value();
			glob->prepareExportData(json);
		}
//		ImageProcessing::fillBlackBuffer(frame, outFrame, subsample);
        ImageProcessing::convertBuffer(frame, outFrame, subsample);
		cout << "Goal Keeper mode on" << endl;

	}
		/*else if (ptype == ObstacleRun)
        {
            ColourDefinition
                *obstacle = glob->configuration->getColour("obstacle"),
                *tape = glob->configuration->getColour("obstacle-tape"),
                *floor = glob->configuration->getColour("obstacle-floor"),
                *gate = glob->configuration->getColour("obstacle-gate");

            Rect floorArea;

            this->findBall2BallFrame->fill(Colors::black);

            if ((obstacle != nullptr) && (tape != nullptr) && (floor != nullptr) && (gate != nullptr))
            {
                this->results.clear();
                if (FindObstacles::find(frame, outFrame, this->findBall2BallFrame, floor, obstacle, gate, tape, this->results, floorArea))
                {
                    for (VisionObject &o : this->results)
                    {
                        ImageProcessing::drawCross(frame, o.center.x, o.center.y, Colors::green, o.boundBox.width() / 2);
                    }
                    cout << "Obstacle found." << endl;
                }
                else
                    cout << "Obstacles not found." << endl;
                this->buildResultString();
            }
            else
            {
                ERROR("Required colors were not found: obstacle, tape, floor, tape or gate.");
                ImageProcessing::convertBuffer(frame, outFrame, subsample);
            }
        }*/ else if (ptype == ObstacleRun) { //SoccerBallTracker (just doing temporary overwrite of other mode)
		/*Configuration config;
        {
            ifstream configFile;
            configFile.open("../../conf");
            config.UpdateConfiguration(configFile);
        }*/

		//std::vector<ColourDefinition> &colourDefs = config.colours;



		/*fb->initialize(glob->configuration->width, glob->configuration->height);
        outFb->initialize(glob->configuration->width, glob->configuration->height);
        calOutFb->initialize(glob->configuration->width, glob->configuration->height);
        tapeOutFb->initialize(glob->configuration->width, glob->configuration->height);
        ballOutFb->initialize(glob->configuration->width, glob->configuration->height);
        grassOutFb->initialize(glob->configuration->width, glob->configuration->height);
         */


		//tsai2D_define_camera_parameters(NEW_CAMERA, frame->width, frame->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);


		ColourDefinition
				*grass = glob->configuration->getColour("grass"),
				*ballCD = glob->configuration->getColour("cal"),
                *ball = glob->configuration->getColour("ball"),
				*tape = glob->configuration->getColour("tape");

		assert(tape != NULL);

		std::vector <VisionObject>
				grassResults,
				tapeResults,
				ballResults;
        this->results.clear();
		ImageProcessing::SegmentColours(frame, this->findBall2CalFrame, 255, 5, 20, subsample, *grass, Colors::red,
										grassResults, false, false);
		ImageProcessing::SegmentColours(frame, this->findBall2TapeFrame, 255, 5, 20, subsample, *tape, Colors::red,
										tapeResults, false, true);
        ImageProcessing::SegmentColours(frame, outFrame, 255, 5, 20, subsample, *ball,
                                        mark, ballResults, true, true);
        results = ballResults;
		if (FindBall::find2(frame, this->findBall2CalFrame, outFrame, tape, this->findBall2TapeFrame, ballCD,
							this->findBall2BallFrame, grassResults, tapeResults, ballResults)) {
			for (VisionObject &ball : ballResults) {
				ImageProcessing::drawCross(frame, ball.center.x, ball.center.y, Colors::green,
										   ball.boundBox.width() / 2);
			}
			cout << "Ball found." << endl;
		}
		else {
            cout << "Ball not found." << endl;
        }
        this->buildResultString();
        if (glob->exportData){
            Json::Value  *json = new Json::Value();
            glob->prepareExportData(json);
        }
        ImageProcessing::convertBuffer(frame, outFrame, subsample);
	}

	//  unsigned int threshold = mainWindow->sbThreshold->value();
	//unsigned int minimumLineLength = mainWindow->sbMinimumLineLength->value();
	//unsigned int maximumLineLength = mainWindow->sbMaximumLineLength->value();

	//unsigned int minimumSize = mainWindow->sbMinimumSize->value();

	if (ptype == Scanlines) {
		//      ImageProcessing::segmentScanLines( frame, outFrame, threshold, minimumLineLength, maximumLineLength, minimumSize, mark, subsample, 0 );
	}

	if (ptype == Segmentation) {
		//ColourDefinition target;
		//    getColourDefinition( & target );

		//ImageProcessing::segmentScanLines( frame, outFrame, threshold, minimumLineLength, maximumLineLength, minimumSize, mark, subsample, & target );
	}
	vision::Text::draw(20, 225, to_string(fpsRate), Colors::green, outFrame);
}

void VideoStream::run_trampoline(VideoStream *vs)
{
	vs->run();
}

VideoStream::~VideoStream()
{
	delete this->findMarkerFrame;
	if (device != 0)
	{
		device->stopCapture();
		delete device;
	}
}

int VideoStream::sendImage(FrameBuffer *img)
{
	Globals *glob = Globals::GetGlobals();

	if (glob->GetClientRequest() == true)
	{
#ifdef VERBOSE__VIDEOSTREAM
		std::cout << "Framebuffer type " << img->type() << std::endl;
#endif
		if (img->type() == FrameBuffer::RGB24BE)
		{
			//	  memcpy(img->buffer, img->buffer, img->frameSize);

			glob->LockBuffer();

			unsigned int size = img->ConvertToJpeg(glob->GetBuffer(), glob->GetSize(), 80);
			if (size == 0)
			{
				std::cerr << "Conversion of framebuffer to jpeg failed" << std::endl;
			}
			glob->SetSize(size);

			glob->UnlockBuffer();
		}
		else
			std::cerr << "Unable to deal with this image type" << std::endl;
		glob->SetClientRequest(false);
	}

	glob->LockBuffer();
	glob->BroadcastBuffer();
	glob->UnlockBuffer();

	return 0;
}

/*  
void * 
VideoStream::server_thread(void * arg)
{
  HTTPD::server_thread(arg);
  return NULL;
}
*/

// Make sure that these match the ProcessType enum above.
/*
static char const *processTypeStrings[] =
	{
		"raw",
		"showcolours",
		"segmentcolours",
		"scanlines",
		"segmentation",
		"calibrationprocessing",
		"findmarkers",
		"followline",
		"marathon"
	};
*/

// Must match the enum VideoControl

char const *VideoControlStrings[] =
	{
		"illegal control",
		"brightness",
		"hue",
		"saturation",
		"contrast",
		"sharpness",
		"gain"
	};

char const *UpdateCameraCalibrationStrings[] =
	{
		"x_origin",
		"y_origin",
		"x_distance",
		"y_distance",
		"cols",
		"rows",
		"trapezoid0",
		"trapezoid1",
		"trapezoid2",
		"trapezoid3",
		"clustering_threshold",
		"brightness_threshold"
	};


/*
/ * Assume all legal values are positive. -1 specifices the default value * /
int VideoStream::CommandVideoControl(VideoStream *video, char const *command, char *response, unsigned int respLength)
{
	int ret = COMMAND_ERR_COMMAND;
	char const *s;
	enum VideoControl vcontrol = IllegalControl;
	int val = -2;
	char const *control;

	response[0] = '\0';

	if ((s = strstr(command, "command=videocontrol")) != NULL)
	{
		if ((s = strstr(s, "control=")) != NULL)
		{
			s = s + strlen("control=");
			control = 0;
			for (unsigned int i = 0; i < sizeof(VideoControlStrings) / sizeof(VideoControlStrings[0]); ++i)
			{
				if (!strncmp(VideoControlStrings[i], s, strlen(VideoControlStrings[i])))
				{
					control = VideoControlStrings[i];
					vcontrol = static_cast<enum VideoControl>( i );
					s = s + strlen(control);
					break;
				}
			}

			if (control != 0)
			{
				if ((s = strstr(s, "value=")) != NULL)
				{
					s = s + strlen("value=");
					if (!strncmp("query", s, strlen("query")))
					{
						val = -2;
						ret = COMMAND_ERR_OK;
					}
					else if (sscanf(s, "%d", &val) != 1)
					{
						ret = COMMAND_ERR_PARAMETER;
						val = -2;
					}
				}

				if (val >= -1)
				{
					int err;
					switch (vcontrol)
					{
						case Brightness:
							err = video->device->SetBrightness(val);
							if (err >= 0)
							{
								ret = COMMAND_ERR_OK;
							}
							break;
						case Contrast:
							err = video->device->SetContrast(val);
							if (err >= 0)
							{
								ret = COMMAND_ERR_OK;
							}
							break;
						case Saturation:
							err = video->device->SetSaturation(val);
							if (err >= 0)
							{
								ret = COMMAND_ERR_OK;
							}
							break;
						case Sharpness:
							err = video->device->SetSharpness(val);
							if (err >= 0)
							{
								ret = COMMAND_ERR_OK;
							}
							break;
						case Gain:
							err = video->device->SetGain(val);
							if (err >= 0)
							{
								ret = COMMAND_ERR_OK;
							}
							break;
						default:
							ret = COMMAND_ERR_COMMAND;
							break;
					}
				}

				if (ret == COMMAND_ERR_OK)
				{
					int v;

					switch (vcontrol)
					{
						case Brightness:
							v = video->device->GetBrightness();
							if (v < 0)
							{
								ret = COMMAND_ERR_COMMAND;
							}
							break;
						case Contrast:
							v = video->device->GetContrast();
							if (v < 0)
							{
								ret = COMMAND_ERR_COMMAND;
							}
							break;
						case Saturation:
							v = video->device->GetSaturation();
							if (v < 0)
							{
								ret = COMMAND_ERR_COMMAND;
							}
							break;
						case Sharpness:
							v = video->device->GetSharpness();
							if (v < 0)
							{
								ret = COMMAND_ERR_COMMAND;
							}
							break;
						case Gain:
							v = video->device->GetGain();
							if (v < 0)
							{
								ret = COMMAND_ERR_COMMAND;
							}
							break;
						default:
							ret = COMMAND_ERR_COMMAND;
							break;
					}

					if (v >= 0)
					{
						snprintf(response, respLength - 1, "control=%s&value=%d", control, v);
					}
				}
			}
		}
	}
	return ret;
}
*/

enum VideoStream::ProcessType VideoStream::GetMode(void) const
{
	return mode;
}

void VideoStream::SetMode(enum ProcessType m)
{
	mode = m;
}

void VideoStream::UpdateColour(ColourDefinition const col)
{
/*
	for (vector<ColourDefinition>::iterator i = colours.begin(); i != colours.end(); ++i)
	{
		if ((*i).name == col.name)
		{
			(*i).min = col.min;
			(*i).max = col.max;
			break;
		}
	}
*/
}

void
VideoStream::SetColours(std::vector<ColourDefinition> colours)
{
	// this->colours = colours;
}

std::string
VideoStream::ReadRunningConfiguration(void)
{
	Globals *glob = Globals::GetGlobals();

	Configuration config;
	config.cameraCalibrations = glob->configuration->cameraCalibrations;
	// General options
	config.subsample = GetSubsample();

	// Camera options
	config.deviceVideo = device->GetDeviceName();

	if (device != 0)
	{
		config.width = device->GetWidth();
		config.height = device->GetHeight();
		config.depth = device->GetDepth();
		config.brightness = device->GetBrightness();
		config.contrast = device->GetContrast();
		config.saturation = device->GetSaturation();
		config.sharpness = device->GetSharpness();
		config.gain = device->GetGain();
	}

	// HTTP options
	config.httpPort = glob->GetHTTPDServer()->GetHTTPPort();
	config.httpAddr = glob->GetHTTPDServer()->GetHTTPAddr();
	config.docroot = glob->GetHTTPDServer()->GetDocRoot();
	config.index = glob->GetHTTPDServer()->GetIndex();

	// Colour options
	config.colours.clear();

	/*
	for (vector<ColourDefinition>::iterator i = this->colours.begin();
		 i != this->colours.end();
		 ++i)
	{
		config.colours.push_back((*i).ToString());
	}
	 */

	// Serial options
	if (glob->GetSerial() != 0)
	{
		config.deviceSerial = glob->GetSerial()->GetDeviceName();
		config.baudrate = Serial::ConvertBaudrateToString(glob->GetSerial()->GetBaudrate());
	}

	std::stringstream os;
	os << config;
	return os.str();
}

unsigned int
VideoStream::GetSubsample(void) const
{
	return subsample;
}

void
VideoStream::SetSubsample(unsigned int subsample)
{
	this->subsample = subsample;
}

void VideoStream::buildResultString()
{
	Globals *glob = Globals::GetGlobals();

	std::lock_guard<std::mutex> lock(glob->exportDataMutex);
	this->resultString.clear();
	std::ostringstream strstream;
	strstream << "{\"success\":true,\"data\":[";
	unsigned int i = 0;
	for (VisionObject &vo : this->results)
	{
		VERBOSE(vo.type);
		if (++i > 1)
			strstream << ",";
		strstream << &vo;
	}
	strstream << "]}";
	this->resultString = strstream.str();
	VERBOSE("Exporting: " << this->resultString);
}


void VideoStream::buildResultString(std::vector<ObstacleVisionObject> &objects)
{
	Globals *glob = Globals::GetGlobals();

	std::lock_guard<std::mutex> lock(glob->exportDataMutex);
	this->resultString.clear();
	std::ostringstream strstream;
	strstream << "{\"success\":true,\"data\":[";
	unsigned int i = 0;
	for (VisionObject &vo : objects)
	{
		VERBOSE(vo.type);
		if (++i > 1)
			strstream << ",";
		strstream << &vo;
	}
	strstream << "]}";
	this->resultString = strstream.str();
	VERBOSE("Exporting: " << this->resultString);
}

void VideoStream::buildResultString(FindMarkerResult &result)
{
	Globals *glob = Globals::GetGlobals();

	std::lock_guard<std::mutex> lock(glob->exportDataMutex);
	this->resultString.clear();
	std::ostringstream strstream;
	strstream << "{\"success\":true,\"data\":[";
	strstream << "{\"type\":\"line\", \"a\":{\"x\":" << result.line.a.x << ",\"y\":" << result.line.a.y << "},\"b\":{\"x\":" << result.line.b.x << ",\"y\":" << result.line.b.y << "}},";
	strstream << "{\"type\":\"marker\", \"marker\":\"" << result.marker << "\",\"x\":" << result.markerCenter.x << ", \"y\": " << result.markerCenter.y << "}";
	strstream << "]}";
	this->resultString = strstream.str();
	VERBOSE("Exporting: " << this->resultString);
}
