/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date April 2, 2015.
 */

#ifndef _VISIONMODULE_COMPILATIONDEFINITIONS_H_
#define _VISIONMODULE_COMPILATIONDEFINITIONS_H_

#define FINDMARKER__OUTPUTDRAW

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#ifdef DEBUG
#define VERBOSE_HEADER "[VERBOSE] " << __FILENAME__ << ":" << __LINE__ << ") "
#define VERBOSE(x) std::cout << VERBOSE_HEADER << x << std::endl
#define VERBOSENEL(x) std::cout << VERBOSE_HEADER << x
#define VERBOSEDATA(d, s) Interface::dumpData(d, s)
#define VERBOSEB(x) std::cout << x << std::endl
#define VERBOSEBNEL(x) std::cout << x
#else
#define VERBOSE_HEADER
#define VERBOSE(x)
#define VERBOSENEL(x)
#define VERBOSEDATA(d, s)
#define VERBOSEB(x)
#define VERBOSEBNEL(x)
#endif

#define ERROR_HEADER "[ERROR] " << __FILENAME__ << ":" << __LINE__ << ") "
#define ERROR(x) std::cout << ERROR_HEADER << x << std::endl
#define ERRORNEL(x) std::cout << ERROR_HEADER << x
#define ERRORB(x) std::cout << x << std::endl
#define ERRORBNEL(x) std::cout << x

#define R2D(a) (180.0*(a)/M_PIl)
#define D2R(a) (M_PIl*(a)/180.0)

// #define AUTMAN

#endif //_VISIONMODULE_COMPILATIONDEFINITIONS_H_
