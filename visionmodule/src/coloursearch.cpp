/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#include "coloursearch.h"


using namespace std;

vision::ColourSearchParams::ColourSearchParams()
{ }

vision::ColourSearchParams::ColourSearchParams(unsigned int subsample, unsigned int min,
	unsigned int max, unsigned int threshold, ColourDefinition *colour, RawPixel *mark): subsample(subsample), min(min),
	max(max), threshold(threshold), colour(colour), mark(mark)
{ }
