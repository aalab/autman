/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#include "line.h"
#include "compilationdefinitions.h"

std::string Line2D::line2DClassName("Line2D");

Line2D::Line2D()
{ }

Line2D::Line2D(Point2D &pa, Point2D &pb)
	: a(pa), b(pb)
{ }

Line2D::Line2D(double paX, double paY, double pbX, double pbY)
	: a(paX, paY), b(pbX, pbY)
{ }

double Line2D::hypot()
{
	int
		_x = (this->b.x - this->a.x),
		_y = (this->b.y - this->a.y);
	return std::sqrt(_x*_x + _y*_y);
}

double Line2D::inclination()
{
	return std::atan2((this->b.y - this->a.y), (this->b.x - this->a.x));
}

std::string &Line2D::serializeClassName()
{
	return line2DClassName;
}

double Line2D::xFromY(double y)
{
	double d = (y - this->a.y)/std::sin(this->inclination());
	return this->a.x + std::cos(this->inclination())*d;
}

double Line2D::yFromX(double x)
{
	double d = (x - this->a.x)/std::cos(this->inclination());
	return this->a.y + std::sin(this->inclination())*d;
}

double Line2D::slope()
{
	return std::tan(this->inclination());
}
