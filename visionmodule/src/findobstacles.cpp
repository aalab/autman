/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 03, 2015
 */

#include "findobstacles.h"
#include "../libvideo/imageprocessing.h"
#include "colors.h"
#include "compilationdefinitions.h"

std::string ObstacleVisionObject::className = "ObstacleVisionObject";

bool FindObstacles::find(
	FrameBuffer *fb, FrameBuffer *outFb, FrameBuffer *outFloorFb, ColourDefinition *floorCD,
	ColourDefinition *obstacleCD, ColourDefinition *gateCD, ColourDefinition *tapeCD,
	std::vector<VisionObject> &obstacleResults, Rect &floorArea
)
{
	std::vector<VisionObject> obstacles;
	std::vector<VisionObject> gates;
	std::vector<VisionObject> tapes;
	std::vector<VisionObject> floors;

	// ImageProcessing::SegmentColours(fb, outFloorFb, 255, 5, 10, 1, *floorCD, Colors::green, floors, true, false);

	{
		// outFb->fill(Colors::red);

		// floorArea = floors[0].boundBox;
		// outFb->fill(floorArea, Colors::black);
		outFb->fill(Colors::black);
		ImageProcessing::SegmentColours(fb, outFb, 255, 5, 10, 1, *obstacleCD, Colors::blue, obstacles, true, false);
		ImageProcessing::SegmentColours(fb, outFb, 255, 5, 10, 1, *tapeCD, Colors::yellow, tapes, true, false);
		ImageProcessing::SegmentColours(fb, outFb, 255, 5, 10, 1, *gateCD, Colors::red, obstacles, true, false);

		for (VisionObject &o : obstacles)
		{
			obstacleResults.emplace_back(o);
			// getShapeInfo(fb, outFb, o, obstacleResults.back());

			/*
			for (Point2D &p : obstacleResults.back().shape)
			{
				ImageProcessing::drawCross(fb, p.x, p.y, Colors::green, 10);
			}
			 */
		}
	}

	return (!obstacleResults.empty());
}

void FindObstacles::getShapeInfo(FrameBuffer *fb, FrameBuffer *outFb, VisionObject &vo, ObstacleVisionObject &wcvo)
{
	int
		x, y;
	bool startNotFound = true;
	float ultSlope, slope;
	RawPixel currentColor;
	Point2D tmpP;
	for (
		x = vo.boundBox.topLeft().x();
		(x < vo.boundBox.bottomRight().x()) && (startNotFound);
		x++
		)
	{
		for (
			y = vo.boundBox.bottomRight().y();
			(y > vo.boundBox.topLeft().y()) && (startNotFound);
			y--
		)
		{
			outFb->getPixel(y, x, &currentColor);
			if (currentColor.isSameColor(&vo.seedColour))
			{
				startNotFound = true;
				tmpP.x = x;
				tmpP.y = y;
				ultSlope = 0;
				wcvo.shape.emplace_back(x, y);
				for (x = x+1; (x < vo.boundBox.bottomRight().x()); x += 3)
				{
					// VERBOSE("---------");
					// VERBOSE("x " << x << ", y " << y);
					outFb->getPixel(y, x, &currentColor);
					// VERBOSE("(" << vo.seedColour.red << ", " << vo.seedColour.green << ", " << vo.seedColour.blue << ")");
					// VERBOSE("(" << currentColor.red << ", " << currentColor.green << ", " << currentColor.blue << ")");
					if (currentColor.isSameColor(&vo.seedColour))
					{
						// VERBOSE("SAME");
						for (
							y = y+1, outFb->getPixel(y, x, &currentColor);
							(y < vo.boundBox.bottomRight().y()) && currentColor.isSameColor(&vo.seedColour);
							outFb->getPixel(++y, x, &currentColor)
						)
						{
							// VERBOSE("+ " << y);
						}
					}
					else
					{
						// VERBOSE("NOT SAME");
						for (
							y = y, outFb->getPixel(y, x, &currentColor);
							(y >= vo.boundBox.topLeft().y()) && (!currentColor.isSameColor(&vo.seedColour));
							outFb->getPixel(--y, x, &currentColor)
							)
						{
							// VERBOSE("- " << y);
						}
					}
					ImageProcessing::drawCross(fb, x, y+1, Colors::red, 3);
					slope = ((y - tmpP.y) / (x - tmpP.x));
					float anglediff = std::atan(slope - ultSlope);
					if (
						///(std::abs(std::atan(slope)) < D2R(45))
						(
							((ultSlope > 0) && (slope < 0))
							|| ((ultSlope < 0) && (slope > 0))
						)
						|| (std::abs(anglediff) > 10)
					)
					{
						wcvo.shape.emplace_back(x, y);
					}
					tmpP.x = x;
					tmpP.y = y;
					ultSlope = slope;
				}
				wcvo.shape.emplace_back(x, y);
			}
		}
	}
}

std::ostream &ObstacleVisionObject::writeJsonProperties(std::ostream &os)
{
	BaseVisionObject2D::writeJsonProperties(os);
	os << ",\"shape\":[";
	unsigned int i = 0;
	for (Point2D &s : this->shape)
	{
		if (i++ > 0)
			os << ",";
		os << s;
	}
	os << "]";
	return os;
}

std::string &ObstacleVisionObject::serializeClassName()
{
	return className;
}
