/* 
 * A class that encapsulates all configuration information
 * Jacky Baltes <jacky@cs.umanitoba.ca> Tue Jan  8 23:20:07 CST 2013
 *
 */

#include "configuration.h"
#include "../libvideo/colourdefinition.h"
#include <boost/program_options.hpp>
#include <json/reader.h>
#include <json/writer.h>
#include "http/json.h"

namespace po = boost::program_options;

Configuration::Configuration()
	: httpThreads(5), currentColour(nullptr)
{
	po::options_description generalOptions("General Options");
	generalOptions.add_options()
		("subsample", po::value<unsigned int>()->default_value(1), "sub sample");
	generalOptions.add_options()
		("udp_port", po::value<unsigned int>()->default_value(0), "udp port");

	po::options_description cameraOptions("Camera Options");
	cameraOptions.add_options()
		("video_device,d", po::value<string>()->default_value("/dev/video0"), "video device name")
		("width,w", po::value<unsigned int>()->default_value(320), "width")
		("height,h", po::value<unsigned int>()->default_value(240), "height")
		("depth", po::value<unsigned int>()->default_value(24), "depth")
		("brightness", po::value<int>()->default_value(-1), "brightness")
		("contrast", po::value<int>()->default_value(-1), "contrast")
		("saturation", po::value<int>()->default_value(-1), "saturation")
		("sharpness", po::value<int>()->default_value(-1), "sharpness")
		("gain", po::value<int>()->default_value(-1), "gain");

	//joel:
	po::options_description tsaiCalibrationConstants("Tsai Calibration Constants");
	tsaiCalibrationConstants.add_options()
		("f", po::value<double>()->default_value(0.0), "f")
		("kappa1", po::value<double>()->default_value(0.0), "kappa1")
		("p1", po::value<double>()->default_value(0.0), "p1")
		("p2", po::value<double>()->default_value(0.0), "p2")
		("Tx", po::value<double>()->default_value(0.0), "Tx")
		("Ty", po::value<double>()->default_value(0.0), "Ty")
		("Tz", po::value<double>()->default_value(0.0), "Tz")
		("Rx", po::value<double>()->default_value(0.0), "Rx")
		("Ry", po::value<double>()->default_value(0.0), "Ry")
		("Rz", po::value<double>()->default_value(0.0), "Rz")
		("r1", po::value<double>()->default_value(0.0), "r1")
		("r2", po::value<double>()->default_value(0.0), "r2")
		("r3", po::value<double>()->default_value(0.0), "r3")
		("r4", po::value<double>()->default_value(0.0), "r4")
		("r5", po::value<double>()->default_value(0.0), "r5")
		("r6", po::value<double>()->default_value(0.0), "r6")
		("r7", po::value<double>()->default_value(0.0), "r7")
		("r8", po::value<double>()->default_value(0.0), "r8")
		("r9", po::value<double>()->default_value(0.0), "r9")
		("is_load_constants", po::value<int>()->default_value(0), "is_load_constants");

	po::options_description httpOptions("Http Server Options");
	httpOptions.add_options()
		("http_port", po::value<unsigned int>()->default_value(8080)->required(), "http port number")
		("http_addr", po::value<string>()->default_value("0.0.0.0")->required(), "http address")
		("docroot", po::value<string>()->default_value("www/")->required(), "http document root")
		("index", po::value<string>()->default_value("index.html"), "index.html file name");

	po::options_description colourOptions("Colour Options");
	colourOptions.add_options()
		("colour", po::value<vector<string> >(), "colour definition");

	po::options_description serialOptions("Serial Port Options");
	serialOptions.add_options()
		("serial_device", po::value<string>()->default_value(""), "serial device name or empty for no serial port output")
		("baudrate", po::value<string>()->default_value("B115200"), "baudrate");

	options.add(generalOptions).add(cameraOptions).add(httpOptions).add(tsaiCalibrationConstants).add(colourOptions).add(serialOptions);
}

void
Configuration::UpdateConfiguration(std::string configStr)
{
	std::istringstream is(configStr);

	std::cout << ">>>>>>>>>> UpdateConfiguration" << std::endl;
	std::cout << configStr << std::endl;
	std::cout << "<<<<<<<<<< UpdateConfiguration" << std::endl;

	UpdateConfiguration(is);
}

void
Configuration::UpdateConfiguration(std::istream &iconfig)
{
	Json::Value json;
	Json::Reader reader;
	if (reader.parse(iconfig, json))
		this->UpdateConfiguration(json);
	else
	{
		cerr << "Error parsing the config file." << endl;
		exit(3);
	}
}

void Configuration::UpdateConfiguration(Json::Value &json)
{
	*this << json;

	cout << "Configuration: " << endl;
	cout << json << endl << "----------------------------" << endl;
}

void Configuration::UpdateConfiguration(po::variables_map const &vm)
{
	// General options
	subsample = vm["subsample"].as<unsigned int>();
	udp_port = vm["udp_port"].as<unsigned int>();

	// Camera options
	deviceVideo = vm["video_device"].as<std::string>();
	width = vm["width"].as<unsigned int>();
	height = vm["height"].as<unsigned int>();
	depth = vm["depth"].as<unsigned int>();

	brightness = vm["brightness"].as<int>();
	contrast = vm["contrast"].as<int>();
	saturation = vm["saturation"].as<int>();
	sharpness = vm["sharpness"].as<int>();
	gain = vm["gain"].as<int>();

	// Tsai calibration constants

/*
	try
	{
		calibrationData.getResult().f = vm["f"].as<double>();
		calibrationData.getResult().kappa1 = vm["kappa1"].as<double>();
		calibrationData.getResult().p1 = vm["p1"].as<double>();
		calibrationData.getResult().p2 = vm["p2"].as<double>();
		calibrationData.getResult().Tx = vm["Tx"].as<double>();
		calibrationData.getResult().Ty = vm["Ty"].as<double>();
		calibrationData.getResult().Tz = vm["Tz"].as<double>();
		calibrationData.getResult().Rx = vm["Rx"].as<double>();
		calibrationData.getResult().Ry = vm["Ry"].as<double>();
		calibrationData.getResult().Rz = vm["Rz"].as<double>();
		calibrationData.getResult().r1 = vm["r1"].as<double>();
		calibrationData.getResult().r2 = vm["r2"].as<double>();
		calibrationData.getResult().r3 = vm["r3"].as<double>();
		calibrationData.getResult().r4 = vm["r4"].as<double>();
		calibrationData.getResult().r5 = vm["r5"].as<double>();
		calibrationData.getResult().r6 = vm["r6"].as<double>();
		calibrationData.getResult().r7 = vm["r7"].as<double>();
		calibrationData.getResult().r8 = vm["r8"].as<double>();
		calibrationData.getResult().r9 = vm["r9"].as<double>();
		calibrationData.getResult().is_load_constants = vm["is_load_constants"].as<int>();
	}
	catch (const std::exception &e)
	{
		//ignore
	}
 */

	// HTTPD options
	httpPort = vm["http_port"].as<unsigned int>();
	httpAddr = vm["http_addr"].as<std::string>();
	docroot = vm["docroot"].as<std::string>();
	index = vm["index"].as<std::string>();

	/*
	// Colour options
	if (vm.count("colour") > 0)
	{
		colours = vm["colour"].as<std::vector<string> >();
	}
	*/

	// Serial options
	deviceSerial = vm["serial_device"].as<std::string>();
	baudrate = vm["baudrate"].as<std::string>();
}

std::ostream& operator<<(std::ostream &os, Configuration const &config)
{
	Json::Value json;
	Json::FastWriter writer;
	os << writer.write(json);
	return os;
}

ColourDefinition* Configuration::getColour(std::string name)
{
	for (auto &c : this->colours)
	{
		if (c.name == name)
			return &c;
	}
	return NULL;
}

CameraCalibrationData *Configuration::cameraCalibrationByName(std::string name)
{
	for (CameraCalibrationData &cc : this->cameraCalibrations)
	{
		if (cc.name == name)
			return &cc;
	}
	return nullptr;
}

bool Configuration::removeCameraCalibration(std::string name)
{
	for (
		vector<CameraCalibrationData>::iterator it = this->cameraCalibrations.begin();
		it != this->cameraCalibrations.end();
		++it
	)
	{
		if (it->name == name)
		{
			if (this->currentCameraCalibration == &(*it))
			{
				if (this->cameraCalibrations.size() > 1)
					this->currentCameraCalibration = &this->cameraCalibrations[0];
				else
					this->currentCameraCalibration = nullptr;
			}
			this->cameraCalibrations.erase(it);
			return true;
		}
	}
	return false;
}

bool Configuration::removeCameraCalibration(unsigned int index)
{
	if (this->cameraCalibrations.size() > index)
	{
		this->cameraCalibrations.erase(this->cameraCalibrations.begin() + index);
		return true;
	}
	return false;
}

config::Server &config::Server::operator<<(const Json::Value &json)
{
	this->hostName = json["host"].asString();
	this->port = json["port"].asUInt();
	if (json["timeout"].isUInt())
		this->timeout = json["timeout"].asUInt();
	return *this;
}

config::Server::Server()
	: timeout(50)
{ }
