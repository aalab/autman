#include "httpd.h"
#include "httpdthread.h"
#include "httpdserverthread.h"
#include "videostream.h"
#include "serial.h"
#include "configuration.h"
#include "globals.h"
#include "udpvisionserver.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <boost/program_options.hpp>
#include <arpa/inet.h>
#include <json/value.h>
#include <json/reader.h>
#include <thread>

#include "../libvideo/videodevice.h"

#include "http/json.h"
#include "compilationdefinitions.h"

using namespace std;

namespace po = boost::program_options;

int ApplyConfiguration(Configuration &configuration);

static char progname[256];

void CatchHUPSignal(int num)
{
	signal(SIGHUP, CatchHUPSignal);
	int err;

	if ((err = execl(progname, "-c", "./www/__config__.cfg", NULL)) < 0)
	{
		perror("execl failed:");
		exit(10);
	}
}

int main(int argc, char **argv)
{
	string config_file;
	Configuration configuration;
	//  Globals * glob = Globals::GetGlobals();
	Globals::GetGlobals()->configuration = &configuration;

	strncpy(progname, argv[0], 255);
	progname[255] = '\0';

	try
	{
		// Declare a group of options that will be
		// allowed only on command line
		po::options_description commandLineOnlyOptions("Command Line Options");
		commandLineOnlyOptions.add_options()
			("version,v", "print version string")
			("help", "produce help message")
			("config,c", po::value<string>(&config_file)->default_value(""), "config file name");

		po::options_description commandLineOptions;
		commandLineOptions.add(commandLineOnlyOptions).add(configuration.options);
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, commandLineOptions), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOptions << "\n";
			return 1;
		}

		if (vm.count("version"))
		{
			cout << "Vision Module: Version " << __PROJECT_VERSION__ << " (compilation date: " << __DATE__ << " " << __TIME__ << ")" << endl << endl;
			return 1;
		}

		/*
		if (config_file != "")
		{
			ifstream ifs(config_file.c_str());

			po::store(po::parse_config_file(ifs, configuration.options), vm);
			po::notify(vm);
		}
		*/

		if (config_file != "")
		{
			Json::Value configJson;
			Json::Reader reader;
			ifstream ifs(config_file);
			if (reader.parse(ifs, configJson))
				configuration.UpdateConfiguration(configJson);
			else
			{
				ERROR("Error parsing the configuration file.");
				ERRORB("\t" << reader.getFormattedErrorMessages());
				exit(3);
			}
		}

		std::cout << configuration;

		ApplyConfiguration(configuration);
	}
	catch (exception &e)
	{
		cerr << e.what() << endl;
		return 1;
	}
}

int ApplyConfiguration(Configuration &configuration)
{
	string driver;
	Globals *glob = Globals::GetGlobals();

	if (configuration.deviceVideo.substr(0, 10) == "/dev/video")
	{
		driver = "V4L2";
	}
	else if (configuration.deviceVideo.substr(0, 5) == "file:")
	{
		driver = "File";
		configuration.deviceVideo = configuration.deviceVideo.substr(5, configuration.deviceVideo.length());
	}
	else if (configuration.deviceVideo.substr(0, 11) == "/dev/dc1394")
	{
		driver = "DC1394";
	}
	else
	{
		ERROR("Unable to determine video capture device driver " << configuration.deviceVideo);
		return 1;
	}

	VERBOSE("device " << configuration.deviceVideo << ", driver " << driver << ", width " << configuration.width << ", height " << configuration.height);

	if (configuration.teleop)
	{
		VERBOSE("Initializing teleop service proxy to: " << configuration.teleop->hostName << ":" << configuration.teleop->port);
		glob->teleopService.reset(new services::Teleop(configuration.teleop->hostName, configuration.teleop->port));
		glob->teleopService->defaultTimeout(boost::posix_time::milliseconds(configuration.teleop->timeout));
	}

	if (configuration.robot)
	{
		VERBOSE("Initializing robot service proxy to: " << configuration.robot->hostName << ":" << configuration.robot->port);
		glob->robotService.reset(new services::PublicClient(configuration.robot->hostName, configuration.robot->port));
		glob->robotService->defaultTimeout(boost::posix_time::milliseconds(configuration.robot->timeout));
	}

	string input("default");
	string standard("default");
	unsigned int numBuffers = 3;
	unsigned int fps = 30;

	glob->SetBuffer((uint8_t *) malloc(configuration.width * configuration.height * configuration.depth / 8), configuration.width * configuration.height * configuration.depth / 8);

	VERBOSE("Using driver: " << driver);

	VideoStream *video = new VideoStream(
		driver, configuration.deviceVideo, input, standard, fps, configuration.width, configuration.height,
		configuration.depth, numBuffers, configuration.subsample, configuration.brightness, configuration.contrast,
		configuration.saturation, configuration.sharpness, configuration.gain
	);

	glob->SetVideo(video);

	Serial *serial = 0;
	if (configuration.deviceSerial != "")
	{
		serial = new Serial(configuration.deviceSerial,
			Serial::ConvertStringToBaudrate(configuration.baudrate));
		if ((serial == 0) || (!serial->isValid()))
		{
			serial = 0;
		}
	}

	glob->SetSerial(serial);

	video->nextColours = 0;
	video->SetColours(configuration.colours);

#if defined(DEBUG)
	cout << "Starting video thread" << endl;
#endif

	HTTPD *server = new HTTPD(
		configuration.httpPort, configuration.httpAddr.c_str(), NULL, configuration.docroot.c_str(),
		configuration.index.c_str()
	);
	glob->SetHTTPDServer(server);

	HTTPDServerThread *thread = new HTTPDServerThread(server);

	glob->SetHTTPDServerThread(thread);
	thread->StartAndDetach();

	cout << "Starting video processing thread" << endl;

	pthread_create(&(video->threadID), NULL, (void *(*)(void *)) video->run_trampoline, static_cast<void *>( video ));
	pthread_detach(video->threadID);

	//


	/*
	http::Server s(configuration);
	s.start();
	*/

	signal(SIGHUP, CatchHUPSignal);

	if (configuration.udp_port > 0)
	{
		boost::asio::io_service io_service;

		UDPVisionServer *vs = new UDPVisionServer(io_service, configuration.udp_port, video);
		vs->StartServer();   // Does not return right now
	}
}
