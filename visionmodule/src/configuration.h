/* 
 * A class that encapsulates all configuration information
 * Jacky Baltes <jacky@cs.umanitoba.ca> Tue Jan  8 23:20:07 CST 2013
 *
 */

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <string>
#include <vector>
#include <ostream>
#include <json/value.h>
#include <boost/program_options.hpp>
#include "markerdescriptor.h"
#include "cameracalibrationdata.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "trapezoid.h"

using namespace std;
namespace po = boost::program_options;

class ConfigurationFindMarker
{
public:
	/*
	 * Marker size.
	 */
	double size;

	/*
	 * Amount which defines how open the lookup angle in the IntegralCircle. Ex: 90 - angle and 90 + angle
	 *
	 * TODO: Check with Joel to describe this better.
	 */
	double angle;

	/*
	 * Radius of the internal circle in the marker detection.
	 */
	double internalRadius;

	/*
	 * Radius of the external circle in the marker detection.
	 */
	double externalRadius;

	/*
	 * Radius of the extra1 circle in the marker detection.
	 */
	double extra1Radius;
	/*
	 * Offset used to find the top/bottom of the line. Used in the refinement as well.
	 */
	int lineOffset;
};

namespace config
{
	class Server
	{
	public:
		Server();

		std::string hostName;
		unsigned int port;
		unsigned int timeout;

		Server& operator<<(const Json::Value &json);
		Server& operator>>(const Json::Value &json);
	};
}

class Configuration
{
public:
	Configuration();

public:
	void UpdateConfiguration(po::variables_map const &vm);
	void UpdateConfiguration(Json::Value &json);
	void UpdateConfiguration(std::string configStr);
	void UpdateConfiguration(std::istream &iconfig);

public:
	// General options
	unsigned int subsample;
	unsigned int udp_port;

	// Camera options
	string deviceVideo;

	unsigned int width;
	unsigned int height;
	unsigned int depth;
	int brightness;
	int contrast;
	int saturation;
	int sharpness;
	int gain;

	// HTTPD options
	unsigned int httpPort;
	string httpAddr;
	unsigned int httpThreads;

	string docroot;
	string index;

	// Colour options
	// vector<string> colours;
	vector<ColourDefinition> colours;
	vector<MarkerDescriptor> markers;

	// Serial options
	string deviceSerial;
	string baudrate;

	std::vector<CameraCalibrationData> cameraCalibrations;
	CameraCalibrationData* currentCameraCalibration;

	FrameBuffer *frame;

	bool flipImage;

	po::options_description options;

	/*
	 * Configuration for the marker identification.
	 */
	ConfigurationFindMarker marker;

	config::Server controlModule;

	std::unique_ptr<config::Server> teleop;

	std::unique_ptr<config::Server> robot;

	friend std::ostream &operator<<(std::ostream &os, Configuration const &config);
public:
	ColourDefinition* getColour(std::string name);

	CameraCalibrationData *cameraCalibrationByName(std::string name);
	bool removeCameraCalibration(std::string name);
	bool removeCameraCalibration(unsigned int index);

	ColourDefinition *currentColour;
};

#endif /* __CONFIGURATION_H__ */
