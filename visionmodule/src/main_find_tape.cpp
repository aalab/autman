/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Fri Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "videostream.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "http/json.h"
#include "globals.h"
#include "colors.h"
#include "findline.h"

/*
 * Load a data file dumped from a frame.
 * Do the identification of the shapes by the colour.
 * Locates the marker.
 */
int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		cout << "Invalid params. Usage: " << argv[0] << " <config> <raw_image>" << endl;
		exit(2);
	}
	FrameBuffer
		*fbOriginal = new FrameBufferRGB24BE(),
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	fbOriginal->initialize(config.width, config.height);
	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);

/*
	fbOriginal->initialize(10, 1);
	fb->initialize(10, 1);
	outFb->initialize(10, 1);
*/

	unsigned int subsample = config.subsample;

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	{
		fbOriginal->inFromPPM(argv[2]);
		memcpy(fb->buffer, fbOriginal->buffer, fbOriginal->frameSize);
	}

	std::vector<ColourDefinition> colourDefs;
	ColourDefinition *tapeColour = config.getColour("tape");

	if (tapeColour == NULL)
	{
		cerr << "Tape colour was not found." << endl;
		exit(1);
	}

	std::vector<VisionObject> tapes, tapes2;
	ImageProcessing::SegmentColours(fb, outFb, 255, 5, 10, subsample, *tapeColour, Colors::pink, tapes, false, false);
	// ImageProcessing::segmentScanLines(fb, outFb, 255, 10, 10, 10, Colors::pink, 2, tapeColour, results, false, false);

	Rect r(Point(0, 0), Point(fb->width, fb->height));

	cout << tapes.size() << " objects were found" << endl;

	for (VisionObject &v : tapes)
	{
		ImageProcessing::drawRectangle(fb, v.boundBox, Colors::green);
	}

	if (tapes.size() > 0)
	{
		FindLineData data;
		tapes2.push_back(tapes[0]);
		if (FindLine::find(fb, outFb, tapes2, data, false, 20))
		{
			cout << "Lines Found" << endl;
			for (Line2D &line : data.lines)
			{
				ImageProcessing::drawBresenhamLine(outFb, line.a.x, line.a.y, line.b.x, line.b.y, Colors::pink);
			}
		}
		else
			cout << "No lines found" << endl;
	}

	// End process here.

	fb->outToPPM("ppms/result.ppm");
	outFb->outToPPM("ppms/resultOut.ppm");

	delete fb;
	delete outFb;
}
