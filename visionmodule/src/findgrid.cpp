#include <stdlib.h>
#include <string.h>

#include "findgrid.h"
#include "globals.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/floodfillstate.h"
#include "cameracalibrationdata.h"
#include "colors.h"

tsai_camera_parameters FindGrid::cameraParameters;

struct tsai_camera_parameters& FindGrid::getCameraParameters()
{
	return cameraParameters;
}

/*
std::vector<CameraCalibrationData>& FindGrid::getCalibrations()
{
	return calibrations;
}

CameraCalibrationData& FindGrid::getCurrentCalibration()
{
	return currentCalibration;
}
*/

unsigned int dist(double ax, unsigned int ay, unsigned int bx, unsigned int by)
{
	return sqrt((ax - bx) * (ax - bx) + ((ay - by) * (ay - by)));
}

/**
* Distance between two coords.
*/
unsigned int dist(VisionObject *a, VisionObject *b)
{
	return dist(a->center.x, a->center.y, b->center.x, b->center.y);
}

VisionObject* FindGrid::closest(unsigned int x, unsigned int y, std::vector<VisionObject> &objects)
{
	VisionObject *tmp = &(objects.front());
	unsigned int lastDist = dist(x, y, tmp->center.x, tmp->center.y), tmpDist, index = 0, foundAt = 0;
	std::vector<VisionObject>::iterator i = objects.begin();
	while (i != objects.end())
	{
		if ((tmpDist = dist(x, y, i->center.x, i->center.y)) < lastDist)
		{
			tmp = &(*i);
			lastDist = tmpDist;
			foundAt = index;
		}
		++i;
		++index;
	}
	objects.erase(objects.begin() + foundAt - 1);
	return tmp;
}

VisionObject* FindGrid::closest(unsigned int x, unsigned int y, std::vector<VisionObject*> &objects)
{
	VisionObject *tmp = objects.front();
	unsigned int lastDist = dist(x, y, tmp->center.x, tmp->center.y), tmpDist, index = 0, foundAt = 0;
	std::vector<VisionObject*>::iterator i = objects.begin();
	while (i != objects.end())
	{
		if ((tmpDist = dist(x, y, (*i)->center.x, (*i)->center.y)) < lastDist)
		{
			tmp = *i;
			lastDist = tmpDist;
			foundAt = index;
		}
		++i;
		++index;
	}
	objects.erase(objects.begin() + foundAt);
	return tmp;
}

/**
* Return if the distance between the objects are less than threshold.
*/
bool dotsClose(VisionObject *a, VisionObject *b, double threshold)
{
	return (dist(a->center.x, a->center.y, b->center.x, b->center.y) < threshold);
}

void clusterizeObjectsStep(VisionObject *origin, std::vector<VisionObject *> *src, std::vector<VisionObject *> *dest, unsigned int threshold)
{
	std::vector<VisionObject *>::iterator i = src->begin();
	VisionObject *tmp;
	while (i != src->end())
	{
		if (dotsClose(origin, (*i), threshold))
		{
			dest->push_back(tmp = (*i));
			src->erase(i);
			clusterizeObjectsStep(tmp, src, dest, threshold);
			i = src->begin();
		}
		else
			i++;
	}
}

/*
 * Join all close shapes.
 */
void FindGrid::clusterizeObjects(std::vector<VisionObject> &src, std::vector<VisionObject> &dest, unsigned int threshold)
{
	unsigned int
		ss = src.size(),
		sumX,
		sumY;

	unsigned int
		br,
		bl,
		bt,
		bb,
		newSize;

	std::vector<VisionObject *> tmp, src2;
	std::vector<VisionObject *>::iterator j;

	for (br = 0; br < ss; br++)
		src2.push_back(&src[br]);

	VisionObject *i;

	while (src2.size() > 0)
	{
		i = src2.back();
		src2.pop_back();

		tmp.clear();
		sumX = i->center.x;
		sumY = i->center.y;

		newSize = i->size;

		br = i->boundBox.bottomRight().x();
		bl = i->boundBox.topLeft().x();
		bt = i->boundBox.topLeft().y();
		bb = i->boundBox.bottomRight().y();

		clusterizeObjectsStep(i, &src2, &tmp, threshold);

		if (tmp.size() > 0)
		{
			for (j = tmp.begin(); j != tmp.end(); ++j)
			{
				newSize = newSize + (*j)->size;
				sumX += (*j)->center.x;
				sumY += (*j)->center.y;

				if ((*j)->boundBox.topLeft().x() < bl)
					bl = (*j)->boundBox.topLeft().x();

				if ((*j)->boundBox.topLeft().y() < bt)
					bt = (*j)->boundBox.topLeft().y();

				if ((*j)->boundBox.bottomRight().x() > br)
					br = (*j)->boundBox.bottomRight().x();

				if ((*j)->boundBox.bottomRight().y() > bb)
					bb = (*j)->boundBox.bottomRight().y();
			}
		}
		Point pTL(bl, bt);
		Point pRB(br, bb);
		Rect r(pTL, pRB);
		RawPixel rpx;
		VisionObject vo("TODO", newSize/(tmp.size() + 1), sumX / (tmp.size() + 1), sumY / (tmp.size() + 1), rpx, rpx, r);
		dest.push_back(vo);
	}
}

bool intersects(unsigned int ax, unsigned int ax2, unsigned int bx, unsigned int bx2)
{
	unsigned int
		aw = (ax2 - ax),
		bw = (bx2 - bx),
		tmp1 = std::abs((int)ax - (int)bx);
	return
		// ((ax > bx) && (ax2 < bx));
		(tmp1 < aw) || (tmp1 < bw);
}

/**
* The colCount here is a workaround. TODO: Find a better way to do it.
*/
void getRow(VisionObject *start, std::vector<VisionObject *> &src, std::vector<VisionObject *> &row,
	unsigned int colIndex, unsigned int rowIndex, unsigned int colCount, unsigned int rowCount, Trapezoid &trapezoid)
{
	VisionObject *o;
	row.push_back(start);
	Point p;
	double wratio, hratio;
	for (unsigned int colIndex = 1; (colIndex < colCount) && (src.size() > 0); colIndex++)
	{
		trapezoid.point(rowIndex, colIndex, p, rowCount, colCount);
		o = FindGrid::closest(
			p.x(), p.y(), src
		);
		// TODO: Check the size of the VisionObject
		wratio = ((double)o->boundBox.width() / (double)start->boundBox.width());
		hratio = ((double)o->boundBox.height() / (double)start->boundBox.height());
		// printf("Ratio = %f, %f #%d (%d : %d)\n\to: %d\n\ts: %d\n", wratio, hratio, rowIndex * trapezoid.cols + colIndex, o->x, o->y, o->bBox.size(), start->bBox.size());
		if (
			(wratio >= 0.6)
			&& (wratio <= 1.4)
			&& (hratio >= 0.4)
			&& (hratio <= 2.0)
		)
		{
			row.push_back(o);
		}
		else
		{
			// printf("Rejected by ratio.\n");
			colIndex--;
		}
	}
	// printf("getRow - end\n");
}

bool findStartAbove(VisionObject **start, std::vector<VisionObject *> src)
{
	// printf("findStartAbove, Start: %d, %d\n", (*start)->x, (*start)->y);
	std::vector<VisionObject *>::iterator i;
	VisionObject *newStart = NULL;
	unsigned int
		tmpY,
		distY = -1;
	for (i = src.begin(); i != src.end(); ++i)
	{
		if (
			((*i)->center.y < (*start)->center.y)
			&& (intersects((*start)->boundBox.topLeft().x(), (*start)->boundBox.bottomRight().x(), (*i)->boundBox.topLeft().x(), (*i)->boundBox.bottomRight().x()))
			&& ((tmpY = std::abs((int)(*start)->center.y - (int)(*i)->center.y)) < distY)
			&& ((*start)->center.x < (*i)->center.x)
		)
		{
			distY = tmpY;
			newStart = (*i);
		}
	}
	if (newStart == NULL)
		return false;
	else
	{
		*start = newStart;
		return true;
	}
}

bool FindGrid::find(
	FrameBuffer *in, FrameBuffer *out, Trapezoid &trapezoid, std::vector<VisionObject> &results,
	std::vector<VisionObject *> &grid, unsigned int rowCount, unsigned int colCount
)
{
	// double ratio;
	std::vector<VisionObject *> src;
	Point p;
	for (std::vector<VisionObject>::iterator i = results.begin(); i != results.end(); ++i)
	{
		// ratio = ((double)(*i).bBox.height() / (*i).bBox.width());
		// if (((*i).bBox.size() < 5000) && (ratio > 0.4) && (ratio < 1.6))
		if (((*i).boundBox.size() < 5000))
		{
			src.push_back(&(*i));
			ImageProcessing::drawRectangle(in, (*i).boundBox, Colors::pink);
		}
		else
		{
			ImageProcessing::drawRectangle(in, (*i).boundBox, Colors::red);
		}
	}

	std::vector<VisionObject *> row;
	std::vector<VisionObject *>::iterator j;
	unsigned int
		rows = 0,
		cols = 0;
	VisionObject *o;
	while (rows < rowCount && (src.size() > 0))
	{
		row.clear();
		for (cols = 0; (cols < colCount) && (src.size() > 0); cols++)
		{
			o = closest(
				Trapezoid::line(trapezoid.sw()->x(), trapezoid.se()->x(), colCount, cols)
				+ Trapezoid::line(0, trapezoid.nw()->x() - trapezoid.sw()->x(), rowCount, rows)
				,
				Trapezoid::line(trapezoid.sw()->y(), trapezoid.nw()->y(), rowCount, rows)
				,
				src
			);
			printf("Object at (%dx%d): (%d, %d) (%d, %d)\n",
				cols, rows,
				Trapezoid::line(trapezoid.sw()->x(), trapezoid.se()->x(), colCount, cols)
				+ Trapezoid::line(0, trapezoid.nw()->x() - trapezoid.sw()->x(), rowCount, rows),
				Trapezoid::line(trapezoid.sw()->y(), trapezoid.nw()->y(), rowCount, rows),
				o->center.x, o->center.y
			);
			getRow(o, src, row, cols, rows, colCount, rowCount, trapezoid);
			cols = colCount;
		}
		j = row.begin();
		while (j != row.end())
		{
			grid.push_back((*j));
			++j;
		}
		++rows;
	}

	/*
	printf("Grid:\n");
	for (int x = 0; x < grid.size(); x++)
	{
		if (!(x % colCount))
			printf("\n%d: ", x / colCount);
		printf("(%4d %4d)[%4d]  ", grid[x]->x, grid[x]->y, grid[x]->bBox.size());
	}
	printf("\n");
	printf("Grid for DUMP:");
	for (int x = 0; x < grid.size(); x++)
	{
		printf("\n%d\n%d", grid[x]->x, grid[x]->y, grid[x]->bBox.size());
	}
	printf("\n");
	*/
	return (grid.size() == (rowCount * colCount));
}

bool FindGrid::calibrate(
	FrameBuffer* frame, FrameBuffer* outFrame, std::vector<VisionObject *> grid,
	CameraCalibrationData &cameraCalibration, struct tsai_calibration_constants &calibrationResults
)
{
	if (grid.size() == (cameraCalibration.params.rows * cameraCalibration.params.columns))
	{
		double
			A[3 * 3], K[3 * 4], distortion,
			imagePoints[grid.size()*2],
			worldPoints[grid.size()*3];

		unsigned int index = 0;
		for (std::vector<VisionObject*>::iterator i = grid.begin(); i != grid.end(); ++i)
		{
			imagePoints[index*2 + 0] = (*i)->boundBox.bbCenterX();
			imagePoints[index*2 + 1] = (*i)->boundBox.bbCenterY();

			ImageProcessing::drawCross(frame, (*i)->boundBox.bbCenterX(), (*i)->boundBox.bbCenterY(), Colors::black, 3);

			if ((index % cameraCalibration.params.rows) == 0)
			{
				printf("\n");
			}
			worldPoints[index*3 + 0] = cameraCalibration.params.origin.x + (cameraCalibration.params.distance.x * (index % cameraCalibration.params.columns));
			worldPoints[index*3 + 1] = cameraCalibration.params.origin.y + (cameraCalibration.params.distance.y * (index / cameraCalibration.params.columns));
			worldPoints[index*3 + 2] = 0.01;
			// printf("\t(%d : %d:%d)%-3.2f %-3.2f %-3.2f", index, (index % cameraCalibration.params.rows), (index / cameraCalibration.params.columns), worldPoints[index*3 + 0],  worldPoints[index*3 + 1],  worldPoints[index*3 + 2]);
			index++;
		}

		if (tsai2D_calibration(grid.size(), worldPoints, imagePoints, &A[0], &K[0], &distortion, &cameraParameters, &calibrationResults))
		{
			/*Save results in a file: Calib.txt*/
			// saveIntrinsicData("Calib.txt", A, K, distortion, CameraParameters);
			printf("\n\nCamera parameters:\n");
			printf("\nNcx :	%.8lf \t	 Nfx: %.8lf	\n", cameraParameters.Ncx, cameraParameters.Nfx);
			printf("\nCell size (dx,dy):	(%.8lf,%.8lf) \n", cameraParameters.dx, cameraParameters.dx);
			printf("\nImage center (Cx,Cy): (%.8lf, %.8lf) \n", cameraParameters.Cx, cameraParameters.Cy);
			printf("\nsx : %.8lf  \n\n", cameraParameters.sx);

			printf("\nCalibration results:\n");
			printf("\nIntrinsic parameters:\n");
			printf("%.8lf %.8lf	 %.8lf \n", A[0], A[1], A[2]);
			printf("%.8lf %.8lf	 %.8lf \n", A[3], A[4], A[5]);
			printf("%.8lf %.8lf	 %.8lf \n\n", A[6], A[7], A[8]);

			printf("\nExtrinsic parameters:\n");
			printf("%.8lf  %.8lf	%.8lf  %.8lf \n", K[0], K[1], K[2], K[3]);
			printf("%.8lf  %.8lf	%.8lf  %.8lf \n", K[4], K[5], K[6], K[7]);
			printf("%.8lf  %.8lf	%.8lf  %.8lf \n\n", K[8], K[9], K[10], K[11]);

			printf("\nDistortion parameters: \t %.8lf\n", distortion);

			return true;
		}
		else
		{
			printf("\nERROR IN CALIBRATION PROCESS\n");
			return false;
		}
	}
	return false;
}
