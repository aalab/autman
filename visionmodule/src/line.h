/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef VISIONMODULE_LINE_H
#define VISIONMODULE_LINE_H

#include <string>

#include "../libvideo/point.h"
#include "../libvideo/basevisionobject.h"

class Line2D
	: BaseVisionObject2D
{
private:
	static std::string line2DClassName;
public:
	Line2D();
	Line2D(Point2D &pa, Point2D &pb);
	Line2D(double paX, double paY, double pbX, double pbY);

	virtual std::string &serializeClassName();

	Point2D a;
	Point2D b;

	double hypot();
	double inclination();

	double xFromY(double y);
	double yFromX(double x);

	double slope();
};

#endif //VISIONMODULE_LINE_H
