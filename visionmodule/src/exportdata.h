/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef VISIONMODULE_EXPORTDATA_H
#define VISIONMODULE_EXPORTDATA_H


#include <chrono>
#include <json/value.h>

class ExportData
{
public:
	std::string name;
	std::chrono::system_clock::time_point exportTime;
	Json::Value data;
	Json::Value balls;
};


#endif //VISIONMODULE_EXPORTDATA_H
