#ifndef __UTILS__FINALLY_H__
#define __UTILS__FINALLY_H__

namespace utils
{ //adapt to your "private" namespace
	template<typename F>
	struct FinalAction
	{
		FinalAction(F f) : clean_{f}
		{ }

		~FinalAction()
		{
			clean_();
		}

		F clean_;
	};
}

template<typename F>
utils::FinalAction <F> finally(F f)
{
	return utils::FinalAction<F>(f);
}

#endif