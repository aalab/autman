/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "http/json.h"
#include "globals.h"
#include "colors.h"
#include "findball.h"
#include "compilationdefinitions.h"

int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		cout << "Invalid params. Usage: " << argv[0] << " <config> <raw_image>" << endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE(),
		*ballOutFb = new FrameBufferRGB24BE(),
		*tapeOutFb = new FrameBufferRGB24BE(),
		*grassOutFb = new FrameBufferRGB24BE(),
		*calOutFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	std::vector<ColourDefinition> &colourDefs = config.colours;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);
	grassOutFb->initialize(config.width, config.height);
	calOutFb->initialize(config.width, config.height);
	tapeOutFb->initialize(config.width, config.height);
	ballOutFb->initialize(config.width, config.height);

	unsigned int subsample = 1;

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	fb->inFromPPM(argv[2]);

	ColourDefinition
		*grass = config.getColour("grass"),
		*tape = config.getColour("tape"),
		*ballCD = config.getColour("cal");

	assert(tape != NULL);

	std::vector<VisionObject>
		grassResults,
		tapeResults,
		ballResults;

	ImageProcessing::SegmentColours(fb, grassOutFb, 255, 5, 20, subsample, *grass, Colors::red, grassResults, false, false);
	ImageProcessing::SegmentColours(fb, tapeOutFb, 255, 5, 20, subsample, *tape, Colors::red, tapeResults, false, true);
	if (FindBall::find2(fb, calOutFb, grassOutFb, tape, tapeOutFb, ballCD, ballOutFb, grassResults, tapeResults, ballResults))
	{
		for (VisionObject &ball : ballResults)
		{
			ImageProcessing::drawCross(fb, ball.center.x, ball.center.y, Colors::green, ball.boundBox.width()/2);
		}
		cout << "Ball found." << endl;
	}
	else
		cout << "Ball not found." << endl;

	fb->outToPPM("result.ppm");
	grassOutFb->outToPPM("resultGrassOut.ppm");
	tapeOutFb->outToPPM("resultTapeOut.ppm");
	ballOutFb->outToPPM("resultBallOut.ppm");

	delete fb;
	delete outFb;
}
