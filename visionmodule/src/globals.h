/* 
 * Class that encapsulates global data
 * Jacky Baltes <jacky@cs.umanitoba.ca> Tue Jan 15 00:09:43 CST 2013
 */

#include <inttypes.h>
#include <pthread.h>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <json/value.h>
#include <services/teleop.h>
#include <services/publicclient.h>
#include "markerdescriptor.h"
#include "trapezoid.h"
#include "../libvideo/colourdefinition.h"
#include "exportdata.h"

class FrameBuffer;

class VideoStream;

class Serial;

class HTTPD;

class HTTPDThread;

class HTTPDServerThread;

class Configuration;

class Globals
{
public:
	static Globals *GetGlobals(void);

	std::unique_ptr<services::Teleop> teleopService;
	std::unique_ptr<services::PublicClient> robotService;
private:
	static Globals *globals;

protected:
	pthread_mutex_t db;
	pthread_mutex_t controls_mutex;

private:
	uint8_t *buf;
	VideoStream *video;
	Serial *serial;
	HTTPD *server;
	HTTPDServerThread *thread;

	volatile unsigned int size;
	volatile bool clientRequest;

public:
	pthread_cond_t db_update;

	std::string userName;

private:
	Globals(void);
	~Globals();

public:
	inline uint8_t *GetBuffer(void)
	{
		return buf;
	};

	inline void SetBuffer(uint8_t *buf, unsigned int size)
	{
		this->buf = buf;
		this->size = size;
	};

	inline unsigned int GetSize(void)
	{
		return size;
	};

	inline void SetSize(unsigned int size)
	{
		this->size = size;
	};

	int LockBuffer(void);
	int UnlockBuffer(void);
	int BroadcastBuffer(void);
	int CondWaitForBuffer(void);
	int CompressImageToJpeg(FrameBuffer *img);

	inline VideoStream *GetVideo(void)
	{
		return video;
	};

	inline void SetVideo(VideoStream *video)
	{
		this->video = video;
	};

	inline Serial *GetSerial(void)
	{
		return serial;
	};

	inline void SetSerial(Serial *serial)
	{
		this->serial = serial;
	};

	inline void SetHTTPDServer(HTTPD *server)
	{
		this->server = server;
	};

	inline HTTPD *GetHTTPDServer(void)
	{
		return server;
	};

	inline HTTPDServerThread *GetHTTPDServerThread(void)
	{
		return thread;
	};

	inline void SetHTTPDServerThread(HTTPDServerThread *thread)
	{
		this->thread = thread;
	};

	inline bool GetClientRequest(void)
	{
		return static_cast<bool>( clientRequest );
	};

	inline void SetClientRequest(bool clientRequest)
	{
		this->clientRequest = clientRequest;
	};

	void UpdateRunningConfiguration(Configuration const *cfg);

	Configuration *configuration;

	std::vector<MarkerDescriptor> markersTrain;

	MarkerDescriptor lastFoundMarker;

	bool trainMarker;

	std::mutex trainMarkerMutex;
	std::condition_variable trainMarkerCondition;

	/**
	 * Flag to export raw
	 */
	bool exportFrameBuffer;
	FrameBuffer* frameBufferExported;
	std::condition_variable exportConditionVariable;
	std::mutex exportMutex;

	bool exportData;
	std::condition_variable exportDataConditionVariable;
	std::mutex exportDataMutex;
	Json::Value* exportDataJson;

	bool waitExportData(unsigned int timeoutMilliseconds);
	void prepareExportData(Json::Value *json);
	void doExportData();

	ExportData exportDataGoalArea;

	Trapezoid trapezoid;
};
