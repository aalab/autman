/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 5/16/15.
 */

#ifndef VISIONMODULE_FINDSPRINTMARKER_H
#define VISIONMODULE_FINDSPRINTMARKER_H

#include "../libvideo/visionobject.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "cameracalibrationdata.h"

#include <vector>
#include <cmath>

using namespace std;

class SprintMarker
{
public:
	SprintMarker();

	VisionObject *left;
	VisionObject *right;

	int centerX;
	int centerY;

	unsigned int height;
};

class FindSprintMarker
{
public:

	static bool shapeFits(VisionObject &object);

	static void filterObjects(std::vector<VisionObject>& objects, std::vector<VisionObject*>& result);

	static bool find(
		FrameBuffer* in, FrameBuffer* out, vector<VisionObject> &leftMarkers, vector<VisionObject> &rightMarkers,
		SprintMarker *last, SprintMarker *result, tsai_camera_parameters &cameraParameters,
		CameraCalibrationData& calibrationData
	);
};


#endif //VISIONMODULE_FINDSPRINTMARKER_H
