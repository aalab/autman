/**
 * Jacky Baltes <jacky@cs.umanitoba.ca> Tue Jan  8 22:55:42 CST 2013
 */


#include <ostream>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <sstream>

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>

#include "httpd.h"
#include "httpdthread.h"
#include "videostream.h"
#include "configuration.h"
#include "globals.h"

#include "http/response.h"

#include "http/actions/commands.h"
#include "http/actions/snapshot.h"
#include "http/actions/staticfiles.h"
#include "http/actions/stream.h"
#include "http/actions/messages.h"
#include "http/actions/teleop.h"

#include "../src/compilationdefinitions.h"
#include "http/actions/robot.h"

HTTPD::HTTPD(
	unsigned int http_port, char const *http_addr, char const *credentials, char const *docroot, char const *index
)
{
	this->http_port = http_port;
	this->http_addr = http_addr;
	this->credentials = credentials;
	this->docroot = docroot;
	this->index = index;
	this->commands = commands;

	//  HTTPDThread * thread = new HTTPDThread( this );

	// Start the server thread here?
	//thread->StartAndDetach();

	this->resource["^/command/([a-z_]+)$"]["POST"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Commands::action));
	this->resource["^/command/([a-z_]+)$"]["GET"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Commands::action));
	this->resource["^/messages$"]["POST"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Messages::action));
	this->resource["^/video/snapshot.jpg([?].*)?"]["GET"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Snapshot::action));
	this->resource["^/video/stream.jpg$"]["GET"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Stream::action));

	this->resource["^/teleop$"]["POST"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Teleop::action));

	this->resource["^/robot"]["POST"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Robot::action));

	this->resource["^/robot"]["OPTIONS"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::Robot::options));

	this->default_resource["GET"] = std::function<void(http::Response &, http::Request &)>(
		std::ref(http::actions::StaticFiles::action));

	// Copy the resources to opt_resource for more efficient request processing
	VERBOSE("Resources: ");
	opt_resource.clear();
	for (auto &res: resource)
	{

		for (auto &res_method: res.second)
		{
			auto it = opt_resource.end();
			for (auto opt_it = opt_resource.begin(); opt_it != opt_resource.end(); opt_it++)
			{
				if (res_method.first == opt_it->first)
				{
					it = opt_it;
					break;
				}
			}
			if (it == opt_resource.end())
			{
				opt_resource.emplace_back();
				it = opt_resource.begin() + (opt_resource.size() - 1);
				it->first = res_method.first;
			}
			VERBOSEB("+ '" << res.first << "'");
			try
			{
				it->second.emplace_back(boost::regex(res.first), res_method.second);
			}
			catch (const boost::regex_error &e)
			{
				ERROR(e.code() << "] " << e.what());
			}
		}
	}
}

void HTTPD::findResource(http::Request &request, http::Response &result)
{
	//Find path- and method-match, and call write_response
	for (auto &res: opt_resource)
	{
		if (request.method == res.first)
		{
			for (auto &res_path: res.second)
			{
				boost::smatch sm_res;
				if (boost::regex_match(request.path, sm_res, res_path.first))
				{
					request.path_match = std::move(sm_res);
					res_path.second(result, request);
					return;
				}
			}
		}
	}

	auto it_method = default_resource.find(request.method);
	if (it_method != default_resource.end())
		it_method->second(result, request);
}
