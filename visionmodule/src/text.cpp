/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 16, 2015
 */

#include "text.h"
#include "../libvideo/imageprocessing.h"

#define LETTERS_WIDTH 8

#define LETTERS_LINE_WIDTH 32

#define LETTERS_HEIGHT 12

#define LETTERS_START 32
#define LETTERS_LENGTH 32*3

static unsigned char letters[] = {
	0x00, 0x0c, 0x33, 0x33, 0x3c, 0x03, 0x0c, 0x0c, 0x30, 0x03, 0x33, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x3f, 0x0c, 0x3f, 0x3f, 0x33, 0x3f, 0x3f, 0x3f,
	0x3f, 0x3f, 0x00, 0x00, 0x30, 0x00, 0x03, 0x3f, 0x00, 0x0c, 0x33, 0x33,
	0x3c, 0x03, 0x0c, 0x0c, 0x30, 0x03, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x3f, 0x0c, 0x3f, 0x3f, 0x33, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x00, 0x00,
	0x30, 0x00, 0x03, 0x3f, 0x00, 0x0c, 0x33, 0x3f, 0x0f, 0x30, 0x33, 0x0c,
	0x0c, 0x0c, 0x0c, 0x0c, 0x00, 0x00, 0x00, 0x30, 0x33, 0x0c, 0x30, 0x30,
	0x33, 0x03, 0x03, 0x30, 0x33, 0x33, 0x0c, 0x0c, 0x0c, 0x3f, 0x0c, 0x30,
	0x00, 0x0c, 0x33, 0x3f, 0x0f, 0x30, 0x33, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c,
	0x00, 0x00, 0x00, 0x30, 0x33, 0x0c, 0x30, 0x30, 0x33, 0x03, 0x03, 0x30,
	0x33, 0x33, 0x0c, 0x0c, 0x0c, 0x3f, 0x0c, 0x30, 0x00, 0x0c, 0x00, 0x33,
	0x3c, 0x0c, 0x0c, 0x00, 0x0c, 0x0c, 0x3f, 0x3f, 0x00, 0x3f, 0x00, 0x0c,
	0x33, 0x0c, 0x3f, 0x3c, 0x3f, 0x3f, 0x3f, 0x30, 0x3f, 0x3f, 0x00, 0x00,
	0x03, 0x00, 0x30, 0x0c, 0x00, 0x0c, 0x00, 0x33, 0x3c, 0x0c, 0x0c, 0x00,
	0x0c, 0x0c, 0x3f, 0x3f, 0x00, 0x3f, 0x00, 0x0c, 0x33, 0x0c, 0x3f, 0x3c,
	0x3f, 0x3f, 0x3f, 0x30, 0x3f, 0x3f, 0x00, 0x00, 0x03, 0x00, 0x30, 0x0c,
	0x00, 0x00, 0x00, 0x3f, 0x0f, 0x03, 0x33, 0x00, 0x0c, 0x0c, 0x0c, 0x0c,
	0x0c, 0x00, 0x0c, 0x03, 0x33, 0x0c, 0x03, 0x30, 0x30, 0x30, 0x33, 0x30,
	0x33, 0x30, 0x0c, 0x0c, 0x0c, 0x3f, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x3f,
	0x0f, 0x03, 0x33, 0x00, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x00, 0x0c, 0x03,
	0x33, 0x0c, 0x03, 0x30, 0x30, 0x30, 0x33, 0x30, 0x33, 0x30, 0x0c, 0x0c,
	0x0c, 0x3f, 0x0c, 0x00, 0x00, 0x0c, 0x00, 0x33, 0x0c, 0x30, 0x3c, 0x00,
	0x30, 0x03, 0x33, 0x00, 0x03, 0x00, 0x0c, 0x00, 0x3f, 0x0c, 0x3f, 0x3f,
	0x30, 0x3f, 0x3f, 0x30, 0x3f, 0x3f, 0x00, 0x03, 0x30, 0x00, 0x03, 0x0c,
	0x00, 0x0c, 0x00, 0x33, 0x0c, 0x30, 0x3c, 0x00, 0x30, 0x03, 0x33, 0x00,
	0x03, 0x00, 0x0c, 0x00, 0x3f, 0x0c, 0x3f, 0x3f, 0x30, 0x3f, 0x3f, 0x30,
	0x3f, 0x3f, 0x00, 0x03, 0x30, 0x00, 0x03, 0x0c, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x0c, 0x0c, 0x0f, 0x3c, 0x0f, 0x3f, 0x3f, 0x3c, 0x33, 0x3f, 0x30, 0x33,
	0x03, 0x33, 0x33, 0x0c, 0x0f, 0x0c, 0x0f, 0x3c, 0x3f, 0x33, 0x33, 0x33,
	0x33, 0x33, 0x3f, 0x3f, 0x00, 0x3f, 0x0c, 0x00, 0x0c, 0x0c, 0x0f, 0x3c,
	0x0f, 0x3f, 0x3f, 0x3c, 0x33, 0x3f, 0x30, 0x33, 0x03, 0x33, 0x33, 0x0c,
	0x0f, 0x0c, 0x0f, 0x3c, 0x3f, 0x33, 0x33, 0x33, 0x33, 0x33, 0x3f, 0x3f,
	0x00, 0x3f, 0x0c, 0x00, 0x33, 0x33, 0x33, 0x03, 0x33, 0x03, 0x03, 0x03,
	0x33, 0x0c, 0x30, 0x33, 0x03, 0x3f, 0x3f, 0x33, 0x33, 0x33, 0x33, 0x03,
	0x0c, 0x33, 0x33, 0x33, 0x33, 0x33, 0x30, 0x03, 0x03, 0x30, 0x33, 0x00,
	0x33, 0x33, 0x33, 0x03, 0x33, 0x03, 0x03, 0x03, 0x33, 0x0c, 0x30, 0x33,
	0x03, 0x3f, 0x3f, 0x33, 0x33, 0x33, 0x33, 0x03, 0x0c, 0x33, 0x33, 0x33,
	0x33, 0x33, 0x30, 0x03, 0x03, 0x30, 0x33, 0x00, 0x3f, 0x3f, 0x0f, 0x03,
	0x33, 0x3f, 0x3f, 0x3f, 0x3f, 0x0c, 0x30, 0x0f, 0x03, 0x3f, 0x3f, 0x33,
	0x0f, 0x33, 0x3f, 0x0c, 0x0c, 0x33, 0x33, 0x3f, 0x0c, 0x0c, 0x0c, 0x03,
	0x0c, 0x30, 0x00, 0x00, 0x3f, 0x3f, 0x0f, 0x03, 0x33, 0x3f, 0x3f, 0x3f,
	0x3f, 0x0c, 0x30, 0x0f, 0x03, 0x3f, 0x3f, 0x33, 0x0f, 0x33, 0x3f, 0x0c,
	0x0c, 0x33, 0x33, 0x3f, 0x0c, 0x0c, 0x0c, 0x03, 0x0c, 0x30, 0x00, 0x00,
	0x03, 0x33, 0x33, 0x03, 0x33, 0x03, 0x03, 0x33, 0x33, 0x0c, 0x33, 0x33,
	0x03, 0x33, 0x3f, 0x33, 0x03, 0x3f, 0x0f, 0x30, 0x0c, 0x33, 0x0c, 0x3f,
	0x33, 0x0c, 0x03, 0x03, 0x30, 0x30, 0x00, 0x00, 0x03, 0x33, 0x33, 0x03,
	0x33, 0x03, 0x03, 0x33, 0x33, 0x0c, 0x33, 0x33, 0x03, 0x33, 0x3f, 0x33,
	0x03, 0x3f, 0x0f, 0x30, 0x0c, 0x33, 0x0c, 0x3f, 0x33, 0x0c, 0x03, 0x03,
	0x30, 0x30, 0x00, 0x00, 0x3c, 0x33, 0x0f, 0x3c, 0x0f, 0x3f, 0x03, 0x3c,
	0x33, 0x3f, 0x0c, 0x33, 0x3f, 0x33, 0x33, 0x0c, 0x03, 0x3c, 0x33, 0x0f,
	0x0c, 0x3c, 0x0c, 0x33, 0x33, 0x0c, 0x3f, 0x3f, 0x00, 0x3f, 0x00, 0x3f,
	0x3c, 0x33, 0x0f, 0x3c, 0x0f, 0x3f, 0x03, 0x3c, 0x33, 0x3f, 0x0c, 0x33,
	0x3f, 0x33, 0x33, 0x0c, 0x03, 0x3c, 0x33, 0x0f, 0x0c, 0x3c, 0x0c, 0x33,
	0x33, 0x0c, 0x3f, 0x3f, 0x00, 0x3f, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x03, 0x00, 0x03, 0x00, 0x30, 0x00, 0x30, 0x00, 0x03, 0x0c, 0x30, 0x00,
	0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x3c, 0x0c, 0x0f, 0x3c, 0x3f, 0x03, 0x00, 0x03, 0x00,
	0x30, 0x00, 0x30, 0x00, 0x03, 0x0c, 0x30, 0x00, 0x0f, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c,
	0x0c, 0x0f, 0x3c, 0x3f, 0x0c, 0x00, 0x03, 0x00, 0x30, 0x3c, 0x0c, 0x3c,
	0x03, 0x00, 0x00, 0x03, 0x0c, 0x00, 0x00, 0x00, 0x0c, 0x0c, 0x03, 0x3c,
	0x3f, 0x00, 0x00, 0x00, 0x00, 0x33, 0x3f, 0x0c, 0x0c, 0x0c, 0x0f, 0x3f,
	0x0c, 0x00, 0x03, 0x00, 0x30, 0x3c, 0x0c, 0x3c, 0x03, 0x00, 0x00, 0x03,
	0x0c, 0x00, 0x00, 0x00, 0x0c, 0x0c, 0x03, 0x3c, 0x3f, 0x00, 0x00, 0x00,
	0x00, 0x33, 0x3f, 0x0c, 0x0c, 0x0c, 0x0f, 0x3f, 0x00, 0x3c, 0x0f, 0x3c,
	0x3c, 0x33, 0x3f, 0x3f, 0x0f, 0x0c, 0x30, 0x33, 0x0c, 0x3f, 0x0f, 0x0c,
	0x33, 0x33, 0x3f, 0x0f, 0x0c, 0x33, 0x33, 0x33, 0x33, 0x3c, 0x3c, 0x03,
	0x00, 0x30, 0x00, 0x3f, 0x00, 0x3c, 0x0f, 0x3c, 0x3c, 0x33, 0x3f, 0x3f,
	0x0f, 0x0c, 0x30, 0x33, 0x0c, 0x3f, 0x0f, 0x0c, 0x33, 0x33, 0x3f, 0x0f,
	0x0c, 0x33, 0x33, 0x33, 0x33, 0x3c, 0x3c, 0x03, 0x00, 0x30, 0x00, 0x3f,
	0x00, 0x33, 0x33, 0x03, 0x33, 0x0f, 0x0c, 0x30, 0x33, 0x0c, 0x30, 0x0f,
	0x0c, 0x3f, 0x33, 0x33, 0x0f, 0x3c, 0x03, 0x3c, 0x0c, 0x33, 0x3f, 0x3f,
	0x0c, 0x30, 0x0f, 0x0c, 0x0c, 0x0c, 0x00, 0x3f, 0x00, 0x33, 0x33, 0x03,
	0x33, 0x0f, 0x0c, 0x30, 0x33, 0x0c, 0x30, 0x0f, 0x0c, 0x3f, 0x33, 0x33,
	0x0f, 0x3c, 0x03, 0x3c, 0x0c, 0x33, 0x3f, 0x3f, 0x0c, 0x30, 0x0f, 0x0c,
	0x0c, 0x0c, 0x00, 0x3f, 0x00, 0x3f, 0x0f, 0x3c, 0x3c, 0x3c, 0x0c, 0x0f,
	0x33, 0x0c, 0x0f, 0x33, 0x3f, 0x33, 0x33, 0x0c, 0x03, 0x30, 0x03, 0x0f,
	0x0c, 0x3c, 0x0c, 0x3f, 0x33, 0x0f, 0x3f, 0x3c, 0x0c, 0x0f, 0x00, 0x3f,
	0x00, 0x3f, 0x0f, 0x3c, 0x3c, 0x3c, 0x0c, 0x0f, 0x33, 0x0c, 0x0f, 0x33,
	0x3f, 0x33, 0x33, 0x0c, 0x03, 0x30, 0x03, 0x0f, 0x0c, 0x3c, 0x0c, 0x3f,
	0x33, 0x0f, 0x3f, 0x3c, 0x0c, 0x0f, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


void vision::Text::draw(unsigned int x, unsigned int y, string text, RawPixel &colour, FrameBuffer *buffer)
{
	unsigned char
		letterIdx,
		row, col;
	for (unsigned char c : text)
	{
		if ((c >= LETTERS_START) && (c < LETTERS_START + LETTERS_LENGTH))
		{
			letterIdx = (c - LETTERS_START);
			row = letterIdx/LETTERS_LINE_WIDTH;
			col = letterIdx%LETTERS_LINE_WIDTH;
			// cout << "Printing: " << c << "(" << (unsigned int)letterIdx << ") : " << (unsigned int)row << ":" << (unsigned int)col << endl;
			for (unsigned int i = 0; (i < LETTERS_HEIGHT) && (y + i < buffer->height); i++)
			{
				for (unsigned int j = 0; (j < LETTERS_WIDTH) && (x + j < buffer->width); j++)
				{
					if (((letters[(row*LETTERS_HEIGHT + i)*LETTERS_LINE_WIDTH + col]) & 1<<j) == 1<<j)
						buffer->setPixel(y + i, x + j, colour);
				}
			}
		}
		else
			ImageProcessing::drawRectangle(buffer, x, y, LETTERS_WIDTH, LETTERS_HEIGHT, colour);
		x += LETTERS_WIDTH - 1;
	}
}
