/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 13, 2015
 */

#ifndef VISIONMODULE_FINDGOALAREA_H
#define VISIONMODULE_FINDGOALAREA_H

#include <vector>
#include "line.h"
#include "../libvideo/framebuffer.h"
#include "../libvideo/visionobject.h"
#include "../libvideo/colourdefinition.h"

class FindGoalAreaData
{
public:
	std::vector<Line2D> lines;
};

class FindGoalArea
{
private:
	class FindGoalAreaSegmentContext
	{
	public:
		bool colourFound;
		double segmentStartX;
		double segmentStartY;
	};
public:
	static bool find(FrameBuffer *in, FrameBuffer *out, std::vector<VisionObject> &tapes,
		FindGoalAreaData &data, bool ressegment, unsigned int squareSize);

protected:
	static void filterTapes(std::vector<VisionObject>& tapeList, std::vector<VisionObject *>& filtered);

	static bool getImageLine(
		double *centerX, double *centerY, unsigned int x, unsigned int y, int offset,
		VisionObject *line, FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
		RawPixel *tapeColour, const double angle, float *width
	);

	static void getLineInfo(
		FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, unsigned int lineOffset,
		unsigned int minLength, unsigned int subsample, RawPixel *tapePixel, bool expand, std::vector<Line2D> &resultLines,
		bool ressegment, unsigned int squareSize
	);

	static void expandImageLineWhileMatch(
		double *pointX, double *pointY, unsigned int x, unsigned int y, VisionObject &line, FrameBuffer *frame,
		FrameBuffer *outFrame, RawPixel *tapePixel, const double angle, unsigned int threshold
	);

	static void getImageLineTop(
		VisionObject &vo, double *lineTopCenterX, double *lineTopY, unsigned int offset, VisionObject *line,
		FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
		RawPixel *tapePixel
	);

	static void getImageLineBottom(
		VisionObject &vo, double *lineBottomCenterX, double *lineBottomY, unsigned int offset, VisionObject *line,
		FrameBuffer *frame, FrameBuffer *outFrame, unsigned int minLength, unsigned int subsample,
		RawPixel *tapePixel
	);

	static void resegmentLine(FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, RawPixel *tapePixel,
		unsigned int imageLineTopX, unsigned int imageLineTopY, unsigned int imageLineBottomX,
		unsigned int imageLineBottomY, std::vector<Line2D> &resultLines);

	static void getLineSegments(
		FrameBuffer *frame, FrameBuffer *outFrame, VisionObject &vo, double cX, double cY, unsigned int squareSize,
		std::vector<Line2D> &segments
	);
};


#endif //VISIONMODULE_FINDGOALAREA_H
