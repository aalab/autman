#include "udpvisionserver.h"
#include "videostream.h"
#include "globals.h"
#include "compilationdefinitions.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>

#include <boost/array.hpp>
#include <boost/bind.hpp>

#include <ctime>
#include <iostream>
#include <unistd.h>
#include <string>

#include "http/json.h"

#include "serialization/json.h"
#include "configuration.h"
#include "findgrid.h"

using boost::asio::ip::udp;

UDPVisionServer::UDPVisionServer(boost::asio::io_service &io_service, unsigned int port, VideoStream const *vs)
	: socket(io_service, udp::endpoint(udp::v4(), port)), videostream(vs), signals(io_service)
{
	signals.add(SIGINT);
	signals.async_wait(boost::bind(&UDPVisionServer::StopServer, this));
}

void UDPVisionServer::StartServer()
{
	char data[4096];
	size_t maxLength = sizeof(data);

	while (this->socket.is_open())
	{
		size_t len = socket.receive_from(boost::asio::buffer(data, maxLength), remoteEP);
#ifndef NDEBUG
		std::cout << "Received message of size " << len << std::endl;
#endif
		if (len > 0)
		{
			std::string
				command(data, len),
				results,
				commandName;

			if (data[0] == '{')
			{
				Json::Value json;
				if (this->reader.parse(command, json))
				{
					Globals *glob = Globals::GetGlobals();
					commandName = json["command"].asString();
					if (commandName == "visionmode")
					{
						commandName = json["params"]["mode"].asString();
						results = "{\"success\": true}";
						if (commandName == "obstaclerun")
						{
							glob->GetVideo()->SetMode(VideoStream::ProcessType::ObstacleRun);
						}
						else if (commandName == "sprint")
						{
							glob->GetVideo()->SetMode(VideoStream::ProcessType::Sprint);
						}
						else
						{
							results = "{\"success\":false,\"message\":\"Cannot find mode '" + commandName + "'.\"}";
						}
					}
					else if (commandName == "fetch")
					{
						/*
						glob->waitExportData(100);
						Json::Value data;
						data["success"] = true;
						data["goalArea"] = glob->exportDataGoalArea.data;
						data["balls"] = glob->exportDataGoalArea.balls;
						json::serialize(data, results);
						*/
						std::lock_guard<std::mutex> lock(glob->exportDataMutex);
						results = glob->GetVideo()->resultString;
					}
					else if (commandName == "cameracalibrations")
					{
						Json::Value data;

						data["success"] = true;
						data["camera"] << FindGrid::cameraParameters;
						data["configurations"] = Json::arrayValue;
						for (auto c : glob->configuration->cameraCalibrations)
						{
							Json::Value cameraConfJson;
							cameraConfJson["name"] = c.name;
							cameraConfJson["data"] << c.getResult();
							data["configurations"].append(cameraConfJson);
						}
						json::serialize(data, results);
					}
					else if (commandName == "emptyqueue")
					{
						std::cout << "UdpVisionServer::StartServer:: emptyqueue received" << std::endl;
						Globals::GetGlobals()->GetVideo()->SetMode(VideoStream::ProcessType::Marathon);
						Globals::GetGlobals()->GetVideo()->marathonMode = VideoStream::MarathonMode::Marathon_ScanForLine;
						results = "{\"success\":true}";
					}
					else
					{
						cout << "Unknown JSON command: " << command;
						results = "{\"success\":false,\"message\":\"Unknown command.\"}";
					}
				}
				else
				{
					VERBOSE("Error parsing: " << command);
					results = "{\"success\":false,\"message\":\"Error parsing the input.\"}";
				}
			}
			else
			{
#ifndef NDEBUG
				VERBOSE("Received command " << command);
#endif
				results = videostream->resultString;
#ifndef NDEBUG
				VERBOSE("Sending response " << results);
#endif
			}
			this->SendResponse(results);
		}
	}
}

int
UDPVisionServer::SendResponse(std::string const &msg)
{
	VERBOSE("OUT >> " << msg);
	return socket.send_to(boost::asio::buffer(msg), remoteEP);
}

void UDPVisionServer::StopServer()
{
	if (this->socket.is_open())
		this->socket.close();
}
