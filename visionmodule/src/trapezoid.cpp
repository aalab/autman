
#include "trapezoid.h"

Trapezoid::Trapezoid()
{ }

int Trapezoid::line2(unsigned int a, unsigned b, unsigned int count, unsigned int index)
{
	return ((((double)b-a) / (count-1)) * index);
}

int Trapezoid::line(unsigned int a, unsigned b, unsigned int count, unsigned int index)
{
	return (a + (((double)b-a) / (count-1)) * index);
}

bool Trapezoid::point(unsigned int row, unsigned int column, Point &p, unsigned int rows, unsigned int cols)
{
	unsigned int
		ax, ay, bx, by;

	ax = this->sw()->x() + line2(this->sw()->x(), this->nw()->x(), rows, row);
	ay = line(this->sw()->y(), this->nw()->y(), rows, row);

	bx = this->se()->x() + line2(this->se()->x(), this->ne()->x(), rows, row);
	by = line(this->se()->y(), this->ne()->y(), rows, row);

	// printf("a = (%d %d) || b = (%d %d)\n", ax, ay, bx, by);

	p.setX(line(ax, bx, cols, column));
	p.setY(line(ay, by, cols, column)
		+ line2(ay, by, cols, column));
	// printf("p = (%d %d)\n", p.x(), p.y());
}
