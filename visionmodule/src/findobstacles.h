/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 03, 2015
 */

#ifndef VISIONMODULE_FINDOBSTACLES_H
#define VISIONMODULE_FINDOBSTACLES_H


#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/visionobject.h"

class ObstacleVisionObject :
	public VisionObject
{
private:
	static std::string className;
public:
	std::vector<Point2D> shape;

	virtual std::string &serializeClassName() override;

	virtual std::ostream &writeJsonProperties(std::ostream &os) override;

};

class Map
{
public:
	std::vector<ObstacleVisionObject> objects;
};

class FindObstacles
{
private:
	static void getShapeInfo(FrameBuffer *asdasd, FrameBuffer *outFb, VisionObject &vo, ObstacleVisionObject &wcvo);
public:
	static bool find(
		FrameBuffer *fb, FrameBuffer *outFb, FrameBuffer *outFloorFb, ColourDefinition *floorCD,
		ColourDefinition *obstacleCD, ColourDefinition *gateCD, ColourDefinition *tapeCD,
		std::vector<VisionObject> &obstacleResults, Rect &floorArea
	);
};


#endif //VISIONMODULE_FINDOBSTACLES_H
