/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#include "objectdescriptor.h"

#include "../colors.h"
#include "../text.h"
#include "stageinfo.h"

#include "../../libvideo/imageprocessing.h"
#include "../compilationdefinitions.h"

#include "stage.h"

vision::goits::Descriptor::Descriptor()
{
	this->createState();
}

void vision::goits::Descriptor::createState()
{
	this->state = new DescriptorState();
}

void vision::goits::Descriptor::track(Stage &stage)
{
	std::vector<VisionObject> results;
	std::vector<VisionObject*> filtered;
	std::vector<unsigned int> forDeletion;

	this->find(stage.info, results);

#ifdef DEBUG__PRINT_IMAGE__GOITS
	vision::Text::draw(10, 10, to_string(results.size()) + " objects found", Colors::green, stage.info.in);
#endif
	if (!results.empty())
	{
		unsigned int i = 0;
		bool tmpMatch;
		for (ObjectInstance *instance : this->state->instances)
		{
#ifdef VERBOSE__GOITS
			VERBOSE("Matching instance " << i << ") " << results.size());
#endif
			tmpMatch = instance->match(results);
			if ((!tmpMatch) && (instance->getState()->isExpired()))
			{
				forDeletion.push_back(i);
				delete instance;
			}
			else if (tmpMatch)
			{
				ImageProcessing::drawRectangle(stage.info.in, instance->getState()->position.x, instance->getState()->position.y, instance->getState()->size.x, instance->getState()->size.y, Colors::green);
			}
#ifdef VERBOSE__GOITS
			VERBOSE(results.size() << " objects");
#endif
			i++;
		}

		this->match(results, filtered);
#ifdef VERBOSE__GOITS
		VERBOSE(filtered.size() << " filtered objects");
#endif

		// Removes the non macthed and expired instances.
		i = 0;
		for (unsigned int &idx : forDeletion)
		{
			this->state->instances.erase(this->state->instances.begin() + idx - i);
			i++;
		}

		for (VisionObject* c : filtered)
		{
			this->createNewInstance(c, &stage);
			ImageProcessing::drawRectangle(stage.info.in, c->boundBox, Colors::blue);
		}

#ifdef DEBUG__PRINT_IMAGE__GOITS
		vision::Text::draw(10, 22, to_string(filtered.size()) + " objects found", Colors::green, stage.info.in);
		vision::Text::draw(10, 34, to_string(this->state->instances.size()) + " instances on stage", Colors::green, stage.info.in);
#endif
	}
}

void vision::goits::Descriptor::match(std::vector<VisionObject> &objects,
	std::vector<VisionObject*> &results)
{
	for (VisionObject &vo : objects)
	{
		if (this->isMatch(vo))
			results.emplace_back(&vo);
	}
}

bool vision::goits::Descriptor::isMatch(VisionObject &vo)
{
	return true;
}

void vision::goits::Descriptor::createNewInstance(VisionObject *object, Stage* stage)
{
	VERBOSE("Creating object");
	ObjectInstance* tmp = new ObjectInstance(stage);
	tmp->setState(stateFromVisionObject(tmp, object, stage));
	tmp->id = this->state->incrementalCounter++;
	this->state->incrementalCounter++;
	this->state->instances.push_back(tmp);
}

vision::goits::DescriptorState::DescriptorState()
	: incrementalCounter(0)
{ }
