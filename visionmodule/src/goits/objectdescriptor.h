/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#ifndef VISIONMODULE_GOITS_OBJECTDESCRIPTOR_H
#define VISIONMODULE_GOITS_OBJECTDESCRIPTOR_H

#include <vector>
#include <string>

#include "../../libvideo/visionobject.h"
#include "../../libvideo/framebuffer.h"

#include "objectinstance.h"
#include "../../libvideo/colourdefinition.h"

namespace vision
{
	namespace goits
	{
		class StageInfo;

		class ObjectInstance;

		class DescriptorState
		{
		public:
			DescriptorState();

			unsigned int incrementalCounter;
			std::vector<ObjectInstance*> instances;
		};

		/**
		 * Class that represent each entity searchble on the stage. Eg: BallDescriptor, LineDescriptor,
		 * BasketDescriptor.
		 */
		class Descriptor
		{
		protected:
			virtual void createState();

			virtual void find(StageInfo &info, std::vector<VisionObject> &vector) = 0;
			virtual bool isMatch(VisionObject &vo);

			virtual void createNewInstance(VisionObject *vo, Stage *stageInfo);
		public:
			DescriptorState *state;

			Descriptor();

			virtual void track(Stage &stage);
			void match(std::vector<VisionObject> &objects, std::vector<VisionObject*> &results);
		};


		class VisionObjectCandidate
		{
		public:
			VisionObject *visionObject;
			double error;

			VisionObjectCandidate(VisionObject *visionObject);
		};
	}
}


#endif //VISIONMODULE_GOITS_OBJECTDESCRIPTOR_H
