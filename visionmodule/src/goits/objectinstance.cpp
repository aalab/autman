/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#include "objectinstance.h"
#include "../compilationdefinitions.h"
#include "../../libvideo/imageprocessing.h"
#include "../videostream.h"
#include "../globals.h"
#include "../colors.h"

vision::goits::ObjectInstance::ObjectInstance(Stage* stage)
	: state(NULL), historyCapacity(5), stage(stage)
{ }

vision::goits::ObjectInstance::~ObjectInstance()
{
	if (this->state != NULL)
		delete this->state;
	this->clearHistory();
}

void vision::goits::ObjectInstance::appendHistory()
{
	this->history.push_back(this->state);
	if (this->history.size() > this->historyCapacity)
	{
		ObjectState *s = this->history.back();
		this->history.pop_back();
		delete s;
	}
}

void vision::goits::ObjectInstance::clearHistory()
{
	for (ObjectState *s : this->history)
		delete s;
	this->history.clear();
}

vision::goits::ObjectState *vision::goits::ObjectInstance::getState() const
{
	return this->state;
}

void vision::goits::ObjectInstance::setState(ObjectState *state)
{
	if (this->state != NULL)
	{
		this->appendHistory();
		this->state = state;

		this->updateCacheData();
	}
	else
	{
		this->state = state;
	}
}

vision::goits::ObjectState::ObjectState(ObjectInstance *instance)
	: objectinstance(instance)
{ }

void vision::goits::ObjectInstance::updateCacheData()
{
	if (!this->history.empty())
	{
		double dtime =
			std::chrono::duration_cast<std::chrono::milliseconds>(this->state->time - this->history.front()->time).count();

		this->state->velocity.x = (this->state->center.x - this->history.front()->center.x) / dtime;
		this->state->velocity.y = (this->state->center.y - this->history.front()->center.y) / dtime;

		this->state->acceleration.x = (this->state->velocity.x - this->history.front()->velocity.x) / dtime;
		this->state->acceleration.y = (this->state->velocity.y - this->history.front()->velocity.y) / dtime;

		VERBOSE(
			std::endl << "\tdtime: " << dtime <<
			std::endl << "\tNew Velocity: " << this->state->velocity.x << ":" << this->state->velocity.y <<
			std::endl << "\tNew Acceleration: " << this->state->acceleration.x << ":" << this->state->acceleration.y
		);
	}
	else
	{
		this->state->velocity.x = this->state->velocity.y = 0.0;
		this->state->acceleration.x = this->state->acceleration.y = 0.0;
	}
}

vision::goits::ObjectState *vision::goits::stateFromVisionObject(ObjectInstance *instance, VisionObject *visionObject, Stage* stage)
{
	ObjectState *result = new ObjectState(instance);

	result->center.x = visionObject->boundBox.bbCenterX();
	result->center.y = visionObject->boundBox.bbCenterY();

	result->position.x = visionObject->boundBox.topLeft().x();
	result->position.y = visionObject->boundBox.topLeft().y();

	result->size.x = visionObject->boundBox.width();
	result->size.y = visionObject->boundBox.height();

	result->area = visionObject->size;
	result->time = stage->info.currentTime;

	return result;
}

bool vision::goits::ObjectInstance::match(std::vector<VisionObject> &objects)
{
	double
		error,
		lError = INFINITY;
	VisionObject
		*lVo = nullptr;
	unsigned int
		index = 0,
		lIndex = 0;
	for (VisionObject &vo : objects)
	{
		error = this->matchError(&vo);
		if (error < lError)
		{
			lError = error;
			lVo = &vo;
			lIndex = index;
		}
		index++;
	}

	if (lVo && (lError < 0.1))
	{
		VERBOSE(lError << " accepted");
		this->setState(vision::goits::stateFromVisionObject(this, lVo, this->stage));
		objects.erase(objects.begin() + lIndex);
		this->matched = true;
	}
	else
	{
		VERBOSE(lError << " rejected");
		this->matched = false;
	}
	return this->matched;
}

double vision::goits::ObjectInstance::matchError(VisionObject *object)
{
// 3 pixels per millisecond
#define MAX_SPEED 3.0

// 3 pixels of area change tolerance
#define AREA_TOLERANCE 3.0
	double
		dtime = (std::chrono::duration_cast<std::chrono::milliseconds>(this->stage->info.currentTime - this->getState()->time)).count(),

		areaX = std::abs(1.0 - (object->boundBox.width() / this->getState()->size.x)),
		areaY = std::abs(1.0 - (object->boundBox.height() / this->getState()->size.y)),

		speed = std::abs(
			this->getState()->position.distance(object->center.x, object->center.y) / dtime
		) / MAX_SPEED,

/*
		speedX = std::abs(
			(object->bBox.topLeft().x() - this->getState()->position.x) / dtime
		),
		speedY = std::abs(
			(object->bBox.topLeft().y() - this->getState()->position.y) / dtime
		),
*/

		areaToleranceX = (AREA_TOLERANCE / this->getState()->size.x),
		areaToleranceY = (AREA_TOLERANCE / this->getState()->size.y),

		overlapMin = ImageProcessing::overlapMin(
			this->getState()->position.x, this->getState()->position.y, this->getState()->size.x, this->getState()->size.y,
			object->boundBox.topLeft().x(), object->boundBox.topLeft().y(), object->boundBox.width(), object->boundBox.height()
		);

	// dtime = 500.0;

	double
		fVelocityX = this->getState()->velocity.x + this->getState()->acceleration.x/dtime,
		fVelocityY = this->getState()->velocity.y + this->getState()->acceleration.y/dtime,

		fX = this->getState()->position.x + fVelocityX * dtime,
		fY = this->getState()->position.y + fVelocityY * dtime;

	ImageProcessing::drawCross(this->stage->info.in, fX, fY, Colors::pink, 3);

	VERBOSE(std::endl <<
		"\tdTime: " << dtime << std::endl <<
		"\tFrom " << this->getState()->position.x << ":" << this->getState()->position.y << " to " << object->boundBox.topLeft().x() << ":" << object->boundBox.topLeft().y() << std::endl <<
		"\tPosition: " << object->boundBox.topLeft().x() << ":" << object->boundBox.topLeft().y() << " (prevision " << fX << ":" << fY << ")" << std::endl <<
		"\tVelocity: " << fVelocityX << ":" << fVelocityY << std::endl <<
		"\tSpeed: " << speed << std::endl <<
		"\tSize: " << areaX << ":" << areaY << std::endl <<
		"\tOverlap (max:min): " << ImageProcessing::overlapMax(
				this->getState()->position.x, this->getState()->position.y, this->getState()->size.x, this->getState()->size.y,
				object->boundBox.topLeft().x(), object->boundBox.topLeft().y(), object->boundBox.width(), object->boundBox.height()
			) << " : " << overlapMin << std::endl <<
		"\tTolerance: " << areaToleranceX << ":" << areaToleranceY << std::endl
	);

	if (
		(
			(
				(areaX < areaToleranceX)
				&& (areaY < areaToleranceY)
			)
			||
			(
				(overlapMin > 0.8)
				&& (
					(areaX < areaToleranceX)
					|| (areaY < areaToleranceY)
				)
			)
		)
		&& (speed < 1.0)
	)
		return 0.0;
	else
		return INFINITY;
}

bool vision::goits::ObjectState::isExpired()
{
	return
		std::chrono::duration_cast<std::chrono::milliseconds>(this->objectinstance->stage->info.currentTime - this->time).count() > 500;
}
