/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#include "stage.h"
#include "../compilationdefinitions.h"
#include "../text.h"
#include "../colors.h"

unsigned int t = 0;

void vision::goits::Stage::process(std::chrono::system_clock::time_point &current)
{
	VERBOSE("Starting ...");
	this->info.currentTime = current;
	for (Descriptor * od : this->descriptors)
	{
		od->track(*this);
		VERBOSE("Instances: " << od->state->instances.size());
		for (ObjectInstance *i : od->state->instances)
		{
			vision::Text::draw(
				i->getState()->position.x, i->getState()->position.y, to_string(i->id), Colors::cyan, this->info.in
			);
		}
	}
	VERBOSE("Ending." << endl << "---------------------");
}
