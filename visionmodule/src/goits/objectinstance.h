/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#ifndef VISIONMODULE_GOITS_OBJECTINSTANCE_H
#define VISIONMODULE_GOITS_OBJECTINSTANCE_H

#include <list>
#include <vector>
#include <stddef.h>
#include <chrono>

#include "../../libvideo/point.h"
#include "../../libvideo/visionobject.h"

namespace vision
{
	namespace goits
	{

		class Stage;

		class ObjectInstance;

		/**
		 * Represents the state of an object at a time.
		 */
		class ObjectState
		{
		public:
			ObjectState(ObjectInstance *instance);

			ObjectInstance *objectinstance;

			std::chrono::system_clock::time_point time;

			Point2D center;
			Point2D position;
			Point2D size;

			unsigned int area;

			Point2D velocity;
			Point2D acceleration;

			bool isExpired();
		};

		/**
		 * Create an ObjectState instance from a VisionObject.
		 *
		 * ATTENTION ON USE! Possible memory leak if not delete object after use!
		 */
		ObjectState *stateFromVisionObject(ObjectInstance *instance, VisionObject *visionObject, Stage *stage);

		class ObjectInstance
		{
		private:
			/**
			 * Current state of the instance.
			 */
			ObjectState* state;


		public:

			ObjectInstance(Stage* stage);
			~ObjectInstance();

			Stage* stage;

			unsigned int id;

			/**
			 * Keep the current state of the object. It is a pointer for future use of polymorphism to represent
			 * different object states.
			 */
			std::list<ObjectState*> history;

			unsigned int historyCapacity;

			/**
			 * Stores the last result of the `match` function.
			 */
			bool matched;

			/**
			 * Stores the time point of the last `match` function.
			 */
			std::chrono::system_clock::time_point matchedTime;

			ObjectState *getState() const;
			void setState(ObjectState *state);

			bool match(std::vector<VisionObject> &objects);

			/**
			 * Clear the whole history (ensure that deletes all ObjectState).
			 */
			virtual void clearHistory();
		protected:
			/**
			 * Compact and add the current state to the history.
			 */
			virtual void appendHistory();

			/**
			 * Update the data from the current state.
			 */
			virtual void updateCacheData();

			/**
			 * Matching error from a VisionObject instance.
			 */
			virtual double matchError(VisionObject* object);
		};
	}
}

#endif //VISIONMODULE_GOITS_OBJECTINSTANCE_H
