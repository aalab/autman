/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 16, 2015
 */

#include "ball.h"
#include "../stageinfo.h"
#include "../../../libvideo/imageprocessing.h"
#include "../../colors.h"

vision::goits::descriptors::BallDescriptor::BallDescriptor()
{ }

bool vision::goits::descriptors::BallDescriptor::isMatch(VisionObject &vo)
{
	double r = (vo.boundBox.width() / (double) vo.boundBox.height());
	return (r >= 0.8) && (r <= 1.2) && (vo.boundBox.height() > 20);
}

void vision::goits::descriptors::BallDescriptor::find(vision::goits::StageInfo &info, std::vector<VisionObject> &objects)
{
	ColourDefinition* ball = info.colourByName("ball");
	if (ball)
		ImageProcessing::SegmentColours(info.in, info.out, 50, 5, 10, 1, *ball, Colors::red, objects, true, true);
}
