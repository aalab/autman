/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 16, 2015
 */

#ifndef VISIONMODULE_BALLDESCRIPTOR_H
#define VISIONMODULE_BALLDESCRIPTOR_H

#include "ball.h"

#include "../objectdescriptor.h"

namespace vision
{
	namespace goits
	{
		namespace descriptors
		{
			class BallDescriptor
				: public vision::goits::Descriptor
			{
			protected:
				virtual void find(StageInfo &info, std::vector<VisionObject> &objects);

				// Check shape of the ball (proportion)
				virtual bool isMatch(VisionObject &vo);
			public:
				BallDescriptor();
			};
		}
	}
}


#endif //VISIONMODULE_BALLDESCRIPTOR_H
