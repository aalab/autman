/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#include "stageinfo.h"

ColourDefinition *vision::goits::StageInfo::colourByName(string colour)
{
	for (ColourDefinition &c : *this->colours)
	{
		if (c.name == colour)
			return &c;
	}
	return nullptr;
}
