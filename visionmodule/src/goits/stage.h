/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#ifndef VISIONMODULE_GOITS_STAGE_H
#define VISIONMODULE_GOITS_STAGE_H

#include <vector>

#include "../../libvideo/framebuffer.h"

#include "objectdescriptor.h"
#include "objectinstance.h"
#include "stageinfo.h"

namespace vision
{
	namespace goits
	{
		class Stage
		{
		public:
			std::vector<Descriptor *> descriptors;
			StageInfo info;

			virtual void process(std::chrono::system_clock::time_point &current);
		};
	}
}


#endif //VISIONMODULE_GOITS_STAGE_H
