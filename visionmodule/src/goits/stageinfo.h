/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 15, 2015
 */

#ifndef VISIONMODULE_STAGEINFO_H
#define VISIONMODULE_STAGEINFO_H

#include <vector>
#include <string>

#include "../../libvideo/framebuffer.h"
#include "../../libvideo/colourdefinition.h"
#include "objectinstance.h"

using namespace std;

namespace vision
{
	namespace goits
	{
		class ObjectInstance;

		class StageInfo
		{
		public:
			std::chrono::system_clock::time_point currentTime;

			FrameBuffer *in;
			FrameBuffer *out;

			std::vector<ColourDefinition> *colours;

			std::vector<vision::goits::ObjectInstance *> instances;

			ColourDefinition* colourByName(string colour);
		};
	}
}
#endif //VISIONMODULE_STAGEINFO_H
