/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date 4/1/15.
 */

#ifndef _VISIONMODULE_INTEGRALCIRCLE_H_
#define _VISIONMODULE_INTEGRALCIRCLE_H_


class IntegralCircle
{
public:
	double values[360];

	unsigned int maxValue;
	unsigned int minValue;

	double extract(int from, int to);
	double extract(int angle);

	double extractAverage(int from, int to);
	double proportion(int angle);
};


#endif //_VISIONMODULE_INTEGRALCIRCLE_H_
