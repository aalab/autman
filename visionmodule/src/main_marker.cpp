/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Fri Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "http/json.h"
#include "findmarker.h"
#include "globals.h"
#include "colors.h"
#include "compilationdefinitions.h"
#include "findgrid.h"

/*
 * Load a data file dumped from a frame.
 * Do the identification of the shapes by the colour.
 * Locates the marker.
 */
int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		std::cout << "Invalid params. Usage: find_marker <config> <raw_image>" << std::endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	std::vector<ColourDefinition> &colourDefs = config.colours;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);

	unsigned int subsample = 1;

	std::vector<VisionObject>
		tapeResults,
		markerResults,
		markerInsideResults;

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	fb->inFromPPM(argv[2]);

	ColourDefinition
		*marker = config.getColour("marker"),
		*markerInside = config.getColour("marker-inside"),
		*tape = config.getColour("tape");

	assert(marker != NULL);
	assert(markerInside != NULL);
	assert(tape != NULL);

	double
		markerSize = 10.0;

	ImageProcessing::SegmentColours(fb, outFb, 255, 5, 20, subsample, *tape, Colors::red, tapeResults, false, false);
	ImageProcessing::SegmentColours(fb, outFb, 255, 5, 20, subsample, *marker, Colors::blue, markerResults, false, false);
	ImageProcessing::SegmentColours(fb, outFb, 255, 5, 20, subsample, *markerInside, Colors::green, markerInsideResults, false, false);

	if (
		(tapeResults.size() > 0) && (markerResults.size() > 0) && (markerInsideResults.size() > 0)
	)
	{
		if (
			FindMarker::find2(
				fb, outFb, tapeResults, markerResults, markerInsideResults, FindGrid::cameraParameters,
				*config.currentCameraCalibration
			)
		)
		{
			VERBOSE("Marker found");
		}
		else
		{
			VERBOSE("Cannot find marker");
		}
	}
	else
	{
		ERROR("No vision object found.");
	}

	fb->outToPPM("ppms/result.ppm");
	outFb->outToPPM("ppms/resultOut.ppm");

	delete fb;
	delete outFb;
}
