/**
 *
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date Fri Feb 6, 2015
 */

#include <sstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "colors.h"
#include "text.h"
#include "compilationdefinitions.h"

/*
 * Load a data file dumped from a frame.
 * Do the identification of the shapes by the colour.
 * Locates the grid.
 */
int main(int argc, char * argv[])
{
	if (argc < 6)
	{
		printf("Invalid params. Usage: find_grid <frame_file> <trapezoid1> <trapezoid2> <trapezoid3> <trapezoid4>\n");
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	unsigned int subsample = 1;

	Trapezoid trapezoid;
	{
		unsigned int tmp1, tmp2;

		printf("Trapezoid:");
		for (unsigned int i = 0; i < 4; i++)
		{
			if (sscanf(argv[2+i], "%d,%d", &tmp1, &tmp2) == 2)
			{
				trapezoid.coords[i].setX(tmp1);
				trapezoid.coords[i].setY(tmp2);
				printf(" (%d %d)", tmp1, tmp2);
			}
			else
			{
				printf("Error reading trapezoid #%d\n", i);
				exit(101);
			}
		}
		printf("\n");
	}

	std::vector<VisionObject> results;

	fb->initialize(320, 240);
	outFb->initialize(320, 240);

	tsai2D_define_camera_parameters(NEW_CAMERA, 320, 320, 1.0, 1.0, 320 / 2, 240 / 2, 1.0, &FindGrid::cameraParameters);

	FILE *f = fopen(argv[1], "r");
	if (!f)
	{
		printf("'%s' cannot be opened.\n", argv[1]);
		exit(1);
	}
	fread(fb->buffer, fb->frameSize, 1, f);
	fclose(f);

	printf("Frame buffer loaded.\n");

	ColourDefinition colour;
	{
		std::istringstream strstream("{ball&254&0&0&255&255&255&0&0&0&254&254&0&0&0&0&255&255&255}");
		strstream >> colour;
	}

	printf("Colour: %d %d %d\n", colour.min.red, colour.min.green, colour.min.blue);

	RawPixel p;
	for (unsigned int x = 0; x < fb->width; ++x)
	{
		for (unsigned int y = 0; y < fb->height; ++y)
		{
			fb->getPixel(y, x, &p);
			p.red = p.green = p.blue = ((p.red + p.green + p.blue)/3 < 100)?255:0;
			fb->setPixel(y, x, p);
		}
	}

	unsigned int rows = 5, cols = 6;

	ImageProcessing::SegmentColours(fb, outFb, 50, 5, 20, subsample, colour, Colors::red, results, true, true);

	std::vector<VisionObject> clusterized;
	FindGrid::clusterizeObjects(results, clusterized, 10);

	clusterized = results;

	printf("%d objects were found. %d after clusterization.\n", results.size(), clusterized.size());

	CameraCalibrationData dt;
	dt.params.columns = 6;
	dt.params.rows = 5;
	dt.params.distance.x = 3.4;
	dt.params.distance.y = 3.4;
	dt.params.origin.x = 0;
	dt.params.origin.y = 0;

	if (clusterized.size() > 0)
	{
		int j = 0;

		std::vector<VisionObject *> grid;
		if (FindGrid::find(fb, outFb, trapezoid, clusterized, grid, rows, cols))
		{
			unsigned int count = 0;
			for (std::vector<VisionObject *>::iterator i = grid.begin(); i != grid.end(); ++i)
			{
				ImageProcessing::drawRectangle(fb, (*i)->boundBox, Colors::green);
				vision::Text::draw((*i)->boundBox.bbCenterX(), (*i)->boundBox.bbCenterY(), std::to_string(count), Colors::pink, outFb);
				count++;
			}

			if (FindGrid::calibrate(fb, outFb, grid, dt, dt.getResult()))
			{

				double wx, wy;
				for (unsigned int x = 0; x < cols; x++)
				{
					for (unsigned int y = 0; y < rows; y++)
					{
						world_coord_to_image_coord(
							x*dt.params.distance.x,
							y*dt.params.distance.y,
							0,
							&wx, &wy,
							&FindGrid::cameraParameters,
							&dt.getResult()
						);
						ImageProcessing::drawCross(fb, wx, wy, Colors::pink);
					}
					// ImageProcessing::drawBresenhamLine(fb, x *dt.params.distance.x, 50, (x +1)*20, 55, Colors::pink);
				}
			}
			else
			{
				VERBOSE("Not calibrated");
			}
		}
		else
		{
			VERBOSE("Grid not found");
		}
	}

	fb->outToPPM("result.ppm");
	outFb->outToPPM("resultOut.ppm");

	delete fb;
	delete outFb;
}
