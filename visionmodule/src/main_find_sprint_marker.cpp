/**
*
* @author Jamillo Santos <jamillo@gmail.com>
* @date Feb 6, 2015
*/

#include <sstream>
#include <json/value.h>
#include <json/reader.h>
#include <fstream>

#include "../libvideo/framebuffer.h"
#include "../libvideo/colourdefinition.h"
#include "../libvideo/imageprocessing.h"
#include "../libvideo/framebufferrgb24.h"
#include "../libvideo/framebufferrgb24be.h"
#include "findgrid.h"
#include "http/json.h"
#include "findmarker.h"
#include "../libvideo/framebufferrgb565.h"
#include "../libvideo/framebufferrgb32.h"
#include "../libvideo/framebufferbayer.h"
#include "globals.h"
#include "colors.h"
#include "findsprintmarker.h"

/*
 * Load a data file dumped from a frame.
 * Do the identification of the shapes by the colour.
 * Locates the marker.
 */
int main(int argc, char * argv[])
{
	if (argc < 3)
	{
		cout << "Invalid params. Usage: " << argv[0] << "s <config> <raw_image>" << endl;
		exit(2);
	}
	FrameBuffer
		*fb = new FrameBufferRGB24BE(),
		*outFb = new FrameBufferRGB24BE();

	Configuration config;
	{
		ifstream configFile;
		configFile.open(argv[1]);
		config.UpdateConfiguration(configFile);
	}
	Globals::GetGlobals()->configuration = &config;

	std::vector<ColourDefinition> &colourDefs = config.colours;

	fb->initialize(config.width, config.height);
	outFb->initialize(config.width, config.height);

	unsigned int subsample = 1;

	tsai2D_define_camera_parameters(NEW_CAMERA, fb->width, fb->width, 1.0, 1.0, fb->width/2, fb->width/2, 1.0, &FindGrid::cameraParameters);

	cout << "Loading frame from " << argv[2] << endl;
	fb->inFromPPM(argv[2]);

	ColourDefinition
		*sup = config.getColour("sup"),
		*sdown = config.getColour("sdown"),
		/// *grass = config.getColour("grass"),
		*tape = config.getColour("tape"),
		*markerCD = config.getColour("marker");

	assert(sup != NULL);
	assert(markerCD != NULL);
	assert(tape != NULL);

	std::vector<VisionObject>
		supResults,
		sdownResults,
		tapeResults;

	std::vector<VisionObject*>
		supFiltered, sdownFiltered;

	ImageProcessing::SegmentColours(fb, outFb, 50, 5, 20, subsample, *sup, Colors::yellow, supResults, false, true);
	ImageProcessing::SegmentColours(fb, outFb, 50, 5, 20, subsample, *tape, Colors::blue, tapeResults, false, true);
	ImageProcessing::SegmentColours(fb, outFb, 50, 5, 20, subsample, *markerCD, Colors::green, tapeResults, false, true);

	unsigned int topX, topY, bottomX, bottomY;

	SprintMarker marker;
	if (FindSprintMarker::find(fb, outFb, supResults, sdownResults, *tape, *markerCD, tapeResults, NULL, &marker,
		FindGrid::cameraParameters, *config.currentCameraCalibration, &topX, &topY, &bottomX, &bottomY))
	{
		ImageProcessing::drawCross(outFb, marker.centerX, marker.centerY, Colors::red);

		cout << "Marker found." << endl;
	}
	else
		cout << "Marker not found." << endl;

	fb->outToPPM("result.ppm");
	outFb->outToPPM("resultOut.ppm");

	delete fb;
	delete outFb;
}
