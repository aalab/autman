/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 20, 2015
 */

#ifndef VISIONMODULE_FINDBALL_H
#define VISIONMODULE_FINDBALL_H


#include "../libvideo/framebuffer.h"

#include <glob.h>
#include "../libvideo/colourdefinition.h"
#include "colors.h"
#include "../libvideo/visionobject.h"
#include "compilationdefinitions.h"
#include "cameracalibrationdata.h"

class FindBall
{
public:
	static bool find(
		FrameBuffer *frame, FrameBuffer *outFrame, int subsample, ColourDefinition *ballColourDefinition,
		std::vector<VisionObject> &balls, std::vector<VisionObject*> &results, std::vector<VisionObject> &vectorList,
		std::chrono::system_clock::time_point &last_time, CameraCalibrationData &cameraCalibration
	);
	static bool find2(
		FrameBuffer *frame, FrameBuffer *callOutFrame, FrameBuffer *grassOutFrame, ColourDefinition *tape, FrameBuffer *tapeOutFrame,
		ColourDefinition *ball, FrameBuffer *ballOutFrame, std::vector<VisionObject> &grass,
		std::vector<VisionObject> &tapes, std::vector<VisionObject> &balls
	);
};


#endif //VISIONMODULE_FINDBALL_H
