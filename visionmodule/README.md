
Camera Calibration using Tsai’s planar method
=============================================

What is done?
-------------

I extracted three frames and stored on `visionmodule/bin/debug/data_t[1-3]`. These three images were tested and the grid
is successfully detected on them.

The result of the "static" computation is stored on `visionmodule/bin/debug/result_t[1-3].jpg`. To run the files you
created shells scripts `visionmodule/bin/debug/t[1-3]_find_grid`.

The live detection is working as well. However, sometimes a segmentation fault raises (I would need more time to find
the cause). To run the live detection you have to select "Calibration Processing" at "Processing mode" <select>. Then,
you must to select the area (a trapezoid in the following order: SW, SW, NE, NW).

Process
-------

First I identify that the segment detection is better using two colors images (black and white). All pixels lower than
100 (brightness) where converted to white, otherwise the pixels was converted to black (yes, negative effect).

Then I used the `ImageProcessing::SegmentColours` to trace the white colour (white is better detected than black).

With help of the trapezoid, which is provided by the user, I trace the grid.

TODO
----

The grid was "successfully" found but the real world coordinates were not calculated yet. Since the coordinates are
already sorted. It would not be a hard task.

