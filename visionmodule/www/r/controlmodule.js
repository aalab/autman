/**
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *		 http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author mwichary@google.com (Marcin Wichary)
 */

/*
 * 
 * Adding custom features for the robot.
 * 
 * @author Jamillo Santos <jamillo@gmail.com>
 */
// (function () {
var gamepadSupport = {

	threshold: 0.3,

	models: [
		{
			name: 'xbox logitech (linux)',
			buttons:
			{
				'0': 'a',
				'1': 'b',
				'2': 'x',
				'3': 'y',
				'4': 'lbumper',
				'5': 'rbumper',
				'6': 'ltrigger',
				'7': 'rtrigger',
				'8': 'back',
				'9': 'start',
				'10': 'ljoystick',
				'11': 'rjoystick',
				'16': 'xbox'
			},
			axes:
			{
				'0': 'ljoystick_x',
				'1': 'ljoystick_y',
				'2': 'rjoystick_x',
				'3': 'rjoystick_y'
			}
		},
		{
			name: 'xbox official (linux)',
			buttons:
			{
				'0': 'a',
				'1': 'b',
				'2': 'x',
				'3': 'y',
				'4': 'lbumper',
				'5': 'rbumper',
				'6': 'back',
				'7': 'start',
				'8': 'xbox',
				'9': 'ljoystick',
				'10': 'rjoystick'
			},
			axes:
			{
				'0': 'ljoystick_x',
				'1': 'ljoystick_y',
				'2': 'ltrigger',
				'3': 'rjoystick_x',
				'4': 'rjoystick_y',
				'5': 'rtrigger',
				'6': 'cross_x',
				'7': 'cross_y'
			}
		}
	],

	// A number of typical buttons recognized by Gamepad API and mapped to
	// standard controls. Any extraneous buttons will have larger indexes.
	TYPICAL_BUTTON_COUNT: 16,

	// A number of typical axes recognized by Gamepad API and mapped to
	// standard controls. Any extraneous buttons will have larger indexes.
	TYPICAL_AXIS_COUNT: 4,

	// Whether we’re requestAnimationFrameing like it’s 1999.
	ticking: false,

	// The canonical list of attached gamepads, without “holes” (always
	// starting at [0]) and unified between Firefox and Chrome.
	gamepads: [],
	_gamepads: {},

	// Remembers the connected gamepads at the last check; used in Chrome
	// to figure out when gamepads get connected or disconnected, since no
	// events are fired.
	prevRawGamepadTypes: [],

	// Previous timestamps for gamepad state; used in Chrome to not bother with
	// analyzing the polled data if nothing changed (timestamp is the same
	// as last time).
	prevTimestamps: [],

	/**
	 * Initialize support for Gamepad API.
	 */
	init: function() {
		var gamepadSupportAvailable = navigator.getGamepads || !!navigator.webkitGetGamepads || !!navigator.webkitGamepads;
		this.currentModel = this.models[0];

		if (!gamepadSupportAvailable)
			$('.gamepad-view').addClass('unavailable');
		else
		{
			if ('ongamepadconnected' in window)
			{
				window.addEventListener('gamepadconnected', gamepadSupport.onGamepadConnect, false);
				window.addEventListener('gamepaddisconnected', gamepadSupport.onGamepadDisconnect, false);
			}
			else
				gamepadSupport.startPolling();
		}
	},

	/**
	 * React to the gamepad being connected.
	 */
	onGamepadConnect: function(event) {
		gamepadSupport.gamepads.push(event.gamepad);
		gamepadSupport._gamepads[event.gamepad.id] = event.gamepad;
		gamepadSupport.startPolling();

		/*
		this._buttons = {};
		for (var i in event.gamepad.buttons)
		{
			this._buttons[i] = $('<div class="gamepad-status"></div>').css('left', i * 50).prependTo('body');
		}
		this._axes = {};
		for (var i in event.gamepad.axes)
		{
			this._axes[i] = $('<div class="gamepad-status" style="top: 50px"></div>').css('left', i * 50).prependTo('body');
		}
		*/
		$('.gamepad-view').addClass('active');
	},

	/**
	 * React to the gamepad being disconnected.
	 */
	onGamepadDisconnect: function(event) {
		console.log('onGamepadConnection');
		// Remove the gamepad from the list of gamepads to monitor.
		for (var i in gamepadSupport.gamepads)
		{
			if (gamepadSupport.gamepads[i].index == event.gamepad.index) {
				gamepadSupport.gamepads.splice(i, 1);
				break;
			}
		}

		// If no gamepads are left, stop the polling loop.
		if (gamepadSupport.gamepads.length == 0) {
			gamepadSupport.stopPolling();
		}

		$('.gamepad-view').addClass('active');
	},

	/**
	 * Starts a polling loop to check for gamepad state.
	 */
	startPolling: function() {
		// Don’t accidentally start a second loop, man.
		if (!gamepadSupport.ticking)
		{
			gamepadSupport.ticking = true;
			gamepadSupport.tick();
		}
	},

	/**
	 * Stops a polling loop by setting a flag which will prevent the next
	 * requestAnimationFrame() from being scheduled.
	 */
	stopPolling: function() {
		gamepadSupport.ticking = false;
	},

	/**
	 * A function called with each requestAnimationFrame(). Polls the gamepad
	 * status and schedules another poll.
	 */
	tick: function() {
		gamepadSupport.pollStatus();
		gamepadSupport.scheduleNextTick();
	},

	scheduleNextTick: function() {
		if (gamepadSupport.ticking)
		{
			if (window.requestAnimationFrame)
				window.requestAnimationFrame(gamepadSupport.tick);
			else if (window.mozRequestAnimationFrame)
				window.mozRequestAnimationFrame(gamepadSupport.tick);
			else if (window.webkitRequestAnimationFrame)
				window.webkitRequestAnimationFrame(gamepadSupport.tick);
		}
	},

	/**
	 * Checks for the gamepad status. Monitors the necessary data and notices
	 * the differences from previous state (buttons for Chrome/Firefox,
	 * new connects/disconnects for Chrome). If differences are noticed, asks
	 * to update the display accordingly. Should run as close to 60 frames per
	 * second as possible.
	 */
	pollStatus: function()
	{
		gamepadSupport.pollGamepads();

		var gamepad;
		for (var i in gamepadSupport.gamepads)
		{
			gamepad = gamepadSupport.gamepads[i];

			if (gamepad.timestamp && (gamepad.timestamp == gamepadSupport.prevTimestamps[i]))
				continue;
			gamepadSupport.prevTimestamps[i] = gamepad.timestamp;
			gamepadSupport.updateDisplay(gamepad);
		}
	},

	pollGamepads: function()
	{
		var rawGamepads =
			(navigator.getGamepads && navigator.getGamepads()) ||
			(navigator.webkitGetGamepads && navigator.webkitGetGamepads());

		var
			curr = [],
			news = [];

		var old = this._gamepads;

		this._gamepads = {};

		if (rawGamepads)
		{
			for (var i = 0; i < rawGamepads.length; i++)
			{
				if (rawGamepads[i])
				{
					if (!old[rawGamepads[i].id])
						news.push(rawGamepads[i]);
					else
						curr.push(this._gamepads[rawGamepads[i].id] = rawGamepads[i]);
				}
			}

			for (var i = 0; i < news.length; i++)
				this.onGamepadConnect({
					gamepad: news[i]
				});
			for (var i in old)
				if (!this._gamepads[i])
					this.onGamepadDisconnect(old[i]);
		}
	},

	angleLimits: {
		tilt: {
			min: -25,
			max: 60
		}
	},

	lastGamepadCommand: new Date(),

	__handleGamepadSuccess: function (data)
	{
		if (typeof data != "object")
			data = JSON.parse(data);

		var $robotTilt = $('.robot-positioning .robot-head-tilt');

		$('.robot-positioning .robot-head')
			.css({
				'-webkit-transform': 'rotate(' + data.head.params.pan + 'deg)',
				'-moz-transform': 'rotate(' + data.head.params.pan + 'deg)',
				'-ms-transform': 'rotate(' + data.head.params.pan + 'deg)',
				'-o-transform': 'rotate(' + data.head.params.pan + 'deg)',
				'transform': 'rotate(' + data.head.params.pan + 'deg)'
			});

		var h = document.getElementById('videoContainer').offsetHeight;
		$robotTilt.css('top', h-(((data.head.params.tilt - this.angleLimits.tilt.min)/(this.angleLimits.tilt.max - this.angleLimits.tilt.min))*h) - ($robotTilt[0].offsetHeight/2));
	},

	// Call the tester with new state and ask it to update the visual
	// representation of a given gamepad.
	updateDisplay: function(gamepad)
	{
		var now = new Date();
		if ((now.getTime() - this.lastGamepadCommand.getTime()) > 30)
		{
			var me = this;
			var params = {};

			for (var i in gamepad.buttons)
			{
				params[this.currentModel.buttons[i]] = gamepad.buttons[i].value;
			}

			// fix for logitech controller
			params['cross_x'] = (gamepad.buttons[14].value > 0 ? -1 : (gamepad.buttons[15].value > 0) ? 1 : 0 );
			params['cross_y'] = (gamepad.buttons[12].value > 0 ? -1 : (gamepad.buttons[13].value > 0) ? 1 : 0 );
			// ---------------------------

			var axis;
			for (var i in gamepad.axes)
			{
				axis = gamepad.axes[i];
				if ((axis >= this.threshold) || (axis <= -this.threshold))
					axis = (Math.abs(axis) - this.threshold)/(1 - this.threshold) * (axis > 0 ? 1 : -1);
				else
					axis = 0;
				params[this.currentModel.axes[i]] = axis;
			}

			services.gamepad(params)
				.success($.proxy(me.__handleGamepadSuccess, me))
				.error(function (data) {
					console.log('gamepad::error');
				});
			this.lastGamepadCommand = now;
		}
	}
};

$(function () {
	gamepadSupport.init();
});
// });