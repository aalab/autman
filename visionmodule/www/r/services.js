/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 30, 2015
 **/

var services = {
	keyboard: function (payload)
	{
		return $.ajax({
			url: '/teleop',
			method: 'POST',
			datatType: 'json',
			data: JSON.stringify({type:"keyboard", data: payload})
		});
	},
	xbox: function (payload)
	{
		return $.ajax({
			url: '/teleop',
			method: 'POST',
			datatType: 'json',
			data: JSON.stringify({type:"xbox", data: payload})
		});
	},
	touch: function (payload)
	{
		return $.ajax({
			url: '/teleop',
			method: 'POST',
			datatType: 'json',
			data: JSON.stringify({type:"touch", data: payload})
		});
	},
	messages: function ()
	{
		return $.ajax({
			url: '/messages',
			method: 'POST',
			datatType: 'json',
			data: '{}'
		});
	},
	robot:
	{
		// Default name of the robot
		name: "Arash",

		baseUrl: '',
		// baseUrl: 'http://localhost:8080',

		/**
		 * General purpose method to send data to the HTTP proxy which redirects it to the Robot Server.
		 *
		 * @param data JSON that will be sent to the server.
		 * @returns jqXHR
		 * @see http://api.jquery.com/jQuery.ajax/#jqXHR
		 */
		robot: function (data)
		{
			data.name = this.name;
			return $.ajax({
				url: this.baseUrl + '/robot',
				method: 'POST',
				dataType: 'json',
				// contentType: 'application/json',
				data: JSON.stringify(data)
			});
		},

		/**
		 * Read the state of all servos.
		 *
		 * @returns jqXHR
		 * @see http://api.jquery.com/jQuery.ajax/#jqXHR
		 */
		readState: function ()
		{
			var data = {
				command: 'ReadState',
				params: {}
			};
			if (arguments.length > 0)
			{
				data.params.actuators = [];
				for (var i in arguments)
					data.params.actuators.append(arguments[i]);
			}
			return this.robot(data);
		},

		/**
		 * Set a position to an actuator. If any gain is supplied all of them must be supplied as well.
		 *
		 * @param actuator Actuator name
		 * @param angle Goal angle
		 * @param speed Speed of the movement
		 * @param gainP [optional] Proportional gain
		 * @param gainI [optional] Integral gain
		 * @param gainD [optional] Derivative gain
		 * @returns jqXHR
		 * @see http://api.jquery.com/jQuery.ajax/#jqXHR
		 */
		position: function (actuator, angle, speed, gainP, gainI, gainD)
		{
			var data = {
				command: 'Actuator',
				params:
				{
					command: 'Position',
					name: actuator,
					angle: angle,
					speed: speed
				}
			};
			if ((gainP !== null) || (gainI !== null) || (gainD !== null))
			{
				data.params.gainP = gainP;
				data.params.gainI = gainI;
				data.params.gainD = gainD;
			}
			return this.robot(data);
		},

		/**
		 * Turn of an actuator.
		 *
		 * @param actuator Actuator name
		 * @param mode Boolean turning torque on or off
		 * @returns jqXHR
		 * @see http://api.jquery.com/jQuery.ajax/#jqXHR
		 */
		actuatorMode: function (actuator, mode)
		{
			var data = {
				command: 'Actuator',
				params:
				{
					name: actuator,
					mode: (mode ? 'On' : 'Off')
				}
			};
			return this.robot(data);
		},
		

		/**Author	Bryan Wodi <talk2kamp@gmail.com>
		 * Moves x steps
		 * @param stop Optional. If true, ends the current motion at the end of the cycle.
		 * @param dx         The forward extension length of the legs
		 * @param dy         The backward extension length of the legs
		 * @param dtheta      The sideways extension length of the legs
		 * @param cycleTimeUSec  Time to do the motion
		 * @param cycles      The number of steps to make the robot take
		 */

		 move: function ( dx, dy, dtheta, cycleTimeUSec, cycles)
		 {
			  var data = {
				  command: 'Move',
				  params: {
					  'stop': true
	  			  }
      			  };

		          // Checking to see if dx was passed
			  if (typeof dx != 'undefined' && dx != null) 
				  data.params.dx = parseFloat(dx);                               
			  // Checking to see if dy was passed
			  if (typeof dy != 'undefined' && dy != null)
				  data.params.dy = parseFloat(dy);
			  // Checking to see if dtheta was passed
			  if (typeof dtheta != 'undefined' && dtheta != null)
				  data.params.dtheta = parseFloat(dtheta);
			  // Checking to see if cycleTimeUSec was passed
			  if (typeof cycleTimeUSec != 'undefined' && cycleTimeUSec != null)
				  data.params.cycleTimeUSec = parseInt(cycleTimeUSec);
			  // Checking to see if a cycles entry speed was passed
			  if (typeof cycles != 'undefined' && cycles != null)
				  data.params.cycles = parseInt(cycles);

			  return this.robot(data);
		 },




		/**
		 * Plays a trajectory
		 * 
		 * @param trajectory 		Trajectory name
		 * @param cycleTimeUsec		Time to do the trajectory
		 * @param cycle				Number of trajectories to do
		 * @param entryTimeUSec		Time to get to the first frame
		 * @param motionSpeed		[optional] The speed of the motion
		 * @param motionEntrySpeed	[optional] The speed to get to the first motion
		 */
		playTrajectory: function (trajectory, cycleTimeUsec, cycle, entryTimeUSec, motionSpeed, motionEntrySpeed)
		{
			var data = {
				command: 'Trajectory',

				params: {
					name: trajectory,
					cycleTimeUSec: cycleTimeUsec,
					cycles: cycle || 1
				}
			};

			// Checking to see if a motion speed was passed
			if (typeof entryTimeUSec != 'undefined')
				data.params.entryTimeUSec = entryTimeUSec;

			// Checking to see if a motion speed was passed
			if (typeof motionSpeed != 'undefined')
				data.params.motionSpeed = motionSpeed;

			// Checking to see if a motion entry speed was passed
			if (typeof motionEntrySpeed != 'undefined')
				data.params.motionEntrySpeed = motionSpeed;

			return this.robot(data);
		},
		
		/**
		 * Stores the trajectory
		 * 
		 * @param actuator			Actuator name
		 * @param trajectoryName	Trajectory name
		 * @param frameInfo			The frames for the trajectory
		 */
		uploadTrajectories: function(actuator, trajectoryName, frameInfo) 
		{
			var data = {
				command: 'Actuator',
				
				params: {
					command: 	'Trajectory',
					name:		actuator,
					trajectory:	trajectoryName,
					frames:		frameInfo
				}
			};
			
			return this.robot(data);
		}
	}
};
