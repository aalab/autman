
/*
 * @author J. Santos <jamillo@gmail.com>
 * @author Joel Wieb
 */

(function ($) {

	var touches;

	function TouchJoystick(config)
	{
		var me = this;

		this.config = config;

		this.$container = $(config.container);
		this.$dom = $(this.dom = this.initDom());
		this.handler = (this.$handler = this.$dom.find('.touch-handler'))[0];
		this.$container.append(this.$dom);

		this.axes = {
			x: 0,
			y: 0
		};

		this.center = {
			x: 0,
			y: 0
		};

		$(window)
			.on('resize', function () {
				me.refreshSize();
			})
			.on('touchstart mousedown', function (e)
			{
				var t;
				if (e.type == 'mousedown')
				{
					if (me.distance(e.pageX, e.pageY) < me.__radius)
						me.setIdentifier(1);
				}
				else
				{
					for (var i in e.originalEvent.touches)
					{
						if ((t = e.originalEvent.touches[i]).identifier)
						{
							if (me.distance(t.pageX, t.pageY) < me.__radius)
							{
								me.setIdentifier(t.identifier);
								break;
							}
						}
					}
				}
			})
			.on('touchmove mousemove', function (e)
			{
				if (e.type == 'mousemove')
				{
					if (me.__identifier)
						me.setXY(e.originalEvent.pageX, e.originalEvent.pageY);
				}
				else
				{
					if (me.__identifier)
					{
						for (var i in touches)
						{
							if (touches[i].identifier == me.__identifier)
							{
								me.setXY(touches[i].pageX, touches[i].pageY);
								break;
							}
						}
					}
				}
			})
			.on('touchend mouseup', function (e)
			{
				if (e.type == "mouseup")
					me.setIdentifier(false);
				else
				{
					if (me.__identifier && (!touches[me.__identifier]))
						me.setIdentifier(false);
				}
			});

		this.refreshSize();
		me.recalcCenter();
		setTimeout(function () {
			me.recalcCenter();
		}, 100);
	}

	TouchJoystick.prototype = {
		initDom: function ()
		{
			return $('<div class="touch-joystick"><div class="touch-handler" /></div>')[0];
		},
		refreshSize: function ()
		{
			this.setDiameter(window.innerWidth*(this.config.size || 0.1));
		},
		distance: function (x, y)
		{
			return Math.sqrt(Math.pow(this.center.x - x, 2) + Math.pow(this.center.y - y, 2));
		},
		setXY: function (x, y)
		{
			var
				d = this.distance(x, y);

			if (d > this.__radius)
			{
				var ang = Math.atan((this.center.y-y)/(this.center.x-x)) + (this.center.x-x < 0 ? 0 : Math.PI);
				x = this.center.x + Math.cos(ang) * this.__radius;
				y = this.center.y + Math.sin(ang) * this.__radius;
			}

			this.axes.x = (x - this.center.x) / this.__radius;
			this.axes.y = (y - this.center.y) / this.__radius;

			this.$handler
				.css('top', y - (this.handler.offsetHeight/2) - this.dom.offsetTop)
				.css('left', x - (this.handler.offsetHeight/2) - this.dom.offsetLeft);

			this.$dom.trigger('joystickmove');
		},

		setDiameter: function (diameter)
		{
			this.__diameter = diameter;
			this.__radius = diameter/2;
			this.$dom.css({
				top: (window.innerHeight*this.config.y) || 0,
				left: (window.innerWidth*this.config.x) || 0
			});
			this.$dom
				.css('width', diameter)
				.css('margin-left', -diameter/2)
				.css('height', diameter)
				.css('margin-top', -diameter/2);
			this.recalcCenter();
		},

		setIdentifier: function (identifier)
		{
			this.__identifier = identifier;
			if (!identifier)
				this.setXY(this.center.x, this.center.y);
		},

		/*
		 * Recalculate the center of the joystick related to the page.
		 */
		recalcCenter: function ()
		{
			var o = this.dom;
			this.center.x = o.offsetWidth/2;
			this.center.y = o.offsetHeight/2;
			while (o)
			{
				this.center.x += o.offsetLeft;
				this.center.y += o.offsetTop;
				o = o.offsetParent;
			}
		},

		on: function (event,func)
		{
			this.$dom.on(event, func);
			return this;
		},

		show: function ()
		{
			this.$dom.show();
			this.refreshSize();
			return this;
		},

		hide: function ()
		{
			this.$dom.hide();
			return this;
		}
	};

	$(window)
		.on('touchstart', function (e)
		{
			touches = e.originalEvent.touches;
		})
		.on('touchend', function (e)
		{
			touches = e.originalEvent.touches;
		})
		.on('touchmove', function (e)
		{
			touches = e.originalEvent.touches;
			e.preventDefault();
		});

	window.TouchJoystick = TouchJoystick;
})(jQuery);
