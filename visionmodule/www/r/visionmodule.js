/**
 * @author J. Santos <jamillo@gmail.com>
 */

(function ($) {
	var
		globalConfiguration,
		colourCtrl,
		calibrationCameraProcessingCtrl,
		videoCtrl,
		cameraParametersCtrl;

	var app = angular.module('Vision', [function (){
		var initInjector = angular.injector(["ng"]);
		var $http = initInjector.get("$http");
		var me = this;

		$http.post('/command/query')
			.success(function (data)
			{
				globalConfiguration = data;

				if (videoCtrl)
					videoCtrl.setProcessingMode(data.processingmode, true);

				if (colourCtrl)
					colourCtrl.setColours(data.colours);

				if (calibrationCameraProcessingCtrl)
					calibrationCameraProcessingCtrl.setCameraCalibrations(data.cameraCalibrations);

				if (cameraParametersCtrl)
					cameraParametersCtrl.setParams(data.camera);
			})
			.error(function (data){
				console.log('Could not load the configuration.');
			});
	}]);

	app.controller('VideoCtrl', ['$scope', '$http', '$element', function ($scope, $http, $element){
		var me = videoCtrl = this;

		var
			origin = {x:0,y:0},
			$canvas = $element.find('canvas'),
			ctx = $canvas[0].getContext('2d'),
			$videoContainer = $element.find('#videoContainer');

		this.filename = '';
		this.paused = false;
		this.modes = [
			{ name: 'raw', label: 'Raw' },
			{ name: 'showcolours', label: 'Show Colours' },
			{ name: 'segmentcolours', label: 'Segment Colours' },
			{ name: 'calibrationprocessing', label: 'Calibration Processing' },
			{ name: 'goalarea', label: 'Goal Area Test' },
			{ name: 'findmarker', label: 'Find Markers' },
			{ name: 'followline', label: 'Follow Line' },
			{ name: 'marathon', label: 'Marathon' },
			{ name: 'sprint', label: 'Sprint' },
			{ name: 'headtracking', label: 'Head tracking' },
			{ name: 'goistest', label: 'GOIS' },
			{ name: 'goalkeeper', label:'Goal Keeper'},
			{ name: 'obstaclerun', label:'Obstacle Run'}
		];
		me.trapezoid = [];

		this.setProcessingMode = function (value, notSave)
		{
			if (notSave)
			{
				this.processingMode = value;
				$scope.$apply();
			}
			else
			{
				$http.post('/command/processingmode', { mode: value })
					.success(function (data)
					{
						me.processingMode = value;
					})
					.error(function (data)
					{
						alert(data.message); // TODO: Improve error exibition.
					});
			}
		};

		this.isProcessingMode = function (processingMode)
		{
			return this.processingMode == processingMode;
		}

		this.savePpm = function ()
		{
			$http.post('/command/saveppm', { filename: me.filename })
				.success(function (data) {
					me.filename = '';
					alert('File saved.'); // TODO: Improve success messsage.
				})
				.error(function (data) {
					alert(data.message); // TODO: Improve error exibition.
				})
				.complete(function () {
					$('#saveppmInput').focus();
				});
		}

		function calculateOrigin()
		{
			var o = $canvas[0];
			origin.x = origin.y = 0;
			while (o)
			{
				origin.x += o.offsetLeft;
				origin.y += o.offsetTop;
				o = o.offsetParent;
			}
		}

		$(window).on('resize', calculateOrigin);

		setTimeout(function (){
			calculateOrigin();
		}, 300);

		function globalToClient(x, y)
		{
			return {
				x: (x - origin.x) + document.body.scrollLeft | 0,
				y: (y - origin.y) + document.body.scrollTop | 0
			};
		}

		function documentMouseMoveHandler(e)
		{
			var p = globalToClient(e.clientX, e.clientY);

			var imgd = ctx.getImageData(p.x, p.y, 1, 1);
			var pix = imgd.data;
			addPixelToColourDefinition(p);
		}

		var docPoints = [];

		function addPixelToColourDefinition(pix)
		{
			docPoints.push(pix);
		}

		function addPixelFlushPoints(points)
		{
			$http.post('/command/addpixelcolourdefinition', {index: colourCtrl.indexColour(), points:points})
				.success(function (data)
				{
					// TODO: Update colour definition on the screen.
					colourCtrl.setSelectedColour(data.colour);
				})
				.error(function (data)
				{
					alert(data.message); // TODO: Pretiffy error.
				});
		}

		function documentMouseUpHandler(e)
		{
			if (docPoints.length > 0)
			{
				addPixelFlushPoints(docPoints);
				docPoints = [];
			}
			$(document)
				.off('mouseup', documentMouseUpHandler);
			$canvas
				.off('mousemove', documentMouseMoveHandler);
		}

		function cancelCalibrationPointsHandler(e)
		{
			if ((!(e.ctrlKey || e.shiftKey || e.altKey)) && (e.keyCode == 27))
			{
				me.trapezoid.splice(0, me.trapezoid.length);
				var tmp = { x: 0, y: 0};
				for (var i = 0; i < 4; i++)
					me.trapezoid.push(tmp);
				sendTrapezoid();
				me.trapezoid = [];
				me.trapezoidSent = false;
				$(document).off('keydown', cancelCalibrationPointsHandler);
			}
		}

		function sendTrapezoid()
		{
			$http.post('/command/calibrationtrapezoid', { trapezoid: me.trapezoid })
				.success(function (data)
				{
					// Do nothing.
					// TODO: Start process for listening the calibration result.
				})
				.error(function (data)
				{
					alert(data.message); // TODO: Pretiffy error.
				});
		}

		$canvas
			.on('mousedown', function () {
				if (me.processingMode == "showcolours")
				{
					$(document)
						.on('mouseup', documentMouseUpHandler);
					$canvas
						.on('mousemove', documentMouseMoveHandler);
				}
			})
			.on('click', function (e)
			{
				if (me.processingMode == "calibrationprocessing")
				{
					var p = globalToClient(e.clientX, e.clientY);
					$(document)
						.off('keydown', cancelCalibrationPointsHandler)
						.on('keydown', cancelCalibrationPointsHandler);

					if (me.trapezoid.length < 4)
						me.trapezoid.push(p);

					if ((me.trapezoid.length == 4) && (!me.trapezoidSent))
					{
						sendTrapezoid();
						me.trapezoidSent = true;
					}
					e.preventDefault();
					e.stopPropagation();
				}
			});

		var
			date = (new Date()).getTime(),
			count = 0,
			imgSetted;

		function preventCache()
		{
			return date + '_' + count++;
		}

		function imgLoadHandler()
		{
			if (!imgSetted)
			{
				$canvas[0].width = img.width;
				$canvas[0].height = img.height;
				$canvas.width(img.width).height(img.height);
				$videoContainer.height(img.height);
			}
			ctx.clearRect(0, 0, img.width, img.height);
			ctx.drawImage(img, 0, 0, img.width, img.height);
			ctx.beginPath();
			ctx.strokeStyle = '#0000ff';
			for (var i = 0; i < me.trapezoid.length; i++)
			{
				ctx.moveTo(me.trapezoid[i].x, me.trapezoid[i].y);
				ctx.lineTo(me.trapezoid[(i+1) % me.trapezoid.length].x, me.trapezoid[(i+1) % me.trapezoid.length].y, 100);
			}
			ctx.stroke();

			if (!me.isPaused())
				me.load();
		}

		var img = new Image();
		$(img).on('load', $.proxy(imgLoadHandler, this));
		this.load = function ()
		{
			img.src = '/video/snapshot.jpg?' + preventCache();
		};

		this.isPaused = function ()
		{
			return this.paused;
		};

		this.togglePause = function ()
		{
			if (this.isPaused())
				this.resume();
			else
				this.pause();
		};

		this.pause = function ()
		{
			this.paused = true;
		};

		this.resume = function ()
		{
			this.paused = false;
			this.load();
		};

		this.load();
	}]);

	app.controller('TabCtrl', ['$scope', function (){
		this.tab = 1;

		this.setTab = function (value)
		{
			this.tab = value;
		};

		this.isSelected = function (value)
		{
			return this.tab == value;
		};
	}]);

	app.controller('MarkersCtrl', ['$scope', '$http', function ($scope, $http) {
		this.storeMarker = function ()
		{
			var me = this;
			$http.post('/command/storemarker', { })
				.success(function (data)
				{
					me.marker = data.marker;
				})
				.error(function (data)
				{
					alert(data.message || 'Could not store the marker.'); // TODO: Prettify error
				});
		};

		this.removeMarker = function ()
		{
			var me;
			$http.post('/command/removemarker', {})
				.success(function (data)
				{
					if (data.marker)
						me.marker = data.marker;
					else
						alert('There is no marker left.');
				})
				.error(function (data)
				{
					alert(data.message || 'Could not remove the marker.'); // TODO: Prettify error
				});
		};

		this.train = function ()
		{
			var me = this;
			if ((!this.name) || (this.name == ''))
			{
				alert('Name cannot me blank.'); // Improve error messages
				return;
			}
			$http.post('/command/trainmarker', { name: this.name })
				.success(function (data)
				{
					me.marker = data.marker.marker;
					me.name = '';
					alert('Trained under name ' + data.marker.name);
				})
				.error(function (data)
				{
					alert(data.message || 'Could not train.'); // TODO: Prettify error
				});
		};
	}]);

	app.controller('ColoursCtrl', ['$scope', '$http', '$element', function ($scope, $http, $element){
		var me = colourCtrl = this;
		this.colours = (globalConfiguration) ? globalConfiguration.colours : [];
		this.colour = null;

		$element.on('change', function () {
			me.colour._changed = false;
			if (me.colour)
			{
				if (me.colour.name != me.colour._original.name)
				{
						me.colour._changed = true;
						$scope.$apply();
						return;
				}
				for (var i in me.colour.min)
				{
					if (me.colour.min[i] != me.colour._original.min[i])
					{
						me.colour._changed = true;
						$scope.$apply();
						return;
					}
				}
				for (var i in me.colour.max)
				{
					if (me.colour.max[i] != me.colour._original.max[i])
					{
						me.colour._changed = true;
						$scope.$apply();
						return;
					}
				}
			}
			$scope.$apply();
		});

		this.colourIndex = function (colour)
		{
			for (var i = 0; i < this.colours.length; i++)
			{
				if (this.colours[i] == colour)
					return i;
			}
			return -1;
		};

		this.setColours = function (value)
		{
			this.colours = value || [];
			if (this.colours.length)
			{
				this.setColour(value[0]);
				this.colours.forEach(function (c)
				{
					c._name = c.name;
				});
			}
			$scope.$apply();
		};

		this.setSelectedColour = function (colour)
		{
			var i = this.indexColour(this.colour);
			if ((i > -1) && (this.colours[i] == this.colour))
			{
				this.colours[i] = this.colour = colour;
				$scope.$apply();
			}
		}

		this.indexColour = function (colour)
		{
			if (!colour)
				colour = this.colour;

			for (var i = 0; i < this.colours.length; i++)
			{
				if (colour == this.colours[i])
					return i;
			}
			return -1;
		}

		this.setColour = function (colour)
		{
			this.colour = colour;
			this.selectColour();
			if (colour && (!colour._original))
				colour._original = $.extend(true, {}, colour);
		};

		this.selectColour = function (colour)
		{
			colour = colour || this.colour;
			$http.post('/command/selectcolour', { index: this.indexColour(colour) })
				.error(function (data)
				{
					alert(data.message || 'Could not select the colour.'); // TODO: Prettify error
				});
		};

		this.isColour = function (colour)
		{
			return this.colour == colour;
		};

		this.reset = function (colour)
		{
			if (confirm('The current color configuration will be lost. Are you sure?'))
			{
				colour._changed = true;
				this._reset(colour);
				this.save(colour);
			}
		};

		this._reset = function (colour)
		{
			colour.min.red = 255;
			colour.min.green = 255;
			colour.min.blue = 255;

			colour.min.red_green = 255;
			colour.min.red_blue = 255;
			colour.min.green_blue = 255;

			colour.max.red = 0;
			colour.max.green = 0;
			colour.max.blue = 0;

			colour.max.red_green = -255;
			colour.max.red_blue = -255;
			colour.max.green_blue = -255;
		};

		this.remove = function (colour)
		{
			if (confirm('The current color configuration will be lost. Are you sure?'))
			{
				var me = this;

				var idx = me.colours.indexOf(colour);
				if (idx > -1)
				{
					$http.post('/command/removecolour', { index: me.indexColour(colour) })
						.success(function (data) {
							me.colours.splice(idx, 1);
							if (me.colours.length)
							{
								if (me.colours.length > idx)
									me.setColour(me.colours[idx]);
								else
									me.setColour(me.colours[idx-1]);
							}
						})
						.error(function (data){
							alert(data.message); // TODO: Improve error layout
						});
				}
			}
		};

		this._pickNewName = function ()
		{
			var
				name = 'New colour',
				count = 1,
				nameOk;
			while (!nameOk)
			{
				nameOk = true;
				for (var i = 0; i < this.colours.length; i++)
				{
					if (this.colours[i].name == name)
					{
						nameOk = false;
						name = 'New colour ' + (++count);
						break;
					}
				}
			}
			return name;
		};

		this.newColour = function ()
		{
			var
				tmp = this._pickNewName(),
				colour = { name: tmp, _name: tmp, min: {}, max: {}};
			this._reset(colour);
			colour.index = this.colours.push(colour) - 1;
			this.save(colour, function ()
			{
				me.setColour(colour);
			});
		};

		this.save = function (colour, fnc)
		{
			if (!colour)
				colour = this.colour;
			var me = this;

			colour.index = this.indexColour(colour);

			for (var i in colour.min)
			{
				colour.min[i] = parseInt(colour.min[i]);
				colour.max[i] = parseInt(colour.max[i]);
			}

			$http.post('/command/savecolour', colour)
				.success(function (data){
					colour._changed = false;
					colour._name = colour.name;
					colour.index = me.indexColour(colour);
					colour._original = $.extend(true, {}, colour);
					if (fnc)
						fnc(colour);
				})
				.error(function (data) {
					alert(data.message || "Unknown error"); // TODO: Improve error layout
				});
		}
	}]);

	app.controller('CalibrationCameraProcessingCtrl', ['$scope', '$http', '$element', function ($scope, $http, $element){
		calibrationCameraProcessingCtrl = this;
		var me = this;

		$(document).on('mouseenter', '.brightnessProgress', function (e) {
			var
				$el = $(this);
			if (!$el.data('draggerInitialized'))
			{
				var
					$cursor = $el.find('.progress-bar-cursor'),
					t = false;

				$cursor
					.draggable({
						containment: 'parent',
						axis: 'x'
					})
					.on('dragstart', function () {
						$(document).on('mousemove', documentMouseMoveHandler);
					})
					.on('dragstop', function () {
						$(document).off('mousemove', documentMouseMoveHandler);
						clearTimeout(t);
						t = false;
						me.save(me.calibration);
					});

				function documentMouseMoveHandler()
				{
					var p = $cursor[0].offsetLeft - $cursor[0].offsetWidth/2 - 4;
					me.calibration.params.thresholds.brightness = Math.round(p*255 / $el[0].offsetWidth);
					$scope.$apply();
					if (!t)
					{
						t = setTimeout(function () {
							me.save(me.calibration);
							t = false;
						}, 100);
					}
				}

				$el.closest('.form-group').find('.ccpCtrlSettingsThresholdsBrightness').on('blur', function () {
					var v = parseInt(this.value);
					$cursor.css('left', 11 + ($cursor[0].offsetWidth/2) + 4 + (v/255)*$el[0].offsetWidth);
				});
			}
		});

		this.setCameraCalibrations = function (cameraCalibrations)
		{
			this.cameraCalibrations = cameraCalibrations || [];
			if ((!this.calibration) && (this.cameraCalibrations.length > 0))
			{
				this.calibration = this.cameraCalibrations[0];
			}
			$scope.$apply();
		}

		this.setCalibration = function (calibration)
		{
			this.calibration = calibration;
			setTimeout(function () {
				$('.ccpCtrlSettingsThresholdsBrightness').blur();
			}, 100);
			$http.post('/command/setcurrentcameracalibration', { index: this.calibrationIndex() })
				.error(function (data) {
					alert(data.message); // TODO: Improve error layout
				});
		};

		this._pickNewName = function ()
		{
			var
				name = 'New calibration',
				count = 1,
				nameOk;
			while (!nameOk)
			{
				nameOk = true;
				for (var i = 0; i < this.cameraCalibrations.length; i++)
				{
					if (this.cameraCalibrations[i].name == name)
					{
						nameOk = false;
						name = 'New calibration ' + (++count);
						break;
					}
				}
			}
			return name;
		};

		this.newCalibration = function ()
		{
			var
				tmp = this._pickNewName(),
				calibration = {
					_changed: true,
					name: tmp,
					params: {
						rows: 0,
						columns: 0,
						origin: {
							x: 0,
							y: 0
						},
						distance: {
							x: 0,
							y: 0
						},
						thresholds: {
							clustering: 0,
							brightness: 0
						}
					}
				};
			;
			this.cameraCalibrations.push(calibration);
			this.save(calibration, function ()
			{
				me.setCalibration(calibration)
			});
		};

		this.isCalibration = function (calibration)
		{
			return (this.calibration == calibration);
		};

		this.calibrationIndex = function ()
		{
			for (var i = 0; i < this.cameraCalibrations.length; i++)
			{
				if (this.isCalibration(this.cameraCalibrations[i]))
					return i;
			}
			return -1;
		}

		this.save = function (calibration, handler)
		{
			var me = this;

			var s = {
				name: calibration.name,
				params: {
					rows: parseInt(calibration.params.rows),
					columns: parseInt(calibration.params.columns),
					origin: {
						x: parseFloat(calibration.params.origin.x),
						y: parseFloat(calibration.params.origin.y)
					},
					distance: {
						x: parseFloat(calibration.params.distance.x),
						y: parseFloat(calibration.params.distance.y)
					},
					thresholds: {
						clustering: parseFloat(calibration.params.thresholds.clustering),
						brightness: parseFloat(calibration.params.thresholds.brightness)
					}
				}
			};

			for (var i = 0; i < this.cameraCalibrations.length; i++)
			{
				if (this.isCalibration(this.cameraCalibrations[i]))
				{
					s['index'] = i;
					break;
				}
			}

			$http.post('/command/cameracalibrationsettings', s)
				.success(function (data){
					me.calibration._changed = false;
					if (handler)
						handler();
				})
				.error(function (data) {
					alert(data.message); // TODO: Improve error layout
				});
		};

		this.remove = function (calibration)
		{
			var me = this;
			for (var i = 0; i < this.cameraCalibrations.length; i++)
			{
				if (this.isCalibration(this.cameraCalibrations[i]))
				{
					$http.post('/command/cameracalibrationremove', { index: i })
						.success(function (data){
							if (data.success)
							{
								me.cameraCalibrations.splice(i, 1);
							}
						})
						.error(function (data) {
							alert(data.message); // TODO: Improve error layout
						});
					return
				}
			}
			alert('Cannot find the current "calibration".');
		};
	}]);

	app.controller('CameraParametersCtrl', ['$scope', '$http', function ($scope, $http){
		var me = cameraParametersCtrl = this;

		this.setParams = function (params)
		{
			this.params = params;
			$scope.$apply();
		}

		this.save = function ()
		{
			for (var i in me.params)
			{
				if (i != 'flip')
					me.params[i] = parseInt(me.params[i]);
			}
			$http.post('/command/savecameraparameters', me.params)
				.success(function (data) {
					delete data.success;
					for (var i in data)
						me.params[i] = data[i];
					$scope.$apply();
					alert('Camera configuration applied.'); // TODO: Improve success messsage.
				})
				.error(function (data) {
					alert(data.message); // TODO: Improve error exibition.
				});
		}
	}]);

	var keys = {
		'left': 37,
		'up': 38,
		'right': 39,
		'down': 40,
		'space': 32,
		'a': 65,
		's': 83,
		'w': 87,
		'd': 68,
		'q': 81,
		'z': 90,
		'u': 85,
		'j': 74,
		'm': 77,
		inverse: { },
		state: { }
	};

	for (var key in keys)
	{
		if ((key != 'inverse') && (key != 'state'))
			keys.inverse[keys[key]] = key;
	}

	var keysRev = {};

	for (var i in keys)
		keysRev[keys[i]] = i;

	var sendKeyboardData = false;
	$('.keyboard-view a').on('click', function () {
		sendKeyboardData = !sendKeyboardData;

		if (sendKeyboardData)
			$('.keyboard-view').addClass('active');
		else
			$('.keyboard-view').removeClass('active');
	});

	var keyboardState = {
	};

	$(window)
		.on('keydown', function (e) {
			if (sendKeyboardData)
			{
				keyboardState['control'] = e.ctrlKey ? 1.0 : 0;
				keyboardState['shift'] = e.shiftKey ? 1.0 : 0;
				keyboardState['alt'] = e.altKey ? 1.0 : 0;

				keys.state[e.keyCode] = true;

				if (keys.inverse[e.keyCode])
					e.preventDefault();
			}
		})
		.on('keyup', function (e) {
			if (sendKeyboardData)
			{
				keyboardState['control'] = e.ctrlKey ? 1.0 : 0;
				keyboardState['shift'] = e.shiftKey ? 1.0 : 0;
				keyboardState['alt'] = e.altKey ? 1.0 : 0;

				keys.state[e.keyCode] = false;

				if (keys.inverse[e.keyCode])
					e.preventDefault();
			}
		});

	$(document)
		.on('mousedown touchstart', '.btn-ui-control', function (e) {
			var $me = $(e.currentTarget);
			sendKeyboardData = true;
			keys.state[keys[$me.data('key')]] = true;
			realSendKeyboardData();
			$(window).one('mouseup touchend', function () {
				sendKeyboardData = false;
				keys.state[keys[$me.data('key')]] = false;
				realSendKeyboardData();
				e.preventDefault();
			});
			e.preventDefault();
		});

	var tabletSendData = false;
	$('.tablet-view a').on('click', function () {
		tabletSendData = !tabletSendData;

		if (tabletSendData)
		{

			$('.tablet-view').addClass('active');
			lJoystick.show();
			rJoystick.show();
		}
		else
		{keyboardState[keysRev[$me.data('key')]] = true;
			$('.tablet-view').removeClass('active');
			lJoystick.hide();
			rJoystick.hide();
		}
	});

	var lJoystick = new TouchJoystick({
		container: document.body,
		size: 0.1,
		x: 0.1,
		y: 0.8
	}).hide();

	var rJoystick = new TouchJoystick({
		container: document.body,
		size: 0.1,
		x: 0.9,
		y: 0.8
	}).hide();

	function realSendKeyboardData()
	{
		console.log(keyboardState);
		services.keyboard(keyboardState)
			.success($.proxy(gamepadSupport.__handleGamepadSuccess, gamepadSupport))
			.error(function (data) {
				console.log('keyboard::error');
			});
	}

	function sendKeyboardDataInterval()
	{
		if (sendKeyboardData)
		{
			if (!keyboardState['arrowLetters'])
				keyboardState['arrowLetters'] = {};
			if (!keyboardState['arrows'])
				keyboardState['arrows'] = {};

			if (keys.state[keys.a] && keys.state[keys.d])
				keyboardState['arrowLetters']['x'] = 0
			else if (keys.state[keys.a])
				keyboardState['arrowLetters']['x'] = -1;
			else if (keys.state[keys.d])
				keyboardState['arrowLetters']['x'] = 1;
			else
				keyboardState['arrowLetters']['x'] = 0;

			if (keys.state[keys.w] && keys.state[keys.s])
				keyboardState['arrowLetters']['y'] = 0
			else if (keys.state[keys.w])
				keyboardState['arrowLetters']['y'] = 1;
			else if (keys.state[keys.s])
				keyboardState['arrowLetters']['y'] = -1;
			else
				keyboardState['arrowLetters']['y'] = 0;

			if (keys.state[keys.left] && keys.state[keys.right])
				keyboardState['arrows']['x'] = 0
			else if (keys.state[keys.left])
				keyboardState['arrows']['x'] = -1;
			else if (keys.state[keys.right])
				keyboardState['arrows']['x'] = 1;
			else
				keyboardState['arrows']['x'] = 0;

			if (keys.state[keys.up] && keys.state[keys.down])
				keyboardState['arrows']['y'] = 0
			else if (keys.state[keys.up])
				keyboardState['arrows']['y'] = 1;
			else if (keys.state[keys.down])
				keyboardState['arrows']['y'] = -1;
			else
				keyboardState['arrows']['y'] = 0;

			if (keys.state[keys.up] && keys.state[keys.down])
				keyboardState['arrows']['y'] = 0
			else if (keys.state[keys.up])
				keyboardState['arrows']['y'] = 1;
			else if (keys.state[keys.down])
				keyboardState['arrows']['y'] = -1;
			else
				keyboardState['arrows']['y'] = 0;

			realSendKeyboardData();
		}
		else if (tabletSendData)
		{
			services.touch({
				'left':
				{
					x: lJoystick.axes.x,
					y: lJoystick.axes.y
				},
				'right':
				{
					x: rJoystick.axes.x,
					y: rJoystick.axes.y
				}
			})
			.success($.proxy(gamepadSupport.__handleGamepadSuccess, gamepadSupport));
		}
	}

	setInterval(sendKeyboardDataInterval, 200);

	loadMessages();

	var calibrationTimer;

	function hideCalibrationData()
	{
		$('.grid-icon').css('opacity', 0.0);
		$('.grid-error').html('');
	}

	var messageHandlers = {
		markers: function (data)
		{
			$('.markers-set .marker').removeClass('active');
			for (var i in data)
			{
				$('.markers-set .marker-' + i.split('-')[0] + ' .marker-error').html(data[i]);
				if (data[i] == 0)
					$('.markers-set .marker-' + i.split('-')[0]).addClass('active');
			}
		},
		calibration: function (data)
		{
			clearTimeout(calibrationTimer);
			$('.grid-icon')
				.removeClass('glyphicon-search')
				.removeClass('glyphicon-ok')
				.removeClass('glyphicon-remove')
				.css('opacity', 1);
			if (data.found)
			{
				if (data.calibrated)
				{
					$('.grid-icon').addClass('glyphicon-ok');
					$('.grid-error', data.error);
				}
				else
					$('.grid-icon')
						.addClass('glyphicon-remove');
			}
			else
				$('.grid-icon')
					.addClass('glyphicon-search');
			calibrationTimer = setTimeout(calibrationTimer, 1000);
		}
	};

	function handleMessageSuccess(data)
	{
		if (data.success && data.messages)
		{
			for (var i = 0 ; i < data.messages.length; i++)
			{
				if (messageHandlers[data.messages[i].command])
					messageHandlers[data.messages[i].command](data.messages[i].params);
			}
		}
	}

	function loadMessages()
	{
		services.messages()
			.success(function (data) {
				if (typeof data != "object")
					data = JSON.parse(data);
				handleMessageSuccess(data);
			})
			.error(function (data) {
				console.log('Error loading messages.', data);
			})
			.complete(function () {
				setTimeout(loadMessages, 300);
			});
	}
})(jQuery);
