/**
 * Initial implementaiton.
 *
 * @author  Qail Mukhi <mqai101@gmail.com>
 *
 * BUGFIX on the return of the methods.
 * Documentation of the return documentation.
 *
 * @author  J. Santos <jamillo@gmail.com>
 * @date    October 9, 2015
 */

/**
 * The 'Constructor' for the servo class
 * @param options
 * 		options.name: Name of the servo
 */
var Servo = function (options)
{
	if ((!options) || typeof options.name == 'undefined')
		throw new Error("UndefinedNameError");
	this.name = options.name;
	if (options.angle)
		this._angle = parseFloat(options.angle);
};

/**
 * Stub for changing the position of the servo.
 *
 * @param angle Goal angle
 * @param speed [Optional] Speed of movement
 * @param gainP [Optional] Proportional gain
 * @param gainI [Optional] Integral gain
 * @param gainD [Optional] Derivative gain
 * @return jqXHR Return the jQuery.ajax result for handling the result.
 */
Servo.prototype.position = function (angle, speed, gainP, gainI, gainD)
{
	if (arguments.length)
	{
		// When no speed entered, setting the default speed
		if (typeof speed == 'undefined' || speed == null)
			speed = 100;

		// When ready to send command
		return services.robot.position(this.name, angle, speed, gainP, gainI, gainD);
	}
	else
	{
		return this._angle;
	}
};

/**
 * Turn the servo ON or OFF.
 *
 * @param mode True for ON and false for OFF.
 * @return jqXHR Return the jQuery.ajax result for handling the result.
 */
Servo.prototype.mode = function (mode)
{
	if (arguments.length)
	{
		this._mode = mode;
		return services.robot.mode(this.name, mode);
	}
	else
		return this._mode;
};

/**
 * Releases the torque of the servo.
 * @return jqXHR Return the jQuery.ajax result for handling the result.
 */
Servo.prototype.modeOff = function ()
{
	return services.robot.mode(this.name, false);
}
/**
 * Enables the torque of the servo.
 * @return jqXHR Return the jQuery.ajax result for handling the result.
 */
Servo.prototype.modeOn = function ()
{
	return services.robot.mode(this.name, true);
}