/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 30, 2015
 **/

var robots = {
	models:
	{
		autman:
		{
			name: "Autman",
			defaultName: "Arash",
			actuators:
			{
				NeckLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.5,
						y: 1
					},
					link: {
						n: "NeckLateral"
					}
				},
				NeckTransversal:
				{
					orientation: "n",
					position:
					{
						x: 0.5,
						y: 0.9
					},
					link: {
						w: "RightShoulderLateral",
						e: "LeftShoulderLateral"
					}
				},
				RightShoulderLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.1,
						y: 0.9
					},
					link: {
						w: "RightShoulderFrontal",
					}
				},
				RightShoulderFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.0,
						y: 0.9
					},
					link: {
						s: "RightElbowLateral",
					}
				},
				RightElbowLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.0,
						y: 0.6
					}
				},
				LeftShoulderLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.9,
						y: 0.9
					},
					link: {
						e: "LeftShoulderFrontal"
					}
				},
				LeftShoulderFrontal:
				{
					orientation: "u",
					position:
					{
						x: 1.0,
						y: 0.9
					},
					link: {
						s: "LeftElbowLateral",
					}
				},
				LeftElbowLateral:
				{
					orientation: "e",
					position:
					{
						x: 1.0,
						y: 0.6
					}
				},
				RightHipTransversal:
				{
					orientation: "s",
					position:
					{
						x: 0.4,
						y: 0.5
					}
				},
				RightHipFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.4,
						y: 0.4
					},
					link:
					{
						n: "RightHipTransversal"
					}
				},
				RightHipLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.3,
						y: 0.4
					},
					link:
					{
						e: "RightHipFrontal"
					}
				},
				RightKneeLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.3,
						y: 0.2
					},
					link:
					{
						n: "RightHipLateral"
					}
				},
				RightAnkleLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.3,
						y: 0.1
					},
					link:
					{
						n: "RightKneeLateral"
					}
				},
				RightAnkleFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.3,
						y: 0.0
					},
					link:
					{
						n: "RightAnkleLateral"
					}
				},
				LeftHipTransversal:
				{
					orientation: "s",
					position:
					{
						x: 0.6,
						y: 0.5
					}
				},
				LeftHipFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.6,
						y: 0.4
					},
					link:
					{
						n: "LeftHipTransversal"
					}
				},
				LeftHipLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.7,
						y: 0.4
					},
					link:
					{
						w: "LeftHipFrontal"
					}
				},
				LeftKneeLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.7,
						y: 0.2
					},
					link:
					{
						n: "LeftHipLateral"
					}
				},
				LeftAnkleLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.7,
						y: 0.1
					},
					link:
					{
						n: "LeftKneeLateral"
					}
				},
				LeftAnkleFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.7,
						y: 0.0
					},
					link:
					{
						n: "LeftAnkleLateral"
					}
				}
			}
		},
		silverback:
		{
			name: "Silverback",
			defaultName: "Arash",
			actuators:
			{
				RightShoulderLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.1,
						y: 0.9
					},
					link: {
						w: "RightShoulderFrontal",
					}
				},
				RightShoulderFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.0,
						y: 0.9
					},
					link: {
						s: "RightElbowLateral",
					}
				},
				RightElbowLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.0,
						y: 0.6
					}
				},
				LeftShoulderLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.9,
						y: 0.9
					},
					link: {
						e: "LeftShoulderFrontal"
					}
				},
				LeftShoulderFrontal:
				{
					orientation: "u",
					position:
					{
						x: 1.0,
						y: 0.9
					},
					link: {
						s: "LeftElbowLateral",
					}
				},
				LeftElbowLateral:
				{
					orientation: "e",
					position:
					{
						x: 1.0,
						y: 0.6
					}
				},
				RightHipFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.4,
						y: 0.4
					},
					link:
					{
						n: "RightHipTransversal"
					}
				},
				RightKneeLateral:
				{
					orientation: "w",
					position:
					{
						x: 0.3,
						y: 0.2
					},
					link:
					{
						n: "RightHipLateral"
					}
				},
				LeftHipFrontal:
				{
					orientation: "u",
					position:
					{
						x: 0.6,
						y: 0.4
					},
					link:
					{
						n: "LeftHipTransversal"
					}
				},
				LeftKneeLateral:
				{
					orientation: "e",
					position:
					{
						x: 0.7,
						y: 0.2
					},
					link:
					{
						n: "LeftHipLateral"
					}
				}
			}
		}
	}
};