/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 30, 2015
 **/

if (typeof KeyEvent == "undefined")
{
    var KeyEvent = {
        DOM_VK_CANCEL:3,DOM_VK_HELP:6,DOM_VK_BACK_SPACE:8,DOM_VK_TAB:9,DOM_VK_CLEAR:12,DOM_VK_RETURN:13,DOM_VK_ENTER:14,DOM_VK_SHIFT:16,DOM_VK_CONTROL:17,DOM_VK_ALT:18,DOM_VK_PAUSE:19,DOM_VK_CAPS_LOCK:20,DOM_VK_ESCAPE:27,DOM_VK_SPACE:32,DOM_VK_PAGE_UP:33,DOM_VK_PAGE_DOWN:34,DOM_VK_END:35,DOM_VK_HOME:36,DOM_VK_LEFT:37,DOM_VK_UP:38,DOM_VK_RIGHT:39,DOM_VK_DOWN:40,DOM_VK_PRINTSCREEN:44,DOM_VK_INSERT:45,DOM_VK_DELETE:46,DOM_VK_0:48,DOM_VK_1:49,DOM_VK_2:50,DOM_VK_3:51,DOM_VK_4:52,DOM_VK_5:53,DOM_VK_6:54,DOM_VK_7:55,DOM_VK_8:56,DOM_VK_9:57,DOM_VK_SEMICOLON:59,DOM_VK_EQUALS:61,DOM_VK_A:65,DOM_VK_B:66,DOM_VK_C:67,DOM_VK_D:68,DOM_VK_E:69,DOM_VK_F:70,DOM_VK_G:71,DOM_VK_H:72,DOM_VK_I:73,DOM_VK_J:74,DOM_VK_K:75,DOM_VK_L:76,DOM_VK_M:77,DOM_VK_N:78,DOM_VK_O:79,DOM_VK_P:80,DOM_VK_Q:81,DOM_VK_R:82,DOM_VK_S:83,DOM_VK_T:84,DOM_VK_U:85,DOM_VK_V:86,DOM_VK_W:87,DOM_VK_X:88,DOM_VK_Y:89,DOM_VK_Z:90,DOM_VK_CONTEXT_MENU:93,DOM_VK_NUMPAD0:96,DOM_VK_NUMPAD1:97,DOM_VK_NUMPAD2:98,DOM_VK_NUMPAD3:99,DOM_VK_NUMPAD4:100,DOM_VK_NUMPAD5:101,DOM_VK_NUMPAD6:102,DOM_VK_NUMPAD7:103,DOM_VK_NUMPAD8:104,DOM_VK_NUMPAD9:105,DOM_VK_MULTIPLY:106,DOM_VK_ADD:107,DOM_VK_SEPARATOR:108,DOM_VK_SUBTRACT:109,DOM_VK_DECIMAL:110,DOM_VK_DIVIDE:111,DOM_VK_F1:112,DOM_VK_F2:113,DOM_VK_F3:114,DOM_VK_F4:115,DOM_VK_F5:116,DOM_VK_F6:117,DOM_VK_F7:118,DOM_VK_F8:119,DOM_VK_F9:120,DOM_VK_F10:121,DOM_VK_F11:122,DOM_VK_F12:123,DOM_VK_F13:124,DOM_VK_F14:125,DOM_VK_F15:126,DOM_VK_F16:127,DOM_VK_F17:128,DOM_VK_F18:129,DOM_VK_F19:130,DOM_VK_F20:131,DOM_VK_F21:132,DOM_VK_F22:133,DOM_VK_F23:134,DOM_VK_F24:135,DOM_VK_NUM_LOCK:144,DOM_VK_SCROLL_LOCK:145,DOM_VK_COMMA:188,DOM_VK_PERIOD:190,DOM_VK_SLASH:191,DOM_VK_BACK_QUOTE:192,DOM_VK_OPEN_BRACKET:219,DOM_VK_BACK_SLASH:220,DOM_VK_CLOSE_BRACKET:221,DOM_VK_QUOTE:222,DOM_VK_META: 224
    };
}

function R2D(a)
{
    return (180.0 * (a) / Math.PI);
}

function D2R(a)
{
    return (Math.PI * (a) / 180.0);
}

(function ($) {
    var app = angular.module('MotionEditor', [function (){
        var initInjector = angular.injector(["ng"]);
    }]);

    function uiAngle(a)
    {
        return R2D(a).toFixed(2);
    }

    function rsAngle(a)
    {
        return D2R(a);
    }

    function compileToTrajectory(doc)
    {
        var
            result = {},
            sequence,
            position,
            totalTime,
            currentTime,
            resultSequence,
            keyframe,
            entry;
        for (var s in doc.sequences)
        {
            sequence = doc.sequences[s];
            resultSequence = result[sequence.name] = {};
            totalTime = currentTime = 0;
            for (var p in sequence.positions)
            {
                position = sequence.positions[p];
                totalTime += parseFloat(position.time);
            }


            for (var p in sequence.positions)
            {
                position = sequence.positions[p];
                currentTime += parseFloat(position.time);
                keyframe = false;
                for (var i in doc.keyframes)
                {
                    if (doc.keyframes[i].name == position.name)
                    {
                        keyframe = doc.keyframes[i];
                        break;
                    }
                }

                if (keyframe)
                {
                    for (var a in keyframe.actuators)
                    {
                        entry = {
                            time: currentTime/totalTime,
                            angle: rsAngle(parseFloat(keyframe.actuators[a].angle))
                        };
                        if (resultSequence[a])
                            resultSequence[a].push(entry);
                        else
                        {
                            resultSequence[a] = [entry];
                        }
                    }
                }
                else
                {
                    throw new Error("Position '" + position.name + "' not defined.");
                }
            }

        }
        return result;
    }

    app.directive('sequence', function()
    {
        var from = false;
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.find('.sequence-containers').droppable({
                        activeClass: "ui-state-default",
                        hoverClass: "ui-state-hover",
                        accept: ":not(.ui-sortable-helper)",
                        drop: function(e, ui)
                        {
                            scope.sequence.positions.push({
                                name: ui.draggable.find('.panel-title').text(),
                                time: 1
                            });
                            $(this).find( ".placeholder" ).remove();
                            scope.$apply();
                        }
                    })
                    .sortable({
                        items: ".position:not(.placeholder)",
                        sort: function(e, ui)
                        {
                            $( this ).removeClass( "ui-state-default" );
                        },
                        start: function(e, ui)
                        {
                            from = ui.item.parent().find('.position').index(ui.item);
                        },
                        stop: function(e, ui)
                        {
                            var index = ui.item.parent().find('.position').index(ui.item);
                            if (from != index)
                            {
                                var o = scope.sequence.positions[from];
                                scope.sequence.positions.splice(from, 1);
                                scope.sequence.positions.splice(index, 0, o);
                                scope.$apply();
                            }
                        }
                    });
            }
        };
    });

    app.directive('keyframeDraggable', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.draggable({
                    appendTo: "body",
                    helper: "clone"
                });
            }
        };
    });

    var lastMotionFileName = 'motion';

    app.directive('saveDocument', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.click(function (e) {
                    var d = {
                        keyframes: [],
                        sequences: [],
                        groups: []
                    };

                    for (var i in scope.robotCtrl.document.keyframes)
                    {
                        d.keyframes.push({
                            name: scope.robotCtrl.document.keyframes[i].name,
                            actuators: scope.robotCtrl.document.keyframes[i].actuators,
                        });
                    }

                    var tmp;
                    for (var i in scope.robotCtrl.document.sequences)
                    {
                        tmp = {
                            name: scope.robotCtrl.document.sequences[i].name,
                            positions: []
                        };
                        for (var j in scope.robotCtrl.document.sequences[i].positions)
                        {
                            tmp.positions.push({
                                name: scope.robotCtrl.document.sequences[i].positions[j].name,
                                time: scope.robotCtrl.document.sequences[i].positions[j].time
                            });
                        }
                        d.sequences.push(tmp);
                    }

                    for (var i in scope.robotCtrl.document.groups)
                    {
                        tmp = {
                            name: scope.robotCtrl.document.groups[i].name,
                            actuators: scope.robotCtrl.document.groups[i].actuators
                        };
                        d.groups.push(tmp);
                    }

                    var t = prompt("Confirm the file name:", lastMotionFileName);
                    if (t)
                    {
                        lastMotionFileName = t;
                        element.attr('download', lastMotionFileName + '.json');
                        element.attr('href', 'data:application/octet-stream;base64,' + encodeURIComponent(btoa(JSON.stringify(d, null, '\t'))));
                    }
                    else
                    {
                        e.preventDefault();
                    }
                });
            }
        };
    });

    var lastTrajectoryFileName = 'trajectories';

    app.directive('compileDocument', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.click(function (e) {
                    var d = compileToTrajectory(scope.robotCtrl.document);
                    var result = ["trajectories\n{"];
                    var trajectory, actuator;
                    for (var trajectoryName in d)
                    {
                        trajectory = d[trajectoryName];
                        result.push("\t" + trajectoryName + "\n\t{");
                        for (var actuatorName in trajectory)
                        {
                            actuator = trajectory[actuatorName];
                            result.push("\t\t" + actuatorName + "\n\t\t{");
                            for (var i in actuator)
                            {
                                result.push("\t\t\t" + (actuator[i].angle.toFixed(3)) + " " + actuator[i].time.toFixed(3));
                            }
                            result.push("\t\t}");
                        }
                        result.push("\t}");
                    }
                    result.push("}");

                    var t = prompt("Confirm the file name:", lastTrajectoryFileName);
                    if (t)
                    {
                        lastTrajectoryFileName = t;
                        element.attr('download', lastTrajectoryFileName + '.info');
                        element.attr('href', 'data:application/octet-stream;base64,' + encodeURIComponent(btoa(result.join('\n'))));
                    }
                    else
                    {
                        e.preventDefault();
                    }
                });
            }
        };
    });

    app.directive('compileTrajectory', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.click(function (e) {
                    var d = compileToTrajectory(scope.robotCtrl.document);
                    var result = ["trajectories\n{"];
                    var trajectory, actuator;
                    trajectory = d[scope.sequence.name];
                    result.push("\t" + scope.sequence.name + "\n\t{");
                    for (var actuatorName in trajectory)
                    {
                        actuator = trajectory[actuatorName];
                        result.push("\t\t" + actuatorName + "\n\t\t{");
                        for (var i in actuator)
                        {
                            result.push("\t\t\t" + (actuator[i].angle.toFixed(3)) + " " + actuator[i].time.toFixed(3));
                        }
                        result.push("\t\t}");
                    }
                    result.push("\t}");
                    result.push("}");

                    var t = prompt("Confirm the file name:", scope.sequence.name);
                    if (t)
                    {
                        element.attr('download', scope.sequence.name + '.info');
                        element.attr('href', 'data:application/octet-stream;base64,' + encodeURIComponent(btoa(result.join('\n'))));
                    }
                    else
                    {
                        e.preventDefault();
                    }
                });
            }
        };
    });

    var lastSeconds = true;

    app.directive('playTrajectory', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                element.click(function (e) {
                    var d = compileToTrajectory(scope.robotCtrl.document);

                    var
                        actuatorsDone = 0,
                        actuatorsError = 0,
                        actuatorsTotal = 0;

                    function checkDone()
                    {
                        if (actuatorsError > 0)
                        {
                            if ((actuatorsError + actuatorsDone) >= actuatorsTotal)
                            {
                                alert('Cannot play sequence.');
                            }
                        }
                        else if (actuatorsDone == actuatorsTotal)
                        {
                            var seconds = prompt('Seconds', lastSeconds);
                            if (seconds)
                            {
                                services.robot.playTrajectory(scope.sequence.name, parseFloat(seconds) * 1000000, 1, 1000000);
                                lastSeconds = seconds;
                            }
                        }
                    }

                    var trajectory = d[scope.sequence.name];
                    if (trajectory)
                    {
                        for (var actuatorName in trajectory)
                        {
                            actuator = trajectory[actuatorName];
                            (function (actuatorName, sequence, frames)
                            {
                                services.robot.uploadTrajectories(actuatorName, sequence.name, frames)
                                    .error(function (data){
                                        actuatorsError++;
                                    })
                                    .done(function (data) {
                                        if (data.success)
                                            actuatorsDone++;
                                        else
                                            actuatorsError++;
                                        checkDone();
                                    });
                            })(actuatorName, scope.sequence, actuator);
                            actuatorsTotal++;
                        }
                    }
                });
            }
        };
    });

    app.directive('loadDocument', function()
    {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                function handleFileSelect(e) {
                    var
                        files = e.target.files,
                        data,
                        reader = new FileReader();

                    reader.onload = function(e) {
                        try
                        {
                            console.log(e);
                            var object = JSON.parse(e.target.result);
                            for (var i in object.keyframes)
                            {
                                delete object.keyframes[i]['$$hashKey'];
                            }
                            for (var i in object.sequences)
                            {
                                delete object.sequences[i]['$$hashKey'];
                                for (var j in object.sequences[i].positions)
                                {
                                    delete object.sequences[i].positions[j]['$$hashKey'];
                                }
                            }
                            scope.robotCtrl.document = object;
                            scope.$apply();
                        }
                        catch (e)
                        {
                            alert('Error parsing file:\n' + e);
                        }
                    };

                    var t;
                    for (var i = 0, f; f = files[i]; i++)
                    {
                        data = reader.readAsText(f);
                        t = f.name.split('.');
                        t.pop();
                        lastMotionFileName = t.join('.');
                    }
                }

                element.parent().find('input').on('change', handleFileSelect);
            }
        };
    });

    app.controller('RobotCtrl', ['$scope', '$http', '$element', function ($scope, $http, $element){
        var me = this;

        this.screen = 'start';
        this.tab = 'positions';
        this.model = 'autman';
        this.models = robots.models;
        this.currentModel = robots.models[this.model];
        this.name = 'Arash';
        // this.model = false;
        // this.currentModel = false;

        this.legs = 1;

        this.$actuatorsContainer = $('#robotView');

        var copiedActuators = false;

        this.$sortableFramesContainer = $('#framesContainer').draggable({});

        this._documentKeyDownHandler = function (e)
        {
            if (e.altKey && (!(e.ctrlKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_L))
            {
                $('#loadDocumentInput').click();
                e.stopPropagation();
                e.preventDefault();
            }
            else if (e.altKey && (!(e.ctrlKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_U))
            {
                me._updateCurrentFrameAngles();
                e.stopPropagation();
                e.preventDefault();
            }
            else if (e.ctrlKey && (!(e.altKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_A))
            {
                this._selectAllActuators();
                e.stopPropagation();
                e.preventDefault();
            }
            else if (e.altKey && (!(e.ctrlKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_E))
            {
                if (this.$actuatorsContainer.find('.ui-selected').length > 0)
                {
                    this._turnSelectedOff();
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            else if (e.altKey && (!(e.ctrlKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_S))
            {
                me.createSequence();
                me.tab = 'sequences';
                e.stopPropagation();
                e.preventDefault();
            }
            else if (e.altKey && (!(e.ctrlKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_N))
            {
                me.createFrame();
                me.tab = 'positions';
                e.stopPropagation();
                e.preventDefault();
            }
            else if ($(e.target).is('.angle'))
            {
                if (!(e.ctrlKey || e.altKey || e.shiftKey))
                {
                    if (e.keyCode == KeyEvent.DOM_VK_UP)
                    {
                        $(e.target).val(parseFloat($(e.target).val()) + 1);
                        this._current.actuators[$(e.target).data('actuator')].angle = parseFloat(e.target.value);
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else if (e.keyCode == KeyEvent.DOM_VK_DOWN)
                    {
                        $(e.target).val(parseFloat($(e.target).val()) - 1);
                        this._current.actuators[$(e.target).data('actuator')].angle = parseFloat(e.target.value);
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
                if (!(e.ctrlKey || e.altKey || e.shiftKey))
                {
                    if (e.keyCode == KeyEvent.DOM_VK_PAGE_UP)
                    {
                        $(e.target).val(parseFloat($(e.target).val()) + 5);
                        this._current.actuators[$(e.target).data('actuator')].angle = parseFloat(e.target.value);
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else if (e.keyCode == KeyEvent.DOM_VK_PAGE_DOWN)
                    {
                        $(e.target).val(parseFloat($(e.target).val()) - 5);
                        this._current.actuators[$(e.target).data('actuator')].angle = parseFloat(e.target.value);
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
            }
            else if (!$(e.target).is('input, textarea'))
            {
                if (e.keyCode == KeyEvent.DOM_VK_ESCAPE)
                {
                    this.$actuatorsContainer.find('.ui-selected').removeClass('ui-selected');
                    //
                    e.stopPropagation();
                    e.preventDefault();
                }
                else if (e.ctrlKey && !e.altKey && !e.shiftKey && (e.keyCode == KeyEvent.DOM_VK_G))
                {
                    this.createGroup();
                    e.stopPropagation();
                    e.preventDefault();
                }
                else if ((!(e.ctrlKey || e.altKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_U))
                {
                    this._updateCurrentFrameAngles();
                    e.stopPropagation();
                    e.preventDefault();
                }
                else if (e.ctrlKey && (!(e.altKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_C))
                {
                    var $selected = this.$actuatorsContainer.find('.ui-selected');
                    copiedActuators = {};
                    if (me._current)
                    {
                        $selected.each(function () {
                            copiedActuators[this.label] = {
                                angle: parseFloat(me._current.actuators[this.label].angle)
                            };
                        });
                    }
                    e.stopPropagation();
                    e.preventDefault();
                }
                else if (e.ctrlKey && (!(e.altKey || e.shiftKey)) && (e.keyCode == KeyEvent.DOM_VK_V))
                {
                    if (copiedActuators && (me._current))
                    {
                        for (var actuator in copiedActuators)
                        {
                            me._current.actuators[actuator].angle = copiedActuators[actuator].angle;
                        }
                        $scope.$apply();
                    }
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
        };

        this._selectAllActuators = function ()
        {
            this.$actuatorsContainer.find('me-sgraph').addClass('ui-selected');
        };

        this._saveThisFrame = function()
        {
            this._selectAllActuators();
            this._updateCurrentFrameAngles();
        };

        this._updateCurrentFrameAngles = function ()
        {
            if (this._current)
            {
                this.reloadPositions()
                    .done(function (data)
                    {
                        if (data.success)
                        {
                            var $selected = me.$actuatorsContainer.find('.ui-selected');
                            $selected.each(function ()
                            {
                                if (me.robot.actuators[this.label])
                                {
                                    me._current.actuators[this.label].angle = uiAngle(data.data[this.label].angle);
                                    console.log('Updating', this.label, 'angle to', me._current.actuators[this.label].angle);
                                }
                            });
                            $scope.$apply();
                        }
                    })
                    .error(function (e)
                    {
                        alert('Cannot update positions.');
                    });
            }
        };

        this._turnSelectedOff = function ()
        {
            var $selected = this.$actuatorsContainer.find('.ui-selected');
            var enabled = false;
            $selected.each(function ()
            {
                var s = this;
                services.robot.actuatorMode(this.label, enabled)
                    .done(function (data) {
                        if (data.success)
                        {
                            if (enabled)
                                $(s).removeClass('off');
                            else
                                $(s).addClass('off');
                        }
                        else
                        {
                            console.log('Error updating servos.', data);
                        }
                    });
            });
        };
        this._turnAllOff = function()
        {
            this._selectAllActuators();
            this._turnSelectedOff();
        };

        this._turnAllOn = function()
        {
            this._selectAllActuators();
            this._turnSelectedOn();
        }

        this._turnSelectedOn = function ()
        {
            var $selected = this.$actuatorsContainer.find('.ui-selected');
            var enabled = true;
            $selected.each(function ()
            {
                var s = this;
                services.robot.actuatorMode(this.label, enabled)
                    .done(function (data) {
                        if (data.success)
                        {
                            if (enabled)
                                $(s).removeClass('off');
                            else
                                $(s).addClass('off');
                        }
                        else
                        {
                            console.log('Error updating servos.', data);
                        }
                    });
            });
        };

        this.$actuatorsContainer
            .selectable({
                filter: 'me-sgraph'
            })
            .on('selectableselected', function (e, ui)
            {
                $('.actuator-' + ui.selected.label).addClass('ui-selected');
            })
            .on('selectableunselected', function (e, ui)
            {
                $('.actuator-' + ui.unselected.label).removeClass('ui-selected');
            });

        $(document)
            .on('keydown', $.proxy(this._documentKeyDownHandler, this));

        this.robot = {
            actuators: {}
        };

        this.document = {
            keyframes: [],
            sequences: [],
            groups: []
        };

        this._changeRobotModelHandle = function ()
        {
            this.name = this.models[this.model].defaultName;
        };

        this.applyAngles = function (keyframe)
        {
            this.$actuatorsContainer.find('.ui-selected').each(function ()
            {
                console.log(this.label, keyframe.actuators[this.label], me.robot.actuators);
                if (keyframe.actuators[this.label] && me.robot.actuators[this.label])
                {
                    console.log(this.label, keyframe.actuators[this.label].angle);
                    me.robot.actuators[this.label].position(rsAngle(keyframe.actuators[this.label].angle));
                }
            });
        };

        // this._changeRobotModelHandle();

        this.start = function ()
        {
            services.robot.name = this.name;

            this.currentModel = robots.models[this.model];

            this.screen = 'edit';
            this.state = 'loading'

            var result = services.robot.readState()
                .error(function (e, data)
                {
                    if (data.message)
                        me.errorMessage = data.message;
                    if (data.errmsg)
                        me.errorMessage = data.errmsg;
                    else
                    {
                        me.errorMessage = 'Cannot load data from server.';
                    }
                    me.state = 'error';
                    $scope.$apply();
                })
                .done(function (data)
                {
                    if (data.success)
                    {
                        for (var actuator in data.data)
                        {
                            if (robots.models[me.model].actuators[actuator])
                            {
                                data.data[actuator].name = actuator;
                                me.robot.actuators[actuator] = new Servo(data.data[actuator]);
                            }
                        }
                        me.state = 'ready';
                    }
                    else
                    {
                        if (data.message)
                            me.errorMessage = data.message;
                        else if (data.errmsg)
                            me.errorMessage = data.errmsg;
                        else
                            me.errorMessage = 'Cannot load data from server.';
                        me.state = 'error';
                    }
                    $scope.$apply();
                });

            // setInterval($.proxy(this.reloadPositions, this), 500);
            return result;
        };

        this.reloadPositions = function ()
        {
            var skipfail = true;
            return services.robot.readState()
                .error(function (e, data)
                {
                    if (!skipfail)
                    {
                        if (data.message)
                            me.errorMessage = data.message;
                        if (data.errmsg)
                            me.errorMessage = data.errmsg;
                        else
                        {
                            me.errorMessage = 'Cannot load data from server.';
                        }
                        me.state = 'error';
                        $scope.$apply();
                    }
                    else
                    {
                        me.state = 'ready';
                        $scope.$apply();
                    }
                })
                .done(function (data)
                {
                    if (data.success)
                    {
                        for (var actuator in data.data)
                        {
                            if (me.robot.actuators[actuator])
                                me.robot.actuators[actuator]._angle = parseFloat(data.data[actuator].angle);
                        }
                        me.state = 'ready';
                    }
                    else
                    {
                        if (data.message)
                            me.errorMessage = data.message;
                        else if (data.errmsg)
                            me.errorMessage = data.errmsg;
                        else
                            me.errorMessage = 'Cannot load data from server.';
                        me.state = 'error';
                    }
                    $scope.$apply();
                });
        };

        this.findKeyFrame = function (name)
        {
            for (var i in this.document.keyframes)
            {
                if (this.document.keyframes[i].name == name)
                    return i;
            }
            return false;
        };

        this.findSequence = function (name)
        {
            for (var i in this.document.sequences)
            {
                if (this.document.sequences[i].name == name)
                    return i;
            }
            return false;
        };

        this._frameName = function ()
        {
            var n = 1;
            for (var i = 0 ; i < this.document.keyframes.length; i++)
            {
                if (this.document.keyframes[i].name == 'Frame ' + n)
                {
                    n++;
                    i = -1;
                }
            }
            return 'Frame ' + n;
        };

        this._sequenceName = function ()
        {
            var n = 1;
            for (var i = 0 ; i < this.document.sequences.length; i++)
            {
                if (this.document.sequences[i].name == 'Sequence ' + n)
                {
                    n++;
                    i = -1;
                }
            }
            return 'Sequence ' + n;
        };

        this.createFrame = function (index)
        {
            var keyframeName = prompt("Enter the frame name:", (this._current ? this._current.name : this._frameName()));
            if (keyframeName)
            {
                var keyframe = {name: keyframeName, actuators: {}};

                if ((index != undefined) || (index < 0) || (index >= this.document.keyframes.length))
                    this.document.keyframes.splice(index, 0, keyframe);
                else
                    this.document.keyframes.push(keyframe);

                if (this.document.keyframes.length == 1)
                    this.current(keyframe);

                for (var actuator in this.robot.actuators)
                {
                    keyframe.actuators[actuator] = {
                        name: actuator,
                        angle: uiAngle(this.robot.actuators[actuator].position())
                    };
                }

                this.current(keyframe);
                return keyframe;
                this._updateCurrentFrameAngles();
            }
            return false;
        };
        this._playCurrentFrame = function(keyFrame)
        {
            var sequence = {name: "playCurrent", positions: [{name: keyFrame.name, time: 1}]};
            var i = this.findSequence("playCurrent");
            if (i !== false){
                this.document.sequences[i] = sequence;
            }
            else {
                this.document.sequences.push(sequence);
            }
            this.currentSequence(sequence);
            $("#play-trajectory-button").click();

        };
        this.createSequence = function (index)
        {
            var sequenceName = prompt('Sequence name:', this._sequenceName());
            if (sequenceName)
            {
                var kf = this.findSequence(name);
                if (kf === false)
                {
                    var sequence = { name: sequenceName, positions: [] };

                    if ((index != undefined) || (index < 0) || (index >= this.document.sequences.length))
                        this.document.sequences.splice(index, 0, sequence);
                    else
                        this.document.sequences.push(sequence);

                    if (this.document.sequences.length == 1)
                        this.currentSequence(sequence);

                    return sequence;
                }
            }
            return false;
        };

        this.remove = function (keyframe)
        {
            for (var i = 0; i < this.document.keyframes.length; i++)
            {
                if (this.document.keyframes[i] == keyframe)
                {
                    this.document.keyframes.splice(i, 1);
                    for (var s in this.document.sequences)
                    {
                        for (var j = this.document.sequences[s].positions.length - 1; j >= 0; j--)
                        {
                            if (keyframe.name == this.document.sequences[s].positions[j].name)
                            {
                                this.document.sequences[s].positions.splice(j, 1);
                            }
                        }
                    }
                    return true;
                }
            }
            return false;
        };

        this.removeSequence = function (sequence)
        {
            for (var i = 0; i < this.document.sequences.length; i++)
            {
                if (this.document.sequences[i] == sequence)
                {
                    this.document.sequences.splice(i, 1);
                    return true;
                }
            }
            return false;
        };

        this.removeGroup = function (group)
        {
            for (var i = 0; i < this.document.groups.length; i++)
            {
                if (this.document.groups[i] == group)
                {
                    this.document.groups.splice(i, 1);
                    return true;
                }
            }
            return false;
        };

        this.removePositionFromSequence = function (position)
        {
            if (this._currentSequence)
            {
                for (var i = this._currentSequence.positions.length-1; i >= 0; i--)
                {
                    if (this._currentSequence.positions[i] == position)
                    {
                        this._currentSequence.positions.splice(i, 1);
                    }
                }
            }
        };

        this.current = function (current)
        {
            this._current = current;
        };

        this.isCurrent = function (keyframe)
        {
            if (this._current)
                return (this._current == keyframe);
            else
                return false;
        };

        this.currentSequence = function (current)
        {
            this._currentSequence = current;
        };

        this.isCurrentSequence = function (keyframe)
        {
            if (this._currentSequence)
                return (this._currentSequence == keyframe);
            else
                return false;
        };

        this._groupName = function ()
        {
            var n = 1;
            for (var i = 0; i < this.document.groups.length; i++)
            {
                if (this.document.groups[i].name == 'Group ' + n)
                {
                    n++;
                    i = -1;
                }
            }
            return 'Group ' + n;
        };

        this.createGroup = function ()
        {
            var $selected = this.$actuatorsContainer.find('.ui-selected');
            if ($selected.length > 0)
            {
                var groupName = prompt('Group name:', this._groupName());
                if (groupName)
                {
                    var group = {name: groupName, actuators: []};
                    $selected.each(function ()
                    {
                        group.actuators.push(this.label);
                    });
                    this.document.groups.push(group);

                    $scope.$apply();
                }
            }
            else
                alert('Cannot create an empty group.');
        };

        this.isGroup = function (group)
        {
            return this._group == group;
        };

        this.group = function (group)
        {
            if (group)
            {
                this._group = group;
                this.$actuatorsContainer.find('me-sgraph').each(function (index, o)
                {
                    var a = o.$label.text();
                    for (var i in group.actuators)
                    {
                        if (a == group.actuators[i])
                        {
                            $(this).addClass('ui-selected');
                            return;
                        }
                    }
                    $(this).removeClass('ui-selected');
                });
            }
            else
                return this._group;
        };

        this.start(); // TODO: Not auto start


        function reloadWindowSize()
        {
            $('#robotView').width($('#colMap').width());
        }

        var $window = $(window).on('resize', reloadWindowSize);

        $(function () {
            setTimeout(function () {
                reloadWindowSize();
                $window.trigger('resize');
            }, 100);
        });
    }]);

    var $document = $(document)
        .ajaxStart(function () {
            var $loading = $('<div class="label label-default">loading...</div>');
            $loading.appendTo('#loadingContainer');

            function ajaxSuccess()
            {
                $loading.removeClass('label-default').addClass('label-success').html('ok');
            }

            function ajaxError()
            {
                $loading.removeClass('label-default').addClass('label-error').html('failed');
            }

            $document.on('ajaxSuccess', ajaxSuccess).on('ajaxError', ajaxError)
                .one('ajaxComplete', function (e)
                {
                    $document
                        .off('ajaxSuccess', ajaxSuccess)
                        .off('ajaxError', ajaxError);

                    setTimeout(function (e) {
                        $loading.remove();
                    }, 500);
                });
        });

})(jQuery);

