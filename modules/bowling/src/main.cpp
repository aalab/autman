
#include <config.h>

#include "compilerdefinitions.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <services/robot.h>

using namespace std;

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("no-walk,n", "Disable the walking")
		("config,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "weight_lifting configuration file");

	using boost::property_tree::ptree;
	ptree pt;

	po::variables_map vm;
	try
	{
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	/*
	auto ptWL = pt.get_child_optional("weight_lifting");
	if (!ptWL)
	{
		ERROR("'puppet' configuration was not found.");
		return 2;
	}
	 */

	auto ptRobot = pt.get_child_optional("robot");
	if (!ptRobot)
	{
		ERROR("'robot' configuration was not found.");
		return 3;
	}

	VERBOSE("Initializing robotService: " << (*ptRobot).get<std::string>("address", "127.0.0.1") << ":" << (*ptRobot).get<unsigned int>("port"));
	services::Robot robotService(
		(*ptRobot).get<std::string>("robot_name", "arash"), (*ptRobot).get<std::string>("address", "127.0.0.1"),
		(*ptRobot).get<unsigned int>("port")
	);

	try
	{
		namespace rdata = services::data::robot;
		{
			rdata::Trajectory t("prepare", rdata::sec(2));
			t.entryCycleTimeUSec() = rdata::sec(1);
			robotService.trajectory(t);
			sleep(5);

		}
		if (!vm.count("no-walk"))
		{
			rdata::Move move;
			move.frames.emplace_back(0.03, 0.0, 0.0, rdata::msec(750), 4);
			move.frames.emplace_back(0.06, 0.0, 0.0, rdata::msec(750), 5);
			move.frames.emplace_back(0.03, 0.0, 0.0, rdata::msec(750), 4);
			robotService.move(move);
			usleep(rdata::msec(750)*4);
			usleep(rdata::msec(750)*5);
			usleep(rdata::msec(750)*4);
			sleep(1);
		}

		{
			rdata::Trajectory t("throw", rdata::sec(0.75));
			t.entryCycleTimeUSec(0);
			robotService.trajectory(t);
		}
	}
	catch (std::exception &e)
	{
		ERROR("boost::po exception " << e.what());
		return 4;
	}

	return 0;
}
