/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <thread>
#include "robotstatereader.h"
#include "../compilerdefinitions.h"
#include "../robocuparbitrator.h"

robocup::behaviors::RobotStateReader *robocup::behaviors::RobotStateReader::instance = nullptr;

robocup::behaviors::RobotStateReader::RobotStateReader(robocup::Arbitrator &arbitrator)
	: arbitrator(arbitrator)
{
	instance = this;

	this->setDefaultInterval();
}

void robocup::behaviors::RobotStateReader::process()
{
	/*
	if (this->arbitrator.state.update())
	{
		this->arbitrator.headMoveByStep(D2R(5), 0.0);
		if (this->dumpUiData)
		{
			VERBOSE("NeckLateral: " << this->arbitrator.state.neckLateral.angle);
			VERBOSE("NeckTransversal: " << this->arbitrator.state.neckTransversal.angle);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(this->readIntervalUSec));
	}
	if (this->dumpUiData)
	{
		VERBOSE("NeckLateral: " << this->arbitrator.state.neckLateral.angle);
		VERBOSE("NeckTransversal: " << this->arbitrator.state.neckTransversal.angle);
	}
	 */
}

void robocup::behaviors::RobotStateReader::setDefaultInterval()
{
	this->readIntervalUSec = 100;
}
