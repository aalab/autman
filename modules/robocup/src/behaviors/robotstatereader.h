/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_HEAD_H
#define ROBOCUP_HEAD_H

#include <brain/base/behavior.h>
#include "../arashrobotstate.h"

namespace robocup
{
class Arbitrator;
namespace behaviors
{
class RobotStateReader
	: public brain::Behavior
{
public:
	static RobotStateReader *instance;
private:
	Arbitrator &arbitrator;
public:
	RobotStateReader(robocup::Arbitrator &arbitrator);

	unsigned int readIntervalUSec;

	void setDefaultInterval();

protected:
	virtual void process();
};
}
}

#endif //ROBOCUP_HEAD_H
