/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ALIGNMENT_H
#define ROBOCUP_ALIGNMENT_H

#include <brain/base/behavior.h>
#include <services/vision.h>
#include <services/robot.h>
#include "../robocuparbitrator.h"

#ifndef override
#define override
#endif

namespace robocup
{

class Arbitrator;

namespace behaviors
{
class Alignment
	: public brain::Behavior
{
public:
	static Alignment *instance;
protected:
	Arbitrator &arbitrator;

	virtual void process() override;
public:
	Alignment(robocup::Arbitrator &arbitrator);
	std::chrono::system_clock::time_point currentFrameTime;
	std::chrono::system_clock::time_point lastFrameTime;
	virtual void disable() override;
};
}
}


#endif //ROBOCUP_ALIGNMENT_H
