/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 21, 2015
 */

#ifndef ROBOCUP_BALLSEARCH_H
#define ROBOCUP_BALLSEARCH_H

#include <brain/base/behavior.h>

namespace robocup
{
class Arbitrator;
namespace behaviors
{
class BallSearch
	: public brain::Behavior
{
public:
	static BallSearch *instance;

	BallSearch(robocup::Arbitrator &arbitrator);
protected:
	int center_offset;

	Arbitrator &arbitrator;
protected:
	virtual void process();
};
}
}


#endif //ROBOCUP_BALLSEARCH_H
