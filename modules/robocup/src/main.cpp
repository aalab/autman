
#include "config.h"

#include "compilerdefinitions.h"

#include "robocuparbitrator.h"
#include "arashrobotstate.h"

#include "behaviors/gccontroller.h"
#include "behaviors/alignment.h"
#include "behaviors/robotstatereader.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <services/robot.h>
#include <services/vision.h>
#include <services/gyro.h>
#include <services/robocup/gamecontroller.h>

using namespace std;

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("config_file,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "robot config file");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "RoboCup Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	auto ptRobot = pt.get_child_optional("robot");
	if (!ptRobot)
	{
		ERROR("'robot' configuration was not found.");
		return 2;
	}
	auto ptGyro = pt.get_child_optional("gyro");
	if (!ptGyro)
	{
		ERROR("'gyro' configuration was not found.");
		return 3;
	}
	auto ptVision = pt.get_child_optional("vision");
	if (!ptVision)
	{
		ERROR("'vision' configuration was not found.");
		return 4;
	}
	VERBOSE("GameController service");
	services::robocup::GameController gcService;
	if (gcService.initNetwork())
		VERBOSE("GameController service OK!!");
	else
	{
		ERROR("Error initializing the GameController Service.");
		return 0;
	}
	VERBOSE("Robot service");
	services::Robot robotService((*ptRobot).get<std::string>("robot_name", "arash"), (*ptRobot).get<std::string>("address", "localhost"), (*ptRobot).get<unsigned int>("port", 1313));
	VERBOSE("Gyro service");
	services::Gyro gyroService((*ptGyro).get<std::string>("address", "localhost"), (*ptGyro).get<unsigned int>("port", 1414));
	VERBOSE("Vision service");
	services::Vision visionService((*ptVision).get<std::string>("address", "localhost"), (*ptVision).get<unsigned int>("port", 1515));

	/*
	services::data::robot::Actuator cmd("NeckLateral", -0.83);
	robotService.actuator(cmd);
	cmd.name = "NeckTransversal";
	cmd.angle = 0.0;
	robotService.actuator(cmd);
	 */

	/*
	services::data::robot::Move moveRequest;
	moveRequest.entryTimeUSec = 1000000;
	moveRequest.frames.emplace_back(0.05, 0.0, 0.0, 700000, 3);
	moveRequest.frames.emplace_back(0.1, 0.0, 0.0, 700000, 6);
	moveRequest.frames.emplace_back(0.0, 0.0, 0.0, 100, 0);
	if (robotService.move(moveRequest))
	{
		VERBOSE("OK");
	}
	else
		VERBOSE("ERROR");
	*/

	/*
	services::data::vision::FetchResponse visionResponse;
	if (visionService.fetch(visionResponse))
	{
		VERBOSE("Fetch Ok");
	}
	else
	{
		VERBOSE("Fetch Fail");
	}

	if (visionService.updateCameraCalibrations() && (!visionService.cameraCalibrations.calibrations.empty()))
	{
		VERBOSE("updateCameraCalibrations Ok");
	}
	else
	{
		VERBOSE("updateCameraCalibrations Fail");
		return 5;
	}
	*/

	robocup::Arbitrator arbitrator(visionService, robotService);
	arbitrator.addBehavior(new robocup::behaviors::Alignment(arbitrator));
	arbitrator.addBehavior(new robocup::behaviors::RobotStateReader(arbitrator));
	// arbitrator.addBehavior(new robocup::behaviors::GCController(arbitrator, gcService));
	arbitrator.start(true);

	return 0;
}
