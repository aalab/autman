/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#ifndef ROBOCUP_ROBOCUPBRAIN_H
#define ROBOCUP_ROBOCUPBRAIN_H

#include <brain/arbitrator.h>
#include "behaviors/alignment.h"
#include "behaviors/robotstatereader.h"
#include "arashrobotstate.h"
#include "providers/visiondataprovider.h"

namespace robocup
{

namespace behaviors
{
class Alignment;
class RobotStateReader;
}
class Arbitrator
	: public brain::Arbitrator
{
private:
	services::Vision &vision;
protected:
	virtual void process();
public:
	Arbitrator(services::Vision &vision, services::Robot &robot);
	virtual ~Arbitrator();

	services::data::vision::TsaiWrapper* currentCameraCalibration;

	services::Robot &robot;

	providers::VisionProvider visionDataProvider;

	ArashRobotState state;

	void headMoveByStep(float pan, float tilt);
	void headMoveTo(float pan, float tilt);
};
}


#endif //ROBOCUP_ROBOCUPBRAIN_H
