/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include "robocuparbitrator.h"

#include <services/data/robot/actuator.h>

namespace rdata = services::data::robot;

robocup::Arbitrator::Arbitrator(services::Vision &vision, services::Robot &robot)
	: vision(vision), currentCameraCalibration(nullptr), robot(robot), visionDataProvider(vision), state(robot)
{
	this->currentCameraCalibration = &vision.cameraCalibrations.calibrations[0];
}

void robocup::Arbitrator::process()
{ }

robocup::Arbitrator::~Arbitrator()
{
	for (brain::Behavior *b : this->behaviors)
		delete b;
}

void robocup::Arbitrator::headMoveByStep(float pan, float tilt)
{
	float
		_pan = this->state.neckTransversal.angle + pan,
		_tilt = this->state.neckLateral.angle + tilt;

	if (_tilt < -1.60445)
		_tilt = -1.60445;
	else if (_tilt > 1.46504)
		_tilt = 1.46504;

	if (_pan < -2.29321)
		_pan = -2.29321;
	else if (_pan > 2.46827)
		_pan = 2.46827;

	rdata::Actuator actuator("NeckLateral", _tilt);
	actuator.speed = 200;
	this->robot.actuator(actuator);
	actuator.name = "NeckTransversal";
	actuator.angle = _pan;
	this->robot.actuator(actuator);

	this->state.neckTransversal.update(_pan);
	this->state.neckLateral.update(_tilt);
}

void robocup::Arbitrator::headMoveTo(float pan, float tilt)
{
	if (tilt < -1.60445)
		tilt = -1.60445;
	else if (tilt > 1.46504)
		tilt = 1.46504;

	if (pan < -2.29321)
		pan = -2.29321;
	else if (pan > 2.46827)
		pan = 2.46827;

	rdata::Actuator actuator("NeckLateral", tilt);
	actuator.speed = 200;
	this->robot.actuator(actuator);
	actuator.name = "NeckTransversal";
	actuator.angle = pan;
	this->robot.actuator(actuator);

	this->state.neckTransversal.angle = pan;
	this->state.neckLateral.angle = tilt;
}
