/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef TELEOP_SERVER_H
#define TELEOP_SERVER_H

#include <services/base/udpserver.h>
#include <services/data/teleop/playstation.h>
#include <services/data/teleop/xbox.h>
#include <services/data/teleop/keyboard.h>
#include <services/data/teleop/touch.h>
#include <services/robot.h>

namespace rdata = services::data::robot;

namespace teleop
{
class Server
	: public services::base::UDPServer
{
private:
	services::Robot& robot;

	rdata::Head head;
	bool headStarted;
	std::chrono::system_clock::time_point headLastTime;
public:
	Server(boost::asio::io_service &io_service, unsigned int port, services::Robot &robot);

protected:
	virtual void processCommand(Json::Value &jsonCommand) override;

	virtual void processCommand(services::data::teleop::XboxGamepad &gamepad);
	virtual void processCommand(services::data::teleop::Keyboard &keyboard);
	virtual void processCommand(services::data::teleop::PSGamepad &gamepad);
	virtual void processCommand(services::data::teleop::Touch &touch);
};
}


#endif //TELEOP_SERVER_H
