/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#ifndef TELEOP_CONFIG_H
#define TELEOP_CONFIG_H

namespace teleop
{
class Config
{
private:
	float _speedXMin;
	float _speedXMax;
	float _speedYMin;
	float _speedYMax;
	float _speedThetaMin;
	float _speedThetaMax;
	float _headPanMax;
	float _headPanMin;
	float _headTiltMax;
	float _headTiltMin;
public:
	const float speedXMin() const;
	Config &speedXMin(float speedXMin);

	const float speedXMax() const;
	Config &speedXMax(float speedXMax);

	const float speedYMin() const;
	Config &speedYMin(float speedYMin);

	const float speedYMax() const;
	Config &speedYMax(float speedYMax);

	const float speedThetaMin() const;
	Config &speedThetaMin(float speedThetaMin);

	const float speedThetaMax() const;
	Config &speedThetaMax(float speedThetaMax);

	const float headPanMax() const;
	Config &headPanMax(float headPanMax);

	const float headPanMin() const;
	Config &headPanMin(float headPanMin);

	const float headTiltMax() const;
	Config &headTiltMax(float headTiltMax);

	const float headTiltMin() const;
	Config &headTiltMin(float headTiltMin);
};
}

#endif //TELEOP_CONFIG_H
