/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include "server.h"
#include "compilerdefinitions.h"

#include <iostream>

#define HEAD_SPEED 30.0/1000.0	// 10 degrees per second.

teleop::Server::Server(boost::asio::io_service &io_service, unsigned int port, services::Robot &robot)
	: robot(robot), headStarted(false), UDPServer(io_service, port)
{ }

void teleop::Server::processCommand(Json::Value &jsonCommand)
{
	std::string commandName(jsonCommand["command"].asString());

	if (commandName == "inputstate")
	{
		std::string inputType(jsonCommand["type"].asString());
		VERBOSE("Input type received: " << inputType);
		if (inputType == "xbox")
		{
			services::data::teleop::XboxGamepad xbox;
			services::json::factory(jsonCommand["data"], xbox);
			this->processCommand(xbox);
		}
		else if (inputType == "playstation")
		{
			services::data::teleop::PSGamepad ps;
			services::json::factory(jsonCommand["data"], ps);
			this->processCommand(ps);
		}
		else if (inputType == "keyboard")
		{
			services::data::teleop::Keyboard keyboard;
			services::json::factory(jsonCommand["data"], keyboard);
			this->processCommand(keyboard);
		}
		else if (inputType == "touch")
		{
			services::data::teleop::Touch touch;
			services::json::factory(jsonCommand["data"], touch);
			this->processCommand(touch);
		}
	}
	else
	{
		ERROR("Unknown command \"" << commandName << "\"");
		this->send("{\"success\":false,\"message\":\"Unknown command.\"}");
	}
}

void teleop::Server::processCommand(services::data::teleop::XboxGamepad &gamepad)
{
	rdata::Move move;
	move.frames.emplace_back();
	rdata::Frame &frame = move.frames.back();
	//
	frame.x = gamepad.rjoystick().y;
	frame.y = gamepad.cross().x;
	frame.theta = gamepad.rjoystick().x;

	if (this->robot.move(move))
	{
		this->send("{\"success\":true}");
	}
	else
	{
		this->send("{\"success\":false}");
	}
}

void teleop::Server::processCommand(services::data::teleop::PSGamepad &gamepad)
{
	this->send("{\"success\":true}");
}

void teleop::Server::processCommand(services::data::teleop::Keyboard &keyboard)
{
	rdata::Move move;
	move.frames.emplace_back();
	rdata::Frame &frame = move.frames.back();

	if (keyboard.shift())
	{
		frame.x = keyboard.arrowLetters().y;
		frame.theta = keyboard.arrowLetters().x;
		if (keyboard.alt())
		{
			frame.y = keyboard.arrows().x;
		}
	}
	else
	{
		frame.x = keyboard.arrowLetters().y / 2.0;
		frame.theta = keyboard.arrowLetters().x / 2.0;
		if (keyboard.alt())
		{
			frame.y = keyboard.arrows().x / 2.0;
		}
	}

	if (keyboard.control() > 0)
	{
		this->headStarted = false;
		this->head.pan = 0.0;
		this->head.tilt = 40.0;
		this->robot.move(head);
	}
	else if ((keyboard.alt() == 0) && ((keyboard.arrows().x != 0) || (keyboard.arrows().y != 0)))
	{
		if (!this->headStarted)
		{
			this->headStarted = true;
			this->headLastTime = std::chrono::system_clock::now();
		}
		else
		{
			this->head.pan += HEAD_SPEED * (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - this->headLastTime).count()) * keyboard.arrows().x;
			this->head.tilt += HEAD_SPEED * (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - this->headLastTime).count()) * keyboard.arrows().y;

			if (this->head.pan < -90)
				this->head.pan = -90;
			else if (this->head.pan > 90)
				this->head.pan = 90;

			if (this->head.tilt < -25)
				this->head.tilt = -25;
			else if (this->head.tilt > 60)
				this->head.tilt = 60;
		}
		this->robot.move(this->head);
		this->headLastTime = std::chrono::system_clock::now();
	}
	else
	{
		this->headStarted = false;
	}

	if (this->robot.move(move))
	{
		Json::Value result;
		result["success"] = true;
		services::json::factory(this->head, result["data"]["head"]);
		this->send(result);
	}
	else
	{
		this->send("{\"success\":false}");
	}
}

void teleop::Server::processCommand(services::data::teleop::Touch &touch)
{
	this->send("{\"success\":true}");
}
