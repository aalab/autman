/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include "teleopconfig.h"

const float teleop::Config::speedXMin() const
{
	return this->_speedXMin;
}

teleop::Config &teleop::Config::speedXMin(float speedXMin)
{
	this->_speedXMin = speedXMin;
	return *this;
}

const float teleop::Config::speedXMax() const
{
	return this->_speedXMax;
}

teleop::Config &teleop::Config::speedXMax(float speedXMax)
{
	this->_speedXMax = speedXMax;
	return *this;
}

const float teleop::Config::speedYMin() const
{
	return this->_speedYMin;
}

teleop::Config &teleop::Config::speedYMin(float speedYMin)
{
	this->_speedYMin = speedYMin;
	return *this;
}

const float teleop::Config::speedYMax() const
{
	return this->_speedYMax;
}

teleop::Config &teleop::Config::speedYMax(float speedYMax)
{
	this->_speedYMax = speedYMax;
	return *this;
}

const float teleop::Config::speedThetaMin() const
{
	return this->_speedThetaMin;
}

teleop::Config &teleop::Config::speedThetaMin(float speedThetaMin)
{
	this->_speedThetaMin = speedThetaMin;
	return *this;
}

const float teleop::Config::speedThetaMax() const
{
	return this->_speedThetaMax;
}

teleop::Config &teleop::Config::speedThetaMax(float speedThetaMax)
{
	this->_speedThetaMax = speedThetaMax;
	return *this;
}

const float teleop::Config::headPanMax() const
{
	return this->_headPanMax;
}

teleop::Config &teleop::Config::headPanMax(float headPanMax)
{
	this->_headPanMax = headPanMax;
	return *this;
}

const float teleop::Config::headPanMin() const
{
	return this->_headPanMin;
}

teleop::Config &teleop::Config::headPanMin(float headPanMin)
{
	this->_headPanMin = headPanMin;
	return *this;
}

const float teleop::Config::headTiltMax() const
{
	return this->_headTiltMax;
}

teleop::Config &teleop::Config::headTiltMax(float headTiltMax)
{
	this->_headTiltMax = headTiltMax;
	return *this;
}

const float teleop::Config::headTiltMin() const
{
	return this->_headTiltMin;
}

teleop::Config &teleop::Config::headTiltMin(float headTiltMin)
{
	this->_headTiltMin = headTiltMin;
	return *this;
}
