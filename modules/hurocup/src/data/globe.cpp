/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 07, 2015
 */

#include "globe.h"

hurocup::data::GlobeObject::GlobeObject(services::data::vision::VisionObject &object)
	: object(object)
{ }

void hurocup::data::Globe::clear()
{
	this->objects.clear();
}

bool hurocup::data::Globe::isObstacle(float angleX, float angleY)
{
	for (GlobeObject &o : this->objects)
	{
		if (
			(angleX >= o.boundBox.a.x) && (angleX <= o.boundBox.b.x)
			&& (o.boundBox.b.y > angleY)
		)
			return true;
	}
	return false;
}
