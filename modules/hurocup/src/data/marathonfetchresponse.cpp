/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 09, 2015
 */

#include <services/base/json.h>
#include "marathonfetchresponse.h"

bool hurocup::data::MarathonFetchResponse::classifyJson(Json::Value &json)
{
	if (json["type"].asString() == "line")
	{
		services::json::factory(json, this->line);
		return true;
	}
	else if (json["type"].asString() == "marker")
	{
		this->marker = json["marker"].asString();
		this->markerCenter.x = json["x"].asDouble();
		this->markerCenter.y = json["y"].asDouble();
		return true;
	}
	return false;
}
