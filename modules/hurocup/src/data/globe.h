/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 07, 2015
 */

#ifndef HUROCUP_GLOBE_H
#define HUROCUP_GLOBE_H

#include <vector>

#include <services/data/boundbox.h>
#include <services/data/vision/visionobject.h>

namespace hurocup
{
namespace data
{
class GlobeObject
{
public:
	GlobeObject(services::data::vision::VisionObject &object);

	services::data::BoundBox2D boundBox;
	services::data::vision::VisionObject object;
};

class Globe
{
public:
	std::vector<GlobeObject> objects;

	float leftEdge;
	float rightEdge;

	float worldLeftEdge;
	float worldRightEdge;

	void clear();
	bool isObstacle(float angleX, float angleY);
};
}
}

#endif //HUROCUP_GLOBE_H
