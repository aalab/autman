/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include "hurocuparbitrator.h"
#include "compilerdefinitions.h"

#include <services/data/robot/actuator.h>

#include <iostream>

namespace rdata = services::data::robot;

hurocup::Arbitrator::Arbitrator(services::Vision &vision, services::Robot &robot)
	: robot(robot), currentCameraCalibration(nullptr), vision(vision), state(robot)
{
	this->currentCameraCalibration = &vision.cameraCalibrations.calibrations[0];
}

void hurocup::Arbitrator::process()
{ }

hurocup::Arbitrator::~Arbitrator()
{
	for (brain::Behavior *b : this->behaviors)
		delete b;
}

void hurocup::Arbitrator::headMoveByStep(float pan, float tilt)
{
	float
		_pan = this->state.neckTransversal.angle + pan,
		_tilt = this->state.neckLateral.angle + tilt;

	if (_tilt < -1.60445)
		_tilt = -1.60445;
	else if (_tilt > 1.46504)
		_tilt = 1.46504;

	if (_pan < -2.29321)
		_pan = -2.29321;
	else if (_pan > 2.46827)
		_pan = 2.46827;

	rdata::Actuator actuator("NeckLateral", _tilt);
	actuator.speed = 200;
	this->robot.actuator(actuator);
	actuator.name = "NeckTransversal";
	actuator.angle = _pan;
	// this->robot.actuator(actuator);

	this->state.neckTransversal.update(_pan);
	this->state.neckLateral.update(_tilt);
}

void hurocup::Arbitrator::headMoveTo(float pan, float tilt)
{
	if (tilt < -1.60445)
		tilt = -1.60445;
	else if (tilt > 1.46504)
		tilt = 1.46504;

	if (pan < -2.29321)
		pan = -2.29321;
	else if (pan > 2.46827)
		pan = 2.46827;

	rdata::Actuator actuator("NeckLateral", tilt);
	actuator.speed = 200;
	this->robot.actuator(actuator);
	actuator.name = "NeckTransversal";
	actuator.angle = pan;
	// this->robot.actuator(actuator);

	this->state.neckTransversal.angle = pan;
	this->state.neckLateral.angle = tilt;
}

bool hurocup::Arbitrator::move(rdata::FullMove &move)
{
	if (this->robot.move(move))
	{
		this->state.neckTransversal.angle = move.head.pan;
		this->state.neckLateral.angle = move.head.tilt;
		return true;
	}
	return false;
}

bool hurocup::Arbitrator::move(rdata::Head &head)
{
	if (this->robot.move(head))
	{
		this->state.neckTransversal.angle = head.pan;
		this->state.neckLateral.angle = head.tilt;
		return true;
	}
	return false;
}

bool hurocup::Arbitrator::move(rdata::Move &move)
{
	return this->robot.move(move);
}

void hurocup::Arbitrator::terminate()
{
	VERBOSE("Shutdown signal received, terminating behaviors.");
	Behavior::terminate();
	for (brain::Behavior *behavior : this->behaviors)
	{
		behavior->terminate();
	}
}

bool hurocup::Arbitrator::gyroData(services::data::gyro::DataResponse &data)
{
	if (this->gyro.data(data))
	{
		return true;
	}
	return false;
}
