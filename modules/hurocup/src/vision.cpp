/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 31, 2015
 */

#include "vision.h"
#include "compilerdefinitions.h"

#include <algorithm>

services::data::Point2D hurocup::Vision::camera(320, 240);
services::data::Point2D hurocup::Vision::cameraAngles(D2R(58.0), D2R(46.0));

double hurocup::Vision::overlapArea(
	double x1, double y1, double width1, double height1,
	double x2, double y2, double width2, double height2
)
{
	return
		(std::min(x1 + width1, x2 + width2) - std::max(x1, x2))
		* (std::min(y1 + height1, y2 + height2) - std::max(y1, y2));
}

double hurocup::Vision::overlapArea(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapArea(a.a.x, a.a.y, a.width(), a.height(), b.a.x, b.a.y, b.width(), b.height());
}

double hurocup::Vision::overlapMax(
	double x1, double y1, double width1, double height1,
	double x2, double y2, double width2, double height2
)
{
	return (
		overlapArea(x1, y1, width1, height1, x2, y2, width2, height2) / std::max(width1*height1, width2*height2)
	);
}

double hurocup::Vision::overlapMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapMax(a.a.x, a.a.y, a.width(), a.height(), b.a.x, b.a.y, b.width(), b.height());
}

double hurocup::Vision::overlapMin(
	double x1, double y1, double width1, double height1,
	double x2, double y2, double width2, double height2)
{
	return (
		overlapArea(x1, y1, width1, height1, x2, y2, width2, height2) / std::min(width1*height1, width2*height2)
	);
}

double hurocup::Vision::overlapMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapMin(a.a.x, a.a.y, a.width(), a.height(), b.a.x, b.a.y, b.width(), b.height());
}

double hurocup::Vision::overlapAreaHorizontal(double x1, double width1, double x2, double width2)
{
	return std::max(0.0, std::min(x1 + width1, x2 + width2) - std::max(x1, x2));
}

double hurocup::Vision::overlapHorizontalMax(double x1, double width1, double x2, double width2)
{
	return (overlapAreaHorizontal(x1, width1, x2, width2) / std::max(width1, width2));
}

double hurocup::Vision::overlapHorizontalMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapHorizontalMax(a.a.x, a.width(), b.a.x, b.width());
}

double hurocup::Vision::overlapHorizontalMin(double x1, double width1, double x2, double width2)
{
	return (overlapAreaHorizontal(x1, width1, x2, width2) / std::min(width1, width2));
}

double hurocup::Vision::overlapHorizontalMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapHorizontalMin(a.a.x, a.width(), b.a.x, b.width());
}

double hurocup::Vision::overlapAreaVertical(double y1, double height1, double y2, double height2)
{
	return std::max(0.0, std::min(y1 + height1, y2 + height2) - std::max(y1, y2));
}

double hurocup::Vision::overlapVerticalMax(double y1, double height1, double y2, double height2)
{
	return (overlapAreaVertical(y1, height1, y2, height2) / std::max(height1, height2));
}

double hurocup::Vision::overlapVerticalMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapVerticalMax(a.a.y, a.height(), b.a.y, b.height());
}

double hurocup::Vision::overlapVerticalMin(double y1, double height1, double y2, double height2)
{
	return (overlapAreaVertical(y1, height1, y2, height2) / std::min(height1, height2));
}

double hurocup::Vision::overlapVerticalMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b)
{
	return overlapVerticalMin(a.a.y, a.height(), b.a.y, b.height());
}
