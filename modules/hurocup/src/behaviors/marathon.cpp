/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 09, 2015
 */

#include <thread>
#include "marathon.h"
#include "../data/marathonfetchresponse.h"
#include "../hurocuparbitrator.h"
#include "../compilerdefinitions.h"
#include "../vision.h"

#include <iostream>

hurocup::behaviors::Marathon::Marathon(hurocup::Arbitrator &arbitrator)
	: arbitrator(arbitrator)
{ }

bool hurocup::behaviors::Marathon::isUnique()
{
	return true;
}

void hurocup::behaviors::Marathon::process()
{
	double maxVx = 0.1;

	hurocup::data::MarathonFetchResponse fetchResponse;
	if (this->arbitrator.vision.fetch(fetchResponse))
	{
		if (this->dumpUiData)
		{
			VERBOSE("Data fetched:");
			VERBOSEB("+           A: " << fetchResponse.line.a.x << ":" << fetchResponse.line.a.y);
			VERBOSEB("+           B: " << fetchResponse.line.b.x << ":" << fetchResponse.line.b.y);
			VERBOSEB("+ Inclination: " << R2D(fetchResponse.line.inclination()));
			VERBOSEB("+       Hypot: " << fetchResponse.line.hypot());
			VERBOSEB("+      Marker: " << fetchResponse.marker);
			VERBOSEB("+    Marker x: " << fetchResponse.markerCenter.x);
			VERBOSEB("+    Marker y: " << fetchResponse.markerCenter.y);
		}

		if (true || (fetchResponse.marker == "notfound") || (fetchResponse.markerCenter.y < Vision::camera.y/2.0))
		{
			this->followLine(fetchResponse);
		}
		else if (fetchResponse.marker == "unknown")
		{
			// MOVE the head a litle bit
			if (this->arbitrator.state.neckTransversal.angle == 0)
			{
				VERBOSE("Unknown marker, starting scanning, looking right.");
				rdata::FullMove move;                // Moves head and stops body
				move.head.pan = D2R(5);
				move.head.tilt = DEFAULT_HEAD_TILT;
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}
			else if (this->arbitrator.state.neckTransversal.angle == D2R(5))
			{
				VERBOSE("Unknown marker, starting scanning, looking left.");
				rdata::Head head;
				head.tilt = DEFAULT_HEAD_TILT;
				head.pan = D2R(-5);
				this->arbitrator.move(head);
				std::this_thread::sleep_for(std::chrono::milliseconds(500));
			}
			else if (this->arbitrator.state.neckTransversal.angle == D2R(-5))
			{
				VERBOSE("Nothing found, keep going forward.");
				rdata::FullMove move;                // Moves head and stops body
				move.move.vx = maxVx;
				move.head.tilt = DEFAULT_HEAD_TILT;
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}
		}
		else if (fetchResponse.marker == "forward")
		{
			rdata::FullMove move;                // Moves head and stops body
			move.move.vx = maxVx;
			move.head.tilt = DEFAULT_HEAD_TILT;
			this->arbitrator.move(move);
			std::this_thread::sleep_for(std::chrono::milliseconds(8000));
		}
		else if (fetchResponse.marker == "left")
		{
			rdata::FullMove move;                // Moves head and stops body
			move.move.vx = 0;
			move.move.vy = 0;
			move.move.vt = -0.1;
			move.head.tilt = DEFAULT_HEAD_TILT;
			this->arbitrator.move(move);

			std::this_thread::sleep_for(std::chrono::milliseconds(8710));
		}
		else if (fetchResponse.marker == "right")
		{
			rdata::FullMove move;                // Moves head and stops body
			move.move.vx = 0;
			move.move.vy = 0;
			move.move.vt = 0.1;
			move.head.tilt = DEFAULT_HEAD_TILT;
			this->arbitrator.move(move);

			std::this_thread::sleep_for(std::chrono::milliseconds(8510));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(30));
	}
	else
	{
		if (this->dumpUiData)
		{
			ERROR("Cannot fetch data from visionmodule.");
		}
	}
}

void hurocup::behaviors::Marathon::followLine(hurocup::data::MarathonFetchResponse &fetchResponse)
{
	boost::optional<services::data::vision::TsaiWrapper &> cc = this->arbitrator.vision.cameraCalibrations.calibrationByName(
		"arash-marathon");

	if (cc)
	{
		rdata::Move move;
		long double
			angle = fetchResponse.line.inclination() - M_PI_2l,
			bottomDistance = fetchResponse.line.b.x - (Vision::camera.x/2.0);

		VERBOSE("Inclination: " << R2D(angle));
		VERBOSE("Inclination: " << R2D(fetchResponse.line.inclination()));

		move.vt = MINMAXRATIO(0.0, 0.1, (std::min(std::max(angle, D2R(-25.0)), D2R(25.0)) / D2R(25.0))),
		move.vx = 0.1 - MINMAXRATIO(0.0, 0.1, (std::min(std::max(angle, D2R(-25.0)), D2R(25.0)) / D2R(25.0)));

		if (std::abs(angle) > D2R(22))
		{
			if (std::abs(bottomDistance) > 50)
			{
				move.vy = 0.2 * (bottomDistance > 0 ? -1 : 1);
				move.vt = 0;
			}
			else
			{
				move.vt = 0.1;
			}
		}
		else
		{
			move.vy = 0.0;
		}
		this->arbitrator.move(move);
	}
	else
	{
		ERROR("Cannot find 'arash-marathon' camera calibration.");
	}
}
