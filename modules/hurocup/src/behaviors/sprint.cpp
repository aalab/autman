/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 14, 2015
 */

#include <iostream>
#include <services/data/vision/fetch.h>
#include <services/data/line.h>
#include <thread>

#include "sprint.h"

#include "../compilerdefinitions.h"
#include "../data/sprintfetchresponse.h"
#include "headtracker.h"
#include "../vision.h"

#define MINMAXRATIO(min, max, ratio) (min + ((max - min) * (ratio)))

namespace rdata = services::data::robot;
namespace vdata = services::data::vision;

hurocup::behaviors::Sprint *hurocup::behaviors::Sprint::instance = nullptr;

hurocup::behaviors::Sprint::Sprint(hurocup::Arbitrator &arbitrator)
	: arbitrator(arbitrator), direction(FORWARD), state(NONE)
{
	instance = this;

	if (HeadTracker::instance == nullptr)
	{
		arbitrator.addBehavior(new HeadTracker(arbitrator));
	}
}

void hurocup::behaviors::Sprint::process()
{
	hurocup::data::SprintFetchResponse fetchResponse;
	if (this->arbitrator.vision.fetch(fetchResponse))
	{
		float ratio, overlapAreaX, overlapAreaY;
		int diff;

		SprintState currentSprintState;

		rdata::Move move;
		vdata::VisionObject *left = nullptr, *right = nullptr, *leftCandidate = nullptr;
		for (vdata::VisionObject &lfm : fetchResponse.lefts)
		{
			if (this->shapeFits(lfm))
			{
				if (leftCandidate == nullptr)
					leftCandidate = &lfm;
				for (vdata::VisionObject &rfm : fetchResponse.rights)
				{
					overlapAreaX = hurocup::Vision::overlapHorizontalMax(lfm.boundBox, rfm.boundBox);
					overlapAreaY = hurocup::Vision::overlapVerticalMax(lfm.boundBox, rfm.boundBox);

					ratio = (lfm.boundBox.height() / rfm.boundBox.height());
					diff = (lfm.boundBox.b.x - rfm.boundBox.a.x);
					if (diff < 0)
						diff = -diff;

					if (
						this->shapeFits(rfm)
						/*&& (ratio >= 0.9) && (ratio <= 1.1)
						&& (std::abs(diff) < 5)
						*/
						&& ((overlapAreaX > 0.0) || (diff < 5))
						&& (overlapAreaY > 0.5)
					)
					{
						left = &lfm;
						right = &rfm;

						/*
						result->centerX = result->left->boundBox.bottomRight().x() +
										  (result->left->boundBox.bottomRight().x() -
										   result->right->boundBox.topLeft().x()) / 2.0;
						result->centerY = result->left->boundBox.bbCenterY();
						result->height = (result->left->boundBox.height() + result->right->boundBox.height()) / 2.0;
						VERBOSE("Markers accepted (" << result->centerX << ":" << result->centerY << ")");
						*/
						break;
					}
					else
					{
						if (this->dumpUiData)
						{
							VERBOSE("Skipping RM due to shape compliance (" << overlapAreaX << ":" << overlapAreaY << ")");
						}
					}
				}
			}
			if (left != nullptr)
				break;
		}

		if ((left == nullptr) && (leftCandidate != nullptr))
			left = leftCandidate;

		if (left == nullptr)	// Cound not find the left side, search for the right side
		{
			if (this->dumpUiData)
			{
				VERBOSE("'left' not found, trying for 'right' color.");
			}
			for (vdata::VisionObject &rfm : fetchResponse.rights)
			{
				if (this->shapeFits(rfm))
				{
					right = &rfm;
					break;
				}
				else
				{
					if (this->dumpUiData)
					{
						VERBOSE("Skipping RM due to shape compliance");
					}
				}
			}
		}

		if ((left == nullptr) && (right == nullptr))
		{
			if (this->state == SCANNING)
			{
				int diff = (std::chrono::duration_cast<std::chrono::milliseconds>(
					this->currentTime - this->scanningStart).count());
				if (diff < 3000)    // If it is bigger then 3sec
				{
					// Recalculate the angle using the time (in this case it will take 3 seconds to scan the whole
					// tilt range.
					this->scanningAngle.y = (
						HeadTracker::instance->maxTilt +
						(/* HeadTracker::instance->minTilt */ 0 - HeadTracker::instance->maxTilt) / 3000.0 * (diff)
					);
					VERBOSE("Tilt: " << this->scanningAngle.y);
					rdata::Head headMove(0, this->scanningAngle.y);
					this->arbitrator.move(headMove);
					std::this_thread::sleep_for(std::chrono::milliseconds(120));
				}
				else
				{
					// Reset the variables to rescan everything
					this->scanningAngle.y = HeadTracker::instance->maxTilt;
					rdata::Head headMove(0, this->scanningAngle.y);
					VERBOSE("GOING AROUND! Tilt: " << this->scanningAngle.y);
					this->arbitrator.move(headMove);
					std::this_thread::sleep_for(std::chrono::milliseconds(470));
					this->scanningStart = std::chrono::system_clock::now();
				}
			}
			else if (this->state == NOTFOUND)
			{
				if ((std::chrono::duration_cast<std::chrono::milliseconds>(this->currentTime - this->notFoundStartTime).count()) > 2000)
				{
					this->scanningAngle.y = HeadTracker::instance->maxTilt;
					rdata::Head headMove(0, this->scanningAngle.y);
					this->arbitrator.move(headMove);
					std::this_thread::sleep_for(std::chrono::seconds(1));
					HeadTracker::instance->disable();
					this->state = SCANNING;
					this->scanningStart = std::chrono::system_clock::now();
				}
			}
			else if (this->state != NOTFOUND)
			{
				this->state = NOTFOUND;
				this->notFoundStartTime = this->currentTime;
				HeadTracker::instance->x = HeadTracker::instance->y = -1;
			}

			if (this->dumpUiData)
			{
				VERBOSE("Marker not found.");
			}
		}
		else
		{
			if (!HeadTracker::instance->isEnabled())
				HeadTracker::instance->enable();
			if ((left != nullptr) && (right == nullptr))
			{
				if (this->dumpUiData)
				{
					VERBOSE("LEFT!");
				}
				move.vx = 0.0;
				move.vy = 0.2;
				move.vt = -0.01;
				currentSprintState = ALIGNING;

			}
			else if ((left == nullptr) && (right != nullptr))
			{
				if (this->dumpUiData)
				{
					VERBOSE("RIGHT!");
				}
				move.vx = 0.0;
				move.vy = -0.1;
				move.vt = 0.05;
				currentSprintState = ALIGNING;
			}
			else // if ((left != nullptr) && (right != nullptr))
			{
				VERBOSE("Marker at " << left->boundBox.b.x << ":" << left->boundBox.b.y);

				float
					cameraXMiddle = this->arbitrator.state.camera.x/2.0,
					cameraYMiddle = this->arbitrator.state.camera.y/2.0;

				HeadTracker::instance->x = left->boundBox.b.x;
				HeadTracker::instance->y = left->boundBox.centerPoint().y;

				services::data::Point2D centerDiff(cameraXMiddle - left->boundBox.b.x, cameraYMiddle - left->boundBox.centerPoint().y);

				float
				// minXSpeed = 0.1, maxXSpeed = 0.2,
					minTSpeed = 0.0, maxTSpeed = 0.2,
					minYSpeed = 0.0, maxYSpeed = 0.2,
					minXSpeed = 0.0, maxXSpeed = 0.1,
				// centerDistanceThreshold = (left->boundBox.width() + right->boundBox.width())/2.0;
					centerDistanceThreshold = 40;

				if (this->dumpUiData)
				{
					VERBOSEB("       LEFT: " << left->boundBox.a.x << ":" << left->boundBox.a.y << "   " << left->boundBox.width() << ":" << left->boundBox.height());
					VERBOSEB("      RIGHT: " << right->boundBox.a.x << ":" << right->boundBox.a.y << "   " << right->boundBox.width() << ":" << right->boundBox.height());
					VERBOSEB(" Ratio W->L: " << (left->size/(float)right->size));
					VERBOSEB(" Ratio L->W: " << (right->size/(float)left->size));
					VERBOSEB("CENTER DIFF: " << centerDiff.x << ":" << centerDiff.y);
				}

				if (
					((left->size/(float)right->size) < 0.6)
					|| ((right->size/(float)left->size) < 0.6)
					)															// Need to align with the marker
				{
					if (left->size > right->size)
					{
						if (this->dumpUiData)
						{
							VERBOSE("Side shifiting RIGHT!");
						}
						move.vx = 0.0;
						move.vy = 0.3;
						move.vt = 0.0;
						currentSprintState = ALIGNING;
					}
					else
					{
						if (this->dumpUiData)
						{
							VERBOSE("Side shifiting LEFT!");
						}
						move.vx = 0.0;
						move.vy = -0.3;
						move.vt = 0.0;
						currentSprintState = ALIGNING;
					}
				}
				else											// Front aligned with the marker
				{
					if (centerDiff.x > centerDistanceThreshold)
					{
						if (this->dumpUiData)
						{
							VERBOSE("Turning LEFT!");
						}
						move.vt = -MINMAXRATIO(minYSpeed, maxYSpeed, (centerDiff.x - centerDistanceThreshold)/(cameraXMiddle - centerDistanceThreshold));
						currentSprintState = ALIGNING;
					}
					else if (centerDiff.x < -centerDistanceThreshold)
					{
						if (this->dumpUiData)
						{
							VERBOSE("Turning  RIGHT!");
						}
						move.vt = -MINMAXRATIO(minYSpeed, maxYSpeed, (centerDiff.x - centerDistanceThreshold)/(cameraXMiddle - centerDistanceThreshold));
						currentSprintState = ALIGNING;
					}
					else
					{
						if (this->dumpUiData)
						{
							VERBOSE("Going straight!");
						}

						// if (left->boundBox.height() > ((this->arbitrator.state.camera.y / 3.0) * 2.0))
						if ((this->direction == FORWARD) && (this->arbitrator.state.neckLateral.angle >= 1.0))
						{
							VERBOSE("Inverting sprint!");
							VERBOSE("STOPPING ARASH TO START REVERSE WALKING!");
							this->direction = BACKWARD;
							move.vx = move.vy = move.vt = 0;
							this->arbitrator.move(move);
							std::this_thread::sleep_for(std::chrono::seconds(1));
							move.vx = -0.1;
							this->arbitrator.move(move);
							std::this_thread::sleep_for(std::chrono::seconds(5));
							return;
						}

						if (this->direction == FORWARD)
						{
							if (this->dumpUiData)
							{
								VERBOSE("FORWARD!");
							}
							move.vx = 0.1;
						}
						else
						{
							if (this->dumpUiData)
							{
								VERBOSE("BACKWARD!");
							}
							move.vx = -0.1;
						}
						currentSprintState = WALKING;
					}
				}
			}
		}

		/*
		if (this->dumpUiData)
		{
			VERBOSE("Move command:");
			VERBOSEB("Vx: " << move.vx);
			VERBOSEB("Vy: " << move.vy);
			VERBOSEB("Vt: " << move.vt);
		}
		 */

		if (currentSprintState != this->state)
		{
			rdata::Move m;
			this->arbitrator.move(m);
			std::this_thread::sleep_for(std::chrono::milliseconds(300));
			this->state = currentSprintState;
		}
		if (this->arbitrator.move(move))
		{
			if (this->dumpUiData)
			{
				VERBOSE("Command sent!");
			}
		}
		else
		{
			if (this->dumpUiData)
			{
				ERROR("Error sending move command.");
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(30));
	}
	else
	{
		if (this->dumpUiData)
		{
			ERROR("Cannot fetch data from vision_module.");
		}
	}
}

void hurocup::behaviors::Sprint::disable()
{
	if (this->isEnabled())
	{
		brain::Behavior::disable();

		rdata::Move move;
		this->arbitrator.move(move);

	}
}

bool hurocup::behaviors::Sprint::shapeFits(services::data::vision::VisionObject &object)
{
	return (object.boundBox.width() / (float) object.boundBox.height()) < 5.0;
}

