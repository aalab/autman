/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 28, 2015
 */

#include <brain/base/behavior.h>
#include <services/data/point.h>

#ifndef HUROCUP_HEADTRACKER_H
#define HUROCUP_HEADTRACKER_H

#ifndef override
#define override
#endif

namespace hurocup
{
class Arbitrator;
namespace behaviors
{
class HeadTracker
	: public brain::Behavior
{
public:
	static HeadTracker *instance;

protected:
	Arbitrator &arbitrator;

public:
	HeadTracker(hurocup::Arbitrator &arbitrator);

	volatile double pan;
	volatile double tilt;

	double minTilt;
	double maxTilt;

	volatile int x;
	volatile int y;

	volatile unsigned int tolerance;

	virtual bool isUnique() override;
protected:
	virtual void process() override;
};
}
}

#endif //HUROCUP_HEADTRACKER_H
