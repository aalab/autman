/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date August 06, 2015
 */

#include "obstaclerun.h"

#include <thread>

#include "../hurocuparbitrator.h"
#include "../compilerdefinitions.h"
#include "headtracker.h"
#include "../vision.h"

#include <iostream>

#define OR_ANGLESTEP D2R(3.0)

namespace rdata = services::data::robot;
namespace vdata = services::data::vision;

::hurocup::behaviors::ObstacleRun::ObstacleRun(Arbitrator &arbitrator)
	: arbitrator(arbitrator)
{ }

void ::hurocup::behaviors::ObstacleRun::process()
{
	float scanTilt = 0.8;

	// Stops
	rdata::Move move;
	this->arbitrator.move(move);
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	if (this->scanGlobe(scanTilt))
	{
		for (hurocup::data::GlobeObject &o : this->globe.objects)
		{
			VERBOSE("+ " << R2D(o.boundBox.a.x) << " to " << R2D(o.boundBox.b.x));
		}

		// TODO: Check for left, right or forward

		bool passage = true;
		float angleY = scanTilt + (Vision::cameraAngles.y / 6.0);
		for (float angleX = (D2R(-20)); angleX < D2R(20); angleX += OR_ANGLESTEP)
		{
			if (this->globe.isObstacle(angleX, angleY))
			{
				VERBOSE(R2D(angleX) << " isObstacle");
				passage = false;
				break;
			}
		}

		if (passage)
		{
			VERBOSE("Go FORWARD!");
			move.vx = 0.1;
			move.vy = 0.0;
			move.vt = 0.0;
			this->arbitrator.move(move);
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			move.vx = 0.1;
			move.vy = 0.0;
			move.vt = 0.0;
			this->arbitrator.move(move);
		}
		else
		{
			std::vector<services::data::Point2D> gates;
			float angleSince = this->globe.worldLeftEdge;
			bool found = false;
			unsigned int angleCount = 0;
			int biggerGateIdx = -1;
			float biggerGateAngle = 0;
			for (float angleX = this->globe.worldLeftEdge; angleX < this->globe.worldRightEdge; angleX += OR_ANGLESTEP)
			{
				// VERBOSE("Checking " << R2D(angleX));
				// VERBOSE("if: " << this->globe.isObstacle(angleX, angleY) << " || (" << found << " && " << ((angleX + OR_ANGLESTEP) >= this->globe.worldRightEdge) << ")");
				if (
					this->globe.isObstacle(angleX, angleY)
					|| (
						found
						&& ((angleX + OR_ANGLESTEP) >= this->globe.worldRightEdge)
					)
				)
				{
					if (found)
					{
						gates.emplace_back(angleSince, angleX - OR_ANGLESTEP);
						if ((biggerGateIdx == -1) || ((gates.back().y - gates.back().x) > biggerGateAngle))
						{
							biggerGateAngle = (gates.back().y - gates.back().x);
							biggerGateIdx = gates.size() - 1;
						}
						VERBOSE("Gate from: " << R2D(angleSince) << " to: " << R2D(angleX - OR_ANGLESTEP));
					}
					found = false;
					angleCount = 0;
				}
				else
				{
					if (!found)
					{
						found = true;
						angleSince = angleX;
					}
					angleCount++;
				}
			}
			if (biggerGateIdx > -1)
			{
				VERBOSE("Gate: " << biggerGateIdx << " : " << R2D(biggerGateAngle) << " From: " <<
						R2D(gates[biggerGateIdx].x) << " to: " << R2D(gates[biggerGateIdx].y));
				float middle = gates[biggerGateIdx].x + (gates[biggerGateIdx].y - gates[biggerGateIdx].x) / 2;
				VERBOSE("Angle middle: " << middle);

				if (middle > 0)
				{
					VERBOSE("RIGHT!");
					move.vx = 0.0;
					move.vy = 0.1;
					move.vt = 0.0;
					this->arbitrator.move(move);
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
					move.vx = 0.0;
					move.vy = 0.3;
					move.vt = 0.0;
				}
				else
				{
					VERBOSE("LEFT!");
					move.vx = 0.0;
					move.vy = -0.1;
					move.vt = 0.0;
					this->arbitrator.move(move);
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
					move.vx = 0.0;
					move.vy = -0.3;
					move.vt = 0.0;
				}
				this->arbitrator.move(move);
			}
			else
			{
					move.vx = 0.0;
					move.vy = 0.1;
					move.vt = 0.0;
					this->arbitrator.move(move);
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
					move.vx = 0.0;
					move.vy = 0.3;
					move.vt = 0.0;
					this->arbitrator.move(move);
					VERBOSE("RIGHT!");
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(4000));
	}
	else
	{
		ERROR("Cannot scan.");
	}
}

bool hurocup::behaviors::ObstacleRun::scanGlobe(float angleTilt)
{
	vdata::FetchResponse fetchResponse;

	rdata::Head head(0, angleTilt);

	unsigned int nstep = 0;

	float
		step = Vision::cameraAngles.x - Vision::cameraAngles.x/2.0 /* 50% of angle overlap */,
		start = 0 - step*nstep,
		end = 0 + step*nstep,
		tmpAX, tmpAY, tmpBX, tmpBY;

	bool found;

	this->globe.clear();

	VERBOSE("Globe with " << this->globe.objects.size());

	this->globe.worldLeftEdge = start - (Vision::cameraAngles.x / 2.0);
	this->globe.worldRightEdge = end + (Vision::cameraAngles.x / 2.0);

	for (float angle = start; angle <= end; angle += step)
	{
		head.pan = angle;
		VERBOSE("Changing camera angle to " << R2D(angle));
		if (this->arbitrator.move(head))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(900));
			if (this->arbitrator.vision.fetch(fetchResponse))
			{
				// fetchResponse.objects.clear();

				for (vdata::VisionObject &o : fetchResponse.objects)
				{
					found = false;
					tmpAX = angle + this->coordToAngle(o.boundBox.a.x, Vision::camera.x, Vision::cameraAngles.x);
					tmpAY = head.tilt + this->coordToAngle(o.boundBox.a.y, Vision::camera.y, Vision::cameraAngles.y);
					tmpBX = angle + this->coordToAngle(o.boundBox.b.x, Vision::camera.x, Vision::cameraAngles.x);
					tmpBY = head.tilt + this->coordToAngle(o.boundBox.b.y, Vision::camera.y, Vision::cameraAngles.y);

					for (data::GlobeObject &go : this->globe.objects)
					{
						if (hurocup::Vision::overlapHorizontalMax(tmpAX, tmpBX - tmpAX, go.boundBox.a.x, go.boundBox.width()) > 0)
						{
							go.boundBox.a.x = std::min(
								go.boundBox.a.x,
								static_cast<double>(tmpAX)
							);
							go.boundBox.a.y = std::min(
								go.boundBox.a.y,
								static_cast<double>(tmpAY)
							);
							go.boundBox.b.x = std::max(
								go.boundBox.b.x,
								static_cast<double>(tmpBX)
							);
							go.boundBox.b.y = std::max(
								go.boundBox.b.y,
								static_cast<double>(tmpBY)
							);

							this->globe.leftEdge = std::min(this->globe.leftEdge, tmpAX);
							this->globe.rightEdge = std::max(this->globe.rightEdge, tmpBX);

							found = true;
							break;
						}
					}
					if (!found)
					{
						if (this->globe.objects.empty())
						{
							this->globe.leftEdge = tmpAX;
							this->globe.rightEdge = tmpBX;
						}
						else
						{
							this->globe.leftEdge = std::min(this->globe.leftEdge, tmpAX);
							this->globe.rightEdge = std::max(this->globe.rightEdge, tmpBX);
						}
						this->globe.objects.emplace_back(o);

						this->globe.objects.back().boundBox.a.x = tmpAX;
						this->globe.objects.back().boundBox.a.y = tmpAY;

						this->globe.objects.back().boundBox.b.x = tmpBX;
						this->globe.objects.back().boundBox.b.y = tmpBY;
					}
				}
			}
			else
			{
				ERROR("Error fetching data from vision module.");
				return false;
			}
		}
		else
		{
			ERROR("Error moving the head.");
			return false;
		}
	}

	head.pan = 0;
	head.tilt = 0.3;
	this->arbitrator.move(head);

	std::this_thread::sleep_for(std::chrono::milliseconds(500));

	return true;
}

float hurocup::behaviors::ObstacleRun::coordToAngle(float coord, float coordLength, float angleLength)
{
	return (angleLength / coordLength)*(coord - (coordLength/2.0));
}
