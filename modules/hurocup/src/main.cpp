
#include "config.h"

#include "compilerdefinitions.h"

#include "hurocuparbitrator.h"
#include "arashrobotstate.h"

#include "behaviors/sprint.h"
#include "behaviors/headtracker.h"
#include "behaviors/obstaclerun.h"
#include "behaviors/marathon.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <services/robot.h>
#include <services/vision.h>
#include <services/gyro.h>

hurocup::Arbitrator* sigHandlerArbitratorPointer;

void endSignalHandler(int signalCode)
{
	sigHandlerArbitratorPointer->terminate();
}

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string configFile, modality;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("modality,m", po::value<std::string>(&modality), "Modality of the competition.")
		("version,v", "print version string.")
		("help,h", "print help message.")
		("config_file,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "robot config file.");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "RoboCup Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	auto ptRobot = pt.get_child_optional("robot");
	if (!ptRobot)
	{
		ERROR("'robot' configuration was not found.");
		return 2;
	}
	auto ptGyro = pt.get_child_optional("gyro");
	if (!ptGyro)
	{
		ERROR("'gyro' configuration was not found.");
		return 3;
	}
	auto ptVision = pt.get_child_optional("vision");
	if (!ptVision)
	{
		ERROR("'vision' configuration was not found.");
		return 4;
	}

	VERBOSE("Robot service");
	services::Robot robotService((*ptRobot).get<std::string>("robot_name", "arash"), (*ptRobot).get<std::string>("address", "localhost"), (*ptRobot).get<unsigned int>("port", 1313));
	VERBOSE("Gyro service");
	services::Gyro gyroService((*ptGyro).get<std::string>("address", "localhost"), (*ptGyro).get<unsigned int>("port", 1414));
	VERBOSE("Vision service");
	services::Vision visionService((*ptVision).get<std::string>("address", "localhost"), (*ptVision).get<unsigned int>("port", 1515));

	visionService.updateCameraCalibrations();

	hurocup::Arbitrator arbitrator(visionService, robotService);

	services::data::robot::FullMove t (0, 0, 0, 0, 0, 1.3);
	arbitrator.move(t);
	sleep(2);

	sigHandlerArbitratorPointer = &arbitrator;	// Work around
	if (modality == "sprint")
	{
		arbitrator.addBehavior(new hurocup::behaviors::Sprint(arbitrator));
	}
	else if (modality == "obstaclerun")
	{
		arbitrator.addBehavior(new hurocup::behaviors::ObstacleRun(arbitrator));
	}
	else if (modality == "marathon")
	{
		arbitrator.addBehavior(new hurocup::behaviors::Marathon(arbitrator));
	}
	else
	{
		ERROR("Invalid modality.");
	}

	if (arbitrator.empty())
	{
		ERROR("No modality.");
	}
	else
	{
		struct sigaction sigIntHandler;

		sigIntHandler.sa_handler = endSignalHandler;
		sigemptyset(&sigIntHandler.sa_mask);
		sigIntHandler.sa_flags = 0;

		sigaction(SIGINT, &sigIntHandler, NULL);
		sigaction(SIGTERM, &sigIntHandler, NULL);

		arbitrator.start(true);
	}

	robotService.move(t);

	return 0;
}
