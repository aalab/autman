/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 31, 2015
 */

#ifndef HUROCUP_VISION_H
#define HUROCUP_VISION_H

#include <services/data/boundbox.h>

namespace hurocup
{
class Vision
{
public:
	static services::data::Point2D camera;
	static services::data::Point2D cameraAngles;

	static double overlapArea(
		double x1, double y1, double width1, double height1,
		double x2, double y2, double width2, double height2
	);

	static double overlapArea(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	/**
	 * Returns the overlap factor based on the bigger object.
	 */
	static double overlapMax(
		double x1, double y1, double width1, double height1,
		double x2, double y2, double width2, double height2
	);

	static double overlapMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	/**
	 * Returns the overlap factor based on the smaller object.
	 */
	static double overlapMin(
		double x1, double y1, double width1, double height1,
		double x2, double y2, double width2, double height2
	);

	static double overlapMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	static double overlapAreaHorizontal(double x1, double width1, double x2, double width2);

	static double overlapHorizontalMax(double x1, double width1, double x2, double width2);

	static double overlapHorizontalMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	static double overlapHorizontalMin(double x1, double width1, double x2, double width2);

	static double overlapHorizontalMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	static double overlapAreaVertical(double x1, double height1, double y2, double height2);

	static double overlapVerticalMax(double x1, double height1, double y2, double height2);

	static double overlapVerticalMax(services::data::BoundBox2D &a, services::data::BoundBox2D &b);

	static double overlapVerticalMin(double x1, double height1, double y2, double height2);

	static double overlapVerticalMin(services::data::BoundBox2D &a, services::data::BoundBox2D &b);
};
}


#endif //HUROCUP_VISION_H
