/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 22, 2015
 **/

#include "server.h"
#include "compilerdefinitions.h"
#include "integral.hpp"
#include "servocontroller.hpp"

#include <iostream>
#include <boost/concept_check.hpp>

//#define JUST_HEAD

puppet::Server::Server(boost::asio::io_service &io_service, unsigned int port, services::Robot &master,
	services::Robot &slave)
	: master(master), slave(slave), UDPServer(io_service, port), runningMotionThread(true)
{
	this->motionThread.reset(new std::thread(&Server::runMotionThreadTrampolin, this));
}

void puppet::Server::processCommand(Json::Value &jsonCommand)
{
	std::string commandName(jsonCommand["command"].asString());

	if (commandName == "inputstate")
	{
		//
	}
	else
	{
		ERROR("Unknown command \"" << commandName << "\"");
		this->send("{\"success\":false,\"message\":\"Unknown command.\"}");
	}
}

void puppet::Server::runMotionThreadTrampolin()
{
	rdata::ReadState readState, slaveReadState;
	rdata::ReadStateResponse response, slaveResponse;

	ServoChain
		neckChain,
		rightArmChain,
		leftArmChain,
		restChain;

	if (this->master.readState(readState, response))
	{
		if (this->slave.readState(slaveReadState, slaveResponse))
		{
			std::chrono::system_clock::time_point
					lastTime = std::chrono::system_clock::now(),
					ll = lastTime,
					lastSlaveUpdate = lastTime,
					now;

			std::unique_ptr<ServoController> controllers[response.states.size()];

			VERBOSE(response.states.size() << " servos found.");

			rdata::Actuator initialPosition;
			initialPosition.speed = 100;
			for (std::pair<std::string, rdata::ServoState*> s : slaveResponse.states)
			{
				if (
					(s.second->name == "NeckLateral")
					|| (s.second->name == "NeckTransversal")
#ifndef JUST_HEAD
					|| (s.second->name == "RightShoulderLateral")
					|| (s.second->name == "LeftShoulderLateral")
					|| (s.second->name == "RightShoulderFrontal")
					|| (s.second->name == "LeftShoulderFrontal")
					|| (s.second->name == "RightElbowLateral")
					|| (s.second->name == "LeftElbowLateral")
#endif
				)
				{
					initialPosition.name = s.second->name;
					if (
							(s.first == "NeckLateral")
							|| (s.first == "LeftShoulderFrontal")
							)
						initialPosition.angle = -s.second->angle;
					else
						initialPosition.angle = s.second->angle;
					this->master.actuator(initialPosition);
				}
			}
			VERBOSE(3);
			sleep(1);
			VERBOSE(2);
			sleep(1);
			VERBOSE(1);
			sleep(1);

			{
				ServoChain *chain;
				unsigned int i = 0;
				for (std::pair<std::string, rdata::ServoState *> state : response.states)
				{
					VERBOSE("Creating " << state.first << ": " << state.second->angle);

					if (
							(state.first == "NeckLateral")
							|| (state.first == "NeckTransversal")
							)
					{
						chain = &neckChain;
					}
					else if (
							(state.first == "RightShoulderLateral")
							|| (state.first == "RightShoulderFrontal")
							|| (state.first == "RightElbowLateral")
							)
					{
						chain = &rightArmChain;
					}
					else if (
							(state.first == "RightShoulderLateral")
							|| (state.first == "RightShoulderFrontal")
							|| (state.first == "RightElbowLateral")
							)
					{
						chain = &leftArmChain;
					}
					else
						chain = &restChain;

					controllers[i].reset(new ServoController(this->master, state.first, state.second, chain));
					rdata::Actuator actuator(state.first, state.second->angle);
					actuator.mode = rdata::ActuatorMode::OFF;
					/*
					rdata::Actuator actuator(state.first, state.second->angle);
					actuator.mode = rdata::ActuatorMode::ON;
					actuator.gainP = 32;
					actuator.gainI = 0;
					actuator.gainD = 0;
					*/
					this->master.actuator(actuator);
					i++;
				}
			}

			while (this->runningMotionThread)
			{
				now = std::chrono::system_clock::now();
				if (this->master.readState(readState, response))
				{
					for (std::unique_ptr<ServoController> &servoController : controllers)
					{
						if (
								(servoController->name() == "NeckLateral")
								|| (servoController->name() == "NeckTransversal")
#ifndef JUST_HEAD
								|| (servoController->name() == "RightShoulderLateral")
								|| (servoController->name() == "LeftShoulderLateral")
								|| (servoController->name() == "RightShoulderFrontal")
								|| (servoController->name() == "LeftShoulderFrontal")
								|| (servoController->name() == "RightElbowLateral")
								|| (servoController->name() == "LeftElbowLateral")
#endif
							)
							servoController->update();
					}
				}
				else
				{
					ERROR("Cannot read state from server.");
				}
				if (std::chrono::duration_cast<std::chrono::milliseconds>(now - lastSlaveUpdate).count() > 1000)
				{
					std::vector<std::unique_ptr<rdata::BaseCommand>> updateSlaves;
					services::data::robot::Actuator *updateSlave;
					for (std::unique_ptr<ServoController> &servoController : controllers)
					{
						if (
								(servoController->name() == "NeckLateral")
								|| (servoController->name() == "NeckTransversal")
#ifndef JUST_HEAD
								|| (servoController->name() == "RightShoulderLateral")
								|| (servoController->name() == "LeftShoulderLateral")
								|| (servoController->name() == "RightShoulderFrontal")
								|| (servoController->name() == "LeftShoulderFrontal")
								|| (servoController->name() == "RightElbowLateral")
								|| (servoController->name() == "LeftElbowLateral")
#endif
								)
						{
							rdata::Actuator updateSlave(servoController->name(), servoController->servoState()->angle);
							VERBOSE("Updating servo: " << servoController->name() << " to " <<
									servoController->servoState()->angle);
							updateSlave.name = servoController->name();
							if (
									(servoController->name() == "NeckLateral")
									|| (servoController->name() == "LeftShoulderFrontal")
									)
								updateSlave.angle = -servoController->servoState()->angle;
							else
								updateSlave.angle = servoController->servoState()->angle;
							updateSlave.speed = 70;
							updateSlave.gainP = 32;
							updateSlave.gainI = 0;
							updateSlave.gainD = 0;

							this->slave.actuator(updateSlave);
						}
					}
					lastSlaveUpdate = now;
				}
				usleep(10000);
			}
		}
		else
		{
			ERROR("Cannot read initial slave servo states.");
		}
	}
	else
	{
		ERROR("Cannot read initial servo states.");
	}
}

puppet::Server::~Server()
{
	if (this->motionThread)
	{
		this->runningMotionThread = false;
		if (this->motionThread->joinable())
			this->motionThread->join();
	}
}
