/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 24, 2015
 **/

#include <iostream>
#include "servocontroller.hpp"
#include "compilerdefinitions.h"

puppet::ServoController::ServoController(services::Robot &robot, const std::string &name, services::data::robot::ServoState *state,
	ServoChain *chain)
	: robot(robot), state(State::LOCKED), _name(name), _servoState(state), ii(20), dii(20), chain(chain)
{
	this->chain->chain.emplace_back(this);
}

void puppet::ServoController::update()
{
	this->now = std::chrono::system_clock::now();

	this->ii.add(this->_servoState->load, this->_servoState->angle);
	this->dii.add((this->ii.time() / this->ii.size()), (this->ii.number() / this->ii.size()));

	/*
	if (this->state == State::LOCKED)
	{
		// VERBOSE(this->_name << " LOCKED " << std::abs((this->ii.time() / this->ii.size()) - (this->dii.time() / this->dii.size())) << " > 3?");
		if (std::abs((this->ii.time() / this->ii.size()) - (this->dii.time() / this->dii.size())) > 3)
		{
			this->chain->turnOff();
			this->state = State::UNLOCKED;
		}
	}
	else
	{
		VERBOSE(this->_name << ((this->ii.number() / this->ii.size()) - (this->dii.number() / this->dii.size())) << " < " << D2R(5) << "?");
		if (std::abs((this->ii.number() / this->ii.size()) - (this->dii.number() / this->dii.size())) < D2R(5))
		{
			if (this->state != State::STATIONARY)
			{
				this->stationaryStart = std::chrono::system_clock::now();
				this->state = State::STATIONARY;
			}
		}
		else
		{
			this->state = State::UNLOCKED;
		}

		if (this->state == State::STATIONARY)
		{
			// VERBOSE(this->_name << " STATIONARY " << std::chrono::duration_cast<std::chrono::milliseconds>(now - this->stationaryStart).count() << " > 300?");
			if (std::chrono::duration_cast<std::chrono::milliseconds>(now - this->stationaryStart).count() > 300)
			{
				this->state = State::LOCKED;
				this->chain->turnOn();
			}
		}
	}
	*/

	this->lastTime = this->now;
}

const std::string &puppet::ServoController::name()
{
	return this->_name;
}

services::data::robot::ServoState *puppet::ServoController::servoState()
{
	return this->_servoState;
}

void puppet::ServoChain::turnOff()
{
	for (ServoController *sc : this->chain)
	{
		sc->turnOff();
	}
}

void puppet::ServoChain::turnOn()
{
	for (ServoController *sc : this->chain)
	{
		sc->turnOn();
	}
}

void puppet::ServoController::turnOn()
{
	rdata::Actuator actuator(this->_name);
	actuator.mode = rdata::ActuatorMode::ON;
	this->robot.actuator(actuator);
}

void puppet::ServoController::turnOff()
{
	rdata::Actuator actuator(this->_name);
	actuator.mode = rdata::ActuatorMode::OFF;
	this->robot.actuator(actuator);
}
