
#include <config.h>

#include "compilerdefinitions.h"
#include "server.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <services/robot.h>

using namespace std;

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("config,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "puppet configuration file");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	auto ptPuppet = pt.get_child_optional("puppet");
	if (!ptPuppet)
	{
		ERROR("'puppet' configuration was not found.");
		return 2;
	}

	auto ptRobotMaster = pt.get_child_optional("master");
	if (!ptRobotMaster)
	{
		ERROR("'master' configuration was not found.");
		return 3;
	}

	auto ptRobotSlave = pt.get_child_optional("slave");
	if (!ptRobotSlave)
	{
		ERROR("'robot' configuration was not found.");
		return 3;
	}

	VERBOSE("Initializing master robotService: " << (*ptRobotMaster).get<std::string>("address", "127.0.0.1") << ":" << (*ptRobotMaster).get < unsigned int > ("port"));
	services::Robot robotMasterService(
			(*ptRobotMaster).get<std::string>("robot_name", "arash"), (*ptRobotMaster).get<std::string>("address", "127.0.0.1"),
	(*ptRobotMaster).get < unsigned int > ("port")
	);

	VERBOSE("Initializing slave robotService: " << (*ptRobotSlave).get<std::string>("address", "127.0.0.1") << ":" << (*ptRobotSlave).get < unsigned int > ("port"));
	services::Robot robotSlaveService(
			(*ptRobotSlave).get<std::string>("robot_name", "arash"), (*ptRobotSlave).get<std::string>("address", "127.0.0.1"),
	(*ptRobotSlave).get < unsigned int > ("port")
	);

	VERBOSE("Initializing server...");
	boost::asio::io_service ioService;
	puppet::Server server(ioService, (*ptPuppet).get < unsigned int > ("port", 1717), robotMasterService, robotSlaveService);
	VERBOSE("Starting server...");
	try
	{
		server.start();
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 4;
	}

	return 0;
}
