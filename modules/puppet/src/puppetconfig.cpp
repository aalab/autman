/**
 * @author J. Santos <jamillo@gmail.com>
 * @date September 15, 2015
 **/

#include "puppetconfig.h"

const float puppet::Config::speedXMin() const
{
	return this->_speedXMin;
}

puppet::Config &puppet::Config::speedXMin(float speedXMin)
{
	this->_speedXMin = speedXMin;
	return *this;
}

const float puppet::Config::speedXMax() const
{
	return this->_speedXMax;
}

puppet::Config &puppet::Config::speedXMax(float speedXMax)
{
	this->_speedXMax = speedXMax;
	return *this;
}

const float puppet::Config::speedYMin() const
{
	return this->_speedYMin;
}

puppet::Config &puppet::Config::speedYMin(float speedYMin)
{
	this->_speedYMin = speedYMin;
	return *this;
}

const float puppet::Config::speedYMax() const
{
	return this->_speedYMax;
}

puppet::Config &puppet::Config::speedYMax(float speedYMax)
{
	this->_speedYMax = speedYMax;
	return *this;
}

const float puppet::Config::speedThetaMin() const
{
	return this->_speedThetaMin;
}

puppet::Config &puppet::Config::speedThetaMin(float speedThetaMin)
{
	this->_speedThetaMin = speedThetaMin;
	return *this;
}

const float puppet::Config::speedThetaMax() const
{
	return this->_speedThetaMax;
}

puppet::Config &puppet::Config::speedThetaMax(float speedThetaMax)
{
	this->_speedThetaMax = speedThetaMax;
	return *this;
}

const float puppet::Config::headPanMax() const
{
	return this->_headPanMax;
}

puppet::Config &puppet::Config::headPanMax(float headPanMax)
{
	this->_headPanMax = headPanMax;
	return *this;
}

const float puppet::Config::headPanMin() const
{
	return this->_headPanMin;
}

puppet::Config &puppet::Config::headPanMin(float headPanMin)
{
	this->_headPanMin = headPanMin;
	return *this;
}

const float puppet::Config::headTiltMax() const
{
	return this->_headTiltMax;
}

puppet::Config &puppet::Config::headTiltMax(float headTiltMax)
{
	this->_headTiltMax = headTiltMax;
	return *this;
}

const float puppet::Config::headTiltMin() const
{
	return this->_headTiltMin;
}

puppet::Config &puppet::Config::headTiltMin(float headTiltMin)
{
	this->_headTiltMin = headTiltMin;
	return *this;
}
