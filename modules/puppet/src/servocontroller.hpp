/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 24, 2015
 **/

#ifndef PUPPET_SERVOCONTROLLER_HPP
#define PUPPET_SERVOCONTROLLER_HPP

#include <string>
#include <services/data/robot/readstate.h>
#include <services/robot.h>
#include "integral.hpp"

namespace puppet
{
namespace rdata = services::data::robot;

class ServoController;

class ServoChain
{
public:
	std::vector<ServoController*> chain;

	void turnOff();
	void turnOn();
};

class ServoController
{
	enum class State
	{
		LOCKED,
		UNLOCKED,
		STATIONARY
	};
private:
	services::Robot &robot;

	State state;

	std::string _name;
	rdata::ServoState lastState;
	rdata::ServoState *_servoState;

	Integral ii;
	Integral dii;

	std::chrono::system_clock::time_point now;
	std::chrono::system_clock::time_point lastTime;

	std::chrono::system_clock::time_point stationaryStart;
public:
	ServoController(services::Robot &robot, const std::string &name, services::data::robot::ServoState *state, ServoChain *chain);
	void update();

	const std::string &name();

	rdata::ServoState *servoState();

	ServoChain *chain;

	void turnOn();

	void turnOff();
};
}

#endif //PUPPET_SERVOCONTROLLER_HPP
