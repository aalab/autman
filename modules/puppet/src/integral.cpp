/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 22, 2015
 **/

#include "integral.hpp"

Integral::Integral(unsigned int capacity)
	: cacheTime(0), cacheNumber(0), start(0), capacity(capacity), _size(0), items(capacity)
{ }

void Integral::add(double time, double number)
{
	if (this->_size < this->capacity)
	{
		this->_size++;
		this->items[this->start].first = time;
		this->items[this->start].second = number;
		this->cacheTime += time;
		this->cacheNumber += number;
	}
	else
	{
		this->cacheTime -= this->items[this->start].first;
		this->cacheNumber -= this->items[this->start].second;
		this->items[start].first = time;
		this->items[start].second = number;
		this->cacheTime += time;
		this->cacheNumber += number;
	}
	this->start = (this->start + 1) % this->capacity;
}

double Integral::time()
{
	return this->cacheTime;
}

double Integral::number()
{
	return this->cacheNumber;
}

unsigned int Integral::size()
{
	return this->_size;
}

std::pair<double, double> &Integral::get(unsigned int i)
{
	return this->items[i];
}
