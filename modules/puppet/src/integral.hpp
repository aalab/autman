/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 22, 2015
 **/

#ifndef PUPPET_INTEGRAL_HPP
#define PUPPET_INTEGRAL_HPP

#include <vector>

class Integral
{
private:
	double cacheTime;
	double cacheNumber;
	unsigned int start;
	unsigned int capacity;
	unsigned int _size;

	std::vector<std::pair<double, double>> items;
public:
	Integral(unsigned int capacity);

	void add(double time, double number);

	double time();

	double number();

	unsigned int size();

	std::pair<double, double> &get(unsigned int i);
};

#endif //PUPPET_INTEGRAL_HPP
