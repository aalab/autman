/**
 * @author J. Santos <jamillo@gmail.com>
 * @date October 22, 2015
 **/

#ifndef TELEOP_SERVER_H
#define TELEOP_SERVER_H

#include <services/base/udpserver.h>
#include <services/data/teleop/playstation.h>
#include <services/data/teleop/xbox.h>
#include <services/data/teleop/keyboard.h>
#include <services/data/teleop/touch.h>
#include <services/robot.h>
#include <thread>

namespace rdata = services::data::robot;

namespace puppet
{
class Server
	: public services::base::UDPServer
{
private:
	bool runningMotionThread;
	services::Robot &master;
	services::Robot &slave;
	std::unique_ptr<std::thread> motionThread;

	std::chrono::system_clock::time_point headLastTime;
public:
	Server(boost::asio::io_service &io_service, unsigned int port, services::Robot &master, services::Robot &slave);
	~Server();

	void runMotionThreadTrampolin();

protected:
	virtual void processCommand(Json::Value &jsonCommand) override;
};
}


#endif //TELEOP_SERVER_H
