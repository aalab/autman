
#include <config.h>

#include "compilerdefinitions.h"
#include "server.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <services/robot.h>

using namespace std;

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string configFile;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("version,v", "print version string")
		("help,h", "print help message")
		("config,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "teleop configuration file");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	auto ptTeleop = pt.get_child_optional("teleop");
	if (!ptTeleop)
	{
		ERROR("'teleop' configuration was not found.");
		return 2;
	}

	auto ptRobot = pt.get_child_optional("robot");
	if (!ptRobot)
	{
		ERROR("'robot' configuration was not found.");
		return 3;
	}

	VERBOSE("Initializing robotService: " << (*ptRobot).get<std::string>("address", "127.0.0.1") << ":" << (*ptRobot).get<unsigned int>("port"));
	services::Robot robotService(
		(*ptRobot).get<std::string>("robot_name", "arash"), (*ptRobot).get<std::string>("address", "127.0.0.1"),
		(*ptRobot).get<unsigned int>("port")
	);

	VERBOSE("Initializing server...");
	boost::asio::io_service ioService;
	teleop::Server server(ioService, (*ptTeleop).get<unsigned int>("port", 1616), robotService);
	VERBOSE("Starting server...");
	try
	{
		server.start();
	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 4;
	}

	return 0;
}
