
#include "config.h"

#include <iostream>

#include "udpgyroserver.h"
#include "serial.h"
#include "compilerdefinitions.h"

#include <fstream>

#include <boost/program_options.hpp>
#include <boost/property_tree/info_parser.hpp>

using namespace std;

namespace po = boost::program_options;

int
main(int argc, char *argv[])
{
	namespace po = boost::program_options;
	std::string
		configFile,
		configDevice,
		configBaudrate;
	unsigned int configUdpPort;

#ifndef NDEBUG
	std::cout << "Debugging build" << std::endl;
#endif

	po::options_description commandLineOnlyOptions("Command Line Options");

	commandLineOnlyOptions.add_options()
		("device,d", "USB device")
		("udp,u", "UDP Server")
		("baudrate,b", "Baudrate")
		("version,v", "print version string")
		("help,h", "print help message")
		("config_file,c", po::value<std::string>(&configFile)->default_value(DEFAULT_CONFIG_FILE), "robot config file");

	using boost::property_tree::ptree;
	ptree pt;

	try
	{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(commandLineOnlyOptions).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			std::cout << commandLineOnlyOptions << "\n";
			return 0;
		}

		if (vm.count("version"))
		{
			std::cout << "Robot Server Version:" << VERSION_MAJOR << "." << VERSION_MINOR << std::endl;
			return 0;
		}

		if (vm.count("baudrate") && vm.count("device") && vm.count("udp"))
		{
			VERBOSE("Ignoring configuration file.");
			pt.put("device", configDevice);
			pt.put("baudrate", configBaudrate);
			pt.put("udp", configUdpPort);
		}
		else
		{
			std::string configPath = std::string(CONFIG_DIR) + std::string("/") + configFile;
			VERBOSE("Loading config file " << configFile << " config path " << configPath);
			read_info(configPath, pt);
		}

	}
	catch (std::exception &e)
	{
		std::cerr << "boost::po exception " << e.what() << std::endl;
		return 1;
	}

	Serial *serial = new Serial();

	serial->deviceName(pt.get<std::string>("device"));
	if (serial->baudrate(pt.get<std::string>("baudrate")))
	{
		if (serial->open())
		{
			if ((serial == nullptr) || (!serial->isValid()))
			{
				serial = nullptr;
			}
			else
			{
				boost::asio::io_service io_service;
				devices::ImuSensor imuSensor(serial);
				UDPGyroServer *gs = new UDPGyroServer(pt, io_service, &imuSensor);
				gs->StartServer();   // Does not return right now
				delete gs;
			}
		}
		else
		{
			ERROR("Failed opening port: " << serial->deviceName() << ".");
		}
	}
	else
	{
		ERROR("Failed setting the baudrate.");
	}
}

