//-----------------------------------------
// PROJECT: MotionController
// AUTHOR : POSIX - Jacky Baltes <jacky@cs.umanitoba.ca>
//          Windows - Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------
#ifndef __SERIAL_HPP__
#define __SERIAL_HPP__


#ifdef _WIN32
#include <windows.h>
#else

#include <termios.h>
#include <cinttypes>
#include <mutex>

#endif


//------------------------------------------------------------------------------
// CONSTANTS / TYPES
//------------------------------------------------------------------------------
#define SERIAL_MAX_DEVNAME 32
#ifdef _WIN32
typedef DWORD BAUDRATE_TYPE;
#else
typedef speed_t BAUDRATE_TYPE;
#endif

class SerialLockGuard;

//------------------------------------------------------------------------------
// PROTOTYPES
//------------------------------------------------------------------------------
class Serial
{
	friend class SerialLockGuard;
private:
	BAUDRATE_TYPE _baudrateSpeed; // baudrate

#ifdef _WIN32
    DCB _olddcb;
    HANDLE _hCommPort;
#else

	struct termios _oldtio;
	int _fd;

#endif

	bool _opened;
	std::mutex _mutex;

	std::string _device;
	std::string _baudrate;

	termios _currentTIO;
public:
	Serial();

	virtual ~Serial(void);

	const std::string &baudrate() const;

	bool baudrate(std::string str);

	const std::string deviceName() const;

	Serial& deviceName(std::string deviceName);

	/**
	 * Close opened serial port.
	 */
	virtual int close();

	/**
	 * Open serial port without lock.
	 *
	 * @param devname Device name to use.
	 * @param baudrate Baudrate of serial port.
	 *
	 * @return True, if succeeded; otherwise, false.
	 */
	virtual bool open();

	/**
	 * Flushes both data received but not read and data written but not transmitted
	 *
	 * @see flushRx
	 * @see flushTx
	 */
	virtual void flush(void);

	virtual void flushRx(void);

	virtual void flushTx(void);

	/**
	 * Check if this serial is valid
	 *
	 * @return True, if serial is valid; otherwise, false
	 */
	virtual bool isValid(void) const;

	/**
	 * Read character from serial port
	 *
	 * @param pChar Pointer to character to receive the result
	 * @return True, if succeeded; otherwise, false
	 */
	virtual bool readChar(char *pChar);

	/**
	 * Reads data with lock.
	 *
	 * @see readData
	 */
	virtual int readDataWithLock(uint8_t *data, unsigned int length, unsigned int timeout = 50);

	/**
	 * Read data from serial port.
	 *
	 * @param data Buffer where the data will be stored.
	 * @param length Length of the buffer.
	 * @param timeout Timeout of reading in milliseconds.
	 *
	 * @return Number of received bytes.
	 */
	virtual int readData(uint8_t *data, unsigned int length, unsigned int timeout = 50);

	/**
	 * Write character onto serial port.
	 *
	 * @param ch Character to write.
	 *
	 * @return True, if succeeded; otherwise, false.
	 */
	virtual bool writeChar(char ch);

	/**
	 * Write data onto serial port with lock.
	 *
	 * @param data Bytes to write.
	 * @param length Length of the array.
	 *
	 * @return Number of written bytes.
	 *
	 * @see writeData
	 */
	virtual int writeDataWithLock(uint8_t const *data, unsigned int length);

	/**
	 * Write data onto serial port slowly.
	 *
	 * @param data Bytes to write.
	 * @param length Length of the array.
	 * @param milliseconds Milliseconds delay between each bytes.
	 *
	 * @return Number of written bytes.
	 */
	virtual int writeSlowly(const char *data, int length, int milliseconds = 10);

	/**
	 * Write data onto serial port.
	 *
	 * @param data Bytes to write.
	 * @param length Length of the array.
	 *
	 * @return Number of written bytes.
	 */
	virtual int writeData(uint8_t const *data, unsigned int length);
protected:
	/**
	 * Locks the serial port.
	 *
	 * @see _mutex
	 */
	virtual void lock();

	/**
	 * Unlocks the serial port.
	 *
	 * @see _mutex
	 */
	virtual void unlock();
public:
	/**
	 * Convert baudrate speed into string.
	 *
	 * @param speed Speed to convert.
	 *
	 * @return Convert string.
	 */
	static std::string strBaudrate(BAUDRATE_TYPE baudrateSpeed);
};

class SerialLockGuard
{
private:
	Serial& _serial;
public:
	SerialLockGuard(Serial &serial);
	~SerialLockGuard();
};

#endif
