/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 11, 2015
 */

#include <iostream>
#include "imusensor.h"

#include <boost/io/ios_state.hpp>
#include <iomanip>

#include "compilerdefinitions.h"

uint8_t devices::ImuSensor::commandCalibrate = 0x01;
uint8_t devices::ImuSensor::commandData = 0x02;

devices::ImuSensor::ImuSensor(Serial *port)
	: port(port)
{ }

bool devices::ImuSensor::calibrate()
{
	SerialLockGuard locker(*this->port);
	int ret = this->port->writeData(&commandCalibrate, 1);
	VERBOSE(ret << " bytes written");
	if (ret > 0)
	{
		sleep(1);			// Waiting for calibration
		VERBOSE("Reading " << sizeof(Header) << " bytes");
		Header header;
		ret = this->port->readData((uint8_t*)&header, sizeof(Header));
		VERBOSE("Read return " << ret);
		if (ret == sizeof(Header))
		{
			VERBOSENEL("Data [");
			VERBOSEDATA((uint8_t*)&header, sizeof(Header));
			VERBOSEB("]");
			return (header.header == Protocol::FULL_HEADER) && (header.command == Protocol::CMD_CALIBRATE);
		}
		VERBOSE("Cannot read " << ret << ".");
	}
	return false;
}

bool devices::ImuSensor::data(devices::DataPacket &data)
{
	SerialLockGuard locker(*this->port);
	int ret = this->port->writeData(&commandData, 1);
	VERBOSE(ret << " bytes written");
	if (ret > 0)
	{
		VERBOSE("Reading " << sizeof(DataPacket) << " bytes");
		ret = this->port->readData((uint8_t*)&data, sizeof(DataPacket));
		VERBOSE("Read return " << ret);
		if (ret == sizeof(DataPacket))
		{
			VERBOSENEL("Data [");
			VERBOSEDATA((uint8_t*)&data, sizeof(DataPacket));
			VERBOSEB("]");
			return (data.header.header == Protocol::FULL_HEADER) && (data.header.command == Protocol::CMD_DATA);
		}
		else
		{
			VERBOSE("Cannot read. Expected: " << sizeof(DataPacket) << "  Got: " << ret << ".");
			VERBOSENEL("Data [");
			VERBOSEDATA((uint8_t*)&data, ret);
			VERBOSEB("]");
		}
	}
	return false;
}

Json::Value &::devices::operator<<(Json::Value &result, devices::Int32_3 &data)
{
	result["x"] = data.x/1000.0;
	result["y"] = data.y/1000.0;
	result["z"] = data.z/1000.0;
	return result;
}
