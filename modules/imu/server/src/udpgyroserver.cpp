#include "udpgyroserver.h"
#include "compilerdefinitions.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <ctime>
#include <iostream>
#include <syslog.h>
#include <unistd.h>
#include <string>
#include <json/value.h>

#include "json.hpp"

using boost::asio::ip::udp;

UDPGyroServer::UDPGyroServer(
	boost::property_tree::ptree &pt, boost::asio::io_service &io_service, devices::ImuSensor *sensor
) : socket(io_service, udp::endpoint(udp::v4(), pt.get<unsigned int>("port", 1414))), sensor(sensor)
{
	VERBOSE("Listening port " << pt.get<unsigned int>("port", 1414));
	state = UPRIGHT;
}

void
UDPGyroServer::StartServer()
{
	char commandBuffer[4048];
	size_t commandBufferMaxLength = sizeof(commandBuffer);

	std::string command;
	for (; ;)
	{
#ifdef DEBUG
		std::cout << "Waiting command ..." << std::endl;
#endif
		size_t commandLength = this->socket.receive_from(boost::asio::buffer(commandBuffer, commandBufferMaxLength),
														 this->remoteEP);
		std::string commandString = std::string((const char *) commandBuffer, commandLength);
		std::string resp;
		std::cout << "Received command: " << commandString << std::endl;

		Json::Value commandJson;
		try
		{
			json::unserialize(commandString, commandJson);
			command = commandJson["command"].asString();
#ifdef DEBUG
			std::cout << "UDPGyroServer::StartServer - " << command << " received" << std::endl;
#endif

			if (command == "calibrate")
			{
				if (this->sensor->calibrate())
				{
					resp = "{\"success\":true}";
				}
				else
				{
					resp = "{\"success\":false}";
				}
			}
			else if (command == "data")
			{
				devices::DataPacket data;
				if (this->sensor->data(data))
				{
					Json::Value result;
					result["success"] = true;
					result["data"]["deltaT"] = data.deltaT;
					result["data"]["accelerometer"] << data.accelerometer;
					result["data"]["gyro"] << data.gyro;
					result["data"]["filtered"] << data.filtered;
					json::serialize(result, resp);
				}
				else
				{
					resp = "{\"success\":false}";
				}
			}
		}
		catch (std::exception &e)
		{
			std::cout << "EXCEPTION: " << e.what() << std::endl;
			resp = "{\"success\":false}";
		}
		this->SendResponse(resp);
	}
}

std::string
UDPGyroServer::getStateString()
{
	if (state == UPRIGHT)
	{
		return "UPRIGHT";
	}
	else if (state == FALLEN_FORWARD)
	{
		return "FALLEN_FORWARD";
	}
	else if (state == FALLEN_BACKWARD)
	{
		return "FALLEN_BACKWARD";
	}
	else
	{
		return "UNKNOWN_STATE";
	}
}

int UDPGyroServer::SendResponse(std::string const &msg)
{
	const boost::asio::const_buffers_1 &bf = boost::asio::buffer(msg, msg.size());
	int
		sent = 0,
		remaining = msg.size(),
		ret;

	while (remaining > 0)
	{
		ret = this->socket.send_to(bf, this->remoteEP);
		remaining -= ret;
		sent += ret;
	}
	return sent;
}
