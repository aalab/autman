/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 05, 2015
 */

#include "json.hpp"

#include <json/reader.h>
#include <json/writer.h>

Json::Reader jsonReader;
Json::FastWriter jsonWriter;

bool json::unserialize(std::string &json, Json::Value &result)
{
	return jsonReader.parse(json, result, false);
}

void ::json::serialize(Json::Value &value, std::string &result)
{
	result = jsonWriter.write(value);
}
