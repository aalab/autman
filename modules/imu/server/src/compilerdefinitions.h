/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date June 30, 2015
 */

#ifndef COMPILERDEFINITIONS_H
#define COMPILERDEFINITIONS_H

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#ifdef DEBUG
#define VERBOSE_HEADER "[VERBOSE] " << __FILENAME__ << ":" << __LINE__ << ") "
#define VERBOSE(x) std::cout << VERBOSE_HEADER << x << std::endl
#define VERBOSENEL(x) std::cout << VERBOSE_HEADER << x
#define VERBOSEDATAINLINE(d, s) \
	{ \
		boost::io::ios_flags_saver ifs(std::cout); \
		for (unsigned int i = 0; i < static_cast<unsigned int>(s);) \
		{ \
			std::cout << " " << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned int>((d)[i]); \
			i++; \
			if ((i % 8 == 0) \
				std::cout << "   "; \
		} \
		ifs.restore(); \
	 }
#define VERBOSEDATA(d, s) { \
		boost::io::ios_flags_saver ifs(std::cout); \
		std::cout << std::endl; \
		for (unsigned int i = 0; i < static_cast<unsigned int>(s); ) \
		{ \
			std::cout << " " << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned int>((d)[i]); \
			i++; \
			if (i < static_cast<unsigned int>(s)) \
			{ \
				if (i % 16 == 0) \
					std::cout << std::endl; \
				else if (i % 8 == 0) \
					std::cout << "  "; \
			} \
		} \
		std::cout << std::endl; \
		ifs.restore(); \
	 }

#define VERBOSEB(x) std::cout << x << std::endl
#define VERBOSEBNEL(x) std::cout << x
#else
#define VERBOSE_HEADER
#define VERBOSE(x)
#define VERBOSENEL(x)
#define VERBOSEDATA(d, s)
#define VERBOSEB(x)
#define VERBOSEBNEL(x)
#endif

#define ERROR_HEADER "[ERROR] " << __FILENAME__ << ":" << __LINE__ << ") "
#define ERROR(x) std::cout << VERBOSE_HEADER << x << std::endl
#define ERRORNEL(x) std::cout << VERBOSE_HEADER << x
#define ERRORB(x) std::cout << x << std::endl
#define ERRORBNEL(x) std::cout << x

#endif //COMPILERDEFINITIONS_H
