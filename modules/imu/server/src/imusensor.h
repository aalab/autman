/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 11, 2015
 */

#ifndef MOTIONLIBRARY_IMUSENSOR_H
#define MOTIONLIBRARY_IMUSENSOR_H

#include <json/value.h>
#include "serial.h"

namespace devices
{
struct __attribute__((__packed__)) Header
{
	uint16_t header;
	uint8_t command;
};

struct __attribute__((__packed__)) Int32_3
{
	int32_t x;
	int32_t y;
	int32_t z;
};

Json::Value &operator<<(Json::Value &result, Int32_3 &data);

struct __attribute__((__packed__)) DataPacket
{
	Header header;
	int32_t deltaT;
	Int32_3 accelerometer;
	Int32_3 gyro;
	Int32_3 filtered;
};

class ImuSensor
{
enum Protocol
{
	FULL_HEADER = 0xF1F1,

	CMD_CALIBRATE = 0X01,
	CMD_DATA = 0X02,
	CMD_ERROR = 0X00
};
private:
	Serial *port;

	static uint8_t commandCalibrate;
	static uint8_t commandData;
public:
	ImuSensor(Serial *port);

	bool calibrate();

	bool data(DataPacket &data);
};

}


#endif //MOTIONLIBRARY_IMUSENSOR_H
