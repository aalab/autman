/*
 * @author Jamillo Santos <jamillo@gmail.com>
 * @date July 05, 2015
 */

#ifndef MOTIONLIBRARY_JSON_HPP
#define MOTIONLIBRARY_JSON_HPP

#include <string>
#include <json/value.h>

namespace json
{
	bool unserialize(std::string &json, Json::Value &result);
	void serialize(Json::Value &value, std::string &result);
}

#endif //MOTIONLIBRARY_JSON_HPP
