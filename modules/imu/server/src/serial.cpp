//-----------------------------------------
// PROJECT: MotionController
// AUTHOR : POSIX - Jacky Baltes <jacky@cs.umanitoba.ca>
//          Windows - Stela H. Seo <shb8775@hotmail.com>
//-----------------------------------------

//------------------------------------------------------------------------------
// INCLUDE HEADERS
//------------------------------------------------------------------------------

#include "serial.h"

#include <string.h>

#ifdef _WIN32
#include <windows.h>
#else

#include <iomanip>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include "compilerdefinitions.h"

#endif


//------------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------------
static BAUDRATE_TYPE BAUDRATES_SPEED[] =
	{
#ifdef _WIN32
	CBR_110,
	CBR_300,
	CBR_600,
	CBR_1200,
	CBR_2400,
	CBR_4800,
	CBR_9600,
	CBR_19200,
	CBR_38400,
	CBR_57600,
	CBR_115200,
	CBR_256000,
#else
		B110,
		B300,
		B600,
		B1200,
		B2400,
		B4800,
		B9600,
		B19200,
		B38400,
		B57600,
		B115200,
		B500000,
		B1000000,
#endif
	};
static const int BAUDRATES__LENGTH = (sizeof(BAUDRATES_SPEED) / sizeof(BAUDRATES_SPEED[0]));

static const std::string BAUDRATES_STRING[] =
	{
		"B110",
		"B300",
		"B600",
		"B1200",
		"B2400",
		"B4800",
		"B9600",
		"B19200",
		"B38400",
		"B57600",
		"B115200",
#ifdef _WIN32
		"B256000",
#else
		"B500000",
		"B1000000",
#endif
	};


//------------------------------------------------------------------------------
// FUNCTIONS / METHODS
//------------------------------------------------------------------------------
Serial::Serial()
	: _opened(false)
{
#ifdef _WIN32
	_hCommPort = 0;
#else
	this->_fd = 0;
#endif
}

Serial::~Serial(void)
{
	this->close();
}

int
Serial::close()
{
	if (this->isValid())
	{
		this->flush();
#ifdef _WIN32
		SetCommState( _hCommPort, &_olddcb );
		CloseHandle( _hCommPort );
		_hCommPort = 0;
#else
		tcsetattr(_fd, TCSANOW, &_oldtio);
		::close(_fd);
		_fd = 0;

		this->_opened = false;
#endif
	}
	return 1;
}


void Serial::flush(void)
{
#ifdef _WIN32
	FlushFileBuffers( _hCommPort );
#else
	tcflush(_fd, TCIOFLUSH);
#endif
}


void Serial::flushRx(void)
{
#ifdef _WIN32
	FlushFileBuffers( _hCommPort );
#else
	tcflush(_fd, TCIFLUSH);
#endif
}


void Serial::flushTx(void)
{
#ifdef _WIN32
	FlushFileBuffers( _hCommPort );
#else
	tcflush(_fd, TCOFLUSH);
#endif
}


bool Serial::isValid(void) const
{
#ifdef _WIN32
	return _hCommPort != 0;
#else
	return (this->_fd > 0);
#endif
}

bool
Serial::open()
{
	bool success = false;

	this->close(); // Close opened serial port first

#ifdef _WIN32
	// Taken from UnixSerialPort Communcation in Windows
	// http://www.codeproject.com/KB/system/serial_com.aspx
	DCB newdcb;
	_hCommPort = CreateFileA( _devname, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );
	if( _hCommPort == 0 || _hCommPort == INVALID_HANDLE_VALUE )
	{   // failed
	}
	else
	{
		memset( &_olddcb, 0, sizeof( DCB ) );
		_olddcb.DCBlength = sizeof( DCB );
		GetCommState( _hCommPort, &_olddcb );
		memcpy( &newdcb, &_olddcb, sizeof( DCB ) );

		newdcb.BaudRate = _baudrateSpeed;
		newdcb.ByteSize = 8;
		newdcb.Parity = NOPARITY;
		newdcb.StopBits = ONE5STOPBITS;
		SetCommState( _hCommPort, &newdcb );
		success = true;
	}
#else
	// Taken from the UnixSerialPort Programming HOWTO
	// Open modem device for reading and writing and not as controlling tty
	// because we don't want to get killed if linenoise sends CTRL-C.
	VERBOSE("Trying to open " << this->_device << " at " << this->_baudrate);
	_fd = ::open(this->_device.c_str(), O_RDWR | O_NOCTTY);
	if (_fd < 0)
	{
		ERROR("Opening of serial port failed " << this->_device << ". Error: " << _fd);
	}
	else
	{
		VERBOSE("Opened the first part.");
		tcgetattr(_fd, &_oldtio); /* save current serial port settings */
		memset(&this->_currentTIO, 0, sizeof(this->_currentTIO)); /* clear struct for new port settings */

		// BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
		// CRTSCTS : output hardware flow control (only used if the cable has all necessary lines. See sect. 7 of UnixSerialPort-HOWTO)
		// CS8     : 8n1 (8bit,no parity,1 stopbit)
		// CLOCAL  : local connection, no modem contol
		// CREAD   : enable receiving characters
		cfsetispeed(&this->_currentTIO, this->_baudrateSpeed);
		cfsetospeed(&this->_currentTIO, this->_baudrateSpeed);

		this->_currentTIO.c_cflag = _baudrateSpeed | CS8 | CLOCAL | CREAD;

		// IGNPAR  : ignore bytes with parity errors
		// ICRNL   : map CR to NL (otherwise a CR input on the other computer will not terminate input)
		//           otherwise make device raw (no other input processing)
		this->_currentTIO.c_iflag = IGNPAR | IXANY; // | IGNBRK; //| ICRNL;

		// Raw output
		this->_currentTIO.c_oflag = 0;
		this->_currentTIO.c_lflag = 0;

		// now clean the modem line and activate the settings for the port
		tcsetattr(_fd, TCSANOW, &this->_currentTIO);

		this->flushTx();
		this->flushRx();

		return true;
	}
#endif
	return success;
}


bool Serial::readChar(char *pChar)
{
	if (isValid())
	{
#ifdef _WIN32
		DWORD nBytes = 0;
		success = (ReadFile( _hCommPort, pChar, 1, &nBytes, 0 ) && nBytes == 1);
#else
		std::lock_guard<std::mutex> lock(_mutex);
		return (read(_fd, pChar, 1) == 1);
#endif
	}
	return false;
}

int
Serial::readDataWithLock(uint8_t *data, unsigned int length, unsigned int timeout)
{
	std::lock_guard<std::mutex> lock(_mutex);
	return readData(data, length, timeout);
}

int
Serial::readData(uint8_t *data, unsigned int length, unsigned int timeout)
{

	if (isValid())
	{
#ifdef _WIN32
		COMMTIMEOUTS timeouts = {0};
		DWORD nBytes = 0;

		timeouts.ReadIntervalTimeout = timeout * 1000;
		timeouts.ReadTotalTimeoutMultiplier = 0;
		timeouts.ReadTotalTimeoutConstant = 10;
		timeouts.WriteTotalTimeoutMultiplier = 0;
		timeouts.WriteTotalTimeoutConstant = 0;
		SetCommTimeouts( _hCommPort, &timeouts );
		ReadFile( _hCommPort, data, length, &nBytes, 0 );
		received = nBytes;
#else

		ssize_t
			received = 0,
			retVal;
		fd_set readfs; // file descriptor set
		int
			maxfd = _fd + 1,
			nSources;
		struct timeval timeOut;

		FD_ZERO(&readfs);

		FD_SET(_fd, &readfs); // set testing
		timeOut.tv_sec = 0;
		timeOut.tv_usec = timeout * 1000;

		while ((received < length) && (0 < (nSources = select(maxfd, &readfs, 0, 0, &timeOut))))
		{
			if (FD_ISSET(_fd, &readfs))
			{
				retVal = read(_fd, data, (size_t) (length - received));
				if (retVal <= 0)
				{   // failed to read from serial port
					break;
				}
				data = data + retVal;
				received = received + static_cast<unsigned int>( retVal );
			}
		}
		return received;
#endif
	}
	return 0;
}


bool
Serial::writeChar(char ch)
{
	bool success = false;

	if (isValid())
	{
		if (ch == '\n')
		{
			ch = '\r';
		}
#ifdef _WIN32
		DWORD nBytes = 0;
		success = (WriteFile( _hCommPort, &ch, 1, &nBytes, 0 ) && nBytes == 1);
#else
		std::lock_guard<std::mutex> lock(_mutex);
		success = (write(_fd, &ch, 1) == 1);
#endif
	}
	return success;
}


int
Serial::writeDataWithLock(uint8_t const *data, unsigned int length)
{
	std::lock_guard<std::mutex> lock(_mutex);
	return this->writeData(data, length);
}

int
Serial::writeData(uint8_t const *data, unsigned int length)
{
	int written = 0;
	if (isValid())
	{
#ifdef _WIN32
		DWORD nBytes = 0;
		WriteFile( _hCommPort, data, length, &nBytes, 0 );
		written = nBytes;
#else
		for (unsigned int i = 0; (written <= 0) && (i < 3); i++)
		{
			written = write(_fd, data, (size_t) length);
			VERBOSE("Written " << written << " bytes");
			if (written <= 0)
			{
				this->close();
			}
		}
		this->flushTx();
#endif
	}
	VERBOSE("written=" << written);
	return written;
}


int Serial::writeSlowly(const char *data, int length, int milliseconds)
{
	int written = 0;

	std::lock_guard<std::mutex> lock(_mutex);
	for (int i = 0; i < length; ++i)
	{
		if (writeChar(data[i]))
		{
			++written;
		}
#ifdef _WIN32
		Sleep( milliseconds );
#else
		usleep(milliseconds * 1000);
#endif
	}
	return written;
}

std::string Serial::strBaudrate(BAUDRATE_TYPE speed)
{
	std::string str = "";
	int index = 0;

	while (index < BAUDRATES__LENGTH)
	{
		if (speed == BAUDRATES_SPEED[index])
		{
			str = BAUDRATES_STRING[index];
			index = BAUDRATES__LENGTH;
		}
		++index;
	}
	return str;
}

void Serial::lock()
{
	this->_mutex.lock();
}

void Serial::unlock()
{
	this->_mutex.unlock();
}

const std::string &Serial::baudrate() const
{
	return this->_baudrate;
}

bool Serial::baudrate(std::string str)
{
	BAUDRATE_TYPE speed = 0;
	int index = 0;

	bool found = false;

	if (str.at(0) != 'B')
	{
		str = std::string("B") + str;
	}

	while (index < BAUDRATES__LENGTH)
	{
		if (str == BAUDRATES_STRING[index])
		{
			this->_baudrateSpeed = BAUDRATES_SPEED[index];
			return true;
		}
		++index;
	}
	ERROR("ERROR: Cannot find baudrate " << str);
	return false;
}

const std::string Serial::deviceName() const
{
	return this->_device;
}

Serial& Serial::deviceName(std::string deviceName)
{
	this->_device = deviceName;
	return *this;
}

SerialLockGuard::SerialLockGuard(Serial &serial)
	: _serial(serial)
{
	serial.lock();
}

SerialLockGuard::~SerialLockGuard()
{
	this->_serial.unlock();
}
