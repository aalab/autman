#ifndef __UDP_GYRO_SERVER_HPP__
#define __UDP_GYRO_SERVER_HPP__

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/ptree.hpp>
#include "imusensor.h"

#define GYRO_BUFFER_SIZE 85
#define MIN_ALLOWED_ROLL -30
#define MAX_ALLOWED_ROLL 30
#define MIN_ALLOWED_PITCH -30
#define MAX_ALLOWED_PITCH 30

class Serial;

class UDPGyroServer
{
public:
	UDPGyroServer(boost::property_tree::ptree &pt, boost::asio::io_service &io_service, devices::ImuSensor* sensor);

	void StartServer();

	std::string getStateString();

	typedef enum gyro_state_t
	{
		UPRIGHT = 0,
		FALLEN_FORWARD = 1,
		FALLEN_BACKWARD = 2
	} GyroState;

private:
	int SendResponse(std::string const &msg);

private:
	GyroState state;

	boost::asio::ip::udp::socket socket;
	boost::asio::ip::udp::endpoint remoteEP;
	devices::ImuSensor *sensor;
};

#endif /* __UDP_GYRO_SERVER_HPP__ */
