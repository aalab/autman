IMU Module
==========

IMU Arduino
-----------

On the `imuArduino`, this is the firmware applied to the Arduino Nano v3.0 (mega328). The protocol is very simple and
the whole project is an adaption from this project:
http://www.geekmomprojects.com/gyroscopes-and-accelerometers-on-a-chip/ .

### Communication

The communication is using a `38400` baudrate.

#### Calibration

##### Request

Binary command: `0x01`
ASCII command: `c`

##### Response

Binary response: `0xF1 0xF1 0x01`

#### Sensor data

##### Request

Binary command: `0x02`
ASCII command: `d`

##### Response

Binary response:

	0xf1 0xf1 0x02 0x03 0x00 0x00 0x00 0x38
	0x00 0x00 0x00 0x33 0x11 0x00 0x00 0x00
	0x00 0x00 0x00 0xc4 0x02 0x00 0x00 0x5c
	0x04 0x00 0x00 0x52 0x04 0x00 0x00 0x61
	0x00 0x00 0x00 0x69 0x0f 0x00 0x00 0x52
	0x04 0x00 0x00

Struct scheme:


	struct __attribute__((__packed__)) Int32_3
	{
		int32_t x;
		int32_t y;
		int32_t z;
	};

	struct __attribute__((__packed__)) DataPacket
	{
		uint16_t header;
		uint8_t command;
		int32_t deltaT;
		Int32_3 accelerometer;								// Value in degrees * 1000
		Int32_3 rawGyro;									// Value in degrees * 1000
		Int32_3 gyro;										// Value in degrees * 1000
	};
