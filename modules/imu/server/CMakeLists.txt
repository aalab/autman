CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

project(imuModule)

set (VERSION_MAJOR 1)
set (VERSION_MINOR 0)

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Debug CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=c++0x -DDEBUG -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++0x -Wall")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG -Wall")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")

configure_file (
	"${PROJECT_SOURCE_DIR}/src/config.h.in"
	"${PROJECT_BINARY_DIR}/include/config.h"
)

include_directories("${PROJECT_BINARY_DIR}/include")

## Boost

set(BOOST_ROOT "$ENV{HOME}/libs/libboost_1_59_0")
set(BOOST_INCLUDEDIR "${BOOST_ROOT}/include")
set(BOOST_LIBRARYDIR "${BOOST_ROOT}/lib")
find_package(Boost 1.59.0 REQUIRED system unit_test_framework)
include_directories(${BOOST_INCLUDEDIR})
link_directories(${BOOST_LIBRARYDIR})

## jsoncpp

set(jsoncpp_ROOT "$ENV{HOME}/libs/jsoncpp")
message(JSON dir: ${jsoncpp_ROOT})
include_directories(${jsoncpp_ROOT}/include)
link_directories(${jsoncpp_ROOT}/lib)
find_library(jsoncpp NAMES jsoncpp)

add_executable( imu_module
	src/main.cpp
	src/serial.cpp
	src/json.cpp
	src/imusensor.cpp
	src/udpgyroserver.cpp
)

add_definitions()

target_link_libraries(imu_module
   "boost_program_options"
   "boost_system"
   "boost_thread"
   "pthread"
   "jsoncpp"
)


