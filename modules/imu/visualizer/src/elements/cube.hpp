/**
 * @author J. Santos <jamillo@gmail.com>
 * @date November 25, 2015
 **/

#ifndef VISUALIZER_ELEMENTS_CUBE_HPP
#define VISUALIZER_ELEMENTS_CUBE_HPP

#include "../draw/drawable.hpp"
#include "../draw/colour.hpp"

namespace elements
{

class CubeColours
{
private:
	draw::Colour _top;
	draw::Colour _bottom;
	draw::Colour _left;
	draw::Colour _right;
	draw::Colour _front;
	draw::Colour _back;
public:
	CubeColours() :
		_top(draw::Colour::RED), _bottom(draw::Colour::ORANGE), _left(draw::Colour::YELLOW),
		_right(draw::Colour::WHITE), _front(draw::Colour::BLUE), _back(draw::Colour::GREEN)
	{ }

	draw::Colour &top()
	{
		return this->_top;
	}

	void top(draw::Colour &top)
	{
		this->_top = top;
	}

	draw::Colour &bottom()
	{
		return this->_bottom;
	}

	void bottom(draw::Colour &bottom)
	{
		this->_bottom = bottom;
	}

	draw::Colour &left()
	{
		return this->_left;
	}

	void left(draw::Colour &left)
	{
		this->_left = left;
	}

	draw::Colour &right()
	{
		return this->_right;
	}

	void right(draw::Colour &right)
	{
		this->_right = right;
	}

	draw::Colour &front()
	{
		return this->_front;
	}

	void front(draw::Colour &front)
	{
		this->_front = front;
	}

	draw::Colour &back()
	{
		return this->_back;
	}

	void back(draw::Colour &back)
	{
		this->_back = back;
	}

	void colour(const draw::Colour &colour)
	{
		this->_top = colour;
		this->_bottom = colour;
		this->_left = colour;
		this->_right = colour;
		this->_front = colour;
		this->_back = colour;
	}
};

class Cube
	: public draw::DrawableObject
{
private:
	static draw::Colour lineColour;

	CubeColours _colour;
public:
	virtual void draw() override
	{
		glBegin(GL_QUADS);
		// Top
		this->_colour.top().draw();
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f, -0.5f);
		// Bottom
		this->_colour.bottom().draw();
		glVertex3f(-0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		// Left
		this->_colour.left().draw();
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f,  0.5f);
		// Right
		this->_colour.right().draw();
		glVertex3f( 0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		// Front
		this->_colour.front().draw();
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f,  0.5f);
		// Back
		this->_colour.back().draw();
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glEnd();

		// Draws the line contour of the cube.
		glBegin(GL_LINES);
		lineColour.draw();
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f,  0.5f);
		glVertex3f( 0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f, -0.5f);
		glVertex3f( 0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f, -0.5f,  0.5f);
		glVertex3f(-0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f, -0.5f);
		glVertex3f( 0.5f,  0.5f,  0.5f);
		glVertex3f(-0.5f,  0.5f,  0.5f);
		glEnd();
	}

	CubeColours &colour()
	{
		return this->_colour;
	}
};

draw::Colour Cube::lineColour(1.0f, 1.0f, 1.0f, 0.6f);
}

#endif //VISUALIZER_ELEMENTS_CUBE_HPP
