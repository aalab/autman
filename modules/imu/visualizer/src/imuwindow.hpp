/**
 * @author J. Santos <jamillo@gmail.com>
 * @date December 14, 2015
 **/

#ifndef VISUALIZER_IMUWINDOW_HPP
#define VISUALIZER_IMUWINDOW_HPP

#include <chrono>
#include <thread>
#include <ostream>
#include <iostream>
#include <services/gyro.h>
#include "elements/cube.hpp"
#include "draw/stage.hpp"

class ImuWindow
{
private:
	bool _initialized;
	bool _running;

	services::data::Point3D _offset;

	services::Gyro _gyro;

	elements::Cube *_accelerometerCube;
	elements::Cube *_gyroCube;
	elements::Cube *_filteredCube;

	draw::Stage _stage;
	std::unique_ptr<std::thread> updateThread;
public:
	ImuWindow()
		: _initialized(false), _running(false), _gyro("127.0.0.1", 1414)
	{ }

	virtual ~ImuWindow()
	{
		if (this->_running && this->updateThread)
		{
			this->_running = false;
			this->updateThread->join();
		}
	}

	virtual void init()
	{
		VERBOSE("Initializing...");

		this->_stage.add(this->_accelerometerCube = new elements::Cube());
		this->_accelerometerCube->colour().colour(draw::Colour::RED);
		this->_accelerometerCube->position().xyz(-0.65f, 0.0f, 0.0f);
		this->_accelerometerCube->size().xyz(0.3f, 0.5f, 0.02f);

		this->_stage.add(this->_gyroCube = new elements::Cube());
		this->_gyroCube->colour().colour(draw::Colour::GREEN);
		this->_gyroCube->position().xyz(0.0f, 0.0f, 0.0f);
		this->_gyroCube->size().xyz(0.3f, 0.5f, 0.02f);

		this->_stage.add(this->_filteredCube = new elements::Cube());
		this->_filteredCube->colour().colour(draw::Colour::BLUE);
		this->_filteredCube->position().xyz(0.65f, 0.0f, 0.0f);
		this->_filteredCube->size().xyz(0.3f, 0.5f, 0.02f);

		this->updateThread.reset(new std::thread(&ImuWindow::runUpdateTrampolin, std::ref(*this)));

		this->_initialized = true;
		VERBOSE("Initialized.");
	}

	void runUpdateTrampolin()
	{
		this->_running = true;
		services::data::gyro::DataResponse data;
		while (this->_running)
		{
			if (this->_gyro.data(data))
			{
				VERBOSE("Updating cubes data.");
				this->_accelerometerCube->rotate().xyz(
					D2R(data.accelerometer.x) + 360.0 - this->_offset.x,
					D2R(data.accelerometer.y) + 360.0 - this->_offset.y,
					D2R(data.accelerometer.z) + 360.0 - this->_offset.z
				);
				this->_gyroCube->rotate().xyz(
					D2R(data.gyro.x) + 360.0f - this->_offset.x,
					D2R(data.gyro.y) + 360.0f - this->_offset.y,
					D2R(data.gyro.z) + 360.0f - this->_offset.z
				);
				this->_filteredCube->rotate().xyz(
					D2R(data.filtered.x) + 360.0f - this->_offset.x,
					D2R(data.filtered.y) + 360.0f - this->_offset.y,
					D2R(data.filtered.z) + 360.0f - this->_offset.z
				);
			}
			else
			{
				ERROR("Cannot read data.");
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}

	void draw()
	{
		VERBOSE("Accelerometer: "
			<< this->_accelerometerCube->rotate().x() << ":"
			<< this->_accelerometerCube->rotate().y() << ":"
			<< this->_accelerometerCube->rotate().z()
		);
		VERBOSE("Gyro: "
			<< this->_gyroCube->rotate().x() << ":"
			<< this->_gyroCube->rotate().y() << ":"
			<< this->_gyroCube->rotate().z()
		);
		VERBOSE("Filtered: "
			<< this->_filteredCube->rotate().x() << ":"
			<< this->_filteredCube->rotate().y() << ":"
			<< this->_filteredCube->rotate().z()
		);
		this->_stage.draw();
	}

	void calibrate()
	{
		this->_gyro.calibrate();
	}

	void offset()
	{
		services::data::gyro::DataResponse gyroData;
		if (this->_gyro.data(gyroData))
		{
			this->_offset = gyroData.filtered;
		}
		else
		{
			ERROR("Error reading data from server.");
		}
	}
};

#endif // VISUALIZER_IMUWINDOW_HPP