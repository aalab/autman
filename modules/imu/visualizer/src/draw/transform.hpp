#ifndef SIMULATION_TRANSFORM_HPP
#define SIMULATION_TRANSFORM_HPP

namespace draw
{
	/**
	 * Push and pop transformations.
	 */
	class TransformGuard
	{
	public:
		TransformGuard()
		{
			glPushMatrix();
		}

		~TransformGuard()
		{
			glPopMatrix();
		}
	};
}

#endif //SIMULATION_TRANSFORM_HPP
