/**
 * @author J. Santos <jamillo@gmail.com>
 * @date November 25, 2015
 **/

#ifndef VISUALIZER_DRAW_COLOUR_HPP
#define VISUALIZER_DRAW_COLOUR_HPP

#include "drawable.hpp"

namespace draw
{
class Colour
	: public Drawable
{
public:
	static Colour BLACK;
	static Colour WHITE;
	static Colour RED;
	static Colour GREEN;
	static Colour GREEN_ARMY;
	static Colour BLUE;
	static Colour YELLOW;
	static Colour ORANGE;
	static Colour PURPLE;
	static Colour GRAY_LIGHT;
	static Colour GRAY;

private:
	float _r;
	float _g;
	float _b;
	float _a;
public:
	Colour(float r, float g, float b, float a)
		: _r(r), _g(g), _b(b), _a(a)
	{ }

	Colour(float r, float g, float b)
		: Colour(r, g, b, 1.0f)
	{ }

	Colour(const Colour &&colour)
	{
		*this = colour;
	}

	Colour(const Colour &colour)
	{
		*this = colour;
	}

	Colour(const Colour *colour)
	{
		*this = colour;
	}

	float r() const
	{
		return this->_r;
	}

	void r(float r)
	{
		this->_r = r;
	}

	float g() const
	{
		return this->_g;
	}

	void g(float g)
	{
		this->_g = g;
	}

	float b() const
	{
		return this->_b;
	}

	void b(float b)
	{
		this->_b = b;
	}

	float a() const
	{
		return this->_a;
	}

	void a(float a)
	{
		this->_a = a;
	}

	virtual void beginDraw()
	{ }

	virtual void draw()
	{
		glColor4f(this->_r, this->_g, this->_b, this->_a);
	}

	virtual void endDraw()
	{ }

	virtual bool isInvalid()
	{
		return false;
	}

	Colour &operator=(const Colour *colour)
	{
		this->_r = colour->_r;
		this->_g = colour->_g;
		this->_b = colour->_b;
		this->_a = colour->_a;
		return *this;
	}

	Colour &operator=(const Colour &colour)
	{
		this->_r = colour._r;
		this->_g = colour._g;
		this->_b = colour._b;
		this->_a = colour._a;
		return *this;
	}
};

draw::Colour draw::Colour::BLACK(0.0f, 0.0f, 0.0f);
draw::Colour draw::Colour::WHITE(1.0f, 1.0f, 1.0f);
draw::Colour draw::Colour::RED(1.0f, 0.0f, 0.0f);
draw::Colour draw::Colour::GREEN(0.0f, 1.0f, 0.0f);
draw::Colour draw::Colour::GREEN_ARMY(0.0f, 0.6f, 0.2f);
draw::Colour draw::Colour::BLUE(0.0f, 0.0f, 1.0f);
draw::Colour draw::Colour::YELLOW(1.0f, 1.0f, 0.0f);
draw::Colour draw::Colour::ORANGE(1.0f, 0.6f, 0.0f);
draw::Colour draw::Colour::PURPLE(0.5f, 0.0f, 0.5f);
draw::Colour draw::Colour::GRAY_LIGHT(0.8f, 0.8f, 0.8f);
draw::Colour draw::Colour::GRAY(0.4f, 0.4f, 0.4f);
}

#endif //VISUALIZER_DRAW_COLOUR_HPP
