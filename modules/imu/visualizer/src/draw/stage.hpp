#ifndef SIMULATION_DRAW_STAGE_HPP
#define SIMULATION_DRAW_STAGE_HPP

#include "drawable.hpp"

#include <vector>

namespace draw
{
	class Stage
	{
	protected:
		std::vector<Drawable*> objects;
	public:
		virtual void draw()
		{
			for (Drawable *drawable : this->objects)
			{
				drawable->beginDraw();
				drawable->draw();
				drawable->endDraw();
			}
		}

		virtual void add(Drawable *drawable)
		{
			this->objects.push_back(drawable);
		}
	};
}

#endif //SIMULATION_STAGE_HPP
