
#ifndef SIMULATION_DRAWNABLE_HPP
#define SIMULATION_DRAWNABLE_HPP

#include <GL/gl.h>

#include "vector.hpp"
#include "transform.hpp"
#include "value.hpp"
#include "../definitions.h"

namespace draw
{
	class Drawable
	{
	public:
		virtual void beginDraw() = 0;
		virtual void draw() = 0;
		virtual void endDraw() = 0;

		virtual bool isInvalid() = 0;
	};

	class DrawableObject
		: public Drawable
	{
	protected:
		bool _absolute;

		Vector3D _position;
		Vector3D _rotate;
		Vector3D _size;
		Vector3D _center;

		virtual void applyTransformation()
		{
			glPushMatrix();
			glTranslatef(this->_position.x(), this->_position.y(), this->_position.z());
			glPushMatrix();
			if (this->_rotate.x() != 0.0f)
				glRotatef(R2D(this->_rotate.x()), 1.0f, 0.0f, 0.0f);

			if (this->_rotate.y() != 0.0f)
				glRotatef(R2D(this->_rotate.y()), 0.0f, 1.0f, 0.0f);

			if (this->_rotate.z() != 0.0f)
				glRotatef(R2D(this->_rotate.z()), 0.0f, 0.0f, 1.0f);
		}

		virtual void unapplyTransformation()
		{
			glPopMatrix();
			glPopMatrix();
		}
	public:
		DrawableObject()
			: _absolute(false), _size(1.0f)
		{ }

		virtual bool isAbsolute()
		{
			return this->_absolute;
		}

		virtual Vector3D &position()
		{
			return this->_position;
		}

		virtual void position(const Vector3D &position)
		{
			this->_position = position;
		}

		virtual Vector3D &rotate()
		{
			return this->_rotate;
		}

		virtual void rotate(const Vector3D &rotate)
		{
			this->_rotate = rotate;
		}

		virtual Vector3D &size()
		{
			return this->_size;
		}

		virtual void size(const Vector3D &size)
		{
			this->_size = size;
		}

		virtual Vector3D &center()
		{
			return this->_center;
		}

		virtual void center(const Vector3D &center)
		{
			this->_center = center;
		}

		virtual void beginDraw() override
		{
			this->applyTransformation();

			// Changes the center before the translation.
			glTranslatef(this->_size.x() * this->_center.x(), this->_size.y() * this->_center.y(), this->_size.z() * this->_center.z());
			glPushMatrix();
			glScalef(this->_size.x(), this->_size.y(), this->_size.z());
		}

		virtual void endDraw() override
		{
			this->unapplyTransformation();
			glPopMatrix();
		}

		virtual bool isInvalid() override
		{
			return false;
		}
	};
}

#endif //SIMULATION_DRAWNABLE_HPP
