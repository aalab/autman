#ifndef SIMULATION_VECTOR_HPP
#define SIMULATION_VECTOR_HPP

namespace draw
{
	class Vector3D
	{
	private:
		float _x;
		float _y;
		float _z;

		bool _dirty;
	public:
		Vector3D(float x, float y, float z):
			_x(x), _y(y), _z(z), _dirty(false)
		{ }

		Vector3D(float value)
			: Vector3D(value, value, value)
		{ }

		Vector3D():
			Vector3D(0, 0, 0)
		{ }

		bool isDirty()
		{
			return this->_dirty;
		}

		void clean()
		{
			this->_dirty = false;
		}

		float x()
		{
			return this->_x;
		}

		Vector3D &xyz(float x, float y, float z)
		{
			this->_x = x;
			this->_y = y;
			this->_z = z;
			this->_dirty = true;
			return *this;
		}

		Vector3D &xyz(float value)
		{
			this->_x = value;
			this->_y = value;
			this->_z = value;
			this->_dirty = true;
			return *this;
		}

		Vector3D &x(float value)
		{
			this->_x = value;
			this->_dirty = true;
			return *this;
		}

		float y()
		{
			return this->_y;
		}

		Vector3D &y(float value)
		{
			this->_y = value;
			this->_dirty = true;
			return *this;
		}

		float z()
		{
			return this->_z;
		}

		Vector3D &z(float value)
		{
			this->_z = value;
			this->_dirty = true;
			return *this;
		}

		Vector3D &operator=(const Vector3D &source)
		{
			this->_dirty =
				(this->_x != source._x)
				|| (this->_y != source._y)
				|| (this->_z != source._z);

			this->_x = source._x;
			this->_y = source._y;
			this->_z = source._z;
			return *this;
		}

		Vector3D &operator=(const Vector3D *source)
		{
			this->_dirty =
				(this->_x != source->_x)
				|| (this->_y != source->_y)
				|| (this->_z != source->_z);

			this->_x = source->_x;
			this->_y = source->_y;
			this->_z = source->_z;
			return *this;
		}
	};
}

#endif //SIMULATION_VECTOR_HPP
